Rails.application.routes.draw do

	require 'sidekiq/web'
  mount Sidekiq::Web, at: '/sidekiq'
  mount Ckeditor::Engine => '/ckeditor'
  
	devise_for :admin_users, ActiveAdmin::Devise.config
	ActiveAdmin.routes(self)
	resources :slides
	resources :discounts
	post '/discounts/:id/like', to: 'discounts#like', as: :discount_like

	post '/pages/save_location', to: 'pages#save_location'
	post '/pages/update_location', to: 'pages#update_location', as: :update_location
	get '/favorite-discounts', to: 'users#favorite_discounts', as: :favorite_discounts
	get '/redeem', to: 'users#redeem', as: :redeem_deals
	get '/my-purchase', to: 'users#my_purchase', as: :my_purchase
	get '/my-sales', to: 'users#my_sales', as: :my_sales
	post '/mark_deal_redeem/:item_id', to: 'users#mark_deal_redeem', as: :mark_deal_redeem
	post '/users/send_deal_as_gift/:item_id', to: 'users#send_deal_as_gift', as: :send_deal_as_gift
	get '/users/accept-deal-gift', to: 'users#accept_deal_gift', as: :accept_deal_gift

	resources :affiliates, only: []
	get '/affiliations', to: 'affiliates#index', as: :affiliations
	match 'affiliates/create_business', to: 'affiliates#create_business', as: :create_business, via: [:get, :post]
	match 'affiliates/update_business/:slug', to: 'affiliates#update_business', as: :update_business, via: [:get, :put]
	match 'affiliates/create_discount', to: 'affiliates#create_discount', as: :create_discount, via: [:get, :post]
	match 'affiliates/update_discount/:slug', to: 'affiliates#update_discount', as: :update_discount, via: [:get, :put]
	post '/pages/load_states', to: 'pages#load_states', as: :load_states
	get '/payout/request', to: 'pages#request_payout', as: :request_payout
	post 'payout/submit_request', to: 'pages#submit_payout_request', as: :submit_payout_request
	

	root 'pages#index'
	get '/search', to: 'pages#search', as: :search
	post '/subscribe', to: 'pages#subscribe', as: :subscribe

	devise_for :users,
	controllers: {registrations: :registrations, omniauth_callbacks: :omniauth_callbacks, sessions: :sessions}

	as :user do
		get "/login", to: 'sessions#new'
		post "/login", to: 'sessions#create'
		get '/join', to: 'registrations#new'
		post '/join', to: 'registrations#create'
		delete '/logout', to: 'sessions#destroy'
		get '/forgot_password', to: 'devise/passwords#new'
		post '/forgot_password', to: 'devise/passwords#create'
		get '/account', to: 'registrations#edit'
		put '/account', to: 'registrations#update'
		get '/profile', to: 'users#profile'
		get '/:type/:slug', to: 'pages#show_account', as: :show_account
		post 'update_profile', to: 'users#update_profile'
		get '/merchant_account', to: 'users#merchant_account', as: :merchant_account
	end
  get '/stripe_connect', to: 'users#stripe_connect', as: :stripe_connect
  get '/stripe_connect_callback', to: 'users#stripe_connect_callback', as: :stripe_connect_callback
  
  resources :carts, only: []
	get '/cart', to: 'carts#index', as: :carts
	post '/add_to_cart', to: 'carts#add_to_cart', as: :add_to_cart
	delete '/remove_from_cart', to: 'carts#remove_from_cart', as: :remove_from_cart
	post '/checkout', to: "carts#checkout", as: :checkout
	post '/carts/purchase', to: "carts#purchase", as: :purchase
	get '/purchase/invoice/:purchase_number', to: "carts#invoice", as: :invoice

	get '/deals', to: 'pages#discounts'
	get '/businesses', to: 'pages#businesses'
	get '/organizations', to: 'pages#organizations'
	get '/products', to: 'pages#products'

	get '/about', to: 'pages#about'
	get '/contact', to: 'pages#contact'
	get '/terms', to: 'pages#terms'
	get '/privacy', to: 'pages#privacy'
	get '/faq', to: 'pages#faq'
	get '/learn', to: 'pages#learn'
	get '/charity', to: 'pages#charity'
	get '/autism', to: 'pages#autism'
	get '/for-business', to: 'pages#for_business'
	get '/welcome-back', to: 'pages#welcome_back'

end
