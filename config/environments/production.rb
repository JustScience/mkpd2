Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true

  config.serve_static_assets = true

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Mount Action Cable outside main process or domain
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]

  # Use a different cache store in production.
  config.cache_store = :redis_store, ENV['REDISCLOUD_URL'], { :compress => true }

  # Use a real queuing backend for Active Job (and separate queues per environment)
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "mkpd2_0_#{Rails.env}"
  config.action_mailer.perform_caching = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require 'syslog/logger'
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger = ActiveSupport::TaggedLogging.new(logger)
  end

  config.action_mailer.default_url_options = { :host => 'https://www.marketpad.com' }
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :smtp

  config.action_mailer.smtp_settings = {
    address: "smtp.gmail.com",
    port: 587,
    domain: "gmail.com",
    user_name: "alerts@marketpad.com",
    password: "qwer.1234!",
    authentication: "plain",
    enable_starttls_auto: true
  }

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
  ENV['FACEBOOK_APP_ID'] = '1320018081447455'
  ENV['FACEBOOK_SECRET'] = 'ba41a16d25e16537ac244f027f88ff11'
  ENV['GOOGLE_APP_ID'] = '1059869914508-p0u9nlc7rq8q9f65pmdlmptsfmdvh648.apps.googleusercontent.com'
  ENV['GOOGLE_SECRET'] = 'sICDouE-D8yNR054g4e1FvP3'
  ENV['ACCESS_KEY'] = 'AKIAJV7PE7ZWIEODH3WQ'
  ENV['ACCESS_SECRET'] = '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'
  ENV['S3_REGION'] = 'us-west-2'
  ENV['S3_BUCKET'] = 'mkpd-prod'
  ENV['STRIPE_SECRET_KEY'] = 'sk_live_d7mhIws0HpxprhVqSWCj0y2S'
  ENV['STRIPE_PUBLISH_KEY'] = 'pk_live_J2mhy231WG2lyCEVV2tTIcrG'
  ENV['STRIPE_CONNECT_CLIENT_ID'] = 'ca_1AgeJjV1qLQXFLnqtLBSQw4YnJXw0yI7'
  ENV['ADDTHIS_PUB_KEY'] = 'ra-590fe906f9c458d6'


end
