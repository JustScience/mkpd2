Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => 'public, max-age=172800'
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :redis_store, 'redis://localhost:6379/0/cache', {expires_in: 60.minutes, compress: true }
  end

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  config.action_mailer.default_url_options = { :host => 'localhost:3000' }
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :smtp

  config.action_mailer.smtp_settings = {
    address: "smtp.gmail.com",
    port: 587,
    domain: "gmail.com",
    user_name: "alerts@marketpad.com",
    password: "qwer.1234!",
    authentication: "plain",
    enable_starttls_auto: true
  }

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  ENV['FACEBOOK_APP_ID'] = '297568747083890'
  ENV['FACEBOOK_SECRET'] = '62cf2e52043a1e460338407c67a0a50d'
  ENV['GOOGLE_APP_ID'] = '21078351916-gavodgc5dldtedhgml8oqbb5nkdem990.apps.googleusercontent.com'
  ENV['GOOGLE_SECRET'] = 'kQbZg9TALKdhqgIM3Eu-LPyt'
  ENV['ACCESS_KEY'] = 'AKIAJV7PE7ZWIEODH3WQ'
  ENV['ACCESS_SECRET'] = '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'
  ENV['S3_REGION'] = 'us-west-2'
  ENV['S3_BUCKET'] = 'mkpd-dev'
  ENV['STRIPE_SECRET_KEY'] = 'sk_test_OrfAjf8KNZqp9sjn5p5Wm4ja'
  ENV['STRIPE_PUBLISH_KEY'] = 'pk_test_HHCxZRiAgR1xwBa2gO14O04R'
  ENV['STRIPE_CONNECT_CLIENT_ID'] = 'ca_ANuFpycdrkynn1TmQJzzdYVynNJUmKxc'
  ENV['ADDTHIS_PUB_KEY'] = 'ra-590fe906f9c458d6'
  
end
