require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mkpd20
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths += Dir["#{config.root}/lib/**/"]
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    # ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
    #   errors = Array(instance.error_message)[0]
    #   %(#{html_tag}<span class="validation-error" style="color:red;font-size:16px;">&nbsp;#{errors}</span>).html_safe
    # end
  end
end
