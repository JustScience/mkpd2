CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => 'AWS',
    :aws_access_key_id      => ENV['ACCESS_KEY'],
    :aws_secret_access_key  => ENV['ACCESS_SECRET']
  }
  config.storage             = :fog
  config.fog_directory       = ENV['S3_BUCKET']
  config.fog_public = true
  config.cache_dir = "#{Rails.root}/tmp/uploads"
  config.fog_attributes = {'Cache-Control'=>'max-age=2592000'}
end