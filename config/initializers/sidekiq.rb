if Rails.env.production?
  Sidekiq.configure_server do |config|
    Rails.logger = Sidekiq::Logging.logger
    config.redis = { url: ENV['REDISCLOUD_URL'] }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: ENV["REDISCLOUD_URL"]}
  end

  ENV['REDIS_PROVIDER'] = ENV['REDISCLOUD_URL'] || '127.0.0.1:6379'
else
  Sidekiq.configure_server do |config|
    Rails.logger = Sidekiq::Logging.logger
    config.redis = { url: ENV['REDISCLOUD_URL'] }
    # config.periodic do |mgr|
    #   # mgr.register('2 0 * * *', FetchWalletCreditsWorker)
    #   mgr.register('13 13 * * *', FetchWalletCreditsWorker)
    # end
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: ENV["REDISCLOUD_URL"]}
  end

  ENV['REDIS_PROVIDER'] = ENV['REDISCLOUD_URL'] || '127.0.0.1:6379'
end


require 'sidekiq'
require 'sidekiq/web'

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == ["mkpdadmin", "mkpd%$#@!"]
end