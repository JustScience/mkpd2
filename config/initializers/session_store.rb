# Be sure to restart your server when you modify this file.

if Rails.env.production?
  Rails.application.config.session_store :cookie_store, key: '_mkpd2_0_session', domain: '.marketpad.com'
else
	Rails.application.config.session_store :cookie_store, key: '_mkpd2_0_session'
end
