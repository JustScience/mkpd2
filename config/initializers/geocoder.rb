Geocoder.configure(

  # geocoding service (see below for supported options):
  :lookup => :google,

  # IP address geocoding service (see below for supported options):
  :ip_lookup => :ipinfo_io,

  # to use an API key:
  :api_key => "AIzaSyDNR3dxYIvyLzWFn-5lKlM2LLSl4q0ng_w ",

  # geocoding service request timeout, in seconds (default 3):
  :timeout => 20,

  # set default units to kilometers:
  #:units => :km,

)