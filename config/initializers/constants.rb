DEAFULT_RADIUS = 100 ##miles
DEFAULT_LIMIT = 20
SEARCH_RESULT_LIMIT = 10


REFERER_TYPE_BUSINESS = "bf878bd9fa06a3bc6419ebce7368d44830bc8f3f"
REFERER_TYPE_ORGANIZATION = "ff289a2c9a36721e86bf40913e68d44640ap6f3h"
REFERER_TYPE_AFFILIATE = "a3f513bc1282d30a7e2f9fd9b668d97830bc8f3a"
REFERER_TYPE_AGENCY = "a4ec38366c82f6c0068cea0fa168d49830aw8f9k"

REFERER_TYPES = 
{ 
	REFERER_TYPE_BUSINESS =>  "business", REFERER_TYPE_ORGANIZATION => 'organization', 
  REFERER_TYPE_AFFILIATE => 'affiliate', REFERER_TYPE_AGENCY => 'agency'
}

##all in percentage
BUSINESS_COMMISSION = 90
PAYMENT_GATEWAY_COMMISSION = 4
MARKETPAD_COMMISSION = 2
SELLER_COMMISSION = 3
MARKETPAD_POOL_STRATEGY_COMMISSION = 0.5
AFFILIATE_BUSINESS_COMMISSION = 0.5
AFFILIATE_DISCOUNT_COMMISSION = 0
MARKETPAD_EXTRA_COMMISSION = 33.5
SELLER_EXTRA_COMMISSION = 50.0
MARKETPAD_POOL_STRATEGY_EXTRA_COMMISSION = 8.25
AFFILIATE_BUSINESS_EXTRA_COMMISSION = 8.25
AFFILIATE_DISCOUNT_EXTRA_COMMISSION = 0.0


CATEGORIES_CLASS_NAME = ["Food", "Drinks", "Spa", "Entertainment", "Family", "Health", "Fashion", "Automotive"] 

DISCOUNT_TERMS_AND_CONDITIONS = [
	["Terms 1", "<ul>
	<li>Promotional value expires 90 days after purchase.</li>
	<li>Amount paid never expires.</li>
	<li>Appointment required.</li>
	<li>Merchant's standard cancellation policy applies (any fees not to exceed discounted price).</li>
	<li>Limit 1 per person, may buy 1 additional as gift.</li>
	<li>Limit 1 per visit.</li>
	<li>Valid only for option purchased.</li>
	<li>Not valid with any other discounts, promotions.</li>
	<li>Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>
</ul>"],

["Terms 2", "<ul>
	<li> Promotional value expires 120 days after purchase.</li>
	<li> Amount paid never expires.</li>
	<li> Valid only for amount of discount purchased.</li>
	<li> May be repurchased at any time.</li>
	<li> Not valid with any other discounts, promotions.</li>
	<li> Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>

</ul>"],

["Terms 3", "<ul>
	<li> Promotional value expires 120 days after purchase.</li>
	<li> Amount paid never expires.</li>
	<li> Limit 1 per person, may buy 1 additional as a gift.</li>
	<li> Limit 1 per table. Limit 1 per visit. Dine-in only.</li>
	<li> Must purchase a food item.</li>
	<li> Must use promotional value in 1 visit.</li>
	<li> Reservation required.</li>
	<li> Not valid on federal holidays or Thanksgiving, Christmas Eve, New Year's Eve, Valentine's Day, Easter, Mother's Day, Father's Day, Memorial Day, Fourth of July, and Labor Day.</li>
	<li> Not valid with any other discounts, promotions, or happy hour specials.</li>
	<li> Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>

	
</ul>"],

["Terms 4", "<ul>
	<li> Promotional value expires 120 days after purchase.</li>
	<li> Amount paid never expires.</li>
	<li> Limit 1 per person.</li>
	<li> Limit 1 per visit.</li>
	<li> Limit 1 per table.</li>
	<li> Valid only for option purchased.</li>
	<li> Must be 21 or older.</li>
	<li> Must use promotional value in 1 visit.</li>
	<li> Not valid during Paint Night events.</li>
	<li> Not valid with any other discounts, promotions. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>

	
</ul>"],

["Terms 5", "<ul>
	<li> Promotional value expires 180 days after purchase.</li>
	<li> Amount paid never expires.</li>
	<li> Limit 1 per person, may buy 1 additional as gift. Limit 1 per visit.</li>
	<li> Limit 1 per household per year.</li>
	<li> Valid only for option purchased. In-store only.</li>
	<li> Not valid for sale items. Not valid with other specials or offers.</li>
	<li> Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>

	
</ul>"],


["Terms 6", "<ul>
	<li>Promotional value expires 120 days after purchase.</li>
	<li>Amount paid never expires.</li>
	<li>Valid only for option purchased. May be repurchased at any time.</li>
	<li>Not valid with any other discounts, promotions.</li>
	<li>Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>

	
</ul>"],


["Terms 7", "<ul>
	<li>Promotional value expires 120 days after purchase.</li>
	<li>Amount paid never expires.</li>
	<li>Valid only for amount of discount purchased.</li>
	<li>May be repurchased at any time.</li>
	<li>Not valid with any other discounts, promotions.</li>
	<li>Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.</li>
		
</ul>"]


]

AFFILIATES_ARRAY = ["agency", "affiliate", "organization"]