# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180313181308) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"
  enable_extension "fuzzystrmatch"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "affiliate_earnings", force: :cascade do |t|
    t.integer  "profile_id"
    t.string   "profile_type"
    t.decimal  "total_earnings_till_date",   precision: 10, scale: 2, default: "0.0"
    t.decimal  "earnings_after_last_payout", precision: 10, scale: 2, default: "0.0"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.decimal  "last_payout",                precision: 10, scale: 2, default: "0.0"
    t.index ["profile_id"], name: "index_affiliate_earnings_on_profile_id", using: :btree
    t.index ["profile_type"], name: "index_affiliate_earnings_on_profile_type", using: :btree
  end

  create_table "cart_items", force: :cascade do |t|
    t.integer  "cart_id"
    t.integer  "business_id"
    t.integer  "discount_id"
    t.integer  "seller_id"
    t.string   "sold_by"
    t.decimal  "price",                 precision: 10, scale: 2, default: "0.0"
    t.decimal  "taxes",                 precision: 10, scale: 2, default: "0.0"
    t.decimal  "total_price",           precision: 10, scale: 2, default: "0.0"
    t.decimal  "shipping",              precision: 10, scale: 2, default: "0.0"
    t.string   "currency",                                       default: "USD"
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "sales_purchase_number"
    t.boolean  "redeemed",                                       default: false
    t.integer  "redeemed_by"
    t.datetime "redeemed_time"
    t.boolean  "gift",                                           default: false
    t.string   "giftee_email"
    t.integer  "gifter_id"
    t.datetime "gift_time"
    t.string   "gifter_name"
    t.index ["business_id"], name: "index_cart_items_on_business_id", using: :btree
    t.index ["cart_id"], name: "index_cart_items_on_cart_id", using: :btree
    t.index ["discount_id"], name: "index_cart_items_on_discount_id", using: :btree
    t.index ["gifter_id"], name: "index_cart_items_on_gifter_id", using: :btree
    t.index ["sales_purchase_number"], name: "index_cart_items_on_sales_purchase_number", using: :btree
    t.index ["seller_id"], name: "index_cart_items_on_seller_id", using: :btree
  end

  create_table "carts", force: :cascade do |t|
    t.decimal  "sub_total",        precision: 10, scale: 2, default: "0.0"
    t.decimal  "taxes",            precision: 10, scale: 2, default: "0.0"
    t.decimal  "shipping",         precision: 10, scale: 2, default: "0.0"
    t.decimal  "total_price",      precision: 10, scale: 2, default: "0.0"
    t.integer  "user_id"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.boolean  "paid",                                      default: false
    t.datetime "completed_at"
    t.decimal  "application_fees", precision: 10, scale: 2, default: "0.0"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "tags"
    t.string   "class_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["class_name"], name: "index_categories_on_class_name", using: :btree
    t.index ["name"], name: "index_categories_on_name", using: :btree
  end

  create_table "categories_discounts", force: :cascade do |t|
    t.integer "category_id"
    t.integer "discount_id"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type", using: :btree
  end

  create_table "countries", force: :cascade do |t|
    t.string "shortname"
    t.string "name"
  end

  create_table "discounts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price",              precision: 15, scale: 2,  default: "0.0"
    t.decimal  "discounted_price",   precision: 15, scale: 2,  default: "0.0"
    t.string   "slug"
    t.string   "discount_type"
    t.boolean  "family_friendly",                              default: false
    t.string   "image"
    t.integer  "business_id"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.decimal  "latitude",           precision: 30, scale: 25
    t.decimal  "longitude",          precision: 30, scale: 25
    t.boolean  "active",                                       default: true
    t.string   "affiliate_code"
    t.string   "limit",                                        default: "unlimited"
    t.string   "limit_per_customer",                           default: "unlimited"
    t.decimal  "extra_price",        precision: 10, scale: 2,  default: "0.0"
    t.text     "terms"
    t.date     "expiry_date"
    t.boolean  "sold_out",                                     default: false
    t.index ["active"], name: "index_discounts_on_active", using: :btree
    t.index ["affiliate_code"], name: "index_discounts_on_affiliate_code", using: :btree
    t.index ["business_id"], name: "index_discounts_on_business_id", using: :btree
    t.index ["discount_type"], name: "index_discounts_on_discount_type", using: :btree
    t.index ["family_friendly"], name: "index_discounts_on_family_friendly", using: :btree
    t.index ["limit"], name: "index_discounts_on_limit", using: :btree
    t.index ["limit_per_customer"], name: "index_discounts_on_limit_per_customer", using: :btree
    t.index ["slug"], name: "index_discounts_on_slug", using: :btree
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "discount_id"
    t.index ["discount_id"], name: "index_likes_on_discount_id", using: :btree
    t.index ["user_id"], name: "index_likes_on_user_id", using: :btree
  end

  create_table "merchant_accounts", force: :cascade do |t|
    t.integer "profile_id"
    t.integer "user_id"
    t.string  "stripe_publishable_key"
    t.string  "access_token"
    t.string  "scope"
    t.string  "refresh_token"
    t.boolean "livemode"
    t.boolean "active"
    t.string  "stripe_user_id"
    t.string  "account_type"
    t.string  "token_type"
    t.decimal "total_revenue_cents",    precision: 10, scale: 2, default: "0.0"
    t.index ["access_token"], name: "index_merchant_accounts_on_access_token", using: :btree
    t.index ["profile_id"], name: "index_merchant_accounts_on_profile_id", using: :btree
    t.index ["stripe_publishable_key"], name: "index_merchant_accounts_on_stripe_publishable_key", using: :btree
    t.index ["stripe_user_id"], name: "index_merchant_accounts_on_stripe_user_id", using: :btree
  end

  create_table "payout_requests", force: :cascade do |t|
    t.integer  "requester_id"
    t.string   "requester_type"
    t.decimal  "request_amount", precision: 10, scale: 2, default: "0.0"
    t.datetime "request_date"
    t.index ["requester_id"], name: "index_payout_requests_on_requester_id", using: :btree
    t.index ["requester_type"], name: "index_payout_requests_on_requester_type", using: :btree
  end

  create_table "payout_thresholds", force: :cascade do |t|
    t.decimal  "amount",     precision: 10, scale: 2, default: "0.0"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "payouts", force: :cascade do |t|
    t.integer  "cart_id"
    t.integer  "cart_item_id"
    t.decimal  "price",                         precision: 10, scale: 2, default: "0.0"
    t.decimal  "business_percentage",           precision: 10, scale: 2, default: "0.0"
    t.decimal  "payment_gateway_percentage",    precision: 10, scale: 2, default: "0.0"
    t.decimal  "marketpad_percentage",          precision: 10, scale: 2, default: "0.0"
    t.decimal  "business_affiliate_percentage", precision: 10, scale: 2, default: "0.0"
    t.decimal  "discount_affiliate_percentage", precision: 10, scale: 2, default: "0.0"
    t.decimal  "marketpad_pool_percentage",     precision: 10, scale: 2, default: "0.0"
    t.decimal  "seller_percentage",             precision: 10, scale: 2, default: "0.0"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.index ["cart_id"], name: "index_payouts_on_cart_id", using: :btree
    t.index ["cart_item_id"], name: "index_payouts_on_cart_item_id", using: :btree
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.string   "searchable_type"
    t.integer  "searchable_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "category_type"
    t.index ["category_type"], name: "index_pg_search_documents_on_category_type", using: :btree
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.string   "website"
    t.string   "phone_number"
    t.string   "fb_link"
    t.string   "twitter_link"
    t.string   "google_link"
    t.string   "image"
    t.string   "cover_image"
    t.boolean  "family_friendly",                           default: false
    t.string   "profile_type"
    t.string   "slug"
    t.decimal  "latitude",        precision: 30, scale: 25
    t.decimal  "longitude",       precision: 30, scale: 25
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "affiliate_code"
    t.string   "old_mongo_id"
    t.index ["affiliate_code"], name: "index_profiles_on_affiliate_code", using: :btree
    t.index ["family_friendly"], name: "index_profiles_on_family_friendly", using: :btree
    t.index ["name"], name: "index_profiles_on_name", using: :btree
    t.index ["profile_type"], name: "index_profiles_on_profile_type", using: :btree
    t.index ["slug"], name: "index_profiles_on_slug", using: :btree
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "shipping_addresses", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "street_1"
    t.string   "street_2"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zipcode"
    t.string   "phone_number"
    t.integer  "cart_id"
    t.integer  "cart_item_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "slides", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.string   "headline"
    t.string   "description"
    t.string   "button"
    t.integer  "style"
    t.string   "target"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "states", force: :cascade do |t|
    t.integer "country_id"
    t.string  "name"
    t.index ["country_id"], name: "index_states_on_country_id", using: :btree
  end

  create_table "subscribers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_subscribers_on_email", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                            default: "",    null: false
    t.string   "encrypted_password",                               default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                    default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.string   "user_type"
    t.string   "dob"
    t.string   "gender"
    t.string   "image"
    t.string   "provider"
    t.string   "uid"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "affiliate_code"
    t.string   "old_mongo_id"
    t.string   "zipcode"
    t.string   "city"
    t.string   "state"
    t.string   "search_radius"
    t.decimal  "latitude",               precision: 30, scale: 25
    t.decimal  "longitude",              precision: 30, scale: 25
    t.boolean  "remember_loc",                                     default: false
    t.string   "country"
    t.index ["affiliate_code"], name: "index_users_on_affiliate_code", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["old_mongo_id"], name: "index_users_on_old_mongo_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
