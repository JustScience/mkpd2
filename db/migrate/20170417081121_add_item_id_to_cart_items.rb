class AddItemIdToCartItems < ActiveRecord::Migration[5.0]
  def change
  	add_column :cart_items, :sales_purchase_number, :string
  	add_index :cart_items, :sales_purchase_number
  	add_column :cart_items, :redeemed, :boolean, default: false ##use for deal type discount
  	add_column :cart_items, :redeemed_by, :integer
  	add_column :cart_items, :redeemed_time, :datetime
  end
end
