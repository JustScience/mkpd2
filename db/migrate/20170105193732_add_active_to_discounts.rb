class AddActiveToDiscounts < ActiveRecord::Migration[5.0]
  def change
  	add_column :discounts, :active, :boolean, default: true
  	add_index :discounts, :active
  end
end
