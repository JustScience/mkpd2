class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
    	t.string :shortname
    	t.string :name
    end

     create_table :states do |t|
    	   t.integer :country_id
    	   t.string :name
    end

    add_index :states, :country_id

  end
end
