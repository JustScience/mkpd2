class CreateAffiliateEarnings < ActiveRecord::Migration[5.0]
  def change
    create_table :affiliate_earnings do |t|
    	t.integer :profile_id
    	t.string :profile_type
    	t.decimal :total_earnings_till_date,  precision: 10, scale: 2, default: 0.0
      t.decimal :earnings_after_last_payout,  precision: 10, scale: 2, default: 0.0
    	t.timestamps
    end

    add_index :affiliate_earnings, :profile_type
    add_index :affiliate_earnings, :profile_id

    Profile.all.each do |profile|
      if AFFILIATES_ARRAY.include?(profile.profile_type)
       AffiliateEarning.create(profile_type: profile.profile_type, profile_id: profile.id)
     end
    end

  end
end
