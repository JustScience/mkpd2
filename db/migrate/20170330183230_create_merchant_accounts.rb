class CreateMerchantAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :merchant_accounts do |t|
    	t.integer :profile_id
    	t.integer :user_id
    	t.string :stripe_publishable_key
    	t.string :access_token  ##secret key
    	t.string :scope
    	t.string :refresh_token
    	t.boolean :livemode
    	t.boolean :active
    	t.string :stripe_user_id
    	t.string :account_type
    	t.string :token_type
    	t.decimal :total_revenue_cents, precision: 10, scale: 2, default: 0.0
    end

    add_index :merchant_accounts, :stripe_publishable_key
    add_index :merchant_accounts, :profile_id
    add_index :merchant_accounts, :access_token
    add_index :merchant_accounts, :stripe_user_id
  end
end
