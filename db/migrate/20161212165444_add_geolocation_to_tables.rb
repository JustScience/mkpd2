class AddGeolocationToTables < ActiveRecord::Migration[5.0]
  def change
  	add_column :profiles, :latitude, :decimal, precision: 30, scale: 25
  	add_column :profiles, :longitude, :decimal, precision: 30, scale: 25
  	add_column :profiles, :country, :string
  	add_column :discounts, :latitude, :decimal, precision: 30, scale: 25
  	add_column :discounts, :longitude, :decimal, precision: 30, scale: 25
  end
end
