class CreateCart < ActiveRecord::Migration[5.0]
  def up
    create_table :carts do |t|
    	t.decimal  :sub_total, precision: 10, scale: 2, default: 0.0
	    t.decimal  :taxes, precision: 10, scale: 2, default: 0.0
	    t.decimal  :shipping, precision: 10, scale: 2, default: 0.0
	    t.decimal  :total_price, precision: 10, scale: 2, default: 0.0
	    t.integer  :user_id
    	t.timestamps
    end

    create_table :cart_items do |t|
	    t.integer  :cart_id
	    t.integer  :business_id
	    t.integer  :discount_id
	    t.integer  :seller_id
	    t.string   :sold_by
	    t.decimal  :price, precision: 10, scale: 2, default: 0.0
	    t.decimal  :taxes, precision: 10, scale: 2, default: 0.0
	    t.decimal  :total_price, precision: 10, scale: 2, default: 0.0
	    t.decimal  :shipping, precision: 10, scale: 2, default: 0.0
	    t.string   :currency,   default: "USD"
	    t.timestamps
	  end

	  add_index :cart_items, :cart_id
	  add_index :cart_items, :business_id
	  add_index :cart_items, :discount_id
	  add_index :cart_items, :seller_id

	  create_table :payouts do |t|
	  	t.integer :cart_id
	  	t.integer :cart_item_id
	  	t.decimal :price, precision: 10, scale: 2, default: 0.0
	  	t.decimal :business_percentage, precision: 10, scale: 2, default: 0.0  ##90%
	  	t.decimal :payment_gateway_percentage, precision: 10, scale: 2, default: 0.0 #4%
	  	t.decimal :marketpad_percentage, precision: 10, scale: 2, default: 0.0 #2%
	  	t.decimal :business_affiliate_percentage, precision: 10, scale: 2, default: 0.0 #.5%
	  	t.decimal :discount_affiliate_percentage, precision: 10, scale: 2, default: 0.0 #1%
	  	t.decimal :marketpad_pool_percentage, precision: 10, scale: 2, default: 0.0 #.5%
	  	t.decimal :seller_percentage, precision: 10, scale: 2, default: 0.0 #2%
	  	t.timestamps
	  end

	  add_index :payouts, :cart_id
	  add_index :payouts, :cart_item_id

  end

  def down
  	drop_table :carts
  	drop_table :cart_items
  	drop_table :payouts
  end

end
