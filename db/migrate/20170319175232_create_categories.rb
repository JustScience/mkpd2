class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
    	t.string :name
    	t.text :tags
    	t.string :class_name
    	t.timestamps
    end

    add_index :categories, :name
    #add_index :categories, :tags
    add_index :categories, :class_name
  end
end
