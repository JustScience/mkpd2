class AddPaidToCarts < ActiveRecord::Migration[5.0]
  def change
  	add_column :carts, :paid, :boolean, default: false
  	add_column :carts, :completed_at, :datetime
  end
end
