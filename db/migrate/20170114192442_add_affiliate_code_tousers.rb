class AddAffiliateCodeTousers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :affiliate_code, :string
  	add_column :profiles, :affiliate_code, :string
  	add_column :discounts, :affiliate_code, :string

  	add_index :users, :affiliate_code
  	add_index :profiles, :affiliate_code
  	add_index :discounts, :affiliate_code
  end
end
