class AddApplicationFeesToCarts < ActiveRecord::Migration[5.0]
  def change
  	add_column :carts, :application_fees, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
