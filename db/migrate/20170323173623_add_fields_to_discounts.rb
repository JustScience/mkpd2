class AddFieldsToDiscounts < ActiveRecord::Migration[5.0]
  def up
  	add_column :discounts, :limit, :integer, default: 1
  	add_column :discounts, :limit_per_customer, :integer, default: 1
  	add_column :discounts, :extra_price, :decimal, precision: 10, scale: 2, default: 0.0 ##for products only
    
    add_index :discounts, :limit
    add_index :discounts, :limit_per_customer

    create_table :categories_discounts do |t|
    	t.integer :category_id
    	t.integer :discount_id
    end

  end

  def down
  	remove_column :discounts, :limit
  	remove_column :discounts, :limit_per_customer
  	remove_column :discounts, :extra_price 
    drop_table :categories_discounts

  end
end
