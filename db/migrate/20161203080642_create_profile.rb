class CreateProfile < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
    	t.integer :user_id
    	t.string :name
    	t.text :description
    	t.string :street
    	t.string :city
    	t.string :state
    	t.string :zipcode
    	t.string :website
    	t.string :phone_number
    	t.string :fb_link
    	t.string :twitter_link
    	t.string :google_link
    	t.string :image
    	t.string :cover_image
    	t.boolean :family_friendly, default: false
    	t.string :profile_type
        t.string :slug
    end

    add_index :profiles, :name
    add_index :profiles, :family_friendly
    add_index :profiles, :user_id
    add_index :profiles, :profile_type
    add_index :profiles, :slug
  end
end
