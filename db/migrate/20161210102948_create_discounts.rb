class CreateDiscounts < ActiveRecord::Migration[5.0]
  def up
    drop_table :discounts, if_exists: true
    create_table :discounts do |t|
    	t.string :name
    	t.text :description
    	t.decimal :price, precision: 15, scale: 2, default: 0.0
    	t.decimal :discounted_price, precision: 15, scale: 2, default: 0.0
    	t.string :slug
    	t.string :discount_type
    	t.boolean :family_friendly, default: false
    	t.string :image
      t.integer :business_id
    	t.timestamps
    end

    add_index :discounts, :business_id
    add_index :discounts, :discount_type
    add_index :discounts, :slug
    add_index :discounts, :family_friendly
  end

  def down
    drop_table :discounts
  end
end
