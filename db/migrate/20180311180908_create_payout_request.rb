class CreatePayoutRequest < ActiveRecord::Migration[5.0]
  def change
    create_table :payout_requests do |t|
    	t.integer :requester_id
    	t.string :requester_type
    	t.decimal :request_amount,  precision: 10, scale: 2, default: 0.0
    	t.datetime :request_date
    end
    add_index :payout_requests, :requester_id
    add_index :payout_requests, :requester_type
  end
end
