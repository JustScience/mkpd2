class AddFieldsToDiscount < ActiveRecord::Migration[5.0]
  def change
  	add_column :discounts, :expiry_date, :date
  	add_column :discounts, :sold_out, :boolean, default: false
  	change_column :discounts, :limit, :string, default: 'unlimited'
  	change_column :discounts, :limit_per_customer, :string, default: 'unlimited'
  end
end
