class CreateSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :slides do |t|
      t.string :name
      t.string :image
      t.string :headline
      t.string :description
      t.string :button
      t.integer :style
      t.string :target

      t.timestamps
    end
  end
end
