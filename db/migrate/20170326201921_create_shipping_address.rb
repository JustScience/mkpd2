class CreateShippingAddress < ActiveRecord::Migration[5.0]
  def change
    create_table :shipping_addresses do |t|
    	t.string :first_name
    	t.string :last_name
    	t.string :street_1
    	t.string :street_2
    	t.string :city
    	t.string :state
    	t.string :country
    	t.string :zipcode
    	t.string :phone_number
    	t.integer :cart_id
    	t.integer :cart_item_id
    	t.timestamps
    end
  end
end
