class AddOldMongoIdToProfiles < ActiveRecord::Migration[5.0]
  def change
  	add_column :profiles, :old_mongo_id, :string
  end
end
