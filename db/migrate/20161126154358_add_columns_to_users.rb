class AddColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :dob, :string
    add_column :users, :gender, :string
    add_column :users, :image, :string
    add_column :users, :provider, :string ##storing facebook / Google
    add_column :users, :uid, :string ##storing facebooking user id
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
  end
end
