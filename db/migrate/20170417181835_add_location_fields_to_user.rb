class AddLocationFieldsToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :zipcode, :string
  	add_column :users, :city, :string
  	add_column :users, :state, :string
  	add_column :users, :search_radius, :string
  	add_column :users, :latitude, :decimal, precision: 30, scale: 25
  	add_column :users, :longitude, :decimal, precision: 30, scale: 25
  	add_column :users, :remember_loc, :boolean, default: false
  end
end
