class AddFieldsToCartItem < ActiveRecord::Migration[5.0]
  def change
  	add_column :cart_items, :gift, :boolean, default: false
  	add_column :cart_items, :giftee_email, :string
  	add_column :cart_items, :gifter_id, :integer
  	add_column :cart_items, :gift_time, :datetime
  	add_column :cart_items, :gifter_name, :string

  	#add_index :cart_items, :giftee_email
  	add_index :cart_items, :gifter_id
  end
end
