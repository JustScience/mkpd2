class CreateLikes < ActiveRecord::Migration[5.0]
  def change
    create_table :likes do |t|
    	t.integer :user_id
    	t.integer :discount_id
    end

    add_index :likes, :user_id
    add_index :likes, :discount_id
  end
end
