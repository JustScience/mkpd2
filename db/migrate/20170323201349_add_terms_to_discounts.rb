class AddTermsToDiscounts < ActiveRecord::Migration[5.0]
  def change
  	add_column :discounts, :terms, :text
  end
end
