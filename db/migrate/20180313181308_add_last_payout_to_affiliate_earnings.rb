class AddLastPayoutToAffiliateEarnings < ActiveRecord::Migration[5.0]
  def change
  	add_column :affiliate_earnings,  :last_payout, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
