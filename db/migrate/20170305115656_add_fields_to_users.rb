class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :admin_users, :first_name, :string
  	add_column :admin_users, :last_name, :string

  	add_column :users, :old_mongo_id, :string
  	add_index :users, :old_mongo_id
  end
end
