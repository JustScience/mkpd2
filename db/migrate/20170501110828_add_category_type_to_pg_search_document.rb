class AddCategoryTypeToPgSearchDocument < ActiveRecord::Migration[5.0]
  def up
  	add_column :pg_search_documents, :category_type, :string
  	add_index :pg_search_documents, :category_type
  end

  def down
  	remove_column :pg_search_documents, :category_type
  end

end
