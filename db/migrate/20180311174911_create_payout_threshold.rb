class CreatePayoutThreshold < ActiveRecord::Migration[5.0]
  def change
    create_table :payout_thresholds do |t|
    	t.decimal :amount,  precision: 10, scale: 2, default: 0.0
    	t.timestamps
    end
    PayoutThreshold.create(amount: 0.0)
  end
end
