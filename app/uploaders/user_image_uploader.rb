# encoding: utf-8
class UserImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick
  storage :fog
  
  version :medium do
    process resize_to_fill: [200, 200]
  end

   version :thumb, from_version: :medium do
    process resize_to_fill: [80, 80]
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [50, 50]
  end
  

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this: 
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  #file name is missing extension!!!
  def filename
    original_filename if original_filename.present?
  end


  protected
  def secure_token(length=16)
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.hex(length/2))
  end

end