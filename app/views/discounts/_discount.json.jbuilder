json.extract! discount, :id, :name, :description, :meta_group, :old_price, :new_price, :image, :family_friendly, :created_at, :updated_at
json.url discount_url(discount, format: :json)