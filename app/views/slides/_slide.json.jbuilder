json.extract! slide, :id, :name, :image, :headline, :description, :button, :style, :target, :created_at, :updated_at
json.url slide_url(slide, format: :json)