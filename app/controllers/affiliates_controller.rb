class AffiliatesController < ApplicationController

	before_action :authenticate_user!
	before_action :check_affiliate_access
  

  def index
  	@affiliate_businesses = current_user.affiliate_businesses
  	@affiliate_discounts = current_user.affiliate_discounts
  	add_breadcrumb "Home", :root_path, data: { turbolinks: false }
  	add_breadcrumb "Affiliations" ##without link
  end
  
  ##create new business profile by affiliate user
	def create_business
		add_breadcrumb "Home", :root_path, data: { turbolinks: false }
  	add_breadcrumb "Affiliations", :affiliations_path
  	add_breadcrumb "Add New Business" ##without link
		@business = Profile.new
		if request.post?

			user_params = params[:user]
			if user_params.present?
			  user = User.where(email: user_params[:email]).first

			  if user.present?
			  	flash[:alert] = "Business user email has already exists in our system."
			  	return render action: :create_business
			  else
			  	##business user
			  	user = User.new
			  	user.email = user_params[:email]
			  	user.user_type = "business"
			  	user.password = user_params[:password]
			  	if user.valid_password?(user_params[:password])
			  	else
			  		flash[:alert] = "Email or Password is invalid, password should be atleast 8 characters."
            return render action: :create_business
			  	end
			  end

			else
				flash[:alert] = "Business User details can't be blank."
				return render action: :create_business
			end

			@business.attributes = profile_params
	    @business.profile_type = "business"
	    @business.affiliate_code = current_user.affiliate_code
	    if @business.save
	    	user.save
	    	@business.update_column(:user_id, user.id)
	      redirect_to affiliations_path, notice: "Business Profile created successfully!!"
	    else
	    	flash[:alert] = "Please fill all fields."
	      render action: :create_business
	    end
		end
	end

	##update business profile by affiliate user
	def update_business
		add_breadcrumb "Home", :root_path, data: { turbolinks: false }
  	add_breadcrumb "Affiliations", :affiliations_path
  	add_breadcrumb "Edit Business" ##without link
		@business = Profile.where(profile_type: 'business', slug: params[:slug]).first
		return redirect_to affiliations_path, alert: "You are not authorized to access this page." if @business.blank?
		return redirect_to root_path, alert: "You are not authorized to access this page." unless @business.affiliated_by?(current_user.affiliate_code)
		if request.put?
			@business.attributes = profile_params
	    if @business.save
	      redirect_to affiliations_path, notice: "Business Profile updated successfully!!"
	    else
	    	lash[:alert] = "Please fill all fields."
	      render action: :update_business
	    end
		end
	end

	def create_discount
		add_breadcrumb "Home", :root_path
  	add_breadcrumb "Affiliations", :affiliations_path
  	add_breadcrumb "Add New Discount" ##without link
  	@affiliate_businesses = current_user.affiliate_businesses.pluck(:name, :id)
		@discount = Discount.new
		if request.post?
			@discount = Discount.new(discount_params)
			business = Profile.where(id: params[:discount][:business_id]).first
			if business.present?
				@discount.latitude = business.latitude
				@discount.longitude = business.longitude
			end
			@discount.affiliate_code = current_user.affiliate_code

			if @discount.save
				redirect_to affiliations_path, notice: "Discount Info created successfully!!"
			else
				flash[:alert] = "Discount Info can't be blank."
				return render action: :create_discount
			end
		end
	end

	def update_discount
		add_breadcrumb "Home", :root_path, data: { turbolinks: false }
  	add_breadcrumb "Affiliations", :affiliations_path
  	add_breadcrumb "Edit Discount" ##without link
  	@affiliate_businesses = current_user.affiliate_businesses.pluck(:name, :id)
		@discount = Discount.where(slug: params[:slug]).first
		return redirect_to affiliations_path, alert: "You are not authorized to access this page." if @discount.blank?
		return redirect_to root_path, alert: "You are not authorized to access this page." unless @discount.affiliated_by?(current_user.affiliate_code)
		if request.put?
			@discount.attributes = discount_params
			business = Profile.where(id: params[:discount][:business_id]).first
			if business.present?
				@discount.latitude = business.latitude
				@discount.longitude = business.longitude
			end

			if @discount.save
				redirect_to affiliations_path, notice: "Discount Info updated successfully!!"
			else
				flash[:alert] = "Discount Info can't be blank."
				return render action: :create_discount
			end
		end
	end


	#####################
  ## Protected Methods
  #####################
  
  protected

  ###################
  ## Private Methods
  ###################

  private

    def discount_params
      params.require(:discount).permit(:name, :description, :discount_type, :price, :discounted_price, :image, :family_friendly, :latitude, :longitude, :active, :affiliate_code, :business_id, :limit, :limit_per_customer, :extra_price, :terms, {category_ids: []})
    end

    def profile_params
      params.require(:profile).permit(:name, :description, :street, :city, :state, :zipcode, :website, :fb_link,
        :twitter_link, :google_link, :image, :cover_image, :family_friendly, :phone_number, :country, :affiliate_code)
    end

	  def check_affiliate_access
	  	return redirect_to root_path, alert: "You are not authorized to access this page." unless current_user.affiliate?
	  end 
end