class SlidesController < InheritedResources::Base

  private

    def slide_params
      params.require(:slide).permit(:name, :image, :headline, :description, :button, :style, :target)
    end
end

