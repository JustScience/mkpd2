class DiscountsController < ApplicationController

  before_action :authenticate_user!, except: [:show, :like]
  before_action :set_discount, only: [:show, :edit, :update, :destroy, :like]
  before_action :check_busniess_access, only: [:edit, :new, :create, :update, :destroy]
  before_action :set_business, only: [:show, :edit, :update, :new, :create, :update, :index]
 
  ###########
  ## Filters
  ###########
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  def index
    return redirect_to root_path unless current_user.business?
    return redirect_to root_path, alert: "You must have an active business profile to view this section." if @business.blank?
    @discounts = @business.discounts.includes(:likes)
    make_breadcrumbs  
  end

  def show
    return redirect_to root_path, alert: "Either discount does not exists or is inactive." unless @discount.active
    make_breadcrumbs("show")##call breadcrumbs
    ##set page SEO
    set_page_seo
    
    if Rails.env.development? || Rails.env.staging?
      @merchant_account = MerchantAccount.where(livemode: false, profile_id: @discount.business_id).first.present?
    else
      @merchant_account = MerchantAccount.where(livemode: true, profile_id: @discount.business_id).first.present?
    end

  end

  def new
    return redirect_to discounts_path, alert: "You have to link your stripe merchant account with MarketPad before creating discounts." if @business.merchant_accounts.blank?
    @discount = Discount.new
    make_breadcrumbs("new")##call breadcrumbs
    @existing_affiliates = get_existing_affiliates
  end

  def edit
    return redirect_to root_path unless @discount.owned_by?(current_user)
    make_breadcrumbs("edit")##call breadcrumbs
    @existing_affiliates = get_existing_affiliates
  end

  def create
    @existing_affiliates = get_existing_affiliates
    make_breadcrumbs("new")##call breadcrumbs
    @discount = Discount.new(discount_params)
    @discount.business_id = current_user.profile.id
    respond_to do |format|
      if @discount.save
        format.html { redirect_to @discount, notice: 'Discount was successfully created.' }
        format.json { render :show, status: :created, location: @discount }
      else
        format.html { render :new }
        format.json { render json: @discount.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @existing_affiliates = get_existing_affiliates
    make_breadcrumbs("edit")##call breadcrumbs
    respond_to do |format|
      if @discount.update(discount_params)
        format.html { redirect_to discounts_path, notice: 'Discount was successfully updated.' }
        format.json { render :show, status: :ok, location: @discount }
      else
        format.html { render :edit }
        format.json { render json: @discount.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @discount.destroy
    respond_to do |format|
      format.html { redirect_to discounts_url, notice: 'Discount was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  ##handle discount like / unlike request
  def like
    if current_user.blank?
      session[:return_url] = request.referer
      render js: "window.location = '#{login_path}'"
    else
      liked = current_user.likes.where(discount_id: @discount.id).first
      if liked.present? ##already liked it, remove from like table
        liked.delete
        @msg = "Removed from Favorites!!"
        @status = "unliked"
      else ## like
        Like.create(discount_id: @discount.id, user_id: current_user.id)
        @msg = "Added to Favorites!!"
        @status = "liked"
      end
    end
  end

  #####################
  ## Protected Methods
  #####################
  
  protected

  ###################
  ## Private Methods
  ###################

  private

    def set_page_seo
      @page_title = "#{@discount.name}"
    end
    
    ##create breadcrumbs here
    def make_breadcrumbs(page_type = nil)
      add_breadcrumb "Home", :root_path, data: { turbolinks: false }
      add_breadcrumb @business.name, "#{show_account_path(@business.profile_type, @business.slug) }" if @business.present?
      add_breadcrumb "My Discounts", discounts_path if current_user.present? && current_user.business? ##show this link only to business accounts
      add_breadcrumb "New Discount" if page_type == "new" ##without link 
      add_breadcrumb "Edit Discount" if page_type == "edit" ##without link
      add_breadcrumb @discount.name if page_type == "show" ##without link 

    end

    def set_discount
      @discount = Discount.where(slug: params[:id]).first
      return redirect_to root_path if @discount.blank?
    end

    def check_busniess_access
      return redirect_to root_path, alert: "You are not authorized to access this page." unless current_user.business?
      return redirect_to root_path, alert: "You must have an active business profile to create a discount." if current_user.profile.blank?
    end

    def set_business
      @business = @discount.present? ? @discount.business : current_user.profile
    end

    def discount_params
      params.require(:discount).permit(:name, :description, :discount_type, :price, :discounted_price, :image, :family_friendly, :latitude, :longitude, :active, :affiliate_code, :limit, :limit_per_customer, :extra_price, :terms, {category_ids:  []})
    end
    
    #return existing affiliates on new / edit discount form
    def get_existing_affiliates
      existing_affiliats = []
      existing_affiliats << @business.try(:affiliate_code)
      existing_affiliats << @business.discounts.where("affiliate_code is not null").select(:affiliate_code).collect(&:affiliate_code)
      existing_affiliats.flatten.compact.uniq
    end

end
