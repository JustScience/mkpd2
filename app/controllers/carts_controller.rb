class CartsController < ApplicationController

	##########
  ## Filters
  ###########
  before_action :authenticate_user!, except: [:index, :add_to_cart, :remove_from_cart, :checkout]
  before_action :set_discount, :set_seller, only: [:add_to_cart]
  before_action :set_cart_item, only: [:remove_from_cart]
  before_action :set_cart, only: [:add_to_cart, :remove_from_cart]
   
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################
  
  ##render carts page with cart items
  def index
    if session[:user_cart].present?
      cart_ids = []
      session[:user_cart].each {|uc| cart_ids << uc.values[1]}
    end
    @carts = Cart.where(id: cart_ids).includes(:cart_items, cart_items: [:discount, :business, :seller] ).order("created_at asc")
    #@cart_items = @cart.cart_items.includes(:discount, :business, :seller).order("created_at asc") if @cart.present?
  end

  def add_to_cart
    return redirect_to discount_path(@discount), alert: "Sorry!! this discount is marked as sold out. Please add other discounts in your cart." if @discount.sold_out? 
    item = @cart.cart_items.build(discount_id: @discount.id, business_id: @discount.business.id,
      sold_by: @sold_by, seller_id: @seller_id, price: @discount.discounted_price.to_f,
      total_price: @discount.discounted_price.to_f, taxes: 0.0, shipping: 0.0, sales_purchase_number: generate_item_id)
    
    @cart.sub_total = @cart.sub_total + @discount.discounted_price
    @cart.taxes = 0.0
    @cart.shipping = 0.0
    @cart.total_price = (@cart.sub_total + @cart.taxes + @cart.shipping)
    @cart.application_fees = @cart.application_fees + calculate_application_fees(item)

    if @cart.save
      redirect_to carts_path, notice: "Item added successfully!!"
    else
      redirect_to request.referer, alert: "Item could not be added in cart. Please try again."
    end
  end

  def remove_from_cart
    item = @cart_item
  	if @cart_item && @cart_item.destroy
      @cart.application_fees = @cart.application_fees - calculate_application_fees(item)
      recalculate_cart_prices(@cart.cart_items)
  	  redirect_to carts_path, notice: "Item removed from cart successfully!!"
  	else
  		redirect_to carts_path, alert: "Item could not be removed from cart. Please try again."
  	end
  end

  def checkout
    if current_user.blank?
      session[:return_url] = request.referer
      return redirect_to login_path
    else
      @cart = Cart.where(id: params[:cartID]).first
      return redirect_to carts_path if @cart.blank?
      @cart.update_column(:user_id, current_user.id) if @cart.user_id.blank? ##update user_id
      @cart_items = @cart.cart_items.includes(:discount, :business, :seller).order("created_at asc")
      
      ##check if cart changed (any item is sold out that exist in current cart)
      cart_has_changed = cart_changed
      if cart_has_changed
        flash[:alert] = "Sorry!! some of the discount are marked as sold out and removed from your cart. Please review your cart and add more discounts."
        return redirect_to carts_path
      end

      customer_discount_purchase_limit = check_customer_discount_purchase_limit
      if customer_discount_purchase_limit
        flash[:alert] = "Sorry!! some of the discount purchase maximum limit is reached and removed from your cart. Please review your cart and add more discounts."
        return redirect_to carts_path
      end

      return redirect_to carts_path, alert: "Cart is empty. Add some deals to your cart!" if @cart_items.blank?
    end
  end

  def purchase
    @cart = Cart.where(id: params[:cartID]).first
    return redirect_to carts_path if @cart.blank?
    @cart_items = @cart.cart_items.includes(:discount, :business)
    business = @cart_items.first.business

    # ##check if cart changed (any item is sold out that exist in current cart)
    # cart_has_changed = cart_changed
    # if cart_has_changed
    #   flash[:alert] = "Sorry!! some of the discounts are marked as sold out and removed from your cart. Please review your cart and add more discounts."
    #   return redirect_to carts_path
    # end

    # customer_discount_purchase_limit = check_customer_discount_purchase_limit
    # if customer_discount_purchase_limit
    #   flash[:alert] = "Sorry!! some of the discount purchase maximum limit is reached and removed from your cart. Please review your cart and add more discounts."
    #   return redirect_to carts_path
    # end
    
    begin

      merchant_accounts = business.merchant_accounts
      if merchant_accounts.present?
        if Rails.env.development? || Rails.env.staging?
          merchant_account = merchant_accounts.where(livemode: false).first
        else
          merchant_account = merchant_accounts.where(livemode: true).first
        end
      else
        return redirect_to carts_path, alert: "Sorry!! Payment to this merchant is not alloweds. Please add discounts / products from other merchant."
      end

      # Amount in cents
      amount_in_cents = (@cart.total_price.to_f * 100).to_i
      application_fee = (@cart.application_fees.to_f * 100).to_i

      #customer = Stripe::Customer.create(description: current_user.email, email: current_user.email, source: params[:stripeToken])
      
      ##get application_fee should be 6% (seller + marketpad + pool + affiliate)
      
      charge = Stripe::Charge.create({source: params[:stripeToken], amount: amount_in_cents, 
        description: "MarketPad purchased, purchase number: #{@cart.purchase_number}", 
        currency: 'usd', application_fee: application_fee}, 
        stripe_account: merchant_account.stripe_user_id
      )
      
      Rails.logger.info "Cart Response for #{@cart.purchase_number}::  #{charge} and #{charge.status}"
      ##update cart paid as true
      if charge.status == "succeeded" || charge.status == "paid"

        @cart.update(paid: true, completed_at: Time.now)
        
        ship_addr = nil
        ship_addr = ShippingAddress.create(ship_add_params) if params[:ship_add].present?

        ##remove hash from session
        session[:user_cart].delete_if {|uc| uc["#{business.id}_business_id"] == business.id || uc["#{business.id}_cart_id"] == @cart.id }
       
        ##check and mark discount as sold out and set expiry date
        ##default expiry date Date of sold out + 10 days
        if Rails.env.production?
          @cart.delay.check_discounts_sold_out
          ##calculate sales payouts
          @cart.delay.calculate_payout
          ##send email to business of product sales
          @cart.delay.send_products_sale_to_business if @cart.product_exist_in_cart?         
          ##later we move this email to delay
          NotificationMailer.delay.customer_purchase_receipt(@cart, current_user, ship_addr)
        else
          @cart.check_discounts_sold_out
          @cart.calculate_payout
          @cart.send_products_sale_to_business if @cart.product_exist_in_cart?         
          NotificationMailer.customer_purchase_receipt(@cart, current_user, ship_addr).deliver_now
        end
      
        session[:user_cart] = nil if session[:user_cart].size == 0
    
        redirect_to invoice_path(@cart.purchase_number), notice: "Thank You for your order with Marketpad!! An order receipt has been sent on your email."

      else
        redirect_to carts_path, alert: "Error while attempting payment. Please try again later."
      end

    rescue Stripe::StripeError => e
      flash[:alert] = e.message
      redirect_to carts_path
    end

  end

  def invoice
    cart_id = params[:purchase_number].sub!(/^[0]+/,'') if params[:purchase_number].present?
    @cart = Cart.where(id: cart_id).first
    return redirect_to root_path if @cart.blank? || @cart.unpaid?
    return redirect_to root_path if @cart.customer.try(:id) != current_user.id
    @cart_items = @cart.cart_items
  end
  
  ##check if any of item(discount) sold out then remove that item from cart
  def cart_changed
    cart_has_changed = false
    @cart_items.each do |item|
      if item.discount.sold_out?
        @cart.application_fees = @cart.application_fees - calculate_application_fees(item)
        item.destroy
        recalculate_cart_prices(@cart.cart_items)
        cart_has_changed = true
      end
    end
    
   cart_has_changed
  end

  ##check if customer has reached to maximum limit for discount
  def check_customer_discount_purchase_limit
    customer_limit_reached = false
    @cart_items.each do |item|
      if item.discount.cusomter_limit_reached_for_discount?(current_user.id)
        @cart.application_fees = @cart.application_fees - calculate_application_fees(item)
        item.destroy
        recalculate_cart_prices(@cart.cart_items)
        customer_limit_reached = true
      end
    end
    
   customer_limit_reached
  end

  #####################
  ## Protected Methods
  #####################
  protected

  ###################
  ## Private Methods
  ###################
  private
    
    ##calculate application fess for each item
    def calculate_application_fees(item)
      if item.product? && item.product_extra_price_present?
        extra_price = item.discount.extra_price
        ((item.total_price * 6) / 100).to_f + extra_price
      else
        ((item.total_price * 6) / 100).to_f
      end
    end
    
    ##generate unique cart item ID
    def generate_item_id
      [SecureRandom.hex(4), @cart.purchase_number].join("-")
    end


    def ship_add_params
      params.require(:ship_add).permit(:first_name, :last_name, :street_1, :street_2, :city, :state, :zipcode, :country, :phone_number, :cart_id)
    end

    def recalculate_cart_prices(cart_items)
      @cart.sub_total = cart_items.sum(:price)
      @cart.taxes = cart_items.sum(:taxes)
      @cart.shipping = cart_items.sum(:shipping)
      @cart.total_price = cart_items.sum(:total_price)
      @cart.save
    end
  
    def set_discount
      @discount = Discount.includes(:business).where(active: true, slug: params[:discountID]).first
      return redirect_to root_path, alert: "Discount does not exists. Please add another discount" if @discount.blank?
    end

   def set_seller
    @seller_id = nil
    @sold_by = "marketpad" ##default should be marketpad
    refererType = params[:refererType].squish if params[:refererType].present?
    profile_type = ::REFERER_TYPES[refererType]
    if params[:refererID].present? && params[:refererID].squish.present?
      profile_id = params[:refererID].split("-")[0]
      @seller_id = profile_id
      @sold_by = profile_type
    end

   end
   
  ##handle multiple businesses carts
	def set_cart
    business_id = @discount.present? ? @discount.business_id : @cart_item.business_id
    user_cart = session["user_cart"]
    cart_exist = user_cart.detect {|uc| uc["#{business_id}_business_id"]} if user_cart.present?
    if user_cart.present? && cart_exist.present?
      cart_id = cart_exist["#{business_id}_cart_id"]
      @cart = Cart.find_by_id(cart_id)   
    else
      @cart = Cart.create(user_id: current_user.try(:id))
      (session["user_cart"] ||= []) << {"#{business_id}_business_id" => business_id, "#{business_id}_cart_id" => @cart.id}
    end

    @cart    
  end

  def set_cart_item
  	@cart_item = CartItem.find_by_id(params[:itemID])
  end

end


##