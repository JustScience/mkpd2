class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :location_remembered?, :user_zipcode, :user_city, :user_state, :user_search_radius, :user_location

  ##user location remembered in DB
  def location_remembered?
    current_user.present? && current_user.remember_loc
  end

	def user_zipcode
    if location_remembered?
      current_user.zipcode
    else
      session[:zipcode]
    end
	end

	def user_city
    if location_remembered?
      current_user.city
    else
      session[:city]
    end
	end

  def user_state
    if location_remembered?
      current_user.state
    else
      session[:state]
    end
  end

  def user_search_radius
  	if location_remembered?
      current_user.search_radius.present? ? current_user.search_radius : 100 ##miles
    else
      session[:search_radius].present? ? session[:search_radius] : 100 ##miles
    end
  end

  def user_location
    if user_city.present? && user_state.present? && user_zipcode.present?
  	  "#{user_city}, #{user_state}  |  #{user_zipcode}"
    end
  end


end
