class UsersController < ApplicationController

  before_action :authenticate_user!, except: [:stripe_request_callback, :accept_deal_gift]

  ###########
  ## Filters
  ###########
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  
  ##update profile for Business / Organization / Agency
    def profile
    return redirect_to root_path if current_user.customer?
    @profile = current_user.profile.present? ? current_user.profile : Profile.new
    @account_type = current_user.user_type.titleize
    if @profile.try(:country).present?
      country = Country.where(name: @profile.country).first
      @states = country.states.select(:name).order(:name).pluck(:name) if country.present?
    end
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb "Profile" ##without link
	end

  def update_profile
    @profile = current_user.profile.present? ? current_user.profile : Profile.new
    @profile.attributes = profile_params
    @profile.user_id = current_user.id
    @profile.profile_type = current_user.user_type
    if @profile.save
      redirect_to show_account_path(@profile.profile_type, @profile.slug)
    else
      if @profile.try(:country).present?
         country = Country.where(name: @profile.country).first
         @states = country.states.select(:name).order(:name).pluck(:name) if country.present?
    end
      render action: :profile
    end
  end
  
  ##get all discount liked by any user
  def favorite_discounts
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb "My Favorites" ##without link
    @discounts = Discount.includes(:likes).where(active: true).paginate(page: params[:page], per_page: DEFAULT_LIMIT).where(id: current_user.likes.collect(&:discount_id))
  end

  ##get all deals purchased by any user
  def redeem
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb "Redeem Deals" ##without link
    render "users/customer/redeem"
    @deals = @discounts ##deal type discounts purchased by current user
  end
  
  ##merchant account
  def merchant_account
    if !current_user.business? || current_user.profile.blank? 
     return redirect_to profile_path, alert: "You must have a business profile to enable merchant account abilities."
    end
    @merchant_accounts = current_user.merchant_accounts
  end
  
  ##handle stripe connect request initiate
  def stripe_connect
    if Rails.env.development? || Rails.env.staging?
      callback_url = stripe_connect_callback_url
    else
      callback_url = "https://www.marketpad.com/stripe_connect_callback"
    end
    url = "https://connect.stripe.com/oauth/authorize?"
    query = {
      response_type: "code",
      scope: "read_write",
      stripe_landing: "register",
      client_id: ENV['STRIPE_CONNECT_CLIENT_ID'],
      redirect_uri: callback_url,
      state: current_user.id
    }.to_query
    Rails.logger.info query
    redirect_to url + query
  end

  ##handle stripe connect callback
  def stripe_connect_callback
    ##check if params state(current user id) present
    begin
      code = params[:code]
      state = params[:state]
      if params[:state].present?

        if params["error"]
          flash[:alert] = params["error_description"]
          return redirect_to merchant_account_url
        end
      
        resp = HTTParty.post("https://connect.stripe.com/oauth/token",{
          headers: {"Authorization" => "Bearer #{ENV['STRIPE_SECRET_KEY']}"},
          body: {code: code, grant_type: "authorization_code"}
        })

        if resp["error"]
          flash[:alert] = resp["error_description"]
          return redirect_to merchant_account_url
        end


        user = User.where(id: params[:state]).first
        profile = user.profile
        if profile.present?

          parsed_response = resp.parsed_response
          livemode = parsed_response['livemode'] ##live or test
          merchant_account = profile.merchant_accounts.where(livemode: livemode).first
          merchant_account =  MerchantAccount.new if merchant_account.blank?
          
          attributes = parsed_response.merge!(user_id: state, profile_id: profile.id, account_type: 'Stripe', active: true)
          merchant_account.attributes = attributes
          if merchant_account.save
            redirect_to merchant_account_url, notice: "Your stripe account has been successfully linked or updated with MarketPad."
          else
            redirect_to merchant_account_url, alert: "There is any problem while saving your stripe account details. Please try again."
          end

        end
      end
    rescue Exception => e
      Rails.logger.error "Exception Occurs:: #{e.message}"
      Airbrake.notify(e) if Rails.env.staging? || Rails.env.production?
    end
  end
  
  ##show all purchases of customer
  def my_purchase
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb "My Purchase" ##without link
    @items = CartItem.includes(:discount, :business, :cart).joins(:cart, :discount).where("discounts.discount_type =? AND carts.user_id = ? AND carts.paid =? OR giftee_email =?", 'deal', current_user.id, true, current_user.email).paginate(page: params[:page], per_page: DEFAULT_LIMIT).order("carts.completed_at DESC")
  end
  
  ##show business sales
  def my_sales
    return redirect_to root_path unless current_user.business? && current_user.profile.business?
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb "My Sales" ##without link
    @items = CartItem.includes(:discount, :business, :cart, cart: :customer).joins(:cart).where("cart_items.business_id = ? AND carts.paid =?", current_user.profile.id, true).paginate(page: params[:page], per_page: DEFAULT_LIMIT).order("carts.completed_at DESC")
    render "users/business/my_sales"
  end
  
  ##mark deal as redeemed by customer or business
  def mark_deal_redeem
    item = CartItem.where(id: params[:item_id]).first
    if item.present? && !item.redeemed? && item.deal?
      item.update(redeemed: true, redeemed_by: current_user.id, redeemed_time: Time.now)
      @success = true
    else
    end
  end

  ##send deal as gift by customer to friend
  def send_deal_as_gift
    item = CartItem.where(id: params[:item_id]).first
    if item.present? && !item.redeemed? && item.deal? && !item.gift
      if item.update(gift: true, gifter_id: current_user.id, gift_time: Time.now, giftee_email: params[:giftee_email], gifter_name: current_user.full_name)
        ##send email on giftee's email address
        NotificationMailer.delay.send_deal_as_gift(params[:giftee_name], params[:giftee_email], current_user.full_name, item.sales_purchase_number)
        flash[:notice] = "Gift sent successfully!!"
        render js: "window.location = '#{my_purchase_path}'"
      else
        flash[:alert] = "There is an issue while sending gift. Please try again."
        render js: "window.location = '#{my_purchase_path}'"
      end
    else
      flash[:alert] = "Deal is already redeemed or sent as gift."
      render js: "window.location = '#{my_purchase_path}'"
    end
  end

  def accept_deal_gift
    return redirect_to root_path if params[:giftID].blank?
    
    if current_user.blank?
      session[:deal_gift_id] = params[:giftID] 
      session[:return_url] = request.url
      return redirect_to login_path
    end

    @item = CartItem.where(sales_purchase_number: params[:giftID]).first
    return redirect_to root_path, alert: "Gift does not exist or already redeemed." if @item.blank? || @item.redeemed?

    ##check if giftee email not same as current user's email
    return redirect_to root_path, alert: "Sorry!! This gift does not belongs to you." if @item.giftee_email != current_user.email


  end


  #####################
  ## Protected Methods
  #####################
  
  protected

  ###################
  ## Private Methods
  ###################

  private

    def profile_params
      params.require(:profile).permit(:name, :description, :street, :city, :state, :zipcode, :website, :fb_link,
        :twitter_link, :google_link, :image, :cover_image, :family_friendly, :phone_number, :country, :affiliate_code)
    end

end
