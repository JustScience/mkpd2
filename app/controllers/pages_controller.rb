require 'will_paginate/array'
class PagesController < ApplicationController

  ###########
  ## Filters
  ###########
  before_action :set_slides, only: [:index]
  skip_before_action :verify_authenticity_token, only: [:save_location, :update_location]

  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  def index
    #get local businesss / organizations within range of 100 miles
    @show_categories = true
    radius = user_search_radius
    user_address = get_address
    @businesses = []
    @organizations = []
    @discounts = []

    if user_address.present?
      if radius == "w" ##world wide
        @businesses = Profile.where(profile_type: 'business').limit(DEFAULT_LIMIT)
        @organizations = Profile.where(profile_type: 'organization').limit(DEFAULT_LIMIT)
        if params[:catTypeID].present?
          @discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).limit(DEFAULT_LIMIT)
        else
          @discounts = Discount.includes(:likes).only_active.limit(DEFAULT_LIMIT)
        end
      else
        @businesses = Profile.where(profile_type: 'business').near(user_address, radius, order: 'distance').limit(DEFAULT_LIMIT)
        @organizations = Profile.where(profile_type: 'organization').near(user_address, radius, order: 'distance').limit(DEFAULT_LIMIT)

        if params[:catTypeID].present?
          @discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).near(user_address, radius, order: 'distance').limit(DEFAULT_LIMIT)
        else
          @discounts = Discount.includes(:likes).only_active.near(user_address, radius, order: 'distance').limit(DEFAULT_LIMIT)
        end     
       
      end
    end

    ##we have to show all products irrespective of user location
    if params[:catTypeID].present?
      @products = Discount.includes(:likes).joins(:categories).only_active_products.where(categories: {id: params[:catTypeID]}).limit(DEFAULT_LIMIT)
    else
      @products = Discount.includes(:likes).only_active_products.limit(DEFAULT_LIMIT)
    end   

  end

  def show_account
    @show_categories = true
    @profile = Profile.where(profile_type: params[:type], slug: params[:slug]).first

    return redirect_to root_path, alert: "Profile you are looking does not exist." if @profile.blank?
    #@discounts = Discount.all
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb @profile.name ##without link
    return redirect_to root_path if @profile.blank?

    ##set page SEO
    set_page_seo

    radius = user_search_radius
    user_address = get_address
    
    ##get discounts on the basis of profile type business / organization / agency
    if @profile.business?
      ##DISPLAY ALL DISCOUNTS FOR BUSINESS
      if params[:catTypeID].present?
        @discounts = @profile.discounts.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      else
        @discounts = @profile.discounts.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      end
      
      ##we have to show all products irrespective of user location
      if params[:catTypeID].present?
        @products = @profile.discounts.includes(:likes).joins(:categories).only_active_products.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      else
        @products = @profile.discounts.includes(:likes).only_active_products.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      end

    elsif @profile.organization?

      if radius == "w" ##world wide
        if params[:catTypeID].present?
          @discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
        else
          @discounts = Discount.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
        end
      else
        ##DISPLAY ALL DISCOUNTS WITHIN 100 MILES OF USERS LOCATION
        if params[:catTypeID].present?
          @discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius, order: 'distance')
        else
          @discounts = Discount.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius, order: 'distance')
        end
      end

      if params[:catTypeID].present?
        @products = Discount.includes(:likes).joins(:categories).only_active_products.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      else
        @products = Discount.includes(:likes).only_active_products.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      end

    elsif @profile.agency?

    elsif @profile.affiliate?

      if radius == "w"

        if params[:catTypeID].present?
          @local_discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
        else
          @local_discounts = Discount.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
        end
      else
        ##DISPLAY ALL DISCOUNTS WITHIN 100 MILES OF USERS LOCATION
        if params[:catTypeID].present?
           @local_discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius, order: 'distance')
        else
          @local_discounts = Discount.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius, order: 'distance')
        end
      end

      if params[:catTypeID].present?
        @products = Discount.includes(:likes).joins(:categories).only_active_products.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      else
        @products = Discount.includes(:likes).only_active_products.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      end

      ##DISPLAY ALL DISCOUNTS THAT ARE ASSOCIATED WITH THIS AFFILIATE
      if params[:catTypeID].present?
        @associated_discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).where(affiliate_code: @profile.user.affiliate_code).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      else
        @associated_discounts = Discount.includes(:likes).only_active.where(affiliate_code: @profile.user.affiliate_code).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      end

    end

      render template: "users/#{@profile.profile_type}/show"

  end
  

  ## Index of All Local Organizations
  def organizations
    radius = user_search_radius
    user_address = get_address
    if radius == "w"
      @organizations = Profile.where(profile_type: 'organization').paginate(page: params[:page], per_page: DEFAULT_LIMIT)
    else
      @organizations = Profile.where(profile_type: 'organization').paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius)
    end
  end

  ## Index of All Local Businesses
  def businesses
    radius = user_search_radius
    user_address = get_address
    if radius == "w"
      @businesses = Profile.where(profile_type: 'business').paginate(page: params[:page], per_page: DEFAULT_LIMIT)
    else
      @businesses = Profile.where(profile_type: 'business').paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius)
    end
  end

  ## Index of All Local Discounts
  def discounts
    @show_categories = true
    radius = user_search_radius
    user_address = get_address
    if radius == "w"
      if params[:catTypeID].present?
        @discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      else
        @discounts = Discount.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
      end
    else
      if params[:catTypeID].present?
        @discounts = Discount.includes(:likes).joins(:categories).only_active.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius)
      else
        @discounts = Discount.includes(:likes).only_active.paginate(page: params[:page], per_page: DEFAULT_LIMIT).near(user_address, radius)
      end
    end


  end

  ## Index of All PRODUCTS
  def products
    @show_categories = true
    if params[:catTypeID].present?
      @products = Discount.includes(:likes).joins(:categories).only_active_products.where(categories: {id: params[:catTypeID]}).paginate(page: params[:page], per_page: DEFAULT_LIMIT)
    else
      @products = Discount.includes(:likes).only_active_products.paginate(page: params[:page], per_page: DEFAULT_LIMIT)
    end     
   
  end


  ##Save user location from browser geolocation
  def save_location
    session[:user_lat] = params[:lat]
    session[:user_lon] = params[:lon]

    ##reload page if user location found first time
    if session[:location].blank?
      @reload_page = true
    end
    query = "#{session[:user_lat]},#{session[:user_lon]}"
    begin
      result = Geocoder.search(query).first
      if result.present?

        session[:city] = result.city ##store city in session
        result.data["address_components"].each do |component|
          if component["types"].include?("administrative_area_level_1")
            #state = component["short_name"]
            state = component["long_name"] #if state.blank?
            session[:state] = state ##store state in session
          end

          if component["types"].include?("postal_code")
            zipcode = component["short_name"]
            zipcode = component["long_name"] if zipcode.blank?
            session[:zipcode] = zipcode ##store zipcode in session
          end
        end
      end
      if session[:city].present? && session[:state].present? && session[:zipcode].present?
        ##if location changed then reload page
        if session[:location] != "#{session[:city]}, #{session[:state]}  |  #{session[:zipcode]}"
          @reload_page = true
        end
        session[:location] = "#{session[:city]}, #{session[:state]}  |  #{session[:zipcode]}"

      end

    rescue Exception => e
      Rails.logger.info "Falied to save user location"
    end

  end
  

  ##update user location from location modal box
  def update_location
    ##check for params
    return request.referer if (params[:city].blank? || params[:state].blank? || params[:zipcode].blank?)
    session[:search_radius] = params[:search_radius] if params[:search_radius].present?
    query = "#{params[:city]},#{params[:state]},#{params[:zipcode]}"
    begin
      result = Geocoder.search(query).first
      if result.present?
        session[:user_lat] = result.latitude
        session[:user_lon] = result.longitude
        # session[:city] = result.city ##store city in session
        # result.data["address_components"].each do |component|
        #   if component["types"].include?("administrative_area_level_1")
        #     state = component["short_name"]
        #     state = component["long_name"] if state.blank?
        #     session[:state] = state ##store state in session
        #   end

        #   if component["types"].include?("postal_code")
        #     zipcode = component["short_name"]
        #     zipcode = component["long_name"] if zipcode.blank?
        #     session[:zipcode] = zipcode ##store zipcode in session
        #   end
        # end
      end
      session[:city] = params[:city]
      session[:state] = params[:state]
      session[:zipcode] = params[:zipcode]
      if session[:city].present? && session[:state].present? && session[:zipcode].present?
        session[:location] = "#{session[:city]}, #{session[:state]}  |  #{session[:zipcode]}"
      end

      ##check if user is logged_in and remember location is true
      if current_user.present? 
        if params[:remember_loc].present? && params[:remember_loc] == "true"
          current_user.update(remember_loc: true, zipcode: params[:zipcode], city: params[:city], state: params[:state], search_radius: params[:search_radius])
        else
          current_user.update(remember_loc: false, zipcode: nil, city: nil, state: nil, search_radius: 100)
        end
      end

    rescue Exception => e
      Rails.logger.info "Falied to save / update user location"
    end

    redirect_to request.referer

  end



  ##Static Pages
  def about 
  end
  def contact 
  end
  def terms 
  end
  def privacy 
  end
  def faq 
  end
  def learn 
  end
  def charity 
  end
  def autism 
  end
  def for_business 
  end
  def welcome_back 
  end
  
  ##full text search
  def search
    return redirect_to root_path if params[:q].blank? || params[:q].squish.blank?
    query = params[:q]
    @results = []
    where_conditions = []
    where_conditions << ["category_type = '#{params[:filter_by_dis]}'"] if params[:filter_by_dis].present?
    where_conditions << ["category_type = '#{params[:filter_by_pro]}'"] if params[:filter_by_pro].present?
    result = PgSearch.multisearch(query).includes(:searchable).where(where_conditions.join(" OR "))
    @results = result.map(&:searchable).paginate(page: params[:page], per_page: DEFAULT_LIMIT) if result.present?
   
  end
  
  ##store subscription emails from front end
  def subscribe
    subcribe = Subscriber.new(subscriber_params)
    if subcribe.save
      @success = "Your email has been stored in our mailing list!!"
    else
      @error = "You have already subscribed with us!!"
    end
  end

  def load_states
    country = Country.where(name: params[:country_name]).first
    @states = State.where(country_id: country.try(:id)).order(:name).pluck(:name)
    @profile = Profile.where(id: params[:profile_id]).first
  end
  
  ##request payout by affiliate / ageny / organization
  def request_payout
    return unless current_user.affiliate? || current_user.agency? || current_user.organization?
    profile = current_user.profile
    return unless profile.present?

    @earning = AffiliateEarning.where(profile_id: profile.id).first
    @pt = PayoutThreshold.first
    if @earning.present? && @earning.earnings_after_last_payout.to_f > 0.0
      @enable_payout_btn = @earning.earnings_after_last_payout.to_f >= @pt.amount.to_f     
    end
  end

  def submit_payout_request
    return unless current_user.affiliate? || current_user.agency? || current_user.organization?
    profile = current_user.profile
    return unless profile.present?

    earning = AffiliateEarning.where(profile_id: profile.id).first
    earnings_after_last_payout = earning.earnings_after_last_payout.to_f
    pt = PayoutThreshold.first
    if earning.present? &&  earnings_after_last_payout > 0.0
      if earnings_after_last_payout >= pt.amount.to_f
        ActiveRecord::Base.transaction do
           earning.reload
           earning.update(earnings_after_last_payout:  (earning.earnings_after_last_payout - earnings_after_last_payout), last_payout: earnings_after_last_payout)
          
          ##create payout requests for Admin
           PayoutRequest.create(request_date: Time.now, request_amount: earnings_after_last_payout, requester_id: profile.id, requester_type: profile.profile_type)
           ##send email to marketpad about payout request
           NotificationMailer.delay.payout_request(profile, earnings_after_last_payout)
        end
        redirect_to request_payout_path, alert: "We have received your payout request  and we will process it soon." 
      end
    end

  end

  #####################
  ## Protected Methods
  #####################
  protected

  ###################
  ## Private Methods
  ###################

  private

    def set_page_seo
      @page_title = "Profile - #{@profile.name}"
    end

    def get_address
      [user_city, user_state, user_zipcode].compact.join(', ')
    end

	  def set_slides
      @slides = Slide.where(params[:id])
	  end

    def subscriber_params
      params.require(:subscriber).permit(:name, :email)

    end
	  
end