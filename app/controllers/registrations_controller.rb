class RegistrationsController < Devise::RegistrationsController

  ###########
  ## Filters
  ###########
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  def edit
    ## add breadcrumbs code
    add_breadcrumb "Home", :root_path, data: { turbolinks: false }
    add_breadcrumb "Account" ##without link
    super
  end


  ###################
  ## Private Methods
  ###################


  private

  #####################
  ## Protected Methods
  #####################
  protected
  

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def after_sign_up_path_for(resource)
    if resource.user_type == "customer"
      if session[:return_url].present?
        session[:return_url]
      else
        root_path
      end
    else
      profile_path
    end
  end

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :user_type, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :user_type, :password_confirmation, :current_password)
  end

end