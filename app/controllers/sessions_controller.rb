class SessionsController < Devise::SessionsController

  ###########
  ## Filters
  ###########
  protect_from_forgery
  skip_before_filter :verify_authenticity_token
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  #####################
  ## Protected Methods
  #####################
  protected

  ###################
  ## Private Methods
  ###################
  private

    #return the path after user logged in to system
    def after_sign_in_path_for(resource)
      if session[:return_url].present?
        session[:return_url]
      else
        root_url
      end
    end

end
