class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  ###########
  ## Filters
  ###########
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  ##handle facebook authentication
  def facebook
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in(@user)
      if session[:return_url].present?
        redirect_to session[:return_url], notice: 'Signed in successfully.'
        session[:return_url] = nil
      else
        redirect_to root_path, notice: 'Signed in successfully.'
      end
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to root_url, alert: 'Sorry, we couldn’t finish the login process. Please try again.'
    end
  end

  ##handle Google+ authentication
  def google_oauth2
    @user = User.find_for_google_oauth2(request.env["omniauth.auth"], current_user)
    if @user.persisted?
      sign_in(@user)
      if session[:return_url].present?
        redirect_to session[:return_url], notice: 'Signed in successfully.'
        session[:return_url] = nil
      else
        redirect_to root_path, notice: 'Signed in successfully.'
      end
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to root_url, alert: 'Sorry, we couldn’t finish the login process. Please try again.'
    end
  end
  

  #####################
  ## Protected Methods
  #####################
  protected

  ###################
  ## Private Methods
  ###################

  private

end
