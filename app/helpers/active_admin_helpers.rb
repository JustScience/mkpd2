module ActiveAdminHelpers
  # make this method public (compulsory)
  def self.included(dsl)
    # nothing ...
  end

  def countries_list
    ["USA"]
  end

  def us_states
     [
      ['Alabama'],
      ['Alaska'],
      ['Arizona'],
      ['Arkansas'],
      ['California'],
      ['Colorado'],
      ['Connecticut'],
      ['Delaware'],
      ['District of Columbia'],
      ['Florida'],
      ['Georgia'],
      ['Hawaii'],
      ['Idaho'],
      ['Illinois'],
      ['Indiana'],
      ['Iowa'],
      ['Kansas'],
      ['Kentucky'],
      ['Louisiana'],
      ['Maine'],
      ['Maryland'],
      ['Massachusetts'],
      ['Michigan'],
      ['Minnesota'],
      ['Mississippi'],
      ['Missouri'],
      ['Montana'],
      ['Nebraska'],
      ['Nevada'],
      ['New Hampshire'],
      ['New Jersey'],
      ['New Mexico'],
      ['New York'],
      ['North Carolina'],
      ['North Dakota'],
      ['Ohio'],
      ['Oklahoma'],
      ['Oregon'],
      ['Pennsylvania'],
      ['Puerto Rico'],
      ['Rhode Island'],
      ['South Carolina'],
      ['South Dakota'],
      ['Tennessee'],
      ['Texas'],
      ['Utah'],
      ['Vermont'],
      ['Virginia'],
      ['Washington'],
      ['West Virginia'],
      ['Wisconsin'],
      ['Wyoming']
    ]
  end
  
 ##return users list that are not associated wit
  def users_list
  	users_list = []
  	users = User.where("user_type != 'customer'").order("email asc")
  	users.each {|u| users_list << ["#{u.email} :: (#{u.user_type})", u.id]} if users.present?

  	users_list
  end

  def businesss_list
    Profile.where(profile_type: 'business').order("name asc").pluck(:name, :id)
  end

  def affiliates_list
    affliates_list  = []
    User.where(user_type: 'affiliate').order("email asc").collect{|u| affliates_list << ["#{u.email} :: (#{u.affiliate_code})", u.affiliate_code]}
    affliates_list
  end

end