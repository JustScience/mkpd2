module ApplicationHelper

  def customer?
  	current_user && current_user.user_type == "customer"
  end

  def business?
  	current_user && current_user.user_type == "business"
  end

  def organization?
  	current_user && current_user.user_type == "organization"
  end

  def agency?
  	current_user && current_user.user_type == "agency"
  end

  def affiliate?
    current_user && current_user.user_type == "affiliate"
  end

  def countries_list
    ["USA"]
  end

  def us_states
    # {
    # "AL (Alabama)"=>"AL", "AK (Alaska)"=>"AK", "AZ (Arizona)"=>"AZ", "AR (Arkansas)"=>"AR", "CA (California)"=>"CA", "CO (Colorado)"=>"CO", "CT (Connecticut)"=>"CT", "DE (Delaware)"=>"DE", "DC (District of Columbia)"=>"DC", "FL (Florida)"=>"FL", "GA (Georgia)"=>"GA", "HI (Hawaii)"=>"HI", "ID (Idaho)"=>"ID", "IL (Illinois)"=>"IL", "IN (Indiana)"=>"IN", "IA (Iowa)"=>"IA", "KS (Kansas)"=>"KS", "KY (Kentucky)"=>"KY", "LA (Louisiana)"=>"LA", "ME (Maine)"=>"ME", "MD (Maryland)"=>"MD", "MA (Massachusetts)"=>"MA", "MI (Michigan)"=>"MI", "MN (Minnesota)"=>"MN", "MS (Mississippi)"=>"MS", "MO (Missouri)"=>"MO", "MT (Montana)"=>"MT", "NE (Nebraska)"=>"NE", "NV (Nevada)"=>"NV", "NH (New Hampshire)"=>"NH", "NJ (New Jersey)"=>"NJ", "NM (New Mexico)"=>"NM", "NY (New York)"=>"NY", "NC (North Carolina)"=>"NC", "ND (North Dakota)"=>"ND", "OH (Ohio)"=>"OH", "OK (Oklahoma)"=>"OK", "OR (Oregon)"=>"OR", "PA (Pennsylvania)"=>"PA", "PR (Puerto Rico)"=>"PR", "RI (Rhode Island)"=>"RI", "SC (South Carolina)"=>"SC", "SD (South Dakota)"=>"SD", "TN (Tennessee)"=>"TN", "TX (Texas)"=>"TX", "UT (Utah)"=>"UT", "VT (Vermont)"=>"VT", "VA (Virginia)"=>"VA", "WA (Washington)"=>"WA", "WV (West Virginia)"=>"WV", "WI (Wisconsin)"=>"WI", "WY (Wyoming)"=>"WY"}
     [
      ['Alabama'],
      ['Alaska'],
      ['Arizona'],
      ['Arkansas'],
      ['California'],
      ['Colorado'],
      ['Connecticut'],
      ['Delaware'],
      ['District of Columbia'],
      ['Florida'],
      ['Georgia'],
      ['Hawaii'],
      ['Idaho'],
      ['Illinois'],
      ['Indiana'],
      ['Iowa'],
      ['Kansas'],
      ['Kentucky'],
      ['Louisiana'],
      ['Maine'],
      ['Maryland'],
      ['Massachusetts'],
      ['Michigan'],
      ['Minnesota'],
      ['Mississippi'],
      ['Missouri'],
      ['Montana'],
      ['Nebraska'],
      ['Nevada'],
      ['New Hampshire'],
      ['New Jersey'],
      ['New Mexico'],
      ['New York'],
      ['North Carolina'],
      ['North Dakota'],
      ['Ohio'],
      ['Oklahoma'],
      ['Oregon'],
      ['Pennsylvania'],
      ['Puerto Rico'],
      ['Rhode Island'],
      ['South Carolina'],
      ['South Dakota'],
      ['Tennessee'],
      ['Texas'],
      ['Utah'],
      ['Vermont'],
      ['Virginia'],
      ['Washington'],
      ['West Virginia'],
      ['Wisconsin'],
      ['Wyoming']
    ]
  end

  def get_user_location_from_session
    if location_remembered?
      session[:location] = user_location
    else
      session[:location].present? ? session[:location] : "My Location"
    end
  end

  def location_exists?
    user_location
  end
  
  ##return number of items added in current cart session
  def cart_items_size
    return 0 if session[:user_cart].blank?
    cart_ids = []
    session[:user_cart].each {|uc| cart_ids << uc.values[1]}
    CartItem.where(cart_id: cart_ids).count
    # cart = Cart.where(id: session[:cart_id]).first
    # if cart.present?
    #   cart.cart_items.size
    # else
    #   0
    # end
  end

  def search_radius_select
    [["50miles", 50], ["100miles", 100], ["200miles", 200], ["500miles", 500], ["1000miles", 1000], ["World Wide", 'w'] ]
  end

  def last_error_msg (msg, model)
    msg == model.errors.full_messages.last
  end

  def formatted_error_messages (model)
    errors_string = ''
    model.errors.full_messages.each do |msg|
      errors_string = errors_string +  msg
      errors_string = errors_string +  ', ' unless last_error_msg(msg, model)
    end
    errors_string
  end


  def tel_to(text)
    groups = text.to_s.scan(/(?:^\+)?\d+/)
    link_to text, "tel:#{groups.join '-'}"
  end  

  def get_sub_categories(parent_cat)
    sub_categories = Rails.cache.fetch("sub_categories_#{parent_cat}") {
      Category.where(class_name: parent_cat).pluck(:name, :id)
    }
  end

end


