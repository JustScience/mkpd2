ActiveAdmin.register PayoutRequest do

	includes :requester
	actions :all, except: [:new, :edit, :destroy]

	 index do
	    selectable_column
	    column :id
	    column :requester
	    column :requester_type
	    column :request_amount do |cur|
	      number_to_currency cur.request_amount
	    end
	    column :request_date
	    actions defaults: true
	  end

	  show do
	    attributes_table do
	      row :requester
	       row :requester_type
	      row :request_amount do |cur|
	        number_to_currency cur.request_amount
	      end
	      row :request_date
	      row :created_at
	      row :updated_at
	    end
	  end

end