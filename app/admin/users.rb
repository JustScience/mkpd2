ActiveAdmin.register User do

  includes :profile

	permit_params :first_name, :last_name, :email, :password, :password_confirmation, :user_type, :gender
  

  filter :first_name
  filter :last_name
  filter :email
  filter :user_type

  index do
    selectable_column
    # column :id do |resource|
    #   link_to resource.id, admin_user_path(resource)
    # end
    column :first_name
    column :last_name
    column :email
    column :user_type
    column :profile
    actions defaults: true
  end

  form do |f|
    f.inputs "User Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :user_type, as: :select, prompt: 'Please select account type', collection: User::USER_TYPES
      f.input :password unless resource.persisted?
      f.input :password_confirmation unless resource.persisted?
      f.input :gender, as: :select, prompt: 'Please select a gender', collection: ["female", "male"]
    end
    f.actions
  end

  show do
    attributes_table do
      row :id 
      row :first_name
      row :last_name
      row :email
      row :user_type
      row :gender
      row :provider
      row :uid
      row :dob
      row :affiliate_code
      row :created_at
      row :updated_at      
    end
  end


end
