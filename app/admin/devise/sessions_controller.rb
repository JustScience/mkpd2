class Admin::SessionsController < ActiveAdmin::Devise::SessionsController
  ###########
  ## Filters
  ###########
  protect_from_forgery
  skip_before_filter :verify_authenticity_token
  ############
  ## Requires
  ############
  #############
  ## Constants
  #############
  ##################
  ## Public Actions
  ##################

  #####################
  ## Protected Methods
  #####################
  protected

  ###################
  ## Private Methods
  ###################
  private

end
