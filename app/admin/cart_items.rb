ActiveAdmin.register CartItem do

	actions :all, except: [:new, :destroy, :edit]
	includes :cart

	#filter :cart
	filter :sales_purchase_number
	filter :seller
	filter :discount
	filter :business

	index do
    selectable_column
    column :id
    column :sales_purchase_number
    column :cart
    column :sold_by
    column :total_price do |cur|
      number_to_currency cur.total_price
    end
   
    actions defaults: true
  end


end
