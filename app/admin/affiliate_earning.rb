ActiveAdmin.register AffiliateEarning do
	includes :profile
	actions :all, except: [:new, :edit, :destroy]

	 index do
	    selectable_column
	    column :id
	    column :profile
	    column :profile_type
	    column :total_earnings_till_date do |cur|
	      number_to_currency cur.total_earnings_till_date
	    end
	    column :earnings_after_last_payout do |cur|
	      number_to_currency cur.earnings_after_last_payout
	    end
	    column :last_payout do |cur|
	      number_to_currency cur.last_payout
	    end
	    actions defaults: true
	  end

	  show do
	    attributes_table do
	      row :profile
	      row :profile_type
	      row :total_earnings_till_date do |cur|
	      number_to_currency cur.total_earnings_till_date
	     end
	     row :earnings_after_last_payout do |cur|
	      number_to_currency cur.earnings_after_last_payout
	     end
	     row :last_payout do |cur|
	      number_to_currency cur.last_payout
	     end
	      
	      row :created_at
	      row :updated_at
	    end
	  end
end
