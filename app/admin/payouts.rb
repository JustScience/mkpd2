ActiveAdmin.register Payout do

	actions :all, except: [:new, :destroy]

	includes :cart, :cart_item, cart_item: :business

	#filter :cart
	filter :price
	filter :created_at
	filter :updated_at

	index do
    selectable_column
    column :id
    column :cart
    column :cart_item
    column :price do |cur|
      number_to_currency cur.price
    end
    column :business_percentage do |cur|
      number_to_currency(cur.business_percentage) + " (#{cur.cart_item.try(:business).try(:name)})"
    end
    column :created_at
    actions defaults: true
  end


    show do
	    attributes_table do
	      row :id
	      row :cart
	      row :cart_item

	      ##you can simply change column name like this
	     #  row "Name of label" do |cur|
	     #    number_to_currency cur.price
		    # end
	      row :price do |cur|
	        number_to_currency cur.price
		    end
		    row :business_percentage do |cur|
		      number_to_currency(cur.business_percentage) + " (#{cur.cart_item.business.name})"
		    end
		    row :payment_gateway_percentage do |cur|
	        number_to_currency cur.payment_gateway_percentage
		    end
		    row :marketpad_percentage do |cur|
		      number_to_currency cur.marketpad_percentage
		    end
		    row :seller_percentage do |cur|
		      number_to_currency(cur.seller_percentage) + " (#{cur.cart_item.seller_name})"
		    end  
		    row :business_affiliate_percentage do |cur|
	        number_to_currency(cur.business_affiliate_percentage) + (cur.cart_item.business.affiliate_code.present? ? " (#{User.find_by_affiliate_code(cur.cart_item.business.affiliate_code).try(:full_name)})" : "")
		    end
		    row :discount_affiliate_percentage do |cur|
		      number_to_currency(cur.discount_affiliate_percentage) + (cur.cart_item.discount.affiliate_code.present? ? " (#{User.find_by_affiliate_code(cur.cart_item.discount.affiliate_code).try(:full_name)})" : "")
		    end 
		    row :marketpad_pool_percentage do |cur|
	        number_to_currency cur.marketpad_pool_percentage
		    end
		    

	      row :created_at
	      row :updated_at
	    end
  end

end
