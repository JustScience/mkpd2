ActiveAdmin.register MerchantAccount do

	actions :all, except: [:new, :destroy, :edit, :update]
	includes :profile

	filter :profile, label: "Business"
	
	index do
    column "Business" do |resource|
      resource.profile
    end
    column "Account Connected" do |resource|
      resource.livemode ? "Yes" : "No"
    end
  end


end
