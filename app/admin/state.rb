ActiveAdmin.register State do

	includes :country

	permit_params :country_id, :name

end