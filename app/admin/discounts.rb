include ActiveAdminHelpers
ActiveAdmin.register Discount do

  includes :business

	permit_params :name, :description, :discount_type, :price, :discounted_price, :image, :family_friendly, :business_id, :active, :affiliate_code, :limit, :limit_per_customer, :extra_price
  
  filter :name
  filter :family_friendly
  filter :discount_type
  filter :business
  filter :active
  filter :affiliate_code

  controller do

    def create
      @discount = Discount.new(discount_params)
      if params[:discount][:business_id].present?
      business = Profile.where(id: params[:discount][:business_id]).first
        if business.present?
          @discount.latitude = business.latitude
          @discount.longitude = business.longitude
        end
      end
      if @discount.save
        redirect_to admin_discount_path(@discount), notice: "Discount was successfully created."
      else
        flash[:alert] = "Discount was not created."
        render action: :new
      end
    end


    def update
      @discount = Discount.where(id: params[:id]).first
      if params[:discount][:business_id].present?
      business = Profile.where(id: params[:discount][:business_id]).first
        if business.present?
          @discount.latitude = business.latitude
          @discount.longitude = business.longitude
        end
      end
      if @discount.update(discount_params)
        redirect_to admin_discount_path(@discount), notice: "Discount was successfully updated."
      else
        flash[:alert] = "Discount was not updated."
        render action: :edit
      end
    end

    def discount_params
      params.require(:discount).permit(:name, :description, :discount_type, :price, :discounted_price, :image, :family_friendly, :latitude, :longitude, :active, :affiliate_code, :business_id,
        :limit, :limit_per_customer, :extra_price, :sold_out, :expiry_date, :terms, :default_terms_conditions, {category_ids:  []})
    end

  end
 
  index do
    selectable_column
    column :id
    column :name
    column :discount_type
    column :family_friendly
    column :business
    column :active
    column :sold_out
    column :price do |cur|
      number_to_currency cur.price
    end
    column :discounted_price do |cur|
      number_to_currency cur.discounted_price
    end
    actions defaults: true
  end

  show do
    attributes_table do
      row :name
      row "View Discount on front end" do |resource|
        link_to resource.slug, discount_path(resource.slug), target: "_blank"
      end
      row :description
      row :discount_type
      row :price do |cur|
        number_to_currency cur.price
      end
      row :discounted_price do |cur|
        number_to_currency cur.discounted_price
      end
      row :extra_price do |cur|
        number_to_currency cur.extra_price
      end
      row :limit
      row :limit_per_customer
      row "Categories" do |resource|
        resource.categories.pluck(:name).join(", ") if resource.categories.present?
      end
      row :business
      row :family_friendly
      row :sold_out
      row :expiry_date
      row :active
      row :affiliate_code
      row :terms
      row :image do
        image_tag discount.image_url(:medium), style: "height: 200px;min-width:200px;" if discount.image.present?
      end      
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs "Discount Details" do
      f.input :business_id, as: :select, prompt: "Please select a business", collection: businesss_list
      f.input :name
      f.input :description
      f.input :discount_type, as: :select, include_blank: false, collection: Discount::DISCOUNT_TYPES
      f.input :affiliate_code, as: :select, prompt: "Please select a affiliate", collection: affiliates_list
      f.input :category_ids, label: "Categories", as: :select, include_blank: false, collection: Category.select(:name, :id).collect{ |c| [c.name, c.id]}, input_html: {multiple: true}
      f.input :family_friendly
      f.input :price
      f.input :discounted_price
      f.input :extra_price
      f.input :limit
      f.input :limit_per_customer
      f.input :sold_out
      f.input :expiry_date
      f.input :active
      f.input :default_terms_conditions, as: :select, collection: DISCOUNT_TERMS_AND_CONDITIONS, prompt: "Please select a terms form the list"
      f.input :terms, as: :ckeditor
      f.input :image, as: :file
    end
    f.actions
  end


end
