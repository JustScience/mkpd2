include ActiveAdminHelpers
ActiveAdmin.register Profile do
  includes :user

	permit_params :name, :description, :street, :city, :state, :zipcode, :website, :fb_link,
    :twitter_link, :google_link, :image, :cover_image, :family_friendly, :phone_number, :country, :affiliate_code,
    :profile_type, :user_id, :id

  filter :name
  filter :family_friendly
  filter :profile_type
  filter :country
  filter :state
  filter :affiliate_code

   before_filter only: [:edit, :update] do
    
  end



  index do
    selectable_column
    column :id
    column :name
    column :profile_type
    column :family_friendly
    column :user
    actions defaults: true
  end

  form do |f|
    f.inputs "Profile Details" do
      f.input :id, as: :hidden
      f.input :user_id, as: :select, prompt: "Please select a user", collection: users_list
      f.input :profile_type, input_html: {readonly: true}
      f.input :affiliate_code, as: :select, prompt: "Please select a affiliate", collection: affiliates_list ##display only when profile type is business
      f.input :name
      f.input :family_friendly
      f.input :description
      f.input :street
      f.input :country, as: :select, include_blank: false, collection: Country.order(:name).pluck(:name), prompt: "Please select a country"
      f.input :state, as: :select, prompt: "Please select a state", collection: []
      f.input :city
      f.input :zipcode
      f.input :phone_number    
      f.input :website, hint: "Ex- http://www.example.com"
      f.input :fb_link
      f.input :twitter_link
      f.input :image, as: :file
      f.input :cover_image, as: :file
    end
    f.actions
  end

  show do
    attributes_table do
      row :user
      row :profile_type
      row :affiliate_code if profile.business?
      row :name
      row "View Profile on front end" do |resource|
        link_to resource.slug, show_account_path(resource.profile_type, resource.slug), target: "_blank"
      end
      row :description
      row :street
      row :city
      row :state
      row :phone_number
      row :zipcode
      row :country
      row :latitude
      row :longitude
      row :website
      row :fb_link
      row :twitter_link
      row :family_friendly
      row :image do
        image_tag profile.image_url(:medium), style: "height: 200px;min-width:200px;" if profile.image.present?
      end
      row :cover_image do
        image_tag profile.cover_image_url(:medium), style: "height: 200px;min-width:200px;" if profile.cover_image.present?
      end
      row :created_at
      row :updated_at
    end
  end


end
