ActiveAdmin.register Category do
  
  permit_params :name, :tage, :class_name
  
  filter :name
  filter :class_name

	form do |f|
    f.inputs "Category Details" do
      f.input :name
      f.input :class_name, as: :select, prompt: 'Please select a class', collection: CATEGORIES_CLASS_NAME
      f.input :tags, hint: "Use comma for multiple tags."
    end
    f.actions
  end


end
