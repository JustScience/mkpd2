ActiveAdmin.register ShippingAddress do

  actions :all, except: [:new, :edit, :destroy]

  includes :cart

  #filter :cart
	filter :first_name
	filter :last_name
	filter :city
	filter :state
	filter :phone_number
	filter :cart_id

	index do
    selectable_column
    column :id
    column :cart
    column :first_name
    column :last_name
    column :street_1
    column :street_2
    column :city
    column :phone_number
    column :zipcode
    actions defaults: true
  end

end