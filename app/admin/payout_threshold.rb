ActiveAdmin.register PayoutThreshold do
	permit_params :amount

	actions :all, except: [:new, :destroy]

	index do
	    selectable_column
	    column :amount do |cur|
	      number_to_currency cur.amount
	    end
         actions defaults: true
      end

      show do
         attributes_table do
         	row :amount do |cur|
	        number_to_currency cur.amount
	      end
         end
       end

end