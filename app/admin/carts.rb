ActiveAdmin.register Cart do

  actions :all, except: [:new, :edit, :destroy]

  includes :customer, :shipping_address

	#filter :cart
	filter :paid
	filter :completed_at

	index do
    selectable_column
    column :id
    column :sub_total do |cur|
      number_to_currency cur.sub_total
    end
    # column :shipping do |cur|
    #   number_to_currency cur.shipping
    # end
    # column :taxes do |cur|
    #   number_to_currency cur.taxes
    # end
    column :total_price do |cur|
      number_to_currency cur.total_price
    end
    column :application_fees do |cur|
      number_to_currency cur.application_fees
    end
    column :paid
    column :customer
    column :completed_at
    column :shipping_address
    actions defaults: true
  end

   show do
    attributes_table do
      
	    row :sub_total do |cur|
	      number_to_currency cur.sub_total
	    end
	    row :shipping do |cur|
	      number_to_currency cur.shipping
	    end
	    row :taxes do |cur|
	      number_to_currency cur.taxes
	    end
	    row :total_price do |cur|
	      number_to_currency cur.total_price
	    end
      row :application_fees do |cur|
        number_to_currency cur.application_fees
      end
	    row :customer
	    row :shipping_address
	    row :cart_items do
        if cart.cart_items.present?
          cart.cart_items.each_with_index do |cart_item, index|
            div do
              link_to "Cart Item #{index + 1}:  #{cart_item.discount.discount_type}", admin_cart_item_path(cart_item), class: "white-color"
            end
          end
        end
      end
    	row :paid
      row :completed_at   
      row :created_at
      row :updated_at
    end
  end

 

end
