// Modal Script

var ready;
ready = function() {

	var $btn = $('.modalTrigger'),
		$bg = $('.modal-bg'),
		$content = $('.modal-frame');

	$btn.click(function(){

		// Animate Modal Out

		if ($btn.hasClass('modal-open')) {
			console.log('Close Modal');
				$bg.removeClass('modal-in');
				$content.removeClass('modal-in');
			setTimeout(function(){
				$btn.removeClass('modal-open');
				$content.removeClass('modal-open');
				$bg.removeClass('modal-open');
			}, 300);

		// Animate Modal In

		} else {
			console.log('Open Modal');
				$bg.removeClass('modal-out');
				$content.removeClass('modal-out');
				$btn.addClass('modal-open');
				$bg.addClass('modal-open');
				$content.addClass('modal-open');
				$bg.addClass('modal-in');
				$content.addClass('modal-in');
			setTimeout(function(){
			}, 900);
		}
	});

};

$(document).ready(ready);
$(document).on('page:load', ready);