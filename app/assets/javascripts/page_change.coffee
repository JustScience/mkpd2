$(document).ready ->
	$(document).on 'page:fetch', ->
	  $('#main-content').removeClass 'fadeIn',
	  $('#main-content').addClass 'fadeOut'

	$(document).on 'page:restore', ->
	  $('#main-content').removeClass 'fadeOut',
	  $('#main-content').addClass 'fadeIn'
