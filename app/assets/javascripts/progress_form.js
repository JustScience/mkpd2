var ready;
ready = function() {

  // Progressive Form Next Step

  $('.prog-form-next').click(function(){
    var $body = $('.prog-form').parents('.center'),
        $next = $(this),
        $steps = $('.prog-form-steps').children().length,
        $step = $next.parents('.prog-form-body .prog-form-step'),
        stepIndex = $step.index(),
        $page = $('.prog-form-progress .prog-circle').eq(stepIndex);

    console.log('Go To Next Slide', $body, $steps, $step, stepIndex, $page);

    if($steps > (stepIndex + 1)) { 
      nextStep($step, $page); 
    } else { 
      finalStep($body, $step, $page); 
    }
        
    function nextStep($step, $page){
      console.log('Next Step');
      // animate the step out
      $step.addClass('animate-next-out');
      
      // animate the step in
      setTimeout(function(){
        $step.removeClass('animate-next-out is-visible')
             .next().addClass('is-visible animate-next-in');
        $page.removeClass('is-active')
             .next().addClass('is-active');
      }, 600);
      
      // after the animation, adjust the classes
      setTimeout(function(){
        $step.next().removeClass('animate-next-in');
      }, 1200);
    }

    function finalStep($body, $step, $page){
      // animate the step out
      // $body.addClass('animate-up');
      $('#progress_form').submit();
      $('#progress_submit_btn').attr('disabled', true);
      $('#progress_submit_btn').text("Saving...");
      console.log('DONE!');
    }
  });


  // Progressive Form Previous Step

  $('.prog-form-back').click(function(){
    
    var $back = $(this),
        $step = $back.parents('.prog-form-body .prog-form-step'),
        stepIndex = $step.index(),
        $page = $('.prog-form-progress .prog-circle').eq(stepIndex);

    console.log('Go To Next Slide', $step, stepIndex, $page);

    if(stepIndex === 3 || stepIndex === 2 || stepIndex === 1) { 
      backStep($step, $page); 
    } 
        
    function backStep($step, $page){
      console.log('Step Back');
      // animate the step out
      $step.addClass('animate-back-out');
      
      // animate the step in
      setTimeout(function(){
        $step.removeClass('animate-back-out is-visible')
             .prev().addClass('is-visible animate-back-in');
        $page.removeClass('is-active')
             .prev().addClass('is-active');
      }, 600);
      
      // after the animation, adjust the classes
      setTimeout(function(){
        $step.prev().removeClass('animate-back-in');
      }, 1200);
    }
    
    console.log('Go To Previous Step');
  });
  

  // Progressive Form Icon Navigation

  $('.prog-circle').click(function(){

    var $icon = $(this),
        iconIndex = $icon.index(),
        $fromPage = $('.prog-circle.is-active');
        fromIndex = $fromPage.index();
        $steps = $('.prog-form-steps').children().length,
        $fromStep = $('.prog-form-step.is-visible'),
        $toStep = $('.prog-form-step').eq(iconIndex);

    console.log('Go To Selected Slide', iconIndex, fromIndex);

    if (iconIndex > fromIndex) {

      // Animate Current Step Out
      $fromStep.addClass('animate-next-out');

      // Animate Next Step In
      setTimeout(function(){
        $fromStep.removeClass('animate-next-out is-visible');
        $toStep.addClass('is-visible animate-next-in');
        $fromPage.removeClass('is-active');
        $icon.addClass('is-active');
      }, 600);

      // Remove Animation Classes
      setTimeout(function(){
        $toStep.removeClass('animate-back-in');
      }, 1200);

    } 
    if (iconIndex < fromIndex) {

      // Animate Current Step Out
      $fromStep.addClass('animate-back-out');

      // Animate Next Step In
      setTimeout(function(){
        $fromStep.removeClass('animate-back-out is-visible');
        $toStep.addClass('is-visible animate-back-in');
        $fromPage.removeClass('is-active');
        $icon.addClass('is-active');
      }, 600);

      // Remove Animation Classes
      setTimeout(function(){
        $toStep.removeClass('animate-back-in');
      }, 1200);

    }

  });


  // Progressive Form Finish

  $('.prog-form-done').click(function(){
    var $done = $(this);
    
    console.log('Save Form Updates');
  });
  

  // Progressive Form Cancel

  $('.prog-form-cancel').click(function(){
    var $cancel = $(this);
    
    console.log('Cancel Form Update');
  });
  
};

$(document).ready(ready);
$(document).on('page:load', ready);
