//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require touchSwipe
//= require TweenMax.min
//= require TimelineMax.min
//= require jquery.gsap.min
//= require ScrollMagic
//= require jquery.ScrollMagic
//= require ScrollMagic_indicators
//= require ScrollMagic.gsap
//= require rippleria
//= require addtohomescreen

//= require turbolinks
//= require jqturbo
//= require chosen-jquery
//= require ckeditor/init
//= require_tree .

$( document ).on('turbolinks:load', function() {
  $('#flashes').fadeOut(5000);
  
  addToHomescreen();
});
