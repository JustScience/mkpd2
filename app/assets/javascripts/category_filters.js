var ready;
ready = function() {

	var $drawer = $("#category-filters .sub-categories-drawer"),
		$links = $("#category-filters .category-filters-wrap"),
		$sidenav = $("#side-nav #sidenav-category-trigger"),
		$mobile = $("#side-nav .sub-categories-drawer.mobile");

	$links.mousedown(function() {
		$drawer.slideDown();
		$drawer.addClass('.drawer-is-open');
	});

	$links.mouseleave(function() {
		$drawer.slideUp();
		$drawer.removeClass('.drawer-is-open');
	});

	$sidenav.mousedown(function() {
		if ($mobile.hasClass('.drawer-is-open')) {
			$mobile.slideUp();
			$mobile.removeClass('.drawer-is-open');			
		} else {
			$mobile.slideDown();
			$mobile.addClass('.drawer-is-open');			
		}
	});

};

$(document).ready(ready);
$(document).on('page:load', ready);
