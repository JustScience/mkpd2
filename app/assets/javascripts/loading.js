var ready;
ready = function() {

	var loader = $('#loading'),
		logo = $('#loading-logo'),
		dot = $('.load-dot'),
		tlLoadLogo = new TimelineMax({repeat:-1});
		tlLoadText = new TimelineMax({repeat:-1});

	// LOADER LOGO ANIMATION
	tlLoadLogo
		.fromTo(logo,1.8,
			{autoAlpha:1,scale:0},
			{autoAlpha:0,scale:1,ease:Power0.easeIn}
		);

	// LOADER TEXT ANIMATION
	tlLoadText
		.staggerFromTo(dot,1.8,
			{autoAlpha:1,scale:0},
			{autoAlpha:0,scale:1,ease:Power0.easeIn}
		),0.1;


	// LOADER VISIBILITY
	if (loader.hasClass('is-loading')) {
		loader.css('display:','block;');
		loader.css('opacity:','1;');
 	} else {
		loader.css('opacity:','0;'); 		
		loader.css('display:','none;');
 	};
};

$(document).ready(ready);
$(document).on('page:load', ready);
