var ready;
ready = function() {

	$('.btn, .navbar-burger').rippleria({
		duration: 750,
		easing: 'ease-in-out',
		detectBrightness: true
	});

};

$(document).ready(ready);
$(document).on('page:load', ready);
