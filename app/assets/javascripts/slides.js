var ready;
ready = function() {

	var InfiniteRotator = 
	{
		init: function()
		{
			//initial fade-in time (in milliseconds)
			var initialFadeIn = 600;
			
			//interval between items (in milliseconds)
			var itemInterval = 6300;
			
			//cross-fade time (in milliseconds)
			var fadeTime = 300;
			
			//count number of items
			var numberOfItems = $('.slides').length;

			//set current item
			var currentItem = 0;

			//show first item
			$('.slide').eq(currentItem).toggleClass('is-active');

			//loop through the items		
			var infiniteLoop = setInterval(function(){
				$('.slide').eq(currentItem).toggleClass('is-active');

				if(currentItem == numberOfItems -1){
					currentItem = 0;
				}else{
					currentItem++;
				}
				$('.slide').eq(currentItem).toggleClass('is-active');

			}, itemInterval);	
		}	
	};

	InfiniteRotator.init();


	// var	$slides = $('.slides'),
	// 	$slide = $('.slide'),
	// 	slideIndex = $slide.index(),
	// 	$slideFirst = $slide.first(),
	// 	$slideActive = $('.slide.is-active');

	// function loopSlides() {
	// 	console.log('Looping ' + $slides.length + ' Slides');

	// 	firstSlide();
		
	// 	$slide.each(function(index, element) {
	// 		setTimeout(function() { 
	// 			console.log(index + ': ' + element)
	// 		}, 3000);
	// 	});
	
	// }

	// function firstSlide() {
	// 	$slide.first().addClass('is-active');
	// 	console.log('First Slide');
	// }

	// function nextSlide() {
	// 	$slideActive.removeClass('is-active');
	// 	console.log('Next Slide');
	// }

	// loopSlides();



};

$(document).ready(ready);
$(document).on('page:load', ready);
