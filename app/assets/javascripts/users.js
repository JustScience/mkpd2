var ready;
ready = function() {

	$('#account-type-help').mouseenter(function() {
		$('.form-helper-tip').addClass('is-visible');
	});
	$('#account-type-help').mouseleave(function() {
		$('.form-helper-tip').removeClass('is-visible');
	});

};

$(document).ready(ready);
$(document).on('page:load', ready);

