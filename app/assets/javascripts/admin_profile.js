$(document).ready(function(){

  $('#profile_user_id').change(function(){
    text = $('option:selected', $(this)).text();
    profile_type = text.split("::")[1].split("(")[1].split(")")[0]
    
    $('#profile_profile_type').val(profile_type);

    if(profile_type == "business"){
      $('#profile_affiliate_code').parent().show();
    }else{
      $('#profile_affiliate_code').parent().hide();
      $('#profile_affiliate_code').val(null);
    }

  });
  
  $('#profile_affiliate_code').parent().hide();
  
  if($('#profile_profile_type').val() == "business"){
    $('#profile_affiliate_code').parent().show();
  }
  

  $('#discount_default_terms_conditions').change(function(){
    val = $(this).val();
    CKEDITOR.instances['discount_terms'].setData(val);

  });

   $('#profile_country').change(function(){

      country_name = $(this).val();
      profile_id = $('#profile_id').val();
      $.ajax({
        type: 'POST',
        url: "/pages/load_states",
        data: {country_name: country_name, profile_id: profile_id}
      });
  });

   country_name = $('#profile_country :selected').val();
   profile_id = $('#profile_id').val();
    $.ajax({
      type: 'POST',
      url: "/pages/load_states",
      data: {country_name: country_name, profile_id: profile_id}
    });

});