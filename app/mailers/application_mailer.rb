class ApplicationMailer < ActionMailer::Base
  default from: 'alerts@marketpad.com'
  layout 'mailer'
end
