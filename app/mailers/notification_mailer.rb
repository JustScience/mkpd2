class NotificationMailer < ApplicationMailer

  include AbstractController::Callbacks
  after_action :set_delivery_options, except: [:welcome_email]
  
  def welcome_email(user)
  	@user = user
  	subject = "Welcome to MarketPad!"
  	mail(to: user.email, subject: subject)
  end

  def customer_purchase_receipt(cart, user, shipping_address)
  	@cart = cart
  	@user = user
  	@purchased_items = cart.cart_items.includes(:discount, :business, :seller)
  	subject = "Order Confirmation - Your Order with MarketPad.com [#{cart.purchase_number}] has been placed!"
  	mail(to: user.email, subject: subject)
  end

  def merchant_product_sale_info(cart, business_email, items, shipping_address, sub_total, taxes, shipping, total_price)
    @items = items
    @cart = cart
    @sub_total = sub_total
    @taxes = taxes
    @shipping = shipping
    @total_price = total_price
    subject = "MarketPad Product Sale Info | Order ID: [#{@cart.purchase_number}]"
    @shipping_address = shipping_address
    mail(to: business_email, subject: subject)
  end

  def merchant_weekly_sales_report(business_email, items, sub_total, taxes, shipping, total_sale)
    @items = items
    @sub_total = sub_total
    @taxes = taxes
    @shipping = shipping
    @total_sale = total_sale
    @business_name = items.first[3]
    subject = "MarketPad Weekly Sales Report | #{@business_name}"
    mail(to: business_email, subject: subject)
  end

  def send_deal_as_gift(giftee_name, giftee_email, gifter_name, purchase_no)
    @giftee_name = giftee_name
    @gifter_name = gifter_name
    #@gifter_email = gifter_email
    @purchase_no = purchase_no
    subject = "Your Friend Sent You A Gift with MarketPad!!"
    mail(to: giftee_email, subject: subject)
  end

  def payout_request(profile, earnings)
    @profile = profile
    @earnings = earnings
    subject = "New Payout Request for #{profile.profile_type.titleize}"
    email = "amarkmarketpad@gmail.com"
    mail(to: email, subject: subject)
  end


  private

  def no_reply_smtp_settings
    {
      address: "smtp.gmail.com",
      port: 587,
      domain: "gmail.com",
      user_name: "no-reply@marketpad.com",
      password: "qwer.1234!",
      authentication: "plain",
      enable_starttls_auto: true
    }
  end

  def set_delivery_options
    message.delivery_method.settings.merge!(no_reply_smtp_settings)
  end

end
