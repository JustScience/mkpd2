class PayoutThreshold < ApplicationRecord
	validates :amount, presence: true
end