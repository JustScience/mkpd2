class Slide < ApplicationRecord

  mount_uploader :image, SlideImageUploader

  validates :name, :image, :headline, :description, :button, :target, :style, presence: true

end
