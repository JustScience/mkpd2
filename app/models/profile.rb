class Profile < ActiveRecord::Base

  include PgSearch
  multisearchable against: [:name, :description], using: { tsearch: {any_word: true, prefix: true } , trigram: {:threshold => 0.1}}

  mount_uploader :image, ProfileImageUploader
  mount_uploader :cover_image, ProfileCoverImageUploader

  CATEGORIES = ['Food & Beverage', 'Health & Beauty', 'Clothing & Retail']

  validates :name, :description, :street, :city, :state, :zipcode, :phone_number, :country, :image, presence: true

  validates :name, uniqueness: {scope: :profile_type}
  validates :website, format: URI::regexp(%w(http https))

  before_save :create_slug, :save_lat_long
  after_save :save_pg_search_category
  validate :validate_affiliate
  after_create :create_affiliate_earning

  belongs_to :user
  has_many :discounts, class_name: "Discount", foreign_key: :business_id
  belongs_to :affiliate, class_name: "User", foreign_key: :affiliate_code
  has_many :merchant_accounts
  has_one :affiliate_earning
  has_many :payout_requests, class_name: "Profile", foreign_key: "requester_id"
  has_many :affiliate_earnings, class_name: "Profile", foreign_key: "profile_id"

  geocoded_by :address

  def referer_slug
    [id, slug].join("-").parameterize

  end

  def address
    [street, city, state, zipcode, country].compact.join(', ')
  end
  

  def business?
    profile_type == "business"
  end

  def agency?
    profile_type == "agency"
  end

  def organization?
    profile_type == "organization"
  end

  def affiliate?
    profile_type == "affiliate"
  end

  def affiliated_by?(user_affiliated_code)
    affiliate_code == user_affiliated_code
  end


  private
  
  ##validate affiliate code for businesses
  def validate_affiliate
    if affiliate_code.present? && affiliate_code.squish.present?
      user = User.where(affiliate_code: affiliate_code).first
      errors.add(:affiliate_code, "Affiliate code does not exists in our system, Please enter correct code.") if user.blank?
    end
  end

  def create_slug
  	self.slug = name.parameterize if name.present?
  end
  
  ##save geolocation latitude / longitude for business / organization / agency
  def save_lat_long
    if street.present? && city.present? && state.present?
      if street_changed? || city_changed? || state_changed?
        begin
          query = "#{street}, #{city}, #{state}"
          result = Geocoder.search(query).first
          self.latitude = result.latitude
          self.longitude = result.longitude
        rescue Exception => e
          Rails.logger.info "Falied to save profle latitude / longitude"
        end
      end
    end
  end

  ##add category type for search filters
  def save_pg_search_category
    self.pg_search_document.update_column(:category_type, profile_type) if self.pg_search_document.present?
  end

  def create_affiliate_earning
     if AFFILIATES_ARRAY.include?(profile_type)
       AffiliateEarning.create(profile_type: profile_type, profile_id: id)
     end
  end


end