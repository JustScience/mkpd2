class CartItem < ActiveRecord::Base

	belongs_to :cart
	has_one :payout
	belongs_to :discount
	belongs_to :business, class_name: "Profile", foreign_key: :business_id
	belongs_to :seller, class_name: "Profile", foreign_key: :seller_id

	def seller_name
		if seller_id.present?
			seller = Profile.where(id: seller_id).first
			seller.present? ? seller.name : "MarketPad"
		else
			"MarketPad"
		end
	end

	def sold_by_marketpad?
		sold_by.eql?("marketpad")
	end

	def product?
		discount.discount_type.eql?("product")
	end

	def deal?
		discount.discount_type.eql?("deal")
	end

	def redeemed?
		redeemed
	end

	def product_extra_price_present?
		discount.extra_price.present? && discount.extra_price > 0.0
	end

end