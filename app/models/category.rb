class Category < ActiveRecord::Base

	validates :name, presence: true
	has_and_belongs_to_many :discounts

	after_commit :expire_cache

	private

	  def expire_cache
	  	CATEGORIES_CLASS_NAME.each do |parent_cat|
	  	  Rails.cache.delete("sub_categories_#{parent_cat}")
	  	end
	  end
end