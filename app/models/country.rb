class Country < ActiveRecord::Base
	has_many :states

	validates :name, presence: true

	before_create :set_id

	private

	def set_id
	   id = Country.maximum(:id)
	   self.id = id.to_i + 1
	end
end