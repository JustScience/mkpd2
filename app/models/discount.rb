class Discount < ApplicationRecord

  include PgSearch
  multisearchable against: [:name, :description], using: { tsearch: {any_word: true, prefix: true } , trigram: {:threshold => 0.1}}, if: :active?
  #pg_search_scope :custom_search, :against => [:name, :description], if: :active?

  mount_uploader :image, DiscountImageUploader

  validates :name, :description, :image, :price, :discounted_price, :discount_type, :business_id, :limit, :limit_per_customer, presence: true

  belongs_to :business, class_name: "Profile", foreign_key: :business_id
  has_many :likes
  has_and_belongs_to_many :categories
  has_many :cart_items

  DISCOUNT_TYPES = [['Deal', 'deal'], ['Product', 'product']]
  
  after_save :create_slug, :save_pg_search_category

  scope :only_active, -> { where(discount_type: 'deal', active: true) }
  scope :only_active_products, -> { where(discount_type: 'product', active: true) }

  geocoded_by :address

  attr_accessor :default_terms_conditions

  def address
    [business.street, business.city, business.state, business.zipcode, business.country].compact.join(', ')
  end

  def to_param
    slug
  end

  def affiliated_by?(user_affiliated_code)
    affiliate_code == user_affiliated_code
  end

  def owned_by?(user)
  	business_id == user.try(:profile).try(:id)
  end

  ##check if discount is already liked by a user
  def liked?(user_id)
    likes.collect(&:user_id).include?(user_id)
  end
  
  ##method return true / false if customer purchase limit is reached for discount
  def cusomter_limit_reached_for_discount?(customer_id)
    return false if customer_id.blank? || limit_per_customer == "unlimited" || limit_per_customer.blank? || limit_per_customer.to_i == 0
    items_purchased_by_customer = cart_items.joins(:cart).where("carts.user_id = ? AND carts.paid = ?", customer_id, true).size
    return true if items_purchased_by_customer >= limit_per_customer.to_i
    return false ##else return false
  end
  
  ##method return true / false if discount purchase reached maximum limit 
  def check_discount_sold_out?
    return false if limit == "unlimited" || limit.blank? || limit.to_i == 0
    no_of_items_purchased = cart_items.joins(:cart).where("carts.paid = true").size
    return true if no_of_items_purchased >= limit.to_i
    return false ##else return false
  end
  
  def max_limit_reached?
    check_discount_sold_out?
  end

  def sold_out?
    sold_out
  end

  def expired?
    expiry_date.present? ? true : false
  end


  def mark_discount_as_sold_out
    Rails.logger.info "Inside Sold out discount:: #{id} Sold Out Date:: #{Date.today}"
    self.update_columns(sold_out: true, expiry_date: (Date.today + 10.days))
  end

  def active?
    active
  end

  private

  def create_slug
  	self.update_column(:slug, "#{id} #{name}".parameterize) if name_changed?
  end
  
  ##add category type for search filters
  def save_pg_search_category
    self.pg_search_document.update_column(:category_type, discount_type) if self.pg_search_document.present? && active
  end

end
