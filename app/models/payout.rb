class Payout < ActiveRecord::Base

	belongs_to :cart
	belongs_to :cart_item

	after_commit :calculate_earnings


	private

	def calculate_earnings
	  	##check for seller percentage
	  	if AFFILIATES_ARRAY.include?(cart_item.sold_by)
	  		aff_earning = AffiliateEarning.where(profile_id: cart_item.seller_id).first
	  		if aff_earning.present?
		  		ActiveRecord::Base.transaction do
		  		     aff_earning.reload
		  		     aff_earning.total_earnings_till_date +=  seller_percentage
		  		     aff_earning.earnings_after_last_payout +=  seller_percentage
		  		     aff_earning.save!
		  		end
		  	end
	  	end

	  	##for discount affiliate
	  	if cart_item.discount.affiliate_code.present?
	  		profile_id = User.where(affiliate_code: cart_item.discount.affiliate_code).first.try(:profile).try(:id)
	  		aff_earning = AffiliateEarning.where(profile_id: profile_id).first
	  		if aff_earning.present?
		  		ActiveRecord::Base.transaction do
		  		     aff_earning.reload
		  		     aff_earning.total_earnings_till_date +=  discount_affiliate_percentage
		  		     aff_earning.earnings_after_last_payout +=  discount_affiliate_percentage
		  		     aff_earning.save!
		  		end
		  	end
	  	end

	  	##for business affiliate
	  	if cart_item.business.affiliate_code.present?
	  		profile_id = User.where(affiliate_code: cart_item.business.affiliate_code).first.try(:profile).try(:id)
	  		aff_earning = AffiliateEarning.where(profile_id: profile_id).first
	  		if aff_earning.present?
		  		ActiveRecord::Base.transaction do
		  		     aff_earning.reload
		  		     aff_earning.total_earnings_till_date +=  business_affiliate_percentage
		  		     aff_earning.earnings_after_last_payout +=  business_affiliate_percentage
		  		     aff_earning.save!
		  		end
		  	end
	  	end
	end
end