class State < ActiveRecord::Base

	validates :country_id,  :name, presence: true
	belongs_to :country

	before_create :set_id

	private

	def set_id
	   id = State.maximum(:id)
	   self.id = id.to_i + 1
	end
end