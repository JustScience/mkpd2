class ShippingAddress < ActiveRecord::Base
	belongs_to :cart

	def full_name
		[first_name, last_name].join(" ")
	end

	def address
		[city, state, zipcode, country].join(" ")
	end

end