class Cart < ActiveRecord::Base

	has_many :cart_items
	has_many :payouts
	belongs_to :customer, class_name: "User", foreign_key: :user_id
	has_one :shipping_address
  

  def paid?
  	paid
  end

  def unpaid?
  	!paid
  end
  
  ##check products business and send sale email
  def send_products_sale_to_business
  	data_set = Hash.new { |hash, key| hash[key] = [] }
  	cart_items.includes(:discount, :business).each do |item|
  		if item.product?
	  		discount = item.discount
	  		business = item.business
	  		data_set[business.user.email] << [business.name, discount.name, discount.discount_type, item.price, item.taxes, item.shipping, item.total_price]
  	  end
  	end

  	if data_set.present?
  		ship_addr = shipping_address
  		data_set.each do |email, data|
  			sub_total = 0.0
  			taxes = 0.0
  			shipping = 0.0
  			total = 0.0
  			data.each do |d|
  				sub_total += d[3]
  				taxes += d[4]
  				shipping += d[5]
  				total += d[6]
  			end
  		  NotificationMailer.merchant_product_sale_info(self, email, data, ship_addr, sub_total, taxes, shipping, total).deliver_now
  		end
  	end
  end
  
  ##check if discount type product exist in cart
  def product_exist_in_cart?
    discount_types = Discount.where(id: cart_items.collect(&:discount_id).uniq).collect(&:discount_type)
    discount_types.include?("product") ? true : false
  end

	def purchase_number
		id.to_s.rjust(10, '0')
	end
  
  ##calculate sales commission payout
	def calculate_payout
    cart_items.includes(:discount, :business, :seller).each do |item|
    	payout = Payout.new
    	payout.cart_id = id
    	payout.cart_item_id = item.id
    	payout.price = item.total_price
    	payout.business_percentage = get_business_commission(payout.price)
    	payout.payment_gateway_percentage = get_payment_gateway_commission(payout.price)
    	payout.marketpad_pool_percentage = get_marketpad_pool_commission(payout.price)

      marketpad_percentage, seller_percentage, business_affiliate_percentage, discount_affiliate_percentage = get_sales_commission(item)
    	
    	payout.marketpad_percentage = marketpad_percentage
    	payout.seller_percentage = seller_percentage
    	payout.business_affiliate_percentage = business_affiliate_percentage
    	payout.discount_affiliate_percentage = discount_affiliate_percentage
        
    	###check if item is product and extra_price is present for product
    	if item.product? && item.product_extra_price_present?
    		extra_price = item.discount.extra_price

    		#payout.business_percentage = payout.business_percentage - extra_price
    		marketpad_pool_extra_percentage = (extra_price * MARKETPAD_POOL_STRATEGY_EXTRA_COMMISSION) / 100
    		payout.marketpad_pool_percentage = payout.marketpad_pool_percentage + marketpad_pool_extra_percentage
        marketpad_extra_percentage, seller_extra_percentage, business_extra_affiliate_percentage, discount_extra_affiliate_percentage = get_extra_sales_commission(item, extra_price)
    	  
    	  payout.marketpad_percentage = payout.marketpad_percentage + marketpad_extra_percentage
	    	payout.seller_percentage = payout.seller_percentage + seller_extra_percentage
	    	payout.business_affiliate_percentage = payout.business_affiliate_percentage + business_extra_affiliate_percentage
	    	payout.discount_affiliate_percentage = payout.discount_affiliate_percentage + discount_extra_affiliate_percentage
         
        ##recalculate business percentage as per product extra_price distribution
        payout.business_percentage = payout.business_percentage - (marketpad_pool_extra_percentage + marketpad_extra_percentage + seller_extra_percentage + business_extra_affiliate_percentage + discount_extra_affiliate_percentage)

    	end

    	if payout.save
        merchant_accounts = item.business.merchant_accounts
        if merchant_accounts.present?
          if Rails.env.development? || Rails.env.staging?
            merchant_account = merchant_accounts.where(livemode: false).first
          else
            merchant_account = merchant_accounts.where(livemode: true).first
          end

          if merchant_account.present?
            business_percentage_in_cents = (payout.business_percentage.to_f * 100).to_i
            total_revenue_cents_till_date = merchant_account.total_revenue_cents + business_percentage_in_cents
            merchant_account.total_revenue_cents = total_revenue_cents_till_date
            merchant_account.save
            ##transfer business payout after one week of pruchase
            #self.delay_until(7.days.from_now).transfer_business_payout(payout.business_percentage.to_f, purchase_number, merchant_account.id)

          end

        end
      end

    end
	end

  # def transfer_business_payout(business_percentage, purchase_number, merchant_account_id)
  #   Rails.logger.info "In business payout transfer method"
  #   business_percentage_in_cents = (business_percentage.to_f * 100).to_i
  #   merchant_account = MerchantAccount.find_by_id(merchant_account_id)
  #   transfer = nil
  #   begin
  #     transfer = Stripe::Transfer.create({
  #       amount: business_percentage_in_cents,
  #       currency: "usd",
  #       destination: merchant_account.stripe_user_id,
  #       transfer_group: purchase_number
  #     })
  #   rescue Exception => e
  #     Rails.logger.error "Exception Occurs while transfering money for Purchase #{purchase_number}:: #{e.message}"
  #     Airbrake.notify(e) if Rails.env.staging? || Rails.env.production?
  #   end

  #   if transfer && transfer.status == "paid" ##successfully paid in business account
  #     total_revenue_cents_till_date = merchant_account.total_revenue_cents + business_percentage_in_cents
  #     merchant_account.total_revenue_cents = total_revenue_cents_till_date
  #     merchant_account.save
  #   end
  # end

  def check_discounts_sold_out
    self.cart_items.each do |item|
      discount = item.discount
      discount.mark_discount_as_sold_out if discount.max_limit_reached?
    end
  end


	def get_business_commission(price)
		(price * BUSINESS_COMMISSION) / 100
	end

	def get_payment_gateway_commission(price)
		(price * PAYMENT_GATEWAY_COMMISSION) / 100
	end

	def get_marketpad_pool_commission(price)
		(price * MARKETPAD_POOL_STRATEGY_COMMISSION) / 100
	end

	def get_sales_commission(item)
		marketpad_percentage = 0.0
    seller_percentage = 0.0
    business_affiliate_percentage = 0.0
    discount_affiliate_percentage = 0.0

    ##default markepad commisions 2%
		marketpad_percentage += (item.price * MARKETPAD_COMMISSION) / 100

		if item.sold_by_marketpad?
			marketpad_percentage += (item.price * SELLER_COMMISSION) / 100
		else
			seller_percentage += (item.price * SELLER_COMMISSION) / 100
		end

		if item.business.affiliate_code.present?
			business_affiliate_percentage += (item.price * AFFILIATE_BUSINESS_COMMISSION) / 100
		else
			marketpad_percentage += (item.price * AFFILIATE_BUSINESS_COMMISSION) / 100
		end

		if item.discount.affiliate_code.present?
			discount_affiliate_percentage += (item.price * AFFILIATE_DISCOUNT_COMMISSION) / 100
		else
			marketpad_percentage += (item.price * AFFILIATE_DISCOUNT_COMMISSION) / 100
		end

		return marketpad_percentage, seller_percentage, business_affiliate_percentage, discount_affiliate_percentage

	end

	def get_extra_sales_commission(item, extra_price)
		marketpad_extra_percentage = 0.0
    seller_extra_percentage = 0.0
    business_extra_affiliate_percentage = 0.0
    discount_extra_affiliate_percentage = 0.0

    ##default markepad commisions 2%
		marketpad_extra_percentage += (extra_price * MARKETPAD_EXTRA_COMMISSION) / 100

		if item.sold_by_marketpad?
			marketpad_extra_percentage += (extra_price * SELLER_EXTRA_COMMISSION) / 100
		else
			seller_extra_percentage += (extra_price * SELLER_EXTRA_COMMISSION) / 100
		end

		if item.business.affiliate_code.present?
			business_extra_affiliate_percentage += (extra_price * AFFILIATE_BUSINESS_EXTRA_COMMISSION) / 100
		else
			marketpad_extra_percentage += (extra_price * AFFILIATE_BUSINESS_EXTRA_COMMISSION) / 100
		end

		if item.discount.affiliate_code.present?
			discount_extra_affiliate_percentage += (extra_price * AFFILIATE_DISCOUNT_EXTRA_COMMISSION) / 100
		else
			marketpad_extra_percentage += (extra_price * AFFILIATE_DISCOUNT_EXTRA_COMMISSION) / 100
		end

		return marketpad_extra_percentage, seller_extra_percentage, business_extra_affiliate_percentage, discount_extra_affiliate_percentage

	end

	
end