class PayoutRequest < ApplicationRecord
	belongs_to :requester, class_name: "Profile", foreign_key: "requester_id"
end