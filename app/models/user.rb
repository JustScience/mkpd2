class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  mount_uploader :image, UserImageUploader
  
  validates :user_type, presence: true
  has_many :carts
  has_many :paid_carts, -> { where(paid: true).order('completed_at DESC') }, class_name: "Cart", foreign_key: :user_id


  USER_TYPES = [['Affiliate', 'affiliate'], ['Agency', 'agency'], ['Business', 'business'], ['Customer', 'customer'], ['Organization', 'organization'] 
  ]


  HUMANIZED_ATTRIBUTES = {
    :user_type => "Account Type"
  }

  validates :first_name, :last_name, presence: true

  after_create :send_welcome_email_to_user

  def full_name
    [first_name, last_name].join(" ")
  end

  def self.us_states(state)
    st = "Idaho"
    if state.present?
      states = {
      "AL (Alabama)"=>"AL", "AK (Alaska)"=>"AK", "AZ (Arizona)"=>"AZ", "AR (Arkansas)"=>"AR", "CA (California)"=>"CA", "CO (Colorado)"=>"CO", "CT (Connecticut)"=>"CT", "DE (Delaware)"=>"DE", "DC (District of Columbia)"=>"DC", "FL (Florida)"=>"FL", "GA (Georgia)"=>"GA", "HI (Hawaii)"=>"HI", "ID (Idaho)"=>"ID", "IL (Illinois)"=>"IL", "IN (Indiana)"=>"IN", "IA (Iowa)"=>"IA", "KS (Kansas)"=>"KS", "KY (Kentucky)"=>"KY", "LA (Louisiana)"=>"LA", "ME (Maine)"=>"ME", "MD (Maryland)"=>"MD", "MA (Massachusetts)"=>"MA", "MI (Michigan)"=>"MI", "MN (Minnesota)"=>"MN", "MS (Mississippi)"=>"MS", "MO (Missouri)"=>"MO", "MT (Montana)"=>"MT", "NE (Nebraska)"=>"NE", "NV (Nevada)"=>"NV", "NH (New Hampshire)"=>"NH", "NJ (New Jersey)"=>"NJ", "NM (New Mexico)"=>"NM", "NY (New York)"=>"NY", "NC (North Carolina)"=>"NC", "ND (North Dakota)"=>"ND", "OH (Ohio)"=>"OH", "OK (Oklahoma)"=>"OK", "OR (Oregon)"=>"OR", "PA (Pennsylvania)"=>"PA", "PR (Puerto Rico)"=>"PR", "RI (Rhode Island)"=>"RI", "SC (South Carolina)"=>"SC", "SD (South Dakota)"=>"SD", "TN (Tennessee)"=>"TN", "TX (Texas)"=>"TX", "UT (Utah)"=>"UT", "VT (Vermont)"=>"VT", "VA (Virginia)"=>"VA", "WA (Washington)"=>"WA", "WV (West Virginia)"=>"WV", "WI (Wisconsin)"=>"WI", "WY (Wyoming)"=>"WY"
       }
      if states.key(state).present?
        st = states.key(state).split(" ")[1].split("(")[1].split(")")[0]        
      end
    end

    st

  end


  def self.human_attribute_name(attr, options = {})
    HUMANIZED_ATTRIBUTES[attr.to_sym] || super
  end

  has_one :profile
  has_many :likes ##discount likes
  has_many :merchant_accounts

  before_save :generate_affiliate_code
  
  ##return affiliate user businessess
  def affiliate_businesses
    Profile.where(affiliate_code: affiliate_code)
  end
  
  ##return affiliate user discounts
  def affiliate_discounts
    Discount.where(affiliate_code: affiliate_code)
  end

  def customer?
    user_type == "customer"
  end

  def business?
    user_type == "business"
  end

  def organization?
    user_type == "organization"
  end

  def agency?
    user_type == "agency"
  end

  def affiliate?
    user_type == "affiliate"
  end
  
  ##check if user already like a discount
  def liked?(discount_id)
    likes.collect(&:discount_id).include?(discount_id)
  end
  

  ###Auth from FB
  def self.from_omniauth(auth)
    data = auth.info
    extra = auth.extra.try(:raw_info)
    gender = (extra.present? && extra['gender'].present?) ? extra['gender'] : ''
    image = "https://graph.facebook.com/#{auth['uid']}/picture?type=large"
    user = User.where(provider: auth.provider, uid: auth.uid).first
    if user
      user.attributes = {first_name: data["first_name"], 
        last_name: data["last_name"], provider: auth.provider,
        gender: gender
      }
      user.save
    else
      user = User.where(email: auth.info.email).first
      if user
        user.attributes = {first_name: data["first_name"], 
        last_name: data["last_name"], provider: auth.provider
        }
        user.save
      else
        user = User.new
        user.email = data['email']
        user.password = Devise.friendly_token[0, 8]
        user.uid = auth.uid
        user.gender = gender
        user.remote_image_url = image
        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.provider = auth.provider
        user.user_type = "customer" ##default user should be customer
        user.save
      end
    end

    return user
  end

  ###Auth from Google+
  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    provider = "google"
    data = access_token.info
    uid = access_token.uid
    extra = data.extra.try(:raw_info)
    gender = (extra.present? && extra['gender'].present?) ? extra['gender'] : ''
    user = User.where(provider: provider, uid: uid).first
    if user
      user.attributes = {first_name: data["first_name"], 
        last_name: data["last_name"], provider: provider,
        gender: gender
      }
    user.save
      
    else
      user = User.where(email: data["email"]).first
      if user
        user.attributes = {first_name: data["first_name"], 
        last_name: data["last_name"], provider: provider
        }
        user.save
      else
        user = User.create(
          first_name: data["first_name"],
          last_name: data["last_name"],
          email: data["email"],
          provider: provider,
          remote_image_url: data["image"],
          password: Devise.friendly_token.first(8),
          uid: uid,
          gender: gender,
          user_type: "customer" ##default user should be customer
        )
      end
    end

    return user

  end


  private

    def generate_affiliate_code
      ##generate 6 digit unique code for every affiliate user
      self.affiliate_code = SecureRandom.hex(3) if affiliate? && affiliate_code.blank?
    end

    def send_welcome_email_to_user
      NotificationMailer.delay.welcome_email(self)
    end

  
end 
