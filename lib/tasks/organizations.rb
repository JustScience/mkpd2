###31 organizations profiles

org = []

org << {
  "name" => "K.G.S.A",
  "alias" => "k-g-s-a",
  "email" => "kgsaidahosoftball@gmail.com",
  "phone" => "(208) 755-8519",
  "website" => "",
  "main" => true,
  "affiliate" => true,
  "user_id" => "56719ee1f02cbe21660054b1",
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Kootenai Girls Softball Association",
  "mission_statement" => "This is a fundraising page created to help Kootenai Girls Softball Association (K.G.S.A.) with league costs and help give the girls of Kootenai County a fun and competitive softball league.  A portion of ALL gift cards and deals sold will help K.G.S.A. with league costs each year.  Browse! Purchase! Save! Share! Repeat!\r\n\r\n\r\n"
}

org <<

{
  "_id" => "56684f97f02cbe1732000b19",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Lake City High School DECA",
  "alias" => "lakecityhighschooldeca",
  "email" => "",
  "phone" => "(208) 769-0769",
  "website" => "",
  "main" => true,
  "affiliate" => true,
  "user_id" => "56684ef5f02cbe9fc1000b8f",
  "description" => "",
  
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!",
  "organization" => true,
  "mission_title" => "Lake City HS DECA",
  "mission_statement" => "This special store filled with local deals is for YOU! We want to help those that support and fund our schools- our parents, extended family, friends and community members! Do what you do everyday; go tanning, to the gym, eating out, getting hair cuts etc. just now save money doing those things.\r\n\r\nA portion of EVERY purchase made here will go to our school. Our goal is to help you help us fundraise, while we help you save $$$ doing things you already do.\r\n\r\nGroupon takes up to 50-60% of small businesses sales profits, whereas these deals leave business owners with over 90% of their profits. Our goal is to help build up our local economy, small businesses and enjoy great deals along the way. Browse! Purchase! Save! Share! Repeat!",
}


org<< 
{

  "name" => "Northwest Wildfire 14U",
  "alias" => "northwest-wildfire-14u",
  "email" => "",
  "phone" => "(208) 818 5878",
  "website" => "",
  "main" => true,
  "affiliate" => true,
  "user_id" => "56718eabf02cbe949100514b",
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Northwest Wildfire",
  "mission_statement" => "A portion of every purchase made on this page will go to NW Wildfire for fundraising efforts. This allows us to fundraise 24/7 365 days a year!  Browse! Purchase! Save! Share! Repeat!\r\n"
}

org << 

{
  "_id" => "56c6a52cbd40b1510d009d47",
  "show_real_name" => true,
  "show_email" => false,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "affiliate" => false,
  "name" => "Compare Policy",
  "phone" => "9818072345",
  "alias" => "comparepolicy",
  "user_id" => "56c6a52cbd40b1510d009d44",
  "email" => "",
  "website" => "https =>//www.comparepolicy.com",
 
  "description" => "<p>Comparepolicy.com is an insurance web Aggregator licensed and approved by the Insurance Regulatory and Development Authority of India (IRDAI). Our online insurance buying space provides users to compare, choose and buy the required insurance product like Term Insurance, Car Insurance, Health Plans and much more at the best rates available in the industry. The specialized expert team of comparepolicy.com  will hand hold in the online insurance buying process by offering quotes from top insurers till the hassle free policy issuance process. We proffer the unbiased insurance product’s comparisons and help you to make an informed hassle free buying decision just at an ease of a few clicks.</p>\n\n<h1>Contact Us</h1>\n\n<p>4/21 wave Silver Tower</p>\n\n<p>Sec-18 Noida UP 201301</p>\n\n<h2>For sales queries => +91-9818072345</h2>\n\n<h2>Complaints/suggestions => <a href=\"mailto =>help@comparepolicy.com\">help@comparepolicy.com</a></h2>\n",
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!",
  "organization" => true,
  "mission_title" => "Compare Policy",
  "mission_statement" => "Comparepolicy.com is a licensed insurance web Aggregator providing an online expert insurance solutions to the users as per their need and requirement. Comparepolicy.com deals in the insurance products available online, both Life & General from the top Insurance Companies in India. They offer customized insurance products like online Term Insurance, Car Insurance, Health Insurance, etc. To get an unbiased insurance plan which is best suited to you log on now."
}

org <<

{
  "_id" => "56c763e4bd40b10dba00ab55",
  "show_real_name" => true,
  "show_email" => false,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Coeur d'Alene High School DECA",
  "alias" => "chsdeca",
  "email" => "test@test.com",
  "phone" => "(208) 667-4507",
  "website" => "http =>//www.cdaschools.org//site/default.aspx?PageID=3012",
  "main" => true,
  "affiliate" => false,
  "user_id" => "56de107b1aa720f75400a4bc",

  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "CHS DECA",
  "mission_statement" => "A portion of EVERY purchase made here will go to our school. Our goal is to help you help us fundraise, while we help you save $$$ doing things you already do.  Browse! Purchase! Save! Share! Repeat!"
}

org <<
  {
  "_id" => "56e32b021aa720f70a012cf2",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "The Altar CDA",
  "alias" => "altarcda",
  "email" => "",
  "phone" => "(208) 664-1453",
  "website" => "http =>//www.altarcda.com",
  "main" => true,
  "affiliate" => true,
  "user_id" => "56e98ce71aa7208b9f003464",
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "The Altar Church",
  "mission_statement" => "A portion of EVERY purchase made here will go to The Altar Church and Pastor Tim. Our goal is to help you help us fundraise, while we help you save $$$ doing things you already do.  Browse! Purchase! Save! Share! Repeat!"
}


org <<
  {
  "_id" => "56fd7857e9330cf9e9004842",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "affiliate" => false,
  "name" => "Baymont Inn & Suites",
  "phone" => "(208) 667-6777",
  "user_id" => "56fd7857e9330cf9e900483f",
  "alias" => "baymontcda",
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Baymont Inn & Suites CdA",
  "mission_statement" => "Enjoy your stay at Baymont while also enjoying these local deals- the best deals in town!  Browse. Purchase. Print the voucher for free and ENJOY!  We support local businesses!",
  "email" => "",
  "website" => ""
}

org <<

{
  "_id" => "5710f9cc1aa72026a30053e3",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Qualfon",
  "alias" => "qualfonnw",
  "email" => "sday@qualfon.com",
  "phone" => "(208) 676-7801",
  "website" => "http =>//www.qualfon.com/",
  "main" => true,
  "affiliate" => false,
  "user_id" => "5716b1d9e9330cb092006ba4",
 
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Qualfon - Inland Northwest",
  "mission_statement" => "Qualfon and MarketPad have teamed up to bring you sweet deals.  A portion of EVERY purchase will be donated into an in-house pot that will be used to help one of our own.  Browse! Purchase! Save! Share! Repeat!"
}

org <<

{
  "_id" => "5717e69ce9330cc68200726c",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Syringa Family Partnership",
  "alias" => "syringafp",
  "email" => "colleen@syringafp.com",
  "phone" => "(208) 635-5907",
  "website" => "http =>//www.syringafp.com",
  "main" => true,
  "affiliate" => false,
  "user_id" => "5728fa621aa720c208018d3b",
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Syringa FP",
  "mission_statement" => "A portion of EVERY purchase will go to support OUR local cause or cause of our choice.  As a company we can put our collective minds together and come up with what, or who, we'd like to support, or maybe one of our own has hit hard times and we would like to help them out. Browse! Purchase! Save! Share! Repeat!\r\n"
}


org <<

{
  "_id" => "57436c691aa7206d370005fd",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Coeur d' Alene Crush Softball",
  "phone" => "(208) 818-0901",
  "alias" => "cdacrushsoftball",
  "user_id" => "57436c691aa7206d370005fa",
  "email" => "",
  "website" => "http =>//crushsoftball.org/",
  "mission_title" => "CdA Crush Softball",
  "mission_statement" => "A portion of every purchase made on this page supports fundraising efforts for CdA Crush.  We want to help those that support and fund our league- our parents, extended family, friends and community members!  Browse! Purchase! Save! Share! Repeat!\r\n",
  "description" => ""
}


org <<

  {
  "_id" => "5756650a1aa720377b014dba",
  "emailer_description" => "Value Build Homes is among the leading custom homebuilder in Sanford, NC. They expertise themselves in catering the highest quality construction in North Carolina, Fayetteville, Rocky Mount, Wilson, Burlington, Goldsboro, Asheboro, Sanford, Lumberton at the most reasonable prices. They endeavor their customers with 10 Year Warranty on the structural integrity of every home.  \r\n",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Value Build Homes",
  "phone" => "(919) 777-0393",
  "alias" => "value-build-homes",
  "user_id" => "5756650a1aa720377b014db7",

  "email" => "info@valuebuildhomes.com",
  "website" => "http =>//www.valuebuildhomes.com/",
  "mission_title" => "Office and Model Showroom",
  "mission_statement" => "",
  "description" => "<p><a href=\"http =>//www.valuebuildhomes.com/\">Value Built Homes</a>, Pioneer  custom homebuilder in North Carolina, Fayetteville, Rocky Mount, Wilson, Burlington, Goldsboro, Asheboro, Sanford, Lumberton, Pinehurst, Henderson, Mebane, Smithfield, Clayton, Louisburg, Dunn. They focusses on constructing highest quality homes at the most reasonable prices. They endeavor their services in custom built homes by offering a 10 Year Warranty on the structural integrity of every home.  Construct the house of your dreams by calling us today at 919.777.0393.</p>\n\n<p> </p>\n\n<p><strong>Contact =>-</strong><br>\nValue Build Homes<br>\nAaron Garner<br>\nOffice and Model Showroom<br>\n3015 S Jefferson Davis Hwy<br>\nSanford, NC 27332<br>\nGet Directions<br>\nTel => (919) 777-0393<br>\nFax => (919) 777-0133<br>\ninfo@valuebuildhomes.com<br>\nhttp =>//www.valuebuildhomes.com/</p>\n"
}

org <<

{
  "_id" => "5759202e1aa720f68f016998",
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Seattle General Contractor",
  "phone" => "(206) 499-7510",
  "alias" => "seattle-general-contractor",
  "user_id" => "5759202e1aa720f68f016995",
  "mission_title" => "",
  "mission_statement" => "",
  "description" => "<p>DP Palmer General Contractors has been proudly offering design, restoration, and painting services in the greater Seattle metropolitan area for many years. Our priority is the satisfaction of our clients, so we always take the time to ensure you are satisfied with the design of your space before starting any work. Our licensed, insured, and bonded painters, carpenters, masons, and designers have years of experience in small and large painting projects in Seattle.</p>\n\n<p>For more information please visit the website => <a href=\"http =>//www.dppalmer.com\">http =>//www.dppalmer.com</a></p>\n"
}

org <<

{
  "_id" => "5761577b1aa72055910042d5",
  "emailer_description" => "At BlueGraphix we hear you. Since our founding in 2006, we've striven to simplify the design process, while providing you with easy-to-manage tools. Keeping your website up to date is important with the changing times =>\r\n\r\nWe specialize in =>\r\n- Web Refresh - Full responsive mobile friendly websites\r\n- Mobile Website Design\r\n- Website Design\r\n- Website Support\r\n- Custom Programming\r\n- Custom WordPress Widgets\r\n- Custom Widgets\r\n- Search Engine Optimization\r\n- Google Local SEO\r\n- Google Plus",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Long Beach Web Designer",
  "alias" => "long-beach-web-designer",
  "email" => "bluegrafixdesignca@gmail.com",
  "phone" => "(562) 366-2640",
  "website" => "http =>//www.bluegrafixdesign.com/",
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "user_id" => "576155241aa720110e004271",
  "mission_title" => "",
  "mission_statement" => "",
  "description" => ""
}

org <<

{
  "_id" => "5761854d1aa720eaab0046dd",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => false,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Knudtsen Chevrolet",
  "phone" => "(208) 664-8107",
  "alias" => "knudtsenchevrolet",
  "user_id" => "5761854d1aa720eaab0046da",
  "updated_at" => Date(1476469052189),
  "created_at" => Date(1466008909779),
  "email" => "",
  "website" => "",
  
  "mission_title" => "Knudtsen Chevrolet",
  "mission_statement" => "A portion of EVERY purchase made here will go to the Kootenai County Boys & Girls Club. Our goal is to help you help them fundraise, while we help you save $$$ doing things you already do.\r\nBrowse! Purchase! Save! Share! Repeat!",
  "description" => ""
}

org <<

{
  "_id" => "5761854d1aa720eaab0046dd",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => false,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Knudtsen Chevrolet",
  "phone" => "(208) 664-8107",
  "alias" => "knudtsenchevrolet",
  "user_id" => "5761854d1aa720eaab0046da",
  "email" => "",
  "website" => "",
 
  "mission_title" => "Knudtsen Chevrolet",
  "mission_statement" => "A portion of EVERY purchase made here will go to the Kootenai County Boys & Girls Club. Our goal is to help you help them fundraise, while we help you save $$$ doing things you already do.\r\nBrowse! Purchase! Save! Share! Repeat!",
  "description" => ""
}

org <<

{
  "_id" => "576c069c59ffa83f5c0027dd",
  "emailer_description" => "",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Fishing Charter",
  "phone" => "(360) 268-1000",
  "alias" => "fishing-charter",
  "user_id" => "576c069c59ffa83f5c0027da",
  "mission_title" => " Fishing Charter",
  "mission_statement" => "",
  "description" => "<p>Quality sport fishing charters in Westport, Washington. Home of the Ranger, Neddie Rose, Shenandoah and 6-man Rock-n-Roll. We provide the following fishing charters =><br>\nSalmon.Tuna.Albacore.Lingcod.Rockfish.Halibu.</p>\n\n<p> </p>\n\n<p>Phone => (360) 268-1000</p>\n\n<p>Address => 2549 Westhaven Dr, Westport, WA 98595</p>\n\n<p><br>\nWebsite => </p>\n"
}

org <<

{
  "_id" => "5772db581aa7200ebb00443d",
  "emailer_description" => "Desert Safari is very special famous and must to do tour in Dubai in which you have an unforgettable real desert safari drive adventure with our traditional Arabic Bedouin campsite visit.\r\nwhere you can also enjoy with the entertainment of =>\r\n\r\n-Sheesha ( huubly bubbly )\r\n-Camel Riding\r\n-Henna painting\r\n-Sand Boarding\r\n-BBQ dinner\r\n-Unlimited soft drinks\r\n\r\nSpecial Features of Desert Safari Dubai =>\r\n-Pick and drop\r\n-Dune Bashing\r\n-Camel Riding\r\n-Sand Boarding\r\n-Heena painting\r\n-Arabic dates\r\n-Belly dancing\r\n-Tanoura Dancing\r\n-Dance floor\r\n-Music system\r\n-Sheesha ( hubbly bubbly )\r\n-Welcome drinks and starter\r\n-Toilet for ladies & gents\r\n-Unlimited Soft drinks and mineral water\r\n-Bar be que dinner for veg and non veg peoples\r\n-Bedouin tents with carpet & pillows\r\n-Dinner tables with carpet and pillows\r\n-Local dresses for men & women for photography",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Desert Safari Dubai",
  "alias" => "desert-safari-dubai",
  "email" => "abudhabidesertsafari@yandex.com",
  "phone" => "2104904347",
  "website" => "http =>//desertsafaridubai.mtdubai.com/desert-safari-dubai.html",
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "user_id" => "5772d9ec59a0dfe13c003dca",
  "mission_title" => "",
  "mission_statement" => "",
  "description" => "<p>Desert Safari is very special famous and must to do tour in Dubai in which you have an unforgettable real desert safari drive adventure with our traditional Arabic Bedouin campsite visit.<br>\nwhere you can also enjoy with the entertainment of =></p>\n\n<p>-Sheesha ( huubly bubbly )<br>\n-Camel Riding<br>\n-Henna painting<br>\n-Sand Boarding<br>\n-BBQ dinner<br>\n-Unlimited soft drinks</p>\n\n<p>Special Features of Desert Safari Dubai =><br>\n-Pick and drop<br>\n-Dune Bashing<br>\n-Camel Riding<br>\n-Sand Boarding<br>\n-Heena painting<br>\n-Arabic dates<br>\n-Belly dancing<br>\n-Tanoura Dancing<br>\n-Dance floor<br>\n-Music system<br>\n-Sheesha ( hubbly bubbly )<br>\n-Welcome drinks and starter<br>\n-Toilet for ladies &amp; gents<br>\n-Unlimited Soft drinks and mineral water<br>\n-Bar be que dinner for veg and non veg peoples<br>\n-Bedouin tents with carpet &amp; pillows<br>\n-Dinner tables with carpet and pillows<br>\n-Local dresses for men &amp; women for photography</p>\n"
}

org <<

{
  "_id" => "577a177dce4560b17500524c",
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!At Furniture Galore we have a great range of lounge suites to suit any style and budget.We operate 11 stores across Melbourne, and have done so for the last 25 years.",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Furniture Galore - Bedroom, Lounge, Furniture Store in Melbourne",
  "phone" => "1800 737 733",
  "alias" => "furniture-galore---bedroom--lounge--furniture-store-in-melbourne",
  "user_id" => "577a177dce4560b175005249",
  
  "mission_title" => "Furniture Galore - Bedroom, Lounge, Furniture Store in Melbourne",
  "mission_statement" => "Our roots stem from a Furniture manufacturing company operating in Melbourne for 25 years prior to opening our retail stores, we import all our range and we pride ourselves on offering our customers great styles at very affordable prices, with an assurance that we back our sales with a guarantee of quality and a high level of personal customer service.",
  "description" => "<p><span style=\"background-color =>transparent; color =>rgb(0, 0, 0); font-family =>arial; font-size =>16px\">Furniture Galore designs the furniture to inspire your ideal interior exclusively, we display and accessorize our furniture collection in a series of room settings so you can visualise how specific items may look in your space. Should you experience a problem with any product purchased from Furniture Galore please be assured it will be covered against faulty material or workmanship for the period of guarantee and conditions as listed below. We look for practicality in design, originality and vision when it comes to selecting furniture for our clients, which includes both the professional design community and private buyers alike.</span></p>\n"
}

org <<

{
  "_id" => "577c450c59a0df44d9005c46",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "CDA Casino Resort",
  "phone" => "(800) 523-2464",
  "alias" => "cdacasino",
  "user_id" => "577c450c59a0df44d9005c43",
  "email" => "",
  "website" => "http =>//cdacasino.com/",
 
  "mission_title" => "CDA Casino Resort",
  "mission_statement" => "The Coeur d'Alene Tribe has a grand legacy of taking care of its own and helping its neighbors. That, too, has been central through six expansions, creating 300 luxury hotel rooms and over 100,000 square feet of gaming space. \r\n\r\nOur Circling Raven Golf Club is renowned as one of the finest new golf challenges in the region, the nation and the world.",
  "description" => ""
}

org <<

{
  "_id" => "57965197ce45607718001ec9",
  "emailer_description" => "Good Guys Moving Company is based in San Antonio. We are very professional, experienced and we care about our customers and their belongings. We know how to make your move as stress-free as possible. Free estimates and friendly service. Call us today.",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "San Antonio Good Guys",
  "phone" => "(210) 960-6683",
  "alias" => "san-antonio-good-guys",
  "user_id" => "57965197ce45607718001ec6",
  "mission_title" => "San Antonio Good Guys",
  "mission_statement" => "Good Guys Moving Company is based in San Antonio. We are very professional, experienced and we care about our customers and their belongings. We know how to make your move as stress-free as possible. Free estimates and friendly service. Call us today.",
  "description" => "<p>Good Guys Moving Company is based in San Antonio. We are very professional, experienced and we care about our customers and their belongings. We know how to make your move as stress-free as possible. Free estimates and friendly service. Call us today.</p>\n"
}

org <<

{
  "_id" => "579a67871aa720a4ed0002b0",
  "emailer_description" => "Village Cove – Seattle Senior Housing and continuing care at Green Lake. Here are a few of the services and amenities we offer => \r\n\r\n- Weekly housekeeping services\r\n- Assisted Living\r\n- Senior Living\r\n- Nursing Home\r\n- Building maintenance\r\n- Landscaping services\r\n- Fitness center\r\n- Community room\r\n- Pets OK\r\n- Underground parking\r\n- Shuttle Service\r\n- Cable television & Wi-Fi\r\n- 24-hour security",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Seattle Senior Living",
  "alias" => "seattle-senior-living",
  "email" => "seattleseniorliving@yahoo.com",
  "phone" => "206-774-5153",
  "website" => "http =>//www.villagecove.org/about-us/our-story/",
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "user_id" => "579a66874811f59373000277",
  "mission_title" => "Seattle Senior Living",
  "mission_statement" => "WA",
  "description" => ""
}

org <<

{
  "_id" => "57b5e2d8a4152f6e340025c5",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Kootenai Humane Society",
  "phone" => "(208) 772-4019",
  "alias" => "kootenaihumanesociety",
  "user_id" => "57b5e2d8a4152f6e340025c2",
  "email" => "",
  "website" => "",
 
  "mission_title" => "Kootenai Humane Society",
  "mission_statement" => "This page is for all who support our movement.  A portion of EVERY transaction will go to help all of our costs.  Browse! Purchase! Save! Share! Repeat!",
  "description" => ""
}

org <<

{
  "_id" => "57bd7cafa4152f5b62000128",
  "emailer_description" => "We are Specialised Web Developers servicing in Melbourne, Glen Iris, Hawthorn, Richmond & Caulfield, provides Web Design & Web Development services.",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Slashdot - Web Design & Development",
  "phone" => "03 9998 2543",
  "alias" => "slashdot---web-design---development",
  "user_id" => "57bd7cafa4152f5b62000125",
  
  "mission_title" => "Slashdot - Web Design & Development",
  "mission_statement" => "Slashdot Web Development is a full-service development and design agency which specializes in creating an unforgiven web presence for you and your business. Visit our website and see how we can expand the roots of your business.  Whatever you need, is it a new website or refresh your custom built application, we got you covered. Not only that, we give you the best options at the most affordable rates possible. So, call us at Slashdot Web Development and see how we change the way you do business, and how we can make your business excel online.",
  "description" => "",
  "email" => "",
  "website" => ""
}

org <<

{
  "_id" => "57bdd48e4811f5874500024f",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Paul Mitchell The School Spokane",
  "phone" => "(509) 924-7454",
  "alias" => "pmtsspokane",
  "user_id" => "57bdd48e4811f5874500024c",
  "email" => "",
  "website" => "",
  "mission_title" => "Paul Mitchell The School Spokane",
  "mission_statement" => "Located at 15303 E. Sprague Ave Suite C, Spokane Valley\r\n\r\nPaul Mitchell Beauty School Spokane occupies a 12,000 square-foot building near the bustling eastern border of Washington. Our beauty school and salon has a modern and open feel, and our talented team has over 79 years of combined experience in the beauty industry. \r\n\r\nThis has helped our school build a proven track record of turning passionate Future Professionals into successful cosmetologists and instructors. Nothing makes us happier than seeing our students reach their goals!",
  "description" => ""
}

org <<

{
  "_id" => "57e23682e262db3eeb0173ba",
  "emailer_description" => "Contact us for => Amazon Data Entry Services, Amazon Product Listing Services, ecommerce Product Data Entry, Amazon Product Listing Optimization Services, Catalog data entry services, White Pages Data Entry Services, Vpn Data Entry, Higher-Ed eBook Conversion Services, STM conversion Services, DocBook Conversion Services, TEI XML Creation Services, nopCommerce customization, HTML5 App Development, Android Games Development",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "SunTecIndia",
  "phone" => "+1 219 232 6883",
  "alias" => "suntecindia",
  "user_id" => "57e23682e262db3eeb0173b7",

  "mission_title" => "",
  "mission_statement" => "",
  "description" => "<p>SunTec India is a reliable IT Outsourcing Company, holding extensive expertise in <a href=\"https =>//www.suntecindia.com/data-entry-services.html\">data entry services</a> and data management, eCommerce product data management and advanced <a href=\"https =>//www.suntecindia.com/android-application-development-services-company.html\">application development</a> &amp; cloud solutions. We are a resource pool of well-trained and experienced professionals, supporting businesses of all shapes and sizes in building websites and enhancing online presence. </p>\n\n<p>Our experts assist right from designing and developing customized websites and mobile apps, populating eCommerce stores, to promoting them through search engine optimization, managing inventory and providing back office support. Catering to diverse industry verticals such as eCommerce, educational institutions, real estate, legal, marketing firms, etc., we hold experience of more than 15 years in serving clients in the US, UK, Germany, Canada, Australia, etc. </p>\n",
  "email" => "info@suntecindia.com",
  "website" => "https =>//www.suntecindia.com"
}

org <<

{
  "_id" => "57fa916588682e9540002f4b",
  "emailer_description" => "",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "user_id" => "57fa916588682e9540002f48",
  "alias" => "krwlawyers43",
  "name" => "Phil Bernal Slip And Fall Lawyer",
  "email" => "businesssumbission@yahoo.com",
  "phone" => "210-490-4357",
  "website" => "http =>//www.krwlawyers.com/phil-bernal",
  "company_name" => "Phil Bernal Slip And Fall Lawyer",
  "mission_title" => "Phil Bernal Slip And Fall Lawyer",
  "mission_statement" => "",
  "description" => "<p>Phil has been Board Certified in Personal Injury Trial Law since 1998. Phil currently concentrates his practice representing victims in the areas of Personal Injury, products liability, premises liability and trucking and auto accidents.Phil is licensed to practice law in all State, District and County Courts in the State of Texas, as well as Federal District Courts for the Western District and Eastern District of Texas.Phil proudly joined Ketterman Rowland &amp; Westlund after 12 years as a sole practitioner.</p>\n\n<p>Contact Us =><br>\nAddress => 16500 San Pedro Ave #302, San Antonio, TX 78232, United States<br>\nPhone => (210) 490-4357<br>\nWebsite => <a href=\"http =>//www.krwlawyers.com/phil-bernal\">http =>//www.krwlawyers.com/phil-bernal</a></p>\n"
}

org <<

{
  "_id" => "5805190f88682ec0a800d681",
  "emailer_description" => "Sign up to receive our deals via email!",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Kootenai Technical Education Campus",
  "phone" => "(208) 712-4733",
  "alias" => "ktec",
  "user_id" => "5805190f88682ec0a800d67e",
  "mission_title" => "KTEC",
  "mission_statement" => "KTEC’s commitment is to provide industry standard training, skills for positive employment, post secondary opportunities and to personally assist all students in pathways to successful careers.\r\n\r\nKTEC a cost-free public technical high school available to all junior and senior high school students.  We are a partnership between business and industry leaders, local school districts and local manufacturers. This high school hybrid will offer dual enrollment credits, industrial certifications and a new route to success through applied skill learning. \r\n\r\n\"Real Life, Real Skills, Real Futures\"",
  "description" => "",
  "email" => "",
  "website" => "http =>//ktectraining.org/index.html"
}

org <<

{
  "_id" => "582dde6688682e8bd905fea7",
  "emailer_description" => "Black diamond engagement ring\r\nBlack diamond is very attractive due to the black color. Generally, people associate the \"Black\" color with death and grief. diamond rings However, black is also identified with authority, power, strength, certainty and passion.\r\nCouples are looking for black diamond engagement ring \r\n because black diamond rings are popular nowadays. \r\nBeautiful and striking appearance is not enough reason\r\n to choose engagement ring. Engagement is one-step closer\r\n to the wedding that could change your life. Therefore, you \r\nshould choose meaningful engagement ring for the special\r\n moment. For the black diamond, the stone carry the universal \r\nmeaning of diamond as the guardian of love\r\n",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "malik salan",
  "alias" => "malik-salan",
  "email" => "",
  "phone" => "03324821296",
  "website" => "http =>//blackdiamondengagementring.net/",
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "user_id" => "582ddde488682e076b05fe96",
  
  "mission_title" => "Black Diamond Engagement Ring",
  "mission_statement" => "Bold, mysterious and alluring, black diamonds are one of the hottest trends in fine jewelry today. Using black diamond as engagement ring, you will have the ring that could guard the love you have, powerful, sensual, and mysterious. Love between two people is private. You only need people to know that you are in love and keep the rest for yourself",
  "description" => "<p> A black diamond combines the brilliance of a<br>\n faceted diamond with dark romantic love.<br>\n The black diamond engagement ring<br>\ncan also be set in white gold, black gold and rose gold. diamond rings.</p>\n\n<p>In western world, black diamond engagement ring meaning is sophistication. Black diamond is special because black diamonds are not founded in the conventional diamond mine but in different places. If you want to have sophisticated ring for the unique woman, choose black diamond as the center of the ring. <br>\nA black diamond engagement ring <br>\nis exotic, unique, opaque and mysterious.<br>\nIs black diamond engagement ring is affordable?<br>\nDepending on whether the stone is natural or color-enhanced, clarity, and size, we can work within your budget. <br>\nBlack diamond engagement ring could be different in different part of the world.<br>\nAsian cultures are commonly related to the Chinese culture. In Chinese, black diamond is the symbol of water. Since human could not live without water, the engagement ring will show that the man could not live without the woman. diamond rings.</p>\n"
}


org <<
{
  "_id" => "583fba3188682e1f9407999c",
  "emailer_description" => "Nomers Business Services is one of the well known partners for your organization’s accounting service requirements. They offer a complete suite of Accounting, Bookkeeping, Payroll, Tax and Business Set-up services with high in-depth intelligence on what it’s really like to work within an industry. They are specialized in software setup and training and can help to set up or reorganize your business accounting system while making sure to deliver you the results you need for proper analysis of your business operations. ",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Nomers Business Services,Inc",
  "phone" => "415-827-5191",
  "alias" => "nomers-business-services-inc",
  "user_id" => "583fba3188682e1f94079999",
  
  "mission_title" => "Nomers Business Services,Inc is a Accounting and Bookkeeping Service Provider",
  "mission_statement" => "Below are the services they offer for a high-level financial, strategic solutions to their partners =>\r\n\r\nAccounting Services (In; Bay Area, San Francisco and Silicon Valley)\r\n\r\nBookkeeping Services (In; Bay Area, San Francisco and Silicon Valley)\r\n\r\nPayroll Services (In; Bay Area, San Francisco and Silicon Valley)\r\n\r\nTax Services (In; Bay Area, San Francisco and Silicon Valley)\r\n\r\nBusiness & Software Setup Solutions (In; Bay Area, San Francisco and Silicon Valley)\r\n",
  "description" => "<p>Nomers Business Services is one of the well known partners for your organization’s accounting service requirements. They offer a complete suite of Accounting, Bookkeeping, Payroll, Tax and Business Set-up services with high in-depth intelligence on what it’s really like to work within an industry. They are specialized in software setup and training and can help to set up or reorganize your business accounting system while making sure to deliver you the results you need for proper analysis of your business operations. <br>\nWhether it's an established business or a new start-up firm, Nomers Business Services’s goal is to help entrepreneurs with framing the business of their idea and to assist anyone working in a small business make smart decisions about services, products and ideas. <br>\nFrom startups to established enterprises, businesses rely on accurate and insightful financial information in order to maintain profitability and capitalize on new opportunities. Nomers Business Services has extensive experience working with companies at all stages of development to provide services to keep internal resources focused on the business.<br>\n<br>\n<strong>Below are the services they offer for a high-level financial, strategic solutions to their partners =></strong></p>\n\n<ul>\n\t<li>Accounting Services (In; Bay Area, San Francisco and Silicon Valley)</li>\n\t<li>Bookkeeping Services (In; Bay Area, San Francisco and Silicon Valley)</li>\n\t<li>Payroll Services (In; Bay Area, San Francisco and Silicon Valley)</li>\n\t<li>Tax Services (In; Bay Area, San Francisco and Silicon Valley)</li>\n\t<li>Business &amp; Software Setup Solutions (In; Bay Area, San Francisco and Silicon Valley)</li>\n</ul>\n\n<p><br>\nIn case of any requirement, shoot them an email at => contact@nomersbiz.com Visit their official website => <a href=\"http =>//nomersbiz.com\">http =>//nomersbiz.com</a>, Or Call them at => 415-827-5191</p>\n"
}


org <<
{
  "_id" => "58672f0488682e43c20078d7",
  "emailer_description" => "Welcome to Illinois Dental Careers ",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Illinois Dental Careers",
  "alias" => "illinois-dental-careers",
  "email" => "",
  "phone" => "224-246-2694",
  "website" => "http =>//www.illinoisdentalcareers.com",
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "user_id" => "58672e0d88682efd110078c6",
 
  "mission_title" => "Dental Assisting and Orthodontic Assisting School",
  "mission_statement" => "Illinois Health Careers DBA Illinois Dental Careers was approved by the Division of Private Business and Vocational Schools of the Illinois Board of Higher Education with no accreditation from institutions recognized through the U.S. Board of Education.\r\n\r\nYou can contact Illinois Board of Higher Education’s (IBHE) or visit their website for more information\r\n For any complaints to IBHE, visit this page.\r\n\r\nEnroll in our dental assistant or orthodontic assistant programs today and jump start your career in dentistry today!\r\n\r\n\r\n",
  "description" => "<p style=\"text-align => center;\"><strong><span style=\"font-size =>26px\">Welcome to Illinois Dental Careers – Dental Assisting and Orthodontic Assisting School</span></strong></p>\n\n<p>Illinois Dental Careers provides a number of dental training programs that will help you take the right step toward a professional career in the dental field. Whether you are interested in becoming a dental assistant, an orthodontic assistant or a dental administrative assistant, Illinois Dental Careers has a program that is geared toward your success. All of our dental and orthodontic assistant training programs are approved by the Illinois Board of Higher Education. Our programs are designed to provide classroom and hands-on training, in order to maximize your learning experience. Our experienced instructors will work with you to prepare you for an exciting and rewarding and life-long career in this expanding field of dentistry. We offer reasonable tuition fees and flexible class schedule. Contact us to learn how you can be an Illinois Health Careers graduate and start your dental assisting career today.</p>\n\n<p><strong>Why Choose Us?</strong></p>\n\n<p>Depending on the program in which you enroll, you will be placed with an experienced instructor who will guide you through a module-based program twice a week in the evening during the week, or on Saturdays on the weekends (visit our Academic Catalog – Academic Calendar for details). Our innovative, module-based programs are designed to make the learning process easier and more convenient for you. You can choose your start date, your pace of studying, and take advantage of our flexible attendance policy (check Academic Catalog – Attendance Policy for details)</p>\n\n<p><strong>How are We Going to Prepare You to Be a Successful Dental/Orthodontic Assistant? </strong></p>\n\n<p>You will get extensive classroom and hands-on training by experienced instructors. You will have open access to our state-of-the-art equipment and latest dental materials and supplies. You will study a comprehensive curriculum prepared by a licensed dental practitioner exclusively for you. And, at the end of your training, we will give you tips on how to prepare for your job interviews, how to write your best resume, as well as give you information about job openings in the area. Contact us now to learn how you can be an Illinois Health Careers graduate and make the first step toward your dental assisting career today!</p>\n\n<p><strong>Illinois Dental Careers</strong> is the top dental assisting school in Chicago and neighboring suburbs and the only orthodontic assisting school in Illinois.</p>\n\n<p>According to the Bureau of Labor Statistics dental assisting and orthodontic assisting jobs will experience much faster than average growth in the next 10 years => “Employment of dental assistants is projected to grow 18 percent from 2014 to 2024, much faster than the average for all occupations. Ongoing research linking oral health and general health will likely continue to increase the demand for preventive dental services.”</p>\n\n<p> </p>\n"
}


org <<

{
  "_id" => "58a49b5088682e34cf06813d",
  "emailer_description" => "I will work with you to develop a design uniquely yours. Each fine art piece and functional piece is individually crafted from the highest quality materials available on the market. My art glass and functional art glass is created exclusively with materials manufactured in the USA.\r\nSee my art at Studio 107 in Coeur d'Alene, Id. and online at www.Loriblessing.com and www.facebook.com/LoriBlessingArtGlass/photos/ ",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "organization" => true,
  "affiliate" => false,
  "name" => "Lori Blessing Kilnformed Art Glass",
  "phone" => "Please e-mail",
  "alias" => "lori-blessing-kilnformed-art-glass",
  "user_id" => "58a49b5088682e34cf06813a",
  
  "email" => "",
  "website" => "https =>//www.facebook.com/LoriBlessingArtGlass/ ",
  "mission_title" => "",
  "mission_statement" => "",
  "description" => "<p>Create a design statement in your home, office building or outside area with my unique and original kilnformed fine art glass and functional art glass tiles, lighting, cabinet knobs and pulls, door panels and clocks, etc. My goal is to help you add high impact design into the natural flow of your living or work area.</p>\n\n<p>I will work with you to develop a design uniquely yours. Each fine art piece and functional piece is individually crafted from the highest quality materials available on the market. My art glass and functional art glass is created exclusively with materials manufactured in the USA.<br>\nSee my art at Studio 107 in Coeur d'Alene, Id. and online at www.Loriblessing.com and www.facebook.com/LoriBlessingArtGlass/photos/</p>\n"
}


org <<

{
  "_id" => "4fb6cba308dea15b5a07768d",
  "alias" => "realteam",
  "description" => "<p> </p>\n\n<p> </p>\n", 
  "main" => true,
  "name" => "Realteam",
  "show_email" => true,
  "show_location" => true,
  "show_phone" => true,
  "show_real_name" => true,
  "show_website" => true,
  "user_id" => "4fb5538e08dea13806000009",
  "website" => "http =>//realteamcda.com",
  "affiliate" => false,
  "email" => "",
  "phone" => "208.655.0292",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Realteam Real Estate Center",
  "mission_statement" => "A portion of EVERY purchase made on this page supports North Idaho Stem Charter Academy and their fundraising efforts.  \r\n\r\nYou can save money doing what you're already doing every day and help a local cause on our area at the same time!  Browse deals below and enjoy great savings!  Browse! Purchase! Save! Share! Repeat!\r\n"
}


org <<

{
  "_id" => "5592f256f02cbec07b002f8f",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Rathdrum Lions Club",
  "alias" => "rathdrum-lionsclub",
  "email" => "lionlonnie@gmail.com",
  "phone" => "(208) 777-5157",
  "website" => "http =>//www.e-clubhouse.org/sites/rathdrum",
  "main" => true,
  "user_id" => "5282ba8df02cbed3de004efc",
 
  "description" => "",
  "affiliate" => false,
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => true,
  "mission_title" => "Rathdrum Lions Club",
  "mission_statement" => "A portion of EVERY purchase on this page supports the Rathdrum Lions Club and our community fundraising efforts.  \r\n\r\nWe are a non-profit organization that donates money, time and labor to help people in need and support our community.  All money raised from our fundraisers and donations is used to provide scholarships, glasses and exams, hearing aids and exams, cornea transplants, help with medical expenses and more!  \r\n\r\nOur fund raising activities include a Car Show, Auctions, Raffles, seek grants, White Cane Days and ask for general monetary donations.  Check out the deals below to help support our cause and the community.  Browse! Purchase! Save! Share! Repeat!\r\n"
}


org.each do |o|

  user = User.find_by_old_mongo_id(o["user_id"])
  if user.blank?
    user = User.find_by_email(o["email"])
  end

  if user.present?
    user.user_type = "organization"
    user.save

    profile = Profile.new
    profile.user_id = user.id
    profile.name = o["name"]
    profile.profile_type = user.user_type
    profile.description = o["description"].present? ? o["description"] : o["mission_statement"]
    profile.website = o["website"]
    profile.phone_number = o["phone"]
    profile.save(validate: false)
  end

end

