namespace :send_welcome_email do

  desc "Send Welcome Email to all users"
  task :done => :environment do
  	User.find_each do |u|
  		NotificationMailer.welcome_email(u).deliver_now
  	end
  end

end