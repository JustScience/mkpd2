namespace :import_deals do

  desc "Create Deals"
  task :update => :environment do


deals = []

deals <<
{
  "_id": "51919284f02cbeeaf100004c",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304033940,
  "updated_at": 1413304033940,
  "expires_at": 1404264060000,
  "last_reminder": "",
  "user_id": "514db52bf02cbeb3fd0007e5",
  "activated_at": "",
  "title": "Grapevine Discount Card",
  "offer": "Buy the Grapevine Discount Card good at over 300 merchants in Spokane and Coeur d Alene. Card will be sent in the mail. ",
  "terms": "Grapevine Discount Card is good for one full year.",
  "profile_id": "5152f542f02cbecc3f0000cd",
  "listing_ids": [
    
  ],
  "category_id": "4f7d215774dbb02de300030c",
  "value": "25.0",
  "price": "15.0",
  "limit": 20,
  "limit_per_customer": 2,
  "image": {
    "_id": "543d4ee1f02cbea0d0000004",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee1f02cbe3cae000003",
    "attached_width": 850,
    "attached_height": 315,
    "attached_size": 230953,
    "attached_name": "Spokane.jpg",
    "updated_at": 1413304033940,
    "created_at": 1413304033940
  }
}
deals << {
  "_id": "53406c7ff02cbe7089000c6e",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034002,
  "updated_at": 1413304034002,
  "expires_at": 1428266760000,
  "last_reminder": "",
  "user_id": "51201b95f02cbefca500073f",
  "activated_at": "",
  "title": "Expert Advice Introductory Offer",
  "offer": "Three hours of consulting at 50% off.",
  "terms": "Call to set up your schedule.",
  "profile_id": "51426a23f02cbecae80001f6",
  "listing_ids": [
    
  ],
  "category_id": "4f7d215774dbb02de30002fb",
  "value": "150.0",
  "price": "75.0",
  "limit": 1,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53406c7ff02cbe7089000c6f",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -116.7757,
        47.66655
      ],
      "address": {
        "_id": "53406c7ff02cbe7089000c70",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee1f02cbea0d0000006",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee1f02cbe3cae000005",
    "attached_width": 573,
    "attached_height": 350,
    "attached_size": 45618,
    "attached_name": "nexttofall.JPG",
    "updated_at": 1413304034002,
    "created_at": 1413304034002
  }
}
deals << {
  "_id": "5345826bf02cbe963600e3bc",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034034,
  "updated_at": 1413304034034,
  "expires_at": 1404925920000,
  "last_reminder": "",
  "user_id": "534496eaf02cbeb95f00c3a7",
  "activated_at": "",
  "title": "Spokane Therapeutic & Medical Massage",
  "offer": "Choose from Three Options\r\n\r\n*  $37 for one deep-tissue massage ($75 value\r\n*  $68 for two deep-tissue massages ($150 value\r\n*  $89 for three deep-tissue massages ($225 value",
  "terms": "Expires 90 days after purchase.   Limit 1 per person.   Valid only for option purchased.   Limit 1 per visit.   Appointment required.  24hr cancellation notice required.    Consultation required; non-candidates and other refund requests  will be honored before service provided.  Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.  SEE THE RULES that apply to all deals.",
  "profile_id": "534496eaf02cbeb95f00c3a6",
  "listing_ids": [
    "534499ecf02cbe4edc00c406"
  ],
  "category_id": "4f7d215f74dbb02de30006dd",
  "value": "75.0",
  "price": "37.0",
  "limit": 1,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5345826bf02cbe963600e3bd",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -117.239945130435,
        47.6797647608696
      ],
      "address": {
        "_id": "5345826bf02cbe963600e3be",
        "country": "US",
        "street": "2510 N. Pines ",
        "suite": "207",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99206"
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d0000008",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae000007",
    "attached_width": 313,
    "attached_height": 461,
    "attached_size": 29012,
    "attached_name": "massage.JPG",
    "updated_at": 1413304034033,
    "created_at": 1413304034033
  }
}
deals << {
  "_id": "534d70c4f02cbe3c87005377",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304033787,
  "updated_at": 1413304033787,
  "expires_at": 1405443660000,
  "last_reminder": "",
  "user_id": "534d5a17f02cbe0440005143",
  "activated_at": "",
  "title": "One Hour Tax Consultation",
  "offer": "One hour tax consultation",
  "terms": "Expires in 90 days",
  "profile_id": "534d5a17f02cbe0440005142",
  "listing_ids": [
    "534d6a6cf02cbe474a00525c",
    "534d620cf02cbe08aa00430f"
  ],
  "category_id": "4f7d215a74dbb02de300044b",
  "value": "65.0",
  "price": "10.0",
  "limit": 1,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "534d70c4f02cbe3c87005378",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -116.78897,
        47.718227
      ],
      "address": {
        "_id": "534d70c4f02cbe3c87005379",
        "country": "US",
        "street": "254 W Hanley Ave",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    },
    {
      "_id": "534d70c4f02cbe3c8700537a",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.790487567553,
        47.729932026927
      ],
      "address": {
        "_id": "534d70c4f02cbe3c8700537b",
        "country": "US",
        "street": "254 W Hanley Ave",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee1f02cbea0d0000002",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee1f02cbe3cae000001",
    "attached_width": 600,
    "attached_height": 590,
    "attached_size": 61942,
    "attached_name": "basiccalculator.jpg",
    "updated_at": 1413304033786,
    "created_at": 1413304033786
  }
}
deals << {
  "_id": "534d8330f02cbe2a49005475",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034159,
  "updated_at": 1413304034159,
  "expires_at": 1402878600000,
  "last_reminder": "",
  "user_id": "51547fadf02cbed1a1000041",
  "activated_at": "",
  "title": "Plant your own Herb Garden!",
  "offer": "5 ea. of our 4\" Herbs of your Choice for only $10.00, normally $19.99!",
  "terms": "Offer expires 06/15/2014",
  "profile_id": "5154838df02cbe9c67000056",
  "listing_ids": [
    "515485dff02cbe9c6700005c"
  ],
  "category_id": "4f7d216074dbb02de300074d",
  "value": "19.99",
  "price": "10.0",
  "limit": 1,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "534d8330f02cbe2a49005476",
      "geocode_source": "map_quest",
      "geocoded_city_level": false,
      "point": [
        -116.8754970846558,
        47.81508653909255
      ],
      "address": {
        "_id": "534d8330f02cbe2a49005477",
        "country": "US",
        "city": "Rathdrum",
        "postal_code": "83858",
        "region": "ID",
        "street": "15825 N. Westwood Dr., (Behind Super 1,",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d000000a",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae000009",
    "attached_width": 1024,
    "attached_height": 977,
    "attached_size": 305178,
    "attached_name": "Westwood Gardens.jpg",
    "updated_at": 1413304034158,
    "created_at": 1413304034158
  }
}
deals << {
  "_id": "535ac199f02cbeef0b002005",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034226,
  "updated_at": 1413304034226,
  "expires_at": 1406318640000,
  "last_reminder": "",
  "user_id": "535aba6cf02cbe3b50001b94",
  "activated_at": "",
  "title": "Getaway Vacation Buy Fabulous Branson !",
  "offer": "Considered to be one of the top vacation spots in the United States, Branson, Missouri has something for the whole family. Live entertainment everyday and night, hiking, golfing, museums, theme parks, boating, fishing...you name it Branson's got it.\r\n\r\nAnd right now Resort Travel Center is offering a great vacation right in the heart of \"The Live Music Capital of the World\" for just $99\r\n \r\nThis incredible package includes:\r\n3 Days / 2 Nights Lodging in a Deluxe Hotel\r\n2 VIP Show Tickets\r\n$25 Dinner Certificate",
  "terms": "This package is part of a resort promotion. We are able to offer low priced, quality vacations in exchange for guests taking a small amount of time to view the hosting resorts vacation opportunities. Guests will be required to attend a presentation lasting approximately 90 minutes on the benefits of Vacation Ownership or Membership at your hosting resort. Qualifications of this package must be met in order to take advantage of this offer. If you fail to attend presentation or qualify you agree to pay the difference of our package price and actual retail price. There is absolutely no obligation to purchase whatsoever! Please review the Resort Qualifications below and if you have any questions please call for clarification. There are no blackout dates on this package; however, hotel reservations are based on availability and some dates might be available at a higher cost. This promotion is not intended for group travel. The resort considers anyone traveling together or meeting at resort as Group Travel. ONLY ONE PACKAGE MAY BE PURCHASED AND USED PER YEAR. All travel documents will be sent via e-mail, unless otherwise arranged by customer. We offer to mail confirmations with map via first class mail at no cost when requested. Guests agree to be responsible for any additional room charges(pay-per-view, long distance, etc..\r\n\r\nBy giving us your personal information, you consent to being contacted by telephone or email, regardless of you belonging to any state, federal, or internal DNC lists. Specifically, you agree that by providing this information to us, you affirmatively give us permission to contact you at the telephone number provided.\r\n\r\nQualifications:\r\n-Must be a citizen or permanent resident of the U.S. and over the age of 25.\r\n-If married, engaged, or similarly committed, couples must attend the entire presentation together. -Single Men and Women may attend if they meet all other qualifications. \r\n-Couples must show IDs with same address at time of check in.\r\n-Must have a minimum annual combined household income of $50,000 or higher.\r\n-Need two forms of ID at check in. Drivers license and major Credit Card . DEBIT CARDS NOT ACCEPTED.\r\n-Only one package per family or group.                                                                                           \r\n-Must not have attended a presentation at this resort in the last 12 months.          \r\n-Must not be traveling in an RV.\r\n\r\nRe-scheduling Your Reservation:\r\nAs long as you call us 3 working Days before your arrival date, we can reschedule your vacation once for free. Any other reschedules could incur a cost of $29. Please note that if you don’t give us at least 3 business days notice our cancellation policy will apply. (See below.\r\n\r\nTo reschedule call us at 888-653-1025. If calling after office hours or emailing, please make sure to include:\r\n• your name\r\n• your phone number\r\n• the original date of arrival and departure\r\n• your new date of arrival and departure\r\n\r\nCancellation Policy:\r\nVacation packages are non refundable but may be saved for use to any of our destinations within 6 months. All qualifications for that destination must be met.They may also be transferable if the person that it is being transferred to meets the vacations qualifications.\r\n\t \r\n  \t\r\n",
  "profile_id": "535aba6cf02cbe3b50001b93",
  "listing_ids": [
    "535abcfaf02cbe3c12001fba"
  ],
  "category_id": "4f7d216474dbb02de30008e0",
  "value": "255.0",
  "price": "99.0",
  "limit": 10,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "535ac199f02cbeef0b002006",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -93.280045,
        36.628274
      ],
      "address": {
        "_id": "535ac199f02cbeef0b002007",
        "country": "US",
        "street": "817 St Hwy 165",
        "suite": "",
        "city": "Branson",
        "region": "MO",
        "postal_code": "65616"
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d000000c",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae00000c",
    "attached_width": 496,
    "attached_height": 391,
    "attached_size": 1506,
    "attached_name": "Untitled.png",
    "updated_at": 1413304034226,
    "created_at": 1413304034226
  }
}
deals << {
  "_id": "535ee4e2f02cbed6c70003f5",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034440,
  "updated_at": 1413304034440,
  "expires_at": 1406592000000,
  "last_reminder": "",
  "user_id": "535edc26f02cbea8190003ea",
  "activated_at": 1398728212869,
  "title": "Summerize your RV! 47% DISCOUNT! SAVE $40!",
  "offer": "It's TIME!  Get your RV ready for SUMMER! *Gas Check *Light Check *Water Flush *Water Pressure Test *Light & Check Appliances  *Full Tire Check   Let our experience keep you on the RV Trail and having fun with your friends and family! CALL For Appointment!",
  "terms": "ONLY one Voucher per Customer & Job.  Expires 90 days after Purchase! Call Earl (208 683-1735 for Appointment!",
  "profile_id": "535edc26f02cbea8190003e9",
  "listing_ids": [
    "535eddccf02cbe27790003d1"
  ],
  "category_id": "4f7d216874dbb02de3000a98",
  "value": "85.0",
  "price": "45.0",
  "limit": 50,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "535ee4e2f02cbed6c70003f6",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        0,
        47.943718
      ],
      "address": {
        "_id": "535ee4e2f02cbed6c70003f7",
        "country": "US",
        "city": "Athol",
        "postal_code": "83801",
        "region": "ID",
        "street": "30480 North HWY 95",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d0000010",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae000011",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 303040,
    "attached_name": "Class A-2.JPG",
    "updated_at": 1413304034440,
    "created_at": 1413304034440
  }
}
deals << {
  "_id": "535ee756f02cbe283a00040c",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034292,
  "updated_at": 1413304034292,
  "expires_at": 1406851200000,
  "last_reminder": "",
  "user_id": "535edc26f02cbea8190003ea",
  "activated_at": 1398728545303,
  "title": "2 HOURS RV Labor for the Price of 1! SAVE 50% or $85!",
  "offer": "SAVE $85 as your buying 2 Hours of RV Maintenance for just ONE Hour! 50% Discount! PLEASE CALL for Appointment after you have purchased Deal Voucher!",
  "terms": "Expires 90 Days after Purchase Date. ONLY ONE per customer or RV.  Must Call for Appointment!  Call Earl: (208 683-1735",
  "profile_id": "535edc26f02cbea8190003e9",
  "listing_ids": [
    "535eddccf02cbe27790003d1"
  ],
  "category_id": "4f7d216874dbb02de3000a98",
  "value": "170.0",
  "price": "85.0",
  "limit": 50,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "535ee756f02cbe283a00040d",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        0,
        47.943718
      ],
      "address": {
        "_id": "535ee756f02cbe283a00040e",
        "country": "US",
        "city": "Athol",
        "postal_code": "83801",
        "region": "ID",
        "street": "30480 North HWY 95",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d000000e",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae00000e",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 305676,
    "attached_name": "Pull Trailer-2.JPG",
    "updated_at": 1413304034292,
    "created_at": 1413304034292
  }
}
deals << {
  "_id": "53625f78f02cbe2f32005620",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034576,
  "updated_at": 1485233167510,
  "expires_at": 1485936000000,
  "last_reminder": "",
  "user_id": "4fb5539b08dea138060003f1",
  "activated_at": 1398956008658,
  "title": "3 Tanning Sessions | 20 Minute Bed",
  "offer": " 40% DISCOUNT!  3 VISITS-20 Minute Tan Bed Time for Just $9.  REGULAR PRICE $15  Get ready for the summer beach! NO Membership or Start-Up Fees!  7 Bed Location to meet all your needs and desires!  CALL for Appointment (208 457-9700\r\n\r\nMUST SHOW your MarketPad DEAL before you tan session starts!",
  "terms": "ONLY one purchase per customer!",
  "profile_id": "4fb7259d08dea126b9004824",
  "listing_ids": [
    "4fb7259d08dea126b9004823"
  ],
  "category_id": "4f7d215974dbb02de30003cb",
  "value": "15.0",
  "price": "9.0",
  "limit": 40,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53625f78f02cbe2f32005621",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.8923077246066,
        47.71665274085393
      ],
      "address": {
        "_id": "53625f78f02cbe2f32005622",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "900 N. Highway 41,",
        "suite": "9,"
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d0000014",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae000016",
    "attached_width": 300,
    "attached_height": 301,
    "attached_size": 36877,
    "attached_name": "tan1.jpg",
    "updated_at": 1413304034576,
    "created_at": 1413304034576
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "536261cff02cbe522600568b",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034534,
  "updated_at": 1485233179866,
  "expires_at": 1485936000000,
  "last_reminder": "",
  "user_id": "4fb5539b08dea138060003f1",
  "activated_at": 1398956626726,
  "title": "(3 Tanning Sessions | 15 Minute Bed",
  "offer": "38% DISCOUNT!!!! (3 Visits 15 minute Tan Bed Time for just $15! Regular Price is $24!  SAVE $9!  Looking your best this summer is EASY at Club Tan in Post Falls!  Sunkissed Faces & Relaxed Bodies!  CALL for Appointment (208 457-9700\r\n\r\nMUST SHOW your MarketPad DEAL before your tan session starts!",
  "terms": "ONLY one purchase per customer!",
  "profile_id": "4fb7259d08dea126b9004824",
  "listing_ids": [
    "4fb7259d08dea126b9004823"
  ],
  "category_id": "4f7d215974dbb02de30003cb",
  "value": "24.0",
  "price": "15.0",
  "limit": 40,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "536261cff02cbe522600568c",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.8923077246066,
        47.71665274085393
      ],
      "address": {
        "_id": "536261cff02cbe522600568d",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "900 N. Highway 41,",
        "suite": "9,"
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d0000012",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae000014",
    "attached_width": 250,
    "attached_height": 250,
    "attached_size": 133318,
    "attached_name": "tan6.png",
    "updated_at": 1413304034533,
    "created_at": 1413304034533
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "5362639bf02cbeabe50053ba",
  "active": false,
  "vouchers_count": 2,
  "created_at": 1413304034624,
  "updated_at": 1464107455398,
  "expires_at": 1425110400000,
  "last_reminder": "",
  "user_id": "4fb5539b08dea138060003f1",
  "activated_at": 1398956963179,
  "title": "28% SAVINGS!!! (3 12 minute bed times for just $26! SAVE $10",
  "offer": "SAVE 28% off the regular price! (3 sessions on our 12 minute bed for just $26.  SAVE $10 to look your best this summer! ",
  "terms": "Offer expires 90 days after purchase. PLEASE call (208 457-9700 for appointment and SHOW your MarketPad DEAL before you tan session.",
  "profile_id": "4fb7259d08dea126b9004824",
  "listing_ids": [
    "4fb7259d08dea126b9004823"
  ],
  "category_id": "4f7d215974dbb02de30003cb",
  "value": "36.0",
  "price": "26.0",
  "limit": 20,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "5362639bf02cbeabe50053bb",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.8923077246066,
        47.71665274085393
      ],
      "address": {
        "_id": "5362639cf02cbeabe50053bc",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "900 N. Highway 41,",
        "suite": "9,"
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d0000016",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae000018",
    "attached_width": 356,
    "attached_height": 240,
    "attached_size": 101244,
    "attached_name": "tan5.jpg",
    "updated_at": 1413304034623,
    "created_at": 1413304034623
  }
}
deals << {
  "_id": "5362842ef02cbea9ae006bc5",
  "active": false,
  "vouchers_count": 0,
  "created_at": 1413304034763,
  "updated_at": 1413304034763,
  "expires_at": 1401470700000,
  "last_reminder": "",
  "user_id": "4fb553b108dea138060009fd",
  "activated_at": 1398965312739,
  "title": "Mother's Day Special",
  "offer": "\r\nMother's Day Special - Save 50% off regular prices\r\n\r\n\r\nCompany\t APM Cleaning, Carpet & Upholstery Cleaning Specialists\r\nExpiration\t May 11, 2015 8:03AM\r\nOFFER\r\nA clean carpet makes Mom happy. And remember, \"if Momma ain't happy, nobody is happy!\" Make her happy with this coupon \r\nChoose Between Two Options\r\n\r\n$120 for carpet cleaning for four rooms and one hallway (a $240 value \r\n\r\nThis service is good for rooms up to 250 square feet each and a hallway up to 100 square feet, and is valid within 11 miles of zip code 83854\r\n\r\n",
  "terms": "\r\nTERMS\r\nExpires May 11, 2014 Limit 1 per person, may buy 3 additional as gifts. Limit 1 per visit. Valid only for option purchased. Appointment required 3 days in advance. 24hr cancellation notice required. Valid only within 11 miles of 83854. Valid for up to 100 sq. ft. for one hallway and up to 250 sq. ft. for each room. Extra fees for tough stains or high traffic areas may apply.\r\nVoucher must be presented at time of service.\r\n Coupon #535fc1f3f02cbe6419000cc6",
  "profile_id": "535e8c27f02cbe3bc9003b63",
  "listing_ids": [
    "535fb9bcf02cbecd12000cbb"
  ],
  "category_id": "4f7d215974dbb02de30003e2",
  "value": "240.0",
  "price": "120.0",
  "limit": 10,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5362842ef02cbea9ae006bc6",
      "geocode_source": "postal code",
      "geocoded_city_level": true,
      "point": [
        -116.94564,
        47.720306
      ],
      "address": {
        "_id": "5362842ef02cbea9ae006bc7",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d0000018",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae00001a",
    "attached_width": 1024,
    "attached_height": 765,
    "attached_size": 387119,
    "attached_name": "image.jpg",
    "updated_at": 1413304034763,
    "created_at": 1413304034763
  }
}
deals << {
  "_id": "53628d67f02cbecb77005e5d",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304038491,
  "updated_at": 1413304038491,
  "expires_at": 1409382000000,
  "last_reminder": "",
  "user_id": "4fb553b108dea138060009fd",
  "activated_at": 1398967835069,
  "title": "Tile & Grout Cleaning Special",
  "offer": "Tile & Grout Cleaning & Sealing - Save 33% off regular prices\r\n\r\n\r\nAre your white grout lines looking not so white?  Restore them to like new condition with our Tile & Grout cleaning special.  \r\n\r\n$200 for tile & grout cleaning & sealing for 200 square feet (a $300 value \r\n\r\nThis service is valid within 15 miles of zip code 83854\r\n",
  "terms": "\r\nExpires August 30, 2014 Limit 1 per person, may buy 1 additional as gifts. Limit 1 per visit. Valid only for option purchased. Appointment required 3 days in advance. 24hr cancellation notice required. Valid only within 15 miles of 83854.  Voucher must be presented at time of service. ",
  "profile_id": "535e8c27f02cbe3bc9003b63",
  "listing_ids": [
    "535fb9bcf02cbecd12000cbb"
  ],
  "category_id": "4f7d215974dbb02de30003e7",
  "value": "300.0",
  "price": "200.0",
  "limit": 3,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53628d67f02cbecb77005e5e",
      "geocode_source": "postal code",
      "geocoded_city_level": true,
      "point": [
        -116.94564,
        47.720306
      ],
      "address": {
        "_id": "53628d67f02cbecb77005e5f",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d0000078",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae00007b",
    "attached_width": 393,
    "attached_height": 258,
    "attached_size": 40866,
    "attached_name": "image.jpg",
    "updated_at": 1413304038491,
    "created_at": 1413304038491
  }
}
deals << {
  "_id": "53629e20f02cbeb4f6005f88",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034945,
  "updated_at": 1485233070792,
  "expires_at": 1485936000000,
  "last_reminder": "",
  "user_id": "536295a6f02cbeae11006d6c",
  "activated_at": 1398972013769,
  "title": "Swedish Massage | 60 Minute",
  "offer": "ONLY $40 for one 60-minute Swedish Massage | Normally $80\r\n\r\nLone Wolf Therapies located in Spokane Valley WA\r\n(509 228-3772\r\n",
  "terms": "> Must Present Deal at your appointment.\r\n> Limit 1 per person. Appointment required.  \r\n> 24-hr cancellation notice or fee may apply. \r\n> Services must be used by the same person. ",
  "profile_id": "536295a6f02cbeae11006d6b",
  "listing_ids": [
    "536299fff02cbe758b005f03"
  ],
  "category_id": "4f7d215974dbb02de30003d1",
  "value": "80.0",
  "price": "40.0",
  "limit": 100,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53629e20f02cbeb4f6005f89",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -117.285255624812,
        47.6715581362681
      ],
      "address": {
        "_id": "53629e20f02cbeb4f6005f8a",
        "country": "US",
        "street": "8817 E. Mission Ave",
        "suite": "105",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d000001a",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae00001d",
    "attached_width": 385,
    "attached_height": 380,
    "attached_size": 45778,
    "attached_name": "massage1.jpg",
    "updated_at": 1413304034945,
    "created_at": 1413304034945
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "53629e66f02cbea5e4005f95",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304034989,
  "updated_at": 1413304034989,
  "expires_at": 1409382000000,
  "last_reminder": "",
  "user_id": "4fb553b108dea138060009fd",
  "activated_at": 1398972026890,
  "title": "Summer Carpet Cleaning Special",
  "offer": "Summer Carpet Cleaning Special - Save 40% off regular prices\r\n\r\n\r\nA clean carpet makes for a healthier indoor environment.  Your home will have a fresher aroma and feel cleaner.\r\n\r\n$231 for carpet cleaning for four rooms and one hallway (a $385 value \r\n\r\nThis service is good for rooms up to 250 square feet each and a hallway up to 100 square feet, and is valid within 15 miles of zip code 83854\r\n",
  "terms": "Expires August 30, 2014 Limit 1 per person, may buy 1 additional as a gift. Limit 1 per visit. Valid only for option purchased. Appointment required 3 days in advance. 24hr cancellation notice required. Valid only within 15 miles of 83854. Valid for up to 100 sq. ft. for one hallway and up to 250 sq. ft. for each room. Extra fees for tough stains or high traffic areas may apply.\r\nVoucher must be presented at time of service.",
  "profile_id": "535e8c27f02cbe3bc9003b63",
  "listing_ids": [
    "535fb9bcf02cbecd12000cbb"
  ],
  "category_id": "4f7d215974dbb02de30003e2",
  "value": "385.0",
  "price": "231.0",
  "limit": 6,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "53629e66f02cbea5e4005f96",
      "geocode_source": "postal code",
      "geocoded_city_level": true,
      "point": [
        -116.94564,
        47.720306
      ],
      "address": {
        "_id": "53629e66f02cbea5e4005f97",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee2f02cbea0d000001c",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee2f02cbe3cae00001f",
    "attached_width": 250,
    "attached_height": 202,
    "attached_size": 15868,
    "attached_name": "image.jpg",
    "updated_at": 1413304034989,
    "created_at": 1413304034989
  }
}
deals << {
  "_id": "5362a484f02cbe41ae006106",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035026,
  "updated_at": 1413304035026,
  "expires_at": 1430509200000,
  "last_reminder": "",
  "user_id": "5362950bf02cbea065005f5a",
  "activated_at": 1398973613337,
  "title": "BUY 1 HOUR GET 1 HOUR FREE INVESTIGATION",
  "offer": "1 HOUR FREE INVESTIGATION WITH PURCHASE OF 1 HOUR @ $60 PER HOUR",
  "terms": "VALID ANYTIME\r\nMUST PRESENT VOUCHER AT TIME OF SERVICE",
  "profile_id": "5362969bf02cbec5dd005f98",
  "listing_ids": [
    "5362a18df02cbeb9c3006e63"
  ],
  "category_id": "4f7d216274dbb02de30007e6",
  "value": "120.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5362a484f02cbe41ae006107",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5362a484f02cbe41ae006108",
        "country": "US",
        "city": "Coeur d'alene",
        "postal_code": "83815",
        "region": "ID",
        "street": "PO Box 1232",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d000001e",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000021",
    "attached_width": 950,
    "attached_height": 1024,
    "attached_size": 70217,
    "attached_name": "justice.jpg",
    "updated_at": 1413304035025,
    "created_at": 1413304035025
  }
}
deals << {
  "_id": "5367f6c4f02cbefaeb00c944",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035074,
  "updated_at": 1413304035074,
  "expires_at": 1430858100000,
  "last_reminder": "",
  "user_id": "5367e7bdf02cbeb00000d744",
  "activated_at": 1399322496579,
  "title": "BASECAMP LODGING SPECIAL!",
  "offer": "REGULAR PRICE $250 PER NIGHT\r\nBUY 2 NIGHTS FOR $400 SAVE $100!  ",
  "terms": "CALL FOR RESERVATIONS DATES! (208 818-8392",
  "profile_id": "5367e7bdf02cbeb00000d743",
  "listing_ids": [
    "5367f21af02cbe3a3500c96f"
  ],
  "category_id": "4f7d215774dbb02de3000314",
  "value": "500.0",
  "price": "400.0",
  "limit": "",
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "5367fd51f02cbe381e00cb48",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.9292082,
        47.6899149
      ],
      "address": {
        "_id": "5367fd51f02cbe381e00cb49",
        "country": "US",
        "street": "Tanglewood Drive",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000020",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000023",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 171728,
    "attached_name": "Black Bay 003 (Large.jpg",
    "updated_at": 1413304035074,
    "created_at": 1413304035074
  }
}
deals << {
  "_id": "5368420cf02cbeef4700e1e3",
  "active": false,
  "vouchers_count": 0,
  "created_at": 1413304035110,
  "updated_at": 1418147958477,
  "expires_at": 1430877240000,
  "last_reminder": "",
  "user_id": "53683749f02cbeca4900e060",
  "activated_at": 1399392647526,
  "title": "CAPTAIN COOK NAUTICAL CLOTHING & ACCESSORIES",
  "offer": "BUY ANY ITEMS OF CLOTHING & ACCESSORIES THAT TOTAL $75 OR MORE AND RECEIVE 20% DISCOUNT!",
  "terms": "",
  "profile_id": "53683749f02cbeca4900e05f",
  "listing_ids": [
    "53683926f02cbe104b00e2d8"
  ],
  "category_id": "4f7d216974dbb02de3000af2",
  "value": "75.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "5368420cf02cbeef4700e1e4",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        0,
        47.678798
      ],
      "address": {
        "_id": "5368420cf02cbeef4700e1e5",
        "country": "US",
        "street": "PO BOX 702",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83816"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000022",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000025",
    "attached_width": 300,
    "attached_height": 300,
    "attached_size": 11425,
    "attached_name": "anchor.png",
    "updated_at": 1413304035110,
    "created_at": 1413304035110
  }
}
deals << {
  "_id": "537290b3f02cbe3035009104",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035272,
  "updated_at": 1413304035272,
  "expires_at": 1408086000000,
  "last_reminder": "",
  "user_id": "536c50b4f02cbe79ae001014",
  "activated_at": "",
  "title": "$35 for a Two-Hour Luxury Shellac Mani-Pedi Package at Salon Lusso ($85 Value",
  "offer": "Long, healthy nails can help in tasks such as scratching an itch or retrieving a wedding ring from inside your pet snake.  Get in there with this Deal.\r\n\r\n*    $35 for a two-hour luxury Shellac mani-pedi package (an $85 value\r\n*    Luxury shellac mani-pedi\r\n*    Cooling gel eye mask\r\n*    Hand and foot paraffin treatment\r\n*    Aromatherapy lavender warming neck wrap\r\n\r\nComplimentary glass of house red or white wine or a treat from the coffee and refreshment bar Using specially formulated Shellac lacquer, nail technicians layer nails with vibrant color before curing the polish to a quick-dry, high-shine finish beneath a UV lamp. This process strengthens the polish as well, which means nails remain chip-free for up to two weeks. The mineral-packed paraffin dip drives moisture deep into skin, relieving parched skin and leaving hands soft. Mani-pedi packages can be booked in groups of up to 4 people.\r\n\r\nSalon Lusso\r\n\r\nIn a cozy alcove lit by a chandelier, clients recline on overstuffed pillows while their feet soak in a warm bath. Nearby, a nail technician leans over a set of freshly lacquered nails, and the click of a blow dryer sends a wash of warm air across a new hairdo. This flurry of activity is typical of Salon Lusso—no surprise given the vast list of services available to men and women alike. Possible treatments include spray tanning, perms, and acrylic nail art painted with tiny likenesses of Johnny Futurepresident, an up-and-coming politician who claims to have no interest in becoming president. On Saturdays, customers can enjoy complimentary muffins and mimosas.\r\nShellac polish dries quickly beneath a UV lamp, curing to a mirror-shine finish that stays chip free for up to two weeks.",
  "terms": "Expires 120 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Limit 1 per visit. Appointment required. 24-hr cancellation notice required. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.\r\n\r\nParties of up to 4 people are welcome.",
  "profile_id": "536c50b4f02cbe79ae001013",
  "listing_ids": [
    "5372552ef02cbe067f0099a5"
  ],
  "category_id": "4f7d215374dbb02de3000164",
  "value": "85.0",
  "price": "35.0",
  "limit": 100,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "537290b3f02cbe3035009105",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.786574,
        47.759036
      ],
      "address": {
        "_id": "537290b3f02cbe3035009106",
        "country": "US",
        "street": "9424 N. Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000028",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae00002b",
    "attached_width": 600,
    "attached_height": 450,
    "attached_size": 23697,
    "attached_name": "Pink Nails.jpg",
    "updated_at": 1413304035271,
    "created_at": 1413304035271
  }
}
deals << {
  "_id": "5372ce62f02cbe9e91008f4c",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035147,
  "updated_at": 1413304035147,
  "expires_at": 1413183600000,
  "last_reminder": "",
  "user_id": "5372ac10f02cbe732d008c9b",
  "activated_at": 1400032901263,
  "title": "$135-Supreme Interior & Exterior Detail for a sedan (a $299 value.  SAVE 55%",
  "offer": "Interior and exterior detail with more than 40 different steps, including a hand wash, clay-bar treatment, steam clean, and shampoo for your precious sedan!  \r\n\r\nTrusted Detailing's technicians have detailed everything from automobiles to Air Force ONE!",
  "terms": "Expires 180 Days after PURCHASE DATE.  Appointment Required. 24 Hour Advance Notice Required.  LIMIT (1 per person, May buy (1 additional as a GIFT.  Limit (1 per visit. Valid only for option purchased. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "5372ac10f02cbe732d008c9a",
  "listing_ids": [
    "5372cb53f02cbecb3e0094a7"
  ],
  "category_id": "4f7d216774dbb02de3000a67",
  "value": "299.0",
  "price": "135.0",
  "limit": 50,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5372ce62f02cbe9e91008f4d",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -117.411147564356,
        47.6916548019802
      ],
      "address": {
        "_id": "5372ce62f02cbe9e91008f4e",
        "country": "US",
        "street": "3721 North Division",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99207"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000024",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000027",
    "attached_width": 800,
    "attached_height": 600,
    "attached_size": 69839,
    "attached_name": "detail-4.jpg",
    "updated_at": 1413304035147,
    "created_at": 1413304035147
  }
}
deals << {
  "_id": "5372cffbf02cbeeb1900a336",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035196,
  "updated_at": 1413304035196,
  "expires_at": 1413183600000,
  "last_reminder": "",
  "user_id": "5372ac10f02cbe732d008c9b",
  "activated_at": 1400033490222,
  "title": "$159 For a Supreme Detail for a SUV, TRUCK, or VAN! ($450 Value SAVE 65%",
  "offer": "$159 for a Supreme Detail for a SUV, Truck, or Van (a $450 Value! SAVE 65%\r\nInterior and Exterior detail with more than 40 different steps, including:\r\n*Hand Wash\r\n*Wax & Buff\r\n*Clay-Bar Treatment\r\n*Tire Dressing & Wheel-Well Cleaning\r\n*Interior & Trunk Vacuuming\r\n*Interior Shampoo & Steam Cleaning\r\n",
  "terms": "Expires 180 Days after PURCHASE DATE.  Appointment Required. 24 Hour Advance Notice Required.  LIMIT (1 per person, May buy (1 additional as a GIFT.  Limit (1 per visit. Valid only for option purchased. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "5372ac10f02cbe732d008c9a",
  "listing_ids": [
    "5372cb53f02cbecb3e0094a7"
  ],
  "category_id": "4f7d216774dbb02de3000a67",
  "value": "450.0",
  "price": "159.0",
  "limit": 50,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5372cffbf02cbeeb1900a337",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -117.411147564356,
        47.6916548019802
      ],
      "address": {
        "_id": "5372cffbf02cbeeb1900a338",
        "country": "US",
        "street": "3721 North Division",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99207"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000026",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000029",
    "attached_width": 440,
    "attached_height": 330,
    "attached_size": 36989,
    "attached_name": "detail-1.jpg",
    "updated_at": 1413304035196,
    "created_at": 1413304035196
  }
}
deals << {
  "_id": "53764287f02cbef41900230b",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035351,
  "updated_at": 1413304035351,
  "expires_at": 1431794580000,
  "last_reminder": "",
  "user_id": "53762f69f02cbedb5c002137",
  "activated_at": 1400259434993,
  "title": "Childs Petti Set at The WOW Store of CDA!",
  "offer": "Child's Petti Set  (ONLY 20 Available! \r\nSizes in:   xs, sm, med, lg, xlg\r\nIncludes: Petti Skirt, Top, Headband, Wand and Wings!",
  "terms": "ONLY 1 Voucher Purchase per Customer.\r\nPromotional Value Expires 45 Days after Purchase Date.\r\nOffer Does Not include Consigned Items. \r\nStore Pickup ONLY, NO Deliveries! \r\nThe WOW Store location:  N. 6055 Government Way, CDA, ID 83815",
  "profile_id": "53762f69f02cbedb5c002136",
  "listing_ids": [
    
  ],
  "category_id": "4f7d216974dbb02de3000ae4",
  "value": "34.95",
  "price": "27.96",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53764287f02cbef41900230c",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.7862867,
        47.6980508
      ],
      "address": {
        "_id": "53764287f02cbef41900230d",
        "country": "US",
        "street": "6055 N. Government Way",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d000002a",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae00002d",
    "attached_width": 640,
    "attached_height": 1024,
    "attached_size": 174286,
    "attached_name": "Childs Petti Set Purple.jpg",
    "updated_at": 1413304035351,
    "created_at": 1413304035351
  }
}
deals << {
  "_id": "53764537f02cbe31b9002348",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035475,
  "updated_at": 1413304035475,
  "expires_at": 1404230400000,
  "last_reminder": "",
  "user_id": "53762f69f02cbedb5c002137",
  "activated_at": 1400259899575,
  "title": "Child's Tutu Set at The WOW Store in CDA!",
  "offer": "Child's Tutu Set\r\nIncludes: Tutu Skirt, Top, Headband, Wand & Wings!\r\nSizes: xs, sm, med, lg, xlg \r\n",
  "terms": "ONE Only Per Customer!\r\nPromotional Value Expires 45 Days After PURCHASE DATE!\r\nVoucher can not be applied to other items or any Consigned Items.\r\nStore Pickup ONLY, No Deliveries!\r\nThe WOW Store Location: N. 6055 Government Way, CDA, ID 83815",
  "profile_id": "53762f69f02cbedb5c002136",
  "listing_ids": [
    
  ],
  "category_id": "4f7d216974dbb02de3000ae5",
  "value": "29.95",
  "price": "23.96",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53764537f02cbe31b9002349",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.7862867,
        47.6980508
      ],
      "address": {
        "_id": "53764537f02cbe31b900234a",
        "country": "US",
        "street": "6055 N. Government Way",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d000002c",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae00002f",
    "attached_width": 640,
    "attached_height": 1024,
    "attached_size": 195254,
    "attached_name": "Green Tutu Shot.jpg",
    "updated_at": 1413304035475,
    "created_at": 1413304035475
  }
}
deals << {
  "_id": "537a323ff02cbec740004b63",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035528,
  "updated_at": 1413304035528,
  "expires_at": 1404111660000,
  "last_reminder": "",
  "user_id": "536c50b4f02cbe79ae001014",
  "activated_at": "",
  "title": "One or Three Men's Haircuts with Hot-Towel Facials at Salon Lusso (Up to 57% Off",
  "offer": "Choose Between Two Options\r\n$11 for one men’s haircut with a hot-towel facial ($23 value\r\n$30 for three men’s haircuts with hot-towel facials ($69 value\r\n*This is an Anytime Trade-in Deal\r\nFeel good about snagging this deal. If you don’t get a chance to use this deal before the expiration date, trade it in for Groupon Bucks. Simply email trade-in@groupon.com before the expiration date and we’ll take care of you. Learn More\r\n\r\nSalon Lusso\r\nIn a cozy alcove lit by a chandelier, clients recline on overstuffed pillows while their feet soak in a warm bath. Nearby, a nail technician leans over a set of freshly lacquered nails, and the click of a blow dryer sends a wash of warm air across a new hairdo. This flurry of activity is typical of Salon Lusso—no surprise given the vast list of services available to men and women alike. Possible treatments include spray tanning, perms, and acrylic nail art painted with tiny likenesses of Johnny Futurepresident, an up-and-coming politician who claims to have no interest in becoming president. On Saturdays, customers can enjoy complimentary muffins and mimosas.",
  "terms": "In a Nutshell\r\nStylist trims men's hair for a flattering new look and soothes away stress with a hot-towel facial; visit includes water, beer, or wine\r\n\r\nThe Fine Print\r\nExpires 180 days after purchase. Limit 1 per person, may buy 1 additional as gift. Limit 1 per visit. Valid only for option purchased. Appointment required. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "536c50b4f02cbe79ae001013",
  "listing_ids": [
    "5372552ef02cbe067f0099a5"
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "23.0",
  "price": "11.0",
  "limit": 50,
  "limit_per_customer": 0,
  "locations": [
    {
      "_id": "537a323ff02cbec740004b64",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.786574,
        47.759036
      ],
      "address": {
        "_id": "537a323ff02cbec740004b65",
        "country": "US",
        "street": "9424 N. Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d000002e",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000031",
    "attached_width": 300,
    "attached_height": 300,
    "attached_size": 18397,
    "attached_name": "Salon Lusso Men's salon Pic.jpg",
    "updated_at": 1413304035527,
    "created_at": 1413304035527
  }
}
deals << {
  "_id": "537a337bf02cbe63a5004a26",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035575,
  "updated_at": 1413304035575,
  "expires_at": 1404111660000,
  "last_reminder": "",
  "user_id": "536c50b4f02cbe79ae001014",
  "activated_at": "",
  "title": "Body Wave, Perm, or One or Two Keratin Smoothing Treatments with Brow or Lip Waxes at Salon Lusso (Up to 62% Off",
  "offer": "Smoothing frizzy hair provides a sense of control, even if you live on the rim of an active volcano. Get explosive good looks with this Groupon.\r\n\r\nChoose from Three Options\r\n$79 for one professional body wave or perm with shampoo, scalp massage, and one brow or lip wax ($195 value\r\n$79 for one keratin smoothing treatment with shampoo, scalp massage, and one brow or lip wax ($195 value\r\n$149 for two keratin smoothing treatments with shampoo, scalp massage, and two brow or lip waxes ($390 value\r\nPerms and body waves use a reconstructive formula that protects the cuticle during processing to provide springier, more resilient curls. Keratin smoothing treatments tame hair for up to three months and are effective on all hair types, including colored or chemically treated hair.\r\n\r\nSalon Lusso\r\nIn a cozy alcove lit by a chandelier, clients recline on overstuffed pillows while their feet soak in a warm bath. Nearby, a nail technician leans over a set of freshly lacquered nails, and the click of a blow dryer sends a wash of warm air across a new hairdo. This flurry of activity is typical of Salon Lusso—no surprise given the vast list of services available to men and women alike. Possible treatments include spray tanning, perms, and acrylic nail art painted with tiny likenesses of Johnny Futurepresident, an up-and-coming politician who claims to have no interest in becoming president. On Saturdays, customers can enjoy complimentary muffins and mimosas.",
  "terms": "In a Nutshell\r\nStylists transform locks with body waves, perms, or keratin smoothing treatments, which can be paired with brow or lip waxes\r\n\r\nThe Fine Print\r\nExpires 120 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Limit 1 per visit. Valid only for option purchased. Appointment required. 24hr cancellation notice required for appointments scheduled Tuesday-Friday, 48hr cancellation notice required for appointments scheduled on Saturday or for 2 or more people, otherwise fee up to Groupon price may apply. Promotional value must be used in 1 visit. Cannot be combined with any other offers. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "536c50b4f02cbe79ae001013",
  "listing_ids": [
    "5372552ef02cbe067f0099a5"
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "195.0",
  "price": "79.0",
  "limit": 48,
  "limit_per_customer": 0,
  "locations": [
    {
      "_id": "537a337bf02cbe63a5004a27",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.786574,
        47.759036
      ],
      "address": {
        "_id": "537a337bf02cbe63a5004a28",
        "country": "US",
        "street": "9424 N. Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000030",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000033",
    "attached_width": 600,
    "attached_height": 420,
    "attached_size": 63168,
    "attached_name": "Salon Lusso Hair pic.jpg",
    "updated_at": 1413304035575,
    "created_at": 1413304035575
  }
}
deals << {
  "_id": "537a34e8f02cbe94cd00577a",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035655,
  "updated_at": 1413304035655,
  "expires_at": 1401606060000,
  "last_reminder": "",
  "user_id": "536c50b4f02cbe79ae001014",
  "activated_at": "",
  "title": "Haircut, Conditioning, and Brow Wax with Optional All-Over Color or Partial or Full Weave at Salon Lusso (Up to 60% Off",
  "offer": "Unlike love or nephews with healthy kidneys, hair is something you can have too much of. Trim down with this Groupon.\r\n\r\nChoose from Three Options\r\n$20 for a haircut and style (up to a $25 value, deep-conditioning treatment (a $15 value, and eyebrow wax (a $10 value; up to a $50 total value\r\n$49 for everything in the first option and a partial weave or all-over color (up to a $60 value; up to a $110 total value\r\n$55 for everything in the first option and a full weave (an $80 value; up to a $130 total value\r\nThough Salon Lusso sometimes offers a discounted price online, this Groupon is still the best deal available.\r\n\r\nSalon Lusso\r\nIn a cozy alcove lit by a chandelier, clients recline on overstuffed pillows while their feet soak in a warm bath. Nearby, a nail technician leans over a set of freshly lacquered nails, and the click of a blow dryer sends a wash of warm air across a new hairdo. This flurry of activity is typical of Salon Lusso—no surprise given the vast list of services available to men and women alike. Possible treatments include spray tanning, perms, and acrylic nail art painted with tiny likenesses of Johnny Futurepresident, an up-and-coming politician who claims to have no interest in becoming president. On Saturdays, customers can enjoy complimentary muffins and mimosas.\r\n",
  "terms": "In a Nutshell\r\nFollowing a haircut and deep-conditioning treatment, stylists adorn hair with color or weaves and expertly shape brows\r\n\r\nThe Fine Print\r\nExpires 120 days after purchase. Limit 1 per person, may buy 1 additional as a gift. Limit 1 per visit. Valid only for option purchased. Reservation required. 24 hour cancellation notice required. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "536c50b4f02cbe79ae001013",
  "listing_ids": [
    "5372552ef02cbe067f0099a5"
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "50.0",
  "price": "20.0",
  "limit": 98,
  "limit_per_customer": 0,
  "locations": [
    {
      "_id": "537a34e8f02cbe94cd00577b",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.786574,
        47.759036
      ],
      "address": {
        "_id": "537a34e8f02cbe94cd00577c",
        "country": "US",
        "street": "9424 N. Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000032",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000035",
    "attached_width": 420,
    "attached_height": 270,
    "attached_size": 47657,
    "attached_name": "Sloan Lusso Hair pic 4.jpg",
    "updated_at": 1413304035655,
    "created_at": 1413304035655
  }
}
deals << {
  "_id": "537cfb5ef02cbec0bc00000f",
  "active": true,
  "vouchers_count": 1,
  "created_at": 1413304035776,
  "updated_at": 1413304035776,
  "expires_at": 1432235100000,
  "last_reminder": "",
  "user_id": "537cec15f02cbe5912006c88",
  "activated_at": 1400796043602,
  "title": "$24.99 for five day passes (a $75 value!",
  "offer": "$24.99 for five day passes (a $75 value",
  "terms": "Expires 120 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Valid only for option purchased. Limit 1 per visit. 24-hr cancellation notice required. Five day passes do not expire once redeemed. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537cec15f02cbe5912006c87",
  "listing_ids": [
    "537cef69f02cbecd53007d03"
  ],
  "category_id": "4f7d216574dbb02de300092a",
  "value": "75.0",
  "price": "24.99",
  "limit": 340,
  "limit_per_customer": 3,
  "locations": [
    {
      "_id": "537cfb5ef02cbec0bc000010",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -117.4154984142029,
        47.65480221144533
      ],
      "address": {
        "_id": "537cfb5ef02cbec0bc000011",
        "country": "US",
        "street": "202 West 2nd Avenue,",
        "suite": "2,",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000034",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae000037",
    "attached_width": 704,
    "attached_height": 423,
    "attached_size": 351336,
    "attached_name": "coupon image.png",
    "updated_at": 1413304035776,
    "created_at": 1413304035776
  }
}
deals << {
  "_id": "537cfc8ff02cbed25400001c",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035881,
  "updated_at": 1413304035881,
  "expires_at": 1432235880000,
  "last_reminder": "",
  "user_id": "537cec15f02cbe5912006c88",
  "activated_at": 1400796076755,
  "title": "$12.99 for a vertical-introduction class, which includes a one-week membership (a $35 value!",
  "offer": "$12.99 for a vertical-introduction class, which includes a one-week membership (a $35 value",
  "terms": "Expires 120 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Valid only for option purchased. Limit 1 per visit. 24-hr cancellation notice required. Reservation required. Ages 12 & up for intro classes. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537cec15f02cbe5912006c87",
  "listing_ids": [
    "537cef69f02cbecd53007d03"
  ],
  "category_id": "4f7d216574dbb02de300092a",
  "value": "35.0",
  "price": "12.99",
  "limit": 340,
  "limit_per_customer": 3,
  "locations": [
    {
      "_id": "537cfc8ff02cbed25400001d",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -117.4154984142029,
        47.65480221144533
      ],
      "address": {
        "_id": "537cfc8ff02cbed25400001e",
        "country": "US",
        "street": "202 West 2nd Avenue,",
        "suite": "2,",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000037",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae00003a",
    "attached_width": 704,
    "attached_height": 423,
    "attached_size": 351336,
    "attached_name": "coupon image.png",
    "updated_at": 1413304035880,
    "created_at": 1413304035880
  }
}
deals << {
  "_id": "537cfd8af02cbef49a000023",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304035964,
  "updated_at": 1413304035964,
  "expires_at": 1432236180000,
  "last_reminder": "",
  "user_id": "537cec15f02cbe5912006c88",
  "activated_at": 1400796090324,
  "title": "$34.99 for a vertical-introduction class with a one-month climbing membership (a $95 value!",
  "offer": "$34.99 for a vertical-introduction class with a one-month climbing membership (a $95 value",
  "terms": "Expires 120 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Valid only for option purchased. Limit 1 per visit. 24-hr cancellation notice required. Reservation required. Ages 12 & up for intro classes. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537cec15f02cbe5912006c87",
  "listing_ids": [
    "537cef69f02cbecd53007d03"
  ],
  "category_id": "4f7d216574dbb02de300092a",
  "value": "95.0",
  "price": "34.99",
  "limit": 340,
  "limit_per_customer": 3,
  "locations": [
    {
      "_id": "537cfd8af02cbef49a000024",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -117.4154984142029,
        47.65480221144533
      ],
      "address": {
        "_id": "537cfd8af02cbef49a000025",
        "country": "US",
        "street": "202 West 2nd Avenue,",
        "suite": "2,",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "543d4ee3f02cbea0d0000039",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee3f02cbe3cae00003d",
    "attached_width": 704,
    "attached_height": 423,
    "attached_size": 351336,
    "attached_name": "coupon image.png",
    "updated_at": 1413304035963,
    "created_at": 1413304035963
  }
}
deals << {
  "_id": "537d34d7f02cbed741000098",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036049,
  "updated_at": 1413304036049,
  "expires_at": 1412204400000,
  "last_reminder": "",
  "user_id": "537d2cb0f02cbe54ab000068",
  "activated_at": 1400714470790,
  "title": "$44  for your auto Air-Conditioning Tune-Up Package ($88 Value-SAVE 50%",
  "offer": "AC units are Evacuated, Cleaned, Recharged, Filled with Refrigerant, and Inspected to Ensure Peak Functioning.  OIL CHANGE ADD-ON AVAILABLE!\r\n*Visual air-conditioner inspection ( a $22.50 value.\r\n*AC Evacuation, vacuum, and recharge (a $37.50 value\r\n*Up to 2 POUNDS of R-134A refrigerant (a $20 value\r\n*Leak-detection dye for oil and UV (a $8 value.\r\n1993 & NEWER Vehicles ONLY!",
  "terms": "Expires 90 days after purchase. LIMIT 1 per person, may buy 1 additional as a gift. VALID ONLY for option purchased. Limit 1 per visit.  Appointment REQUIRED.  Vehicle must be dropped off for a 4 hour appointment. After hours pick up & drop off available. 24 hour cancellation notice REQUIRED. Valid only for vehicles 1993 and newer. Voucher does NOT include repair of broken components, but may be redeemed towards that repair. Must use full promotional value in 1 Visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537d2cb0f02cbe54ab000067",
  "listing_ids": [
    "537d2fe2f02cbebc71000080"
  ],
  "category_id": "4f7d216774dbb02de3000a5e",
  "value": "88.0",
  "price": "44.0",
  "limit": 8,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "537d34d7f02cbed741000099",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.786603893446,
        47.7685240187817
      ],
      "address": {
        "_id": "537d34d7f02cbed74100009a",
        "country": "US",
        "street": "10643 North Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d000003b",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000040",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 252516,
    "attached_name": "Engine Coolant Picture Daves Truck.JPG",
    "updated_at": 1413304036048,
    "created_at": 1413304036048
  }
}
deals << {
  "_id": "537d3693f02cbe03a40000b4",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036183,
  "updated_at": 1413304036183,
  "expires_at": 1412204400000,
  "last_reminder": "",
  "user_id": "537d2cb0f02cbe54ab000068",
  "activated_at": 1400714918891,
  "title": "$64 for Auto AC Tune-Package PLUS an Oil Change with Filter Replacement! ($125 Value",
  "offer": "AC units are Evacuated, Cleaned, Recharged, Filled with Refrigerant, and Inspected to ensure Peak Functioning. OIL CHANGE ADD-ON AVAILABLE! \r\n*Visual air-conditioner inspection ( a $22.50 value. \r\n*AC Evacuation, vacuum, and recharge (a $37.50 value \r\n*Up to 2 POUNDS of R-134A refrigerant (a $20 value \r\n*Leak-detection dye for oil and UV (a $8 value. \r\nOil Change only valid for up to 5 quarts standard oil. Extra $3.49 fee per additional quart.\r\n1993 & NEWER Vehicles ONLY!\r\n\r\n\r\n",
  "terms": "Expires 90 days after purchase. LIMIT 1 per person, may buy 1 additional as a gift. VALID ONLY for option purchased. Limit 1 per visit. Appointment REQUIRED. Vehicle must be dropped off for a 4 hour appointment. After hours pick up & drop off available. 24 hour cancellation notice REQUIRED. Valid only for vehicles 1993 and newer. Voucher does NOT include repair of broken components, but may be redeemed towards that repair. Must use full promotional value in 1 Visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537d2cb0f02cbe54ab000067",
  "listing_ids": [
    "537d2fe2f02cbebc71000080"
  ],
  "category_id": "4f7d216774dbb02de3000a5e",
  "value": "125.0",
  "price": "64.0",
  "limit": 8,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "537d3693f02cbe03a40000b5",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.786603893446,
        47.7685240187817
      ],
      "address": {
        "_id": "537d3693f02cbe03a40000b6",
        "country": "US",
        "street": "10643 North Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d000003d",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000042",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 322504,
    "attached_name": "Engine Oil Cap 2.JPG",
    "updated_at": 1413304036183,
    "created_at": 1413304036183
  }
}
deals << {
  "_id": "537fd82af02cbe38fb001136",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036292,
  "updated_at": 1485230445123,
  "expires_at": 1485936000000,
  "last_reminder": "",
  "user_id": "4fb5539308dea138060001a5",
  "activated_at": 1400887412619,
  "title": "Bedliner - Over-the-Rail! SAVE $100",
  "offer": "ONLY $350 for a Bedliner - Over-the-Rail | Normally $450\r\n\r\nCustom Truck located in Coeur d'Alene ID\r\n(208 765-4444\r\nrob@custom-truck.net\r\n\r\n> Vehicle-customization company since 1971! \r\n> Transforms Trucks & SUVs into full expressions of their owners styles!\r\n> ANY TRUCK\r\n> Get your favorite truck ready for summer work! \r\n> Protect your truck bed with our Liner!!",
  "terms": "> Promotional Value of $100 Expires 30 Days After Purchase Date! \r\n> Limit 1 per person, may buy 1 additional as gift. \r\n> Limit 1 per visit. \r\n> Valid only for option purchased. \r\n> In-Store ONLY. Not valid for sale items. \r\n> Not valid with other specials or offers. \r\n> Must use promotional value in 1 visit. \r\n> Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.  \r\n> MUST make an appointment with store manager so call ahead. \r\n> Spokane: Call (509 924-9999   CDA: Call (208 765-4444",
  "profile_id": "537fc71af02cbef1c2001059",
  "listing_ids": [
    "537fd226f02cbee6d80011d1",
    "537fcc92f02cbe71ec001189"
  ],
  "category_id": "4f7d215774dbb02de3000319",
  "value": "450.0",
  "price": "350.0",
  "limit": 5,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "537fd82af02cbe38fb001137",
      "point": [
        -116.7895391,
        47.7150691
      ],
      "address": {
        "_id": "537fd82af02cbe38fb001138",
        "country": "US",
        "street": "254 W. Kathleen Ave.",
        "suite": "",
        "city": "Coeur d 'Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    },
    {
      "_id": "537fd82af02cbe38fb001139",
      "point": [
        -117.3069313,
        47.6574996
      ],
      "address": {
        "_id": "537fd82af02cbe38fb00113a",
        "country": "US",
        "street": "7225 E. Sprague Avenue",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d000003f",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000045",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 123825,
    "attached_name": "Spray Liner 2.jpg",
    "updated_at": 1413304036292,
    "created_at": 1413304036292
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "538783169c058ebc1800011b",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036512,
  "updated_at": 1413304036512,
  "expires_at": 1404112800000,
  "last_reminder": "",
  "user_id": "537e6c7bf02cbe89990005c1",
  "activated_at": 1402442379671,
  "title": "Pamper Special -All over color with foils up to 3 colors",
  "offer": "  ALL-NUTRIENT Professional Hair color - certified-organic hair color system  that is formulated with a natural base of vegetable oils and keratin Protein.  \r\n  This all-natural formula provides gentle penetration of pigment with long-lasting color and increased manageability and shine. \r\n  All-Nutrient hair color is available in permanent, semi- and demipermanent shades, toners, fillers and temporary color and a complete range of color care products.",
  "terms": "Expires 90 days after purchase. Limit 1 per person, may buy 1 additional as a gift. Valid only for option purchased. Appointment required. 24hr cancellation notice required. Must use full promotional value in 1 visit.  Longer or denser hair may be subject to additional fee. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537e8090f02cbe2016000003",
  "listing_ids": [
    "537fc104f02cbec2090010ce"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "110.0",
  "price": "75.0",
  "limit": 15,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5388c5d49c058e8c540009ba",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.790672111111,
        47.713587
      ],
      "address": {
        "_id": "5388c5d49c058e8c540009bb",
        "country": "US",
        "street": "296 West Sunset Ave",
        "suite": "12",
        "city": "Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d0000045",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae00004b",
    "attached_width": 380,
    "attached_height": 500,
    "attached_size": 48698,
    "attached_name": "heavenlyhair3999@live.com.jpg",
    "updated_at": 1413304036511,
    "created_at": 1413304036511
  }
}
deals << {
  "_id": "5388c33f9c058e02ac0008b0",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036645,
  "updated_at": 1413304036645,
  "expires_at": 1404113100000,
  "last_reminder": "",
  "user_id": "537e6c7bf02cbe89990005c1",
  "activated_at": 1402692344809,
  "title": "Kids Spa Package ",
  "offer": "A day of Beauty for a Cutie!\r\nOverwhelmed with extra-curricular activities, schoolwork, volunteer hours, peer pressure, bullying, stress, or just plain old busy. She works hard, plays hard, and deserves to be pampered!  Heavenly Hair & Nail Salon wants to bring a spa experience to young girls and give them the time they deserve. We strive to make all girls feel beautiful, inside and out! Stop by A Heavenly Hair & Nail Salon and unwind.\r\n\r\nFor girls between the ages of 3 – 16 years old. \r\n     *  Mini Mani\r\n     *  Mini Pedi\r\n     *  Hairstyle\r\n\r\nMakes a great Birthday Gift\r\nA Heavenly Hair & Nail Salon Spa Treatment is every girls’ dream! We will make sure the Birthday Girl is pampered from head to toe!  Let the Birthday Girl relax as our Spa Technicians spoil her! We also have a very special birthday gift just waiting to be opened! ",
  "terms": "Expires 90 days after purchase. Limit 1 per person, may buy 1 additional as a gift. Valid only for option purchased. Appointment required. 24hr cancellation notice required. Must use full promotional value in 1 visit.  Longer or denser hair may be subject to additional fee. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537e8090f02cbe2016000003",
  "listing_ids": [
    "537fc104f02cbec2090010ce"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "35.0",
  "price": "20.0",
  "limit": 7,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "5388cb4d9c058ecf9600067a",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.790672111111,
        47.713587
      ],
      "address": {
        "_id": "5388cb4d9c058ecf9600067b",
        "country": "US",
        "street": "296 West Sunset Ave",
        "suite": "12",
        "city": "Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d0000047",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae00004d",
    "attached_width": 480,
    "attached_height": 600,
    "attached_size": 47226,
    "attached_name": "kids-hairstyles-kids-hairstyle-kids-hair-styles-kids-hairstyles-480x600.jpg",
    "updated_at": 1413304036645,
    "created_at": 1413304036645
  }
}
deals << {
  "_id": "5388c5939c058e1d1600076d",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036355,
  "updated_at": 1413304036355,
  "expires_at": 1402818660000,
  "last_reminder": "",
  "user_id": "537e6c7bf02cbe89990005c1",
  "activated_at": "",
  "title": "Organic Hair Color",
  "offer": "ALL-NUTRIENT Professional Hair color - certified-organic hair color system  that is formulated with a natural base of vegetable oils and keratin Protein.  \r\n  This all-natural formula provides gentle penetration of pigment with long-lasting color and increased manageability and shine. \r\n  All-Nutrient hair color is available in permanent, semi- and demipermanent shades, toners, fillers and temporary color and a complete range of color care products.",
  "terms": "Expires 90 days after purchase. Limit 1 per person, may buy 1 additional as a gift. Valid only for option purchased. Appointment required. 24hr cancellation notice required. Must use full promotional value in 1 visit.  Longer or denser hair may be subject to additional fee. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537e8090f02cbe2016000003",
  "listing_ids": [
    "537fc104f02cbec2090010ce"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "75.0",
  "price": "35.0",
  "limit": 5,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "5388c5939c058e1d1600076e",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.790672111111,
        47.713587
      ],
      "address": {
        "_id": "5388c5939c058e1d1600076f",
        "country": "US",
        "street": "296 West Sunset Ave",
        "suite": "12",
        "city": "Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d0000041",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000047",
    "attached_width": 420,
    "attached_height": 270,
    "attached_size": 47657,
    "attached_name": "Sloan Lusso Hair pic 4.jpg",
    "updated_at": 1413304036355,
    "created_at": 1413304036355
  }
}
deals << {
  "_id": "5388ca2e9c058e47f0000a0c",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036393,
  "updated_at": 1413304036393,
  "expires_at": 1402815600000,
  "last_reminder": "",
  "user_id": "537e6c7bf02cbe89990005c1",
  "activated_at": "",
  "title": "Father's Day Special",
  "offer": "Ladies treat your guy to a Father's Day Special.\r\n\r\n$25 for one Creative Haircut and Color Highlight ($50 value\r\n\r\n       \r\n     \r\n",
  "terms": "Expires 90 days after purchase. Limit 1 per person, may buy 1 additional as a gift. Valid only for option purchased. Appointment required. 24hr cancellation notice required. Must use full promotional value in 1 visit.  Longer or denser hair may be subject to additional fee. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "537e8090f02cbe2016000003",
  "listing_ids": [
    "537fc104f02cbec2090010ce"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "50.0",
  "price": "25.0",
  "limit": 5,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "5388ca2e9c058e47f0000a0d",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.790672111111,
        47.713587
      ],
      "address": {
        "_id": "5388ca2e9c058e47f0000a0e",
        "country": "US",
        "street": "296 West Sunset Ave",
        "suite": "12",
        "city": "Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d0000043",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000049",
    "attached_width": 300,
    "attached_height": 300,
    "attached_size": 18397,
    "attached_name": "Men's salon Pic.jpg",
    "updated_at": 1413304036393,
    "created_at": 1413304036393
  }
}
deals << {
  "_id": "5388ecda9c058e069e000b39",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036717,
  "updated_at": 1413304036717,
  "expires_at": 1433018280000,
  "last_reminder": "",
  "user_id": "5388e6049c058ed8e30009d4",
  "activated_at": "",
  "title": "$39 for a four-visit archery package with equipment rental (a $92 value",
  "offer": "$39 for a four-visit archery package with equipment rental (a $92 value",
  "terms": "Expires 150 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Limit 1 per visit. Valid only for option purchased. Must activate punch card prior to expiration date on your Groupon; punch card expires 1-year from activation. Reservation required for private lessons. 24hr cancellation notice required. Must be 5 or older. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "5388e6049c058ed8e30009d3",
  "listing_ids": [
    "5388e85b9c058eeb27000b11"
  ],
  "category_id": "4f7d216474dbb02de30008f4",
  "value": "92.0",
  "price": "39.0",
  "limit": 60,
  "limit_per_customer": 3,
  "locations": [
    {
      "_id": "5388ecda9c058e069e000b3a",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -117.1570315671515,
        47.62184206883384
      ],
      "address": {
        "_id": "5388ecda9c058e069e000b3b",
        "country": "US",
        "street": "3809 S. Linke Rd.",
        "suite": "",
        "city": "Greenacres ",
        "region": "WA",
        "postal_code": "99016"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d0000049",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae00004f",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 99729,
    "attached_name": "Spokane Valley Archery.jpg",
    "updated_at": 1413304036717,
    "created_at": 1413304036717
  }
}
deals << {
  "_id": "5388ed779c058ed9f7000827",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036850,
  "updated_at": 1413304036850,
  "expires_at": 1433018460000,
  "last_reminder": "",
  "user_id": "5388e6049c058ed8e30009d4",
  "activated_at": "",
  "title": "$49 for a 30-minute private lesson for up to four, including range fees, equipment rental, and shooting practice time for the remainder of the day following the lesson (up to a $122 value",
  "offer": "$49 for a 30-minute private lesson for up to four, including range fees, equipment rental, and shooting practice time for the remainder of the day following the lesson (up to a $122 value",
  "terms": "Expires 150 days after purchase. Limit 1 per person, may buy 2 additional as gifts. Limit 1 per visit. Valid only for option purchased. Must activate punch card prior to expiration date on your Groupon; punch card expires 1-year from activation. Reservation required for private lessons. 24hr cancellation notice required. Must be 5 or older. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "5388e6049c058ed8e30009d3",
  "listing_ids": [
    "5388e85b9c058eeb27000b11"
  ],
  "category_id": "4f7d216474dbb02de30008f4",
  "value": "122.0",
  "price": "49.0",
  "limit": 60,
  "limit_per_customer": 3,
  "locations": [
    {
      "_id": "5388ed779c058ed9f7000828",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -117.1570315671515,
        47.62184206883384
      ],
      "address": {
        "_id": "5388ed779c058ed9f7000829",
        "country": "US",
        "street": "3809 S. Linke Rd.",
        "suite": "",
        "city": "Greenacres ",
        "region": "WA",
        "postal_code": "99016"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d000004b",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000051",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 99729,
    "attached_name": "Spokane Valley Archery.jpg",
    "updated_at": 1413304036850,
    "created_at": 1413304036850
  }
}
deals << {
  "_id": "538919059c058ee4ca0009f7",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304036902,
  "updated_at": 1413304036902,
  "expires_at": 1412121540000,
  "last_reminder": "",
  "user_id": "53890a2d9c058ee156000bdd",
  "activated_at": 1401494011436,
  "title": "Haircut and Conditioning Treatment with Optional Highlights from Amanda Johnstun at Revive Salon & Spa (60% Off",
  "offer": "Get ready for summer! Refreshing deep-conditioning treatment prefaces a haircut to frame the face!  For just $22 Amanda Johnstun offers a Haircut & Deep Conditional Treatment on your hair.",
  "terms": "EXPIRES: 90 days after purchase. Limit 1 per person, may buy 1 additional as a gift. LIMIT 1 per visit. Valid only for option purchased.  APPOINTMENT for service REQUIRED!  24 hour cancellation notice REQUIRED! VALID ONLY with Amanda Johnstun. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "53890a2d9c058ee156000bdc",
  "listing_ids": [
    "538915d29c058e0c81000947"
  ],
  "category_id": "4f7d215374dbb02de3000164",
  "value": "55.0",
  "price": "22.0",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "538919059c058ee4ca0009f8",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.782903959184,
        47.6744129387755
      ],
      "address": {
        "_id": "538919059c058ee4ca0009f9",
        "country": "US",
        "street": "211 E. Lakeside Ave.",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d000004d",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000053",
    "attached_width": 600,
    "attached_height": 808,
    "attached_size": 68138,
    "attached_name": "dark-hair-blonde-highlights.jpg",
    "updated_at": 1413304036902,
    "created_at": 1413304036902
  }
}
deals << {
  "_id": "53891c7d9c058e7882000c63",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037161,
  "updated_at": 1413304037161,
  "expires_at": 1412121540000,
  "last_reminder": "",
  "user_id": "53890a2d9c058ee156000bdd",
  "activated_at": 1401494670615,
  "title": "$59 for Partial Highlight or All Over Color & Condition Treatment & Haircut! $144 VALUE! SAVE $85!",
  "offer": "Haircut, Condition Treatment, Partial Highlight -OR- All Over Color by Amanda Johnstun!",
  "terms": "EXPIRES: 90 Days after Purchase.  Limit 1 per person, May buy 1 additional as a Gift.  Limit 1 per visit.  Valid only for option purchased. APPOINTMENT FOR SERVICE IS REQUIRED! 24 HR CANCELLATION NOTICE IS REQUIRED. Valid only with Amanda Johnstun. Merchant is solely responsible to pruchasers for the care and quality of the advertised goods and services.",
  "profile_id": "53890a2d9c058ee156000bdc",
  "listing_ids": [
    "538915d29c058e0c81000947"
  ],
  "category_id": "4f7d215374dbb02de3000164",
  "value": "144.0",
  "price": "59.0",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "538926a19c058ead8e000cf9",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.782903959184,
        47.6744129387755
      ],
      "address": {
        "_id": "538926a19c058ead8e000cfa",
        "country": "US",
        "street": "211 E. Lakeside Ave.",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee4f02cbea0d000004f",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee4f02cbe3cae000055",
    "attached_width": 426,
    "attached_height": 524,
    "attached_size": 39174,
    "attached_name": "Hair Color Highligts3.jpg",
    "updated_at": 1413304037161,
    "created_at": 1413304037161
  }
}
deals << {
  "_id": "538e5d929c058ef5d000134a",
  "active": false,
  "vouchers_count": 1,
  "created_at": 1413304037356,
  "updated_at": 1418071608527,
  "expires_at": 1409551200000,
  "last_reminder": "",
  "user_id": "538e4d2f9c058e2308000ebe",
  "activated_at": 1401839092003,
  "title": "$10.36  (SAVE $5.59 14\" Specialty Pizza! (regular price $15.95 Hayden & Kellogg Locations",
  "offer": "ONE 14\" Wildcat Specialty Pizza for just $10.36 +Tax. SAVE $5.59 (35% Savings. ",
  "terms": "Expires Sept 1st, 2014. Limit 1 per person, May buy 1 additional as Gift. Limit 1 per table. Limit 1 per visit. Must purchase a food item listed on Voucher. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "538e4d2f9c058e2308000ebd",
  "listing_ids": [
    "538e599e9c058e44da000f50"
  ],
  "category_id": "4f7d215e74dbb02de3000651",
  "value": "15.95",
  "price": "10.36",
  "limit": 100,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "538e64f69c058e3d2b00137f",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.788028065751,
        47.7456989028098
      ],
      "address": {
        "_id": "538e64f69c058e3d2b001380",
        "country": "US",
        "street": "85 W. Prairie Shopping Center",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "538e64f69c058e3d2b001381",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.135096616219,
        47.5421399670481
      ],
      "address": {
        "_id": "538e64f69c058e3d2b001382",
        "country": "US",
        "street": "604 Bunker Ave.",
        "suite": "",
        "city": "Kellogg",
        "region": "ID",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000054",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000059",
    "attached_width": 451,
    "attached_height": 580,
    "attached_size": 92183,
    "attached_name": "Pizza Loaded4 Mushrooms.jpg",
    "updated_at": 1413304037356,
    "created_at": 1413304037356
  }
}
deals << {
  "_id": "538e5ecf9c058e65fc001355",
  "active": false,
  "vouchers_count": 1,
  "created_at": 1413304037211,
  "updated_at": 1418071151736,
  "expires_at": 1409637600000,
  "last_reminder": "",
  "user_id": "538e4d2f9c058e2308000ebe",
  "activated_at": 1401839315420,
  "title": "$13.62 for ONE 18\" Wildcat Specialty Pizza! SAVE $7.33 ( Save 35% Hayden & Kellogg Locations",
  "offer": "ONE 18\" Wildcat Specialty Pizza for just $13.62 + tax.  SAVE 35%!",
  "terms": "Expires Sept 1st, 2014. Limit 1 per person, May buy 1 additional as Gift. Limit 1 per table. Limit 1 per visit. Must purchase a food item listed on Voucher. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "538e4d2f9c058e2308000ebd",
  "listing_ids": [
    "538e599e9c058e44da000f50"
  ],
  "category_id": "4f7d215774dbb02de3000303",
  "value": "20.95",
  "price": "13.62",
  "limit": 100,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "538e64e39c058e2d80000fd5",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.788028065751,
        47.7456989028098
      ],
      "address": {
        "_id": "538e64e39c058e2d80000fd6",
        "country": "US",
        "street": "85 W. Prairie Shopping Center",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "538e64e39c058e2d80000fd7",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.135096616219,
        47.5421399670481
      ],
      "address": {
        "_id": "538e64e39c058e2d80000fd8",
        "country": "US",
        "street": "604 Bunker Ave.",
        "suite": "",
        "city": "Kellogg",
        "region": "ID",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000051",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000057",
    "attached_width": 425,
    "attached_height": 284,
    "attached_size": 58074,
    "attached_name": "Pizza Loaded.jpg",
    "updated_at": 1413304037211,
    "created_at": 1413304037211
  }
}
deals << {
  "_id": "538e7c5f9c058e4d7c000fd8",
  "active": true,
  "vouchers_count": 1,
  "created_at": 1413304037418,
  "updated_at": 1413304037418,
  "expires_at": 1414807200000,
  "last_reminder": "",
  "user_id": "538e6b579c058eccfc001429",
  "activated_at": 1401846978280,
  "title": "$50 Partial Foil, Haircut and Style! $90 VALUE  SAVE $40!",
  "offer": "SAVE $40!  Partial Foil, Haircut, and Style for $50.  Regular price is $90!",
  "terms": "FIRST time customers ONLY! Upgrades may be available upon talking with stylist.  This Voucher is good for ONE COLOR. LIMIT 1 per person, may buy 1 additional as a gift. Limit 1 per visit. Valid only for option purchased. Appointment required. 24 Hour Cancellations notice REQUIRED. Valid only with Kelly Beaudry.  Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.  Stylist does keep track of clients and has the right to refuse service if the customer tries to use a second voucher. ",
  "profile_id": "538e6b579c058eccfc001428",
  "listing_ids": [
    "538e770c9c058e857a0014a0"
  ],
  "category_id": "4f7d216574dbb02de3000936",
  "value": "90.0",
  "price": "50.0",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "538e7ca19c058e9afd000fde",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.786315,
        47.674342
      ],
      "address": {
        "_id": "538e7ca19c058e9afd000fdf",
        "country": "US",
        "street": "211 E. Lakeside",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000057",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae00005b",
    "attached_width": 863,
    "attached_height": 1024,
    "attached_size": 113404,
    "attached_name": "Asgar-Boo-Haircare-3-1-Tousled-Wild-and-Free-Hair-863x1024.jpg",
    "updated_at": 1413304037418,
    "created_at": 1413304037418
  }
}
deals << {
  "_id": "539210c6f02cbe3bec000bb7",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037458,
  "updated_at": 1416508444527,
  "expires_at": 1433574000000,
  "last_reminder": "",
  "user_id": "5391fdedf02cbecf46000aa8",
  "activated_at": 1402081605387,
  "title": "$30 Worth of Laundry for $20",
  "offer": "Gift Card!  $30.00 worth of Dry Cleaning or Coin Operated Machine usage for only $20.00!",
  "terms": "Not Redeemable for Cash, (No Cash Value.  No Change Given.  Voucher Discount Expires 120 Days After Purchase Date.  No Food or Beverages Included.  Cannot Be Combined With Other Offers.",
  "profile_id": "5391fdedf02cbecf46000aa7",
  "listing_ids": [
    "5391ffb4f02cbe3ce1000b7d"
  ],
  "category_id": "4f7d215774dbb02de300030c",
  "value": "30.0",
  "price": "20.0",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "539210c6f02cbe3bec000bb8",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -117.3704461102508,
        47.6279721903828
      ],
      "address": {
        "_id": "539210c6f02cbe3bec000bb9",
        "country": "US",
        "street": "2720 E. 29th,",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99223"
      }
    }
  ],
  "image": {
    "_id": "546e3415f02cbe7c25000038",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "546e3415f02cbe4108000001",
    "attached_width": 500,
    "attached_height": 300,
    "attached_size": 139629,
    "attached_name": "FREEEEE-Laundry_post.jpg",
    "main": false,
    "updated_at": 1416508437652,
    "created_at": 1416508437652
  }
}
deals << {
  "_id": "53921272f02cbe4a6e0009f1",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037584,
  "updated_at": 1416343757514,
  "expires_at": 1433574000000,
  "last_reminder": "",
  "user_id": "5391fdedf02cbecf46000aa8",
  "activated_at": 1402081912415,
  "title": "Down Comforter Deal",
  "offer": "Will Launder Any Size Down Comforter For Only $18.00, a $22.00 Value!",
  "terms": "No Cash Value.  No Change Given.  Voucher Discount Expires 120 Days After Purchase Date.  No Food or Beverages Included.  Cannot Be Combined With Other Offers.",
  "profile_id": "5391fdedf02cbecf46000aa7",
  "listing_ids": [
    "5391ffb4f02cbe3ce1000b7d"
  ],
  "category_id": "4f7d215774dbb02de300030c",
  "value": "22.0",
  "price": "18.0",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "53921272f02cbe4a6e0009f2",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -117.3704461102508,
        47.6279721903828
      ],
      "address": {
        "_id": "53921272f02cbe4a6e0009f3",
        "country": "US",
        "street": "2720 E. 29th,",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99223"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d000005c",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae00005f",
    "attached_width": 220,
    "attached_height": 176,
    "attached_size": 8076,
    "attached_name": "399969_307067929342509_322835116_n.jpg",
    "updated_at": 1413304037584,
    "created_at": 1413304037584
  }
}
deals << {
  "_id": "5392280df02cbec29c000c10",
  "active": true,
  "vouchers_count": 1,
  "created_at": 1413304037619,
  "updated_at": 1413304037619,
  "expires_at": 1433623020000,
  "last_reminder": "",
  "user_id": "53922124f02cbec7ac000d94",
  "activated_at": 1402087440921,
  "title": "$39.99 for one acupuncture session ($80 value",
  "offer": "$39.99 for one acupuncture session ($80 value",
  "terms": "New Clients Only.\r\nExpires 60 days after purchase. Limit 1 per person, may buy 1 additional as gift. Valid only for option purchased. Limit 1 per visit. Appointment required. 24 hr. cancellation notice required. Must sign waiver. Consultation required; non-candidates and other refund requests will be honored before service provided. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. Valid only for option purchased. Limit 1 per visit. \r\nNo Cash Value.  No Change Given.  Cannot be combined with other offers.\r\n* Note: Refund if someone is not medically stable to receive treatment.\r\nVoucher does not apply towards lost of herbal formulas",
  "profile_id": "53922124f02cbec7ac000d93",
  "listing_ids": [
    "5392228ff02cbeb858000bd7"
  ],
  "category_id": "4f7d215774dbb02de3000301",
  "value": "80.0",
  "price": "39.99",
  "limit": 20,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5392280df02cbec29c000c11",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.7829994269729,
        47.69387363207019
      ],
      "address": {
        "_id": "5392280df02cbec29c000c12",
        "country": "US",
        "street": "225 E. Locust Ave.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d000005e",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000061",
    "attached_width": 377,
    "attached_height": 196,
    "attached_size": 10202,
    "attached_name": "safe_image.jpg",
    "updated_at": 1413304037619,
    "created_at": 1413304037619
  }
}
deals << {
  "_id": "53922867f02cbebe6c000e25",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037688,
  "updated_at": 1413304037688,
  "expires_at": 1433623440000,
  "last_reminder": "",
  "user_id": "53922124f02cbec7ac000d94",
  "activated_at": 1402087530063,
  "title": "$69.99 for three acupuncture sessions ($150 value",
  "offer": "$69.99 for three acupuncture sessions ($150 value",
  "terms": "New Clients Only.\r\nExpires 60 days after purchase. Limit 1 per person, may buy 1 additional as gift. Valid only for option purchased. Limit 1 per visit. Appointment required. 24 hr. cancellation notice required. Must sign waiver. Consultation required; non-candidates and other refund requests will be honored before service provided. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.\r\nNo Cash Value.  No Change Given.  Cannot be combined with other offers.\r\n* Note: Refund if someone is not medically stable to receive treatment.\r\nVoucher does not apply towards lost of herbal formulas",
  "profile_id": "53922124f02cbec7ac000d93",
  "listing_ids": [
    "5392228ff02cbeb858000bd7"
  ],
  "category_id": "4f7d215774dbb02de3000301",
  "value": "150.0",
  "price": "69.99",
  "limit": 20,
  "limit_per_customer": 0,
  "locations": [
    {
      "_id": "53922867f02cbebe6c000e26",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.7829994269729,
        47.69387363207019
      ],
      "address": {
        "_id": "53922867f02cbebe6c000e27",
        "country": "US",
        "street": "225 E. Locust Ave.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000061",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000063",
    "attached_width": 377,
    "attached_height": 196,
    "attached_size": 10202,
    "attached_name": "safe_image.jpg",
    "updated_at": 1413304037688,
    "created_at": 1413304037688
  }
}
deals << {
  "_id": "539228b3f02cbef87f000e32",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037720,
  "updated_at": 1413304037720,
  "expires_at": 1433623500000,
  "last_reminder": "",
  "user_id": "53922124f02cbec7ac000d94",
  "activated_at": 1402087605851,
  "title": "$129.99 for six acupuncture sessions ($300 value",
  "offer": "$129.99 for six acupuncture sessions ($300 value",
  "terms": "New Clients Only.\r\nExpires 60 days after purchase. Limit 1 per person, may buy 1 additional as gift. Valid only for option purchased. Limit 1 per visit. Appointment required. 24hr cancellation notice required. Must sign waiver. Consultation required; non-candidates and other refund requests will be honored before service provided. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.\r\nNo Cash Value.  No Change Given.  Cannot be combined with other offers.\r\n* Note: Refund if someone is not medically stable to receive treatment. \r\nVoucher does not apply towards lost of herbal formulas",
  "profile_id": "53922124f02cbec7ac000d93",
  "listing_ids": [
    "5392228ff02cbeb858000bd7"
  ],
  "category_id": "4f7d215774dbb02de3000301",
  "value": "300.0",
  "price": "129.99",
  "limit": 20,
  "limit_per_customer": 0,
  "locations": [
    {
      "_id": "539228b3f02cbef87f000e33",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.7829994269729,
        47.69387363207019
      ],
      "address": {
        "_id": "539228b3f02cbef87f000e34",
        "country": "US",
        "street": "225 E. Locust Ave.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000063",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000065",
    "attached_width": 377,
    "attached_height": 196,
    "attached_size": 10202,
    "attached_name": "safe_image.jpg",
    "updated_at": 1413304037720,
    "created_at": 1413304037720
  }
}
deals << {
  "_id": "5395f40bf02cbed6c800399f",
  "active": false,
  "vouchers_count": 1,
  "created_at": 1413304037750,
  "updated_at": 1423159386784,
  "expires_at": 1420012800000,
  "last_reminder": "",
  "user_id": "538e04b59c058e34c4000bba",
  "activated_at": 1402336986542,
  "title": "Special Deal!",
  "offer": "Get $100.00 worth of Bid lawn Service for only $60.00!",
  "terms": "-Not to Exceed Spokane County, Spokane City, and Spokane Valley City Limits.\r\n-Limit ONE MarketPad Voucher Per Customer Per Visit.\r\n-Purchase Limit 1 Per Person, 5 Additional as Gift.\r\n-Cannot Be Combined With Any Other Deal or Offer.\r\n-MUST Present MarketPad Voucher PRIOR To Services & Product Ordered.\r\n-MUST Call Ahead for Appointment with MarketPad Voucher Number Given.\r\n-Voucher Discount Expires 30 Days After Purchase.\r\n-No Food or Beverages Included.\r\n-Merchant is Solely Responsible to Purchasers For The Care And Quality Of The Advertised Goods And Services.\r\n-Appointment Required.\r\n-Subject to Availability & Weather.\r\n-Must Be 18+.",
  "profile_id": "538e04b59c058e34c4000bb9",
  "listing_ids": [
    "538e075f9c058e7a03000b24",
    "538e05939c058e8efb000bcd"
  ],
  "category_id": "4f7d215974dbb02de30003e9",
  "value": "100.0",
  "price": "60.0",
  "limit": 25,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "5395f40bf02cbed6c80039a0",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -117.43979,
        47.69399
      ],
      "address": {
        "_id": "5395f40bf02cbed6c80039a1",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    },
    {
      "_id": "5395f40bf02cbed6c80039a2",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -117.43979,
        47.69399
      ],
      "address": {
        "_id": "5395f40bf02cbed6c80039a3",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000065",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000067",
    "attached_width": 155,
    "attached_height": 159,
    "attached_size": 5070,
    "attached_name": "GetAttachment (2.jpg",
    "updated_at": 1413304037750,
    "created_at": 1413304037750
  }
}
deals << {
  "_id": "5397368af02cbe5241005646",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037817,
  "updated_at": 1463071621744,
  "expires_at": 1410332400000,
  "last_reminder": "",
  "user_id": "4fb553b208dea13806000a31",
  "activated_at": 1402419591255,
  "title": "Dozen Roses for that special person or event that demands that special fragrance!!",
  "offer": "ONLY $40 for a Dozen Roses | Normally $55\r\nGive 12 roses to that special person or if you have an event that demands that special fragrance! Pick Your Rose Color Choices!\r\n\r\n",
  "terms": "> Limit 1 per customer and 1 as a GIFT.   \r\n> Valid ONLY for Specific Product Purchased on Voucher.  \r\n> No Cash Value & No Change Given.  \r\n> Voucher Discount Expires 90 Days after purchase date. \r\n> Cannot be combined with any other deal or offer.  \r\n> Full Voucher expires 1 year after purchase.  \r\n> Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. ",
  "profile_id": "4fb7452208dea126b90374f5",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "category_id": "4f7d215874dbb02de30003b6",
  "value": "55.0",
  "price": "40.0",
  "limit": 25,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "5397368af02cbe5241005647",
      "geocode_source": "google",
      "geocoded_city_level": false,
      "point": [
        -116.7858881,
        47.7342871
      ],
      "address": {
        "_id": "5397368af02cbe5241005648",
        "country": "US",
        "city": "Dalton Gardens",
        "postal_code": "83815",
        "region": "ID",
        "street": "6848 N. Government Way,",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000067",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae000069",
    "attached_width": 243,
    "attached_height": 274,
    "attached_size": 24207,
    "attached_name": "creativefloralrosess.JPG",
    "updated_at": 1413304037816,
    "created_at": 1413304037816
  }
}
deals << {
  "_id": "5399c63bf02cbed6a5000fba",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037966,
  "updated_at": 1465325840175,
  "expires_at": 1412211600000,
  "last_reminder": "",
  "user_id": "5399bf83f02cbe179a000e1f",
  "activated_at": "",
  "title": "$37.50 All over Color or Ombre, Haircut & Eye Brow Wax! $75 VALUE (SAVE 50%",
  "offer": "Sarah Shearer OFFER: SAVE 50%!!!!  For just $37.50 any NEW client gets a All-Over Color or Ombre, Haircut & Eyebrow Wax!  Regular price is $75.  ",
  "terms": "Limit ONE MarketPad Voucher per Customer per visit. Purchase LIMIT 1 per person, 1 Additional as Gift, Cannot be combined with any other deal or offer. NO CASH Value, No Change Given, Must USE FULL Voucher Value in (1 Visit. MUST Present MarketPad Voucher PRIOR to Services & Product Ordered. MUST CALL AHEAD for Appointment with MarketPad Voucher Number Given.  Voucher Discount Expires 90 DAYS after Purchase DATE. Not valid toward previous sessions.  Must be 14+ in Age, NEW CLIENTS ONLY!!",
  "profile_id": "5399bf83f02cbe179a000e1e",
  "listing_ids": [
    "5399c406f02cbe4b23000dbf"
  ],
  "category_id": "4f7d216574dbb02de3000936",
  "value": "75.0",
  "price": "37.5",
  "limit": 50,
  "limit_per_customer": 1,
  "locations": [
    {
      "_id": "5399c63bf02cbed6a5000fbb",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.782903959184,
        47.6744129387755
      ],
      "address": {
        "_id": "5399c63bf02cbed6a5000fbc",
        "country": "US",
        "street": "211 E. Lakeside Ave.",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d0000069",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae00006b",
    "attached_width": 360,
    "attached_height": 450,
    "attached_size": 50453,
    "attached_name": "image5.jpeg",
    "updated_at": 1413304037966,
    "created_at": 1413304037966
  },
  "referral_code": "currentattractions"
}
deals << {
  "_id": "539a2d53f02cbe2084001342",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304037997,
  "updated_at": 1461348757698,
  "expires_at": 1546329600000,
  "last_reminder": "",
  "user_id": "539a2808f02cbe889b0014d6",
  "activated_at": 1402678001296,
  "title": "Oil Change with 48 point inspection",
  "offer": "$29.95 for Oil Change with a 48 Point Inspection | Normally $44.00",
  "terms": "Limit 1 per person, may buy 2 additional as gifts. Limit 1 per visit. Appointment required. 24-hr cancellation notice required. Must activate by expiration date on voucher, must redeem all services in full within 1 year of activation date. Extra fee for synthetic oil. Valid for up to 5 quarts of oil. All services must be performed on the same vehicle.  Extra fee for diesel vehicles. ",
  "profile_id": "539a2808f02cbe889b0014d5",
  "listing_ids": [
    "539a29b8f02cbef5e40015ae"
  ],
  "category_id": "4f7d216774dbb02de3000a6b",
  "value": "44.0",
  "price": "29.95",
  "limit": 50,
  "limit_per_customer": 3,
  "locations": [
    {
      "_id": "539a2d53f02cbe2084001343",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.977463,
        47.716133
      ],
      "address": {
        "_id": "539a2d53f02cbe2084001344",
        "country": "US",
        "street": "3310 W. Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "543d4ee5f02cbea0d000006b",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee5f02cbe3cae00006d",
    "attached_width": 246,
    "attached_height": 144,
    "attached_size": 17433,
    "attached_name": "image.jpg",
    "updated_at": 1413304037997,
    "created_at": 1413304037997
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "539b3cbff02cbe32ff00013e",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304038087,
  "updated_at": 1413304038087,
  "expires_at": 1410670800000,
  "last_reminder": "",
  "user_id": "539b294ef02cbe0ae1000015",
  "activated_at": 1403207325381,
  "title": "Only $20 For $28 Worth of Any Specialty Pizza.",
  "offer": "Any Specialty Pizza, $20 For A $28 Deal.",
  "terms": "- Limit ONE MarketPad Voucher per customer per visit.  Purchase Limit 2 per person, 2 Additional as Gifts.  Cannot be combined with any other deal or offer.  No Cash Value.  MUST present MarketPad Voucher PRIOR to Services & Product Ordered.  Voucher Discount Expires 90 Days After Purchase Date.  Full Voucher Value Expires ONE Year After Purchase Date.  Dine-In Only.  Valid ONLY For Specific Product Purchased On Voucher!  Cannot Be Combined With Other Offers.",
  "profile_id": "539b294ef02cbe0ae1000014",
  "listing_ids": [
    "539b2bfef02cbee13f00001c"
  ],
  "category_id": "4f7d215e74dbb02de3000651",
  "value": "28.0",
  "price": "20.0",
  "limit": 35,
  "limit_per_customer": 4,
  "locations": [
    {
      "_id": "539b3cbff02cbe32ff00013f",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -117.434906727273,
        47.7161496363636
      ],
      "address": {
        "_id": "539b3cbff02cbe32ff000140",
        "country": "US",
        "street": "6409 N. Maple,",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99208"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d000006d",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae00006f",
    "attached_width": 605,
    "attached_height": 368,
    "attached_size": 366740,
    "attached_name": "Selection_015.png",
    "updated_at": 1413304038087,
    "created_at": 1413304038087
  }
}
deals << {
  "_id": "53a46287f02cbeec930029a3",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304038209,
  "updated_at": 1413304038209,
  "expires_at": 1411254000000,
  "last_reminder": "",
  "user_id": "53a4573bf02cbe1575002714",
  "activated_at": "",
  "title": "$18.00 for a Gel Polish ($30 Value",
  "offer": "$18.00 for a Gel Polish ($30 Value, with Jennifer Haney.",
  "terms": "Limit ONE MarketPad Voucher per customer per visit.  Purchase limit 2 per person, 2 additional as Gifts.  Cannot be combined with any other deal or offer.  No cash value.  No change given.  Must use FULL Voucher Value in (1 visit.  MUST present MarketPad Voucher PRIOR to Services & Product Ordered.  Must call ahead for Appointment with MarketPad Voucher Number Given.  May NOT be used towards gratuity.  Voucher Discount Expires 90 Days after Purchase date.  Cancellation policy:  48-hour cancellation notice required prior to check-in or reservation is nonrefundable; reservations made within cancellation window are nonrefundable.  Rates may vary by date and are subject to availability.  Voucher Valid only for option purchased.  New Clients Only.",
  "profile_id": "53a4573bf02cbe1575002713",
  "listing_ids": [
    "53a4588cf02cbedded002646"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "30.0",
  "price": "18.0",
  "limit": 20,
  "limit_per_customer": 4,
  "locations": [
    {
      "_id": "53a46287f02cbeec930029a4",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -117.2806060256918,
        47.66033308434982
      ],
      "address": {
        "_id": "53a46287f02cbeec930029a5",
        "country": "US",
        "street": "9222 E. Valleyway",
        "suite": "",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99206"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d000006f",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000072",
    "attached_width": 381,
    "attached_height": 541,
    "attached_size": 300129,
    "attached_name": "Selection_014.png",
    "updated_at": 1413304038209,
    "created_at": 1413304038209
  }
}
deals << {
  "_id": "53a65daef02cbe58e400439c",
  "active": false,
  "vouchers_count": 2,
  "created_at": 1413304038257,
  "updated_at": 1415128109337,
  "expires_at": 1406854800000,
  "last_reminder": "",
  "user_id": "53a6561df02cbeeeb8004090",
  "activated_at": 1403412373269,
  "title": "Haircut, deep condition and wax",
  "offer": "Come in for a precision haircut, keratin hair restoring deep conditioning treatment and your choice of a brow or lip wax half off its regular price of $55 for just $28\r\n",
  "terms": "Cannot be combined with other coupons/discounts. Appointment required. 24 hour noticed is required for any rescheduling or cancellations. Tax and gratuity not included.",
  "profile_id": "53a6561df02cbeeeb800408f",
  "listing_ids": [
    "53a6572df02cbef3460041ac"
  ],
  "category_id": "4f7d215374dbb02de3000164",
  "value": "55.0",
  "price": "28.0",
  "limit": 20,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "53a65daef02cbe58e400439d",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -116.791867117547,
        47.7444661960686
      ],
      "address": {
        "_id": "53a65daef02cbe58e400439e",
        "country": "US",
        "street": "277 w Prairie ave",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d0000071",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000075",
    "attached_width": 886,
    "attached_height": 885,
    "attached_size": 100650,
    "attached_name": "1526819_253892458106716_1680477791_n(5.jpg",
    "updated_at": 1413304038257,
    "created_at": 1413304038257
  }
}
deals << {
  "_id": "53aaf1c5f02cbee703008a40",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304038356,
  "updated_at": 1413304038356,
  "expires_at": 1411689000000,
  "last_reminder": "",
  "user_id": "53a4573bf02cbe1575002714",
  "activated_at": "",
  "title": "$35 for a Full Set and Fill ($75 Value",
  "offer": "$35 for a Full Set and Fill ($75 Value, with Jennifer Haney.",
  "terms": "Limit ONE MarketPad Voucher per customer per visit.  Purchase limit 2 per person, 2 additional as Gifts.  Cannot be combined with any other deal or offer.  No cash value.  No change given.  Must use FULL Voucher Value in (1 visit.  MUST present MarketPad Voucher PRIOR to Services & Product Ordered.  Must call ahead for Appointment with MarketPad Voucher Number Given.  May NOT be used towards gratuity.  Voucher Discount Expires 90 Days after Purchase date.  Cancellation policy:  48-hour cancellation notice required prior to check-in or reservation is nonrefundable; reservations made within cancellation window are nonrefundable.  Rates may vary by date and are subject to availability.  Voucher Valid only for option purchased.  New Clients Only.",
  "profile_id": "53a4573bf02cbe1575002713",
  "listing_ids": [
    "53a4588cf02cbedded002646"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "75.0",
  "price": "35.0",
  "limit": 20,
  "limit_per_customer": 4,
  "locations": [
    {
      "_id": "53aaf1c5f02cbee703008a41",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -117.2806060256918,
        47.66033308434982
      ],
      "address": {
        "_id": "53aaf1c5f02cbee703008a42",
        "country": "US",
        "street": "9222 E. Valleyway",
        "suite": "",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99206"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d0000074",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000077",
    "attached_width": 200,
    "attached_height": 295,
    "attached_size": 10472,
    "attached_name": "4029429379_e03b844f39.jpg",
    "updated_at": 1413304038355,
    "created_at": 1413304038355
  }
}
deals << {
  "_id": "53aaf50af02cbee1d4008530",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304038426,
  "updated_at": 1413304038426,
  "expires_at": 1411689000000,
  "last_reminder": "",
  "user_id": "53a4573bf02cbe1575002714",
  "activated_at": "",
  "title": "$30 for 2 Gel Polishes ($60 Value",
  "offer": "$30 for 2 Gel Polishes ($60 Value, by Jennifer Haney.",
  "terms": "Limit ONE MarketPad Voucher per customer per visit.  Purchase limit 2 per person, 2 additional as Gifts.  Cannot be combined with any other deal or offer.  No cash value.  No change given.  Must use FULL Voucher Value in (1 visit.  MUST present MarketPad Voucher PRIOR to Services & Product Ordered.  Must call ahead for Appointment with MarketPad Voucher Number Given.  May NOT be used towards gratuity.  Voucher Discount Expires 90 Days after Purchase date.  Cancellation policy:  48-hour cancellation notice required prior to check-in or reservation is nonrefundable; reservations made within cancellation window are nonrefundable.  Rates may vary by date and are subject to availability.  Voucher Valid only for option purchased.  New Clients Only.",
  "profile_id": "53a4573bf02cbe1575002713",
  "listing_ids": [
    "53a4588cf02cbedded002646"
  ],
  "category_id": "4f7d215774dbb02de30002fa",
  "value": "60.0",
  "price": "30.0",
  "limit": 20,
  "limit_per_customer": 4,
  "locations": [
    {
      "_id": "53aaf50af02cbee1d4008531",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -117.2806060256918,
        47.66033308434982
      ],
      "address": {
        "_id": "53aaf50af02cbee1d4008532",
        "country": "US",
        "street": "9222 E. Valleyway",
        "suite": "",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99206"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d0000076",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000079",
    "attached_width": 500,
    "attached_height": 375,
    "attached_size": 121098,
    "attached_name": "5661106909_b2b85caf0e.jpg",
    "updated_at": 1413304038425,
    "created_at": 1413304038425
  }
}
deals << {
  "_id": "53c34bc2f02cbe97ef002358",
  "active": false,
  "vouchers_count": 1,
  "created_at": 1413304038620,
  "updated_at": 1464107595458,
  "expires_at": 1436842200000,
  "last_reminder": "",
  "user_id": "53c32cc5f02cbe2b1e002092",
  "activated_at": 1405307970889,
  "title": "Touch Therapeutic Massage",
  "offer": "One hour massage (therapeutic, sports, relaxation",
  "terms": " NEW CLIENTS ONLY. Valid Mon-Fri by Appointment. No Cash Value. No Change Given. Must Use FULL Voucher Value In One Visit.  Voucher Discount Expires 90 Days After Purchase. Full Voucher Value Expires One Year After Purchase. Can Not Be Combined With Other Offers. Must Fill Out Client Intake Form",
  "profile_id": "53c32e56f02cbef5890021fc",
  "listing_ids": [
    "53c336e3f02cbe3efc001d48"
  ],
  "category_id": "4f7d215974dbb02de30003d1",
  "value": "45.0",
  "price": "35.0",
  "limit": 35,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "53c34bc2f02cbe97ef002357",
      "point": [
        -116.787017,
        47.751613
      ],
      "address": {
        "_id": "53c34bc2f02cbe97ef002359",
        "country": "US",
        "street": "8785 N Government Way",
        "suite": "108",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d000007a",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae00007d",
    "attached_width": 1024,
    "attached_height": 683,
    "attached_size": 319131,
    "attached_name": "head-massage.gif",
    "updated_at": 1413304038620,
    "created_at": 1413304038620
  }
}
deals << {
  "_id": "53c34f5ff02cbefd2f002395",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304038657,
  "updated_at": 1477340764300,
  "expires_at": 1501484400000,
  "last_reminder": "",
  "user_id": "53c32cc5f02cbe2b1e002092",
  "activated_at": 1405308784241,
  "title": "90 Minute Massage (therapeutic, sports, relaxation",
  "offer": "90 minute Massage (relaxation, therapeutic, sports\r\n\r\nTouch Therapeutic located in Hayden ID\r\n(509 216-3663\r\n",
  "terms": "> NEW CLIENTS ONLY. \r\n> Valid Mon-Fri by Appointment. \r\n> No Cash Value. \r\n> No Change Given. \r\n> Must Use FULL Voucher Value In One Visit. \r\n> Voucher Discount Expires 90 Days After Purchase. \r\n> Full Voucher Value Expires One Year After Purchase. \r\n> Can Not Be Combined With Other Offers. \r\n> Must Fill Out Client Intake Form",
  "profile_id": "53c32e56f02cbef5890021fc",
  "listing_ids": [
    "53c336e3f02cbe3efc001d48"
  ],
  "category_id": "4f7d215974dbb02de30003d1",
  "value": "75.0",
  "price": "65.0",
  "limit": 20,
  "limit_per_customer": 2,
  "locations": [
    {
      "_id": "53c34f5ff02cbefd2f002394",
      "point": [
        -116.787017,
        47.751613
      ],
      "address": {
        "_id": "53c34f5ff02cbefd2f002396",
        "country": "US",
        "street": "8785 N Government Way",
        "suite": "108",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d000007d",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000080",
    "attached_width": 372,
    "attached_height": 250,
    "attached_size": 52683,
    "attached_name": "relax.jpg",
    "updated_at": 1413304038657,
    "created_at": 1413304038657
  },
  "referral_code": "rachelr83",
  "deleted_at": 1485232581877
}
deals << {
  "_id": "53dbf18af02cbe00da002b4b",
  "active": true,
  "vouchers_count": 29,
  "created_at": 1413304038692,
  "updated_at": 1413304038692,
  "expires_at": 1407653940000,
  "last_reminder": "",
  "user_id": "53dab96ff02cbe466d001cdf",
  "activated_at": 1406923325534,
  "title": "First Annual Hot August Knights Ticket",
  "offer": "Save 2 bucks on your ticket buying online!",
  "terms": "Valid for Hot August Knights 2014 Only.",
  "profile_id": "53dab96ff02cbe466d001cde",
  "listing_ids": [
    "53d41b57f02cbebade0034fe"
  ],
  "category_id": "4f7d215d74dbb02de30005f9",
  "value": "8.0",
  "price": "6.0",
  "limit": "",
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "53dbf18af02cbe00da002b4a",
      "point": [
        -116.999991,
        47.711402
      ],
      "address": {
        "_id": "53dbf18af02cbe00da002b4c",
        "country": "US",
        "street": "3920 West 5th Avenue,",
        "suite": "A3,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d000007f",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000082",
    "attached_width": 1024,
    "attached_height": 309,
    "attached_size": 57628,
    "attached_name": "HAK.jpg",
    "updated_at": 1413304038692,
    "created_at": 1413304038692
  }
}
deals << {
  "_id": "54128446f02cbead240005f2",
  "active": false,
  "vouchers_count": 0,
  "created_at": 1413304039147,
  "updated_at": 1414534275977,
  "expires_at": 1422763200000,
  "last_reminder": "",
  "user_id": "540df46ef02cbefd15000075",
  "activated_at": 1410675095691,
  "title": "Raindrop Therapy Session Special (Single Session",
  "offer": "Anastasia's Health and Wellness Raindrop Therapy Session (Single Session\r\nSession duration: 1 hr. 15 min.\r\n\r\nThere are numerous benefits of Raindrop Therapy. Here are just a few:\r\n~Balance and Re-align the Energy Centers of the Body\r\n~Reduce Stress and Minor Anxiety\r\n~Aid the Body's Natural Response to Irritation & Injury\r\n~Ease Muscle, Bone, and Joint Discomfort\r\n~Improve Immune System Functions\r\n~Emotional Well Being and Release\r\n~Help Detox the Body Systems\r\n~Increase in Height\r\n~And many more!\r\n\r\nRaindrop Technique uses a sequence of essential oils that are immune enhancing, support the body's natural defenses, as well as the circulatory, respiratory, endocrine, digestive, nervous, and other body systems. These oils, which are high in antioxidants, are also mood elevating and antiseptic, creating an unfavorable environment for harmful viruses and bacteria that can hibernate in the body. \r\n\r\nEssential oils are known to boost stamina and energy, help you relax, help manage stress and frustration and promote overall health, vitality, and longevity.\r\n\r\nThe whole process takes about an hour and may continue to work in the body for up to one week following a Raindrop Session, with possible realignment and bodily adjustment taking place during this time.\r\n\r\nThe Essential Oils used include:\r\n~Valor (blend: cleanses the energy field, aligns and balances\r\n~Oregano: attacks viruses and bacteria\r\n~Thyme: attacks viruses and bacteria\r\n~Basil: anti-spasmodic, headache relief\r\n~Cypress: increases circulation\r\n~Wintergreen: anti-inflammatory/anti-spasmodic, analgesic (pain reliever\r\n~Marjoram: relaxes musculature, lowers blood pressure\r\n~Peppermint: anti-spasmodic, assists in \"pushing\" in all other oils\r\n~Aroma Siez (blend: relaxing & soothing",
  "terms": "",
  "profile_id": "5408b6e0f02cbe4d40003e11",
  "listing_ids": [
    "5408c0cdf02cbed95d00429e"
  ],
  "category_id": "4f7d215a74dbb02de3000436",
  "value": "60.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "54128446f02cbead240005f1",
      "point": [
        -116.785645,
        47.738174
      ],
      "address": {
        "_id": "54128446f02cbead240005f3",
        "country": "US",
        "street": "7352 N. Government Way, ",
        "suite": "D,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000a2",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae00008e",
    "attached_width": 700,
    "attached_height": 542,
    "attached_size": 401802,
    "attached_name": "Anastasias-Health-Wellness_Raindrop-Special(50each.jpg",
    "updated_at": 1413304039147,
    "created_at": 1413304039147
  }
}
deals << {
  "_id": "54147a5df02cbe259f001782",
  "active": false,
  "vouchers_count": 0,
  "created_at": 1413304039253,
  "updated_at": 1414534285550,
  "expires_at": 1422763200000,
  "last_reminder": "",
  "user_id": "540df46ef02cbefd15000075",
  "activated_at": 1410675119922,
  "title": "Raindrop Therapy Session Special (3 Sessions",
  "offer": "Anastasia's Health and Wellness Raindrop Therapy Session (3 Sessions\r\nSession duration: 1 hr. 15 min.\r\n\r\nThere are numerous benefits of Raindrop Therapy. Here are just a few: \r\n~Balance and Re-align the Energy Centers of the Body \r\n~Reduce Stress and Minor Anxiety \r\n~Aid the Body's Natural Response to Irritation & Injury \r\n~Ease Muscle, Bone, and Joint Discomfort \r\n~Improve Immune System Functions \r\n~Emotional Well Being and Release \r\n~Help Detox the Body Systems \r\n~Increase in Height \r\n~And many more!\r\n\r\nRaindrop Technique uses a sequence of essential oils that are immune enhancing, support the body's natural defenses, as well as the circulatory, respiratory, endocrine, digestive, nervous, and other body systems. These oils, which are high in antioxidants, are also mood elevating and antiseptic, creating an unfavorable environment for harmful viruses and bacteria that can hibernate in the body.\r\n\r\nEssential oils are known to boost stamina and energy, help you relax, help manage stress and frustration and promote overall health, vitality, and longevity.\r\n\r\nThe whole process takes about an hour and may continue to work in the body for up to one week following a Raindrop Session, with possible realignment and bodily adjustment taking place during this time.\r\n\r\nThe Essential Oils used include: \r\n~Valor (blend: cleanses the energy field, aligns and balances \r\n~Oregano: attacks viruses and bacteria \r\n~Thyme: attacks viruses and bacteria \r\n~Basil: anti-spasmodic, headache relief \r\n~Cypress: increases circulation \r\n~Wintergreen: anti-inflammatory/anti-spasmodic, analgesic (pain reliever \r\n~Marjoram: relaxes musculature, lowers blood pressure \r\n~Peppermint: anti-spasmodic, assists in \"pushing\" in all other oils \r\n~Aroma Siez (blend: relaxing & soothing",
  "terms": "",
  "profile_id": "5408b6e0f02cbe4d40003e11",
  "listing_ids": [
    "5408c0cdf02cbed95d00429e"
  ],
  "category_id": "4f7d215a74dbb02de3000436",
  "value": "125.0",
  "price": "105.0",
  "limit": "",
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "54147a5df02cbe259f001781",
      "point": [
        -116.785645,
        47.738174
      ],
      "address": {
        "_id": "54147a5df02cbe259f001783",
        "country": "US",
        "street": "7352 N. Government Way, ",
        "suite": "D,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000a4",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae000091",
    "attached_width": 700,
    "attached_height": 542,
    "attached_size": 401171,
    "attached_name": "Anastasias-Health-Wellness_Raindrop-Special(105for3.jpg",
    "updated_at": 1413304039253,
    "created_at": 1413304039253
  }
}
deals << {
  "_id": "5419dc37f02cbe7ab1000535",
  "active": false,
  "vouchers_count": 0,
  "created_at": 1413304038794,
  "updated_at": 1418071069675,
  "expires_at": 1420088400000,
  "last_reminder": "",
  "user_id": "538e4d2f9c058e2308000ebe",
  "activated_at": 1410980923805,
  "title": "$10.36 (SAVE $5.59 14\" Specialty Pizza! (regular price $15.95 Hayden & Kellogg Locations.",
  "offer": "ONE 14\" Wildcat Specialty Pizza for just $10.36 +Tax. SAVE $5.59 (35% Savings.",
  "terms": "Expires Dec. 31, 2014. Limit 1 per person, May buy 1 additional as Gift. Limit 1 per table. Limit 1 per visit. Must purchase a food item listed on Voucher. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "538e4d2f9c058e2308000ebd",
  "listing_ids": [
    "538e599e9c058e44da000f50"
  ],
  "category_id": "4f7d215e74dbb02de3000651",
  "value": "15.95",
  "price": "10.36",
  "limit": 100,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "5419dc37f02cbe7ab1000533",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.788028065751,
        47.7456989028098
      ],
      "address": {
        "_id": "5419dc37f02cbe7ab1000536",
        "country": "US",
        "street": "85 W. Prairie Shopping Center",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "5419dc37f02cbe7ab1000534",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.135096616219,
        47.5421399670481
      ],
      "address": {
        "_id": "5419dc37f02cbe7ab1000537",
        "country": "US",
        "street": "604 Bunker Ave.",
        "suite": "",
        "city": "Kellogg",
        "region": "ID",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d000009e",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000084",
    "attached_width": 200,
    "attached_height": 200,
    "attached_size": 18448,
    "attached_name": "Pizza Loaded4 Mushrooms.jpg",
    "updated_at": 1413304038794,
    "created_at": 1413304038794
  }
}
deals << {
  "_id": "5419dd9ef02cbee0a00004b6",
  "active": false,
  "vouchers_count": 0,
  "created_at": 1413304039290,
  "updated_at": 1418071232490,
  "expires_at": 1420088400000,
  "last_reminder": "",
  "user_id": "538e4d2f9c058e2308000ebe",
  "activated_at": 1410981283315,
  "title": " $13.62 for ONE 18\" Wildcat Specialty Pizza! SAVE $7.33 ( Save 35% Hayden & Kellogg Locations",
  "offer": "ONE 18\" Wildcat Specialty Pizza for just $13.62 + tax. SAVE 35%!",
  "terms": "Expires Dec. 31, 2014. Limit 1 per person, May buy 1 additional as Gift. Limit 1 per table. Limit 1 per visit. Must purchase a food item listed on Voucher. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "profile_id": "538e4d2f9c058e2308000ebd",
  "listing_ids": [
    "538e599e9c058e44da000f50"
  ],
  "category_id": "4f7d215e74dbb02de3000651",
  "value": "20.95",
  "price": "13.62",
  "limit": 100,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "5419dd9ef02cbee0a00004b4",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.788028065751,
        47.7456989028098
      ],
      "address": {
        "_id": "5419dd9ef02cbee0a00004b7",
        "country": "US",
        "street": "85 W. Prairie Shopping Center",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "5419dd9ef02cbee0a00004b5",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.135096616219,
        47.5421399670481
      ],
      "address": {
        "_id": "5419dd9ef02cbee0a00004b8",
        "country": "US",
        "street": "604 Bunker Ave.",
        "suite": "",
        "city": "Kellogg",
        "region": "ID",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000a6",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae000094",
    "attached_width": 200,
    "attached_height": 200,
    "attached_size": 22146,
    "attached_name": "Pizza Loaded.jpg",
    "updated_at": 1413304039290,
    "created_at": 1413304039290
  }
}
deals << {
  "_id": "541fa90af02cbe6bdb000ee5",
  "active": false,
  "vouchers_count": 1,
  "created_at": 1413304039424,
  "updated_at": 1414534264662,
  "expires_at": 1419220800000,
  "last_reminder": "",
  "user_id": "540df46ef02cbefd15000075",
  "activated_at": 1411361045577,
  "title": "Anastasia's Health & Wellness - Awwwtumn Spa Special",
  "offer": "Awwwtumn Spa Special\r\n\r\n1 Hour Massage \r\n1 Facial\r\n1 Body Wrap",
  "terms": "",
  "profile_id": "5408b6e0f02cbe4d40003e11",
  "listing_ids": [
    "5408c0cdf02cbed95d00429e"
  ],
  "category_id": "4f7d215a74dbb02de3000436",
  "value": "140.0",
  "price": "85.0",
  "limit": "",
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "541fa90af02cbe6bdb000ee4",
      "point": [
        -116.785645,
        47.738174
      ],
      "address": {
        "_id": "541fa90af02cbe6bdb000ee6",
        "country": "US",
        "street": "7352 N. Government Way, ",
        "suite": "D,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000a8",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae000096",
    "attached_width": 900,
    "attached_height": 675,
    "attached_size": 501856,
    "attached_name": "Anastasias-Health-Wellness_Awwwtumn-Special.jpg",
    "updated_at": 1413304039424,
    "created_at": 1413304039424
  }
}
deals << {
  "_id": "54219613f02cbe1c7d000694",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304039486,
  "updated_at": 1413304039486,
  "expires_at": 1420012800000,
  "last_reminder": "",
  "user_id": "53a6561df02cbeeeb8004090",
  "activated_at": 1411487284938,
  "title": "$40 Gel polish pedicure",
  "offer": "Come in and enjoy and hour long luxurious pedicure with reclining massage chairs and jetted tubs, then choose from 1 (or more out of our 200+ gel polish selection. gel polish dries in under one minute so there is no more smudging or chipping and the polish can last up to two months!",
  "terms": "*discounts cannot be combined\r\n*gratuity is not included",
  "profile_id": "53a6561df02cbeeeb800408f",
  "listing_ids": [
    "53a6572df02cbef3460041ac"
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "70.0",
  "price": "40.0",
  "limit": 10,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "54219613f02cbe1c7d000693",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -116.7896293851807,
        47.74669871204058
      ],
      "address": {
        "_id": "54219613f02cbe1c7d000695",
        "country": "US",
        "street": "277 W. Prairie Ave.",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000ab",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae000099",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 77901,
    "attached_name": "toes.jpg",
    "updated_at": 1413304039486,
    "created_at": 1413304039486
  }
}
deals << {
  "_id": "54219b00f02cbee8440006ca",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304039518,
  "updated_at": 1413304039518,
  "expires_at": 1420012800000,
  "last_reminder": "",
  "user_id": "53a6561df02cbeeeb8004090",
  "activated_at": 1411488540107,
  "title": "End of Summer Savings part 1",
  "offer": "Choose from two amazing deals\r\nA vibrant all over color with a complimentary haircut for just $50\r\n-or-\r\nA half head highlight also with a complimentary haircut for just $50",
  "terms": "*discounts cannot be combined\r\n*any additional color is an extra $10\r\n*gratuity not included",
  "profile_id": "53a6561df02cbeeeb800408f",
  "listing_ids": [
    "53a6572df02cbef3460041ac"
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "70.0",
  "price": "50.0",
  "limit": 8,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "54219b00f02cbee8440006c9",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -116.7896293851807,
        47.74669871204058
      ],
      "address": {
        "_id": "54219b00f02cbee8440006cb",
        "country": "US",
        "street": "277 W. Prairie Ave.",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000ad",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae00009b",
    "attached_width": 420,
    "attached_height": 294,
    "attached_size": 23725,
    "attached_name": "hair.jpg",
    "updated_at": 1413304039518,
    "created_at": 1413304039518
  }
}
deals << {
  "_id": "54219f8ef02cbe20240006e2",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304039050,
  "updated_at": 1413304039050,
  "expires_at": 1420012800000,
  "last_reminder": "",
  "user_id": "53a6561df02cbeeeb8004090",
  "activated_at": 1411489711196,
  "title": "End of Summer Savings part 2",
  "offer": "been craving blonde in your life?\r\ncome in for either a full head highlight to add an little dimension for $60\r\n-or-\r\nthe ever popular ombre also just $60\r\nboth color options come with complimentary haircut.",
  "terms": "*discounts cannot be combined\r\n*any additional color is an extra $10\r\n*gratuity not included",
  "profile_id": "53a6561df02cbeeeb800408f",
  "listing_ids": [
    "53a6572df02cbef3460041ac"
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "90.0",
  "price": "60.0",
  "limit": 6,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "54219f8ef02cbe20240006e1",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -116.7896293851807,
        47.74669871204058
      ],
      "address": {
        "_id": "54219f8ef02cbe20240006e3",
        "country": "US",
        "street": "277 W. Prairie Ave.",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "543d4ee6f02cbea0d00000a0",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee6f02cbe3cae000086",
    "attached_width": 1536,
    "attached_height": 1536,
    "attached_size": 1653105,
    "attached_name": "PicsArt_1401829569522.jpg",
    "updated_at": 1413304039050,
    "created_at": 1413304039050
  }
}
deals << {
  "_id": "5421a163f02cbe9a74000701",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304039547,
  "updated_at": 1413304039547,
  "expires_at": 1413010800000,
  "last_reminder": "",
  "user_id": "53a6561df02cbeeeb8004090",
  "activated_at": 1411490160768,
  "title": "Haircut deep condition & wax",
  "offer": "Come in for a precision haircut, keratin hair restoring deep conditioning treatment and your choice of a brow or lip wax half off its regular price of $55 for just $30",
  "terms": "*Cannot be combined with other coupons/discounts. \r\n*Appointment required. 24 hour noticed is required for any rescheduling or cancellations. \r\n*gratuity not included.",
  "profile_id": "53a6561df02cbeeeb800408f",
  "listing_ids": [
    
  ],
  "category_id": "4f7d215974dbb02de300042a",
  "value": "55.0",
  "price": "30.0",
  "limit": 10,
  "limit_per_customer": "",
  "image": {
    "_id": "543d4ee7f02cbea0d00000af",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae00009d",
    "attached_width": 160,
    "attached_height": 160,
    "attached_size": 7026,
    "attached_name": "1526819_253892458106716_1680477791_n(1.jpg",
    "updated_at": 1413304039547,
    "created_at": 1413304039547
  }
}
deals << {
  "_id": "543961d2f02cbe1e0500144d",
  "active": true,
  "vouchers_count": 0,
  "created_at": 1413304039590,
  "updated_at": 1413304039590,
  "expires_at": 1417420800000,
  "last_reminder": "",
  "user_id": "542469b3f02cbeab87000008",
  "activated_at": 1413046780474,
  "title": "Don't Live With Pain!",
  "offer": "Get Your Foot In The Door With A $27 Consultation, Exam and Any Necessary X-rays",
  "terms": "One Coupon Per Customer. Not Valid With Other Offers. ",
  "profile_id": "54206248f02cbee8bc001342",
  "listing_ids": [
    "5420663bf02cbe95ce0012c2"
  ],
  "category_id": "4f7d215974dbb02de30003d0",
  "value": "160.0",
  "price": "27.0",
  "limit": 20,
  "limit_per_customer": "",
  "locations": [
    {
      "_id": "543961d2f02cbe1e0500144c",
      "point": [
        -116.900814,
        47.713474
      ],
      "address": {
        "_id": "543961d2f02cbe1e0500144e",
        "country": "US",
        "street": "640 N. Thornton St.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "543d4ee7f02cbea0d00000b1",
    "_type": "Attachments::Image",
    "position": 100,
    "main": false,
    "attached_uid": "543d4ee7f02cbe3cae00009f",
    "attached_width": 1372,
    "attached_height": 1054,
    "attached_size": 138833,
    "attached_name": "Bridgewater Chiropractic_2.png",
    "updated_at": 1413304039590,
    "created_at": 1413304039590
  }
}
deals << {
  "_id": "545016adf02cbeb3a5001373",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "5408b6e0f02cbe4d40003e11",
  "title": "Pumpkin Cheesecake Peel & Body Wrap with Chi Session",
  "category_id": "4f7d215a74dbb02de3000436",
  "offer": "Schedule a personalized Pumpkin Cheesecake Peel & Body Wrap with Chi Session\r\n\r\nPumpkin Cheesecake Peel is a professional retexturizing facial with 30% Fruit Acid Complex for the ultimate exfoliation and sensory experience of freshly baked pumpkin bread!\r\n\r\n1 Resurfaces and rejuvenates skin\r\n\r\n2 Unclogs pores\r\n\r\n3 Leaves skin radiant and luminous\r\n\r\n\r\n\r\nChi Machine Session benefits:\r\n\r\nThe Chi Machine creates massage of internal organs via passive aerobic exercise.\r\n\r\nMovement of the body - consistent motion energy - creates cellular oxygenation \r\nof both the body and the brain, thereby allowing:\r\n\r\n1 Reduction in muscle soreness, tension, stiffness, body aches and pains.\r\n\r\n2 Improved flexibility of muscles and joints, more limber - spinal alignment.\r\n\r\n3 Improved function and regulation of internal organs and body systems.\r\n\r\n4 Improved circulation - activated lymph drainage and detoxification.\r\n\r\n5 Relief from stress related conditions.\r\n\r\n6 The brain shifts into a relaxed Alpha brainwave state - mental calm, focus, clarity, and relaxation.\r\n\r\n7 Sound, restful sleep.\r\n\r\n8 Improved metabolic rate - Increased energy and improved sense of well being.",
  "terms": "",
  "value": "130.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5408c0cdf02cbed95d00429e"
  ],
  "expires_at": 1420012800000,
  "user_id": "540df46ef02cbefd15000075",
  "updated_at": 1414597256347,
  "created_at": 1414534829854,
  "locations": [
    {
      "_id": "545016adf02cbeb3a5001372",
      "point": [
        -116.785645,
        47.738174
      ],
      "address": {
        "_id": "545016adf02cbeb3a5001374",
        "country": "US",
        "street": "7352 N. Government Way, ",
        "suite": "D,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1414534840913,
  "image": {
    "_id": "5450187ff02cbee62a0013d4",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5450187ff02cbe7e1400001d",
    "attached_width": 1600,
    "attached_height": 1600,
    "attached_size": 497862,
    "attached_name": "Anastasias-Health-and-Wellness-Spa_Pumpkin-Special.jpg",
    "main": false,
    "updated_at": 1414535295278,
    "created_at": 1414535295278
  }
}
deals << {
  "_id": "54650607f02cbe6901001552",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "545930acf02cbe590000e7db",
  "title": "1 All Over Color With FREE Haircut",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "1 All Over Color Complete With Free Haircut. Save $25. Available by appointment only! Please call, text or email! 509.936.5726 Zbritty08@gmail.com",
  "terms": "Valid during regular business hours only. Must call to make an appointment before redeeming. Only one coupon per customer. Valid for new customers only. Cannot be combined with other offers.",
  "value": "80.0",
  "price": "55.0",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "545941eef02cbedf4a00ed00"
  ],
  "expires_at": 1447401600000,
  "user_id": "545930acf02cbe590000e7d8",
  "updated_at": 1430934031366,
  "created_at": 1415906823528,
  "locations": [
    {
      "_id": "54650607f02cbe6901001550",
      "point": [
        -117.412278,
        47.657256
      ],
      "address": {
        "_id": "54650607f02cbe6901001553",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    },
    {
      "_id": "54650607f02cbe6901001551",
      "point": [
        -117.412278,
        47.657256
      ],
      "address": {
        "_id": "54650607f02cbe6901001554",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "54650607f02cbe690100154f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54650607f02cbe09d200003b",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 110072,
    "attached_name": "10391046_1490071357934251_846711485950904676_n.jpg",
    "main": false,
    "updated_at": 1415906823528,
    "created_at": 1415906823528
  },
  "activated_at": 1415906841565
}
deals << {
  "_id": "5465078af02cbe36dd00146d",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "545930acf02cbe590000e7db",
  "title": "2 Color Highlight and FREE Haircut",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "2 Color Highlight Complete With Cut for Only $65. Savings of $45. Available by appointment only! Please call, text or email! 509.936.5726 Zbritty08@gmail.com",
  "terms": "Valid during regular business hours only. Valid only when appointment is called in beforehand. Not to be combined any with other offer. Valid only one per customer. ",
  "value": "110.0",
  "price": "65.0",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "545941eef02cbedf4a00ed00"
  ],
  "expires_at": 1447401600000,
  "user_id": "545930acf02cbe590000e7d8",
  "updated_at": 1430934043667,
  "created_at": 1415907210693,
  "locations": [
    {
      "_id": "5465078af02cbe36dd00146b",
      "point": [
        -117.412278,
        47.657256
      ],
      "address": {
        "_id": "5465078af02cbe36dd00146e",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    },
    {
      "_id": "5465078af02cbe36dd00146c",
      "point": [
        -117.412278,
        47.657256
      ],
      "address": {
        "_id": "5465078af02cbe36dd00146f",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "5465078af02cbe36dd00146a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5465078af02cbe09ca00004b",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 87276,
    "attached_name": "10712802_1490073407934046_4143635979305311340_n.jpg",
    "main": false,
    "updated_at": 1415907210693,
    "created_at": 1415907210693
  },
  "activated_at": 1415907215554
}
deals << {
  "_id": "54650a57f02cbe6da80015aa",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "545930acf02cbe590000e7db",
  "title": "3 Color Highlight and FREE Haircut",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "3 Color Highlight and Cut for $85. Total Savings of $95. Available by appointment only! Please call, text or email! 509.936.5726 Zbritty08@gmail.com",
  "terms": "Valid during regular business hours only. Offer not to be combined with any other offers/discounts. Only one per customer. Valid for New customers only. ",
  "value": "180.0",
  "price": "85.0",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "545941eef02cbedf4a00ed00"
  ],
  "expires_at": 1447401600000,
  "user_id": "545930acf02cbe590000e7d8",
  "updated_at": 1430934054789,
  "created_at": 1415907927380,
  "locations": [
    {
      "_id": "54650a57f02cbe6da80015a8",
      "point": [
        -117.412278,
        47.657256
      ],
      "address": {
        "_id": "54650a57f02cbe6da80015ab",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    },
    {
      "_id": "54650a57f02cbe6da80015a9",
      "point": [
        -117.412278,
        47.657256
      ],
      "address": {
        "_id": "54650a57f02cbe6da80015ac",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "54650a57f02cbe6da80015a7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54650a57f02cbe09c6000019",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 112990,
    "attached_name": "1908441_1490073551267365_3981329274719442435_n.jpg",
    "main": false,
    "updated_at": 1415907927379,
    "created_at": 1415907927379
  },
  "activated_at": 1415907933172
}
deals << {
  "_id": "54694780f02cbe43c80031f6",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "541f71b1f02cbe8020000e8b",
  "title": "Flavored Popcorn Basket",
  "category_id": "4f7d215e74dbb02de300060a",
  "offer": "FLAVORED POPCORN BASKETS FOR SALE!!  Spokane Popcorn has put together a POPPIN special for all you popcorn lovers!!  \r\n\r\nIf you order a $45 popcorn basket, you will get a second basket for $30.  Each basket includes three small bags of popcorn, popcorn cups, signature hand towels and other goodies. FREE local delivery and guarantee delivery by Christmas!  \r\n\r\nCall me or send me an email at spokanepopcorn@gmail.com by November 30th.  I am a travelling vendor and I currently do not have a store front.   ",
  "terms": "Valid through November 30, 2014 plus shipping if applicable",
  "value": "90.0",
  "price": "75.0",
  "limit": 25,
  "limit_per_customer": "",
  "listing_ids": [
    "54335cc1f02cbee52500034f",
    "541f725cf02cbe9065000ef5"
  ],
  "expires_at": 1417334400000,
  "user_id": "541f71b1f02cbe8020000e88",
  "updated_at": 1416259912522,
  "created_at": 1416185728555,
  "locations": [
    {
      "_id": "54694780f02cbe43c80031f4",
      "point": [
        -117.4119989,
        47.7231435
      ],
      "address": {
        "_id": "54694780f02cbe43c80031f7",
        "country": "US",
        "street": "7115 N. Division",
        "suite": "Suite B-144",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99208"
      }
    },
    {
      "_id": "54694780f02cbe43c80031f5",
      "point": [
        -117.4644801,
        47.7872368
      ],
      "address": {
        "_id": "54694780f02cbe43c80031f8",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99208"
      }
    }
  ],
  "image": {
    "_id": "54694780f02cbe43c80031f3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54694780f02cbe09ca0000e1",
    "attached_width": 3264,
    "attached_height": 2448,
    "attached_size": 2164031,
    "attached_name": "basket wrapped.jpg",
    "main": false,
    "updated_at": 1416185728554,
    "created_at": 1416185728554
  },
  "activated_at": 1416185858388
}
deals << {
  "_id": "546aad12f02cbec30b00008f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "541f71b1f02cbe8020000e8b",
  "title": "Flavored Popcorn Basket",
  "category_id": "4f7d215e74dbb02de300060a",
  "offer": "FLAVORED POPCORN BASKETS FOR SALE!! Spokane Popcorn has put together a POPPIN special for all you popcorn lovers!! \r\n\r\nEach basket includes three small bags of popcorn, popcorn cups, signature hand towels and other goodies. \r\n\r\nCurrent flavors available:  chocolate caramel, caramel, bacon cheddar, parmesan garlic, sea salt & pepper and always ordering flavors.  \r\n\r\nFREE local delivery and guarantee delivery by Christmas! \r\n\r\nCall me at 509-218-3672 or send me an email at spokanepopcorn@gmail.com. I am a travelling vendor and I currently do not have a store front. \r\n",
  "terms": "Valid through December 31, 2014",
  "value": "45.00",
  "price": "35.00",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "54335cc1f02cbee52500034f"
  ],
  "expires_at": 1420012800000,
  "user_id": "541f71b1f02cbe8020000e88",
  "updated_at": 1416277273733,
  "created_at": 1416277266778,
  "locations": [
    {
      "_id": "546aad12f02cbec30b00008e",
      "point": [
        -117.4644801,
        47.7872368
      ],
      "address": {
        "_id": "546aad12f02cbec30b000090",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99208"
      }
    }
  ],
  "image": {
    "_id": "546aad12f02cbec30b00008d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "546aad12f02cbe782e000003",
    "attached_width": 3264,
    "attached_height": 2448,
    "attached_size": 2164031,
    "attached_name": "basket wrapped.jpg",
    "main": false,
    "updated_at": 1416277266778,
    "created_at": 1416277266778
  },
  "activated_at": 1416277273715
}
deals << {
  "_id": "546cdd60f02cbe3a0f000c45",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "53dab96ff02cbe466d001cde",
  "title": "20% Off Home Brew Beer Equipment Kit",
  "category_id": "4f7d215d74dbb02de30005f9",
  "offer": "20% Off Home Brew Beer Equipment Kit, Normally $84.95. All Of The Supplies You Need to Start Making Your Own Home Brew!",
  "terms": "Valid during regular business hours only. Not to be combined with any other offer. Must be 21+ years of age.",
  "value": "84.95",
  "price": "67.96",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "53d41b57f02cbebade0034fe"
  ],
  "expires_at": 1451635200000,
  "user_id": "53dab96ff02cbe466d001cdf",
  "updated_at": 1444156026692,
  "created_at": 1416420704427,
  "locations": [
    {
      "_id": "56141347f02cbe1610000001",
      "point": [
        -116.999991,
        47.711402
      ],
      "address": {
        "_id": "56141347f02cbe1610000002",
        "country": "US",
        "street": "3920 West 5th Avenue,",
        "suite": "A3,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "546cdd60f02cbe3a0f000c43",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "546cdd60f02cbe7826000003",
    "attached_width": 244,
    "attached_height": 300,
    "attached_size": 116500,
    "attached_name": "Two Knights Homebrew Logo 8.jpg",
    "main": false,
    "updated_at": 1416420704426,
    "created_at": 1416420704426
  },
  "activated_at": 1416420707414
}
deals << {
  "_id": "547f547df02cbe2e4a004cd4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "546e6a7af02cbe10de0001ae",
  "title": "50% off on dozen Cupcakes!",
  "category_id": "4f7d215e74dbb02de300062d",
  "offer": "Buy 6 cupcakes, get 6 for FREE!  50% off!  A Dozen cupcakes for only $21.54, regularly $43.08.\r\n",
  "terms": "For best selection, AFTER purchasing Deal, please call (208 762-5700 and order ahead.\r\nFor Pick Up Only.  Mon 12:00 pm - 6:00 pm, Tues - Sat 10:30 am - 6:00 pm, Closed Sundays.\r\nTemptations! \"It's all Good\".",
  "value": "43.08",
  "price": "21.54",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "546e7930f02cbeb9f40001c4"
  ],
  "expires_at": 1420444800000,
  "user_id": "54738ba9f02cbeb8f8000dd6",
  "updated_at": 1417635173018,
  "created_at": 1417630845355,
  "locations": [
    {
      "_id": "547f547df02cbe2e4a004cd3",
      "point": [
        -116.7885298,
        47.7455945
      ],
      "address": {
        "_id": "547f547df02cbe2e4a004cd5",
        "country": "US",
        "street": "105 W. Prairie Shopping Center,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "547f547df02cbe2e4a004cd2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "547f547df02cbe2b7300005e",
    "attached_width": 747,
    "attached_height": 713,
    "attached_size": 64350,
    "attached_name": "31032_386412091439271_1630275059_n.jpg",
    "main": false,
    "updated_at": 1417630845355,
    "created_at": 1417630845355
  },
  "activated_at": 1417632799172
}
deals << {
  "_id": "54809e30f02cbeeff7005403",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53519450f02cbe1b2d005909",
  "title": "ATV Oil Change for only $39.95, Reg. $59.95!",
  "category_id": "4f7d216774dbb02de3000a6b",
  "offer": "SPECIAL!  ATV Oil Change for Only $39.95, Reg. $59.95.",
  "terms": "Includes 2.5 quarts of Belray Conventional Oil & Oil Filter, (does not include inspection or tax.",
  "value": "59.95",
  "price": "39.95",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53519682f02cbe6a8300619f"
  ],
  "expires_at": 1420012800000,
  "user_id": "53519450f02cbe1b2d00590a",
  "updated_at": 1417715283453,
  "created_at": 1417715248243,
  "locations": [
    {
      "_id": "54809e30f02cbeeff7005402",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -116.78727,
        47.761455
      ],
      "address": {
        "_id": "54809e30f02cbeeff7005404",
        "country": "US",
        "street": "9873 North Government Way,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "54809e30f02cbeeff7005401",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54809e30f02cbe2b7800004e",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 108919,
    "attached_name": "1620874_455337974567779_986329706_n.jpg",
    "main": false,
    "updated_at": 1417715248243,
    "created_at": 1417715248243
  },
  "activated_at": 1417715283443
}
deals << {
  "_id": "54860bbff02cbe1e850090ed",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "538e4d2f9c058e2308000ebd",
  "title": "$10.36 (SAVE $5.59 14\" Specialty Pizza! (regular price $15.95",
  "category_id": "4f7d215e74dbb02de3000651",
  "offer": "ONE 14\" Wildcat Specialty Pizza for just $10.36 +Tax. SAVE $5.59 (35% Savings.",
  "terms": "Expires January 31, 2015. Limit 1 per person, May buy 1 additional as Gift. Limit 1 per table. Limit 1 per visit. Must purchase a food item listed on Voucher. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "value": "15.95",
  "price": "10.36",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "538e599e9c058e44da000f50"
  ],
  "expires_at": 1422691200000,
  "user_id": "538e4d2f9c058e2308000ebe",
  "updated_at": 1422468575384,
  "created_at": 1418070975441,
  "locations": [
    {
      "_id": "54860fb7f02cbe8ebf00899c",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.7881558,
        47.7457948
      ],
      "address": {
        "_id": "54860fb7f02cbe8ebf00899d",
        "country": "US",
        "street": "85 W. Prairie Shopping Center,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "54860bbff02cbe1e850090eb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54860bbff02cbe2b73000089",
    "attached_width": 200,
    "attached_height": 200,
    "attached_size": 18448,
    "attached_name": "Pizza Loaded4 Mushrooms.jpg",
    "main": false,
    "updated_at": 1418070975441,
    "created_at": 1418070975441
  },
  "activated_at": 1418071013780
}
deals << {
  "_id": "54860cacf02cbe9aad008eb4",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "538e4d2f9c058e2308000ebd",
  "title": " $13.62 for ONE 18\" Wildcat Specialty Pizza! SAVE $7.33 ( Save 35%",
  "category_id": "4f7d215e74dbb02de3000651",
  "offer": "ONE 18\" Wildcat Specialty Pizza for just $13.62 + tax. SAVE 35%!",
  "terms": "Expires January 31, 2015. Limit 1 per person, May buy 1 additional as Gift. Limit 1 per table. Limit 1 per visit. Must purchase a food item listed on Voucher. Must use promotional value in 1 visit. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "value": "20.95",
  "price": "13.62",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "538e599e9c058e44da000f50"
  ],
  "expires_at": 1422691200000,
  "user_id": "538e4d2f9c058e2308000ebe",
  "updated_at": 1422468592168,
  "created_at": 1418071212608,
  "locations": [
    {
      "_id": "54861029f02cbe52e9008a21",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -116.7881558,
        47.7457948
      ],
      "address": {
        "_id": "54861029f02cbe52e9008a22",
        "country": "US",
        "street": "85 W. Prairie Shopping Center,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "54860cacf02cbe9aad008eb1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54860cacf02cbe2b6b000083",
    "attached_width": 200,
    "attached_height": 200,
    "attached_size": 22146,
    "attached_name": "Pizza Loaded.jpg",
    "main": false,
    "updated_at": 1418071212608,
    "created_at": 1418071212608
  },
  "activated_at": 1418071222511
}
deals << {
  "_id": "5489d5d7f02cbeea9a00f888",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5489cea2f02cbe0ef300f904",
  "title": "Receive 5lbs of feed with the purchase of a Rabbit ($33 value",
  "category_id": "4f7d215774dbb02de300030a",
  "offer": "Buy a rabbit for $30 or more and receive 5lbs of feed for free.",
  "terms": "",
  "value": "33.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5489d435f02cbe826900f5f4"
  ],
  "expires_at": 1451635200000,
  "user_id": "5489cea2f02cbe0ef300f901",
  "updated_at": 1426012982364,
  "created_at": 1418319319698,
  "locations": [
    {
      "_id": "5489d5d7f02cbeea9a00f887",
      "point": [
        -117.111917,
        47.704798
      ],
      "address": {
        "_id": "5489d5d7f02cbeea9a00f889",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "otis orchards",
        "region": "WA",
        "postal_code": "99027"
      }
    }
  ],
  "activated_at": 1418319324492,
  "image": {
    "_id": "5489e0dcf02cbe5b4a00f9e8",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5489e0dcf02cbe2b6f000092",
    "attached_width": 1370,
    "attached_height": 1523,
    "attached_size": 464222,
    "attached_name": "photo 5 (4.JPG",
    "main": false,
    "updated_at": 1418322140824,
    "created_at": 1418322140824
  },
  "referral_code": ""
}
deals << {
  "_id": "54d3b08df02cbe9f5100b712",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "538e04b59c058e34c4000bb9",
  "title": "Special Deal!",
  "category_id": "4f7d215974dbb02de30003e9",
  "offer": "Get $100.00 worth of Bid lawn Service for only $60.00!",
  "terms": "-Not to Exceed Spokane County, Spokane City, and Spokane Valley City Limits.\r\n-Limit ONE MarketPad Voucher Per Customer Per Visit.\r\n-Purchase Limit 1 Per Person, 5 Additional as Gift.\r\n-Cannot Be Combined With Any Other Deal or Offer.\r\n-MUST Present MarketPad Voucher PRIOR To Services & Product Ordered.\r\n-MUST Call Ahead for Appointment with MarketPad Voucher Number Given.\r\n-Voucher Discount Expires 30 Days After Purchase.\r\n-No Food or Beverages Included.\r\n-Merchant is Solely Responsible to Purchasers For The Care And Quality Of The Advertised Goods And Services.\r\n-Appointment Required.\r\n-Subject to Availability & Weather.\r\n-Must Be 18+.",
  "value": "100.0",
  "price": "60.0",
  "limit": 25,
  "limit_per_customer": "",
  "listing_ids": [
    "538e075f9c058e7a03000b24",
    "538e05939c058e8efb000bcd"
  ],
  "expires_at": 1451548800000,
  "user_id": "538e04b59c058e34c4000bba",
  "updated_at": 1423159439861,
  "created_at": 1423159437024,
  "locations": [
    {
      "_id": "54d3b08cf02cbe9f5100b710",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -117.43979,
        47.69399
      ],
      "address": {
        "_id": "54d3b08df02cbe9f5100b713",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    },
    {
      "_id": "54d3b08cf02cbe9f5100b711",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -117.43979,
        47.69399
      ],
      "address": {
        "_id": "54d3b08df02cbe9f5100b714",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "54d3b08cf02cbe9f5100b70f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54d3b08cf02cbe25d20000a3",
    "attached_width": 155,
    "attached_height": 159,
    "attached_size": 5070,
    "attached_name": "GetAttachment (2.jpg",
    "main": false,
    "updated_at": 1423159437023,
    "created_at": 1423159437023
  },
  "activated_at": 1423159439853
}
deals << {
  "_id": "54f8dda4f02cbe45db001bb1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ee2196f02cbe319d004823",
  "title": "HEADLIGHT CLEANING GEL ",
  "category_id": "4f7d216774dbb02de3000a53",
  "offer": "Clear Headlight Gel removes oxidation in less than 20 seconds with just a few drops. It attacks oxidation at the molecular level and removes the cloudiness and improves the over all visibility. There is no need for Sanding or extra power tools. Clear Headlight Gel works on contact.",
  "terms": "",
  "value": "12.99",
  "price": "6.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54ee28f6f02cbe85ea003ed5"
  ],
  "expires_at": 1435730400000,
  "user_id": "54ef6a31f02cbebcbd004c65",
  "updated_at": 1425595812516,
  "created_at": 1425595812516,
  "locations": [
    {
      "_id": "54f8dda4f02cbe45db001bb0",
      "point": [
        -111.610635,
        40.165955
      ],
      "address": {
        "_id": "54f8dda4f02cbe45db001bb2",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Springville",
        "region": "UT",
        "postal_code": "84663"
      }
    }
  ],
  "image": {
    "_id": "54f8dda4f02cbe45db001baf",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "54f8dda4f02cbe5cf1000043",
    "attached_width": 799,
    "attached_height": 1126,
    "attached_size": 350360,
    "attached_name": "Clear Headlight Gel Product Shot.jpg",
    "main": false,
    "updated_at": 1425595812516,
    "created_at": 1425595812516
  }
}
deals << {
  "_id": "55270845f02cbe1576001376",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54f0d7aff02cbe2b66005858",
  "title": "Buy 3 Music Lessons, Get 4th  & 5th Lesson FREE",
  "category_id": "57437c27e9330c5bc1000a5e",
  "offer": "ONLY $60 to Buy 3 Music Lessons, and Get 4th& 5th Lesson FREE | Normally $100",
  "terms": "",
  "value": "100.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": 3,
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "4fb5539508dea13806000219",
  "updated_at": 1477448216849,
  "created_at": 1428621381411,
  "locations": [
    {
      "_id": "55270845f02cbe1576001375",
      "point": [
        -117.43471,
        47.673764
      ],
      "address": {
        "_id": "55270845f02cbe1576001377",
        "country": "US",
        "street": "1513 W. NORA",
        "suite": "",
        "city": "SPOKANE",
        "region": "WA ",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "55270845f02cbe1576001374",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55270845f02cbe730a00003b",
    "attached_width": 480,
    "attached_height": 480,
    "attached_size": 28056,
    "attached_name": "kichopromo3.jpg",
    "main": false,
    "updated_at": 1428621381410,
    "created_at": 1428621381410
  },
  "activated_at": 1428621385158,
  "referral_code": ""
}
deals << {
  "_id": "552d8803f02cbeb7880007d5",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$50 FAMILY BURGER DEAL FOR $39.99",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "$50 SCHMIDTY'S BURGERS FAMILY DEAL.  BUY A $50 DEAL COUPON FOR $39.99.  A 20% DISCOUNT",
  "terms": "$50 DEAL FOR $39.99  GOOD DURING REGULAR BUSINESS HOURS.  EXPIRES SEPTEMBER 8TH 2015",
  "value": "50.0",
  "price": "39.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1441695600000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1431377059853,
  "created_at": 1429047299607,
  "locations": [
    {
      "_id": "552d8803f02cbeb7880007d4",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "552d8803f02cbeb7880007d6",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1429047349196,
  "referral_code": "currentattractions",
  "image": {
    "_id": "55304358f02cbe17e50000ce",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55304358f02cbe0664000042",
    "attached_width": 960,
    "attached_height": 717,
    "attached_size": 85368,
    "attached_name": "schmidtysmarketpad3.jpg",
    "main": false,
    "updated_at": 1429226328214,
    "created_at": 1429226328214
  },
  "deleted_at": 1431377955691
}
deals << {
  "_id": "552d8906f02cbe528c000925",
  "active": false,
  "vouchers_count": 4,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$25 GRUMPY BURGER DEAL",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "PURCHASE 2 GRUMPY BURGERS WITH FRIES AND 2 DRINKS NON ALCOHOLIC.  ",
  "terms": "$25 GRUMPY BURGER DEAL FOR $19.99. 2 GRUMPY BURGERS WITH FRIES AND 2 NON ALCOHOLIC DRINKS FOR $19.99  GOOD DURING REGULAR BUSINESS HOURS.  EXPIRES OCTOBER 30TH 2015.  CANNOT BE USED WITH ANY OTHER DISCOUNTS.",
  "value": "25.0",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1441695600000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1463695192947,
  "created_at": 1429047558074,
  "locations": [
    {
      "_id": "552d8906f02cbe528c000924",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "552d8906f02cbe528c000926",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "552d8906f02cbe528c000923",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "552d8906f02cbe4496000001",
    "attached_width": 960,
    "attached_height": 681,
    "attached_size": 102034,
    "attached_name": "schmidtysmarketpad.jpg",
    "main": false,
    "updated_at": 1429047558073,
    "created_at": 1429047558073
  },
  "activated_at": 1429047589012,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "55368d91f02cbec07b0033b8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7259d08dea126b9004824",
  "title": "(3 Tannin Sessions | 12 Minute Bed ",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": "SAVE 28% off the regular price! (3 sessions on our 12 minute bed for just $26.  SAVE $10 to look your best this summer! ",
  "terms": "PLEASE call (208 457-9700 for appointment and SHOW your MarketPad DEAL before you tan session.",
  "value": "36.0",
  "price": "26.0",
  "limit": 40,
  "limit_per_customer": 2,
  "listing_ids": [
    "4fb7259d08dea126b9004823"
  ],
  "expires_at": 1485936000000,
  "user_id": "4fb5539b08dea138060003f1",
  "updated_at": 1485233187630,
  "created_at": 1429638545755,
  "locations": [
    {
      "_id": "55368d91f02cbec07b0033b7",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -116.8923077246066,
        47.71665274085393
      ],
      "address": {
        "_id": "55368d91f02cbec07b0033b9",
        "country": "US",
        "city": "Post Falls",
        "postal_code": "83854",
        "region": "ID",
        "street": "900 N. Highway 41,",
        "suite": "9,"
      }
    }
  ],
  "image": {
    "_id": "55368d91f02cbec07b0033b6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55368d91f02cbe066100005c",
    "attached_width": 356,
    "attached_height": 240,
    "attached_size": 101244,
    "attached_name": "tan5.jpg",
    "main": false,
    "updated_at": 1429638545754,
    "created_at": 1429638545754
  },
  "activated_at": 1429638548653,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "5536a7d7f02cbe35c30039af",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "536295a6f02cbeae11006d6b",
  "title": "Deep Tissue Massage",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": "$40 for one 60-minute Deep Tissue Massage, (an $80 value\r\n\r\nLone Wolf Therapies located in Spokane Valley WA\r\n(509 228-3772\r\n\r\nMust Present Deal at your appointment.",
  "terms": "Limit 1 per person.  Appointment required. 24-hr cancellation notice or fee may apply. Services must be used by the same person.",
  "value": "80.0",
  "price": "40.0",
  "limit": 40,
  "limit_per_customer": 1,
  "listing_ids": [
    "536299fff02cbe758b005f03"
  ],
  "expires_at": 1546243200000,
  "user_id": "536295a6f02cbeae11006d6c",
  "updated_at": 1486071832245,
  "created_at": 1429645271786,
  "locations": [
    {
      "_id": "5536a7d7f02cbe35c30039ae",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -117.2859793,
        47.6718784
      ],
      "address": {
        "_id": "5536a7d7f02cbe35c30039b0",
        "country": "US",
        "street": "8817 E. Mission Ave.,",
        "suite": "105,",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5536a7d7f02cbe35c30039ad",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5536a7d7f02cbe066400009e",
    "attached_width": 310,
    "attached_height": 240,
    "attached_size": 12978,
    "attached_name": "massage5-310x240.jpg",
    "main": false,
    "updated_at": 1429645271786,
    "created_at": 1429645271786
  },
  "activated_at": 1429645464102,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "553ea042f02cbee6f500a145",
  "active": true,
  "vouchers_count": 46,
  "profile_id": "550b3260f02cbe892e000e24",
  "title": "BATTLE OF THE HOMEBREWS",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "2nd Annual BATTLE OF THE HOMEBREWS COMPETITION. Saturday May 16th at the Spokane Fair and Expo! $30.00 which is $10.00 off for Two Admissions which includes 2 voting cards which includes 5 samples of HomeBrew Craft Beer on each card if you choose to sample.",
  "terms": "This deal ends on Saturday May 16th, 2015. ",
  "value": "40.0",
  "price": "30.0",
  "limit": 1000,
  "limit_per_customer": 100,
  "listing_ids": [
    "550b36eef02cbe22e4000e02"
  ],
  "expires_at": 1431759600000,
  "user_id": "551451bbf02cbeaf22002ba4",
  "updated_at": 1430327106729,
  "created_at": 1430167618032,
  "locations": [
    {
      "_id": "553ea042f02cbee6f500a144",
      "point": [
        -117.3427875,
        47.6623434
      ],
      "address": {
        "_id": "553ea042f02cbee6f500a146",
        "country": "US",
        "street": "404 N. Havana St.,",
        "suite": "1",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99202"
      }
    }
  ],
  "activated_at": 1430167629680,
  "image": {
    "_id": "553eb940f02cbe34a800a77a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "553eb940f02cbe065a0000ec",
    "attached_width": 200,
    "attached_height": 130,
    "attached_size": 5133,
    "attached_name": "1458456_564955890240170_292841758_n.jpg",
    "main": false,
    "updated_at": 1430174016056,
    "created_at": 1430174016056
  },
  "referral_code": "bvo9552"
}
deals << {
  "_id": "553eb286f02cbee6d6009ae0",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "553eacb7f02cbec3b200a225",
  "title": "Massage and Facial (90 min",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "Indulge in total relaxation! A rejuvenating  full body Massage to help with all your aches, pains and stress. And ended with a luxurious Spa Facial to top of the treatment of a life time while helping with all you skincare needs! ",
  "terms": "must be used by July 31st, 2015",
  "value": "85.00",
  "price": "60.00",
  "limit": 15,
  "limit_per_customer": 2,
  "listing_ids": [
    "553eb165f02cbeb43300a2aa"
  ],
  "expires_at": 1438326000000,
  "user_id": "553eac43f02cbe028000a209",
  "updated_at": 1430173416351,
  "created_at": 1430172294079,
  "locations": [
    {
      "_id": "553eb286f02cbee6d6009adf",
      "point": [
        -114.659409,
        44.389028
      ],
      "address": {
        "_id": "553eb286f02cbee6d6009ae1",
        "country": "US",
        "street": "124 W. Dakota",
        "suite": "At GTOs",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "activated_at": 1430172302613
}
deals << {
  "_id": "553eb605f02cbee83a009f50",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "553eacb7f02cbec3b200a225",
  "title": "Full set of lash extensions ",
  "category_id": "4f7d215974dbb02de3000426",
  "offer": "A glamorous full set of lash extensions that will enhance you eyes natural beauty! No mascara need so these are a great addition to lake season!",
  "terms": "Must be used by July 31st, 2015",
  "value": "95.0",
  "price": "55.0",
  "limit": 10,
  "limit_per_customer": 1,
  "listing_ids": [
    "553eb165f02cbeb43300a2aa",
    "553eb10bf02cbebf4400a297",
    "553eafd0f02cbeb62400a27d"
  ],
  "expires_at": 1438326000000,
  "user_id": "553eac43f02cbe028000a209",
  "updated_at": 1431470394598,
  "created_at": 1430173189026,
  "locations": [
    {
      "_id": "553fad9bf02cbe06f800bce9",
      "point": [
        -114.659409,
        44.389028
      ],
      "address": {
        "_id": "553fad9bf02cbe06f800bcea",
        "country": "US",
        "street": "124 W. Dakota",
        "suite": "At GTOs",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "553fad9bf02cbe06f800bceb",
      "point": [
        -114.659409,
        44.389028
      ],
      "address": {
        "_id": "553fad9bf02cbe06f800bcec",
        "country": "US",
        "street": "124 W. Dakota",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "553fad9bf02cbe06f800bced",
      "point": [
        -116.788434,
        47.76238799999999
      ],
      "address": {
        "_id": "553fad9bf02cbe06f800bcee",
        "country": "US",
        "street": "124 W. Dakota Ave.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "553eb604f02cbee83a009f4e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "553eb604f02cbe065e00010e",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 81272,
    "attached_name": "image.jpg",
    "main": false,
    "updated_at": 1430173189025,
    "created_at": 1430173189025
  },
  "activated_at": 1430173217003,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "553eb8aef02cbe620400a37f",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "553eacb7f02cbec3b200a225",
  "title": "Massage and Facial Combination ",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": "Indulge in complete relaxation! A rejuvenating full body massage ended with a luxurious spa facial will help melt away those aches, pains, and stress while assisting in your skincare needs.",
  "terms": "Must be used by July 31st, 2015",
  "value": "85.0",
  "price": "60.0",
  "limit": 15,
  "limit_per_customer": 2,
  "listing_ids": [
    "553eb165f02cbeb43300a2aa",
    "553eb10bf02cbebf4400a297",
    "553eafd0f02cbeb62400a27d"
  ],
  "expires_at": 1438326000000,
  "user_id": "553eac43f02cbe028000a209",
  "updated_at": 1431470404155,
  "created_at": 1430173870383,
  "locations": [
    {
      "_id": "553fadf3f02cbe72dd00c2c4",
      "point": [
        -114.659409,
        44.389028
      ],
      "address": {
        "_id": "553fadf3f02cbe72dd00c2c5",
        "country": "US",
        "street": "124 W. Dakota",
        "suite": "At GTOs",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "553fadf3f02cbe72dd00c2c6",
      "point": [
        -114.659409,
        44.389028
      ],
      "address": {
        "_id": "553fadf3f02cbe72dd00c2c7",
        "country": "US",
        "street": "124 W. Dakota",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    },
    {
      "_id": "553fadf3f02cbe72dd00c2c8",
      "point": [
        -116.788434,
        47.76238799999999
      ],
      "address": {
        "_id": "553fadf3f02cbe72dd00c2c9",
        "country": "US",
        "street": "124 W. Dakota Ave.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "553eb8aef02cbe620400a37d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "553eb8aef02cbe0664000167",
    "attached_width": 640,
    "attached_height": 403,
    "attached_size": 54400,
    "attached_name": "image.jpg",
    "main": false,
    "updated_at": 1430173870383,
    "created_at": 1430173870383
  },
  "activated_at": 1430173877556,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "553f2b1ff02cbe7deb00abef",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "THREE Newborn Bonnets! ",
  "category_id": "4f7d216974dbb02de3000ae7",
  "offer": "Three cotton bonnets at a great price! \r\nThese bonnets are handmade with love by me for your sweet babes! \r\n\r\nGreat item for newborn photographers! \r\n\r\nCan also be used to keep the sun off your babies head in the summer or keeping those little ears warm in the winter!  \r\n\r\nCan be made in multiple colors. Please specify colors when checking out! \r\nFrom left to right: Violet, Lavender, Forest, Denim, Grey, Smoke, Burgundy and Dusty Rose\r\n\r\nCobb's Creations\r\nkenzieway18@gmail.com",
  "terms": "",
  "value": "24.0",
  "price": "10.0",
  "limit": 25,
  "limit_per_customer": 25,
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "expires_at": "",
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1477339555811,
  "created_at": 1430203167823,
  "locations": [
    {
      "_id": "553f2b1ff02cbe7deb00abed",
      "point": [
        -122.5153199,
        47.17839739999999
      ],
      "address": {
        "_id": "553f2b1ff02cbe7deb00abf0",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Lakewood",
        "region": "WA",
        "postal_code": "98499"
      }
    },
    {
      "_id": "553f2b1ff02cbe7deb00abee",
      "point": [
        -116.7909562084097,
        47.69932388913045
      ],
      "address": {
        "_id": "553f2b1ff02cbe7deb00abf1",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1430203174650,
  "referral_code": "currentattractions",
  "image": {
    "_id": "57fffede88682ecf29004a43",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57fffede88682e0001000058",
    "attached_width": 960,
    "attached_height": 960,
    "attached_size": 114854,
    "attached_name": "12806194_1711230502451858_210718748178993818_n.jpg",
    "main": false,
    "updated_at": 1476394718380,
    "created_at": 1476394718380
  },
  "deleted_at": 1485232524533
}
deals << {
  "_id": "553faf52f02cbe750000c309",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "550b3260f02cbe892e000e24",
  "title": "beer",
  "category_id": "4f7d216674dbb02de30009bc",
  "offer": "beer",
  "terms": "",
  "value": "20",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "550b36eef02cbe22e4000e02"
  ],
  "expires_at": "",
  "user_id": "551451bbf02cbeaf22002ba4",
  "updated_at": 1430237010530,
  "created_at": 1430237010530,
  "locations": [
    {
      "_id": "553faf52f02cbe750000c308",
      "point": [
        -117.3427875,
        47.6623434
      ],
      "address": {
        "_id": "553faf52f02cbe750000c30a",
        "country": "US",
        "street": "404 N. Havana St.,",
        "suite": "1",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99202"
      }
    }
  ],
  "image": {
    "_id": "553faf52f02cbe750000c307",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "553faf52f02cbe066400016b",
    "attached_width": 466,
    "attached_height": 261,
    "attached_size": 24344,
    "attached_name": "tawnyawysonglogo2.jpg",
    "main": false,
    "updated_at": 1430237010530,
    "created_at": 1430237010530
  },
  "deleted_at": 1430237035488
}
deals << {
  "_id": "5550f2abf02cbeda61002f2f",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$15 FAMILY BURGER DEAL FOR $11.99",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "$15 FAMILY BURGER DEAL FOR $11.99, EXPIRES SEPTEMBER 8TH 2015",
  "terms": "EXPIRES SEPTEMBER 8TH 2015.  VALID DURING REGULAR BUSINESS HOURS",
  "value": "15.0",
  "price": "11.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1441695600000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1431377628046,
  "created_at": 1431368363684,
  "locations": [
    {
      "_id": "5550f2abf02cbeda61002f2e",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "5550f2abf02cbeda61002f30",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5550f2abf02cbeda61002f2d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5550f2abf02cbe46bb000037",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 106923,
    "attached_name": "schmidtysmarketpad4.jpg",
    "main": false,
    "updated_at": 1431368363683,
    "created_at": 1431368363683
  },
  "activated_at": 1431368374121,
  "deleted_at": 1431377963831
}
deals << {
  "_id": "557fbc9ff02cbe212400035a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "BUY 2 get 1 free Hats or Bonnets! ",
  "category_id": "4f7d216974dbb02de3000ae7",
  "offer": "I am offering ALL my hats and bonnet buy 2 get 1 free! \r\nI can do any thing from newborn to adult sizes! \r\nIn almost any color you can think of! \r\nUp to three colors per hat/bonnet. \r\n\r\nAll hats and bonnets will e made with a cotton/acrylic yarn blend. ",
  "terms": "",
  "value": "24.0",
  "price": "16.0",
  "limit": 20,
  "limit_per_customer": 20,
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "expires_at": 1472626800000,
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1457970950171,
  "created_at": 1434434719902,
  "locations": [
    {
      "_id": "557fbc9ff02cbe2124000358",
      "point": [
        -122.5153199,
        47.17839739999999
      ],
      "address": {
        "_id": "557fbc9ff02cbe212400035b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Lakewood",
        "region": "WA",
        "postal_code": "98499"
      }
    },
    {
      "_id": "557fbc9ff02cbe2124000359",
      "point": [
        -116.7909562084097,
        47.69932388913045
      ],
      "address": {
        "_id": "557fbc9ff02cbe212400035c",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "557fbc9ff02cbe2124000357",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "557fbc9ff02cbe7dc3000003",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 112985,
    "attached_name": "10665917_1515635092011401_5782776130348695402_n.jpg",
    "main": false,
    "updated_at": 1434434719902,
    "created_at": 1434434719902
  },
  "activated_at": 1434434727395,
  "deleted_at": 1485232504145
}
deals << {
  "_id": "55a560c3f02cbe5004003ee3",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "2 Mustang Raffle Tickets for $10.00 from the Rathdrum Lions Club",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "2 Mustang Raffle Tickets for $10.00 from the Rathdrum Lions Club\r\nWe raffle off a classic 1965 Mustang from April to the day before Labor Day.\r\n\r\n-- The winning ticket will be drawn at noon on Labor Day, Sept., 7, 2015.\r\n\r\n-- You do not have to be present to win!\r\n\r\n--The Rathdrum Lions Club is Non-Profit organization. 100% of money raised supports local people in need.\r\n\r\n** When Raffle Tickets are purchased, MarketPad will email your ticket numbers within the next business day.",
  "terms": "Must be 18 or over to purchase Raffle Ticket.",
  "value": "10.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1441609200000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1441397424596,
  "created_at": 1436901571278,
  "locations": [
    {
      "_id": "55a560c3f02cbe5004003ee2",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55a560c3f02cbe5004003ee4",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "activated_at": 1436901734076,
  "image": {
    "_id": "55a56801f02cbe63c8003e42",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55a56801f02cbe3ab900001d",
    "attached_width": 2608,
    "attached_height": 2472,
    "attached_size": 644634,
    "attached_name": "email flyer 4.jpg",
    "main": false,
    "updated_at": 1436903426108,
    "created_at": 1436903426108
  }
}
deals << {
  "_id": "55a567d7f02cbe55e6003f24",
  "active": false,
  "vouchers_count": 8,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "5 Mustang Raffle Tickets for only $20.00!",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "2 Mustang Raffle Tickets for $10.00 from the Rathdrum Lions Club \r\nWe raffle off a classic 1965 Mustang from April to the day before Labor Day.\r\n\r\n-- The winning ticket will be drawn at noon on Labor Day, Sept., 7, 2015.\r\n\r\n-- You do not have to be present to win!\r\n\r\n--The Rathdrum Lions Club is Non-Profit organization. 100% of money raised supports local people in need.\r\n\r\n** When Raffle Tickets are purchased, MarketPad will email your ticket numbers within the next business day.",
  "terms": "Must be 18 or over to purchase Raffle Ticket.",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1441609200000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1441398259707,
  "created_at": 1436903383458,
  "locations": [
    {
      "_id": "55a567d7f02cbe55e6003f23",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55a567d7f02cbe55e6003f25",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "55a567d7f02cbe55e6003f22",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55a567d7f02cbe3ab500001f",
    "attached_width": 2608,
    "attached_height": 2472,
    "attached_size": 644634,
    "attached_name": "email flyer 4.jpg",
    "main": false,
    "updated_at": 1436903383458,
    "created_at": 1436903383458
  },
  "activated_at": 1436903397480
}
deals << {
  "_id": "55ae9055f02cbece50009f99",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "10 Mustang Raffle Tickets for only $40.00!",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "10 Mustang Raffle Tickets for $40.00 from the Rathdrum Lions Club.\r\nWe raffle off a classic 1965 Mustang from April to the day before Labor Day.\r\n\r\n-- The winning ticket will be drawn at noon on Labor Day, Sept., 7, 2015.\r\n\r\n-- You do not have to be present to win!\r\n\r\n--The Rathdrum Lions Club is Non-Profit organization. 100% of money raised supports local people in need.",
  "terms": "Must be 18 or over to purchase Raffle Ticket.\r\n** When Raffle Tickets are purchased, MarketPad will email your ticket numbers within the next business day.",
  "value": "50.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1441609200000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1441398285327,
  "created_at": 1437503573083,
  "locations": [
    {
      "_id": "55ae9054f02cbece50009f98",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55ae9055f02cbece50009f9a",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "55ae9054f02cbece50009f97",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55ae9054f02cbe3ab5000042",
    "attached_width": 2608,
    "attached_height": 2472,
    "attached_size": 644634,
    "attached_name": "email flyer 4.jpg",
    "main": false,
    "updated_at": 1437503573083,
    "created_at": 1437503573083
  },
  "activated_at": 1437504009413
}
deals << {
  "_id": "55ae9138f02cbea2930097c5",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "20 Mustang Raffle Tickets for only $80.00!",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "20 Mustang Raffle Tickets for $80.00 from the Rathdrum Lions Club \r\nWe raffle off a classic 1965 Mustang from April to the day before Labor Day.\r\n\r\n-- The winning ticket will be drawn at noon on Labor Day, Sept., 7, 2015.\r\n\r\n-- You do not have to be present to win!\r\n\r\n--The Rathdrum Lions Club is Non-Profit organization. 100% of money raised supports local people in need.",
  "terms": "Must be 18 or over to purchase Raffle Ticket.\r\n** When Raffle Tickets are purchased, MarketPad will email your ticket numbers within the next business day.",
  "value": "100.0",
  "price": "80.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1441609200000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1441398313996,
  "created_at": 1437503800178,
  "locations": [
    {
      "_id": "55ae9138f02cbea2930097c4",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55ae9138f02cbea2930097c6",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "55ae9138f02cbea2930097c3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55ae9138f02cbe3ac1000040",
    "attached_width": 2608,
    "attached_height": 2472,
    "attached_size": 644634,
    "attached_name": "email flyer 4.jpg",
    "main": false,
    "updated_at": 1437503800177,
    "created_at": 1437503800177
  },
  "activated_at": 1437504030533
}
deals << {
  "_id": "55afc840f02cbea1b800a607",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "Donate $15.00 to the Rathdrum Lions Club",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "We All Love To Help Others.\r\nThe Rathdrum Lions Club of Idaho is one of those charities who is always trying to help the whole rather than the individual.\r\n100% of money raised with their charity programs goes to aiding local people.\r\nA few examples are:  Eyeglasses and Hearing Aids, Nutritional Backpack Program, Scholarships, Free Firewood, Leader Dogs for the Blind, plus so much more.",
  "terms": "We are a 501(c3 non-profit organization.",
  "value": "15.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1451548800000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1438364295694,
  "created_at": 1437583424030,
  "locations": [
    {
      "_id": "55afc840f02cbea1b800a606",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55afc840f02cbea1b800a608",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "55afc840f02cbea1b800a605",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55afc840f02cbe3ac1000064",
    "attached_width": 295,
    "attached_height": 276,
    "attached_size": 102329,
    "attached_name": "lions logo.jpg",
    "main": false,
    "updated_at": 1437583424030,
    "created_at": 1437583424030
  },
  "activated_at": 1437583642396,
  "referral_code": ""
}
deals << {
  "_id": "55afc9a0f02cbeed2f00a61b",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "Donate $25.00 to the Rathdrum Lions Club",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "We All Love To Help Others.\r\nThe Rathdrum Lions Club of Idaho is one of those charities who is always trying to help the whole rather than the individual. \r\n100% of money raised with their charity programs goes to aiding local people. \r\nA few examples are: Eyeglasses and Hearing Aids, Nutritional Backpack Program, Scholarships, Free Firewood, Leader Dogs for the Blind, plus so much more.",
  "terms": "We are a 501(c3 non-profit organization.",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1451548800000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1438364308665,
  "created_at": 1437583776245,
  "locations": [
    {
      "_id": "55afc9a0f02cbeed2f00a61a",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55afc9a0f02cbeed2f00a61c",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "55afc9a0f02cbeed2f00a619",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55afc9a0f02cbe3ac1000068",
    "attached_width": 295,
    "attached_height": 276,
    "attached_size": 102329,
    "attached_name": "lions logo.jpg",
    "main": false,
    "updated_at": 1437583776245,
    "created_at": 1437583776245
  },
  "activated_at": 1437583778741,
  "referral_code": ""
}
deals << {
  "_id": "55afca04f02cbecbdf00aed2",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5592f256f02cbec07b002f8f",
  "title": "Donate $35.00 to the Rathdrum Lions Club",
  "category_id": "54e4cff5f02cbe195c003fb4",
  "offer": "We All Love To Help Others.\r\nThe Rathdrum Lions Club of Idaho is one of those charities who is always trying to help the whole rather than the individual. \r\n100% of money raised with their charity programs goes to aiding local people. \r\nA few examples are: Eyeglasses and Hearing Aids, Nutritional Backpack Program, Scholarships, Free Firewood, Leader Dogs for the Blind, plus so much more.",
  "terms": "We are a 501(c3 non-profit organization.",
  "value": "35.0",
  "price": "35.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5592ffaff02cbef7d3002fb1"
  ],
  "expires_at": 1451548800000,
  "user_id": "5282ba8df02cbed3de004efc",
  "updated_at": 1438364318571,
  "created_at": 1437583876242,
  "locations": [
    {
      "_id": "55afca04f02cbecbdf00aed1",
      "point": [
        -116.8715025423279,
        47.81820383833111
      ],
      "address": {
        "_id": "55afca04f02cbecbdf00aed3",
        "country": "US",
        "street": "16114 N. Meyer Road,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "55afca04f02cbecbdf00aed0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "55afca04f02cbe3ab5000059",
    "attached_width": 295,
    "attached_height": 276,
    "attached_size": 102329,
    "attached_name": "lions logo.jpg",
    "main": false,
    "updated_at": 1437583876241,
    "created_at": 1437583876241
  },
  "activated_at": 1437583891338,
  "referral_code": ""
}
deals << {
  "_id": "56315099f02cbe1262000e5e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "562ea24bf02cbef43e00020c",
  "title": "E-Juice 30ml Bottle",
  "category_id": "4f7d215774dbb02de300030c",
  "offer": "cheapejuiceclub.com",
  "terms": "online store: \r\ncheapejuiceclub.com",
  "value": "30.0",
  "price": "20.0",
  "limit": 5,
  "limit_per_customer": 5,
  "listing_ids": [
    "5631459ff02cbe6033000bdf"
  ],
  "expires_at": 1446274800000,
  "user_id": "560dd5c1f02cbefc5300bec9",
  "updated_at": 1462998423640,
  "created_at": 1446072473984,
  "locations": [
    {
      "_id": "573394f8e9330c860e000b48",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "573394f8e9330c860e000b49",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1446072489452,
  "image": {
    "_id": "56315548f02cbe5139000c89",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56315548f02cbe29bb00000b",
    "attached_width": 400,
    "attached_height": 400,
    "attached_size": 55775,
    "attached_name": "electronic cigarette e-liquid 30ml edited.jpg",
    "main": false,
    "updated_at": 1446073672351,
    "created_at": 1446073672351
  },
  "referral_code": "currentattractions",
  "deleted_at": 1485232961954
}
deals << {
  "_id": "5631582cf02cbeb7e4000ca8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "562eb44ff02cbe9cc2000288",
  "title": "AHA CPR Certification",
  "category_id": "4f7d215874dbb02de3000379",
  "offer": "CPR Class, 1/2 Price\r\n\r\nCDA CPR located in Coeur d'Alene ID\r\n(208 755-9540\r\nmatt@cdacpr.com",
  "terms": "By appointment. Must contact via email to schedule.",
  "value": "50.0",
  "price": "25.0",
  "limit": 5,
  "limit_per_customer": 5,
  "listing_ids": [
    "563146bff02cbe7fc3000e2f",
    "56314284f02cbe5482000e14",
    "562eb580f02cbe1d770001b9"
  ],
  "expires_at": 1485936000000,
  "user_id": "560dd5c1f02cbefc5300bec9",
  "updated_at": 1485232981995,
  "created_at": 1446074412868,
  "locations": [
    {
      "_id": "5640eb02f02cbee5ae0040e6",
      "point": [
        -116.7811472,
        47.6734739
      ],
      "address": {
        "_id": "5640eb02f02cbee5ae0040e7",
        "country": "US",
        "street": "401 Sherman Ave.",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83815"
      }
    },
    {
      "_id": "5640eb02f02cbee5ae0040e8",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5640eb02f02cbee5ae0040e9",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83815"
      }
    },
    {
      "_id": "5640eb02f02cbee5ae0040ea",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5640eb02f02cbee5ae0040eb",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5631582cf02cbeb7e4000ca6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5631582cf02cbe29bb00000f",
    "attached_width": 400,
    "attached_height": 400,
    "attached_size": 30075,
    "attached_name": "CPR-Certified-Hard-Hat-Label-HH-0064.gif",
    "main": false,
    "updated_at": 1446074412868,
    "created_at": 1446074412868
  },
  "activated_at": 1446074602019,
  "referral_code": ""
}
deals << {
  "_id": "5631889cf02cbeef34000d59",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "562ea24bf02cbef43e00020c",
  "title": "Kangertech SuBox Mini Starter Kit ",
  "category_id": "4f7d215774dbb02de300030c",
  "offer": "Kangertech SuBox Mini Starter Kit ",
  "terms": "local pick up available",
  "value": "59.99",
  "price": "49.99",
  "limit": 2,
  "limit_per_customer": 2,
  "listing_ids": [
    "5631459ff02cbe6033000bdf"
  ],
  "expires_at": 1446274800000,
  "user_id": "560dd5c1f02cbefc5300bec9",
  "updated_at": 1462998448827,
  "created_at": 1446086812682,
  "locations": [
    {
      "_id": "5631889cf02cbeef34000d58",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5631889cf02cbeef34000d5a",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5631889cf02cbeef34000d57",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5631889cf02cbe29bb000013",
    "attached_width": 300,
    "attached_height": 300,
    "attached_size": 76813,
    "attached_name": "Kanger_300x300.jpg",
    "main": false,
    "updated_at": 1446086812682,
    "created_at": 1446086812682
  },
  "activated_at": 1446086824518,
  "deleted_at": 1485232965492
}
deals << {
  "_id": "5642a2adf02cbea41c000135",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "Show off your Seahawks pride with 2 crochet hats! ",
  "category_id": "4f7d216974dbb02de3000ae7",
  "offer": "We are offering this 2 product deal for one low price! \r\n\r\nThis is a steal! \r\n\r\nAny Seahawks fan would love these handmade gifts as a way to show off that fabulous 12th man pride! \r\n\r\nPlease message me at the time of purchase what sizes you would like your hats to be, I can do anything from newborn to adult! \r\n\r\nCobb's Creations\r\nkenzieway18@gmail.com",
  "terms": "Payment: Orders will started as soon as the invoice is paid. Please pay all invoices within 24 hours. All order cancellations must be made within 48 hours.\r\n \r\nShipping: Please allow 1-2 weeks for order processing, due to the one of a kind handmade nature of each item. \r\nPlease make sure your shipping address is current! \r\n\r\nRefunds and Exchanges: \r\nEach item is custom made per each request. Unfortunately, Cobb's Creations will not accept returns. If an item is damaged during shipping, I will happily replace the item for you! Please return broken or damaged item within 4 weeks of purchase. \r\n\r\nI strive to make every customer 100% satisfied with their order! \r\nIf there is a mistake with the order, please contact me within 48 hours of receiving your item and I do anything I can to make the order right for you. Cobb's Creations does not refund any orders, only accept exchanges. If you would like to exchange an item, your original purchase must be returned in the condition it was received within 14 days. Cobb's Creations will not cover these shipping costs. \r\n\r\nCobb's Creations is not responsible for packages that are lost, stolen, not delivered or damaged. If your order is lost and not delivered, please contact me so that we can work something out. I will provide each order with a tracking number.\r\n",
  "value": "36.0",
  "price": "25.0",
  "limit": 50,
  "limit_per_customer": 5,
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "expires_at": "",
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1477339665034,
  "created_at": 1447207597833,
  "locations": [
    {
      "_id": "5642a2adf02cbea41c000133",
      "point": [
        -122.5153199,
        47.17839739999999
      ],
      "address": {
        "_id": "5642a2adf02cbea41c000136",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Lakewood",
        "region": "WA",
        "postal_code": "98499"
      }
    },
    {
      "_id": "5642a2adf02cbea41c000134",
      "point": [
        -116.7909562084097,
        47.69932388913045
      ],
      "address": {
        "_id": "5642a2adf02cbea41c000137",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5642a2adf02cbea41c000132",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5642a2adf02cbe7bd9000008",
    "attached_width": 1024,
    "attached_height": 1366,
    "attached_size": 317278,
    "attached_name": "image1 (1.JPG",
    "main": false,
    "updated_at": 1447207597833,
    "created_at": 1447207597833
  },
  "activated_at": 1447207616890,
  "referral_code": "",
  "deleted_at": 1485232521242
}
deals << {
  "_id": "5642a5c6f02cbef2d1000165",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": " Seahawks Scarf, Headband and bootcuff set! ",
  "category_id": "4f7d216974dbb02de3000ae7",
  "offer": "The perfect gift for any Seahawks girl! \r\nWhat better way to show off your team spirit then with these three items! \r\nAn oversized inifity scarf, floral headband and tricolored bootcuffs! \r\n\r\nPlease message me to let me know what sizes you would like your items to be made in, I can do anything from newborn to adult sizes! \r\n\r\nCobb's Creations\r\nkenzieway18@gmail.com",
  "terms": "Payment: Orders will started as soon as the invoice is paid. Please pay all invoices within 24 hours. All order cancellations must be made within 48 hours. \r\n\r\nShipping: Please allow 1-2 weeks for order processing, due to the one of a kind handmade nature of each item. \r\nPlease make sure your shipping address is current!\r\n\r\nRefunds and Exchanges: \r\nEach item is custom made per each request. Unfortunately, Cobb's Creations will not accept returns. If an item is damaged during shipping, I will happily replace the item for you! Please return broken or damaged item within 4 weeks of purchase.\r\n\r\nI strive to make every customer 100% satisfied with their order! \r\nIf there is a mistake with the order, please contact me within 48 hours of receiving your item and I do anything I can to make the order right for you. Cobb's Creations does not refund any orders, only accept exchanges. If you would like to exchange an item, your original purchase must be returned in the condition it was received within 14 days. Cobb's Creations will not cover these shipping costs.\r\n\r\nCobb's Creations is not responsible for packages that are lost, stolen, not delivered or damaged. If your order is lost and not delivered, please contact me so that we can work something out. I will provide each order with a tracking number.",
  "value": "45.0",
  "price": "35.0",
  "limit": 10,
  "limit_per_customer": 5,
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "expires_at": "",
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1477339615377,
  "created_at": 1447208390708,
  "locations": [
    {
      "_id": "5642a5c6f02cbef2d1000163",
      "point": [
        -122.5153199,
        47.17839739999999
      ],
      "address": {
        "_id": "5642a5c6f02cbef2d1000166",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Lakewood",
        "region": "WA",
        "postal_code": "98499"
      }
    },
    {
      "_id": "5642a5c6f02cbef2d1000164",
      "point": [
        -116.7909562084097,
        47.69932388913045
      ],
      "address": {
        "_id": "5642a5c6f02cbef2d1000167",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5642a5c6f02cbef2d1000162",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5642a5c6f02cbe7bd900000e",
    "attached_width": 1280,
    "attached_height": 1280,
    "attached_size": 303929,
    "attached_name": "seahwkscarfbootheadband.JPG",
    "main": false,
    "updated_at": 1447208390708,
    "created_at": 1447208390708
  },
  "activated_at": 1447208402094,
  "referral_code": "",
  "deleted_at": 1485232517375
}
deals << {
  "_id": "56688b1af02cbeac13000d35",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "3 Private Golf Lessons",
  "category_id": "57437c99e9330c36f3000a7e",
  "offer": "3 golf lessons for only $99.99 | normally $135\r\n\r\nSandbaggerz Golf located in Coeur d'Alene ID \r\n(208 665-7250 \r\nsandbaggerzgolf.hb@gmail.com",
  "terms": "Please bring in a hard copy of this deal.",
  "value": "135.0",
  "price": "99.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1485936000000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1485229894101,
  "created_at": 1449691930683,
  "locations": [
    {
      "_id": "56688b1af02cbeac13000d34",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56688b1af02cbeac13000d36",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1449691935250,
  "image": {
    "_id": "56688b8df02cbe526c000c93",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56688b8df02cbe1c2900001c",
    "attached_width": 2048,
    "attached_height": 1529,
    "attached_size": 561783,
    "attached_name": "sandbag 2.jpg",
    "main": false,
    "updated_at": 1449692045519,
    "created_at": 1449692045519
  },
  "referral_code": ""
}
deals << {
  "_id": "56688c93f02cbe39c2000d4b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Golf Apparel",
  "category_id": "4f7d216474dbb02de30008b8",
  "offer": "Men's Golf shirt",
  "terms": "Please bring in a hard copy of this Deal to store",
  "value": "75.00",
  "price": "60.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1459321200000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1449692307770,
  "created_at": 1449692307770,
  "locations": [
    {
      "_id": "56688c93f02cbe39c2000d4a",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56688c93f02cbe39c2000d4c",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "deleted_at": 1462997482070
}
deals << {
  "_id": "56688df8f02cbe9665000d5e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Men's & Women's Golf Shirts - Discounted",
  "category_id": "57437fcc1aa7209386000b03",
  "offer": "Men's & Women's Golf Shirts - Discounted\r\n\r\nSandbaggerz Golf located in Coeur d'Alene ID \r\n(208 665-7250 \r\nsandbaggerzgolf.hb@gmail.com",
  "terms": "Please bring in a hard copy of this Deal",
  "value": "34.95",
  "price": "27.96",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1485936000000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1485229902030,
  "created_at": 1449692664120,
  "locations": [
    {
      "_id": "56688df8f02cbe9665000d5d",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56688df8f02cbe9665000d5f",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1449692669021,
  "image": {
    "_id": "5668a674f02cbeb95e000db9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5668a674f02cbe1c29000020",
    "attached_width": 972,
    "attached_height": 726,
    "attached_size": 201443,
    "attached_name": "sandbag.jpg",
    "main": false,
    "updated_at": 1449698932754,
    "created_at": 1449698932754
  },
  "referral_code": ""
}
deals << {
  "_id": "56688e92f02cbe8ca7000d70",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Discounted Men's Golf Shirts",
  "category_id": "4f7d216474dbb02de30008b8",
  "offer": "Men's Golf Shirt",
  "terms": "Please bring in a hard copy of this Deal",
  "value": "34.95",
  "price": "27.96",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1459321200000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1449795535250,
  "created_at": 1449692818381,
  "locations": [
    {
      "_id": "56688e92f02cbe8ca7000d6f",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56688e92f02cbe8ca7000d71",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56688e92f02cbe8ca7000d6e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56688e92f02cbe1c1d00000d",
    "attached_width": 972,
    "attached_height": 726,
    "attached_size": 201443,
    "attached_name": "sandbag.jpg",
    "main": false,
    "updated_at": 1449692818381,
    "created_at": 1449692818381
  },
  "activated_at": 1449692826475,
  "referral_code": "",
  "deleted_at": 1485229855902
}
deals << {
  "_id": "5668a648f02cbebab0000e86",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Custom Fit Clubs",
  "category_id": "4f7d216474dbb02de30008b8",
  "offer": "Discounted Custom Golf Clubs",
  "terms": "",
  "value": "249.95",
  "price": "224.95",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1459321200000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1449796250675,
  "created_at": 1449698888856,
  "locations": [
    {
      "_id": "5668a648f02cbebab0000e85",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "5668a648f02cbebab0000e87",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1449698892177,
  "referral_code": "",
  "image": {
    "_id": "566a1444f02cbe4b460017e3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566a1444f02cbe1c2900002e",
    "attached_width": 498,
    "attached_height": 378,
    "attached_size": 100651,
    "attached_name": "golfclubs.jpg",
    "main": false,
    "updated_at": 1449792580203,
    "created_at": 1449792580203
  },
  "deleted_at": 1485229859637
}
deals << {
  "_id": "566a1bbff02cbe134b0017aa",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "3 Lessons = $99.99 (Normally $135.00",
  "category_id": "4f7d216474dbb02de30008ba",
  "offer": "Sandbaggerz is offering 3 golf lessons for $99.99, which is normally $135.00",
  "terms": "Please bring in a hard copy of this deal",
  "value": "135.0",
  "price": "99.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1459321200000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1449794560773,
  "created_at": 1449794495273,
  "locations": [
    {
      "_id": "566a1bbff02cbe134b0017a9",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "566a1bbff02cbe134b0017ab",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "deleted_at": 1462997437069
}
deals << {
  "_id": "566a21ecf02cbef17b0017e1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": " Custom Fit Golf Clubs",
  "category_id": "57437fcc1aa7209386000b03",
  "offer": "ONLY $224 for a set of Custom Fit Golf Clubs | Normally $249\r\n\r\nSandbaggerz Golf located in Coeur d'Alene ID\r\n(208 665-7250\r\nsandbaggerzgolf.hb@gmail.com",
  "terms": "Please bring in a hard copy of this deal",
  "value": "249.0",
  "price": "224.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1485936000000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1485229885961,
  "created_at": 1449796076637,
  "locations": [
    {
      "_id": "566a21ecf02cbef17b0017e0",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "566a21ecf02cbef17b0017e2",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "566a21fef02cbead300017e3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566a21fef02cbe1c21000029",
    "attached_width": 498,
    "attached_height": 378,
    "attached_size": 100651,
    "attached_name": "golfclubs.jpg",
    "main": false,
    "updated_at": 1449796094774,
    "created_at": 1449796094774
  },
  "activated_at": 1462997497321,
  "referral_code": ""
}
deals << {
  "_id": "566b1487f02cbe5039001d84",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "5 Norvell Spray Tans | ONLY $90 (normally $125 ",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": "Purchase 5 Norvell Spray Tans For $90.00 (Normally a $125.00 Value.  Best Winter Spray Tan in Town.",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "125.0",
  "price": "90.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": 1493535600000,
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1465337558645,
  "created_at": 1449858183359,
  "locations": [
    {
      "_id": "566b1487f02cbe5039001d83",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566b1487f02cbe5039001d85",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1449858229085,
  "referral_code": "currentattractions",
  "image": {
    "_id": "56a018e547ffb00b81008794",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a018e547ffb000070001c3",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 65167,
    "attached_name": "spray-tanning-mask-600x600.jpg",
    "main": false,
    "updated_at": 1453332709389,
    "created_at": 1453332709389
  }
}
deals << {
  "_id": "566b170df02cbe9269001b31",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Spray Tanning | 5 weeks unlimited ",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": "Best Winter Spray Tan in Town. ",
  "terms": "FREE $20.00 Gift Card When You Spend $50.00 or More ",
  "value": "70.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": 1490857200000,
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1465337565005,
  "created_at": 1449858829965,
  "locations": [
    {
      "_id": "566b170df02cbe9269001b30",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566b170df02cbe9269001b32",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1449858922834,
  "referral_code": "currentattractions",
  "image": {
    "_id": "56a0180f47ffb0d37d00876c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a0180f47ffb000070001bd",
    "attached_width": 305,
    "attached_height": 480,
    "attached_size": 16865,
    "attached_name": "spraytan2.jpg",
    "main": false,
    "updated_at": 1453332495870,
    "created_at": 1453332495870
  }
}
deals << {
  "_id": "566b188ff02cbe7c9b001b7e",
  "active": false,
  "vouchers_count": 25,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Flair Sun Salon ",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": "15 min bed | 5 weeks unlimited tanning, normally $20.95 NOW $15.95",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "20.95",
  "price": "15.95",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1465337545214,
  "created_at": 1449859215737,
  "locations": [
    {
      "_id": "566b188ff02cbe7c9b001b7d",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566b188ff02cbe7c9b001b7f",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1449859222837,
  "referral_code": "currentattractions",
  "image": {
    "_id": "56a017f447ffb024bf00873e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a017f447ffb000070001bb",
    "attached_width": 640,
    "attached_height": 426,
    "attached_size": 57090,
    "attached_name": "solarflare4.jpg",
    "main": false,
    "updated_at": 1453332468871,
    "created_at": 1453332468871
  }
}
deals << {
  "_id": "566b195cf02cbe0ebb001b47",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "12 Minute Bed | 5 weeks unlimited tanning!",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": "12 min. bed | 5 weeks unlimited, normally $47.95 NOW $39.95",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "47.95",
  "price": "39.95",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1465337533233,
  "created_at": 1449859420167,
  "locations": [
    {
      "_id": "566b195cf02cbe0ebb001b46",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566b195cf02cbe0ebb001b48",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1449859427398,
  "referral_code": "currentattractions",
  "image": {
    "_id": "56a0183547ffb0bcac00877a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a0183547ffb000070001bf",
    "attached_width": 750,
    "attached_height": 364,
    "attached_size": 36308,
    "attached_name": "solar flare2.jpg",
    "main": false,
    "updated_at": 1453332533374,
    "created_at": 1453332533374
  }
}
deals << {
  "_id": "566b19d4f02cbefaf4001b51",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "10 Minute Bed | 5 weeks unlimited tanning!",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": "10 min. bed | 5 weeks unlimited, normally $57.95 NOW $49.95",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "57.95",
  "price": "49.95",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": 1493535600000,
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1465337552503,
  "created_at": 1449859540402,
  "locations": [
    {
      "_id": "566b19d4f02cbefaf4001b50",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566b19d4f02cbefaf4001b52",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1449859544333,
  "referral_code": "currentattractions",
  "image": {
    "_id": "56a0184b47ffb0a0c2008780",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a0184b47ffb000070001c1",
    "attached_width": 2048,
    "attached_height": 1536,
    "attached_size": 255135,
    "attached_name": "820715_603052383045471_608139666_o.jpg",
    "main": false,
    "updated_at": 1453332555609,
    "created_at": 1453332555609
  }
}
deals << {
  "_id": "566f9ba5f02cbe91f10037eb",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Studded Boxter Tote Bag - Retail $42.95 NOW 27.95",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Studded Boxter Tote Bag - Retail $42.95 NOW 27.95",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "42.95",
  "price": "27.95",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550015685,
  "created_at": 1450154917958,
  "locations": [
    {
      "_id": "566f9ba5f02cbe91f10037ea",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566f9ba5f02cbe91f10037ec",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fa168f02cbe030e0038bf",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fa168f02cbe1c21000062",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 48896,
    "attached_name": "studdedboxter1.jpg",
    "main": false,
    "updated_at": 1450156392433,
    "created_at": 1450156392433
  },
  "activated_at": 1450199953405
}
deals << {
  "_id": "566fa561f02cbe0e820037fe",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Rockstud Padlock Tote - Retail $42.95 NOW 27.95",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Rockstud Padlock Tote - Retail $42.95 NOW 27.95",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "42.95",
  "price": "27.95",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550040643,
  "created_at": 1450157409688,
  "locations": [
    {
      "_id": "566fa561f02cbe0e820037fd",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566fa561f02cbe0e820037ff",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fa561f02cbe0e820037fc",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fa561f02cbe1c1d000051",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 37338,
    "attached_name": "Rockstudpadlocktote.jpg",
    "main": false,
    "updated_at": 1450157409688,
    "created_at": 1450157409688
  },
  "activated_at": 1450200002780
}
deals << {
  "_id": "566fa6f6f02cbe3d74003807",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Trendy Tote Bag - Retail $42.95 NOW 27.95",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Trendy Tote Bag - Retail $42.95 NOW 27.95",
  "terms": "> Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n\r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!\r\n",
  "value": "42.95",
  "price": "27.95",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550042692,
  "created_at": 1450157814767,
  "locations": [
    {
      "_id": "566fa6f6f02cbe3d74003806",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566fa6f6f02cbe3d74003808",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fa6f6f02cbe3d74003805",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fa6f6f02cbe1c1d000053",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 42508,
    "attached_name": "TrendyTotewithscarf.jpg",
    "main": false,
    "updated_at": 1450157814767,
    "created_at": 1450157814767
  },
  "activated_at": 1450196874434
}
deals << {
  "_id": "566fa7bdf02cbe4c80003819",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Designer Handbag - Retail $42.95 NOW 27.95",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Designer Handbag - Retail $42.95 NOW 27.95",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n\r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "42.95",
  "price": "27.95",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550044508,
  "created_at": 1450158013547,
  "locations": [
    {
      "_id": "566fa7bdf02cbe4c80003818",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566fa7bdf02cbe4c8000381a",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fa7bdf02cbe4c80003817",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fa7bdf02cbe1c1d000057",
    "attached_width": 325,
    "attached_height": 243,
    "attached_size": 39315,
    "attached_name": "Designerhandbag.jpg",
    "main": false,
    "updated_at": 1450158013547,
    "created_at": 1450158013547
  },
  "activated_at": 1450200022974
}
deals << {
  "_id": "566fa922f02cbeba940038e5",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Designer Clutch - Retail $42.95 NOW 27.95",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Designer Clutch - Retail $42.95 NOW 27.95",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "22.95",
  "price": "15.00",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550046932,
  "created_at": 1450158370134,
  "locations": [
    {
      "_id": "566fa922f02cbeba940038e4",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566fa922f02cbeba940038e6",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fa922f02cbeba940038e3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fa922f02cbe1c21000068",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 31476,
    "attached_name": "Brownclutch.jpg",
    "main": false,
    "updated_at": 1450158370134,
    "created_at": 1450158370134
  },
  "activated_at": 1450200013566
}
deals << {
  "_id": "566faa46f02cbe862f003836",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Designer Tote Bag - Retail $42.95 NOW 27.95",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Designer Tote Bag - Retail $42.95 NOW 27.95",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "50.95",
  "price": "33.00",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550050722,
  "created_at": 1450158662725,
  "locations": [
    {
      "_id": "566faa46f02cbe862f003835",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566faa46f02cbe862f003837",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566faa46f02cbe862f003834",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566faa46f02cbe1c1d000059",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 40570,
    "attached_name": "DesignerToteBagblack.jpg",
    "main": false,
    "updated_at": 1450158662725,
    "created_at": 1450158662725
  },
  "activated_at": 1450200031691
}
deals << {
  "_id": "566facb8f02cbe22c5003849",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "BLACK NOIR - Retail $105.00 NOW $52.00",
  "category_id": "4f7d216674dbb02de30009bc",
  "offer": "Often imitated, never duplicated. This extravagant 22X bronzer, derived from our exclusive Black Label Private Reserve™, delivers dark color that is truly one of a kind.",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "105.00",
  "price": "52.00",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550054449,
  "created_at": 1450159288431,
  "locations": [
    {
      "_id": "566facb8f02cbe22c5003848",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566facb8f02cbe22c500384a",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566facb8f02cbe22c5003847",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566facb8f02cbe1c1d00005d",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 59025,
    "attached_name": "BlackNoir.jpg",
    "main": false,
    "updated_at": 1450159288431,
    "created_at": 1450159288431
  },
  "activated_at": 1450200046095
}
deals << {
  "_id": "566fada7f02cbe1af7003913",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Designer Skin Adore - Retail $105.00 NOW 52.00",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "The exclusive Skin Activated Moisture™ Technology conditions deep, breaking barriers for the most irresistibly smooth skin. Free your inhibitions and let your ture bronzing potential shine through.",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "105.00",
  "price": "52.00",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550058774,
  "created_at": 1450159527676,
  "locations": [
    {
      "_id": "566fada7f02cbe1af7003912",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566fada7f02cbe1af7003914",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fada7f02cbe1af7003911",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fada7f02cbe1c2100006c",
    "attached_width": 243,
    "attached_height": 325,
    "attached_size": 45014,
    "attached_name": "Adore.jpg",
    "main": false,
    "updated_at": 1450159527675,
    "created_at": 1450159527675
  },
  "activated_at": 1450200047262
}
deals << {
  "_id": "566fb2d6f02cbe13d5003a11",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Tangerine Splash Tanning Lotion | Retail $42.95 NOW $20.00",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Bronze Your Bod! This melanin stimulating complex will ignite the most alluring tan. Get more out of every tanning bed session with Tangerine Splash.",
  "terms": "Print off voucher and show at store to redeem or bring in smartphone to show proof of purchase. \r\n*Item will NOT be shipped.  The item is in-store pick-up ONLY!",
  "value": "42.95",
  "price": "20.00",
  "limit": 1,
  "limit_per_customer": 1,
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1452550072538,
  "created_at": 1450160854703,
  "locations": [
    {
      "_id": "566fb2d6f02cbe13d5003a10",
      "point": [
        -116.807453287533,
        47.69514379189747
      ],
      "address": {
        "_id": "566fb2d6f02cbe13d5003a12",
        "country": "US",
        "street": "2073 Main Street",
        "suite": "Riverstone",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "566fb2d6f02cbe13d5003a0f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "566fb2d6f02cbe1c25000065",
    "attached_width": 385,
    "attached_height": 568,
    "attached_size": 43732,
    "attached_name": "TangerineSplash.JPG",
    "main": false,
    "updated_at": 1450160854702,
    "created_at": 1450160854702
  },
  "activated_at": 1450200051198
}
deals << {
  "_id": "56706041f02cbe4542003bdf",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ee2196f02cbe319d004823",
  "title": "Clear Headlight Gel (Shipping Included",
  "category_id": "4f7d216774dbb02de3000a53",
  "offer": "ONLY 16.98 for Clear Headlight Gel | Normally $23.98 - SHIPPING INCLUDED\r\n> It’s a special formula designed to clean, shine and eliminate oxidation; restoring your headlights to new again.\r\n",
  "terms": "> Allow 7 Days for shipping. \r\n> Add $3.99 for shipping and handling. \r\n> Total amount will be 16.98. \r\n> Please make sure your correct address is listed.",
  "value": "23.98",
  "price": "16.98",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54ee28f6f02cbe85ea003ed5"
  ],
  "expires_at": 1472536800000,
  "user_id": "54ef6a31f02cbebcbd004c65",
  "updated_at": 1460574001753,
  "created_at": 1450205249338,
  "locations": [
    {
      "_id": "56706041f02cbe4542003bde",
      "point": [
        -111.610635,
        40.165955
      ],
      "address": {
        "_id": "56706041f02cbe4542003be0",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Springville",
        "region": "UT",
        "postal_code": "84663"
      }
    }
  ],
  "image": {
    "_id": "56706041f02cbe4542003bdd",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56706041f02cbe1c2900007b",
    "attached_width": 300,
    "attached_height": 300,
    "attached_size": 41676,
    "attached_name": "gell.jpg",
    "main": false,
    "updated_at": 1450205249338,
    "created_at": 1450205249338
  },
  "activated_at": 1450205285794
}
deals << {
  "_id": "5670c1ecf02cbe5d6d004ac3",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "555e1c59f02cbe17ed002574",
  "title": "$50 1st Music Video Production",
  "category_id": "57437de5e9330c54aa000ad1",
  "offer": "Film your bands first Pulse Production music video for only $50 a $200 value (SAVE $150.00",
  "terms": "Includes:\r\n> 2-3 minute HD music video\r\n> Assistance uploading to website/social media\r\n> Creative consultations with Pulse Productions to design a video that compliments your music and will get your name out there.\r\n> Ask about becoming a Pulse Productions Featured Band\r\n\r\nPrint and show voucher or show on smartphone to redeem.",
  "value": "200.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": 1,
  "listing_ids": [
    "555e232bf02cbe935b00288f"
  ],
  "expires_at": "",
  "user_id": "555fa5fbf02cbe21730033dc",
  "updated_at": 1466021521633,
  "created_at": 1450230252608,
  "locations": [
    {
      "_id": "5670c1ecf02cbe5d6d004ac2",
      "point": [
        -116.78498,
        47.69284
      ],
      "address": {
        "_id": "5670c1ecf02cbe5d6d004ac4",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5670c1ecf02cbe5d6d004ac1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5670c1ecf02cbe1c250000ac",
    "attached_width": 807,
    "attached_height": 483,
    "attached_size": 89381,
    "attached_name": "Pulsepromusicvideo.JPG",
    "main": false,
    "updated_at": 1450230252608,
    "created_at": 1450230252608
  },
  "activated_at": 1462997270570,
  "referral_code": "",
  "deleted_at": 1485229950595
}
deals << {
  "_id": "5682daecf02cbe79bb003846",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56442c6df02cbebdb30009e9",
  "title": "Cosmetologist ",
  "category_id": "4f7d215374dbb02de3000164",
  "offer": "Full head highlight or lowlights ",
  "terms": "Only valid for services With Tari. Must make an appointment and mention this deal. ",
  "value": "95",
  "price": "65",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56442ff4f02cbe44b6000a75"
  ],
  "expires_at": 1462086000000,
  "user_id": "56442c6df02cbebdb30009e6",
  "updated_at": 1451416300740,
  "created_at": 1451416300740,
  "locations": [
    {
      "_id": "5682daecf02cbe79bb003845",
      "point": [
        -116.9145487,
        47.7110821
      ],
      "address": {
        "_id": "5682daecf02cbe79bb003847",
        "country": "US",
        "street": "2525 E Seltice Way",
        "suite": "Suite A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "deleted_at": 1462992529319
}
deals << {
  "_id": "5682dba4f02cbea3c0003758",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56442c6df02cbebdb30009e9",
  "title": "Full Head Highlight or Lowlights",
  "category_id": "4f7d215374dbb02de3000164",
  "offer": "ONLY $65 for Full head highlight or lowlights | Normally $95\r\n\r\nHair by Tari located in Post Falls ID\r\n(208 704-3054\r\ntari.moore.tm@gmail.com",
  "terms": "Only valid for services with Tari. Must make an appointment and mention this deal. ",
  "value": "95.0",
  "price": "65.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56442ff4f02cbe44b6000a75"
  ],
  "expires_at": 1546243200000,
  "user_id": "56442c6df02cbebdb30009e6",
  "updated_at": 1486070319966,
  "created_at": 1451416484427,
  "locations": [
    {
      "_id": "5682dba4f02cbea3c0003757",
      "point": [
        -116.9145487,
        47.7110821
      ],
      "address": {
        "_id": "5682dba4f02cbea3c0003759",
        "country": "US",
        "street": "2525 E Seltice Way",
        "suite": "Suite A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1451416512916,
  "referral_code": "currentattractions",
  "image": {
    "_id": "56957b56845f09a1ec009acb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56957b56845f0900060000d3",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 73779,
    "attached_name": "hairbytariimage4.jpg",
    "main": false,
    "updated_at": 1452637014987,
    "created_at": 1452637014987
  }
}
deals << {
  "_id": "56846ef694cacf0f6c0011e4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "555e1c59f02cbe17ed002574",
  "title": "Your First Music Video",
  "category_id": "57437de5e9330c54aa000ad1",
  "offer": "> 2-3 Minute HD music video\r\n> Assistance uploading to website/social media\r\n> Creative consultations to design a video that compliments your music and get your name out there\r\n> Ask about becoming a Pulse Productions Featured Band\r\n\r\nPulse Productions located in Coeur d'Alene ID\r\n(208 771-4118\r\nTravis@pulsecda.com",
  "terms": "> Film your bands FIRST Pulse Productions music video for only $50 -- a $200 value. \r\n> Each video after the FIRST will be $200.  ",
  "value": "200.0",
  "price": "50.0",
  "limit": 100,
  "limit_per_customer": 1,
  "listing_ids": [
    "555e232bf02cbe935b00288f"
  ],
  "expires_at": 1485936000000,
  "user_id": "555fa5fbf02cbe21730033dc",
  "updated_at": 1485229989958,
  "created_at": 1451519734462,
  "locations": [
    {
      "_id": "56846ef694cacf0f6c0011e3",
      "point": [
        -116.78498,
        47.69284
      ],
      "address": {
        "_id": "56846ef694cacf0f6c0011e5",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56846ef694cacf0f6c0011e2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56846ef694cacf000100009e",
    "attached_width": 925,
    "attached_height": 558,
    "attached_size": 108012,
    "attached_name": "150offmusicvideo.JPG",
    "main": false,
    "updated_at": 1451519734461,
    "created_at": 1451519734461
  },
  "activated_at": 1451519757637,
  "referral_code": "reimche"
}
deals << {
  "_id": "56846ffd94cacf139700120c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "555e1c59f02cbe17ed002574",
  "title": "Short Online Commercial Production",
  "category_id": "57437de5e9330c54aa000ad1",
  "offer": "> 1-2 minute HD video\r\n> Assistance uploading to website/social media \r\n> Optional packages with multiple videos of varying lengths\r\n> Creative consultations to create effective averstisement\r\n\r\nPulse Productions located in Coeur d'Alene ID\r\n(208 771-4118\r\nTravis@pulsecda.com",
  "terms": "$50 off short online commercial production totaling $450.00 -- Normally $500.00",
  "value": "500.0",
  "price": "450.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "555e232bf02cbe935b00288f"
  ],
  "expires_at": 1485936000000,
  "user_id": "555fa5fbf02cbe21730033dc",
  "updated_at": 1485230000136,
  "created_at": 1451519997238,
  "locations": [
    {
      "_id": "56846ffd94cacf139700120b",
      "point": [
        -116.78498,
        47.69284
      ],
      "address": {
        "_id": "56846ffd94cacf139700120d",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56846ffd94cacf139700120a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56846ffd94cacf00010000a2",
    "attached_width": 886,
    "attached_height": 599,
    "attached_size": 96211,
    "attached_name": "50offcommercial.JPG",
    "main": false,
    "updated_at": 1451519997238,
    "created_at": 1451519997238
  },
  "activated_at": 1451520017413,
  "referral_code": "reimche"
}
deals << {
  "_id": "568475f394cacf55aa0012ae",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "555e1c59f02cbe17ed002574",
  "title": "EXCLUSIVE Wedding Package",
  "category_id": "57437de5e9330c54aa000ad1",
  "offer": "> Full length wedding video consisting of the prep, ceremony and reception of your wedding (timeframe varies\r\n> Custom DVD provided to bride and groom\r\n> 3-4 minute highlight preview video",
  "terms": "> Exclusive deal offered only through MarketPad.com\r\n> Save $100 on $1,200 package for a total of ONLY $1,100 via showing receipt",
  "value": "1200.0",
  "price": "1100.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "555e232bf02cbe935b00288f"
  ],
  "expires_at": 1472713200000,
  "user_id": "555fa5fbf02cbe21730033dc",
  "updated_at": 1464040981451,
  "created_at": 1451521523546,
  "locations": [
    {
      "_id": "568475f394cacf55aa0012ad",
      "point": [
        -116.78498,
        47.69284
      ],
      "address": {
        "_id": "568475f394cacf55aa0012af",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "568475f394cacf55aa0012ac",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568475f394cacf00010000ae",
    "attached_width": 318,
    "attached_height": 500,
    "attached_size": 83221,
    "attached_name": "weddingdiscount2.jpg",
    "main": false,
    "updated_at": 1451521523546,
    "created_at": 1451521523546
  },
  "activated_at": 1451521531140,
  "referral_code": "reimche",
  "deleted_at": 1485229954893
}
deals << {
  "_id": "568ab38e94cacfe9dd008f98",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568582d294cacf15990023e4",
  "title": "Idaho Didactic Concealed Weapons Permit Course",
  "category_id": "57437acfe9330c97f7000a0e",
  "offer": "Purchase your Idaho Didactic Handgun Course for the deeply discounted rate of $79.00 normally $125.00\r\n\r\nTrigger Control Training Group located in Coeur d'Alene ID\r\n(208 758-0543\r\n\r\nTrigger Control Training Group is exited to invite you to our 1 day Didactic Concealed Weapons course. The course is designed by world class instructors from the civilian and law enforcement community, you will learn firearms laws, your rights to self defense, how to effectively handle the criminal and civil ramification associated with owning and potently using a firearm, and the moral and civil ramifications associated with the use of firearms for self-defense. ",
  "terms": "Only one discounted program may be used for each student, you may purchase as a gift for another person. \r\nNot valid with any other coupons or offerings. ",
  "value": "125.0",
  "price": "79.0",
  "limit": 50,
  "limit_per_customer": 1,
  "listing_ids": [
    "568ab15a94cacf95f2008f52"
  ],
  "expires_at": 1546243200000,
  "user_id": "5689750a94cacfe7540079c3",
  "updated_at": 1486071288991,
  "created_at": 1451930510779,
  "locations": [
    {
      "_id": "568ab38e94cacfe9dd008f97",
      "point": [
        -116.7975748,
        47.7020359
      ],
      "address": {
        "_id": "568ab38e94cacfe9dd008f99",
        "country": "US",
        "street": "2775 N Howard St ",
        "suite": "#14",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "568ab38e94cacfe9dd008f96",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568ab38e94cacf000100023a",
    "attached_width": 435,
    "attached_height": 321,
    "attached_size": 51276,
    "attached_name": "Private class pic (2.jpg",
    "main": false,
    "updated_at": 1451930510779,
    "created_at": 1451930510779
  },
  "activated_at": 1451930529002,
  "referral_code": "reimche"
}
deals << {
  "_id": "568b071a94cacfe50700a163",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "568af05c94cacff84e009aa7",
  "title": "$25 Deal for $20",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": " $25 Deal for $20 at GW Hunters Post Falls  ",
  "terms": "> Cannot be combined with any other deals or coupons\r\n> Entire amount must be used in 1 visit\r\n> 1 voucher per table\r\n> $20 amount never expires\r\n> Must print voucher and present to server",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568b034a94cacfc64c00a094"
  ],
  "expires_at": 1490943600000,
  "user_id": "568af05c94cacff84e009aa4",
  "updated_at": 1486057978831,
  "created_at": 1451951898160,
  "locations": [
    {
      "_id": "568b071a94cacfe50700a162",
      "point": [
        -116.9484784,
        47.7136606
      ],
      "address": {
        "_id": "568b071a94cacfe50700a164",
        "country": "US",
        "street": "615 N. Spokane Street",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "568b071a94cacfe50700a161",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568b071a94cacf000100031f",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 86342,
    "attached_name": "gwhuntersfood.jpg",
    "main": false,
    "updated_at": 1451951898160,
    "created_at": 1451951898160
  },
  "activated_at": 1451952009255,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "568c2c8c0ce944792b001a22",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568acee794cacfde9d009407",
  "title": "3 Month Membership | NO CONTRACT",
  "category_id": "4f7d215f74dbb02de3000691",
  "offer": "ONLY $99 for 3 Month NO CONTRACT Membership\r\n\r\nFrontline Fitness located in Post Falls ID\r\n(208 777-7999\r\nchanda@frontline.fitness",
  "terms": "Must Purchase online and bring Printed Voucher or Show Voucher on Smart Phone.  Amount paid never expires. Limit 1 per person, may buy 3 additional as gifts. All goods or services must be used by the same person. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "value": "150.0",
  "price": "99.0",
  "limit": 100,
  "limit_per_customer": 1,
  "listing_ids": [
    "568c292b0ce944b0af0019ac",
    "568c27de0ce94487f40019a1"
  ],
  "expires_at": 1546243200000,
  "user_id": "568c22e00ce944ca2b0018d6",
  "updated_at": 1486071423402,
  "created_at": 1452027020317,
  "locations": [
    {
      "_id": "568c2c8c0ce944792b001a20",
      "point": [
        -116.9533466,
        47.7145496
      ],
      "address": {
        "_id": "568c2c8c0ce944792b001a23",
        "country": "US",
        "street": "414 W Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    },
    {
      "_id": "568c2c8c0ce944792b001a21",
      "point": [
        -116.9533466,
        47.7145496
      ],
      "address": {
        "_id": "568c2c8c0ce944792b001a24",
        "country": "US",
        "street": "414 W Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "568c2c8c0ce944792b001a1f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568c2c8c0ce944000700005d",
    "attached_width": 721,
    "attached_height": 305,
    "attached_size": 225288,
    "attached_name": "frontline5.jpg",
    "main": false,
    "updated_at": 1452027020316,
    "created_at": 1452027020316
  },
  "activated_at": 1452027224693,
  "referral_code": "reimche"
}
deals << {
  "_id": "568c2df40ce944fa88001a5b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568acee794cacfde9d009407",
  "title": "1 Year Gym Membership",
  "category_id": "4f7d215f74dbb02de3000691",
  "offer": "ONLY $380.00 - SAVE $100 on 1 year Membership\r\n\r\nFrontline Fitness located in Post Falls ID\r\n(208 777-7999\r\nchanda@frontline.fitness",
  "terms": "Requires 12 Month Agreement & Paid In Full Upfront.\r\nMust Purchase online and bring Printed Voucher or Show Voucher on Smart Phone. Amount paid never expires. All goods or services must be used by the same person. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.",
  "value": "480.0",
  "price": "380.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568c27de0ce94487f40019a1"
  ],
  "expires_at": 1546243200000,
  "user_id": "568c22e00ce944ca2b0018d6",
  "updated_at": 1486071453540,
  "created_at": 1452027380243,
  "locations": [
    {
      "_id": "568c2df40ce944fa88001a5a",
      "point": [
        -116.9533466,
        47.7145496
      ],
      "address": {
        "_id": "568c2df40ce944fa88001a5c",
        "country": "US",
        "street": "414 W Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1452027384077,
  "image": {
    "_id": "568d409a0ce944ba9300320d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568d409a0ce94400070000db",
    "attached_width": 960,
    "attached_height": 448,
    "attached_size": 82236,
    "attached_name": "frontline4.jpg",
    "main": false,
    "updated_at": 1452097690351,
    "created_at": 1452097690351
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "568d56f60ce944fcda003478",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "5685696694cacf1cf900226f",
  "title": "ONLY $40 for 60 Minute Massage - SAVE $10!",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": "ONLY $40 for 60 Minute Massage - SAVE $10!",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem.\r\n> A-Game Massage is located inside The Gym LLC, though efforts are made to insulate sound, complete silence is not guaranteed. ",
  "value": "50.0",
  "price": "40.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568c03050ce944361900140b"
  ],
  "expires_at": 1462086000000,
  "user_id": "568b45570ce94475dd000582",
  "updated_at": 1464044011047,
  "created_at": 1452103414134,
  "locations": [
    {
      "_id": "568d56f60ce944fcda003477",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "568d56f60ce944fcda003479",
        "country": "US",
        "street": "411 W Haycraft Avenue",
        "suite": "C-1 (Inside The Gym LLC",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "568d56f60ce944fcda003476",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568d56f60ce94400070000df",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 39542,
    "attached_name": "A-Game Massage Studio6.jpg",
    "main": false,
    "updated_at": 1452103414134,
    "created_at": 1452103414134
  },
  "activated_at": 1452103433808,
  "referral_code": "reimche"
}
deals << {
  "_id": "568d57650ce944fc0b0034a4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5685696694cacf1cf900226f",
  "title": "90 Minute Massage",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": "ONLY $65 for 90 Minute Massage | normally $75\r\n\r\nA-Game Massage Studio located in Coeur d'Alene ID\r\n(208 818-0642",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem.\r\n> A-Game Massage is located inside The Gym LLC, though efforts are made to insulate sound, complete silence is not guaranteed. ",
  "value": "75.0",
  "price": "65.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568c03050ce944361900140b"
  ],
  "expires_at": 1546243200000,
  "user_id": "568b45570ce94475dd000582",
  "updated_at": 1486064789595,
  "created_at": 1452103525904,
  "locations": [
    {
      "_id": "568d57650ce944fc0b0034a3",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "568d57650ce944fc0b0034a5",
        "country": "US",
        "street": "411 W Haycraft Avenue",
        "suite": "C-1 (Inside The Gym LLC",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "568d57650ce944fc0b0034a2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568d57650ce94400070000e3",
    "attached_width": 640,
    "attached_height": 640,
    "attached_size": 44705,
    "attached_name": "A-Game Massage Studio4.jpg",
    "main": false,
    "updated_at": 1452103525903,
    "created_at": 1452103525903
  },
  "activated_at": 1452103530438,
  "referral_code": "reimche"
}
deals << {
  "_id": "568d8c890ce9449a00003b47",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568b020494cacffa2a00a05d",
  "title": "60 Minute Photo Shoot",
  "category_id": "57437d00e9330cdd5b000a96",
  "offer": "SAVE $25!  ONLY $75 - 60 Minute Photo shoot\r\n\r\nSkyline Productions Photography located in Coeur d'Alene ID\r\n(208 277-8584\r\n\r\n> Outdoor / Random location (depending on distance to location additional charges for gas may apply\r\n> Digital Images (via Google Drive or Drop Box, NO CD\r\n> Up to 5 people ",
  "terms": "> Must purchase through website and bring printed voucher or show on smartphone to redeem deal.\r\n> Depending on distance to location additional charges for gas may apply\r\n> Additional people over 5 will incur additional charges",
  "value": "100.0",
  "price": "75.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568d8aec0ce944ec19003aea"
  ],
  "expires_at": 1546243200000,
  "user_id": "568d81c40ce944d7320039f6",
  "updated_at": 1486070884421,
  "created_at": 1452117129887,
  "locations": [
    {
      "_id": "568d8c890ce9449a00003b46",
      "point": [
        -116.791701,
        47.676388
      ],
      "address": {
        "_id": "568d8c890ce9449a00003b48",
        "country": "US",
        "street": "401 Park Dr",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "568d8c890ce9449a00003b45",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568d8c890ce944000700011e",
    "attached_width": 2048,
    "attached_height": 757,
    "attached_size": 200475,
    "attached_name": "skyline3.jpg",
    "main": false,
    "updated_at": 1452117129887,
    "created_at": 1452117129887
  },
  "activated_at": 1452117142230,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "568eb3d80ce944df370056d4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568af16b94cacf6b8a009ad1",
  "title": "5 Month Membership | High School Special",
  "category_id": "57437cc51aa7209821000a47",
  "offer": "> SAVE $100 on 5 Month Membership Class. \r\n> Normally $250 NOW $150 for current high school students.\r\n> Must be Paid In Full\r\n\r\nLake City Highland Dance located in Coeur d'Alene ID\r\n(208 771-0828\r\nlchighlanddance@gmail.com",
  "terms": "> Must be a current high school student to redeem this voucher (Do not have to be student to purchase\r\n> First time students only.\r\n> Must purchase through website and print off voucher to show at location or show via smartphone to redeem",
  "value": "250.0",
  "price": "150.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568eaf0c0ce9443dd2005548"
  ],
  "expires_at": 1546243200000,
  "user_id": "568ea9f70ce9441805005448",
  "updated_at": 1486070732734,
  "created_at": 1452192728132,
  "locations": [
    {
      "_id": "568eb3d80ce944df370056d3",
      "point": [
        -116.7975748,
        47.7020359
      ],
      "address": {
        "_id": "568eb3d80ce944df370056d5",
        "country": "US",
        "street": "2775 N Howard Street",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "568eb3d80ce944df370056d2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568eb3d80ce944000700017a",
    "attached_width": 395,
    "attached_height": 259,
    "attached_size": 31851,
    "attached_name": "dance image 3.jpg",
    "main": false,
    "updated_at": 1452192728132,
    "created_at": 1452192728132
  },
  "activated_at": 1452192732450,
  "referral_code": "reimche"
}
deals << {
  "_id": "568feb8a0ce944e0d900721f",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "568acc9294cacf81e7009396",
  "title": "SAVE $3 - $8.00 for $5.00!",
  "category_id": "4f7d215e74dbb02de3000639",
  "offer": "$8.00 worth of anything for $5.00. \r\nOur cheesesteaks are on Amoroso rolls.  We have classic steak and provolone plus twists like the Chili Philly, Garlic Philly, and Buffalo Philly!",
  "terms": "> Must redeem full amount of deal at visit.\r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Limit 1 per table. ",
  "value": "8.0",
  "price": "5.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568d6beb0ce9442879003729"
  ],
  "expires_at": 1501484400000,
  "user_id": "568d64ea0ce9441590003609",
  "updated_at": 1486063284467,
  "created_at": 1452272522958,
  "locations": [
    {
      "_id": "568feb8a0ce944e0d900721e",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "568feb8a0ce944e0d9007220",
        "country": "US",
        "street": "200 W Hanley Ave",
        "suite": "Inside Silver Lake Mall at Food Court",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "568feb8a0ce944e0d900721d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568feb8a0ce9440007000217",
    "attached_width": 680,
    "attached_height": 455,
    "attached_size": 45577,
    "attached_name": "original steak philly.jpg",
    "main": false,
    "updated_at": 1452272522958,
    "created_at": 1452272522958
  },
  "activated_at": 1452272547188,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "568fec610ce944840800724c",
  "active": true,
  "vouchers_count": 5,
  "profile_id": "568acc9294cacf81e7009396",
  "title": "SAVE $6 - $16 for $10",
  "category_id": "4f7d215e74dbb02de3000639",
  "offer": "$16.00 worth of anything for $10.00. \r\nOur cheesesteaks are on Amoroso rolls.  We have classic steak and provolone plus twists like the Chili Philly, Garlic Philly, and Buffalo Philly!",
  "terms": "> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Limit 1 per table.",
  "value": "16.0",
  "price": "10.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568d6beb0ce9442879003729"
  ],
  "expires_at": "",
  "user_id": "568d64ea0ce9441590003609",
  "updated_at": 1486063308816,
  "created_at": 1452272737485,
  "locations": [
    {
      "_id": "568fec610ce944840800724b",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "568fec610ce944840800724d",
        "country": "US",
        "street": "200 W Hanley Ave",
        "suite": "Inside Silver Lake Mall at Food Court",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452272739341,
  "image": {
    "_id": "568feca80ce9440829007253",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "568feca80ce944000700021d",
    "attached_width": 643,
    "attached_height": 356,
    "attached_size": 58961,
    "attached_name": "buffalo chicken1.JPG",
    "main": false,
    "updated_at": 1452272808120,
    "created_at": 1452272808120
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56902e4b0ce944cc4b007c6d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Small Sports/Characters Bandanas for pets | Buy 3 get 1 free",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "Purchase 3 Small Sports Or Character Bandanas for pets, Get 1 Free\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Buy 3 get 1 free.  Contact seller via email or phone to specify Sports team or Character you want for each bandana after purchasing voucher. Sports and Characters are special orders.",
  "value": "40.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429272039,
  "created_at": 1452289611653,
  "locations": [
    {
      "_id": "56902e4b0ce944cc4b007c6c",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "56902e4b0ce944cc4b007c6e",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56902e4b0ce944cc4b007c6b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56902e4b0ce94400070002a2",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 290092,
    "attached_name": "bandanababesportspromo.jpg",
    "main": false,
    "updated_at": 1452289611653,
    "created_at": 1452289611653
  },
  "activated_at": 1452289637751,
  "referral_code": "currentattractions",
  "deleted_at": 1485232130644
}
deals << {
  "_id": "5690313a0ce944032d007e35",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Extra Small Sports/Characters Bandanas for pets, Buy 3 get 1 Free",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "Buy 3 Extra Small Bandanas for pets, Get 1 Free.  Contact seller via email or phone to specify Sports Team or Character.\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Buy 3 get 1 free.  Contact seller via email or phone to specify Sports team or Character you want for each bandana after purchasing voucher. Sports and Characters are special orders.",
  "value": "26.0",
  "price": "19.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483084800000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429142084,
  "created_at": 1452290362488,
  "locations": [
    {
      "_id": "5690313a0ce944032d007e34",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "5690313a0ce944032d007e36",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5690313a0ce944032d007e33",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5690313a0ce94400070002a7",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 82795,
    "attached_name": "bandanababesportspromo2.jpg",
    "main": false,
    "updated_at": 1452290362488,
    "created_at": 1452290362488
  },
  "activated_at": 1452290384446,
  "referral_code": "",
  "deleted_at": 1485232123651
}
deals << {
  "_id": "569033080ce944f69d007ecd",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Medicum Sports/Characters Bandanas for pets | Buy 3 get 1 Free",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "Buy 3 medium Sports/Character Bandanas for pets, get 1 free\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Contact seller after purchasing voucher via email or phone to specify Sports team or Character for each bandana.  Sports and Characters are special orders.",
  "value": "48.0",
  "price": "36.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429319399,
  "created_at": 1452290824643,
  "locations": [
    {
      "_id": "569033080ce944f69d007ecc",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "569033080ce944f69d007ece",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "569033080ce944f69d007ecb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "569033080ce94400070002ab",
    "attached_width": 1280,
    "attached_height": 460,
    "attached_size": 228820,
    "attached_name": "bandanababesportspromo3.jpg",
    "main": false,
    "updated_at": 1452290824643,
    "created_at": 1452290824643
  },
  "activated_at": 1452290855687,
  "referral_code": "currentattractions",
  "deleted_at": 1485232134478
}
deals << {
  "_id": "5690346b0ce94454b0007f70",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Large Sports/Character Bandanas for pets | Buy 3 Get 1 Free",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": " Buy 3 Large Sports/Character Bandanas for pets, get 1 Free\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Must contact seller after purchasing voucher via email or phone to specify Sports team or Character for each bandana.  Sports and Characters are special orders.",
  "value": "56.0",
  "price": "42.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1480119910875,
  "created_at": 1452291179459,
  "locations": [
    {
      "_id": "5690346b0ce94454b0007f6f",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "5690346b0ce94454b0007f71",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5690346b0ce94454b0007f6e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5690346b0ce94400070002af",
    "attached_width": 371,
    "attached_height": 453,
    "attached_size": 73064,
    "attached_name": "bandanababesportspromo3 (2.jpg",
    "main": false,
    "updated_at": 1452291179458,
    "created_at": 1452291179458
  },
  "activated_at": 1452291230182,
  "referral_code": "currentattractions",
  "deleted_at": 1485232137545
}
deals << {
  "_id": "5691528c0ce944b2a400a630",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Extra Large Sports/Characters Bandanas for pets | Buy get 1 Free",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "Buy 3 Extra Large Sports/Characters Bandanas for pets, get 1 Free\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Must contact seller after purchasing voucher via email or phone to specify Sports team or Character for each bandana.  Sports and Characters are special orders.",
  "value": "72.0",
  "price": "54.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429410674,
  "created_at": 1452364428185,
  "locations": [
    {
      "_id": "5691528c0ce944b2a400a62f",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "5691528c0ce944b2a400a631",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5691530b0ce94446c100a638",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5691530b0ce94400070002e7",
    "attached_width": 960,
    "attached_height": 640,
    "attached_size": 57253,
    "attached_name": "bandanababesportspromo4.jpg",
    "main": false,
    "updated_at": 1452364555467,
    "created_at": 1452364555467
  },
  "activated_at": 1452364581202,
  "referral_code": "",
  "deleted_at": 1485232141629
}
deals << {
  "_id": "5693d8d1845f09db620031ab",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "568adc1694cacfff800096a8",
  "title": "Initial Exam and 3 Adjustments",
  "category_id": "4f7d215f74dbb02de30006dd",
  "offer": "SAVE $110!! $50 initial exam and 3 adjustments (160$ value",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem. ",
  "value": "160.0",
  "price": "50.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568bfb880ce9446d16001293"
  ],
  "expires_at": 1459407600000,
  "user_id": "568ae7ba94cacf10970098eb",
  "updated_at": 1464042404601,
  "created_at": 1452529873056,
  "locations": [
    {
      "_id": "5693d8d1845f09db620031aa",
      "point": [
        -116.786655,
        47.690953
      ],
      "address": {
        "_id": "5693d8d1845f09db620031ac",
        "country": "US",
        "street": "1705 N Government Way",
        "suite": "",
        "city": "Couer d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5693d8d0845f09db620031a9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5693d8d1845f090006000035",
    "attached_width": 736,
    "attached_height": 651,
    "attached_size": 125988,
    "attached_name": "adjustments.jpg",
    "main": false,
    "updated_at": 1452529873056,
    "created_at": 1452529873056
  },
  "activated_at": 1452529877575,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56943529845f09ac12003c54",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53c32e56f02cbef5890021fc",
  "title": "60 Minute Massage (therapeutic, sports, relaxation",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": " ONLY $45 for 60 minute massage (therapeutic, sports, relaxation |  Normally $55\r\n\r\nTouch Therapeutic located in Hayden ID\r\n(509 216-3663\r\n",
  "terms": "> NEW CLIENTS ONLY. \r\n> Valid Mon-Fri by Appointment. \r\n> No Cash Value. \r\n> No Change Given. \r\n> Must Use FULL Voucher Value In One Visit. \r\n> Voucher Discount Expires 90 Days After Purchase. \r\n> Full Voucher Value Expires One Year After Purchase. \r\n> Can Not Be Combined With Other Offers. \r\n> Must Fill Out Client Intake Form",
  "value": "55.0",
  "price": "45.0",
  "limit": 35,
  "limit_per_customer": 2,
  "listing_ids": [
    "53c336e3f02cbe3efc001d48"
  ],
  "expires_at": 1546156800000,
  "user_id": "53c32cc5f02cbe2b1e002092",
  "updated_at": 1486070625184,
  "created_at": 1452553513706,
  "locations": [
    {
      "_id": "56943529845f09ac12003c53",
      "point": [
        -116.788692,
        47.759345
      ],
      "address": {
        "_id": "56943529845f09ac12003c55",
        "country": "US",
        "street": "157 W Hayden Ave",
        "suite": "205",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "56943529845f09ac12003c52",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56943529845f09000600003f",
    "attached_width": 600,
    "attached_height": 400,
    "attached_size": 114705,
    "attached_name": "Touch Therpeutic Massage60min deal.gif",
    "main": false,
    "updated_at": 1452553513706,
    "created_at": 1452553513706
  },
  "activated_at": 1452553516584,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56944474845f09d8d70040bf",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "53a471a1f02cbeda2a002b1c",
  "title": "Churrasco Dinner Experience",
  "category_id": "4f7d216574dbb02de300097e",
  "offer": "CHURRASCO DINNER MONDAY- THURSDAY\r\n",
  "terms": "OFFER VALID MONDAY THROUGH THURSDAY ONLY. Must print off voucher and redeem at Grille From Ipanema during regular business hours.    Deal expires December 31st 2017.  Cash value valid after December 31st 2017.  Cannot be combined with any other deals, coupons or offers.",
  "value": "24.99",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53a472a4f02cbee325002e16"
  ],
  "expires_at": 1514707200000,
  "user_id": "53a471a1f02cbeda2a002b1d",
  "updated_at": 1468374940499,
  "created_at": 1452557428355,
  "locations": [
    {
      "_id": "56944474845f09d8d70040be",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -116.777532,
        47.67288079999999
      ],
      "address": {
        "_id": "56944474845f09d8d70040c0",
        "country": "US",
        "street": "601 E Front Ave,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56944474845f09d8d70040bd",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56944474845f090006000043",
    "attached_width": 920,
    "attached_height": 385,
    "attached_size": 124711,
    "attached_name": "grillefromipanemadealphotodinner.jpg",
    "main": false,
    "updated_at": 1452557428354,
    "created_at": 1452557428354
  },
  "activated_at": 1452614434032,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "569448df845f092dcb0041ff",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "53a471a1f02cbeda2a002b1c",
  "title": "Churrasco Lunch Experience",
  "category_id": "4f7d216574dbb02de300097e",
  "offer": "SAVE $5. REGULAR $14.99 CHURRASCO LUNCH",
  "terms": "Offer valid during regular lunch business hours Monday through Thursday.  Cannot be combined with any other deals or offers.  Offer Expires March 31st 2016.  $9.99 cash value valid after March 31st 2016.",
  "value": "14.99",
  "price": "9.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53a472a4f02cbee325002e16"
  ],
  "expires_at": 1462086000000,
  "user_id": "53a471a1f02cbeda2a002b1d",
  "updated_at": 1456792664465,
  "created_at": 1452558559211,
  "locations": [
    {
      "_id": "569448df845f092dcb0041fe",
      "geocoded_city_level": false,
      "geocode_source": "",
      "point": [
        -116.777532,
        47.67288079999999
      ],
      "address": {
        "_id": "569448df845f092dcb004200",
        "country": "US",
        "street": "601 E Front Ave,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "569448df845f092dcb0041fd",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "569448df845f090006000047",
    "attached_width": 697,
    "attached_height": 310,
    "attached_size": 78505,
    "attached_name": "grillefromipanemadeallunchphoto.jpg",
    "main": false,
    "updated_at": 1452558559210,
    "created_at": 1452558559210
  },
  "activated_at": 1452614679957,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56944d2f845f09051c0042ee",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "568461c994cacf7bf8000fa9",
  "title": "Buffalo Blue Chicken Sandwich (Includes Choice of Soup",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "Buffalo Blue Chicken Sandwich for $6 normally $8.50\r\n> Grilled Chicken, Monterey Jack Cheese, Local Sesame Bun, Buffalo Blue Sauce, Lettuce, Red Onion Tomato\r\n> Includes Your Choice of Soup",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem. \r\n> Limit 1 per person (can buy 1 additional as a gift\r\n> Limit 1 per visit. 1\r\n> 1 voucher per visiting party. \r\n",
  "value": "8.5",
  "price": "6.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568ee28e0ce94459d3005f70"
  ],
  "expires_at": 1467356400000,
  "user_id": "568ed0de0ce9447d34005e00",
  "updated_at": 1458243429599,
  "created_at": 1452559663675,
  "locations": [
    {
      "_id": "56944d2f845f09051c0042ed",
      "point": [
        -116.7865655,
        47.7121517
      ],
      "address": {
        "_id": "56944d2f845f09051c0042ef",
        "country": "US",
        "street": "3670 N Government Way",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56944d2f845f09051c0042ec",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56944d2f845f090006000051",
    "attached_width": 567,
    "attached_height": 429,
    "attached_size": 231179,
    "attached_name": "1135033.jpg",
    "main": false,
    "updated_at": 1452559663675,
    "created_at": 1452559663675
  },
  "activated_at": 1452559666602,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56944dde845f091d06004354",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568461c994cacf7bf8000fa9",
  "title": "Crusin' Chicken Sandwich (Includes Choice of Soup",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "Crusin' Chicken Sandwich for $6 normally $8\r\n> Grilled Chicken, Monterey Jack Cheese, Local Sesame Bun, Buoy Ranch, Lettuce, Red Onion Tomato\r\n> Includes Your Choice of Soup",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem\r\n> Limit 1 per person (can buy 1 additional as a gift \r\n> Limit 1 per visit. 1 \r\n> 1 voucher per visiting party",
  "value": "8.0",
  "price": "6.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568ee28e0ce94459d3005f70"
  ],
  "expires_at": 1467356400000,
  "user_id": "568ed0de0ce9447d34005e00",
  "updated_at": 1458243432624,
  "created_at": 1452559838524,
  "locations": [
    {
      "_id": "56944dde845f091d06004353",
      "point": [
        -116.7865655,
        47.7121517
      ],
      "address": {
        "_id": "56944dde845f091d06004355",
        "country": "US",
        "street": "3670 N Government Way",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56944dde845f091d06004352",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56944dde845f090006000055",
    "attached_width": 640,
    "attached_height": 640,
    "attached_size": 54518,
    "attached_name": "buoycda8.jpg",
    "main": false,
    "updated_at": 1452559838524,
    "created_at": 1452559838524
  },
  "activated_at": 1452559841155,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56944f56845f09838100436c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568461c994cacf7bf8000fa9",
  "title": "Mom's Magic Meat(less Burg (Includes Choice of Soup",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "Mom's Magic Meat(less Burg for $6 normally $8\r\n> 100% From Scratch Veggie patty, Monterey Jack Cheese, Local Sesame Bun, Mayo, Lettuce, Red Onion Tomato\r\n> Includes Your Choice of Soup",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem. \r\n> Limit 1 per person (can buy 1 additional as a gift \r\n> Limit 1 per visit. 1 \r\n> 1 voucher per visiting party.",
  "value": "8.0",
  "price": "6.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568ee28e0ce94459d3005f70"
  ],
  "expires_at": 1467356400000,
  "user_id": "568ed0de0ce9447d34005e00",
  "updated_at": 1458243435479,
  "created_at": 1452560214123,
  "locations": [
    {
      "_id": "56944f56845f09838100436b",
      "point": [
        -116.7865655,
        47.7121517
      ],
      "address": {
        "_id": "56944f56845f09838100436d",
        "country": "US",
        "street": "3670 N Government Way",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56944f56845f09838100436a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56944f56845f090006000059",
    "attached_width": 960,
    "attached_height": 689,
    "attached_size": 73763,
    "attached_name": "veggie1.jpg",
    "main": false,
    "updated_at": 1452560214122,
    "created_at": 1452560214122
  },
  "activated_at": 1452560217227,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56951daf845f09336a0057ca",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "Save $4 L.C.H.S HAT & SCARF",
  "category_id": "4f7d216974dbb02de3000ae7",
  "offer": "Lake City Timberwolves Hat & Scarf $24 | Normally $28\r\n\r\nCobb's Creations \r\nkenzieway18@gmail.com",
  "terms": "This lovely set is the perfet way to show off your\r\n\r\nTimberwolf Pride!!!\r\n\r\nTeal and Navy\r\n\r\nyarn combinations make the perfect accent to with any school attire! \r\n\r\nThese products are made with two strands of worst weight yarn to keep you nice and toasty throughout these freezing winter months! \r\n\r\nUnisex Hat is teen/adult sized! \r\n Unisex Scarf is 50 inches long! \r\n\r\nItems can be made in child sizes also!\r\n\r\nA portion of all the proceeds for these Lake City sets goes directly to the Lake City DECA! \r\n\r\nSupport your local schools and your local businesses today! \r\n\r\n\r\n• Brand New \r\n• For Ladies \r\n",
  "value": "28.0",
  "price": "24.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54eb5345f02cbe538a002266"
  ],
  "expires_at": 1483084800000,
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1477428858353,
  "created_at": 1452613039398,
  "locations": [
    {
      "_id": "56951daf845f09336a0057c8",
      "point": [
        -116.7909562084097,
        47.69932388913045
      ],
      "address": {
        "_id": "56951daf845f09336a0057cb",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56951daf845f09336a0057c7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56951daf845f090006000087",
    "attached_width": 851,
    "attached_height": 851,
    "attached_size": 93599,
    "attached_name": "cobbscreationstimberwolvehat.jpg",
    "main": false,
    "updated_at": 1452613039398,
    "created_at": 1452613039398
  },
  "activated_at": 1452613236503,
  "referral_code": "currentattractions",
  "deleted_at": 1485232508457
}
deals << {
  "_id": "56952043845f099cee005a1b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "Save $2.50  3 MUG COZYS FOR $12",
  "category_id": "4f7d216974dbb02de3000ae7",
  "offer": "3 MUG COZYS FOR $12 REGULAR $14.50....INCLUDES SHIPPING\r\n\r\nCobb's Creations\r\nkenzieway18@gmail.com",
  "terms": "Mug Cozis!  3 for $12     These make great gifts for any tea or coffee drinker!  MUST CONTACT SELLER VIA EMAIL TO SELECT MATERIAL",
  "value": "14.5",
  "price": "12.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54eb5345f02cbe538a002266"
  ],
  "expires_at": 1483171200000,
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1477428804142,
  "created_at": 1452613699856,
  "locations": [
    {
      "_id": "56952043845f099cee005a1a",
      "point": [
        -116.7909562084097,
        47.69932388913045
      ],
      "address": {
        "_id": "56952043845f099cee005a1c",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452613730683,
  "image": {
    "_id": "56952115845f099b05005ae6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56952115845f09000600008b",
    "attached_width": 960,
    "attached_height": 734,
    "attached_size": 155776,
    "attached_name": "cobbscreations12 (2.jpg",
    "main": false,
    "updated_at": 1452613909130,
    "created_at": 1452613909130
  },
  "referral_code": "currentattractions",
  "deleted_at": 1485232499342
}
deals << {
  "_id": "569530a1845f095fb400602f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55b68778f02cbe6fbf001d13",
  "title": "3 Hours of House Cleaning",
  "category_id": "4f7d215974dbb02de30003de",
  "offer": "New clients get 3 hours of housecleaning for only $59.00!\r\n\r\nMichele's House Cleaning located in Rathdrum ID\r\n(208 651-7436\r\nmichele.driggs@yahoo.com",
  "terms": "",
  "value": "75.0",
  "price": "59.0",
  "limit": 30,
  "limit_per_customer": 1,
  "listing_ids": [
    "55f1ddbef02cbef926000614"
  ],
  "expires_at": 1546243200000,
  "user_id": "50f49248cfc07cad1b000030",
  "updated_at": 1486070447694,
  "created_at": 1452617889664,
  "locations": [
    {
      "_id": "569530a1845f095fb400602e",
      "point": [
        -116.8965857,
        47.8124031
      ],
      "address": {
        "_id": "569530a1845f095fb4006030",
        "country": "US",
        "street": "P.O. Box 15,",
        "suite": "",
        "city": "Rathdrum",
        "region": "ID",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "569530a1845f095fb400602d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "569530a1845f0900060000a1",
    "attached_width": 277,
    "attached_height": 182,
    "attached_size": 15261,
    "attached_name": "housecleaning.jpg",
    "main": false,
    "updated_at": 1452617889664,
    "created_at": 1452617889664
  },
  "activated_at": 1452617917106,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56969577845f09539500bb99",
  "active": false,
  "vouchers_count": 6,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "SAVE $5 Schmidty's Deal $25 for $19.99",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "$25 for $19.99 Schmidty's Deal",
  "terms": "Offer expires June 1st 2016.  Must be used during regular business hours.  Must be used in 1 visit.  $19.99 Cash value valid after June 1st 2016",
  "value": "25.0",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1464764400000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1464911951081,
  "created_at": 1452709239790,
  "locations": [
    {
      "_id": "56969577845f09539500bb98",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "56969577845f09539500bb9a",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56969577845f09539500bb97",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56969577845f09000600010b",
    "attached_width": 718,
    "attached_height": 718,
    "attached_size": 70903,
    "attached_name": "schmidtyspromo2016.jpg",
    "main": false,
    "updated_at": 1452709239790,
    "created_at": 1452709239790
  },
  "activated_at": 1452709326939,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56969703845f0955ab00bbfb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "SAVE $3  Schmidty's Deal $15.00 for only $11.99",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "$15 worth of food for $11.99\r\n\r\nSchmidty's Burgers located in Coeur d'Alene ID \r\n(208 292-4545",
  "terms": "Must be used during regular business hours. Must be used in 1 visit. $11.99 Cash value ",
  "value": "15.0",
  "price": "11.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1485936000000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1485227608611,
  "created_at": 1452709635459,
  "locations": [
    {
      "_id": "56969703845f0955ab00bbfa",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "56969703845f0955ab00bbfc",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56969703845f0955ab00bbf9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56969703845f09000600010f",
    "attached_width": 960,
    "attached_height": 717,
    "attached_size": 81256,
    "attached_name": "schmidtys5.jpg",
    "main": false,
    "updated_at": 1452709635459,
    "created_at": 1452709635459
  },
  "activated_at": 1452709684597,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5696ca2f845f092ebc00c513",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": " HIPPO CAR WASH DEAL $50 FOR ONLY $40",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": "HIPPO CAR WASH $50 FOR $40 DEAL",
  "terms": "LIMITED TIME OFFER. MUST BE USED DURING REGULAR BUSINESS HOURS.  CANNOT BE COMBINED WITH ANY OTHER DEALS OR OFFERS.  ONLY 15 AVAILABLE",
  "value": "50.0",
  "price": "40.0",
  "limit": 15,
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": "",
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1479922072712,
  "created_at": 1452722735795,
  "locations": [
    {
      "_id": "5696ca2f845f092ebc00c512",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5696ca2f845f092ebc00c514",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452723062828,
  "referral_code": "currentattractions",
  "image": {
    "_id": "5697fae3845f095bc800e812",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5697fae3845f090006000169",
    "attached_width": 413,
    "attached_height": 413,
    "attached_size": 19848,
    "attached_name": "hippocarwashlogo4.jpg",
    "main": false,
    "updated_at": 1452800739362,
    "created_at": 1452800739362
  }
}
deals << {
  "_id": "5696cc6f845f09eccf00c565",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": " HIPPO  GOLD WASH FOR $13",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": "$15 GOLD WASH FOR ONLY $13  ",
  "terms": "BLUE WASH + LAVA SHINE, EXTREME SHINE, RAIN X, TIRE SHINE. MUST BE USED DURING REGULAR BUSINESS HOURS.  CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS.  DEAL EXPIRES DECEMBER 31ST 2016.  $13 CASH VALUE VALID AFTER DECEMBER 31ST 2016.  MUST REDEEM IN 1 VISIT",
  "value": "15.0",
  "price": "13.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1483171200000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1486057218903,
  "created_at": 1452723311531,
  "locations": [
    {
      "_id": "5696cc6f845f09eccf00c564",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5696cc6f845f09eccf00c566",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452723375507,
  "referral_code": "currentattractions",
  "image": {
    "_id": "5697fa04845f09a24200e7e6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5697fa04845f090006000167",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 72806,
    "attached_name": "hippocarwashdeal.jpg",
    "main": false,
    "updated_at": 1452800516170,
    "created_at": 1452800516170
  }
}
deals << {
  "_id": "5696e653845f09261000cb13",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "HIPPO $12 BLUE WASH FOR $11",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": "$12 BLUE WASH FOR $11 FROM HIPPO CAR WASH",
  "terms": "WHITE WASH + TRIPLE COAT WAX.  MUST BE USED DURING REGULAR BUSINESS HOURS. CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS. DEAL EXPIRES DECEMBER 31ST 2016. $11 CASH VALUE VALID AFTER DECEMBER 31ST 2016. MUST REDEEM IN 1 VISIT",
  "value": "12.0",
  "price": "11.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1483171200000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1457972355512,
  "created_at": 1452729939649,
  "locations": [
    {
      "_id": "5696e653845f09261000cb12",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5696e653845f09261000cb14",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452730100260,
  "image": {
    "_id": "5697fcb7845f09ce3600e840",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5697fcb7845f09000600016b",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 66836,
    "attached_name": "hippowhitewashdeal.jpg",
    "main": false,
    "updated_at": 1452801207975,
    "created_at": 1452801207975
  },
  "deleted_at": 1485229155064
}
deals << {
  "_id": "5696e81b845f092b5000cb65",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "HIPPO $10 WHITE WASH FOR $9",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": " $10 WHITE WASH FOR $9",
  "terms": "RED WASH + WHEEL APPEAL, SEAL COAT PROTECTANT, UNDER BODY FLUSH. MUST BE USED DURING REGULAR BUSINESS HOURS. CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS. DEAL EXPIRES DECEMBER 31ST 2016. $9 CASH VALUE VALID AFTER DECEMBER 31ST 2016. MUST REDEEM IN 1 VISIT",
  "value": "10.0",
  "price": "9.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1483171200000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1452801490920,
  "created_at": 1452730395625,
  "locations": [
    {
      "_id": "5696e81b845f092b5000cb64",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5696e81b845f092b5000cb66",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452730403309,
  "image": {
    "_id": "5697fd91845f092d2e00e856",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5697fd91845f09000600016d",
    "attached_width": 223,
    "attached_height": 178,
    "attached_size": 35544,
    "attached_name": "Hippocarwashlogo3.png",
    "main": false,
    "updated_at": 1452801425843,
    "created_at": 1452801425843
  }
}
deals << {
  "_id": "5696e94c845f0990de00cbd2",
  "active": true,
  "vouchers_count": 4,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "SAVE $1 HIPPO CAR WASH $8 RED WASH FOR $7",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": "$8 RED WASH FOR $7",
  "terms": "MUST BE USED DURING REGULAR BUSINESS HOURS. CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS. DEAL EXPIRES DECEMBER 31ST 2016. $7 CASH VALUE VALID AFTER DECEMBER 31ST 2016. MUST REDEEM IN 1 VISIT",
  "value": "8",
  "price": "7",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1483171200000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1452730749036,
  "created_at": 1452730700925,
  "locations": [
    {
      "_id": "5696e94c845f0990de00cbd1",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5696e94c845f0990de00cbd3",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1452730749027,
  "image": {
    "_id": "5697fe1a845f09d35200e871",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5697fe1a845f09000600016f",
    "attached_width": 397,
    "attached_height": 397,
    "attached_size": 23050,
    "attached_name": "hippocarwashlogo2.jpg",
    "main": false,
    "updated_at": 1452801562423,
    "created_at": 1452801562423
  }
}
deals << {
  "_id": "5698589b845f0950dc00f68f",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Eyelash or Eyebrow  Extensions (Includes 1 Fill",
  "category_id": "4f7d216474dbb02de300091f",
  "offer": "Normally $125.00 - Eyelash or Eyebrow  Extensions (Includes 1 Fill ",
  "terms": "> 1 per person, but can buy 1 additional as a gift. \r\n> 1 per visit. Valid only for option purchased. Appointment required; subject to availability. 24hr cancellation notice required. \r\n> Have to use fill within 21 days of first visit. ",
  "value": "125.0",
  "price": "70.0",
  "limit": "",
  "limit_per_customer": 2,
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": "",
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1454696584108,
  "created_at": 1452824731511,
  "locations": [
    {
      "_id": "5698589b845f0950dc00f68e",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5698589b845f0950dc00f690",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1452824735622,
  "referral_code": "rachelr83",
  "image": {
    "_id": "569eb50a47ffb075460053d9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "569eb50a47ffb000070000a8",
    "attached_width": 301,
    "attached_height": 218,
    "attached_size": 18209,
    "attached_name": "eyelash.JPG",
    "main": false,
    "updated_at": 1453241610075,
    "created_at": 1453241610075
  }
}
deals << {
  "_id": "569969e31859982fbf0018d5",
  "active": true,
  "vouchers_count": 3,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": " ONLY $29.99 for $35 Worth of Food",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": " ONLY $29.99 for $35 Worth of Food",
  "terms": "> Limit 1 per visit. \r\n> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value valid after expiration date",
  "value": "35.0",
  "price": "29.99",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1483084800000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1463512985586,
  "created_at": 1452894691650,
  "locations": [
    {
      "_id": "569969e31859982fbf0018d4",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "569969e31859982fbf0018d6",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "569969e31859982fbf0018d3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "569969e31859980007000016",
    "attached_width": 660,
    "attached_height": 660,
    "attached_size": 79715,
    "attached_name": "thelocaldelipromo4.jpg",
    "main": false,
    "updated_at": 1452894691649,
    "created_at": 1452894691649
  },
  "activated_at": 1452894752963,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56a019d747ffb02add0087d8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5612e677f02cbed3e1002bcf",
  "title": "Buy three get one free guitar lessons",
  "category_id": "57437c27e9330c5bc1000a5e",
  "offer": "Buy three guitar lessons get one free. $75 value for only $56.25\r\n\r\nBranson Guitar Instruction located in Dalton Gardens ID \r\n(208 699-6161",
  "terms": "Cannot be used with any other deals or offers.  Must contact seller after purchase to schedule.  $56.25 Cash value valid after December 30th 2016",
  "value": "75.0",
  "price": "56.25",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "5612ef22f02cbed0e3002d6a"
  ],
  "expires_at": 1483171200000,
  "user_id": "5612e5c7f02cbe734d002bc4",
  "updated_at": 1477425817801,
  "created_at": 1453332951969,
  "locations": [
    {
      "_id": "56a019d747ffb02add0087d7",
      "point": [
        -116.78622,
        47.742123
      ],
      "address": {
        "_id": "56a019d747ffb02add0087d9",
        "country": "US",
        "street": "7736 N. Government Way,",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56a019d747ffb02add0087d6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a019d747ffb000070001d0",
    "attached_width": 1342,
    "attached_height": 1345,
    "attached_size": 498360,
    "attached_name": "Bransonguitarphoto.jpg",
    "main": false,
    "updated_at": 1453332951969,
    "created_at": 1453332951969
  },
  "activated_at": 1453332956301,
  "referral_code": "",
  "deleted_at": 1485232277049
}
deals << {
  "_id": "56a02b0d47ffb07f53008a21",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5612e677f02cbed3e1002bcf",
  "title": "Buy 3 months of guitar lessons, get 1 free",
  "category_id": "57437c27e9330c5bc1000a5e",
  "offer": "Buy 3 months of guitar lessons, get one month free. \r\n\r\nBranson Guitar Instruction located in Dalton Gardens ID\r\n(208 699-6161",
  "terms": "Cannot be used with any other deals or offers.  Contact seller after purchase to schedule.",
  "value": "300.0",
  "price": "225.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "5612ef22f02cbed0e3002d6a"
  ],
  "expires_at": 1546243200000,
  "user_id": "5612e5c7f02cbe734d002bc4",
  "updated_at": 1486070024791,
  "created_at": 1453337357114,
  "locations": [
    {
      "_id": "56a02b0d47ffb07f53008a20",
      "point": [
        -116.78622,
        47.742123
      ],
      "address": {
        "_id": "56a02b0d47ffb07f53008a22",
        "country": "US",
        "street": "7736 N. Government Way,",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56a02b0d47ffb07f53008a1f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a02b0d47ffb000070001d8",
    "attached_width": 1024,
    "attached_height": 817,
    "attached_size": 326343,
    "attached_name": "Bransonguitarphoto2.jpg",
    "main": false,
    "updated_at": 1453337357114,
    "created_at": 1453337357114
  },
  "activated_at": 1453337368462,
  "referral_code": ""
}
deals << {
  "_id": "56a02d5f47ffb00bc4008a94",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "$115 for $99.99 Deal",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": "$115 deal for $99.99   ",
  "terms": "> Limit 1 per visit. \r\n> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value valid after expiration date",
  "value": "115.0",
  "price": "99.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1483084800000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1461890799520,
  "created_at": 1453337951045,
  "locations": [
    {
      "_id": "56a02d5f47ffb00bc4008a93",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "56a02d5f47ffb00bc4008a95",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "56a02d5f47ffb00bc4008a92",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a02d5f47ffb000070001dd",
    "attached_width": 960,
    "attached_height": 503,
    "attached_size": 69333,
    "attached_name": "localdeliphoto.jpg",
    "main": false,
    "updated_at": 1453337951045,
    "created_at": 1453337951045
  },
  "activated_at": 1453337989002
}
deals << {
  "_id": "56a1230247ffb06a9900b332",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "title": "Spa Pedicure $35 | Normally $45",
  "category_id": "4f7d215a74dbb02de3000436",
  "offer": "ONLY $35.00 (Normally $45.00 for a Spa Pedicure\r\n\r\nStunning Creations Salon located in Post Falls ID\r\n(208 262-8019",
  "terms": "> Must setup an appointment\r\n> Cannot be combined with any other promotional offer\r\n> Limit 1 per person per visit\r\n",
  "value": "45.0",
  "price": "35.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "569ff05e47ffb01d870080af",
    "55c14fbcf02cbecd98001044"
  ],
  "expires_at": 1485936000000,
  "user_id": "55c14d2af02cbeecaf000f78",
  "updated_at": 1485231454836,
  "created_at": 1453400834334,
  "locations": [
    {
      "_id": "56a1230247ffb06a9900b330",
      "point": [
        -116.9145487,
        47.7110821
      ],
      "address": {
        "_id": "56a1230247ffb06a9900b333",
        "country": "US",
        "street": "2525 E Seltice Way,",
        "suite": "A,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    },
    {
      "_id": "56a1230247ffb06a9900b331",
      "point": [
        -116.9148121,
        47.7111491
      ],
      "address": {
        "_id": "56a1230247ffb06a9900b334",
        "country": "US",
        "street": "2525 e seltice way",
        "suite": "A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1453400845825,
  "image": {
    "_id": "56a134db47ffb0cfd600b88a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a134db47ffb00007000237",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 43509,
    "attached_name": "spa.jpg",
    "main": false,
    "updated_at": 1453405403276,
    "created_at": 1453405403276
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56a1236a47ffb03acc00b366",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "title": "Haircut and Style $25 | Normally $35",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "ONLY $25.00 (normally $35 for Haircut and Style\r\n\r\nStunning Creations Salon located in Post Falls ID\r\n(208 262-8019",
  "terms": "> Must setup an appointment\r\n> Cannot be combined with any other promotional offer\r\n> Limit 1 per person per visit",
  "value": "35.0",
  "price": "25.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "569ff05e47ffb01d870080af",
    "55c14fbcf02cbecd98001044"
  ],
  "expires_at": 1546243200000,
  "user_id": "55c14d2af02cbeecaf000f78",
  "updated_at": 1486069354351,
  "created_at": 1453400938593,
  "locations": [
    {
      "_id": "56a1236a47ffb03acc00b364",
      "point": [
        -116.9145487,
        47.7110821
      ],
      "address": {
        "_id": "56a1236a47ffb03acc00b367",
        "country": "US",
        "street": "2525 E Seltice Way,",
        "suite": "A,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    },
    {
      "_id": "56a1236a47ffb03acc00b365",
      "point": [
        -116.9148121,
        47.7111491
      ],
      "address": {
        "_id": "56a1236a47ffb03acc00b368",
        "country": "US",
        "street": "2525 e seltice way",
        "suite": "A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56a1236a47ffb03acc00b363",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a1236a47ffb0000700022d",
    "attached_width": 960,
    "attached_height": 960,
    "attached_size": 98066,
    "attached_name": "1654241_650255011698620_1918431839_n.jpg",
    "main": false,
    "updated_at": 1453400938593,
    "created_at": 1453400938593
  },
  "activated_at": 1453400942205,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56a13a4647ffb002e100b974",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "3 Tacos Tuesday (Lunch",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "3 Tacos for $7.50, normally $12.00.  \r\nChoose one of the following: Baja-Grilled Fish Tacos | Korean Steak Tacos | Shredded Chicken Tacos",
  "terms": "> Must be redeemed on a Tuesday\r\n> Must purchase online and show printed voucher to redeem \r\n> Limit 1 per person and 1 per visit\r\n> Not valid with any other special or promotional deal\r\n> Not valid for Happy Hour Specials",
  "value": "12.0",
  "price": "7.5",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687211628,
  "created_at": 1453406790048,
  "locations": [
    {
      "_id": "56a13a4647ffb002e100b973",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a13a4647ffb002e100b975",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a13a4547ffb002e100b972",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a13a4647ffb0000700023d",
    "attached_width": 896,
    "attached_height": 560,
    "attached_size": 96240,
    "attached_name": "rivelles0.JPG",
    "main": false,
    "updated_at": 1453406790048,
    "created_at": 1453406790048
  },
  "activated_at": 1453406804812,
  "referral_code": "rachelr83",
  "deleted_at": 1472061174223
}
deals << {
  "_id": "56a13a9b47ffb0436b00b982",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "3 Tacos Tuesday & A Brew (Dinner",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "3 Tacos for $7.50, normally $12.00 and beer of choice ($5.00 value.  Choice of either Baja-Grilled Fish Tacos,  Korean Steak Tacos or Shredded Chicken Tacos.",
  "terms": "> Must be redeemed on a Tuesday\r\n> Must purchase online and show printed voucher to redeem \r\n> Limit 1 per person and 1 per visit\r\n> Not valid with any other special or promotional deal\r\n> Not valid for Happy Hour Specials\r\n> Must be over 21",
  "value": "17.0",
  "price": "12.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687214163,
  "created_at": 1453406875066,
  "locations": [
    {
      "_id": "56a13a9b47ffb0436b00b981",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a13a9b47ffb0436b00b983",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a13a9b47ffb0436b00b980",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a13a9b47ffb00007000241",
    "attached_width": 480,
    "attached_height": 319,
    "attached_size": 28114,
    "attached_name": "rivelles.jpg",
    "main": false,
    "updated_at": 1453406875066,
    "created_at": 1453406875066
  },
  "activated_at": 1453406879946,
  "referral_code": "rachelr83",
  "deleted_at": 1472061178355
}
deals << {
  "_id": "56a1462047ffb0036300bb57",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "Burger & Brew Wednesday",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "ONLY $12.00 for the Lakeshore Burger ($10.00 Value and a brew of choice ($5.00 Value \r\n\r\n> Lakeshore Burger:  Ground beef mixed with herbs and spices. Topped with cheddar, lettuce, tomato, pickles and sweet & spicy glaze",
  "terms": "> Only valid if redeemed on a Wednesday (can be purchased any day\r\n> Must purchase online and show printed voucher to redeem \r\n> Limit 1 per person and 1 per visit \r\n> Not valid with any other special or promotional deal \r\n> Not valid for Happy Hour Specials",
  "value": "15.0",
  "price": "12.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687216494,
  "created_at": 1453409824222,
  "locations": [
    {
      "_id": "56a1462047ffb0036300bb56",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a1462047ffb0036300bb58",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a1462047ffb0036300bb55",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a1462047ffb00007000245",
    "attached_width": 960,
    "attached_height": 893,
    "attached_size": 90656,
    "attached_name": "lakeshore burger.jpg",
    "main": false,
    "updated_at": 1453409824221,
    "created_at": 1453409824221
  },
  "activated_at": 1453409827641,
  "referral_code": "rachelr83",
  "deleted_at": 1472061185037
}
deals << {
  "_id": "56a151a047ffb0752400bfa4",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "545d2b6af02cbe4c8500ff07",
  "title": "Rancho Viejo $25 for $20 Deal",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "Rancho Viejo Hayden $25 for $20 Deal",
  "terms": "Must be redeemed in 1 visit.  Deal valid through December 31st 2016.  $20 Cash value valid after December 31st 2016.  Good during regular business hours.  ONLY VALID AT HAYDEN RANCHO VIEJO.",
  "value": "25",
  "price": "20",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54612638f02cbeb4a00115ec"
  ],
  "expires_at": 1483171200000,
  "user_id": "555245edf02cbe522800339e",
  "updated_at": 1453419451245,
  "created_at": 1453412768747,
  "locations": [
    {
      "_id": "56a151a047ffb0752400bfa3",
      "point": [
        -116.785799,
        47.75220299999999
      ],
      "address": {
        "_id": "56a151a047ffb0752400bfa5",
        "country": "US",
        "street": "8882 N. Government Way,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "56a151a047ffb0752400bfa2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a151a047ffb00007000249",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 58473,
    "attached_name": "ranchoviejo1.jpg",
    "main": false,
    "updated_at": 1453412768747,
    "created_at": 1453412768747
  },
  "activated_at": 1453412825375
}
deals << {
  "_id": "56a15f4647ffb097ae00c150",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "Mac & Cheese (Adult Size",
  "category_id": "4f7d215e74dbb02de300062c",
  "offer": "ONLY $11.00 for Adult Maccaroni & Cheese normally $17.00",
  "terms": "> Must purchase online and show printed voucher to redeem\r\n> Limit 1 per person and 1 per visit \r\n> Not valid with any other special or promotional deal ",
  "value": "17",
  "price": "11",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687218880,
  "created_at": 1453416262932,
  "locations": [
    {
      "_id": "56a15f4647ffb097ae00c14f",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a15f4647ffb097ae00c151",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a15f4647ffb097ae00c14e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a15f4647ffb00007000251",
    "attached_width": 2348,
    "attached_height": 1867,
    "attached_size": 1133156,
    "attached_name": "mac-and-cheese-on-fork.jpg",
    "main": false,
    "updated_at": 1453416262932,
    "created_at": 1453416262932
  },
  "activated_at": 1453416276505,
  "referral_code": "rachelr83",
  "deleted_at": 1472061189021
}
deals << {
  "_id": "56a1608847ffb0c3a700c192",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "Primal Meatball Sliders",
  "category_id": "4f7d216674dbb02de30009bc",
  "offer": "ONLY $10.00 (normally $14.00 for Primal Meatball Sliders normally",
  "terms": "> Must purchase online and show printed voucher to redeem\r\n> Limit 1 per person and 1 per visit \r\n> Not valid with any other special or promotional deal \r\n",
  "value": "14.00",
  "price": "10.00",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687221308,
  "created_at": 1453416584085,
  "locations": [
    {
      "_id": "56a1608847ffb0c3a700c191",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a1608847ffb0c3a700c193",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a1608847ffb0c3a700c190",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a1608847ffb00007000259",
    "attached_width": 960,
    "attached_height": 640,
    "attached_size": 58919,
    "attached_name": "12509334_998152700252334_3370935197345042649_n.jpg",
    "main": false,
    "updated_at": 1453416584085,
    "created_at": 1453416584085
  },
  "activated_at": 1453416587337,
  "referral_code": "rachelr83",
  "deleted_at": 1472061192040
}
deals << {
  "_id": "56a16f2b47ffb0c3a500c3b4",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "Salmon Creek Champagne (1 Bottle",
  "category_id": "4f7d216674dbb02de300098f",
  "offer": "ONLY $20.00 (normally $30.00 for a bottle of Salmon Creek Champagne ",
  "terms": "> Must purchase online and show printed voucher to redeem\r\n> Limit 1 per person and 1 per visit \r\n> Not valid with any other special or promotional deal\r\n> Must be 21 and over to purchase",
  "value": "30.0",
  "price": "20.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687224754,
  "created_at": 1453420331407,
  "locations": [
    {
      "_id": "56a16f2b47ffb0c3a500c3b3",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a16f2b47ffb0c3a500c3b5",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1453420541448,
  "image": {
    "_id": "56a1810c47ffb05cef00c718",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a1810c47ffb00007000284",
    "attached_width": 363,
    "attached_height": 524,
    "attached_size": 21546,
    "attached_name": "salmon creek.JPG",
    "main": false,
    "updated_at": 1453424908087,
    "created_at": 1453424908087
  },
  "referral_code": "rachelr83",
  "deleted_at": 1472061194859
}
deals << {
  "_id": "56a170fa47ffb0aac700c406",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "Vueve du Vernay (1 Bottle",
  "category_id": "4f7d216674dbb02de300098f",
  "offer": "ONLY $25.00 (normally $45.00 for a bottle of Vueve du Vernay",
  "terms": "> Must purchase online and show printed voucher to redeem \r\n> Limit 1 per person and 1 per visit\r\n> Not valid with any other special or promotional deal \r\n> Must be 21 and over to purchase",
  "value": "45",
  "price": "25",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687229351,
  "created_at": 1453420794788,
  "locations": [
    {
      "_id": "56a170fa47ffb0aac700c405",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a170fa47ffb0aac700c407",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a170fa47ffb0aac700c404",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a170fa47ffb00007000263",
    "attached_width": 567,
    "attached_height": 501,
    "attached_size": 38004,
    "attached_name": "veuve du vernay.JPG",
    "main": false,
    "updated_at": 1453420794788,
    "created_at": 1453420794788
  },
  "activated_at": 1453420807423,
  "referral_code": "rachelr83",
  "deleted_at": 1472061198978
}
deals << {
  "_id": "56a173b047ffb02cd800c494",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "Lamb Chops w Glass of Choice Malbec",
  "category_id": "4f7d216674dbb02de300098f",
  "offer": "ONLY $27.00 (normally $30.00 for Lamb Chops w Glass of Choice Malbec",
  "terms": "> Must purchase online and show printed voucher to redeem \r\n> Limit 1 per person and 1 per visit\r\n> Not valid with any other special or promotional deal \r\n> Must be 21 and over to purchase",
  "value": "30.0",
  "price": "27.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1458687233036,
  "created_at": 1453421488749,
  "locations": [
    {
      "_id": "56a173b047ffb02cd800c493",
      "point": [
        -116.8057091,
        47.6944168
      ],
      "address": {
        "_id": "56a173b047ffb02cd800c495",
        "country": "US",
        "street": "2360 N Old Mill Loop",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a1754a47ffb04faa00c50e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a1754a47ffb0000700026e",
    "attached_width": 545,
    "attached_height": 495,
    "attached_size": 38481,
    "attached_name": "lambwine.JPG",
    "main": false,
    "updated_at": 1453421898310,
    "created_at": 1453421898310
  },
  "activated_at": 1453421929129,
  "referral_code": "rachelr83",
  "deleted_at": 1472061202163
}
deals << {
  "_id": "56a176b247ffb0ef9900c52c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56442c6df02cbebdb30009e9",
  "title": "Haircut and deep conditioning treatment ",
  "category_id": "4f7d215374dbb02de3000164",
  "offer": "ONLY $25 for a Haircut with a deep conditioning treatment | Normally $45\r\n\r\nHair by Tari located in Post Falls ID\r\n(208 704-3054\r\ntari.moore.tm@gmail.com",
  "terms": "Valid only with Tari Moore ",
  "value": "45.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56442ff4f02cbe44b6000a75"
  ],
  "expires_at": 1546243200000,
  "user_id": "56442c6df02cbebdb30009e6",
  "updated_at": 1486070345660,
  "created_at": 1453422258337,
  "locations": [
    {
      "_id": "56a176b247ffb0ef9900c52b",
      "point": [
        -116.9145487,
        47.7110821
      ],
      "address": {
        "_id": "56a176b247ffb0ef9900c52d",
        "country": "US",
        "street": "2525 E Seltice Way",
        "suite": "Suite A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1453422265600,
  "referral_code": "reimche",
  "image": {
    "_id": "56b4d9d6802e8805ab002f20",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b4d9d6802e88000700008e",
    "attached_width": 960,
    "attached_height": 960,
    "attached_size": 128990,
    "attached_name": "hairbytariimage1.jpg",
    "main": false,
    "updated_at": 1454692822467,
    "created_at": 1454692822467
  }
}
deals << {
  "_id": "56a270f847ffb0c3c0017d4a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Mother 11.5\" x 6.5\" - FREE SHIPPING ($6.50 VALUE Price reduced ($5.00 VALUE",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "FREE SHIPPING ($6.50 VALUE - Mother 11.5\"x 6.5\" \r\n\r\n> Mother.  Her special day is on the way.  Let her know she is special with this fancy mother's day gift.  She will enjoy it whether she hangs it inside or outside.\r\n\r\n> Available Color Options are:\r\n5 Pink | 5 Red",
  "terms": "> MUST EMAIL seller to discuss other color options still available before purchase \r\n> Sales Tax is included in pricing \r\n> Shipping is FREE ($6.50 VALUE\r\nPrice reduction ($5.00 VALUE\r\nEMAIL seller to discuss option for attached stake.  Stake is welded to some pieces to make them easy to put into a planter or flower bed.",
  "value": "48.9",
  "price": "31.8",
  "limit": 10,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": 1462518000000,
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1462581265516,
  "created_at": 1453486328853,
  "locations": [
    {
      "_id": "56a270f847ffb0c3c0017d49",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56a270f847ffb0c3c0017d4b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a270f847ffb0c3c0017d48",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a270f847ffb000070002b0",
    "attached_width": 853,
    "attached_height": 640,
    "attached_size": 224354,
    "attached_name": "mother 40.jpeg",
    "main": false,
    "updated_at": 1453486328853,
    "created_at": 1453486328853
  },
  "activated_at": 1453486349934
}
deals << {
  "_id": "56a2723447ffb0530c017d83",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Mom - Another Word for Love 13.25\" x 6.25\"",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "Mom - Another Word for Love 13.25\"x 6.25\" FREE SHIPPING ($6.50 VALUE  PRICE REDUCTION ($10 VALUE\r\n> Does this say it all?  No one loves like a mom, especially yours.  Mom's never quit being a mom and their love does not quit.  Let your mom know that you really appreciate her love for you.\r\n\r\n> Available Color Options are:\r\n3 Pink | 3 Red | 2 Yellow | 1 Blue",
  "terms": "> MUST EMAIL seller to discuss color options still available before purchase \r\n> Sales Tax is included in pricing \r\n> Shipping is FREE ($6.50 VALUE\r\nPRICE REDUCTION ($10 VALUE",
  "value": "69.5",
  "price": "42.4",
  "limit": 9,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": 1462518000000,
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1462581266988,
  "created_at": 1453486644036,
  "locations": [
    {
      "_id": "56a2723347ffb0530c017d82",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56a2723447ffb0530c017d84",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a2723347ffb0530c017d81",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a2723347ffb000070002b4",
    "attached_width": 808,
    "attached_height": 640,
    "attached_size": 184861,
    "attached_name": "mom another world 45.jpeg",
    "main": false,
    "updated_at": 1453486644036,
    "created_at": 1453486644036
  },
  "activated_at": 1453486646321
}
deals << {
  "_id": "56a2735f47ffb0692c017dd6",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Mom 12\" x 3\" - FREE SHIPPING ($6.50 VALUE PRICE REDUCTION ($5.00 VALUE",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "FREE SHIPPING ($6.50 VALUE; Price Reduction ($5.00 VALUE - Mom 12\" x 3\" \r\n> Simple but it says it all.  Who else can get this special gift but the mom of your life.  It's beautiful color with the sparkles will catch the eye of everyone in the room.  You can put this beauty outside too so others know mom lives here.\r\n\r\n> Available Color Options are:\r\n15 Red | 4 Black | 5 Wine",
  "terms": "> MUST EMAIL seller to discuss other color options still available before purchase \r\n> Sales Tax is included in pricing \r\n> Shipping is FREE ($6.50 VALUE\r\nPrice reduction ($5.00 VALUE\r\nEMAIL seller to discuss option for attached stake.  Stake is welded to some pieces to make them easy to put into a planter or flower bed.",
  "value": "43.6",
  "price": "31.8",
  "limit": 24,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": 1462518000000,
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1462581268538,
  "created_at": 1453486943781,
  "locations": [
    {
      "_id": "56a2735f47ffb0692c017dd5",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56a2735f47ffb0692c017dd7",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a2735f47ffb0692c017dd4",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a2735f47ffb000070002b8",
    "attached_width": 853,
    "attached_height": 640,
    "attached_size": 215912,
    "attached_name": "mom 35.jpeg",
    "main": false,
    "updated_at": 1453486943780,
    "created_at": 1453486943780
  },
  "activated_at": 1453486953521
}
deals << {
  "_id": "56a2741b47ffb06071017e02",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Mom - Fancy 14\" x 6\" - FREE SHIPPING ($6.50 VALUE ",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": " Mom - Fancy 14\" x 6\"  FREE SHIPPING ($6.50 VALUE PRICE REDUCTION ($5.00 VALUE\r\n\r\n > Mom with a fancy touch.  Add a little extra to the special way you want to tell your mom that she is very special to you.  Elegantly written but it will be even more special when you tell her as you give this gift to her.  She can display this inside or outside with no worries as the Powder Coat will hold up in any weather condition.  All of the colors have that extra sparkle so they will stand out wherever you put them.\r\n\r\n> Available Color Options are:\r\n4 Red | 10 Pink | 4 Blue | 3 Wine",
  "terms": "> MUST EMAIL seller to discuss other color options still available before purchase \r\n> Sales Tax is included in pricing \r\n> Shipping is FREE ($6.50 VALUE\r\nPrice reduction ($5.00 VALUE\r\nEMAIL seller to discuss option for attached stake.  Stake is welded to some pieces to make them easy to put into a planter or flower bed.",
  "value": "43.6",
  "price": "31.8",
  "limit": 21,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": 1462518000000,
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1462581269962,
  "created_at": 1453487131636,
  "locations": [
    {
      "_id": "56a2741b47ffb06071017e01",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56a2741b47ffb06071017e03",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a2741b47ffb06071017e00",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a2741b47ffb000070002bc",
    "attached_width": 853,
    "attached_height": 640,
    "attached_size": 212251,
    "attached_name": "mom fancy 35.jpeg",
    "main": false,
    "updated_at": 1453487131636,
    "created_at": 1453487131636
  },
  "activated_at": 1453487138638
}
deals << {
  "_id": "56a906d647ffb0b055022c66",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Small Standard Print Bandanas for pets 2 for $8 | Normally $12",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "Buy 2 small standard print bandanas for pets $8.  Normally $12\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Must contact seller after purchasing voucher via email or phone to specify print for each bandana. Cannot be combined with any other deals or offers",
  "value": "12.0",
  "price": "8.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429488687,
  "created_at": 1453917910350,
  "locations": [
    {
      "_id": "56a906d647ffb0b055022c65",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "56a906d647ffb0b055022c67",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a906d647ffb0b055022c64",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a906d647ffb0000700036e",
    "attached_width": 960,
    "attached_height": 459,
    "attached_size": 48434,
    "attached_name": "bandanababeprint.jpg",
    "main": false,
    "updated_at": 1453917910350,
    "created_at": 1453917910350
  },
  "activated_at": 1453920264207,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56a9081147ffb017ff022c99",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Medium Standard print Bandana for pets 2 for $10 | Normally $16",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "2 Medium Standard Print Bandanas for $10 Normally $16\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Must contact seller after purchasing voucher via email or phone to specify print for each bandana.  Cannot be combined with any other deals or offers.",
  "value": "16.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429530636,
  "created_at": 1453918225507,
  "locations": [
    {
      "_id": "56a9081147ffb017ff022c98",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "56a9081147ffb017ff022c9a",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a9081147ffb017ff022c97",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a9081147ffb00007000372",
    "attached_width": 640,
    "attached_height": 480,
    "attached_size": 70278,
    "attached_name": "bandanababeprint2.jpg",
    "main": false,
    "updated_at": 1453918225507,
    "created_at": 1453918225507
  },
  "activated_at": 1453918246891,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56a9095f47ffb0593c022ccd",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Large Standard Print Bandana for pets, 2 for $12 | Normally $20",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "2 Large Standard Print Bandanas for pets for only $12!  Normally $20\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Must contact seller after purchasing voucher via email or phone to specify print for each bandana. Cannot be combined with any other deals or offers.",
  "value": "20.0",
  "price": "12.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483084800000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429190761,
  "created_at": 1453918559258,
  "locations": [
    {
      "_id": "56a9095f47ffb0593c022ccc",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "56a9095f47ffb0593c022cce",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a9095f47ffb0593c022ccb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a9095f47ffb00007000376",
    "attached_width": 960,
    "attached_height": 459,
    "attached_size": 52716,
    "attached_name": "bandanababeprint3.jpg",
    "main": false,
    "updated_at": 1453918559257,
    "created_at": 1453918559257
  },
  "activated_at": 1453918575930,
  "referral_code": "currentattractions",
  "deleted_at": 1485232127251
}
deals << {
  "_id": "56a90c2b47ffb0fda3022cf8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d92730ce94431fb003be9",
  "title": "Extra Large Pet Bandana 2 for $16 Normally $26",
  "category_id": "4f7d216274dbb02de3000818",
  "offer": "2 Extra Large Pet Bandanas for $16 | Normally $26\r\n\r\nBandana Babe located in Coeur d'Alene ID\r\n(509 979-9088\r\nkaren@sewinjeanius.com",
  "terms": "Must contact seller after purchasing voucher via email or phone to specify print for each bandana. Cannot be combined with any other deals or offers.",
  "value": "26.0",
  "price": "16.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568ecac90ce9445ad1005c97"
  ],
  "expires_at": 1483171200000,
  "user_id": "568d92730ce94431fb003be6",
  "updated_at": 1477429099162,
  "created_at": 1453919275652,
  "locations": [
    {
      "_id": "56a90c2b47ffb0fda3022cf7",
      "point": [
        -116.7810116,
        47.6737598
      ],
      "address": {
        "_id": "56a90c2b47ffb0fda3022cf9",
        "country": "US",
        "street": "401-1/2 E. Sherman",
        "suite": "#200",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56a90c2b47ffb0fda3022cf6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56a90c2b47ffb0000700037a",
    "attached_width": 960,
    "attached_height": 459,
    "attached_size": 56156,
    "attached_name": "bandanababeprint4.jpg",
    "main": false,
    "updated_at": 1453919275652,
    "created_at": 1453919275652
  },
  "activated_at": 1453919287396,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56b22b7347ffb0f04603140c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b11e1747ffb0e0d002efa2",
  "title": "Eyebrow Shaping & Bikini Wax",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "ONLY $25.00 | Eyebrow Shaping ($20 & Basic Bikini Wax ($25 normally $45",
  "terms": "> If you'd like to upgrade the Basic Bikini wax to a Brazilian wax, the difference must be paid in cash at time of redemption of deal\r\n> If individual redeeming deal is under 18 years old, they MUST have parent signature for any service.",
  "value": "45.0",
  "price": "25.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b150e047ffb0282f02f43b"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b12ce447ffb020f802f0c5",
  "updated_at": 1486070202452,
  "created_at": 1454517107791,
  "locations": [
    {
      "_id": "56b22b7347ffb0f04603140b",
      "point": [
        -116.781154,
        47.694216
      ],
      "address": {
        "_id": "56b22b7347ffb0f04603140d",
        "country": "US",
        "street": "1911 N 4th Street",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1454517111430,
  "image": {
    "_id": "56b247ba47ffb04c8103173c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b247ba47ffb00007000488",
    "attached_width": 878,
    "attached_height": 960,
    "attached_size": 164762,
    "attached_name": "salonrouge3.jpg",
    "main": false,
    "updated_at": 1454524346283,
    "created_at": 1454524346283
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56b2487d47ffb0a390031755",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b11e1747ffb0e0d002efa2",
  "title": "Eyebrow Shaping",
  "category_id": "4f7d216474dbb02de300091f",
  "offer": "Eyebrow Shaping ($20 ONLY $13.00!",
  "terms": "> If individual redeeming deal is under 18 years old, they MUST have parent signature for any service.",
  "value": "20.0",
  "price": "13.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b150e047ffb0282f02f43b"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b12ce447ffb020f802f0c5",
  "updated_at": 1486070230647,
  "created_at": 1454524541935,
  "locations": [
    {
      "_id": "56b2487d47ffb0a390031754",
      "point": [
        -116.781154,
        47.694216
      ],
      "address": {
        "_id": "56b2487d47ffb0a390031756",
        "country": "US",
        "street": "1911 N 4th Street",
        "suite": "",
        "city": "Coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56b2487d47ffb0a390031753",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b2487d47ffb0000700049d",
    "attached_width": 960,
    "attached_height": 960,
    "attached_size": 72007,
    "attached_name": "salonrouge4.jpg",
    "main": false,
    "updated_at": 1454524541935,
    "created_at": 1454524541935
  },
  "activated_at": 1454524545289,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56b25ae147ffb07dbe03194f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b1588f47ffb0474b02f50c",
  "title": "3 Rooms Cleaned",
  "category_id": "57437b69e9330cb82f000a29",
  "offer": "ONLY $70 for Carpet Cleaning | 3 Rooms (Halls and Stairs Considered Rooms | Normally $80\r\n\r\nFamily First Carpet Cleaning located in Spokane WA\r\n(509 499-5274\r\n",
  "terms": "> Halls and Stairs Considered Rooms\r\n> Maximum 12 Steps\r\n> 250 square feet room maximum\r\n> $70 minimum",
  "value": "80.0",
  "price": "70.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b239fd47ffb0b6390315f5"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b1588f47ffb0474b02f509",
  "updated_at": 1486069812884,
  "created_at": 1454529249531,
  "locations": [
    {
      "_id": "56b25ae147ffb07dbe03194e",
      "point": [
        -117.4974091,
        47.5593099
      ],
      "address": {
        "_id": "56b25ae147ffb07dbe031950",
        "country": "US",
        "street": "Cheney-Spokane Rd",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99224"
      }
    }
  ],
  "image": {
    "_id": "56b25ae147ffb07dbe03194d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b25ae147ffb000070004a9",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 47901,
    "attached_name": "familyfirstcarpet3.jpg",
    "main": false,
    "updated_at": 1454529249531,
    "created_at": 1454529249531
  },
  "activated_at": 1454529650628,
  "referral_code": ""
}
deals << {
  "_id": "56b25fea47ffb08fe00319dd",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b1588f47ffb0474b02f50c",
  "title": "4 Rooms Cleaned",
  "category_id": "57437b69e9330cb82f000a29",
  "offer": "$90 for Carpet Cleaning | 4 Rooms (Halls and Stairs Considered Rooms | Normally $110\r\n\r\nFamily First Carpet Cleaning located in Spokane WA\r\n(509 499-5274",
  "terms": "> Halls and Stairs Considered Rooms\r\n> Maximum 12 Steps\r\n> 250 square feet room maximum\r\n> $70 minimum",
  "value": "110.0",
  "price": "90.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b239fd47ffb0b6390315f5"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b1588f47ffb0474b02f509",
  "updated_at": 1486069841199,
  "created_at": 1454530538851,
  "locations": [
    {
      "_id": "56b25fea47ffb08fe00319dc",
      "point": [
        -117.4974091,
        47.5593099
      ],
      "address": {
        "_id": "56b25fea47ffb08fe00319de",
        "country": "US",
        "street": "Cheney-Spokane Rd",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99224"
      }
    }
  ],
  "image": {
    "_id": "56b25fea47ffb08fe00319db",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b25fea47ffb000070004ad",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 47901,
    "attached_name": "familyfirstcarpet3.jpg",
    "main": false,
    "updated_at": 1454530538851,
    "created_at": 1454530538851
  },
  "activated_at": 1454530546393,
  "referral_code": ""
}
deals << {
  "_id": "56b2613b47ffb08e8c031a18",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b1588f47ffb0474b02f50c",
  "title": "5 Rooms Cleaned",
  "category_id": "57437b69e9330cb82f000a29",
  "offer": "$110 for Carpet Cleaning | 5 Rooms (Halls and Stairs Considered Rooms | Normally $140\r\n\r\nFamily First Carpet Cleaning located in Spokane WA\r\n(509 499-5274",
  "terms": "> Halls and Stairs Considered Rooms\r\n> Maximum 12 Steps\r\n> 250 square feet room maximum\r\n> $70 minimum",
  "value": "140.0",
  "price": "110.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b239fd47ffb0b6390315f5"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b1588f47ffb0474b02f509",
  "updated_at": 1486069870747,
  "created_at": 1454530875027,
  "locations": [
    {
      "_id": "56b2613b47ffb08e8c031a17",
      "point": [
        -117.4974091,
        47.5593099
      ],
      "address": {
        "_id": "56b2613b47ffb08e8c031a19",
        "country": "US",
        "street": "Cheney-Spokane Rd",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99224"
      }
    }
  ],
  "image": {
    "_id": "56b2613b47ffb08e8c031a16",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b2613b47ffb000070004b1",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 43510,
    "attached_name": "familyfirstcarpet2.jpg",
    "main": false,
    "updated_at": 1454530875027,
    "created_at": 1454530875027
  },
  "activated_at": 1454530878438,
  "referral_code": ""
}
deals << {
  "_id": "56b261a847ffb0ae52031a31",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b1588f47ffb0474b02f50c",
  "title": "6 Rooms Cleaned",
  "category_id": "57437b69e9330cb82f000a29",
  "offer": "$130 for Carpet Cleaning | 6 Rooms (Halls and Stairs Considered Rooms | Normally $170\r\n\r\nFamily First Carpet Cleaning located in Spokane WA\r\n(509 499-5274",
  "terms": "> Halls and Stairs Considered Rooms\r\n> Maximum 12 Steps\r\n> 250 square feet room maximum\r\n> $70 minimum",
  "value": "170.0",
  "price": "130.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56b239fd47ffb0b6390315f5"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b1588f47ffb0474b02f509",
  "updated_at": 1486069896483,
  "created_at": 1454530984373,
  "locations": [
    {
      "_id": "56b261a847ffb0ae52031a30",
      "point": [
        -117.4974091,
        47.5593099
      ],
      "address": {
        "_id": "56b261a847ffb0ae52031a32",
        "country": "US",
        "street": "Cheney-Spokane Rd",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99224"
      }
    }
  ],
  "image": {
    "_id": "56b261a847ffb0ae52031a2f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b261a847ffb000070004b5",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 47901,
    "attached_name": "familyfirstcarpet3.jpg",
    "main": false,
    "updated_at": 1454530984373,
    "created_at": 1454530984373
  },
  "activated_at": 1454530987122,
  "referral_code": ""
}
deals << {
  "_id": "56b2627947ffb04c8b031a5e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b1588f47ffb0474b02f50c",
  "title": "7 Rooms Cleaned",
  "category_id": "57437b69e9330cb82f000a29",
  "offer": "$150 for Carpet Cleaning | 7 Rooms (Halls and Stairs Considered Rooms | Normally $200\r\n\r\nFamily First Carpet Cleaning located in Spokane WA\r\n(509 499-5274",
  "terms": "> Halls and Stairs Considered Rooms\r\n> Maximum 12 Steps\r\n> 250 square feet room maximum\r\n> $70 minimum",
  "value": "200.0",
  "price": "150.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b239fd47ffb0b6390315f5"
  ],
  "expires_at": 1546243200000,
  "user_id": "56b1588f47ffb0474b02f509",
  "updated_at": 1486069922814,
  "created_at": 1454531193220,
  "locations": [
    {
      "_id": "56b2627947ffb04c8b031a5d",
      "point": [
        -117.4974091,
        47.5593099
      ],
      "address": {
        "_id": "56b2627947ffb04c8b031a5f",
        "country": "US",
        "street": "Cheney-Spokane Rd",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99224"
      }
    }
  ],
  "image": {
    "_id": "56b2627947ffb04c8b031a5c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b2627947ffb000070004b9",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 47901,
    "attached_name": "familyfirstcarpet3.jpg",
    "main": false,
    "updated_at": 1454531193220,
    "created_at": 1454531193220
  },
  "activated_at": 1454531196213,
  "referral_code": ""
}
deals << {
  "_id": "56b262d747ffb048b2031a6e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b1588f47ffb0474b02f50c",
  "title": "8 Rooms Cleaned",
  "category_id": "57437b69e9330cb82f000a29",
  "offer": "$170 for Carpet Cleaning | 8 Rooms (Halls and Stairs Considered Rooms | Normally $230\r\n\r\nFamily First Carpet Cleaning located in Spokane WA\r\n(509 499-5274",
  "terms": "> Halls and Stairs Considered Rooms\r\n> Maximum 12 Steps\r\n> 250 square feet room maximum\r\n> $70 minimum",
  "value": "230.0",
  "price": "170.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "56b239fd47ffb0b6390315f5"
  ],
  "expires_at": "",
  "user_id": "56b1588f47ffb0474b02f509",
  "updated_at": 1477344246552,
  "created_at": 1454531287190,
  "locations": [
    {
      "_id": "56b262d747ffb048b2031a6d",
      "point": [
        -117.4974091,
        47.5593099
      ],
      "address": {
        "_id": "56b262d747ffb048b2031a6f",
        "country": "US",
        "street": "Cheney-Spokane Rd",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99224"
      }
    }
  ],
  "image": {
    "_id": "56b262d747ffb048b2031a6c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b262d747ffb000070004bd",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 47901,
    "attached_name": "familyfirstcarpet3.jpg",
    "main": false,
    "updated_at": 1454531287190,
    "created_at": 1454531287190
  },
  "activated_at": 1454531289914,
  "referral_code": "",
  "deleted_at": 1485231526447
}
deals << {
  "_id": "56b511a4802e880ee300344c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "title": "Pamper Package $99 | Normally $120",
  "category_id": "4f7d215a74dbb02de3000436",
  "offer": "Treat yourself or your special someone to a customized pamper package today! \r\n\r\nStunning Creations Salon located in Post Falls ID\r\n(208 262-8019\r\n\r\nChoose from three of the following enticing services to enjoy on the special day of your choice:\r\n> Pedicure \r\n> Manicure \r\n> Deep Condition \r\n> Updo \r\n> Haircut & Style \r\n> Facial Wax \r\n> Theracure \r\n\r\n* Enjoy one glass of wine or an ice cold beer as a thank you from all of us here at Stunning Creations Salon (age restrictions apply",
  "terms": "> Must be over 21 to take advantage of the alcohol",
  "value": "120.0",
  "price": "99.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "569ff05e47ffb01d870080af"
  ],
  "expires_at": 1522479600000,
  "user_id": "55c14d2af02cbeecaf000f78",
  "updated_at": 1486069381843,
  "created_at": 1454707108669,
  "locations": [
    {
      "_id": "56b511a4802e880ee300344b",
      "point": [
        -116.9148121,
        47.7111491
      ],
      "address": {
        "_id": "56b511a4802e880ee300344d",
        "country": "US",
        "street": "2525 e seltice way",
        "suite": "A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56b511a4802e880ee300344a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b511a4802e8800070000ef",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 43509,
    "attached_name": "spa.jpg",
    "main": false,
    "updated_at": 1454707108668,
    "created_at": 1454707108668
  },
  "activated_at": 1454707111816,
  "referral_code": ""
}
deals << {
  "_id": "56b529d3802e888a30003638",
  "active": true,
  "vouchers_count": 3,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$13 for $20 Worth of Burgers and Food",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "$13 for $20 Schmidty's Deal - Enjoy Great Burgers and Food",
  "terms": "> Offer expires August 2nd 2016. \r\n> Must be used during regular business hours.\r\n> Must be used in 1 visit. \r\n> $19.99 Cash value valid after August 2nd 2016",
  "value": "20.0",
  "price": "13.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1470121200000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1464707036177,
  "created_at": 1454713299904,
  "locations": [
    {
      "_id": "56b529d3802e888a30003637",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "56b529d3802e888a30003639",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1454713308980,
  "image": {
    "_id": "56b52c0c802e882f7f003681",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b52c0c802e8800070000f7",
    "attached_width": 960,
    "attached_height": 717,
    "attached_size": 92773,
    "attached_name": "schmidtys3.jpg",
    "main": false,
    "updated_at": 1454713868558,
    "created_at": 1454713868558
  },
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56b8ca1e802e8850eb009153",
  "active": false,
  "vouchers_count": 4,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "The Local Deli $25 for $20 Deal",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": "$25 for $20 Local Deli Deal",
  "terms": "Cannot be used with other deals, discounts, or offers.  Must be used in 1 visit.  Valid during regular business hours.  $20 cash value valid after 12/31/16",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": 3,
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1483084800000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1460391718818,
  "created_at": 1454950942260,
  "locations": [
    {
      "_id": "56b8ca1e802e8850eb009152",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "56b8ca1e802e8850eb009154",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "56b8ca1e802e8850eb009151",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b8ca1e802e880007000104",
    "attached_width": 960,
    "attached_height": 960,
    "attached_size": 115966,
    "attached_name": "localdelidealpromo.jpg",
    "main": false,
    "updated_at": 1454950942260,
    "created_at": 1454950942260
  },
  "activated_at": 1454951462385,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56b8cd9e802e889f9a00918f",
  "active": true,
  "vouchers_count": 8,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "Buy a 1/2 Sandwich, get a free Soda",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "Purchase Classic 1/2 Sandwich and get a 20oz Soda free",
  "terms": "Cannot be combined with any other deals, discounts or coupons.  Valid during normal business hours",
  "value": "8.35",
  "price": "6.3",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1483171200000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1461891070689,
  "created_at": 1454951838492,
  "locations": [
    {
      "_id": "56b8cd9e802e889f9a00918e",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "56b8cd9e802e889f9a009190",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "56b8cd9e802e889f9a00918d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56b8cd9e802e880007000108",
    "attached_width": 593,
    "attached_height": 237,
    "attached_size": 43500,
    "attached_name": "localdelidealpromo2.jpg",
    "main": false,
    "updated_at": 1454951838491,
    "created_at": 1454951838491
  },
  "activated_at": 1454951867989,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56bcf18ca10a1a1cd50017f9",
  "active": false,
  "vouchers_count": 2,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "144 Safety & Sunglasses With Custom Logo",
  "category_id": "4f7d216374dbb02de3000893",
  "offer": "Safety & Sunglasses With Custom Logo\r\n> Choose between a few different model safety/sunglasses for your charity, club or business.  These glasses come in smoke, clear and amber colors.  \r\n> Email to discuss colors",
  "terms": "> For custom Logos please send CMYK artwork in Vector Art or in Illustrator  .ill, .ai or .fh or .eps  extensions.  \r\n> This is the format that your logo is created in and we can reproduce it exactly and is required if it is a registered logo.  This will make perfect art work.\r\n> If it has colors please send us the colors using the international Pantone Color Chart. ",
  "value": "1440.0",
  "price": "720.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459799024652,
  "created_at": 1455223180502,
  "locations": [
    {
      "_id": "56bcf18ca10a1a1cd50017f8",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56bcf18ca10a1a1cd50017fa",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1455223183338,
  "image": {
    "_id": "56d5c406a5a015f3fa0076cb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56d5c406a5a015000d000075",
    "attached_width": 492,
    "attached_height": 446,
    "attached_size": 33877,
    "attached_name": "logo.JPG",
    "main": false,
    "updated_at": 1456849926656,
    "created_at": 1456849926656
  }
}
deals << {
  "_id": "56c3a492bd40b1659d005ef5",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "5487976df02cbe84660098ef",
  "title": "Stateline Special Chips & 20oz Soda",
  "category_id": "4f7d215e74dbb02de300060a",
  "offer": "Buy a #15 Stateline Special  1/2 Sandwich and get a 20oz Soda and Homemade Potato Chips for Free.  Sandwich includes Ham, Pastrami, Salami, Pepperoni, Turkey, Bologna, Cotto, Provolone and Swiss Cheese.",
  "terms": "Good during regular business hours. Must present voucher.  Not valid with any other deal or coupons. Expires 12/30/16.  Cash value valid after 12/30/16.",
  "value": "11.0",
  "price": "9.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5490bb45f02cbe24e4000987"
  ],
  "expires_at": 1483084800000,
  "user_id": "5487976df02cbe84660098ec",
  "updated_at": 1468886705531,
  "created_at": 1455662226046,
  "locations": [
    {
      "_id": "56c3a491bd40b1659d005ef4",
      "point": [
        -117.038393,
        47.70429
      ],
      "address": {
        "_id": "56c3a492bd40b1659d005ef6",
        "country": "US",
        "street": "6891 W. Seltice Way,",
        "suite": "B,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56c3a491bd40b1659d005ef3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c3a491bd40b1000e0000a7",
    "attached_width": 3264,
    "attached_height": 2448,
    "attached_size": 2019550,
    "attached_name": "statelinesubsdeal.JPG",
    "main": false,
    "updated_at": 1455662226045,
    "created_at": 1455662226045
  },
  "activated_at": 1455662258971,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56c3b3acbd40b155d000600d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5487976df02cbe84660098ef",
  "title": "1/2 Sandwich Chips & 20oz Soda",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": " Buy a #1 through #14 | 1/2 Sandwich and get a 20oz Soda and Homemade Potato Chips for free.  \r\n\r\nStateline Subs & Pizza located in Post Falls ID\r\n(208 819-3215\r\n",
  "terms": "Good during regular business hours.  Not valid with any other offers or coupons.  Good for your choice any 1/2 sandwich #1 through #14.  $5 cash value valid after 12/30/16",
  "value": "6.0",
  "price": "5.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "5490bb45f02cbe24e4000987"
  ],
  "expires_at": 1483171200000,
  "user_id": "5487976df02cbe84660098ec",
  "updated_at": 1477343127580,
  "created_at": 1455666092897,
  "locations": [
    {
      "_id": "56c3b3acbd40b155d000600c",
      "point": [
        -117.038393,
        47.70429
      ],
      "address": {
        "_id": "56c3b3acbd40b155d000600e",
        "country": "US",
        "street": "6891 W. Seltice Way,",
        "suite": "B,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56c3b3acbd40b155d000600b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c3b3acbd40b1000e0000b5",
    "attached_width": 360,
    "attached_height": 250,
    "attached_size": 17261,
    "attached_name": "statelinepizza2.jpg",
    "main": false,
    "updated_at": 1455666092897,
    "created_at": 1455666092897
  },
  "activated_at": 1455666100135,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56c3b6bcbd40b1569a00604d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5487976df02cbe84660098ef",
  "title": "10\" Pizza (Unlimited Toppings",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": " ONLY $8.50 for a 10\" Pizza with UNLIMITED TOPPINGS | Normally $10\r\n\r\nStateline Subs & Pizza located in Post Falls ID\r\n(208 819-3215",
  "terms": "Good during regular business hours. Offer not valid with any other deals or coupons. 1 per person. 1 per visit. $8.50 cash value",
  "value": "10.0",
  "price": "8.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5490bb45f02cbe24e4000987"
  ],
  "expires_at": 1514707200000,
  "user_id": "5487976df02cbe84660098ec",
  "updated_at": 1477343166864,
  "created_at": 1455666876789,
  "locations": [
    {
      "_id": "56c3b6bcbd40b1569a00604c",
      "point": [
        -117.038393,
        47.70429
      ],
      "address": {
        "_id": "56c3b6bcbd40b1569a00604e",
        "country": "US",
        "street": "6891 W. Seltice Way,",
        "suite": "B,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56c3b6bcbd40b1569a00604b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c3b6bcbd40b1000e0000b9",
    "attached_width": 506,
    "attached_height": 400,
    "attached_size": 152494,
    "attached_name": "statelinesubspizzadeal.jpg",
    "main": false,
    "updated_at": 1455666876788,
    "created_at": 1455666876788
  },
  "activated_at": 1455666927268,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56c3bf2bbd40b14e3a0060cf",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55aeda05f02cbe0e7600aceb",
  "title": "Brazilian Blowout",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "Brazilian Blowout Deal. $200",
  "terms": "Must contact Seller by phone or email for appointment.  Must present voucher with phone or buyer can print out voucher. Good for Hair by Jenny at Traci and Charli's salon.",
  "value": "350.0",
  "price": "200.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "55aee0c7f02cbe3d6c00a2f4"
  ],
  "expires_at": 1483084800000,
  "user_id": "55aed9a1f02cbe91fd00a0db",
  "updated_at": 1471452037868,
  "created_at": 1455669035136,
  "locations": [
    {
      "_id": "56c3bf2bbd40b14e3a0060ce",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "56c3bf2bbd40b14e3a0060d0",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56c3bf2bbd40b14e3a0060cd",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c3bf2bbd40b1000e0000bd",
    "attached_width": 720,
    "attached_height": 331,
    "attached_size": 69432,
    "attached_name": "Brazilian-Blowout-Before-After.jpg",
    "main": false,
    "updated_at": 1455669035136,
    "created_at": 1455669035136
  },
  "activated_at": 1455669130575,
  "referral_code": "currentattractions",
  "deleted_at": 1471452556164
}
deals << {
  "_id": "56c3c933bd40b1a85a00620c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55aeda05f02cbe0e7600aceb",
  "title": "Full Head Highlights",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "Full Head Highlights  Normally $120 for only $85",
  "terms": "Must contact seller vial phone or email to make an appointment after purchasing Deal Voucher.  Cash value valid after 12/30/16",
  "value": "120.0",
  "price": "85.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55aee0c7f02cbe3d6c00a2f4"
  ],
  "expires_at": 1483084800000,
  "user_id": "55aed9a1f02cbe91fd00a0db",
  "updated_at": 1471452040345,
  "created_at": 1455671603535,
  "locations": [
    {
      "_id": "56c3c933bd40b1a85a00620b",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "56c3c933bd40b1a85a00620d",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56c3c933bd40b1a85a00620a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c3c933bd40b1000e0000c1",
    "attached_width": 335,
    "attached_height": 450,
    "attached_size": 65688,
    "attached_name": "Jennymundayhightlights.jpg",
    "main": false,
    "updated_at": 1455671603535,
    "created_at": 1455671603535
  },
  "activated_at": 1455671615473,
  "referral_code": "currentattractions",
  "deleted_at": 1471452560173
}
deals << {
  "_id": "56c6723cbd40b11dd2009b1f",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55aeda05f02cbe0e7600aceb",
  "title": "Fusion Shades $85 for $60",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "Get Fusion Shades for Only $60, Usually $85",
  "terms": "Must contact seller via phone or email to make appointment.  Good for Hair by Jenny at Traci and Charli's Full Service Salon. Must redeem in 1 visit.",
  "value": "85.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55aee0c7f02cbe3d6c00a2f4"
  ],
  "expires_at": 1483084800000,
  "user_id": "55aed9a1f02cbe91fd00a0db",
  "updated_at": 1471452042654,
  "created_at": 1455845948439,
  "locations": [
    {
      "_id": "56c6723cbd40b11dd2009b1e",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "56c6723cbd40b11dd2009b20",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1455846002179,
  "image": {
    "_id": "56c67543bd40b16a2a009b5b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c67543bd40b1000e00013a",
    "attached_width": 3264,
    "attached_height": 2448,
    "attached_size": 2253409,
    "attached_name": "jennymundaymarketpad.JPG",
    "main": false,
    "updated_at": 1455846723696,
    "created_at": 1455846723696
  },
  "deleted_at": 1471452563323
}
deals << {
  "_id": "56c78affbd40b18b2400af58",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5570d8fcf02cbe462c00e318",
  "title": "Picnic travel size cheese board 8\"- 10\"",
  "category_id": "4f7d216374dbb02de300085d",
  "offer": "Picnic travel size cheese board 8\"- 10\" for $15 hand crafted from juniper trees from The Mascall Ranch. ",
  "terms": "Must contact seller via email or phone to order.  Free local delivery within 20 miles of Coeur d' Alene. Delivery time depending on availability.",
  "value": "25.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "559c3465f02cbecb34000b26"
  ],
  "expires_at": 1483171200000,
  "user_id": "5570d8fcf02cbe462c00e315",
  "updated_at": 1456963819419,
  "created_at": 1455917823512,
  "locations": [
    {
      "_id": "56c78affbd40b18b2400af57",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56c78affbd40b18b2400af59",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56c78affbd40b18b2400af56",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c78affbd40b1000e00019c",
    "attached_width": 2656,
    "attached_height": 1494,
    "attached_size": 690186,
    "attached_name": "koshcheeseboardsmall.jpg",
    "main": false,
    "updated_at": 1455917823512,
    "created_at": 1455917823512
  },
  "activated_at": 1455917856300,
  "referral_code": "currentattractions",
  "deleted_at": 1485231369490
}
deals << {
  "_id": "56c78ce1bd40b1a56c00afe0",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "5570d8fcf02cbe462c00e318",
  "title": "Medium 12-14\" Cheese Board $29",
  "category_id": "4f7d216374dbb02de300085d",
  "offer": " Medium 12-14\" Cheese Board $29, hand crafted from juniper trees from The Mascall Ranch. ",
  "terms": "Must contact seller via phone or email to order.  Free delivery within 20 miles of Coeur d' Alene.  Delivery time depending on availability.",
  "value": "45.0",
  "price": "29.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "559c3465f02cbecb34000b26"
  ],
  "expires_at": "",
  "user_id": "5570d8fcf02cbe462c00e315",
  "updated_at": 1478218950167,
  "created_at": 1455918305727,
  "locations": [
    {
      "_id": "56c78ce1bd40b1a56c00afdf",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56c78ce1bd40b1a56c00afe1",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56c78ce1bd40b1a56c00afde",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c78ce1bd40b1000e0001a4",
    "attached_width": 2669,
    "attached_height": 1501,
    "attached_size": 675325,
    "attached_name": "koshcheeseboardmedium.jpg",
    "main": false,
    "updated_at": 1455918305727,
    "created_at": 1455918305727
  },
  "activated_at": 1455918328104,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56c7b0a1bd40b1abe400c0bf",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b3cd21802e881648002210",
  "title": "Full Brazilian Wax",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "SAVE $10 | Full Brazilian Wax (First time clients only ONLY $65 normally $75",
  "terms": "> For Women Only\r\n> Deal valid ONLY for first time clients",
  "value": "75.0",
  "price": "65.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56c7a5f9bd40b133d800bdb0"
  ],
  "expires_at": 1485936000000,
  "user_id": "56c7a17dbd40b151c900b7b0",
  "updated_at": 1485231201011,
  "created_at": 1455927457300,
  "locations": [
    {
      "_id": "56c7b0a1bd40b1abe400c0be",
      "point": [
        -116.8073089,
        47.6948666
      ],
      "address": {
        "_id": "56c7b0a1bd40b1abe400c0c0",
        "country": "US",
        "street": "2062 Main Street",
        "suite": "4",
        "city": " Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1455927461724,
  "image": {
    "_id": "56c7b1c1bd40b1a58000c0ed",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c7b1c1bd40b1000e0001b4",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 44643,
    "attached_name": "c700x420.jpg",
    "main": false,
    "updated_at": 1455927745883,
    "created_at": 1455927745883
  },
  "referral_code": ""
}
deals << {
  "_id": "56c7b1a9bd40b177c200c0d2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b3cd21802e881648002210",
  "title": "Full Brazilian Wax Maintenance ",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "Full Brazilian Wax Maintenance  ",
  "terms": "> For women only",
  "value": "55.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56c7a5f9bd40b133d800bdb0"
  ],
  "expires_at": 1485936000000,
  "user_id": "56c7a17dbd40b151c900b7b0",
  "updated_at": 1485231239588,
  "created_at": 1455927721500,
  "locations": [
    {
      "_id": "56c7b1a9bd40b177c200c0d1",
      "point": [
        -116.8073089,
        47.6948666
      ],
      "address": {
        "_id": "56c7b1a9bd40b177c200c0d3",
        "country": "US",
        "street": "2062 Main Street",
        "suite": "4",
        "city": " Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56c7b1a9bd40b177c200c0d0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c7b1a9bd40b1000e0001b2",
    "attached_width": 756,
    "attached_height": 373,
    "attached_size": 59823,
    "attached_name": "brazilian-wax-citrus.jpg",
    "main": false,
    "updated_at": 1455927721500,
    "created_at": 1455927721500
  },
  "activated_at": 1455927725164
}
deals << {
  "_id": "56c7b2ecbd40b1f8c400c0f8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b3cd21802e881648002210",
  "title": "Earlobe Piercing",
  "category_id": "4f7d215a74dbb02de300043a",
  "offer": "Earlobe Piercing \r\n",
  "terms": "> Must be over 18 or have parental consent ",
  "value": "25.0",
  "price": "22.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56c7a5f9bd40b133d800bdb0"
  ],
  "expires_at": 1485936000000,
  "user_id": "56c7a17dbd40b151c900b7b0",
  "updated_at": 1485231252854,
  "created_at": 1455928044016,
  "locations": [
    {
      "_id": "56c7b2ebbd40b1f8c400c0f7",
      "point": [
        -116.8073089,
        47.6948666
      ],
      "address": {
        "_id": "56c7b2ecbd40b1f8c400c0f9",
        "country": "US",
        "street": "2062 Main Street",
        "suite": "4",
        "city": " Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56c7b2ebbd40b1f8c400c0f6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56c7b2ecbd40b1000e0001b8",
    "attached_width": 350,
    "attached_height": 500,
    "attached_size": 48206,
    "attached_name": "ear-piercing.jpg",
    "main": false,
    "updated_at": 1455928044015,
    "created_at": 1455928044015
  },
  "activated_at": 1455928046967
}
deals << {
  "_id": "56d478c4a5a015dc43005664",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "56c4990cbd40b1a8a30071c9",
  "title": "$10 for $20 Worth of Food & Drinks",
  "category_id": "4f7d215e74dbb02de3000651",
  "offer": "$10 for $20 Worth of Food & Drinks\r\n> Good at either Cheney WA or Airway Heights Locations",
  "terms": "Amount paid never expires. Limit 1 per person. Limit 1 per visit. Limit 1 per table. Valid only for option purchased. Not valid with any daily specials.",
  "value": "20",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d328c4a5a0153e9a0038b7"
  ],
  "expires_at": "",
  "user_id": "56c73f07bd40b1ad7200a8a1",
  "updated_at": 1485227451128,
  "created_at": 1456765124713,
  "locations": [
    {
      "_id": "56d478c4a5a015dc43005663",
      "point": [
        -117.5662009,
        47.4970955
      ],
      "address": {
        "_id": "56d478c4a5a015dc43005665",
        "country": "US",
        "street": "1706 2nd Street",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99004"
      }
    }
  ],
  "image": {
    "_id": "56d478c4a5a015dc43005662",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56d478c4a5a015000d000042",
    "attached_width": 650,
    "attached_height": 350,
    "attached_size": 46884,
    "attached_name": "menu.jpg",
    "main": false,
    "updated_at": 1456765124712,
    "created_at": 1456765124712
  },
  "activated_at": 1456765129027,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56d47e93a5a015cc2c0058ea",
  "active": false,
  "vouchers_count": 3,
  "profile_id": "56c4990cbd40b1a8a30071c9",
  "title": "Cheney Lanes $22 | 60 Mins of Bowling for up to 4 w  14\" 2 topping Pizza, 4 20oz Drinks & Shoe Rental",
  "category_id": "4f7d215974dbb02de30003ff",
  "offer": "Cheney Lanes $22 | 60 Mins of Bowling for up to 4 w  14\" Pizza,  4 20oz Drinks & Shoe Rental - $47.00 Value.  (509 235-5263.",
  "terms": "Amount paid never expires. May be repurchased every 120 days. Reservation required. Limit 2 per person, may buy 1 additional as gift. Limit 1 per visit. Valid only for option purchased.",
  "value": "47.0",
  "price": "22.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d328c4a5a0153e9a0038b7"
  ],
  "expires_at": "",
  "user_id": "56c73f07bd40b1ad7200a8a1",
  "updated_at": 1485227473263,
  "created_at": 1456766611062,
  "locations": [
    {
      "_id": "56d47e93a5a015cc2c0058e9",
      "point": [
        -117.5662009,
        47.4970955
      ],
      "address": {
        "_id": "56d47e93a5a015cc2c0058eb",
        "country": "US",
        "street": "1706 2nd Street",
        "suite": "",
        "city": "Cheney",
        "region": "WA",
        "postal_code": "99004"
      }
    }
  ],
  "activated_at": 1456766614039,
  "image": {
    "_id": "56d47ea9a5a015ff6e005901",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56d47ea8a5a015000d000057",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 117300,
    "attached_name": "c700x420.jpg",
    "main": false,
    "updated_at": 1456766633165,
    "created_at": 1456766633165
  }
}
deals << {
  "_id": "56e0e8621aa7206d3300ed4c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Beets",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Beets.  The beets are easy to find with this beautiful garden marker which is on top of an 18\" rod.  You can almost taste the beets as you look at this beautifully colored sign to mark your row of beets.  Regular price is $40 + $2.40 tax + $12 shipping.  Now only $37.80",
  "terms": "Shipping Included\r\nTax Included\r\n",
  "value": "54.40",
  "price": "37.80",
  "limit": 15,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457580130109,
  "created_at": 1457580130109,
  "locations": [
    {
      "_id": "56e0e8611aa7206d3300ed4b",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e0e8621aa7206d3300ed4d",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e0e8611aa7206d3300ed4a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e0e8611aa720000600005e",
    "attached_width": 1936,
    "attached_height": 1936,
    "attached_size": 2070357,
    "attached_name": "betts-deck1.JPG",
    "main": false,
    "updated_at": 1457580130109,
    "created_at": 1457580130109
  },
  "deleted_at": 1457580152172
}
deals << {
  "_id": "56e19eac1aa720271300fc14",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Chicken Strips, Fries and Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": " ONLY $9 for Chicken Strips, Fries and Small Soda (Normally $10\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com",
  "terms": "Cannot be combined with any other deals or offers.  Must be used in 1 visit..",
  "value": "10.0",
  "price": "9.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1485936000000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1485229733351,
  "created_at": 1457626796649,
  "locations": [
    {
      "_id": "56e19eac1aa720271300fc13",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "56e19eac1aa720271300fc15",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e19eac1aa720271300fc12",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e19eac1aa7200006000118",
    "attached_width": 607,
    "attached_height": 464,
    "attached_size": 54410,
    "attached_name": "fedorachickenstripsdeal.jpg",
    "main": false,
    "updated_at": 1457626796648,
    "created_at": 1457626796648
  },
  "activated_at": 1457626859653,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56e1a3661aa720d98b00fe05",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Cheese Burger, Fries and a Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": "ONLY $8.50 for a Fedora Cheese Burger, Fries and a Small Soda (Normally $9.75\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com",
  "terms": "Cannot be combined with any other offers or coupons.  Must be used in 1 visit.  Offer valid through 12/31/16.  $8.50 cash valid always.",
  "value": "9.75",
  "price": "8.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1485936000000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1485229744088,
  "created_at": 1457628006164,
  "locations": [
    {
      "_id": "56e1a3661aa720d98b00fe04",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "56e1a3661aa720d98b00fe06",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e1a3661aa720d98b00fe03",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e1a3661aa720000600011c",
    "attached_width": 640,
    "attached_height": 460,
    "attached_size": 153670,
    "attached_name": "fedoraburgerdeal.png",
    "main": false,
    "updated_at": 1457628006164,
    "created_at": 1457628006164
  },
  "activated_at": 1457628061174,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56e1ae9d1aa720f9c300ff2e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "1/2 Cuban Melt, Fries or Ceasar Salad and Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": "ONLY $11 for a 1/2 Cuban Melt, Fries or Ceasar Salad, and Small Soda (Normally $13.75\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com",
  "terms": "Cannot be combined with any other offers or coupons. Must be used in 1 visit. Offer valid through 12/31/16. $8.50 cash valid always.",
  "value": "13.75",
  "price": "11.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1485936000000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1485229721723,
  "created_at": 1457630877899,
  "locations": [
    {
      "_id": "56e1ae9d1aa720f9c300ff2d",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "56e1ae9d1aa720f9c300ff2f",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e1af5b1aa720974f00ff55",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e1af5b1aa7200006000122",
    "attached_width": 314,
    "attached_height": 323,
    "attached_size": 73058,
    "attached_name": "fedoracubanmelt.jpg",
    "main": false,
    "updated_at": 1457631067294,
    "created_at": 1457631067294
  },
  "activated_at": 1457631079621,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56e2385e1aa72011a50112c2",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom 27\"x12.5\" Reduced shipping ($6.00 VALUE Price reduction ($10 VALUE",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Want to add a little color and a little encouragement to your garden.  These blooms will do both.  The bright colors will really stand out and beautify your garden even when things are waiting to blossom.\r\n\r\n16\" stake welded to Bloom adds to visibility in the garden or flower bed\r\n",
  "terms": "While supplies last\r\nTax included\r\nShipping reduced ($6.00 VALUE\r\nPrice Reduced ($10 VALUE\r\n7 available",
  "value": "54.40",
  "price": "37.80",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457673668023,
  "created_at": 1457666142056,
  "locations": [
    {
      "_id": "56e2385d1aa72011a50112c1",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e2385e1aa72011a50112c3",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e2385d1aa72011a50112c0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e2385d1aa720000600014a",
    "attached_width": 1936,
    "attached_height": 1936,
    "attached_size": 1645114,
    "attached_name": "bloom yellow flowers tulips.JPG",
    "main": false,
    "updated_at": 1457666142056,
    "created_at": 1457666142056
  },
  "deleted_at": 1457673784675
}
deals << {
  "_id": "56e239bb1aa72034980112ed",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom 27\"x12.5\" Reduced shipping ($6.00 VALUE Price reduction ($10 VALUE",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Want to add a little color and a little encouragement to your garden.  These blooms will do both.  The bright colors will really stand out and beautify your garden even when things are waiting to blossom.\r\n\r\n16\" stake welded to the Bloom adds to appeal and visibility.",
  "terms": "Reduced Shipping ($6.00 VALUE\r\nReduced Price ($10.00 VALUE\r\n7 Available\r\nWhile supplies last",
  "value": "54.40",
  "price": "37.80",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457673677863,
  "created_at": 1457666491902,
  "locations": [
    {
      "_id": "56e239ba1aa72034980112ec",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e239bb1aa72034980112ee",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e239ba1aa72034980112eb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e239ba1aa720000600015a",
    "attached_width": 1936,
    "attached_height": 1936,
    "attached_size": 1656417,
    "attached_name": "bloom red flowers-tulips1.JPG",
    "main": false,
    "updated_at": 1457666491902,
    "created_at": 1457666491902
  },
  "deleted_at": 1457673793187
}
deals << {
  "_id": "56e23a9a1aa720421e01131a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom 27\"x12.5\" Reduced shipping ($6.00 VALUE Price reduction ($10 VALUE",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Want to add a little color and a little encouragement to your garden.  These blooms will do both.  The bright colors will really stand out and beautify your garden even when things are waiting to blossom.\r\n\r\n16\" stake welded to the Bloom adds to appeal and visibility.",
  "terms": "Tax Included\r\nPrice reduction ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\n7 available\r\nWhile supplies last",
  "value": "54.40",
  "price": "37.80",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457666714145,
  "created_at": 1457666714145,
  "locations": [
    {
      "_id": "56e23a991aa720421e011319",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e23a9a1aa720421e01131b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e23a991aa720421e011318",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e23a991aa720000600016a",
    "attached_width": 1936,
    "attached_height": 1936,
    "attached_size": 1748326,
    "attached_name": "bloom orange flowers tulips1.JPG",
    "main": false,
    "updated_at": 1457666714145,
    "created_at": 1457666714145
  },
  "deleted_at": 1457673806423
}
deals << {
  "_id": "56e23e271aa720d110011356",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Carrots 23.5\" x 12.5\" ",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Carrots are good for the eyes and this garden sign will sure catch some eyes.  It is bright orange with a 14.5\" rod so there will be no doubt about where the carrots are.  Watch out for rabbits. \r\n",
  "terms": "Price reduced ($10.00 VALUE\r\nReduced Shipping ($6.00 VALUE\r\nTax Included\r\n21 Available\r\nWhile supplies last",
  "value": "54.40",
  "price": "37.80",
  "limit": 21,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457667623104,
  "created_at": 1457667623104,
  "locations": [
    {
      "_id": "56e23e261aa720d110011355",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e23e271aa720d110011357",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e23e261aa720d110011354",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e23e261aa7200006000187",
    "attached_width": 2156,
    "attached_height": 3178,
    "attached_size": 2293306,
    "attached_name": "carrots in pot.JPG",
    "main": false,
    "updated_at": 1457667623104,
    "created_at": 1457667623104
  },
  "deleted_at": 1457673814773
}
deals << {
  "_id": "56e23f711aa7206c7c011384",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Tomatoes 23.5\" x 12.5\" ",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "A splendid addition to your garden to help identify your plant rows.  The 18\" rod sticks into the ground and still stands tall enough for all to see.  You will want to get those tomatoes in the ground as soon as possible as this stake will get you craving the real thing.",
  "terms": "Price reduced ($10.00 VALUE\r\nReduced Shipping ($6.00 VALUE\r\nTax included\r\n4 Available\r\nWhile supplies last",
  "value": "54.40",
  "price": "37.80",
  "limit": 4,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457667953459,
  "created_at": 1457667953459,
  "locations": [
    {
      "_id": "56e23f701aa7206c7c011383",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e23f711aa7206c7c011385",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e23f701aa7206c7c011382",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e23f701aa720000600019c",
    "attached_width": 2203,
    "attached_height": 3259,
    "attached_size": 2362965,
    "attached_name": "tomatoes in pot.JPG",
    "main": false,
    "updated_at": 1457667953459,
    "created_at": 1457667953459
  },
  "deleted_at": 1457673826402
}
deals << {
  "_id": "56e240ad1aa720acda0113ae",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Green Beans  23.5\" x 15\"",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Green Beans.  No more wondering where you planted the green beans.  And when you send some city folk to pick the green beans, they will know which row to pick.",
  "terms": "Price Reduced ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax included\r\n8 available\r\nWhile supplies last",
  "value": "54.4",
  "price": "37.8",
  "limit": 8,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1457673689526,
  "created_at": 1457668269703,
  "locations": [
    {
      "_id": "56e240ad1aa720acda0113ad",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e240ad1aa720acda0113af",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e240e21aa72039b10113d0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e240e11aa72000060001a7",
    "attached_width": 3165,
    "attached_height": 2844,
    "attached_size": 2681478,
    "attached_name": "green beans in pot close.JPG",
    "main": false,
    "updated_at": 1457668322429,
    "created_at": 1457668322429
  },
  "deleted_at": 1457673835826
}
deals << {
  "_id": "56e242dc1aa720dd740113de",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Beets 22\" x 12\"",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Beets.  The beets are easy to find with this beautiful garden marker which is on top of an 18\" rod.  You can almost taste the beets as you look at this beautifully colored sign to mark your row of beets.\r\nPrice reduction ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax included",
  "terms": "10 available\r\nwhile supplies last",
  "value": "54.4",
  "price": "37.8",
  "limit": 10,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841296534,
  "created_at": 1457668828431,
  "locations": [
    {
      "_id": "56e242dc1aa720dd740113dd",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e242dc1aa720dd740113df",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1457668894197,
  "image": {
    "_id": "56e2459d1aa7201425011424",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e2459d1aa72000060001c3",
    "attached_width": 2821,
    "attached_height": 3526,
    "attached_size": 3331043,
    "attached_name": "beets in pot.JPG",
    "main": false,
    "updated_at": 1457669534295,
    "created_at": 1457669534295
  }
}
deals << {
  "_id": "56e2469f1aa72090ef011450",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Tomatoes 23.5\" x 12.5\" ",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "A splendid addition to your garden to help identify your plant rows.  The 18\" rod sticks into the ground and still stands tall enough for all to see.  You will want to get those tomatoes planted early as this beauty will get your mouth watering for the real thing.\r\nPrice reduced ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax Included",
  "terms": "4 available\r\nWhile supplies last",
  "value": "54.4",
  "price": "37.8",
  "limit": 4,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841293962,
  "created_at": 1457669791539,
  "locations": [
    {
      "_id": "56e2469f1aa72090ef01144f",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e2469f1aa72090ef011451",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e2469f1aa72090ef01144e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e2469f1aa72000060001dc",
    "attached_width": 2203,
    "attached_height": 3259,
    "attached_size": 2362965,
    "attached_name": "tomatoes in pot.JPG",
    "main": false,
    "updated_at": 1457669791539,
    "created_at": 1457669791539
  },
  "activated_at": 1457669795371
}
deals << {
  "_id": "56e247e01aa720f834011481",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Carrots 23.5\" x 12.5\" ",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Carrots are good for the eyes and this garden sign will sure catch some eyes.  It is bright orange with a 14.5\" rod so there will be no doubt about where the carrots are.  Watch out for rabbits. \r\nPrice reduction ($10.00 VALUE\r\nreduced shipping ($6.00 VALUE\r\nTax Included",
  "terms": "10 available\r\nwhile supplies last\r\n",
  "value": "54.4",
  "price": "37.8",
  "limit": 10,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841292611,
  "created_at": 1457670112948,
  "locations": [
    {
      "_id": "56e247e01aa720f834011480",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e247e01aa720f834011482",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e247e01aa720f83401147f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e247e01aa72000060001f1",
    "attached_width": 2156,
    "attached_height": 3178,
    "attached_size": 2293306,
    "attached_name": "carrots in pot.JPG",
    "main": false,
    "updated_at": 1457670112948,
    "created_at": 1457670112948
  },
  "activated_at": 1457670117654
}
deals << {
  "_id": "56e248cf1aa720de520114ad",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Green Beans  23.5\" x 15\"",
  "category_id": "4f7d216074dbb02de3000741",
  "offer": "Green Beans.  No more wondering where you planted the green beans.  And when you send some city folk to pick the green beans, they will know which row to pick.\r\nPrice reduction ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax included",
  "terms": "7 available\r\nwhile supplies last",
  "value": "54.4",
  "price": "37.8",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841291397,
  "created_at": 1457670351860,
  "locations": [
    {
      "_id": "56e248cf1aa720de520114ac",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e248cf1aa720de520114ae",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e249571aa720e5fe0114d2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e249571aa72000060001fb",
    "attached_width": 3165,
    "attached_height": 2844,
    "attached_size": 2681478,
    "attached_name": "green beans in pot close.JPG",
    "main": false,
    "updated_at": 1457670487854,
    "created_at": 1457670487854
  },
  "activated_at": 1457670499732,
  "referral_code": ""
}
deals << {
  "_id": "56e24d2e1aa720ef9d01155e",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom 27\"x12.5\" Red",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Want to add a little color and a little encouragement to your garden.  These blooms will do both.  The bright colors will really stand out and beautify your garden even when things are waiting to blossom.\r\nReduced Price ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax Included",
  "terms": "7 available\r\nwhile supply lasts",
  "value": "54.4",
  "price": "37.8",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841289842,
  "created_at": 1457671470422,
  "locations": [
    {
      "_id": "56e24d2e1aa720ef9d01155d",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e24d2e1aa720ef9d01155f",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1457671475492,
  "image": {
    "_id": "56e73a711aa7202a8c01eeea",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e73a711aa7200006000336",
    "attached_width": 557,
    "attached_height": 533,
    "attached_size": 88220,
    "attached_name": "bloomredflowerstulips1.JPG",
    "main": false,
    "updated_at": 1457994353832,
    "created_at": 1457994353832
  }
}
deals << {
  "_id": "56e24df41aa720e9c8011589",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom 27\"x12.5\" Yellow",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Want to add a little color and a little encouragement to your garden.  These blooms will do both.  The bright colors will really stand out and beautify your garden even when things are waiting to blossom.\r\nReduced Price ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax Included",
  "terms": "7 available\r\nwhile supplies last",
  "value": "54.4",
  "price": "37.8",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841317391,
  "created_at": 1457671668992,
  "locations": [
    {
      "_id": "56e24df41aa720e9c8011588",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e24df41aa720e9c801158a",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1457671674422,
  "image": {
    "_id": "56e739f21aa720f25d01eed9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e739f21aa7200006000332",
    "attached_width": 566,
    "attached_height": 562,
    "attached_size": 94454,
    "attached_name": "bloom yellow2.JPG",
    "main": false,
    "updated_at": 1457994226948,
    "created_at": 1457994226948
  }
}
deals << {
  "_id": "56e24e8a1aa7202c52011595",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom 27\"x12.5\" Orange",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Want to add a little color and a little encouragement to your garden.  These blooms will do both.  The bright colors will really stand out and beautify your garden even when things are waiting to blossom.\r\nReduced Price ($10.00 VALUE\r\nReduced shipping ($6.00 VALUE\r\nTax Included",
  "terms": "7 available\r\nwhile supplies last",
  "value": "54.40",
  "price": "37.80",
  "limit": 7,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841320967,
  "created_at": 1457671818056,
  "locations": [
    {
      "_id": "56e24e891aa7202c52011594",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e24e8a1aa7202c52011596",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e24e891aa7202c52011593",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e24e891aa720000600022f",
    "attached_width": 1936,
    "attached_height": 1936,
    "attached_size": 1748326,
    "attached_name": "bloom orange flowers tulips1.JPG",
    "main": false,
    "updated_at": 1457671818055,
    "created_at": 1457671818055
  },
  "activated_at": 1457671825557
}
deals << {
  "_id": "56e250181aa72067e20115d0",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bell Peppers 23\" x 14.5\"",
  "category_id": "4f7d216074dbb02de3000746",
  "offer": "Green Peppers are a real favorite in the garden and this garden marker will enhance the beauty of the real fruit.  The Green Peppers are on a 14.5\" rod so it will stand tall among the plants.\r\nPrice reduced ($10.00 VALUE\r\nReduced shipping (6.00 VALUE\r\nTax Included",
  "terms": "1 available\r\nwhile supplies last\r\nAlternate color options Red (5/Yellow(5/Orange(5\r\nMUST EMAIL seller regarding alternate colors.  subject to availability",
  "value": "54.4",
  "price": "37.8",
  "limit": 1,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841282333,
  "created_at": 1457672216949,
  "locations": [
    {
      "_id": "56e250181aa72067e20115cf",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e250181aa72067e20115d1",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1457672221677,
  "image": {
    "_id": "56e73a3f1aa72040fe01eee3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e73a3f1aa7200006000334",
    "attached_width": 570,
    "attached_height": 561,
    "attached_size": 97213,
    "attached_name": "peppers-latticeclose.JPG",
    "main": false,
    "updated_at": 1457994304042,
    "created_at": 1457994304042
  }
}
deals << {
  "_id": "56e306671aa7202fac012810",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "1/2 Big House Sandwich, Fries or Ceasar Salad, and Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": "ONLY $11 for a 1/2 Big House Sandwich, Fries or Ceasar Salad, and Small Soda (Normally $13.75",
  "terms": "Cannot be combined with any other offer, coupons or deals. Deal expires 12/31/16.  $11 cash value always valid",
  "value": "13.75",
  "price": "11.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1514707200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1458755387279,
  "created_at": 1457718887032,
  "locations": [
    {
      "_id": "56e306671aa7202fac01280f",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "56e306671aa7202fac012811",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e306671aa7202fac01280e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e306671aa7200006000249",
    "attached_width": 290,
    "attached_height": 128,
    "attached_size": 42403,
    "attached_name": "fedorabighouse.jpg",
    "main": false,
    "updated_at": 1457718887032,
    "created_at": 1457718887032
  },
  "activated_at": 1457718915072,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56e318221aa7207db40129e6",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "533f072bf02cbec521000030",
  "title": "Columbia Glasses w Pouch, Case & Lanyard",
  "category_id": "4f7d215874dbb02de30003c5",
  "offer": "Columbia Safety / Sunglasses w Pouch, Case & Lanyard (Shipping Included Only $29.20 normally $38.93",
  "terms": "- Shipping Included",
  "value": "38.93",
  "price": "29.2",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "533f0ad6f02cbed7b000005e"
  ],
  "expires_at": "",
  "user_id": "533f072bf02cbec521000031",
  "updated_at": 1457724034147,
  "created_at": 1457723426332,
  "locations": [
    {
      "_id": "56e318221aa7207db40129e5",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -119.2780771,
        47.1301417
      ],
      "address": {
        "_id": "56e318221aa7207db40129e7",
        "country": "US",
        "street": "PO Box 1958",
        "suite": "",
        "city": "Moses Lake",
        "region": "WA",
        "postal_code": "98837"
      }
    }
  ],
  "activated_at": 1457723442645,
  "image": {
    "_id": "56e31e9a1aa7200bbf012ae0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e31e9a1aa7200006000290",
    "attached_width": 621,
    "attached_height": 643,
    "attached_size": 39985,
    "attached_name": "BCColumbiacamey2.JPG",
    "main": false,
    "updated_at": 1457725082479,
    "created_at": 1457725082479
  }
}
deals << {
  "_id": "56e31ff61aa7201ec0012b37",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "533f072bf02cbec521000030",
  "title": "Yakima Glasses w Pouch, Case & Lanyard",
  "category_id": "4f7d215874dbb02de30003c5",
  "offer": "Yakima Glasses w Pouch, Case & Lanyard (Shipping Included | ONLY $29.20 Normally $38.93",
  "terms": "- Shipping Included",
  "value": "38.93",
  "price": "29.2",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "533f0ad6f02cbed7b000005e"
  ],
  "expires_at": "",
  "user_id": "533f072bf02cbec521000031",
  "updated_at": 1457725700971,
  "created_at": 1457725430944,
  "locations": [
    {
      "_id": "56e31ff61aa7201ec0012b36",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -119.2780771,
        47.1301417
      ],
      "address": {
        "_id": "56e31ff61aa7201ec0012b38",
        "country": "US",
        "street": "PO Box 1958",
        "suite": "",
        "city": "Moses Lake",
        "region": "WA",
        "postal_code": "98837"
      }
    }
  ],
  "image": {
    "_id": "56e321001aa7203f8e012b64",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e321001aa720000600029e",
    "attached_width": 472,
    "attached_height": 556,
    "attached_size": 33524,
    "attached_name": "sss.JPG",
    "main": false,
    "updated_at": 1457725696639,
    "created_at": 1457725696639
  },
  "activated_at": 1457725700960
}
deals << {
  "_id": "56e3217b1aa72098b7012b89",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "533f072bf02cbec521000030",
  "title": "Yakima (Dark Glasses w Pouch, Case & Lanyard",
  "category_id": "4f7d215874dbb02de30003c5",
  "offer": "Yakima (Dark Safety / Sunglasses w Pouch, Case & Lanyard (Shipping Included Only $29.20 normally $38.93",
  "terms": "> Shipping Included",
  "value": "38.93",
  "price": "29.2",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "533f0ad6f02cbed7b000005e"
  ],
  "expires_at": "",
  "user_id": "533f072bf02cbec521000031",
  "updated_at": 1457725822390,
  "created_at": 1457725819350,
  "locations": [
    {
      "_id": "56e3217b1aa72098b7012b88",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -119.2780771,
        47.1301417
      ],
      "address": {
        "_id": "56e3217b1aa72098b7012b8a",
        "country": "US",
        "street": "PO Box 1958",
        "suite": "",
        "city": "Moses Lake",
        "region": "WA",
        "postal_code": "98837"
      }
    }
  ],
  "image": {
    "_id": "56e3217b1aa72098b7012b87",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e3217b1aa72000060002a2",
    "attached_width": 386,
    "attached_height": 401,
    "attached_size": 24011,
    "attached_name": "Yakimasm.JPG",
    "main": false,
    "updated_at": 1457725819349,
    "created_at": 1457725819349
  },
  "activated_at": 1457725822381
}
deals << {
  "_id": "56e321e01aa7202388012ba0",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "533f072bf02cbec521000030",
  "title": "Pink Columbia Glasses w Pouch, Case & Lanyard",
  "category_id": "4f7d215874dbb02de30003c5",
  "offer": "Pink Columbia Safety / Sunglasses w Pouch, Case & Lanyard (Shipping Included Only $25.45 normally $33.93",
  "terms": "> Shipping Included",
  "value": "33.93",
  "price": "25.45",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "533f0ad6f02cbed7b000005e"
  ],
  "expires_at": "",
  "user_id": "533f072bf02cbec521000031",
  "updated_at": 1457726054057,
  "created_at": 1457725920951,
  "locations": [
    {
      "_id": "56e321e01aa7202388012b9f",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -119.2780771,
        47.1301417
      ],
      "address": {
        "_id": "56e321e01aa7202388012ba1",
        "country": "US",
        "street": "PO Box 1958",
        "suite": "",
        "city": "Moses Lake",
        "region": "WA",
        "postal_code": "98837"
      }
    }
  ],
  "image": {
    "_id": "56e3225f1aa7203a91012bc3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e3225f1aa72000060002aa",
    "attached_width": 541,
    "attached_height": 600,
    "attached_size": 35311,
    "attached_name": "Columbia.JPG",
    "main": false,
    "updated_at": 1457726047977,
    "created_at": 1457726047977
  },
  "activated_at": 1457726054047
}
deals << {
  "_id": "56e323121aa7200729012bd3",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "533f072bf02cbec521000030",
  "title": "Sammamish Glasses w Pouch, Case & Lanyard",
  "category_id": "4f7d215874dbb02de30003c5",
  "offer": "Sammamish Safety / Sunglasses w Pouch, Case & Lanyard (Shipping Included Only $25.45 normally $33.93",
  "terms": "> Shipping Included",
  "value": "33.93",
  "price": "25.45",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "533f0ad6f02cbed7b000005e"
  ],
  "expires_at": "",
  "user_id": "533f072bf02cbec521000031",
  "updated_at": 1457726229215,
  "created_at": 1457726226004,
  "locations": [
    {
      "_id": "56e323111aa7200729012bd2",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -119.2780771,
        47.1301417
      ],
      "address": {
        "_id": "56e323121aa7200729012bd4",
        "country": "US",
        "street": "PO Box 1958",
        "suite": "",
        "city": "Moses Lake",
        "region": "WA",
        "postal_code": "98837"
      }
    }
  ],
  "image": {
    "_id": "56e323111aa7200729012bd1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e323111aa72000060002ae",
    "attached_width": 547,
    "attached_height": 593,
    "attached_size": 35738,
    "attached_name": "Sammamish deal2.JPG",
    "main": false,
    "updated_at": 1457726226004,
    "created_at": 1457726226004
  },
  "activated_at": 1457726229206
}
deals << {
  "_id": "56e324971aa720119a012c29",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "533f072bf02cbec521000030",
  "title": "Yellow Palouse Glasses w Pouch, Case & Lanyard",
  "category_id": "4f7d216674dbb02de30009bc",
  "offer": "Yellow Palouse Glasses w Pouch, Case & Lanyard (Shipping Included Only $25.45 normally $33.93\r\n",
  "terms": "> Shipping Included",
  "value": "33.93",
  "price": "25.45",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "533f0ad6f02cbed7b000005e"
  ],
  "expires_at": "",
  "user_id": "533f072bf02cbec521000031",
  "updated_at": 1457726618471,
  "created_at": 1457726615459,
  "locations": [
    {
      "_id": "56e324971aa720119a012c28",
      "geocode_source": "",
      "geocoded_city_level": false,
      "point": [
        -119.2780771,
        47.1301417
      ],
      "address": {
        "_id": "56e324971aa720119a012c2a",
        "country": "US",
        "street": "PO Box 1958",
        "suite": "",
        "city": "Moses Lake",
        "region": "WA",
        "postal_code": "98837"
      }
    }
  ],
  "image": {
    "_id": "56e324971aa720119a012c27",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e324971aa72000060002b2",
    "attached_width": 553,
    "attached_height": 597,
    "attached_size": 33633,
    "attached_name": "YellowPalouse.JPG",
    "main": false,
    "updated_at": 1457726615459,
    "created_at": 1457726615459
  },
  "activated_at": 1457726618462
}
deals << {
  "_id": "56e8600f1aa720986b001226",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Absolute",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Absolute. A word that describes everything. Literally. This ring speaks to the decision maker in us. It is simple, clean, and dominant. It sends a message to all those with whom you will come into contact and lets them know that you mean business. It serves as a reminder to not simply do, but to do well all those things you take upon yourself. No matter what drives you, let this ring help you achieve your absolute reach, and accomplish your goals absolutely. Leave nothing to chance, make your own destiny.\r\n\r\nTungsten and Black Ceramic",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "375.0",
  "price": "262.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458069552824,
  "created_at": 1458069519155,
  "locations": [
    {
      "_id": "56e8600f1aa720986b001225",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e8600f1aa720986b001227",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e8602e1aa7207c2d00122b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8602e1aa7200007000003",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 99637,
    "attached_name": "Absolute.jpg",
    "main": false,
    "updated_at": 1458069550670,
    "created_at": 1458069550670
  }
}
deals << {
  "_id": "56e860e71aa720245900124d",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Ashton Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Ashton. This ring is for the the visionary who finds joy in analyzing the world around them. By understanding the world in the quiet of your office, home, or study, you find it easier to see what is happening around you and plan accordingly. This analytical thinking helps you make your next move strategically and precisely to achieve success. The “Ashton” serves as a testament and reminder to your perceptive mind and ability to discern the future. Let nothing stop you.\r\n\r\nStainless Steel with Ceramic Inlay",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "140.0",
  "price": "98.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190087499,
  "created_at": 1458069735542,
  "locations": [
    {
      "_id": "56e860e71aa720245900124c",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e860e71aa720245900124e",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e860e71aa720245900124b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e860e71aa7200007000007",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 96001,
    "attached_name": "Ashton.jpg",
    "main": false,
    "updated_at": 1458069735541,
    "created_at": 1458069735541
  },
  "activated_at": 1458070978930
}
deals << {
  "_id": "56e861341aa72020aa001277",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's AXIS | 3",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Axis. The ability to pivot. This ring is perfect for the man who needs to shift his plans to accomplish more. You know the meaning of “The best laid plans of mice and men.” It is more than just flexibility, it is a commitment to do more. The Axis | 3 is perfect for those of us who need the flexibility of rolling up our sleeves for some manual labor, or put on a sports coat for a night out with friends. Pivot, and accomplish everything you need to.\r\n\r\nTungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "100",
  "price": "70",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458069812386,
  "created_at": 1458069812386,
  "locations": [
    {
      "_id": "56e861341aa72020aa001276",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e861341aa72020aa001278",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e861341aa72020aa001275",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e861341aa720000700000b",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 67339,
    "attached_name": "Axis-3.jpg",
    "main": false,
    "updated_at": 1458069812386,
    "created_at": 1458069812386
  }
}
deals << {
  "_id": "56e861851aa720d6f8001286",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Axis | 5",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Shift. You are the one they call on when things get tough and things have to get done. You have the ability to organize and accomplish more than others. The Axis | 5 is the best of both worlds. Sleek and modern but manly in its design, this Men’s ring speaks not only to your ability to express yourself in a clean cut business setting, or at the bar after. You can shift, and that makes you an asset. Shift now, and do the things you know you need to.\r\n\r\nTungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "210",
  "price": "147",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458069893490,
  "created_at": 1458069893490,
  "locations": [
    {
      "_id": "56e861851aa720d6f8001285",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e861851aa720d6f8001287",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e861851aa720d6f8001284",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e861851aa720000700000f",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 72278,
    "attached_name": "Axis-5.jpg",
    "main": false,
    "updated_at": 1458069893490,
    "created_at": 1458069893490
  }
}
deals << {
  "_id": "56e861e21aa720a0b0001291",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Belvedere Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "“A structure designed to command a view”. The name describes this men’s ring. The ability to draw in the attention of those around you, simply by being present is a gift. This wring is designed with these men in mind. You do not seek for this attention, it comes naturally. With this power, you have the opportunity to make your world a better place and shape the opinions of others. Take charge of your fate and command the attention of your network with the Belvedere.\r\n\r\nTungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "300",
  "price": "210",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458069986048,
  "created_at": 1458069986048,
  "locations": [
    {
      "_id": "56e861e11aa720a0b0001290",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e861e21aa720a0b0001292",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e861e11aa720a0b000128f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e861e21aa7200007000013",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 94456,
    "attached_name": "Belvedere.jpg",
    "main": false,
    "updated_at": 1458069986048,
    "created_at": 1458069986048
  }
}
deals << {
  "_id": "56e8622c1aa7205f3a0012a0",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Blazer Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Do you shine brightly amidst the crowd of people in your life? Are you the one that people turn to when they need help? The Blazer was made to suit you and your unique personality. Though classic in style, this men’s ring is paired well with the bright star in a network. You do not need the glory or attention, it just comes. You do not need flashy clothes or accessories to command attention, that comes from within and you get that. Shine bright my friend, and lift those around you.\r\n\r\nTungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "375",
  "price": "262.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070060781,
  "created_at": 1458070060781,
  "locations": [
    {
      "_id": "56e8622c1aa7205f3a00129f",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e8622c1aa7205f3a0012a1",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e8622c1aa7205f3a00129e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8622c1aa7200007000017",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 92389,
    "attached_name": "Blazer.jpg",
    "main": false,
    "updated_at": 1458070060780,
    "created_at": 1458070060780
  }
}
deals << {
  "_id": "56e862761aa7202a2f0012c7",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Carbon Black Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Carbon. The seed of life. Though this element is the most common in our world, it is responsible for creating one of the most soft after and valuable resources on the planet: Diamonds. We all know diamonds are formed when carbon is placed under extreme amounts of pressure. The Carbon | Black men’s ring speaks to those men who are rough around the edges, but priceless to those around them. Live life on your terms; be priceless.\r\n\r\nTungsten with Black Carbon Fiber Inlay",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "240",
  "price": "168",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070134656,
  "created_at": 1458070134656,
  "locations": [
    {
      "_id": "56e862761aa7202a2f0012c6",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e862761aa7202a2f0012c8",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e862761aa7202a2f0012c5",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e862761aa720000700001b",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 102562,
    "attached_name": "Carbon-Black.jpg",
    "main": false,
    "updated_at": 1458070134656,
    "created_at": 1458070134656
  }
}
deals << {
  "_id": "56e862bb1aa7202c800012d2",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Carbon Blue Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Power. This is not something to take lightly, and not something given freely. You do not seek power, but those around you entrust you with it. Be it power over economic and financial matters, justice, or education you are known within your spheres of influence as one they can trust. This men’s ring is powerful but understated. It serves as a perfect reminder of the sacred trust those around you place on your opinion and guidance. Lead well, you were meant to.\r\n\r\nTungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "240",
  "price": "168",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070203913,
  "created_at": 1458070203913,
  "locations": [
    {
      "_id": "56e862bb1aa7202c800012d1",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e862bb1aa7202c800012d3",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e862bb1aa7202c800012d0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e862bb1aa720000700001f",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 112455,
    "attached_name": "Carbon-Blue.jpg",
    "main": false,
    "updated_at": 1458070203913,
    "created_at": 1458070203913
  }
}
deals << {
  "_id": "56e8630d1aa7205b800012f1",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Chancelor Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Power. This is not something to take lightly, and not something given freely. You do not seek power, but those around you entrust you with it. Be it power over economic and financial matters, justice, or education you are known within your spheres of influence as one they can trust. This men’s ring is powerful but understated. It serves as a perfect reminder of the sacred trust those around you place on your opinion and guidance. Lead well, you were meant to.\r\n\r\nTungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "250",
  "price": "175",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070285260,
  "created_at": 1458070285260,
  "locations": [
    {
      "_id": "56e8630d1aa7205b800012f0",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e8630d1aa7205b800012f2",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e8630d1aa7205b800012ef",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8630d1aa7200007000023",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 96908,
    "attached_name": "Chancelor.jpg",
    "main": false,
    "updated_at": 1458070285260,
    "created_at": 1458070285260
  }
}
deals << {
  "_id": "56e8635b1aa72076d2001304",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Descent Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Lineage. Where you come from can speak a lot about you, but that does not mean you are forced to relive the lives of your ancestors; but you can learn from them. Your descent can carry with it a wealth of experience from which you can learn. This men’s ring is designed for this purpose. It speaks of where you are going, by relying on where you have been. Live your life, and remember.\r\n\r\nPolished Tungsten with Black Tungsten Inlay",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "54.0",
  "price": "37.8",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190105691,
  "created_at": 1458070363912,
  "locations": [
    {
      "_id": "56e8635b1aa72076d2001303",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e8635b1aa72076d2001305",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e8635b1aa72076d2001302",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8635b1aa7200007000027",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 106129,
    "attached_name": "Descent.jpg",
    "main": false,
    "updated_at": 1458070363912,
    "created_at": 1458070363912
  },
  "activated_at": 1458071051186
}
deals << {
  "_id": "56e863a71aa720dd4200132b",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's District Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Stand apart. You are not like the others, though you are lauded as a friend by many outside of your tight-knit circle. You are the unique individual who finds his self-expression on the outskirts of society. You were bred for something more and you feel that. This ring is designed with you in mind. Make your mark by showing both the light and dark side of your personality. Stand alone, be your own District.\r\n\r\nBlack Tungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "240",
  "price": "168",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070439879,
  "created_at": 1458070439879,
  "locations": [
    {
      "_id": "56e863a71aa720dd4200132a",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e863a71aa720dd4200132c",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e863a71aa720dd42001329",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e863a71aa720000700002b",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 88733,
    "attached_name": "District.jpg",
    "main": false,
    "updated_at": 1458070439879,
    "created_at": 1458070439879
  }
}
deals << {
  "_id": "56e863f01aa720b7c700133b",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Dynasty 3 Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Dominance. It is not hard to associate that phrase with a dynasty. While there are many definitions of dominance, you subscribe to one: personal success. You do not succeed for the attention of others; you succeed for your own good. The Dynasty 3 is perfect for you. It calls to the classic authority of a men’s ring, and serves as a reminder to the promises you have made your self. Dominate and be successful.\r\n\r\nPolished Tungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "100.0",
  "price": "70.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190092087,
  "created_at": 1458070512568,
  "locations": [
    {
      "_id": "56e863f01aa720b7c700133a",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e863f01aa720b7c700133c",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e863f01aa720b7c7001339",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e863f01aa720000700002f",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 67237,
    "attached_name": "Dynasty-3.jpg",
    "main": false,
    "updated_at": 1458070512568,
    "created_at": 1458070512568
  },
  "activated_at": 1458071040449
}
deals << {
  "_id": "56e8644e1aa7208c2b00134c",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Dynasty 5 Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Legacy. You strive to leave one. The reasons you strive for success are not to impress others, but you are not ashamed of what you have accomplished. No matter what field you are involved in, success is important to you. You take pride in your work and enjoy the fruits of your letters. The Dynasty 5 Men’s ring is perfect for you. Live your life and leave your legacy.\r\n\r\nPolished Tungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "140.0",
  "price": "98.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190096969,
  "created_at": 1458070606726,
  "locations": [
    {
      "_id": "56e8644e1aa7208c2b00134b",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e8644e1aa7208c2b00134d",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e8644e1aa7208c2b00134a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8644e1aa7200007000033",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 72813,
    "attached_name": "Dynasty-5.jpg",
    "main": false,
    "updated_at": 1458070606725,
    "created_at": 1458070606725
  },
  "activated_at": 1458071046268
}
deals << {
  "_id": "56e864d41aa7200e2600136f",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Dynasty 8 Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Dynasty. The essence of pure and utter dominance. Success comes easy to you and achieving greatness is your natural progression. You do not need to gloat about this; it is simply a part of your daily life. This men’s ring allows you to show that. It is powerful, and dominant just as you are. Let there be no mistake in who you are and what you will achieve. Start your Dynasty, and let everyone know it.\r\n\r\nPolished Tungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "210",
  "price": "147",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070740069,
  "created_at": 1458070740069,
  "locations": [
    {
      "_id": "56e864d41aa7200e2600136e",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e864d41aa7200e26001370",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e864d41aa7200e2600136d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e864d41aa7200007000037",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 86662,
    "attached_name": "Dynasty-8.jpg",
    "main": false,
    "updated_at": 1458070740069,
    "created_at": 1458070740069
  }
}
deals << {
  "_id": "56e865171aa720ab0b001384",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Elite Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Better. Whether you are thinking of the political term, or the exact definition, the Elite are the best. Be it athletics, business, education, or recreation, you excel. Excellence is important to you and all that you attempt to do. This men’s ring serves as a perfect reminder of your commitment to achieve more. Achieve more. Be better. Be Elite.\r\n\r\nBlack Tungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "300",
  "price": "210",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070807325,
  "created_at": 1458070807325,
  "locations": [
    {
      "_id": "56e865171aa720ab0b001383",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e865171aa720ab0b001385",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e865171aa720ab0b001382",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e865171aa720000700003b",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 94156,
    "attached_name": "Elite.jpg",
    "main": false,
    "updated_at": 1458070807325,
    "created_at": 1458070807325
  }
}
deals << {
  "_id": "56e8655d1aa72078e2001395",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Finite Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Distinct. Though the meaning can refer to something with limitations, it is what one does with limitations that makes them great. This men’s ring is a perfect piece for those who want to remember how much they can achieve with ingenuity and fight. This is ring is for the adaptable, those who strive for more with less, and the men who want to achieve more in this life. Be more than finite. Be distinct. Be you.\r\n\r\nBlack Tungsten",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "225",
  "price": "157.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1458070877600,
  "created_at": 1458070877600,
  "locations": [
    {
      "_id": "56e8655d1aa72078e2001394",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e8655d1aa72078e2001396",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e8655d1aa72078e2001393",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8655d1aa720000700003f",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 93495,
    "attached_name": "Finite.jpg",
    "main": false,
    "updated_at": 1458070877600,
    "created_at": 1458070877600
  }
}
deals << {
  "_id": "56e865a21aa7208ed90013a3",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "Men's Knight Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "This black ceramic men’s ring has a polished tungsten inlay designed for those who are striving to be a bit better. The ceramic points to durability, while the polished tungsten inlay hints to our need for a perfect core and polished moral guidelines. This black ceramic men’s ring is perfect for those looking for a men’s wedding ring, men’s promise ring, or just a black ceramic ring for men.\r\n\r\nCermaic & Tungsten Carbide",
  "terms": "Available sizes 7-12 but NO 1/2 sizes",
  "value": "140",
  "price": "98",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190082420,
  "created_at": 1458070946166,
  "locations": [
    {
      "_id": "56e865a21aa7208ed90013a2",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56e865a21aa7208ed90013a4",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56e865a21aa7208ed90013a1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e865a21aa7200007000043",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 104856,
    "attached_name": "Knight.jpg",
    "main": false,
    "updated_at": 1458070946166,
    "created_at": 1458070946166
  },
  "activated_at": 1458070951689
}
deals << {
  "_id": "56e8dce71aa7204196001cbb",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Peace buy any 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors- Yellow-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices",
  "value": "56.88",
  "price": "41.16",
  "limit": 1,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841283654,
  "created_at": 1458101479819,
  "locations": [
    {
      "_id": "56e8dce71aa7204196001cba",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8dce71aa7204196001cbc",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8dce71aa7204196001cb9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8dce71aa720000700004b",
    "attached_width": 2220,
    "attached_height": 1659,
    "attached_size": 781087,
    "attached_name": "Peace1.JPG",
    "main": false,
    "updated_at": 1458101479818,
    "created_at": 1458101479818
  },
  "activated_at": 1458101495675,
  "referral_code": ""
}
deals << {
  "_id": "56e8e0591aa7207971001cda",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Relax buy any 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors- Blue-1 | Green-2 | Red-1 | Yellow-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 5,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841285304,
  "created_at": 1458102361184,
  "locations": [
    {
      "_id": "56e8e0591aa7207971001cd9",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e0591aa7207971001cdb",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e0591aa7207971001cd8",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e0591aa7200007000051",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 114818,
    "attached_name": "relax.jpeg",
    "main": false,
    "updated_at": 1458102361183,
    "created_at": 1458102361183
  },
  "activated_at": 1458102379095
}
deals << {
  "_id": "56e8e1681aa720c605001ced",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Sing buy any 3, get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors- Black-3 | Blue-1 | White-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 5,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841251242,
  "created_at": 1458102632743,
  "locations": [
    {
      "_id": "56e8e1681aa720c605001cec",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e1681aa720c605001cee",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e1681aa720c605001ceb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e1681aa7200007000055",
    "attached_width": 723,
    "attached_height": 640,
    "attached_size": 108618,
    "attached_name": "sing.jpeg",
    "main": false,
    "updated_at": 1458102632743,
    "created_at": 1458102632743
  },
  "activated_at": 1458102693067,
  "referral_code": ""
}
deals << {
  "_id": "56e8e22d1aa7208323001d01",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Smile buy any 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors- Black-4 | Blue-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 5,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841249066,
  "created_at": 1458102829415,
  "locations": [
    {
      "_id": "56e8e22d1aa7208323001d00",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e22d1aa7208323001d02",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e22d1aa7208323001cff",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e22d1aa7200007000059",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 119119,
    "attached_name": "smile.jpeg",
    "main": false,
    "updated_at": 1458102829415,
    "created_at": 1458102829415
  },
  "activated_at": 1458102838484,
  "referral_code": ""
}
deals << {
  "_id": "56e8e2a11aa72097c0001d0f",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Sisters",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors- Blue-1 | Pink-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 2,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841246819,
  "created_at": 1458102945253,
  "locations": [
    {
      "_id": "56e8e2a11aa72097c0001d0e",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e2a11aa72097c0001d10",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e2a11aa72097c0001d0d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e2a11aa720000700005d",
    "attached_width": 857,
    "attached_height": 640,
    "attached_size": 111849,
    "attached_name": "sisters.jpeg",
    "main": false,
    "updated_at": 1458102945253,
    "created_at": 1458102945253
  },
  "activated_at": 1458443025796,
  "referral_code": ""
}
deals << {
  "_id": "56e8e3251aa72085b2001d18",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Faith buy any 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors - Yellow-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 1,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841245388,
  "created_at": 1458103077908,
  "locations": [
    {
      "_id": "56e8e3251aa72085b2001d17",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e3251aa72085b2001d19",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e3251aa72085b2001d16",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e3251aa7200007000061",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 110934,
    "attached_name": "faith.jpeg",
    "main": false,
    "updated_at": 1458103077908,
    "created_at": 1458103077908
  },
  "activated_at": 1458103095094,
  "referral_code": ""
}
deals << {
  "_id": "56e8e3b51aa72031ff001d22",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Gentleness buy any 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n\r\n",
  "terms": "Various colors - Yellow-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices",
  "value": "56.88",
  "price": "41.16",
  "limit": 1,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841244220,
  "created_at": 1458103221370,
  "locations": [
    {
      "_id": "56e8e3b51aa72031ff001d21",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e3b51aa72031ff001d23",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e3b51aa72031ff001d20",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e3b51aa7200007000065",
    "attached_width": 970,
    "attached_height": 640,
    "attached_size": 90610,
    "attached_name": "gentleness.jpeg",
    "main": false,
    "updated_at": 1458103221370,
    "created_at": 1458103221370
  },
  "activated_at": 1458103229892,
  "referral_code": ""
}
deals << {
  "_id": "56e8e4311aa720a203001d2c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Jesus buy any 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors - Blue-1 | Green-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 2,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841235219,
  "created_at": 1458103345434,
  "locations": [
    {
      "_id": "56e8e4311aa720a203001d2b",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e4311aa720a203001d2d",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e4311aa720a203001d2a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e4311aa7200007000069",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 131006,
    "attached_name": "Jesus.jpeg",
    "main": false,
    "updated_at": 1458103345434,
    "created_at": 1458103345434
  },
  "activated_at": 1458103354814,
  "referral_code": ""
}
deals << {
  "_id": "56e8e49f1aa7204169001d3a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Joy buy 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors - Black-5 | Pink-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 6,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841271490,
  "created_at": 1458103455407,
  "locations": [
    {
      "_id": "56e8e49f1aa7204169001d39",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e49f1aa7204169001d3b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e49f1aa7204169001d38",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e49f1aa720000700006d",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 100481,
    "attached_name": "joy.jpeg",
    "main": false,
    "updated_at": 1458103455406,
    "created_at": 1458103455406
  },
  "activated_at": 1458103463666,
  "referral_code": ""
}
deals << {
  "_id": "56e8e5421aa720f9ca001d4a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Love buy 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors - Dark Blue-1 | Blue-1 | Orange-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 3,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841239122,
  "created_at": 1458103618473,
  "locations": [
    {
      "_id": "56e8e5421aa720f9ca001d49",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e5421aa720f9ca001d4b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e5421aa720f9ca001d48",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e5421aa7200007000071",
    "attached_width": 913,
    "attached_height": 640,
    "attached_size": 180763,
    "attached_name": "love.jpeg",
    "main": false,
    "updated_at": 1458103618473,
    "created_at": 1458103618473
  },
  "activated_at": 1458103648307,
  "referral_code": ""
}
deals << {
  "_id": "56e8e5e01aa720feb1001d56",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Rejoice",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors - Orange-1 | Yellow-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 2,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841276093,
  "created_at": 1458103776153,
  "locations": [
    {
      "_id": "56e8e5e01aa720feb1001d55",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e5e01aa720feb1001d57",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e5e01aa720feb1001d54",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e5e01aa7200007000075",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 153083,
    "attached_name": "Rejoice.jpeg",
    "main": false,
    "updated_at": 1458103776153,
    "created_at": 1458103776153
  },
  "activated_at": 1459046759453,
  "referral_code": ""
}
deals << {
  "_id": "56e8e7171aa720770c001d89",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d8be50a5a015ed7f00bdf2",
  "title": "Men's Descent Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Lineage. Where you come from can speak a lot about you, but that does not mean you are forced to relive the lives of your ancestors; but you can learn from them. Your descent can carry with it a wealth of experience from which you can learn. This men’s ring is designed for this purpose. It speaks of where you are going, by relying on where you have been. Live your life, and remember.\r\n\r\nPolished Tungsten with Black Tungsten Inlay",
  "terms": "> Available sizes 7-12 but NO 1/2 sizes\r\n> Please email requested size after purchase",
  "value": "54",
  "price": "37.8",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d8fd3c1aa72085ff00015e"
  ],
  "expires_at": "",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "updated_at": 1458104091807,
  "created_at": 1458104087069,
  "locations": [
    {
      "_id": "56e8e7171aa720770c001d88",
      "point": [
        -105.937799,
        35.6869752
      ],
      "address": {
        "_id": "56e8e7171aa720770c001d8a",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Santa Fe",
        "region": "NW",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "56e8e7171aa720770c001d87",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e7171aa720000700007b",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 116654,
    "attached_name": "Descent.jpg",
    "main": false,
    "updated_at": 1458104087069,
    "created_at": 1458104087069
  },
  "activated_at": 1458104091797
}
deals << {
  "_id": "56e8e7651aa72071d8001d99",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "568d59ab0ce94441b3003501",
  "title": "Bloom buy 3 get 1 free",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "Buy any combination of 3 inspirational words and get a 4th free\r\n4th word free ($12.00 VALUE\r\nReduced shipping ($3.00 VALUE\r\nTax included\r\n",
  "terms": "Various colors - Green-1 | Red-1 | Orange-1 | Yellow-1 | Teal-1\r\nNot all colors are available\r\nSubject to availability\r\nMUST EMAIL seller for availability of choices\r\n",
  "value": "56.88",
  "price": "41.16",
  "limit": 5,
  "limit_per_customer": "",
  "listing_ids": [
    "569d31f047ffb05a380020e6"
  ],
  "expires_at": "",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "updated_at": 1469841242121,
  "created_at": 1458104165986,
  "locations": [
    {
      "_id": "56e8e7651aa72071d8001d98",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "56e8e7651aa72071d8001d9a",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "coeur d alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56e8e7651aa72071d8001d97",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e7651aa720000700007d",
    "attached_width": 856,
    "attached_height": 640,
    "attached_size": 116336,
    "attached_name": "Bloom.jpeg",
    "main": false,
    "updated_at": 1458104165985,
    "created_at": 1458104165985
  },
  "activated_at": 1458104367710
}
deals << {
  "_id": "56e8e7881aa72085e1001da2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d8be50a5a015ed7f00bdf2",
  "title": "Men's Dynasty 5 Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Legacy. You strive to leave one. The reasons you strive for success are not to impress others, but you are not ashamed of what you have accomplished. No matter what field you are involved in, success is important to you. You take pride in your work and enjoy the fruits of your letters. The Dynasty 5 Men’s ring is perfect for you. Live your life and leave your legacy.\r\n\r\nPolished Tungsten",
  "terms": "> Available sizes 7-12 but NO 1/2 sizes\r\n> Please email requested ring size after purchase",
  "value": "140",
  "price": "98",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d8fd3c1aa72085ff00015e"
  ],
  "expires_at": "",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "updated_at": 1458104203423,
  "created_at": 1458104200166,
  "locations": [
    {
      "_id": "56e8e7881aa72085e1001da1",
      "point": [
        -105.937799,
        35.6869752
      ],
      "address": {
        "_id": "56e8e7881aa72085e1001da3",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Santa Fe",
        "region": "NW",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "56e8e7881aa72085e1001da0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e7881aa7200007000081",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 72432,
    "attached_name": "Dynasty-5.jpg",
    "main": false,
    "updated_at": 1458104200166,
    "created_at": 1458104200166
  },
  "activated_at": 1458104203415
}
deals << {
  "_id": "56e8e7de1aa720ae81001db1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d8be50a5a015ed7f00bdf2",
  "title": "Men's Dynasty 3 Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Dominance. It is not hard to associate that phrase with a dynasty. While there are many definitions of dominance, you subscribe to one: personal success. You do not succeed for the attention of others; you succeed for your own good. The Dynasty 3 is perfect for you. It calls to the classic authority of a men’s ring, and serves as a reminder to the promises you have made your self. Dominate and be successful.\r\n\r\nPolished Tungsten",
  "terms": "> Available sizes 7-12 but NO 1/2 sizes \r\n> Please email requested ring size after purchase",
  "value": "100",
  "price": "30",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d8fd3c1aa72085ff00015e"
  ],
  "expires_at": "",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "updated_at": 1458104290028,
  "created_at": 1458104286908,
  "locations": [
    {
      "_id": "56e8e7de1aa720ae81001db0",
      "point": [
        -105.937799,
        35.6869752
      ],
      "address": {
        "_id": "56e8e7de1aa720ae81001db2",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Santa Fe",
        "region": "NW",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "56e8e7de1aa720ae81001daf",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e7de1aa7200007000085",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 72432,
    "attached_name": "Dynasty-5.jpg",
    "main": false,
    "updated_at": 1458104286908,
    "created_at": 1458104286908
  },
  "activated_at": 1458104290017
}
deals << {
  "_id": "56e8e8411aa7201821001dc0",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d8be50a5a015ed7f00bdf2",
  "title": "Men's Ashton Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "Ashton. This ring is for the the visionary who finds joy in analyzing the world around them. By understanding the world in the quiet of your office, home, or study, you find it easier to see what is happening around you and plan accordingly. This analytical thinking helps you make your next move strategically and precisely to achieve success. The “Ashton” serves as a testament and reminder to your perceptive mind and ability to discern the future. Let nothing stop you.\r\n\r\nStainless Steel with Ceramic Inlay",
  "terms": "> Available sizes 7-12 but NO 1/2 sizes \r\n> Please email requested ring size after purchase",
  "value": "140",
  "price": "98",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d8fd3c1aa72085ff00015e"
  ],
  "expires_at": "",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "updated_at": 1458104388822,
  "created_at": 1458104385429,
  "locations": [
    {
      "_id": "56e8e8411aa7201821001dbf",
      "point": [
        -105.937799,
        35.6869752
      ],
      "address": {
        "_id": "56e8e8411aa7201821001dc1",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Santa Fe",
        "region": "NW",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "56e8e8411aa7201821001dbe",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e8411aa7200007000089",
    "attached_width": 200,
    "attached_height": 200,
    "attached_size": 36451,
    "attached_name": "Ashton.jpg",
    "main": false,
    "updated_at": 1458104385429,
    "created_at": 1458104385429
  },
  "activated_at": 1458104388811
}
deals << {
  "_id": "56e8e8a81aa7202c21001dd4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d8be50a5a015ed7f00bdf2",
  "title": "Men's Knight Ring",
  "category_id": "4f7d216374dbb02de3000883",
  "offer": "This black ceramic men’s ring has a polished tungsten inlay designed for those who are striving to be a bit better. The ceramic points to durability, while the polished tungsten inlay hints to our need for a perfect core and polished moral guidelines. This black ceramic men’s ring is perfect for those looking for a men’s wedding ring, men’s promise ring, or just a black ceramic ring for men.\r\n\r\nCermaic & Tungsten Carbide",
  "terms": "> Available sizes 7-12 but NO 1/2 sizes \r\n> Please email requested ring size after purchase",
  "value": "140",
  "price": "98",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d8fd3c1aa72085ff00015e"
  ],
  "expires_at": "",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "updated_at": 1458104491082,
  "created_at": 1458104488010,
  "locations": [
    {
      "_id": "56e8e8a71aa7202c21001dd3",
      "point": [
        -105.937799,
        35.6869752
      ],
      "address": {
        "_id": "56e8e8a81aa7202c21001dd5",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Santa Fe",
        "region": "NW",
        "postal_code": ""
      }
    }
  ],
  "image": {
    "_id": "56e8e8a71aa7202c21001dd2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56e8e8a71aa720000700008d",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 116442,
    "attached_name": "Knight.jpg",
    "main": false,
    "updated_at": 1458104488009,
    "created_at": 1458104488009
  },
  "activated_at": 1458104491072
}
deals << {
  "_id": "56f16ec21aa72049fc014fcd",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "60 Day Supply of Garcinia Cambogia",
  "category_id": "4f7d215f74dbb02de30006d7",
  "offer": "ONLY $120 for a 60 Day Supply of Garcinia Cambogia | Normally $165.00\r\n\r\n> FREE Shipping\r\n> FREE Colon Cleanse\r\n\r\nWhat Makes Garcinia Cambogia Ultra The Right Choice?\r\n\r\nQuite frankly: because we have formulated it to be just that. With over 80% HCA in every dose, you are getting the recommended and ideal amount of natural fat burning goodness recommended by the nations top medical specialists. Odds are you heard about Garcinia Cambogia Ultra form a friend, a neighbor, a TV personality, or have already been using our supplements for some time now. Whatever your situation, you can rest assured that you will be happy with your Garcinia Cambogia Ultra supplement and the incredible results you will see as you embark on your weight loss journey.\r\n\r\nGet started today, you won’t regret it.",
  "terms": "This product is not for use by or sale to persons under the age of 18. This product should be used only as directed on the label. It should not be used if you are pregnant or nursing. Consult with a physician before use if you have a serious medical condition or use prescription medications. A Doctor's advice should be sought before using this and any supplemental dietary product. All trademarks and copyrights are the property of their respective owners and are not affiliated with nor do they endorse Garcinia Cambogia Ultra or Wise Vitamins, LLC. These statements have not been evaluated by the FDA. This product is not intended to diagnose, treat, cure or prevent any disease. Individual weight loss results will vary. By using this site you agree to follow the Privacy Policy and all Terms & Conditions printed on this site. Void Where Prohibited By Law. We are not affiliated with Dr. Mehmet Oz, ZoCo Productions LLC or to ZoCo 1 LLC. ZoCo 1 LLC is the owner of the following trademarks: DR. OZ™, ASK DR. OZ™ and THE DOCTOR OZ SHOW™",
  "value": "165.0",
  "price": "120.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190136856,
  "created_at": 1458663106832,
  "locations": [
    {
      "_id": "56f16ec21aa72049fc014fcc",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56f16ec21aa72049fc014fce",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56f16ec21aa72049fc014fcb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f16ec21aa720000700019b",
    "attached_width": 500,
    "attached_height": 406,
    "attached_size": 101208,
    "attached_name": "Garcinia-Cambogia-Ultra-Three.jpg",
    "main": false,
    "updated_at": 1458663106832,
    "created_at": 1458663106832
  },
  "activated_at": 1458663110907
}
deals << {
  "_id": "56f16f351aa72032fc014fda",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "90 Day Supply of Garcinia Cambogia | Free Shipping | Free Colon Cleanse",
  "category_id": "4f7d215f74dbb02de30006d7",
  "offer": "ONLY $150 for a 90 Day Supply of Garcinia Cambogia (3 for price of 2 | Normally $270.00\r\n\r\n> FREE Shipping\r\n> FREE Colon Cleanse\r\n\r\nWhy Pick Garcinia Cambogia Ultra?\r\n\r\nA shovel can help you dig a hole, everyone knows that, but that doesn’t mean it is the right tool for the job in front of you. If you have to dig a foundation for a house, sure a shovel can help you but a backhoe will do a much better job and do it much faster. Regardless of what level of weight loss you are looking to experience, 5-10 pounds or 100+ pounds, Garcina Cambogia Ultra is perfectly designed to help you leverage your hard work and dedication and make the job a bit easier.\r\n\r\nIf you’re looking to lose weight, Garcinia Cambogia Ultra is the right fit to aid you in your battle with the fat cell. Order yours today and live the life you want to live the way you want to look.",
  "terms": "This product is not for use by or sale to persons under the age of 18. This product should be used only as directed on the label. It should not be used if you are pregnant or nursing. Consult with a physician before use if you have a serious medical condition or use prescription medications. A Doctor's advice should be sought before using this and any supplemental dietary product. All trademarks and copyrights are the property of their respective owners and are not affiliated with nor do they endorse Garcinia Cambogia Ultra or Wise Vitamins, LLC. These statements have not been evaluated by the FDA. This product is not intended to diagnose, treat, cure or prevent any disease. Individual weight loss results will vary. By using this site you agree to follow the Privacy Policy and all Terms & Conditions printed on this site. Void Where Prohibited By Law. We are not affiliated with Dr. Mehmet Oz, ZoCo Productions LLC or to ZoCo 1 LLC. ZoCo 1 LLC is the owner of the following trademarks: DR. OZ™, ASK DR. OZ™ and THE DOCTOR OZ SHOW™",
  "value": "270.0",
  "price": "150.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190111060,
  "created_at": 1458663221654,
  "locations": [
    {
      "_id": "56f16f351aa72032fc014fd9",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56f16f351aa72032fc014fdb",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56f16f351aa72032fc014fd8",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f16f351aa720000700019f",
    "attached_width": 500,
    "attached_height": 500,
    "attached_size": 155221,
    "attached_name": "Garcinia-Cambogia-Ultra-90-Day.jpg",
    "main": false,
    "updated_at": 1458663221654,
    "created_at": 1458663221654
  },
  "activated_at": 1458663225943
}
deals << {
  "_id": "56f1a8d11aa72086c30152a1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55d22627f02cbe45d700b51a",
  "title": "$22 for $30 Worth of Food",
  "category_id": "4f7d215e74dbb02de300063d",
  "offer": " ONLY $22 for $30 Worth of Food\r\n\r\nDueling Irons Restaurant located in Post Falls ID\r\n(208 262-9716\r\nduelingironsres@gmail.com",
  "terms": "> Must redeem in 1 visit.  \r\n> Cannot be combined with any other deals or offers. \r\n> $22 cash value never expires.",
  "value": "30.0",
  "price": "22.0",
  "limit": "",
  "limit_per_customer": 10,
  "listing_ids": [
    "55d226eef02cbeda9c00b52e"
  ],
  "expires_at": 1514707200000,
  "user_id": "55d22627f02cbe45d700b517",
  "updated_at": 1477342859137,
  "created_at": 1458677969710,
  "locations": [
    {
      "_id": "56f1a8d11aa72086c30152a0",
      "point": [
        -116.919092,
        47.711717
      ],
      "address": {
        "_id": "56f1a8d11aa72086c30152a2",
        "country": "US",
        "street": "1780 E. Schneidmiller Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56f1a8d11aa72086c301529f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f1a8d11aa72000070001ab",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 71237,
    "attached_name": "duelingirons$30deal.jpg",
    "main": false,
    "updated_at": 1458677969710,
    "created_at": 1458677969710
  },
  "activated_at": 1458678018322,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56f1aab21aa7201ad30152ce",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "55d22627f02cbe45d700b51a",
  "title": "$18 for $25 Worth of Food",
  "category_id": "4f7d215e74dbb02de300062c",
  "offer": "ONLY $18 for $25 Worth of Food",
  "terms": "> Offer expires December 30th 2016. \r\n> Must be used during regular business hours.\r\n> Must be used in 1 visit. \r\n> $18 Cash value valid after December 30th 2016",
  "value": "25.0",
  "price": "18.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55d226eef02cbeda9c00b52e"
  ],
  "expires_at": 1483084800000,
  "user_id": "55d22627f02cbe45d700b517",
  "updated_at": 1468886973069,
  "created_at": 1458678450397,
  "locations": [
    {
      "_id": "56f1aab21aa7201ad30152cd",
      "point": [
        -116.919092,
        47.711717
      ],
      "address": {
        "_id": "56f1aab21aa7201ad30152cf",
        "country": "US",
        "street": "1780 E. Schneidmiller Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56f1aab21aa7201ad30152cc",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f1aab21aa72000070001af",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 69428,
    "attached_name": "duelingirons$25deal.jpg",
    "main": false,
    "updated_at": 1458678450396,
    "created_at": 1458678450396
  },
  "activated_at": 1458678470714,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56f1ae0f1aa720600d015322",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "55d22627f02cbe45d700b51a",
  "title": " $10 for $15 Worth  of Food",
  "category_id": "4f7d215e74dbb02de300063d",
  "offer": "ONLY $10 for $15 Worth of Food\r\n\r\nDueling Irons Restaurant located in Post Falls ID\r\n(208 262-9716\r\nduelingironsres@gmail.com",
  "terms": "> Offer expires December 30th 2016. \r\n> Must be used during regular business hours.\r\n> Must be used in 1 visit. \r\n> $10 Cash value valid after December 30th 2016",
  "value": "15.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": 10,
  "listing_ids": [
    "55d226eef02cbeda9c00b52e"
  ],
  "expires_at": 1483084800000,
  "user_id": "55d22627f02cbe45d700b517",
  "updated_at": 1477342908762,
  "created_at": 1458679311205,
  "locations": [
    {
      "_id": "56f1ae0f1aa720600d015321",
      "point": [
        -116.919092,
        47.711717
      ],
      "address": {
        "_id": "56f1ae0f1aa720600d015323",
        "country": "US",
        "street": "1780 E. Schneidmiller Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "56f1ae0f1aa720600d015320",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f1ae0f1aa72000070001ce",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 80576,
    "attached_name": "duelingirons$15deal.jpg",
    "main": false,
    "updated_at": 1458679311204,
    "created_at": 1458679311204
  },
  "activated_at": 1458679337507,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56f475271aa7203b9f0006bb",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "56ce28aebd40b1e322019b94",
  "title": "ONLY $19 for $25 Worth of Food",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "ONLY $19 for $25 Worth of Food",
  "terms": "> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Cannot be combined with any other deals or coupons\r\n> Cash value valid after expiration date",
  "value": "25.0",
  "price": "19.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56e0578b1aa72095c900d5e5"
  ],
  "expires_at": 1514620800000,
  "user_id": "56e052861aa72021dd00d579",
  "updated_at": 1486061463527,
  "created_at": 1458861351502,
  "locations": [
    {
      "_id": "56f475271aa7203b9f0006ba",
      "point": [
        -116.7816524,
        47.7002452
      ],
      "address": {
        "_id": "56f475271aa7203b9f0006bc",
        "country": "US",
        "street": "356 E. Appleway Ave.",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56f475271aa7203b9f0006b9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f475271aa7200006000005",
    "attached_width": 528,
    "attached_height": 386,
    "attached_size": 69956,
    "attached_name": "surfshack7.jpg",
    "main": false,
    "updated_at": 1458861351495,
    "created_at": 1458861351495
  },
  "activated_at": 1458861379906,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56f4763a1aa720773f0006c5",
  "active": true,
  "vouchers_count": 3,
  "profile_id": "56ce28aebd40b1e322019b94",
  "title": "$15 for $20 Worth of Food",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "$20 worth of food for $15",
  "terms": "> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Cannot be combined with any other deals or coupons\r\n> Cash value valid after expiration date",
  "value": "20.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56e0578b1aa72095c900d5e5"
  ],
  "expires_at": 1483084800000,
  "user_id": "56e052861aa72021dd00d579",
  "updated_at": 1460143615394,
  "created_at": 1458861626366,
  "locations": [
    {
      "_id": "56f4763a1aa720773f0006c4",
      "point": [
        -116.7816524,
        47.7002452
      ],
      "address": {
        "_id": "56f4763a1aa720773f0006c6",
        "country": "US",
        "street": "356 E. Appleway Ave.",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56f4763a1aa720773f0006c3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f4763a1aa7200006000007",
    "attached_width": 528,
    "attached_height": 567,
    "attached_size": 78518,
    "attached_name": "surfshack4 (2.jpg",
    "main": false,
    "updated_at": 1458861626365,
    "created_at": 1458861626365
  },
  "activated_at": 1458861666722,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56f47725e9330c75b2000619",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "56ce28aebd40b1e322019b94",
  "title": " $15 Worth of Food for $11",
  "category_id": "4f7d216574dbb02de3000979",
  "offer": "$15 worth of food for $11",
  "terms": "> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit.  \r\n> Cannot be combined with any other deals or coupons\r\n> Cash value valid after expiration date",
  "value": "15.0",
  "price": "11.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "56e0578b1aa72095c900d5e5"
  ],
  "expires_at": 1483084800000,
  "user_id": "56e052861aa72021dd00d579",
  "updated_at": 1467925075506,
  "created_at": 1458861861469,
  "locations": [
    {
      "_id": "56f47725e9330c75b2000618",
      "point": [
        -116.7816524,
        47.7002452
      ],
      "address": {
        "_id": "56f47725e9330c75b200061a",
        "country": "US",
        "street": "356 E. Appleway Ave.",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56f47725e9330c75b2000617",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f47725e9330c000700000f",
    "attached_width": 412,
    "attached_height": 382,
    "attached_size": 75143,
    "attached_name": "surfshack6 (2.jpg",
    "main": false,
    "updated_at": 1458861861468,
    "created_at": 1458861861468
  },
  "activated_at": 1458861882903,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56f963101aa7201088004da1",
  "active": true,
  "vouchers_count": 1,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "144 Sunglasses / Safety Glasses w Custom Logo",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Safety & Sunglasses With Custom Logo \r\n> Choose between a few different model safety/sunglasses for your charity, club or business. These glasses come in smoke, clear and amber colors. \r\n> Email to discuss colors",
  "terms": "> For custom Logos please send CMYK artwork in Vector Art or in Illustrator .ill, .ai or .fh or .eps extensions. \r\n> This is the format that your logo is created in and we can reproduce it exactly and is required if it is a registered logo. This will make perfect art work. \r\n> If it has colors please send us the colors using the international Pantone Color Chart.",
  "value": "1440.0",
  "price": "540.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459190160409,
  "created_at": 1459184400318,
  "locations": [
    {
      "_id": "56f963101aa7201088004da0",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56f963101aa7201088004da2",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56f963101aa7201088004d9f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f963101aa7200006000021",
    "attached_width": 703,
    "attached_height": 849,
    "attached_size": 42951,
    "attached_name": "lakecity.jpg",
    "main": false,
    "updated_at": 1459184400317,
    "created_at": 1459184400317
  },
  "activated_at": 1459184403463
}
deals << {
  "_id": "56f9638d1aa7206d80004dab",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "288 Sunglasses / Safety Glasses w Custom Logo",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Safety & Sunglasses With Custom Logo\r\n> Click <a href=\"https://youtu.be/aYVPpWQl3lU\">HERE</a> to watch a testimonial from a coach on how to best sell these glasses.\r\n> Choose between a few different model safety/sunglasses for your charity, club or business. These glasses come in smoke, clear and amber colors. \r\n> Email to discuss colors",
  "terms": "> For custom Logos please send CMYK artwork in Vector Art or in Illustrator .ill, .ai or .fh or .eps extensions. \r\n> This is the format that your logo is created in and we can reproduce it exactly and is required if it is a registered logo. This will make perfect art work. \r\n> If it has colors please send us the colors using the international Pantone Color Chart.",
  "value": "2880.0",
  "price": "936.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1462307686321,
  "created_at": 1459184525649,
  "locations": [
    {
      "_id": "56f9638d1aa7206d80004daa",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56f9638d1aa7206d80004dac",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56f9638d1aa7206d80004da9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f9638d1aa7200006000023",
    "attached_width": 847,
    "attached_height": 610,
    "attached_size": 30357,
    "attached_name": "kgsa.jpg",
    "main": false,
    "updated_at": 1459184525648,
    "created_at": 1459184525648
  },
  "activated_at": 1459184528609
}
deals << {
  "_id": "56f964a01aa720f916004dbe",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "720 Sunglasses / Safety Glasses w Custom Logo",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Safety & Sunglasses With Custom Logo \r\nClick <a href=\"https://youtu.be/aYVPpWQl3lU\">HERE</a> to watch a testimonial from a coach on how to best sell these glasses.\r\n> Choose between a few different model safety/sunglasses for your charity, club or business. These glasses come in smoke, clear and amber colors. \r\n> Email to discuss colors",
  "terms": "> For custom Logos please send CMYK artwork in Vector Art or in Illustrator .ill, .ai or .fh or .eps extensions. \r\n> This is the format that your logo is created in and we can reproduce it exactly and is required if it is a registered logo. This will make perfect art work. \r\n> If it has colors please send us the colors using the international Pantone Color Chart.",
  "value": "7200.0",
  "price": "1980.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1462307704546,
  "created_at": 1459184800427,
  "locations": [
    {
      "_id": "56f964a01aa720f916004dbd",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56f964a01aa720f916004dbf",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56f964a01aa720f916004dbc",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f964a01aa7200006000027",
    "attached_width": 672,
    "attached_height": 612,
    "attached_size": 52141,
    "attached_name": "glass2.JPG",
    "main": false,
    "updated_at": 1459184800427,
    "created_at": 1459184800427
  },
  "activated_at": 1459184802994
}
deals << {
  "_id": "56f965241aa720427b004dc9",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "rachelr83",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "1200 + Sunglasses / Safety Glasses w Custom Logo",
  "category_id": "4f7d216974dbb02de3000ae1",
  "offer": "Safety & Sunglasses With Custom Logo\r\nClick <a href=\"https://youtu.be/aYVPpWQl3lU\">HERE</a> to watch a testimonial from a coach on how to best sell these glasses.\r\n> Choose between a few different model safety/sunglasses for your charity, club or business. These glasses come in smoke, clear and amber colors. \r\n> Email to discuss colors",
  "terms": "> For custom Logos please send CMYK artwork in Vector Art or in Illustrator .ill, .ai or .fh or .eps extensions. \r\n> This is the format that your logo is created in and we can reproduce it exactly and is required if it is a registered logo. This will make perfect art work. \r\n> If it has colors please send us the colors using the international Pantone Color Chart.",
  "value": "12000.0",
  "price": "3000.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1462307725106,
  "created_at": 1459184932445,
  "locations": [
    {
      "_id": "56f965241aa720427b004dc8",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "56f965241aa720427b004dca",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "56f965241aa720427b004dc7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56f965241aa720000600002f",
    "attached_width": 3264,
    "attached_height": 2448,
    "attached_size": 1262873,
    "attached_name": "IMG_7461.JPG",
    "main": false,
    "updated_at": 1459184932445,
    "created_at": 1459184932445
  },
  "activated_at": 1459184940644
}
deals << {
  "_id": "56fb091ce9330ccad8003258",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "One Colonic Session",
  "category_id": "570562e81aa720df6f00ed6e",
  "offer": "ONLY $39 for One Colonic Session | Normally $80\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\n> During each 40-minute colonic, veteran colon hydrotherapists flush out debris with warm triple-purified water.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "80.0",
  "price": "39.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230794247,
  "created_at": 1459292444107,
  "locations": [
    {
      "_id": "56fb091be9330ccad8003257",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb091ce9330ccad8003259",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb091be9330ccad8003256",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb091be9330c0007000058",
    "attached_width": 2716,
    "attached_height": 1810,
    "attached_size": 4141035,
    "attached_name": "bodydetox.jpg",
    "main": false,
    "updated_at": 1459292444107,
    "created_at": 1459292444107
  },
  "activated_at": 1459292448980,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0991e9330c843400325f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "Two Colonic Sessions",
  "category_id": "570562e81aa720df6f00ed6e",
  "offer": "ONLY $71.99 for Two Colonic Sessions | Normally $160.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\n> During each 40-minute colonic, veteran colon hydrotherapists flush out debris with warm triple-purified water.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. \r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided.",
  "value": "160.0",
  "price": "71.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230807255,
  "created_at": 1459292561658,
  "locations": [
    {
      "_id": "56fb0990e9330c843400325e",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0991e9330c8434003260",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0990e9330c843400325d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0990e9330c0007000069",
    "attached_width": 2716,
    "attached_height": 1810,
    "attached_size": 4141035,
    "attached_name": "bodydetox.jpg",
    "main": false,
    "updated_at": 1459292561658,
    "created_at": 1459292561658
  },
  "activated_at": 1459292613440,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0ad2e9330c0528003274",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "1 - 60 Minute Ultrasonic Cavitation Session",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $129 for 1 - 60 Minute Ultrasonic-Cavitation Treatment | Normally $250.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nUltrasonic Cavitation or Ultrasound Liposuction is a NEW, non-surgical fat removal procedure. This revolutionary procedure was designed in Europe. Because there is no surgery and no anesthesia, there is no hospital stay, no time off from work and no recovery time. Clients see immediate results and will continue to see results in the reduction of fat up to a week following the treatment.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "250.0",
  "price": "129.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230845642,
  "created_at": 1459292882015,
  "locations": [
    {
      "_id": "56fb0ad2e9330c0528003273",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0ad2e9330c0528003275",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0ad1e9330c0528003272",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0ad2e9330c000700007c",
    "attached_width": 324,
    "attached_height": 233,
    "attached_size": 16911,
    "attached_name": "ultrasoniccavitationtreatments.jpeg",
    "main": false,
    "updated_at": 1459292882015,
    "created_at": 1459292882015
  },
  "activated_at": 1459292885103,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0b6de9330ca24500327d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "2 - 60 Minute Ultrasonic Cavitation Sessions",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $249 for 2 - 60 Minute Ultrasonic-Cavitation Treatments | Normally $500.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nUltrasonic Cavitation or Ultrasound Liposuction is a NEW, non-surgical fat removal procedure. This revolutionary procedure was designed in Europe. Because there is no surgery and no anesthesia, there is no hospital stay, no time off from work and no recovery time. Clients see immediate results and will continue to see results in the reduction of fat up to a week following the treatment.\r\n\r\n2-5 sessions may be needed for some patients to experience optimal overall results.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "500.0",
  "price": "249.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230853946,
  "created_at": 1459293037963,
  "locations": [
    {
      "_id": "56fb0b6de9330ca24500327c",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0b6de9330ca24500327e",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0b6de9330ca24500327b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0b6de9330c000700007e",
    "attached_width": 324,
    "attached_height": 233,
    "attached_size": 16911,
    "attached_name": "ultrasoniccavitationtreatments.jpeg",
    "main": false,
    "updated_at": 1459293037963,
    "created_at": 1459293037963
  },
  "activated_at": 1459293040309,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0d441aa720e918006768",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "1 Lipotropic Vitamin Injection (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $19 for 1 Lipotropic Vitamin Shots (B6 & B12 | Normally $49.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "49.0",
  "price": "19.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230862234,
  "created_at": 1459293508011,
  "locations": [
    {
      "_id": "56fb0d431aa720e918006767",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0d441aa720e918006769",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0d431aa720e918006766",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0d431aa720000600007d",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1459293508011,
    "created_at": 1459293508011
  },
  "activated_at": 1459293510403,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0dc5e9330c04df00329d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "3 Lipotropic Vitamin Injections (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $65 for 3 Lipotropic Vitamin Injections (B6 & B12 | Normally $147.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA\r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. \r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided.",
  "value": "147.0",
  "price": "65.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230890733,
  "created_at": 1459293637329,
  "locations": [
    {
      "_id": "56fb0dc5e9330c04df00329c",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0dc5e9330c04df00329e",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0dc5e9330c04df00329b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0dc5e9330c0007000084",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1459293637329,
    "created_at": 1459293637329
  },
  "activated_at": 1459293668465,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0ef1e9330c0ce60032a4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "6 Lipotropic Vitamin Injection or 10 Self-Administered Kit (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $109 for 6 Lipotropic Vitamin Injection or 10 Self-Administered Kit | Normally $240.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.\r\n\r\n> With all Self-Administered Vitamin Shots you will receive a FREE Do-It-Yourself detailed instruction guide\r\n> NEW! All take home shots are pre-loaded straight from the pharmacy – smaller needles, pre-loaded, no hassle!",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "240.0",
  "price": "109.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230912637,
  "created_at": 1459293937665,
  "locations": [
    {
      "_id": "56fb0ef1e9330c0ce60032a3",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0ef1e9330c0ce60032a5",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0ef1e9330c0ce60032a2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0ef1e9330c0007000086",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1459293937665,
    "created_at": 1459293937665
  },
  "activated_at": 1459293940516,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fb0f5b1aa720b2d2006791",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "12 Lipotropic Vitamin Injection or 20 Self-Administered Kit (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $179 for 12 Lipotropic Vitamin Injection or 20 Self-Administered Kit (B6 & B12 | Normally $480.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.\r\n\r\n> With all Self-Administered Vitamin Shots you will receive a FREE Do-It-Yourself detailed instruction guide \r\n> NEW! All take home shots are pre-loaded straight from the pharmacy – smaller needles, pre-loaded, no hassle!",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "480.0",
  "price": "179.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230925730,
  "created_at": 1459294043690,
  "locations": [
    {
      "_id": "56fb0f5b1aa720b2d2006790",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "56fb0f5b1aa720b2d2006792",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "56fb0f5b1aa720b2d200678f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fb0f5b1aa7200006000081",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1459294043690,
    "created_at": 1459294043690
  },
  "activated_at": 1459294046065,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fd777d1aa720fba000801b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "1 Custom Photo Mug",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $15 for One Custom Photo Mug | Normally $20.65 ",
  "terms": "> 1 picture per mug.\r\n> Words are included\r\n> One full color or black and white photo onto each white,\r\n> 11-ounce mug.",
  "value": "20.65",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1485231045089,
  "created_at": 1459451773401,
  "locations": [
    {
      "_id": "56fd777d1aa720fba000801a",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "56fd777d1aa720fba000801c",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "activated_at": 1459451785860,
  "image": {
    "_id": "56fd8b3ee9330cc1840048bf",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fd8b3ee9330c00070000b8",
    "attached_width": 477,
    "attached_height": 376,
    "attached_size": 24918,
    "attached_name": "andersoninkmug.JPG",
    "main": false,
    "updated_at": 1459456830230,
    "created_at": 1459456830230
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fd78481aa7200d6900802c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "2 Custom Photo Mugs",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $25 for 2 Custom Photo Mugs | Normally $41.30",
  "terms": "> 1 picture per mug. \r\n> Words are included \r\n> One full color or black and white photo onto each white, \r\n> 11-ounce mug.",
  "value": "41.3",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1485231059976,
  "created_at": 1459451976490,
  "locations": [
    {
      "_id": "56fd78481aa7200d6900802b",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "56fd78481aa7200d6900802d",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "activated_at": 1459451979565,
  "image": {
    "_id": "56fd8b5e1aa72031220080ec",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fd8b5e1aa72000060000a2",
    "attached_width": 477,
    "attached_height": 376,
    "attached_size": 24918,
    "attached_name": "andersoninkmug.JPG",
    "main": false,
    "updated_at": 1459456862857,
    "created_at": 1459456862857
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fd8a6f1aa72064f90080d9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "1 Custom Photo Stainless Steel Travel Mug ",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $18 for 1 Custom Photo Stainless Steel Travel Mug | Normally $25\r\n> Either White or Stainless Color\r\n",
  "terms": "> 1 picture per mug. \r\n> Words are included \r\n> One full color or black and white photo onto each white, ",
  "value": "25.0",
  "price": "18.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1485231076091,
  "created_at": 1459456623977,
  "locations": [
    {
      "_id": "56fd8a6f1aa72064f90080d8",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "56fd8a6f1aa72064f90080da",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "activated_at": 1459456626773,
  "image": {
    "_id": "56fd8b23e9330c05a70048bc",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fd8b23e9330c00070000b6",
    "attached_width": 753,
    "attached_height": 555,
    "attached_size": 55839,
    "attached_name": "andersonmug.JPG",
    "main": false,
    "updated_at": 1459456803407,
    "created_at": 1459456803407
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fd8aeae9330cf9ad0048b7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "2 Custom Photo Stainless Steel Travel Mug",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $30 for 2 Custom Photo Stainless Steel Travel Mug | Normally $50 \r\n> Either White or Stainless Color",
  "terms": "> 1 picture per mug. \r\n> Words are included \r\n> One full color or black and white photo onto each white,",
  "value": "50.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1485231093642,
  "created_at": 1459456746384,
  "locations": [
    {
      "_id": "56fd8aeae9330cf9ad0048b6",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "56fd8aeae9330cf9ad0048b8",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "activated_at": 1459456749966,
  "image": {
    "_id": "56fd8b07e9330cdfcb0048b9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fd8b07e9330c00070000b4",
    "attached_width": 753,
    "attached_height": 555,
    "attached_size": 55839,
    "attached_name": "andersonmug.JPG",
    "main": false,
    "updated_at": 1459456775745,
    "created_at": 1459456775745
  },
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fd93b0e9330c9d6c00490a",
  "active": true,
  "vouchers_count": 5,
  "profile_id": "568af45294cacf5fb6009ba8",
  "title": "$10 for $16 Worth of Food",
  "category_id": "4f7d215e74dbb02de300063d",
  "offer": "ONLY $10 for $16 Worth of Food",
  "terms": "> Must be used during regular business hours. \r\n> Must be used in 1 visit. ",
  "value": "16.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fd932d1aa7209edf008149"
  ],
  "expires_at": "",
  "user_id": "56fd730ee9330c677c0047fd",
  "updated_at": 1462980173066,
  "created_at": 1459458992930,
  "locations": [
    {
      "_id": "56fd93b0e9330c9d6c004909",
      "point": [
        -116.7601353,
        47.6735139
      ],
      "address": {
        "_id": "56fd93b0e9330c9d6c00490b",
        "country": "US",
        "street": "1801 E Sherman Ave",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "56fd93b0e9330c9d6c004908",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fd93b0e9330c00070000ca",
    "attached_width": 594,
    "attached_height": 528,
    "attached_size": 72097,
    "attached_name": "foo.JPG",
    "main": false,
    "updated_at": 1459458992930,
    "created_at": 1459458992930
  },
  "activated_at": 1459458995747,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "56fd93ffe9330ce178004912",
  "active": true,
  "vouchers_count": 7,
  "profile_id": "568af45294cacf5fb6009ba8",
  "title": "$18 for $25 Worth of Food",
  "category_id": "4f7d215e74dbb02de300060a",
  "offer": "ONLY $18 for $25 Worth of Food",
  "terms": "> Must be used during regular business hours. \r\n> Must be used in 1 visit. ",
  "value": "25.0",
  "price": "18.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fd932d1aa7209edf008149"
  ],
  "expires_at": "",
  "user_id": "56fd730ee9330c677c0047fd",
  "updated_at": 1465838648658,
  "created_at": 1459459071027,
  "locations": [
    {
      "_id": "56fd93fee9330ce178004911",
      "point": [
        -116.7601353,
        47.6735139
      ],
      "address": {
        "_id": "56fd93ffe9330ce178004913",
        "country": "US",
        "street": "1801 E Sherman Ave",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1459459073709,
  "referral_code": "currentattractions",
  "image": {
    "_id": "57ad53d759ffa865e800064d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57ad53d759ffa8000600001d",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 65871,
    "attached_name": "jeffery's2.jpg",
    "main": false,
    "updated_at": 1470976983876,
    "created_at": 1470976983876
  }
}
deals << {
  "_id": "56fea678e9330c7a81004c6b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56faf140e9330cce4400310b",
  "title": "$7.50 for $10 Worth of Goodies",
  "category_id": "4f7d215e74dbb02de3000662",
  "offer": "ONLY $7.50 for $10 Worth of Goodies",
  "terms": "> Food Items Only\r\n> No Gift Items\r\n> Tins NOT Included",
  "value": "10.0",
  "price": "7.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faf3d9e9330cf48d003125"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1485231111371,
  "created_at": 1459529336447,
  "locations": [
    {
      "_id": "56fea678e9330c7a81004c6a",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "56fea678e9330c7a81004c6c",
        "country": "US",
        "street": "14700 E Indiana Ave, ",
        "suite": "Space 2001",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "56fea678e9330c7a81004c69",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fea678e9330c00070000da",
    "attached_width": 508,
    "attached_height": 337,
    "attached_size": 36971,
    "attached_name": "double.JPG",
    "main": false,
    "updated_at": 1459529336447,
    "created_at": 1459529336447
  },
  "activated_at": 1459529341047,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "56fea770e9330c8e59004c91",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56faf140e9330cce4400310b",
  "title": "$13.50 for $20 Worth of Goodies",
  "category_id": "4f7d215e74dbb02de3000662",
  "offer": "ONLY $13.50 for $20 Worth of Goodies",
  "terms": "> Food Items Only \r\n> No Gift Items \r\n> Tins NOT Included",
  "value": "20.0",
  "price": "13.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faf3d9e9330cf48d003125"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1485231127345,
  "created_at": 1459529584134,
  "locations": [
    {
      "_id": "56fea770e9330c8e59004c90",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "56fea770e9330c8e59004c92",
        "country": "US",
        "street": "14700 E Indiana Ave, ",
        "suite": "Space 2001",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "56fea770e9330c8e59004c8f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "56fea770e9330c00070000de",
    "attached_width": 515,
    "attached_height": 328,
    "attached_size": 32719,
    "attached_name": "double2.JPG",
    "main": false,
    "updated_at": 1459529584134,
    "created_at": 1459529584134
  },
  "activated_at": 1459529589537,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "5702bbdf1aa720740c00bc3d",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "r",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "1 Rooftop Raffle Ticket",
  "category_id": "4f7d215d74dbb02de30005a3",
  "offer": "Win a NEW 26 square foot rooftop.  Includes tear off of old or destroyed roof, trash being hauled away, and one layer of shingles! $9,000 value with upgraded 30-year shingles! ",
  "terms": "",
  "value": "12",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459796972716,
  "created_at": 1459796959331,
  "locations": [
    
  ],
  "image": {
    "_id": "5702bbdf1aa720740c00bc3b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5702bbdf1aa72000060000c9",
    "attached_width": 640,
    "attached_height": 640,
    "attached_size": 99326,
    "attached_name": "edge4.jpg",
    "main": false,
    "updated_at": 1459796959331,
    "created_at": 1459796959331
  },
  "activated_at": 1459796972706,
  "deleted_at": 1459799038216
}
deals << {
  "_id": "5702c38fe9330cb6b8007b7c",
  "active": false,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "5702b6e6e9330cc9c4007acb",
  "title": "1 Rooftop Raffle Ticket",
  "category_id": "4f7d215d74dbb02de30005a3",
  "offer": "Win a NEW 26 square foot rooftop. Includes tear off of old or destroyed roof, trash being hauled away, and one layer of shingles! $9,000 value with upgraded 30-year shingles!",
  "terms": "",
  "value": "12",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1459861121998,
  "created_at": 1459798927943,
  "locations": [
    {
      "_id": "5702c38fe9330cb6b8007b7b",
      "point": [
        -116.7995523,
        47.6903026
      ],
      "address": {
        "_id": "5702c38fe9330cb6b8007b7d",
        "country": "US",
        "street": "1620 Northwest Blvd",
        "suite": "Suite 102",
        "city": " Coeur d’Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5702c38fe9330cb6b8007b7a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5702c38fe9330c00070000fd",
    "attached_width": 640,
    "attached_height": 640,
    "attached_size": 99326,
    "attached_name": "edge4.jpg",
    "main": false,
    "updated_at": 1459798927943,
    "created_at": 1459798927943
  },
  "activated_at": 1459798958032
}
deals << {
  "_id": "570304421aa720abc900c2a8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56f1ac441aa720e8320152ee",
  "title": "Gutter Cleaning",
  "category_id": "57437bf21aa720629c000a01",
  "offer": "ONLY $200 for Gutters Cleaned | Normally $250\r\n\r\nClear Blue Window Cleaning located in Coeur d'Alene ID\r\n(208 664-4241\r\n\r\n> Debris in removed by hand.\r\n> Hose used to flush out gutters and downspouts.\r\n> Gutters are checked for leaks.\r\n> Brackets are all checked to be sturdy and in place\r\n> Debris is picked up and removed from property\r\n> Outside of all gutters are cleaned.",
  "terms": "> Service is within 20 miles of 99202.\r\n> If more than 30 miles from 99202 there may be an additional $25 charge.\r\n> Additional charge is to be discussed at time of service so company knows distance of drive.\r\n> Call, text or email to schedule an appointment.",
  "value": "250.0",
  "price": "200.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56f1ae6c1aa7206a6b015337"
  ],
  "expires_at": 1485936000000,
  "user_id": "5702f6451aa720287e00c182",
  "updated_at": 1485230623980,
  "created_at": 1459815490879,
  "locations": [
    {
      "_id": "570304421aa720abc900c2a7",
      "point": [
        -117.4316272,
        47.66007159999999
      ],
      "address": {
        "_id": "570304421aa720abc900c2a9",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "570304421aa720abc900c2a6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570304421aa72000060000d3",
    "attached_width": 446,
    "attached_height": 375,
    "attached_size": 39838,
    "attached_name": "gutters.JPG",
    "main": false,
    "updated_at": 1459815490879,
    "created_at": 1459815490879
  },
  "activated_at": 1459815493694,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570304f2e9330c3a5c00812a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56f1ac441aa720e8320152ee",
  "title": "Window Cleaning (Exterior Screens Included",
  "category_id": "57437c0f1aa7204c0d000a0b",
  "offer": "ONLY $200 for Interior & Exterior Window Cleaning | Normally $250\r\n\r\nClear Blue Window Cleaning located in Coeur d'Alene ID\r\n(208 664-4241\r\n\r\n> Interior & Exterior\r\n> Roughly 2000 square foot home\r\n> Roughly 60 window panes\r\n> Exterior screens included",
  "terms": "> Service is within 20 miles of 99202.\r\n> If more than 30 miles from 99202 there may be an additional $25 charge.\r\n> Additional charge is to be discussed at time of service so company knows distance of drive.\r\n> Call, text or email to schedule an appointment.",
  "value": "250.0",
  "price": "200.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56f1ae6c1aa7206a6b015337"
  ],
  "expires_at": 1485936000000,
  "user_id": "5702f6451aa720287e00c182",
  "updated_at": 1485230644307,
  "created_at": 1459815666031,
  "locations": [
    {
      "_id": "570304f2e9330c3a5c008129",
      "point": [
        -117.4316272,
        47.66007159999999
      ],
      "address": {
        "_id": "570304f2e9330c3a5c00812b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "570304f2e9330c3a5c008128",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570304f2e9330c0007000109",
    "attached_width": 464,
    "attached_height": 507,
    "attached_size": 48212,
    "attached_name": "windows.JPG",
    "main": false,
    "updated_at": 1459815666031,
    "created_at": 1459815666031
  },
  "activated_at": 1459815672200,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570305bbe9330cbdd4008137",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56f1ac441aa720e8320152ee",
  "title": "Exterior House Cleaning",
  "category_id": "57437d81e9330c851a000ab4",
  "offer": "ONLY $350 for Exterior House Cleaning | Normally $450\r\n\r\nClear Blue Window Cleaning located in Coeur d'Alene ID\r\n(208 664-4241\r\n\r\n> Exterior of house is pressure washed.\r\n> Outside of all gutters are cleaned.\r\n> Removal of all debris on roof.\r\n> Exterior window screens are cleaned\r\n> Exterior windows are cleaner due to being pressure washed (they're not professionally hand cleaned",
  "terms": "> Service is within 20 miles of 99202.\r\n> If more than 30 miles from 99202 there may be an additional $25 charge.\r\n> Additional charge is to be discussed at time of service so company knows distance of drive.\r\n> Call, text or email to schedule an appointment.",
  "value": "450.0",
  "price": "350.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56f1ae6c1aa7206a6b015337"
  ],
  "expires_at": 1485936000000,
  "user_id": "5702f6451aa720287e00c182",
  "updated_at": 1485230661343,
  "created_at": 1459815867534,
  "locations": [
    {
      "_id": "570305bbe9330cbdd4008136",
      "point": [
        -117.4316272,
        47.66007159999999
      ],
      "address": {
        "_id": "570305bbe9330cbdd4008138",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "570305bbe9330cbdd4008135",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570305bbe9330c000700010b",
    "attached_width": 605,
    "attached_height": 600,
    "attached_size": 58710,
    "attached_name": "pressurewash.JPG",
    "main": false,
    "updated_at": 1459815867534,
    "created_at": 1459815867534
  },
  "activated_at": 1459815871741,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57030696e9330ccbea008141",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56f1ac441aa720e8320152ee",
  "title": "Air Duct Cleaning",
  "category_id": "57437bafe9330c91d9000a3c",
  "offer": "ONLY $69 for Air Duct Cleaning Services | Normally $345\r\n\r\nClear Blue Window Cleaning located in Coeur d'Alene ID\r\n(208 664-4241\r\n\r\n> Includes the cleaning of 6 vents\r\n> A furnace Inspection\r\n> An antibacterial & deodorizer for vent systems",
  "terms": "> Service is within 20 miles of 99202.\r\n> If more than 30 miles from 99202 there may be an additional $25 charge.\r\n> Additional charge is to be discussed at time of service so company knows distance of drive.\r\n> Call, text or email to schedule an appointment.",
  "value": "345.0",
  "price": "69.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56f1ae6c1aa7206a6b015337"
  ],
  "expires_at": 1485936000000,
  "user_id": "5702f6451aa720287e00c182",
  "updated_at": 1485230685814,
  "created_at": 1459816086325,
  "locations": [
    {
      "_id": "57030696e9330ccbea008140",
      "point": [
        -117.4316272,
        47.66007159999999
      ],
      "address": {
        "_id": "57030696e9330ccbea008142",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "57030696e9330ccbea00813f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57030696e9330c000700010d",
    "attached_width": 385,
    "attached_height": 254,
    "attached_size": 24819,
    "attached_name": "airduct.JPG",
    "main": false,
    "updated_at": 1459816086325,
    "created_at": 1459816086325
  },
  "activated_at": 1459816088953,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570307cae9330cf7db00814d",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "56f1ac441aa720e8320152ee",
  "title": "Air Duct & Dryer Vent Cleaning",
  "category_id": "57437bafe9330c91d9000a3c",
  "offer": "ONLY $89 for Air Duct & Dryer Vent Cleaning | Normally $425\r\n> Includes the cleaning of 6 vents \r\n> A furnace Inspection \r\n> An antibacterial & deodorizer for vent systems",
  "terms": "> Service is within 20 miles of 99202.\r\n> If more than 30 miles from 99202 there may be an additional $25 charge.\r\n> Additional charge is to be discussed at time of service so company knows distance of drive.\r\n> Call, text or email to schedule an appointment.",
  "value": "425.0",
  "price": "89.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56f1ae6c1aa7206a6b015337"
  ],
  "expires_at": "",
  "user_id": "5702f6451aa720287e00c182",
  "updated_at": 1464040890605,
  "created_at": 1459816394163,
  "locations": [
    {
      "_id": "570307cae9330cf7db00814c",
      "point": [
        -117.4316272,
        47.66007159999999
      ],
      "address": {
        "_id": "570307cae9330cf7db00814e",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "",
        "postal_code": "99201"
      }
    }
  ],
  "image": {
    "_id": "570307cae9330cf7db00814b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570307cae9330c000700010f",
    "attached_width": 321,
    "attached_height": 603,
    "attached_size": 38953,
    "attached_name": "airductnvent.JPG",
    "main": false,
    "updated_at": 1459816394162,
    "created_at": 1459816394162
  },
  "activated_at": 1459816400161,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570451011aa7202f7700da44",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5703eef4e9330c46b2008fad",
  "title": "1 Colonic Session",
  "category_id": "570562e81aa720df6f00ed6e",
  "offer": "ONLY $45 for 1 Colonic Session | Normally $90.00\r\n\r\nCleanse your colon of waste!  Hydrotherapy can lead to a slimmer waste, flatter abs, and a new energetic feeling!\r\n\r\nSymptoms of a toxic bowel: IBS, Bloated? Constipated? Gas? Generally not feeling well? No energy? Wired and tired? Leaky Gut Syndrome? Can’t sleep? Weight gain? Heart Burn? Gastric Reflux (GERD?\r\n\r\nRegenerate and repair: food brings life to the body and herbs are medicine = healing.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. \r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided.",
  "value": "90.0",
  "price": "45.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5703f3371aa7203f0000d2ed"
  ],
  "expires_at": "",
  "user_id": "5703eebde9330c4d6a008f9f",
  "updated_at": 1460396828637,
  "created_at": 1459900673635,
  "locations": [
    {
      "_id": "570451011aa7202f7700da43",
      "point": [
        -116.771119,
        47.7185599
      ],
      "address": {
        "_id": "570451011aa7202f7700da45",
        "country": "US",
        "street": "4473 N Mt Carrol St",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "570451011aa7202f7700da42",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570451011aa72000060000eb",
    "attached_width": 867,
    "attached_height": 539,
    "attached_size": 46946,
    "attached_name": "tummy2.JPG",
    "main": false,
    "updated_at": 1459900673635,
    "created_at": 1459900673635
  },
  "activated_at": 1459900676433,
  "referral_code": "rachelr83",
  "deleted_at": 1485229326567
}
deals << {
  "_id": "5704519de9330c79830096f3",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "5703eef4e9330c46b2008fad",
  "title": "3 Colonic Sessions",
  "category_id": "570562e81aa720df6f00ed6e",
  "offer": "ONLY $129 for 3 Colonic Session | Normally $270.00\r\n\r\nCleanse your colon of waste! Hydrotherapy can lead to a slimmer waste, flatter abs, and a new energetic feeling!\r\n\r\nSymptoms of a toxic bowel: IBS, Bloated? Constipated? Gas? Generally not feeling well? No energy? Wired and tired? Leaky Gut Syndrome? Can’t sleep? Weight gain? Heart Burn? Gastric Reflux (GERD?\r\n\r\nRegenerate and repair: food brings life to the body and herbs are medicine = healing.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. \r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided.",
  "value": "270.0",
  "price": "129.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5703f3371aa7203f0000d2ed"
  ],
  "expires_at": "",
  "user_id": "5703eebde9330c4d6a008f9f",
  "updated_at": 1460396826169,
  "created_at": 1459900829156,
  "locations": [
    {
      "_id": "5704519de9330c79830096f2",
      "point": [
        -116.771119,
        47.7185599
      ],
      "address": {
        "_id": "5704519de9330c79830096f4",
        "country": "US",
        "street": "4473 N Mt Carrol St",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5704519de9330c79830096f1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5704519de9330c0007000128",
    "attached_width": 582,
    "attached_height": 485,
    "attached_size": 40362,
    "attached_name": "tummyheart.JPG",
    "main": false,
    "updated_at": 1459900829156,
    "created_at": 1459900829156
  },
  "activated_at": 1459900834386,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57055e15e9330cb8d900a7f8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "6 - 20 Minute HydroMassage Sessions",
  "category_id": "5705626de9330c5f5a00a834",
  "offer": "ONLY $30 for 6 - 20 Minute HydroMassage Sessions | Normally $60.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nHydroMassage tables were designed to provide the rejuvenating benefits of a warm water massage without getting wet or taking off your clothes. You can receive these therapeutic benefits in just one 20 minute water massage session: Temporary relief from minor aches and pains, Reduced levels of stress and anxiety, Deep relaxation and a sense of well-being, Increased circulation locally in massaged areas, Relief from muscle soreness, stiffness and tension.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. ",
  "value": "60.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230936213,
  "created_at": 1459969557723,
  "locations": [
    {
      "_id": "57055e15e9330cb8d900a7f7",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "57055e15e9330cb8d900a7f9",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57055fe31aa720095d00ed3e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57055fe31aa7200006000102",
    "attached_width": 787,
    "attached_height": 504,
    "attached_size": 39532,
    "attached_name": "hydrobedbig.JPG",
    "main": false,
    "updated_at": 1459970019183,
    "created_at": 1459970019183
  },
  "activated_at": 1459970025813,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57056110e9330cf4b600a818",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "12 - 20 Minute HydroMassage Sessions",
  "category_id": "5705626de9330c5f5a00a834",
  "offer": "ONLY $48 for 12 - 20 Minute HydroMassage Sessions | Normally $96.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.",
  "value": "96.0",
  "price": "48.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1485936000000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1485230952463,
  "created_at": 1459970320475,
  "locations": [
    {
      "_id": "57056110e9330cf4b600a817",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "57056110e9330cf4b600a819",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57056110e9330cf4b600a816",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57056110e9330c0007000154",
    "attached_width": 787,
    "attached_height": 504,
    "attached_size": 39532,
    "attached_name": "hydrobedbig.JPG",
    "main": false,
    "updated_at": 1459970320475,
    "created_at": 1459970320475
  },
  "activated_at": 1459970322848,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570bd476e9330cb798001014",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": " $25 for  $20 Deal ",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": "$25 worth of food for  $20",
  "terms": "> Limit 1 per visit. \r\n> Must redeem full amount of deal in 1 visit. \r\n> Limit 1 per person. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value valid after expiration date",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1514620800000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1488327554304,
  "created_at": 1460393078750,
  "locations": [
    {
      "_id": "570bd476e9330cb798001013",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "570bd476e9330cb798001015",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "570bd476e9330cb798001012",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570bd476e9330c000600000e",
    "attached_width": 960,
    "attached_height": 960,
    "attached_size": 115966,
    "attached_name": "localdelidealpromo.jpg",
    "main": false,
    "updated_at": 1460393078750,
    "created_at": 1460393078750
  },
  "activated_at": 1460393134225,
  "referral_code": ""
}
deals << {
  "_id": "570bdf521aa72002be00136f",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5391fdedf02cbecf46000aa7",
  "title": "$20 for $30 Worth of Laundry Services",
  "category_id": "4f7d216074dbb02de3000740",
  "offer": "ONLY $20 for $30 Worth of Laundry Services",
  "terms": "> Valid during regular business hours only. \r\n> Not to be combined with any other offers.",
  "value": "30.0",
  "price": "20.0",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "5391ffb4f02cbe3ce1000b7d"
  ],
  "expires_at": "",
  "user_id": "5391fdedf02cbecf46000aa8",
  "updated_at": 1460587908095,
  "created_at": 1460395858236,
  "locations": [
    {
      "_id": "570bdf521aa72002be00136e",
      "geocoded_city_level": false,
      "geocode_source": "nominatim",
      "point": [
        -117.3704461102508,
        47.6279721903828
      ],
      "address": {
        "_id": "570bdf521aa72002be001370",
        "country": "US",
        "street": "2720 E. 29th,",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99223"
      }
    }
  ],
  "image": {
    "_id": "570bdf5e1aa72059cf001372",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570bdf5e1aa7200007000017",
    "attached_width": 348,
    "attached_height": 348,
    "attached_size": 31062,
    "attached_name": "348s.jpg",
    "main": false,
    "updated_at": 1460395870195,
    "created_at": 1460395870195
  },
  "activated_at": 1460395874557,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570bea891aa720d825001455",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "538e04b59c058e34c4000bb9",
  "title": "$40 for $50 of Lawn Care Services",
  "category_id": "4f7d216074dbb02de3000749",
  "offer": "ONLY $40 for $50 of Lawn Care Services\r\n> This is a gift card for Big Ben's Property Maintenance in Spokane, Washington. \r\n> I serve Spokane and some surrounding areas.",
  "terms": "> This Gift Card is only applicable for bid lawn care service. \r\n> Because my business is a bid-oriented service, I must survey the area to be worked on and provide a proper bid for my services. \r\n> This amount varies depending on size, weight, distance, etc.",
  "value": "50.0",
  "price": "40.0",
  "limit": 10,
  "limit_per_customer": 1,
  "listing_ids": [
    "538e075f9c058e7a03000b24"
  ],
  "expires_at": "",
  "user_id": "538e04b59c058e34c4000bba",
  "updated_at": 1460588139326,
  "created_at": 1460398729214,
  "locations": [
    {
      "_id": "570bea891aa720d825001454",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -117.43979,
        47.69399
      ],
      "address": {
        "_id": "570bea891aa720d825001456",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "570bea891aa720d825001453",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570bea891aa7200007000027",
    "attached_width": 300,
    "attached_height": 400,
    "attached_size": 137487,
    "attached_name": "img_06921.jpg",
    "main": false,
    "updated_at": 1460398729214,
    "created_at": 1460398729214
  },
  "activated_at": 1460398733894,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570beaf1e9330ca0d90011b3",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "538e04b59c058e34c4000bb9",
  "title": "$90 for $100 of Lawn Care Services",
  "category_id": "4f7d216074dbb02de3000749",
  "offer": "ONLY $90 for $100 of Lawn Care Services\r\n> This is a gift card for Big Ben's Property Maintenance in Spokane, Washington. \r\n> I serve Spokane and some surrounding areas.",
  "terms": "> This Gift Card is only applicable for bid lawn care service. \r\n> Because my business is a bid-oriented service, I must survey the area to be worked on and provide a proper bid for my services. \r\n> This amount varies depending on size, weight, distance, etc. ",
  "value": "100.0",
  "price": "90.0",
  "limit": 10,
  "limit_per_customer": 1,
  "listing_ids": [
    "538e075f9c058e7a03000b24"
  ],
  "expires_at": "",
  "user_id": "538e04b59c058e34c4000bba",
  "updated_at": 1460588152678,
  "created_at": 1460398833269,
  "locations": [
    {
      "_id": "570beaf1e9330ca0d90011b2",
      "geocoded_city_level": true,
      "geocode_source": "postal code",
      "point": [
        -117.43979,
        47.69399
      ],
      "address": {
        "_id": "570beaf1e9330ca0d90011b4",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "570beaf1e9330ca0d90011b1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570beaf1e9330c0006000020",
    "attached_width": 300,
    "attached_height": 402,
    "attached_size": 186047,
    "attached_name": "img_03261.jpg",
    "main": false,
    "updated_at": 1460398833269,
    "created_at": 1460398833269
  },
  "activated_at": 1460398836635,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570bf0c71aa72096900014ca",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56141846f02cbe4b23001bbc",
  "title": "$85 for $100 Worth of Auto Services",
  "category_id": "4f7d215774dbb02de300031a",
  "offer": "ONLY $85 for $100 Worth of Auto Services\r\n> Must Present Gift Card before Purchasing.",
  "terms": "> Valid during normal working hours.\r\n> If work exceeds deal amount, difference must be paid in cash at time of service",
  "value": "100.0",
  "price": "85.0",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "56141c7af02cbeac4b001b4e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5627f042f02cbe046500214c",
  "updated_at": 1485230536737,
  "created_at": 1460400327331,
  "locations": [
    {
      "_id": "570bf0c71aa72096900014c9",
      "point": [
        -116.940599,
        47.707892
      ],
      "address": {
        "_id": "570bf0c71aa72096900014cb",
        "country": "US",
        "street": "516 E. 3rd Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "570bf0c71aa72096900014c8",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570bf0c71aa720000700002d",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 66634,
    "attached_name": "car.jpg",
    "main": false,
    "updated_at": 1460400327331,
    "created_at": 1460400327331
  },
  "activated_at": 1460400329621,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570bf11ce9330c90bb001227",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56141846f02cbe4b23001bbc",
  "title": "$40 for $50 Worth of Auto Services",
  "category_id": "4f7d215774dbb02de300031a",
  "offer": "ONLY $40 for $50 Worth of Auto Services\r\n> Must Present Gift Card before Purchasing.",
  "terms": "> Valid during normal working hours. \r\n> If work exceeds deal amount, difference must be paid in cash at time of service",
  "value": "50.0",
  "price": "40.0",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "56141c7af02cbeac4b001b4e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5627f042f02cbe046500214c",
  "updated_at": 1485230550384,
  "created_at": 1460400412480,
  "locations": [
    {
      "_id": "570bf11ce9330c90bb001226",
      "point": [
        -116.940599,
        47.707892
      ],
      "address": {
        "_id": "570bf11ce9330c90bb001228",
        "country": "US",
        "street": "516 E. 3rd Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "570bf11ce9330c90bb001225",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570bf11ce9330c0006000026",
    "attached_width": 590,
    "attached_height": 443,
    "attached_size": 44298,
    "attached_name": "car1.jpg",
    "main": false,
    "updated_at": 1460400412479,
    "created_at": 1460400412479
  },
  "activated_at": 1460400414861,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570c00a61aa720c051001588",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "537fc71af02cbef1c2001059",
  "title": "$20 for $40 at Custom Truck",
  "category_id": "4f7d215774dbb02de3000319",
  "offer": "ONLY $20 for $40 at Custom Truck\r\n\r\nCustom Truck located in Coeur d'Alene ID \r\n(208 765-4444 \r\nrob@custom-truck.net\r\n\r\n> Double your money at Custom Truck!\r\n> Must Present Gift Card before Purchasing. \r\n> Limited time offer.",
  "terms": "> One coupon per household per year.\r\n> Valid during normal working hours.",
  "value": "40.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "537fd226f02cbee6d80011d1",
    "537fcc92f02cbe71ec001189"
  ],
  "expires_at": 1485936000000,
  "user_id": "4fb5539308dea138060001a5",
  "updated_at": 1485230432284,
  "created_at": 1460404390061,
  "locations": [
    {
      "_id": "570c00a61aa720c051001586",
      "point": [
        -116.7895391,
        47.7150691
      ],
      "address": {
        "_id": "570c00a61aa720c051001589",
        "country": "US",
        "street": "254 W. Kathleen Ave.",
        "suite": "",
        "city": "Coeur d 'Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    },
    {
      "_id": "570c00a61aa720c051001587",
      "point": [
        -117.3069313,
        47.6574996
      ],
      "address": {
        "_id": "570c00a61aa720c05100158a",
        "country": "US",
        "street": "7225 E. Sprague Avenue",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "570c00a61aa720c051001585",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570c00a61aa7200007000035",
    "attached_width": 1000,
    "attached_height": 670,
    "attached_size": 121951,
    "attached_name": "truck.jpg",
    "main": false,
    "updated_at": 1460404390060,
    "created_at": 1460404390060
  },
  "activated_at": 1460404392997,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570d3a121aa72027500022c2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ad9f9af02cbe950100199b",
  "title": "$20 for $25 Worth of Food",
  "category_id": "4f7d216674dbb02de300098f",
  "offer": "ONLY $20 for $25 Worth of Food\r\n\r\n315 Martinis & Tapas located in Coeur d'Alene ID\r\n(208 667-9660",
  "terms": "> Offer only valid for food, NOT DRINKS.\r\n> Expires 120 days after purchase.\r\n> May be repurchased every 120 days. \r\n> Limit 1 per person, may buy 1 additional as gift. \r\n> Limit 1 per visit.\r\n> Must redeem full amount in one visit.",
  "value": "25.0",
  "price": "20.0",
  "limit": 1000,
  "limit_per_customer": 20,
  "listing_ids": [
    "54ada5ecf02cbed2a5001ac0"
  ],
  "expires_at": 1485936000000,
  "user_id": "54aed4cbf02cbe47d000008b",
  "updated_at": 1485230356340,
  "created_at": 1460484626466,
  "locations": [
    {
      "_id": "570d3a121aa72027500022c1",
      "point": [
        -116.781648,
        47.676967
      ],
      "address": {
        "_id": "570d3a121aa72027500022c3",
        "country": "US",
        "street": "315 Wallace Ave.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "570d3a121aa72027500022c0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570d3a121aa7200007000047",
    "attached_width": 1000,
    "attached_height": 750,
    "attached_size": 103825,
    "attached_name": "o (4.jpg",
    "main": false,
    "updated_at": 1460484626465,
    "created_at": 1460484626465
  },
  "activated_at": 1460484632979,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "570d5af31aa720218f00264e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568adc1694cacfff800096a8",
  "title": "Initial Exam and 3 Adjustments",
  "category_id": "4f7d215f74dbb02de30006dd",
  "offer": "SAVE $110!! $50 initial exam and 3 adjustments (160$ value",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem. ",
  "value": "160",
  "price": "50",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568bfb880ce9446d16001293"
  ],
  "expires_at": "",
  "user_id": "568ae7ba94cacf10970098eb",
  "updated_at": 1460493047067,
  "created_at": 1460493043037,
  "locations": [
    {
      "_id": "570d5af21aa720218f00264d",
      "point": [
        -116.786655,
        47.690953
      ],
      "address": {
        "_id": "570d5af31aa720218f00264f",
        "country": "US",
        "street": "1705 N Government Way",
        "suite": "",
        "city": "Couer d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "570d5af21aa720218f00264c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570d5af21aa7200007000051",
    "attached_width": 600,
    "attached_height": 531,
    "attached_size": 88214,
    "attached_name": "adjustments.jpg",
    "main": false,
    "updated_at": 1460493043037,
    "created_at": 1460493043037
  },
  "activated_at": 1460493047056,
  "deleted_at": 1485230265874
}
deals << {
  "_id": "570ffc861aa720fc12004740",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6752e08dea15b5a00a7f6",
  "title": "Resista Carpet & Rug Spot Remover",
  "category_id": "4f7d215974dbb02de30003e2",
  "offer": "ONLY $13.36 for a bottle of Resista Carpet & Rug Spot Remover | Normally $15.36\r\n> For everyday spills, dirt and stains.\r\n> Resista Carpet & Rug Spot Remover is great for those small quick cleanups. \r\n> It’s safe and very effective on small spills like juice, cola, coffee and wine. \r\n> The product is water based and is safe on stain resistant carpets and area rugs.\r\n\r\n",
  "terms": "> Must bring in printed voucher to store location to redeem and get bottle. \r\n> Not shipped to your location\r\n> Only valid for the Resilient Carpet Cleaner.  \r\n> Not valid for any other Resilient product.",
  "value": "15.36",
  "price": "13.36",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6752e08dea15b5a00a7f5"
  ],
  "expires_at": "",
  "user_id": "4fb553a508dea138060006a9",
  "updated_at": 1460665478380,
  "created_at": 1460665478380,
  "locations": [
    {
      "_id": "570ffc861aa720fc1200473f",
      "geocode_source": "google",
      "geocoded_city_level": false,
      "point": [
        -116.797707,
        47.7011619
      ],
      "address": {
        "_id": "570ffc861aa720fc12004741",
        "country": "US",
        "city": "COEUR D'ALENE",
        "postal_code": "83814",
        "region": "ID",
        "street": "739 WEST APPLEWAY",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "570ffc861aa720fc1200473e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570ffc861aa7200007000065",
    "attached_width": 343,
    "attached_height": 519,
    "attached_size": 22531,
    "attached_name": "resista.JPG",
    "main": false,
    "updated_at": 1460665478380,
    "created_at": 1460665478380
  },
  "deleted_at": 1462918935215
}
deals << {
  "_id": "570ffd26e9330c628a003ed8",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6752e08dea15b5a00a7f6",
  "title": "Resilient Cleaner Floor Cleaner",
  "category_id": "4f7d215874dbb02de30003b4",
  "offer": "ONLY $9.99 for a bottle of Resilient Cleaner Floor Cleaner | Normally $11.99\r\n\r\nPanhandle Carpet One located in Coeur d'Alene ID\r\n(208 765-5456\r\n\r\n> The formula is a special blend that provides superior cleaning and a beautiful shine.\r\n\r\n",
  "terms": "> Must bring in printed voucher to store location to redeem and get bottle. \r\n> Not shipped to your location\r\n> Only valid for the Resilient Carpet Cleaner.  \r\n> Not valid for any other Resilient product.",
  "value": "11.99",
  "price": "9.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6752e08dea15b5a00a7f5"
  ],
  "expires_at": 1485936000000,
  "user_id": "4fb553a508dea138060006a9",
  "updated_at": 1485230207701,
  "created_at": 1460665638089,
  "locations": [
    {
      "_id": "570ffd26e9330c628a003ed7",
      "geocode_source": "google",
      "geocoded_city_level": false,
      "point": [
        -116.797707,
        47.7011619
      ],
      "address": {
        "_id": "570ffd26e9330c628a003ed9",
        "country": "US",
        "city": "COEUR D'ALENE",
        "postal_code": "83814",
        "region": "ID",
        "street": "739 WEST APPLEWAY",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "570ffd26e9330c628a003ed6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "570ffd26e9330c0006000064",
    "attached_width": 293,
    "attached_height": 469,
    "attached_size": 30959,
    "attached_name": "resilient 2.JPG",
    "main": false,
    "updated_at": 1460665638089,
    "created_at": 1460665638089
  },
  "activated_at": 1460668641392,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57112c311aa720be970056f7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Compact Car Full Detail (Interior & Exterior",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $99 for Full Interior and Exterior Detailing for a Compact Car | Normally $250",
  "terms": "> Can be bought every 180 days.\r\n> Limit 1 per person (may purchase 1 additional as a gift\r\n> Valid only for the option purchased.",
  "value": "250.0",
  "price": "99.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1485936000000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1485230086190,
  "created_at": 1460743217448,
  "locations": [
    {
      "_id": "57112c311aa720be970056f6",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "57112c311aa720be970056f8",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57112c311aa720be970056f5",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57112c311aa7200007000086",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 125844,
    "attached_name": "car.jpg",
    "main": false,
    "updated_at": 1460743217448,
    "created_at": 1460743217448
  },
  "activated_at": 1460743221388,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57112dfc1aa720e3a600570d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Medium Size Car Full Detail (Interior & Exterior",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $119 for Full Medium Size Car Full Detail (Interior & Exterior | Normally $300",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "300.0",
  "price": "119.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1485936000000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1485230096740,
  "created_at": 1460743676506,
  "locations": [
    {
      "_id": "57112dfc1aa720e3a600570c",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "57112dfc1aa720e3a600570e",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57112dfc1aa720e3a600570b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57112dfc1aa7200007000088",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 143883,
    "attached_name": "medium.jpg",
    "main": false,
    "updated_at": 1460743676506,
    "created_at": 1460743676506
  },
  "activated_at": 1460743679779,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57112e991aa720aa2e005714",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Large Car Full Detail (Interior & Exterior",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $139 Large Car Full Detail (Interior & Exterior | Normally $350",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "350.0",
  "price": "139.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1485936000000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1485230105737,
  "created_at": 1460743833691,
  "locations": [
    {
      "_id": "57112e991aa720aa2e005713",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "57112e991aa720aa2e005715",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57112e991aa720aa2e005712",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57112e991aa720000700008a",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 104382,
    "attached_name": "truck.jpg",
    "main": false,
    "updated_at": 1460743833691,
    "created_at": 1460743833691
  },
  "activated_at": 1460743836445,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57112f3c1aa72033aa005721",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Compact Car Full Interior Detail",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $75 for Compact Car Full Interior Detail | Normally $200",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "200.0",
  "price": "75.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1485936000000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1485230119099,
  "created_at": 1460743996790,
  "locations": [
    {
      "_id": "57112f3c1aa72033aa005720",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "57112f3c1aa72033aa005722",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57112f3c1aa72033aa00571f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57112f3c1aa720000700008c",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 83921,
    "attached_name": "honda.jpg",
    "main": false,
    "updated_at": 1460743996790,
    "created_at": 1460743996790
  },
  "activated_at": 1460743999553,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57112faf1aa720d42200572c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Medium Size Car Full Interior Detail",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $119 for Medium Size Car Full Interior Detail | Normally $250 value",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "250.0",
  "price": "119.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1485936000000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1485230129269,
  "created_at": 1460744111458,
  "locations": [
    {
      "_id": "57112faf1aa720d42200572b",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "57112faf1aa720d42200572d",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "57112faf1aa720d42200572a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57112faf1aa720000700008e",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 137881,
    "attached_name": "compact.jpg",
    "main": false,
    "updated_at": 1460744111458,
    "created_at": 1460744111458
  },
  "activated_at": 1460744113786,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "571130191aa72079b8005735",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Large Car Full Interior Detail",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $139 for Large Car Full Interior Detail | $300 value",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "300.0",
  "price": "139.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1485936000000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1485230138424,
  "created_at": 1460744217498,
  "locations": [
    {
      "_id": "571130191aa72079b8005734",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "571130191aa72079b8005736",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "571130191aa72079b8005733",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "571130191aa7200007000090",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 62697,
    "attached_name": "large.jpg",
    "main": false,
    "updated_at": 1460744217498,
    "created_at": 1460744217498
  },
  "activated_at": 1460744223274,
  "referral_code": "rachelr83"
}
deals << {
  "_id": "57312f02e9330ca1a3016613",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Feed The Team (15 People = $10/person",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "ONLY $150 to Feed The Team | Normally $200 (That's only $10 per person!\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com\r\n\r\nOffer Includes\r\n> Main Choices are: Cheese Burger, Chicken Strips or Grilled Cheese\r\n> 5 Appetizers - Choose from: Sriracha Popcorn Chicken, Fried pickles, Homemade Fedora Chips\r\n> Includes Fedora Fries and a Drink (Soda, Tea or Coffee",
  "terms": "> Must be minimum of 15 people\r\n> Must be pre-ordered, voucher printed and redeemed at time of visit\r\n> Minimum 15 people\r\n> Cannot be combined with any other Deals or Coupons",
  "value": "200.0",
  "price": "150.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1485936000000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1485229673083,
  "created_at": 1462841090947,
  "locations": [
    {
      "_id": "57312f02e9330ca1a3016612",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "57312f02e9330ca1a3016614",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57312f02e9330ca1a3016611",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57312f02e9330c00060001b0",
    "attached_width": 263,
    "attached_height": 255,
    "attached_size": 54440,
    "attached_name": "fedorasoftballdeal.jpg",
    "main": false,
    "updated_at": 1462841090947,
    "created_at": 1462841090947
  },
  "activated_at": 1462841982704,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "574389df1aa720708b000e10",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5685696694cacf1cf900226f",
  "title": "ONLY $40 for 60 Minute Massage - SAVE $10!",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": "ONLY $40 for 60 Minute Massage - SAVE $10!\r\n\r\nA-Game Massage Studio located in Coeur d'Alene ID\r\n(208 818-0642",
  "terms": "> Must Purchase via website and bring printed Voucher or show on your smartphone to redeem.\r\n> A-Game Massage is located inside The Gym LLC, though efforts are made to insulate sound, complete silence is not guaranteed. ",
  "value": "50.0",
  "price": "40.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "568c03050ce944361900140b"
  ],
  "expires_at": 1546243200000,
  "user_id": "568b45570ce94475dd000582",
  "updated_at": 1486064816051,
  "created_at": 1464043999347,
  "locations": [
    {
      "_id": "574389df1aa720708b000e0f",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "574389df1aa720708b000e11",
        "country": "US",
        "street": "411 W Haycraft Avenue",
        "suite": "C-1 (Inside The Gym LLC",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "574389df1aa720708b000e0e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "574389df1aa7200007000015",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 39542,
    "attached_name": "A-Game Massage Studio6.jpg",
    "main": false,
    "updated_at": 1464043999347,
    "created_at": 1464043999347
  },
  "activated_at": 1464044002230,
  "referral_code": ""
}
deals << {
  "_id": "5744e18b1aa72010df0043cd",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5683237ff02cbe7f16003908",
  "title": "Pamper Me Package - Pick 3",
  "category_id": "4f7d215a74dbb02de3000438",
  "offer": "ONLY $39 for the Pamper Me Package | Normally $89\r\nChoose 3 of the following: Mini Facial, Brow-Shaping/Wax, Manicure, Haircut, Deep Conditioning & Style or Traditional Make-Up Session\r\n\r\nFlawless Full Service Salon\r\n(208 765-1110 / amymorfitt@gmail.com",
  "terms": "> Can only choose 3 of the 6 services\r\n> Must redeem all chosen option in one visit",
  "value": "89.0",
  "price": "39.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "573b82d1e9330cd5aa00038d"
  ],
  "expires_at": "",
  "user_id": "56ac03b647ffb0099b0273c0",
  "updated_at": 1477339481483,
  "created_at": 1464131979450,
  "locations": [
    {
      "_id": "5744e18b1aa72010df0043cc",
      "point": [
        -116.7986082,
        47.6894366
      ],
      "address": {
        "_id": "5744e18b1aa72010df0043ce",
        "country": "US",
        "street": "1510 Northwest Blvd, Coeur d'Alene",
        "suite": "",
        "city": "CDA",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5744e18b1aa72010df0043cb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5744e18b1aa7200007000036",
    "attached_width": 484,
    "attached_height": 485,
    "attached_size": 62819,
    "attached_name": "pamper3.JPG",
    "main": false,
    "updated_at": 1464131979449,
    "created_at": 1464131979449
  },
  "activated_at": 1464131983927,
  "referral_code": "currentattractions",
  "deleted_at": 1485229806263
}
deals << {
  "_id": "5744e503e9330cc0a100413b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Dinner/Lunch for 2 (FREE App",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "ONLY $31 for Dinner or Lunch for 2 | Normally $37\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com\r\n\r\nPick any 2:\r\n> Burger (Fedora Burger, Pattywagon, Bugsy, Cheddar jack The Ripper or Sandwich (Hardtimes, Yardbird, Big House, Monte Cristo, Cubano Melt, Ruebon\r\n\r\nComes with: \r\n>Fries and drink (soft drink, tea or coffee\r\n\r\nGet a FREE Appetizer:\r\n> Sriracha popcorn chicken, fried pickles or homemade Fedora chips",
  "terms": "> Must bring in printed voucher to redeem at location\r\n> Cannot be combined with any other deals or offers.\r\n> Must be used in 1 visit",
  "value": "37.0",
  "price": "31.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1485936000000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1485229689071,
  "created_at": 1464132867453,
  "locations": [
    {
      "_id": "5744e503e9330cc0a100413a",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "5744e503e9330cc0a100413c",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5744e503e9330cc0a1004139",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5744e503e9330c0007000040",
    "attached_width": 480,
    "attached_height": 388,
    "attached_size": 35395,
    "attached_name": "saladsandwich.jpg",
    "main": false,
    "updated_at": 1464132867453,
    "created_at": 1464132867453
  },
  "activated_at": 1464132870516,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "574614b61aa72002e4008749",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "$20 For $25 Worth of Food",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": " $20 for $25 worth of food\r\n\r\nRivelle's River Grill located in Coeur d'Alene, ID\r\n(208 930-0381\r\nrivellescda@gmail.com",
  "terms": "> Offer expires September 1st 2016. \r\n> Must be used during regular business hours. \r\n> Must be used in 1 visit. \r\n> $20 Cash does not expire\r\n> Cannot be combined with any other deals or coupons\r\n> Limit 1 per table\r\n> Limit 1 per person",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": 1,
  "listing_ids": [
    "57336af61aa7203be5000ace"
  ],
  "expires_at": 1485936000000,
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1485228067941,
  "created_at": 1464210614164,
  "locations": [
    {
      "_id": "574614b61aa72002e4008748",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "574614b61aa72002e400874a",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57487e6a1aa720fa0600b39a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57487e6a1aa720000700005e",
    "attached_width": 796,
    "attached_height": 531,
    "attached_size": 804118,
    "attached_name": "riverpix2.jpg.png",
    "main": false,
    "updated_at": 1464368746784,
    "created_at": 1464368746784
  },
  "activated_at": 1464368763242,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5750bcdc1aa720aeaf010dc1",
  "active": true,
  "vouchers_count": 5,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$19.99 for $25 Worth of Food ",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "ONLY $19.99 for  $25 Worth of Food ",
  "terms": "> Must redeem full amount of deal in 1 visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value  valid after expiration date",
  "value": "25.0",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": 1496386800000,
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1467924860030,
  "created_at": 1464909020716,
  "locations": [
    {
      "_id": "5750bcdc1aa720aeaf010dc0",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "5750bcdc1aa720aeaf010dc2",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5750bcdc1aa720aeaf010dbf",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5750bcdc1aa7200007000168",
    "attached_width": 718,
    "attached_height": 718,
    "attached_size": 70903,
    "attached_name": "schmidtyspromo2016.jpg",
    "main": false,
    "updated_at": 1464909020716,
    "created_at": 1464909020716
  },
  "activated_at": 1464909052379,
  "referral_code": ""
}
deals << {
  "_id": "5750be391aa7202eed010ddc",
  "active": true,
  "vouchers_count": 4,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "Only $39.99 for $50 worth of food",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "Only $39.99 for $50 worth of food",
  "terms": "> Must redeem full amount of deal in 1 visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value valid after expiration date",
  "value": "50",
  "price": "39.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": "",
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1479862946113,
  "created_at": 1464909369780,
  "locations": [
    {
      "_id": "5750be391aa7202eed010ddb",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "5750be391aa7202eed010ddd",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5750be391aa7202eed010dda",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5750be391aa720000700016c",
    "attached_width": 960,
    "attached_height": 681,
    "attached_size": 102034,
    "attached_name": "schmidtysmarketpad.jpg",
    "main": false,
    "updated_at": 1464909369780,
    "created_at": 1464909369780
  },
  "activated_at": 1464909398974,
  "referral_code": ""
}
deals << {
  "_id": "5751ae0ee9330cd67500d83c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5735fcc01aa7205cc0003002",
  "title": " Custom Metal  Fish Tank Stand",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": " ONLY $675 for a Custom Metal Fish Tank Stand | Normally $800\r\n> Dimensions: 50” x 14” x 36” \r\n> 1 ½ Sq Tube Steel\r\n> Stands are all powder coated\r\n> Wood is finished with Polyurethane \r\n> Good for any Tank 48” up to 55 Gallons\r\n",
  "terms": "> Fish Tank not included\r\n> Must contact seller via phone or email after purchasing voucher to place order\r\n> Allow 3 weeks from order for completion\r\n> Cannot combine with any other deals or coupons\r\n",
  "value": "800.0",
  "price": "675.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57507c98e9330c6f4a00cbd3"
  ],
  "expires_at": "",
  "user_id": "5735fcc01aa7205cc0002fff",
  "updated_at": 1473191140323,
  "created_at": 1464970766667,
  "locations": [
    {
      "_id": "5751ae0ee9330cd67500d83b",
      "point": [
        -116.858399,
        47.704514
      ],
      "address": {
        "_id": "5751ae0ee9330cd67500d83d",
        "country": "US",
        "street": "6604 E Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5751ae0ee9330cd67500d83a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5751ae0ee9330c00070000fd",
    "attached_width": 2729,
    "attached_height": 2265,
    "attached_size": 1139453,
    "attached_name": "patriotfabfishtankstand.JPG",
    "main": false,
    "updated_at": 1464970766667,
    "created_at": 1464970766667
  },
  "activated_at": 1464970770505,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5751b0aee9330cf46f00d869",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5735fcc01aa7205cc0003002",
  "title": " Custom Metal Fireplace  Mantle ",
  "category_id": "4f7d216074dbb02de300072b",
  "offer": " ONLY $775 for a Custom Metal Fireplace Mantle | Normally $900\r\n> Dimensions: 5’ x 12”\r\n> Metal art is Patina finished with powder coat clear coat\r\n> Corbels powder coat Black",
  "terms": "> Fireplace not included \r\n> Must contact seller via phone or email after purchasing voucher to place order \r\n> Allow 3 weeks from order for completion \r\n> Cannot combine with any other deals or coupons",
  "value": "900.0",
  "price": "775.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57507c98e9330c6f4a00cbd3"
  ],
  "expires_at": "",
  "user_id": "5735fcc01aa7205cc0002fff",
  "updated_at": 1473191145096,
  "created_at": 1464971438619,
  "locations": [
    {
      "_id": "5751b0aee9330cf46f00d868",
      "point": [
        -116.858399,
        47.704514
      ],
      "address": {
        "_id": "5751b0aee9330cf46f00d86a",
        "country": "US",
        "street": "6604 E Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5751b0aee9330cf46f00d867",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5751b0aee9330c0007000108",
    "attached_width": 2209,
    "attached_height": 2347,
    "attached_size": 892743,
    "attached_name": "PatriotFabworksdeal2.jpg",
    "main": false,
    "updated_at": 1464971438619,
    "created_at": 1464971438619
  },
  "activated_at": 1464971443466,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5751b751e9330c8b5600d8a9",
  "active": true,
  "vouchers_count": 4,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": " 30 Minute Batting Cage Rental",
  "category_id": "4f7d216474dbb02de30008d0",
  "offer": "ONLY $8 for 30 Minute Batting Cage Rental | Normally $15",
  "terms": "> Limit 1 per person per visit. \r\n> Please call ahead to schedule batting cage time.\r\n> Waiver must be signed at time of visit.\r\n> Two individuals needed to use cages.\r\n> Office staff available to work cages for additional $15 per session.\r\n> Must bring own equipment. \r\n> Use of batting cages is subject to availability. ",
  "value": "15.0",
  "price": "8.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": "",
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1467245393896,
  "created_at": 1464973137809,
  "locations": [
    {
      "_id": "5751b751e9330c8b5600d8a8",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "5751b751e9330c8b5600d8aa",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "5751b751e9330c8b5600d8a7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5751b751e9330c000700010d",
    "attached_width": 1000,
    "attached_height": 750,
    "attached_size": 437076,
    "attached_name": "batting1.jpg",
    "main": false,
    "updated_at": 1464973137809,
    "created_at": 1464973137809
  },
  "activated_at": 1464974609752,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5751b8b7e9330cce6c00d8bc",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": "1 Hour Batting Cage Rental",
  "category_id": "4f7d215874dbb02de30003c1",
  "offer": "ONLY $13 for 1 Hour Batting Cage Rental Session | Normally $30",
  "terms": "> Limit 1 per person per visit. \r\n> Please call ahead to schedule batting cage time.\r\n> Waiver must be signed at time of visit.\r\n> Two individuals needed to use cages.\r\n> Office staff available to work cages for additional $15 per session.\r\n> Must bring own equipment. \r\n> Use of batting cages is subject to availability. ",
  "value": "30.0",
  "price": "13.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": "",
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1465226864540,
  "created_at": 1464973495001,
  "locations": [
    {
      "_id": "5751b8b6e9330cce6c00d8bb",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "5751b8b7e9330cce6c00d8bd",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "5751b8b6e9330cce6c00d8ba",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5751b8b6e9330c0007000110",
    "attached_width": 960,
    "attached_height": 640,
    "attached_size": 156975,
    "attached_name": "batting4.jpg",
    "main": false,
    "updated_at": 1464973495001,
    "created_at": 1464973495001
  },
  "activated_at": 1464974611020,
  "deleted_at": 1485228782953
}
deals << {
  "_id": "5751b9541aa720dd3401186a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": "Five 1 Hour Batting Cage Rentals",
  "category_id": "4f7d215874dbb02de30003c1",
  "offer": "ONLY $50 for Five 1 Hour Batting Cage Rental Sessions | Normally $125",
  "terms": "> Limit 1 per person per visit. \r\n> Please call ahead to schedule batting cage time.\r\n> Waiver must be signed at time of visit.\r\n> Two individuals needed to use cages.\r\n> Office staff available to work cages for additional $15 per session.\r\n> Must bring own equipment. \r\n> Use of batting cages is subject to availability. ",
  "value": "125.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": "",
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1465226867792,
  "created_at": 1464973652187,
  "locations": [
    {
      "_id": "5751b9541aa720dd34011869",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "5751b9541aa720dd3401186b",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "5751b9541aa720dd34011868",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5751b9541aa7200007000177",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 124015,
    "attached_name": "batting5.jpg",
    "main": false,
    "updated_at": 1464973652187,
    "created_at": 1464973652187
  },
  "activated_at": 1464974612408,
  "deleted_at": 1485228787394
}
deals << {
  "_id": "575594a01aa7201b59013ea9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": "  21 Day Fitness Jumpstart",
  "category_id": "4f7d216574dbb02de3000973",
  "offer": "ONLY $98 for a 21 Day Fitness Jumpstart | Normally $175  \r\nComplete Athlete Located in Spokane Valley\r\n(509 808-2716\r\ninfo@completeathletespokane.com\r\n\r\n> 1 personal assessment training session.\r\n> 8 small group training sessions\r\n> 1 group nutrition consultation.\r\n> Daily nutrition tips.\r\n",
  "terms": "> Limit 1 per person per visit. \r\n> Please call ahead to schedule appointment.\r\n> Waiver must be signed at time of visit.",
  "value": "175.0",
  "price": "98.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": 1485936000000,
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1485228815466,
  "created_at": 1465226400654,
  "locations": [
    {
      "_id": "575594a01aa7201b59013ea8",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "575594a01aa7201b59013eaa",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "575594a01aa7201b59013ea7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "575594a01aa7200007000185",
    "attached_width": 2048,
    "attached_height": 1365,
    "attached_size": 564971,
    "attached_name": "3.jpg",
    "main": false,
    "updated_at": 1465226400654,
    "created_at": 1465226400654
  },
  "activated_at": 1465226404019,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "575597c31aa72031cb013f03",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": "  Sports Performance Programs",
  "category_id": "4f7d216574dbb02de3000973",
  "offer": "ONLY $50 for $100 Worth of Services towards Sports Performance Programs (youth to professional \r\nComplete Athlete Located in Spokane Valley.\r\n(509 808-2716\r\ninfo@completeathletespokane.com\r\n",
  "terms": "> Not valid with any other offer\r\n> Limit 1 per person per visit. \r\n> Please call ahead to schedule batting cage time.\r\n> Waiver must be signed at time of visit.",
  "value": "100.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": 1485936000000,
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1485228829941,
  "created_at": 1465227203254,
  "locations": [
    {
      "_id": "575597c31aa72031cb013f02",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "575597c31aa72031cb013f04",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "575597c31aa72031cb013f01",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "575597c31aa720000700018b",
    "attached_width": 960,
    "attached_height": 539,
    "attached_size": 45292,
    "attached_name": "1.jpg",
    "main": false,
    "updated_at": 1465227203254,
    "created_at": 1465227203254
  },
  "activated_at": 1465227208009,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "575adf851aa7209619018253",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "5744a3e2e9330c3ef0003a3a",
  "title": " Pend Orielle Freezer Meat Pack",
  "category_id": "4f7d215e74dbb02de3000615",
  "offer": "ONLY $300 for the The Pend Orielle Freezer Meat Pack | Normally $335\r\n\r\nIncludes:\r\n> 6.5lbs  - Whole Chicken Breast                       \r\n> 10lbs - Whole Chicken\r\n> 4.5lbs - Smoked Chicken\r\n> 3lbs - Sirloin\r\n> 5lbs - T-Bone or Rib Steak\r\n> 1lb - Filet Tail\r\n> 4lbs - Boneless Beef Roast\r\n> 15lbs - Regular Ground Beef\r\n> 10lbs - Pork Chops\r\n> 3lbs - Bone-In Pork Roast\r\n> 3lbs - Fresh Pork Sausage\r\n> 5lbs - Bacon",
  "terms": "> Must be used during regular business hours. \r\n> Must be used in 1 visit. \r\n> $300 Cash value does not expire \r\n> Cannot be combined with any other deals or coupons \r\n> Limit 1 per visit \r\n> Limit 1 per person\r\n",
  "value": "335.0",
  "price": "300.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5744a5451aa7202682003c36"
  ],
  "expires_at": "",
  "user_id": "5750c6e6e9330ce7aa00cf0f",
  "updated_at": 1479862888503,
  "created_at": 1465573253622,
  "locations": [
    {
      "_id": "575adf851aa7209619018252",
      "point": [
        -116.7874768,
        47.7391465
      ],
      "address": {
        "_id": "575adf851aa7209619018254",
        "country": "US",
        "street": "7397 N. Government Way",
        "suite": "",
        "city": "Coeur d'Alene ",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "575adf851aa7209619018251",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "575adf851aa72000070001d9",
    "attached_width": 499,
    "attached_height": 280,
    "attached_size": 69568,
    "attached_name": "Timsspecialcutmeats.jpg",
    "main": false,
    "updated_at": 1465573253622,
    "created_at": 1465573253622
  },
  "activated_at": 1465574395174,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "575ae564e9330c8237013b1c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5744a3e2e9330c3ef0003a3a",
  "title": "  Coeur d'Alene Freezer Meat Pack",
  "category_id": "4f7d215e74dbb02de3000609",
  "offer": "ONLY $310 for the ALL BEEF Coeur d'Alene Freezer Meat Pack | Normally $345\r\nTim's Special Cut Meats\r\n(208 772-3327\r\n\r\n > 4.5lbs  - Sirloin Steaks\r\n> 5lbs -  Rib Steak\r\n> 5lbs - T-Bone Steak\r\n> 8lbs - Boneless Beef Roast\r\n> 8lbs - Boneless Round Roast\r\n> 1.5lbs - New York Steak\r\n> 20lbs -  Regular Ground Beef",
  "terms": "> Must be used during regular business hours. \r\n> Must be used in 1 visit. \r\n> $310 Cash value does not expire \r\n> Cannot be combined with any other deals or coupons \r\n> Limit 1 per visit \r\n> Limit 1 per person",
  "value": "345.0",
  "price": "310.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5744a5451aa7202682003c36"
  ],
  "expires_at": "",
  "user_id": "5750c6e6e9330ce7aa00cf0f",
  "updated_at": 1477338274860,
  "created_at": 1465574756185,
  "locations": [
    {
      "_id": "575ae564e9330c8237013b1b",
      "point": [
        -116.7874768,
        47.7391465
      ],
      "address": {
        "_id": "575ae564e9330c8237013b1d",
        "country": "US",
        "street": "7397 N. Government Way",
        "suite": "",
        "city": "Coeur d'Alene ",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "575ae564e9330c8237013b1a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "575ae564e9330c000700017f",
    "attached_width": 510,
    "attached_height": 264,
    "attached_size": 55736,
    "attached_name": "Timsspecialcutmeats2.jpg",
    "main": false,
    "updated_at": 1465574756185,
    "created_at": 1465574756185
  },
  "activated_at": 1465574910549,
  "referral_code": "currentattractions",
  "deleted_at": 1485227705102
}
deals << {
  "_id": "576abb8a59a0df00e6001d88",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "https://marketpad.com/currentattractionsc9397ece7e87",
  "profile_id": "54f0d7aff02cbe2b66005858",
  "title": " Buy 1 Music Lesson, get 1 free.",
  "category_id": "57437c27e9330c5bc1000a5e",
  "offer": "Buy 1 Music Lesson, get 1 free, limited time offer\r\n\r\nKicho's Musical Instruction located in Spokane WA\r\n (509 991-5272",
  "terms": "> Limited time offer\r\n> Must contact seller via phone or email after purchase to schedule\r\n> Cannot be combined with any other deals or offers\r\n> 1 per visit\r\n> 1 per person",
  "value": "40.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "4fb5539508dea13806000219",
  "updated_at": 1478219011409,
  "created_at": 1466612618273,
  "locations": [
    {
      "_id": "576abb8a59a0df00e6001d87",
      "point": [
        -117.43471,
        47.673764
      ],
      "address": {
        "_id": "576abb8a59a0df00e6001d89",
        "country": "US",
        "street": "1513 W. NORA",
        "suite": "",
        "city": "SPOKANE",
        "region": "WA ",
        "postal_code": "99205"
      }
    }
  ],
  "image": {
    "_id": "576abb8a59a0df00e6001d86",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "576abb8a59a0df000800001f",
    "attached_width": 1212,
    "attached_height": 1212,
    "attached_size": 108405,
    "attached_name": "kichomarketelectricguitar.jpg",
    "main": false,
    "updated_at": 1466612618273,
    "created_at": 1466612618273
  },
  "activated_at": 1466612729472
}
deals << {
  "_id": "577151b8a4152f9b820036b9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "576d8197a4152f878e002a5c",
  "title": "1 Bag of LeJoyva5 Coffee (30 Individual Cups",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "ONLY $19.99 for 1 Bag of Le’JOYva Coffee (30 Individual Cups | Normally $29.99 - INCLUDES FREE SHIPPING\r\n\r\nFINALLY, an instant quality coffee to fall in love with!\r\n\r\nWe Coffee Lovers - love it our way! Our one little bag can satisfy the masses without an expensive coffee maker, anywhere you find yourself at home, the office, or camping the great outdoors, and for ONLY $.66 per cup!\r\n\r\nLOVE in a Instant Single Cup is here!  ENJOY & SAVE!\r\n\r\nTHE PICTURE SHOWS from left to right 8oz, 10oz, 12oz, 14oz.   Make it your way! \r\n8oz of water JITTERS BLAST!\r\n10oz of water STRONG YET SURVIVABLE!\r\n12oz of water MORE THE NORM!\r\n14oz of water WARM AND SMOOTH! ",
  "terms": "> MUST email seller shipping address.\r\n> Will be shipped within 7-10 business days from date shipping address is received.\r\n> Read over added ingredients prior to purchase.",
  "value": "29.99",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57714fb41aa720e682003d49"
  ],
  "expires_at": "",
  "user_id": "576d8197a4152f878e002a59",
  "updated_at": 1467068792525,
  "created_at": 1467044280755,
  "locations": [
    {
      "_id": "577151b7a4152f9b820036b8",
      "point": [
        -111.984923,
        40.729221
      ],
      "address": {
        "_id": "577151b8a4152f9b820036ba",
        "country": "US",
        "street": "1890 S 3850 W",
        "suite": "",
        "city": "Salt Lake City",
        "region": "UT",
        "postal_code": "84104"
      }
    }
  ],
  "image": {
    "_id": "577151b7a4152f9b820036b7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577151b7a4152f0007000067",
    "attached_width": 5312,
    "attached_height": 2988,
    "attached_size": 4474184,
    "attached_name": "20160625_110439.jpg",
    "main": false,
    "updated_at": 1467044280755,
    "created_at": 1467044280755
  },
  "activated_at": 1467044283748,
  "referral_code": ""
}
deals << {
  "_id": "577152e359ffa80645003df7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "576d8197a4152f878e002a5c",
  "title": "1 Case of LeJoyva5 Coffee (36 Bags",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "ONLY $467.64 for 1 Case of Le’JOYva Coffee (36 Bags - 30 cups to a bag | Normally $935.64 - INCLUDES FREE SHIPPING \r\n\r\nFINALLY, an instant quality coffee to fall in love with!\r\n\r\nWe Coffee Lovers - love it our way! Our one little bag can satisfy the masses without an expensive coffee maker, anywhere you find yourself at home, the office, or camping the great outdoors, and for ONLY $.43 per cup!\r\n\r\nLOVE in a Instant Single Cup is here! ENJOY & SAVE!\r\n\r\nPictures show from Left to right 8oz, 10oz, 12oz, 14oz. Make it your way! \r\n8oz of water Jitters Blast! \r\n10oz of water Strong Yet Survivable! \r\n12oz of water More The Norm!\r\n14oz of water Warm & Smooth!",
  "terms": "> MUST email seller shipping address.\r\n> Will be shipped within 7-10 business days from date shipping address is received.\r\n> Read over added ingredients prior to purchase.",
  "value": "1044.0",
  "price": "467.64",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57714fb41aa720e682003d49"
  ],
  "expires_at": "",
  "user_id": "576d8197a4152f878e002a59",
  "updated_at": 1467068755147,
  "created_at": 1467044579937,
  "locations": [
    {
      "_id": "577152e359ffa80645003df6",
      "point": [
        -111.984923,
        40.729221
      ],
      "address": {
        "_id": "577152e359ffa80645003df8",
        "country": "US",
        "street": "1890 S 3850 W",
        "suite": "",
        "city": "Salt Lake City",
        "region": "UT",
        "postal_code": "84104"
      }
    }
  ],
  "image": {
    "_id": "577152e359ffa80645003df5",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577152e359ffa80006000085",
    "attached_width": 5312,
    "attached_height": 2988,
    "attached_size": 4507051,
    "attached_name": "20160625_110455.jpg",
    "main": false,
    "updated_at": 1467044579936,
    "created_at": 1467044579936
  },
  "activated_at": 1467044583708,
  "referral_code": ""
}
deals << {
  "_id": "577bedbb59ffa850cd0065e3",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5762e1fe59ffa8d88f000579",
  "title": "Shellac Manicure ",
  "category_id": "4f7d215a74dbb02de3000434",
  "offer": "ONLY $30 for a Shellac Manicure | Normally $40\r\n\r\nNails By Susie Q \r\n(208 699-0616 \r\nLocated inside Hawaiian Sun Salon and Spa in Dalton Gardens, ID. \r\n\r\n> All natural, no chemicals, it’s all about healthy",
  "terms": " > Must redeem voucher in person\r\n > Voucher must be redeemed before expiration date",
  "value": "40.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5762e67dce45606b3c0004e9"
  ],
  "expires_at": 1485936000000,
  "user_id": "5762e1fe59ffa8d88f000576",
  "updated_at": 1485229074318,
  "created_at": 1467739579954,
  "locations": [
    {
      "_id": "577bedbb59ffa850cd0065e2",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bedbb59ffa850cd0065e4",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bedbb59ffa850cd0065e1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bedbb59ffa800060000be",
    "attached_width": 837,
    "attached_height": 959,
    "attached_size": 112111,
    "attached_name": "shellac.jpg",
    "main": false,
    "updated_at": 1467739579954,
    "created_at": 1467739579954
  },
  "activated_at": 1467739583033,
  "referral_code": "",
  "deleted_at": 1485229081049
}
deals << {
  "_id": "577bee6859ffa82ca00065f3",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5762e1fe59ffa8d88f000579",
  "title": "Manicure & Pedicure",
  "category_id": "4f7d215a74dbb02de3000434",
  "offer": "ONLY $45 for a Manicure & Pedicure Package | Normally $65 \r\nNails By Susie Q\r\n(208 699-0616\r\nLocated inside Hawaiian Sun in Dalton Gardens, ID.\r\n\r\n > Manicures and Pedicures by Susie Q at Hawaiian Sun Salon and Spa.\r\n > All natural, no chemicals, it’s all about healthy.\r\n",
  "terms": "> Must redeem voucher in person.\r\n> Voucher must be redeemed before expiration date.",
  "value": "65.0",
  "price": "45.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5762e67dce45606b3c0004e9"
  ],
  "expires_at": "",
  "user_id": "5762e1fe59ffa8d88f000576",
  "updated_at": 1480120100044,
  "created_at": 1467739752963,
  "locations": [
    {
      "_id": "577bee6859ffa82ca00065f2",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bee6859ffa82ca00065f4",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bee6859ffa82ca00065f1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bee6859ffa800060000c0",
    "attached_width": 835,
    "attached_height": 526,
    "attached_size": 49102,
    "attached_name": "manipedi.JPG",
    "main": false,
    "updated_at": 1467739752962,
    "created_at": 1467739752962
  },
  "activated_at": 1467739756270,
  "referral_code": "",
  "deleted_at": 1485229085619
}
deals << {
  "_id": "577beec6a4152f7a6d005b2c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5762e1fe59ffa8d88f000579",
  "title": "Manicure ",
  "category_id": "4f7d215a74dbb02de3000434",
  "offer": " ONLY $20 for a Manicure | Normally $25\r\n\r\nNails By Susie Q \r\n(208 699-0616 \r\nLocated inside Hawaiian Sun in Dalton Gardens, ID.\r\n\r\n> All natural, no chemicals, it’s all about healthy.",
  "terms": "> Must redeem voucher in person.\r\n> Voucher must be redeemed before expiration date.",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5762e67dce45606b3c0004e9"
  ],
  "expires_at": "",
  "user_id": "5762e1fe59ffa8d88f000576",
  "updated_at": 1480120087884,
  "created_at": 1467739846958,
  "locations": [
    {
      "_id": "577beec6a4152f7a6d005b2b",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577beec6a4152f7a6d005b2d",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577beec6a4152f7a6d005b2a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577beec6a4152f0007000094",
    "attached_width": 2048,
    "attached_height": 1367,
    "attached_size": 201954,
    "attached_name": "nails1.jpg",
    "main": false,
    "updated_at": 1467739846957,
    "created_at": 1467739846957
  },
  "activated_at": 1467739849442,
  "referral_code": "",
  "deleted_at": 1485229089521
}
deals << {
  "_id": "577bef2059ffa8649a0065fa",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5762e1fe59ffa8d88f000579",
  "title": "Pedicure",
  "category_id": "4f7d215a74dbb02de3000434",
  "offer": "ONLY $30 for a Pedicure | Normally $40\r\n\r\nNails By Susie Q \r\n(208 699-0616 \r\nLocated inside Hawaiian Sun in Dalton Gardens, ID\r\n\r\n> All natural, no chemicals, it’s all about healthy.",
  "terms": " > Must redeem voucher in person.\r\n > Voucher must be redeemed before expiration date.",
  "value": "40.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5762e67dce45606b3c0004e9"
  ],
  "expires_at": "",
  "user_id": "5762e1fe59ffa8d88f000576",
  "updated_at": 1480120073239,
  "created_at": 1467739936958,
  "locations": [
    {
      "_id": "577bef2059ffa8649a0065f9",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bef2059ffa8649a0065fb",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1467739939488,
  "image": {
    "_id": "577e950059a0dfefce006440",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577e950059a0df000800005f",
    "attached_width": 665,
    "attached_height": 511,
    "attached_size": 55023,
    "attached_name": "pedicure.JPG",
    "main": false,
    "updated_at": 1467913472524,
    "created_at": 1467913472524
  },
  "referral_code": "",
  "deleted_at": 1485229092650
}
deals << {
  "_id": "577bf694a4152f2218005b47",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "Unlimited Tanning and Sauna",
  "category_id": "4f7d215974dbb02de30003cb",
  "offer": " ONLY $200 for Unlimited use of ALL tanning beds and sauna | Normally $390",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "390.0",
  "price": "200.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1472626800000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1467925302428,
  "created_at": 1467741844197,
  "locations": [
    {
      "_id": "577bf694a4152f2218005b46",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bf694a4152f2218005b48",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bf694a4152f2218005b45",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bf694a4152f0007000096",
    "attached_width": 588,
    "attached_height": 587,
    "attached_size": 49497,
    "attached_name": "tanning.JPG",
    "main": false,
    "updated_at": 1467741844197,
    "created_at": 1467741844197
  },
  "activated_at": 1467741847586,
  "referral_code": "currentattractions",
  "deleted_at": 1485228894416
}
deals << {
  "_id": "577bf70e59ffa83ff000662b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "1 Brazilian Wax",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "ONLY $40 for 1 Brazilian Wax | Normally $50\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "50.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1485228916451,
  "created_at": 1467741966904,
  "locations": [
    {
      "_id": "577bf70e59ffa83ff000662a",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bf70e59ffa83ff000662c",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bf70e59ffa83ff0006629",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bf70e59ffa800060000c6",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 88899,
    "attached_name": "brazilianwax1.jpg",
    "main": false,
    "updated_at": 1467741966904,
    "created_at": 1467741966904
  },
  "activated_at": 1467741969569,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "577bf75aa4152f95b1005b51",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "3 Brazilian Waxes",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "ONLY $110 for 3 Brazilian Waxes | Normally $135\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "135.0",
  "price": "110.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1485228926405,
  "created_at": 1467742042353,
  "locations": [
    {
      "_id": "577bf75aa4152f95b1005b50",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bf75aa4152f95b1005b52",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bf75aa4152f95b1005b4f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bf75aa4152f000700009a",
    "attached_width": 876,
    "attached_height": 493,
    "attached_size": 32397,
    "attached_name": "brazilianwax2.jpg",
    "main": false,
    "updated_at": 1467742042353,
    "created_at": 1467742042353
  },
  "activated_at": 1467742177384,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "577bf8e64811f5bf680059a3",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "1 Microdermabrasion Session",
  "category_id": "4f7d216574dbb02de300093e",
  "offer": "ONLY $60 for 1 Microdermabrasion Session | Normally $100\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "100.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1485228936440,
  "created_at": 1467742438204,
  "locations": [
    {
      "_id": "577bf8e64811f5bf680059a2",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bf8e64811f5bf680059a4",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bf8e64811f5bf680059a1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bf8e64811f50007000068",
    "attached_width": 730,
    "attached_height": 581,
    "attached_size": 41597,
    "attached_name": "micro3.JPG",
    "main": false,
    "updated_at": 1467742438204,
    "created_at": 1467742438204
  },
  "activated_at": 1467742441411,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "577bf9224811f53f380059a7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "3 Microdermabrasion Sessions",
  "category_id": "4f7d216574dbb02de300093e",
  "offer": "ONLY $170 for 3 Microdermabrasion Sessions | Normally $250\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "250.0",
  "price": "170.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1485228948524,
  "created_at": 1467742498814,
  "locations": [
    {
      "_id": "577bf9224811f53f380059a6",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bf9224811f53f380059a8",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bf9224811f53f380059a5",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bf9224811f5000700006a",
    "attached_width": 800,
    "attached_height": 533,
    "attached_size": 70267,
    "attached_name": "micro2.jpg",
    "main": false,
    "updated_at": 1467742498814,
    "created_at": 1467742498814
  },
  "activated_at": 1467742501598,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "577bf9681aa7209e90006531",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "6 Microdermabrasion Sessions",
  "category_id": "4f7d216574dbb02de300093e",
  "offer": "ONLY $320 for 6 Microdermabrasion Sessions | Normally $500\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "500.0",
  "price": "320.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1485936000000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1485228960411,
  "created_at": 1467742568999,
  "locations": [
    {
      "_id": "577bf9681aa7209e90006530",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "577bf9681aa7209e90006532",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "577bf974a4152fc0d7005b59",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "577bf974a4152f000700009c",
    "attached_width": 657,
    "attached_height": 511,
    "attached_size": 82322,
    "attached_name": "micro1.jpg",
    "main": false,
    "updated_at": 1467742580668,
    "created_at": 1467742580668
  },
  "activated_at": 1467742585991,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5786892cce4560b776009124",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "54ed003cf02cbee3b500309d",
  "title": "$1 OFF ANY Flatbread Pizza",
  "category_id": "4f7d216574dbb02de3000979",
  "offer": "$1 OFF ANY Flatbread Pizza",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> Only available during regular Kitchen hours. Thu-Sat 11am-11pm   Sun-Wed 11am-10pm\r\n> 1 per customer per visit.  \r\n> Entire amount must be redeemed in 1 visit.\r\n> Made with ingredients from local companies\r\n",
  "value": "8.0",
  "price": "7.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54f74a09f02cbe12b600101a"
  ],
  "expires_at": "",
  "user_id": "5785181ca4152f9042008de6",
  "updated_at": 1485228009823,
  "created_at": 1468434732913,
  "locations": [
    {
      "_id": "5786892cce4560b776009123",
      "point": [
        -117.0128559479492,
        47.74871308718497
      ],
      "address": {
        "_id": "5786892cce4560b776009125",
        "country": "US",
        "street": "26433 Hwy 53,",
        "suite": "",
        "city": "Hauser Junction,",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5786892cce4560b776009122",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5786892cce4560000700007c",
    "attached_width": 1280,
    "attached_height": 853,
    "attached_size": 330920,
    "attached_name": "CurleysPizza.jpg",
    "main": false,
    "updated_at": 1468434732912,
    "created_at": 1468434732912
  },
  "activated_at": 1468460115069,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "578d6c944811f57b0f000080",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ed003cf02cbee3b500309d",
  "title": "$20 for $25 worth of food",
  "category_id": "4f7d216574dbb02de3000979",
  "offer": "$20 for $25 worth of food\r\n\r\nCurley's located in Hauser ID\r\n(208 773-5816\r\ncurleyshauserjunction@msn.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> Only available during regular Kitchen hours. Thu-Sat 11am-11pm Sun-Wed 11am-10pm \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Voucher for food and non Alcoholic Beverages only\r\n",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54f74a09f02cbe12b600101a"
  ],
  "expires_at": "",
  "user_id": "5785181ca4152f9042008de6",
  "updated_at": 1477342511537,
  "created_at": 1468886164007,
  "locations": [
    {
      "_id": "578d6c934811f57b0f00007f",
      "point": [
        -117.0128559479492,
        47.74871308718497
      ],
      "address": {
        "_id": "578d6c944811f57b0f000081",
        "country": "US",
        "street": "26433 Hwy 53,",
        "suite": "",
        "city": "Hauser Junction,",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "578d6d6c59ffa8908f00007d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "578d6d6c59ffa80008000015",
    "attached_width": 500,
    "attached_height": 333,
    "attached_size": 150424,
    "attached_name": "CurleysWings.jpg",
    "main": false,
    "updated_at": 1468886380985,
    "created_at": 1468886380985
  },
  "activated_at": 1468887204644,
  "referral_code": "currentattractions",
  "deleted_at": 1485227977312
}
deals << {
  "_id": "57928de61aa7204fd8000e8d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1hr of Laser Tag or Nerf Battle for 2 ",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $15 for 1 hour of Laser Tag or Nerf Battles for 2 | Normally $27.98\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased",
  "value": "27.98",
  "price": "15.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1485936000000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1485228640984,
  "created_at": 1469222374770,
  "locations": [
    {
      "_id": "57928de61aa7204fd8000e8c",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "57928de61aa7204fd8000e8e",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57928de61aa7204fd8000e8b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57928de61aa7200008000022",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 57640,
    "attached_name": "deal1.jpg",
    "main": false,
    "updated_at": 1469222374769,
    "created_at": 1469222374769
  },
  "activated_at": 1469222377522,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "57928e7b59ffa82428000e88",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1 hour Laser Tag or Nerf Battles for 4",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $27 for 1 hour of Laser Tag or Nerf Battles for 4 | Normally $55.96\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "55.96",
  "price": "27.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1485936000000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1485228658655,
  "created_at": 1469222523129,
  "locations": [
    {
      "_id": "57928e7b59ffa82428000e87",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "57928e7b59ffa82428000e89",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57928e7b59ffa82428000e86",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57928e7b59ffa80008000025",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 101129,
    "attached_name": "deal3.jpg",
    "main": false,
    "updated_at": 1469222523128,
    "created_at": 1469222523128
  },
  "activated_at": 1469222526354,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "57928faea4152f3e6e000e93",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1 Month Unlimited Nerf Battle Pass",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $15 for a 1 Month Unlimited Nerf Battle Membership Pass for 1 Person | Normally $29\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "29.0",
  "price": "15.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1485936000000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1485228669097,
  "created_at": 1469222830589,
  "locations": [
    {
      "_id": "57928faea4152f3e6e000e92",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "57928faea4152f3e6e000e94",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57928faea4152f3e6e000e91",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57928faea4152f000e000018",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 113313,
    "attached_name": "deal4.jpg",
    "main": false,
    "updated_at": 1469222830588,
    "created_at": 1469222830588
  },
  "activated_at": 1469222833047,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5792906da4152f737d000e9d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1  Month Unlimited Laser Tag Pass",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $29 for a 1 Month Unlimited Laser Tag Membership Pass for 1 Person | Normally $59\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "59.0",
  "price": "29.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1485936000000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1485228679531,
  "created_at": 1469223021299,
  "locations": [
    {
      "_id": "5792906da4152f737d000e9c",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "5792906da4152f737d000e9e",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5792906da4152f737d000e9b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5792906da4152f000e00001a",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 96190,
    "attached_name": "deal5.jpg",
    "main": false,
    "updated_at": 1469223021298,
    "created_at": 1469223021298
  },
  "activated_at": 1469223024491,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "579290f7a4152f62da000ea2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1 Month ALL Access Membership Pass",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $34 for a 1 Month ALL Access Membership Pass for 1 Person | Normally $69\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!\r\n",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "69.0",
  "price": "34.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1485936000000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1485228694015,
  "created_at": 1469223159770,
  "locations": [
    {
      "_id": "579290f7a4152f62da000ea1",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "579290f7a4152f62da000ea3",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "579290f7a4152f62da000ea0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "579290f7a4152f000e00001c",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 79845,
    "attached_name": "deal6.jpg",
    "main": false,
    "updated_at": 1469223159770,
    "created_at": 1469223159770
  },
  "activated_at": 1469223163817,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5797b65a59ffa8dd7d002373",
  "active": false,
  "vouchers_count": 1,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "1 Bag of LeJoyva5 Coffee (30 Individual Cups",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "ONLY $14.99 for 1 Bag of Le’JOYva Coffee (30 Individual Cups | Normally $29.99 - INCLUDES FREE SHIPPING\r\n\r\nFINALLY, an instant coffee to fall in love with!\r\n\r\nWe Coffee Lovers - love it our way! Our one little bag can satisfy the masses without an expensive coffee maker, anywhere you find yourself at home, the office, or camping the great outdoors, and for ONLY $.43 per cup!\r\n\r\nFrom Left to right 8oz, 10oz, 12oz, 14oz. \r\n\r\n**Make it your way! **\r\n8oz of water Jitters Blast! \r\n10oz of water Strong Yet Survivable! \r\n12oz of water More The Norm!\r\n14oz of water Warm & Smooth!",
  "terms": "> MUST email seller shipping address.\r\n> Will be shipped within 7-10 business days from date shipping address is received.\r\n> Read over added ingredients prior to purchase.",
  "value": "29.99",
  "price": "14.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1470677970963,
  "created_at": 1469560410249,
  "locations": [
    {
      "_id": "5797b65959ffa8dd7d002372",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "5797b65a59ffa8dd7d002374",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5797b65959ffa8dd7d002371",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5797b65959ffa80008000035",
    "attached_width": 5312,
    "attached_height": 2988,
    "attached_size": 4474184,
    "attached_name": "20160625_110439.jpg",
    "main": false,
    "updated_at": 1469560410249,
    "created_at": 1469560410249
  },
  "activated_at": 1469560413145
}
deals << {
  "_id": "5797bbb84811f52651002382",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": "1 Case of LeJoyva5 Coffee (36 Bags",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "ONLY $467.64 for 1 Case of Le’JOYva Coffee (36 Bags - 30 cups to a bag | Normally $935.64 - INCLUDES FREE SHIPPING \r\n\r\nFINALLY, an instant coffee to fall in love with!\r\n\r\nWe Coffee Lovers - love it our way! Our one little bag can satisfy the masses without an expensive coffee maker, anywhere you find yourself at home, the office, or camping the great outdoors, and for ONLY $.43 per cup!\r\n\r\nFrom Left to right 8oz, 10oz, 12oz, 14oz. \r\n\r\n**Make it your way! **\r\n8oz of water Jitters Blast! \r\n10oz of water Strong Yet Survivable! \r\n12oz of water More The Norm!\r\n14oz of water Warm & Smooth!",
  "terms": "> MUST email seller shipping address.\r\n> Will be shipped within 7-10 business days from date shipping address is received.\r\n> Read over added ingredients prior to purchase.",
  "value": "1079.64",
  "price": "467.64",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1469561788485,
  "created_at": 1469561784639,
  "locations": [
    {
      "_id": "5797bbb84811f52651002381",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "5797bbb84811f52651002383",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1469561788478,
  "image": {
    "_id": "5797bc42ce45609e1d0023fb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5797bc42ce45600007000052",
    "attached_width": 5312,
    "attached_height": 2988,
    "attached_size": 4507051,
    "attached_name": "20160625_110455.jpg",
    "main": false,
    "updated_at": 1469561922924,
    "created_at": 1469561922924
  }
}
deals << {
  "_id": "5798177059a0dfbbb100272a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "title": "$24 for $30 Worth of Beverages",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "ONLY $24 for $30 worth of Beverages\r\n\r\nEl Patio Bar & Grill located in Post Falls ID \r\n(208 773-2611 \r\nelpatio6902@gmail.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Must be over 21 to purchase alcoholic beverages.",
  "value": "30.0",
  "price": "24.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "578ed303a4152f53910004bd"
  ],
  "expires_at": 1485936000000,
  "user_id": "578ed12a59ffa8af060004d7",
  "updated_at": 1485228519192,
  "created_at": 1469585264293,
  "locations": [
    {
      "_id": "5798177059a0dfbbb1002729",
      "point": [
        -117.0378159,
        47.70392380000001
      ],
      "address": {
        "_id": "5798177059a0dfbbb100272b",
        "country": "US",
        "street": "6902 W. Seltice Wy.",
        "suite": "",
        "city": "Post Falls",
        "region": "Id",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1470075191535,
  "image": {
    "_id": "579f9261ce4560a56e001368",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "579f9261ce45600006000017",
    "attached_width": 4368,
    "attached_height": 2912,
    "attached_size": 1911318,
    "attached_name": "beverages2.jpg",
    "main": false,
    "updated_at": 1470075489487,
    "created_at": 1470075489487
  },
  "referral_code": ""
}
deals << {
  "_id": "5798184559a0df9085002731",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "title": "$20 for $25 Worth of Beverages",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "ONLY $20 for $25 worth of Beverages\r\n\r\nEl Patio Bar & Grill located in Post Falls ID \r\n(208 773-2611 \r\nelpatio6902@gmail.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Must be over 21 to purchase alcoholic beverages.\r\n",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "578ed303a4152f53910004bd"
  ],
  "expires_at": 1485936000000,
  "user_id": "578ed12a59ffa8af060004d7",
  "updated_at": 1485228538698,
  "created_at": 1469585477419,
  "locations": [
    {
      "_id": "5798184559a0df9085002730",
      "point": [
        -117.0378159,
        47.70392380000001
      ],
      "address": {
        "_id": "5798184559a0df9085002732",
        "country": "US",
        "street": "6902 W. Seltice Wy.",
        "suite": "",
        "city": "Post Falls",
        "region": "Id",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1470075193190,
  "image": {
    "_id": "579f922e59ffa820ab00131f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "579f922e59ffa80007000015",
    "attached_width": 626,
    "attached_height": 442,
    "attached_size": 47622,
    "attached_name": "beverages1.jpg",
    "main": false,
    "updated_at": 1470075438292,
    "created_at": 1470075438292
  },
  "referral_code": ""
}
deals << {
  "_id": "57981909ce4560429c002751",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "title": "$12 for $15 Worth of Beverages",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "ONLY $12 for $15 worth of Beverages\r\n\r\nEl Patio Bar & Grill located in Post Falls ID\r\n(208 773-2611\r\nelpatio6902@gmail.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Must be over 21 to purchase alcoholic beverages.",
  "value": "15.0",
  "price": "12.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "578ed303a4152f53910004bd"
  ],
  "expires_at": 1485936000000,
  "user_id": "578ed12a59ffa8af060004d7",
  "updated_at": 1485228560133,
  "created_at": 1469585673791,
  "locations": [
    {
      "_id": "57981909ce4560429c002750",
      "point": [
        -117.0378159,
        47.70392380000001
      ],
      "address": {
        "_id": "57981909ce4560429c002752",
        "country": "US",
        "street": "6902 W. Seltice Wy.",
        "suite": "",
        "city": "Post Falls",
        "region": "Id",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1470075194582,
  "image": {
    "_id": "579f92f459a0df25d500137e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "579f92f459a0df000600000d",
    "attached_width": 2643,
    "attached_height": 2135,
    "attached_size": 542499,
    "attached_name": "beverages3.jpg",
    "main": false,
    "updated_at": 1470075636222,
    "created_at": 1470075636222
  },
  "referral_code": ""
}
deals << {
  "_id": "5798282859a0dfa402002779",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5735fcc01aa7205cc0003002",
  "title": "$80 for $100 Worth of Fabworks Products",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "ONLY $80 for $100 worth of Fabworks products",
  "terms": "> Must contact seller via phone or email after purchasing voucher to place order \r\n> Allow 3 weeks from order for completion \r\n> Cannot combine with any other deals or coupons ",
  "value": "100.0",
  "price": "80.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57507c98e9330c6f4a00cbd3"
  ],
  "expires_at": "",
  "user_id": "5735fcc01aa7205cc0002fff",
  "updated_at": 1473191147240,
  "created_at": 1469589544468,
  "locations": [
    {
      "_id": "5798282859a0dfa402002778",
      "point": [
        -116.858399,
        47.704514
      ],
      "address": {
        "_id": "5798282859a0dfa40200277a",
        "country": "US",
        "street": "6604 E Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5798282859a0dfa402002777",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5798282859a0df0008000030",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 54813,
    "attached_name": "PatriotFabworksdealphoto2.jpg",
    "main": false,
    "updated_at": 1469589544468,
    "created_at": 1469589544468
  },
  "activated_at": 1469596030822
}
deals << {
  "_id": "579828bc4811f561b9002730",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5735fcc01aa7205cc0003002",
  "title": "$40 for $50 Worth of Fabworks Products",
  "category_id": "4f7d216074dbb02de300072f",
  "offer": "Only $40 for $50 worth of Fabworks Products",
  "terms": "> Must contact seller via phone or email after purchasing voucher to place order \r\n> Allow 3 weeks from order for completion \r\n> Cannot combine with any other deals or coupons ",
  "value": "50.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57507c98e9330c6f4a00cbd3"
  ],
  "expires_at": "",
  "user_id": "5735fcc01aa7205cc0002fff",
  "updated_at": 1473191149196,
  "created_at": 1469589692682,
  "locations": [
    {
      "_id": "579828bc4811f561b900272f",
      "point": [
        -116.858399,
        47.704514
      ],
      "address": {
        "_id": "579828bc4811f561b9002731",
        "country": "US",
        "street": "6604 E Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "579828bc4811f561b900272e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "579828bc4811f50007000054",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 108362,
    "attached_name": "PatriotFabworksdealphoto.jpg",
    "main": false,
    "updated_at": 1469589692682,
    "created_at": 1469589692682
  },
  "activated_at": 1469596010884
}
deals << {
  "_id": "57a8c47e4811f5620c000914",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "title": " 1 Bag of LeJoyva5 Coffee (30 Individual Cups",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "1 Bag of Le’JOYva Coffee (30 Individual Cups | Normally $29.99 - INCLUDES FREE SHIPPING\r\n\r\nMarketPad\r\n(208 953-1590\r\nmarketing@marketpad.net\r\n\r\nFINALLY, an instant coffee to fall in love with!\r\n\r\nWe Coffee Lovers - love it our way! Our one little bag can satisfy the masses without an expensive coffee maker, anywhere you find yourself at home, the office, or camping the great outdoors, and for ONLY $.49 per cup!\r\n\r\n**Make it your way! **\r\n8oz of water Jitters\r\n10oz of water Survivable! \r\n12oz of water The Norm!\r\n14oz of water Smooth!",
  "terms": "> MUST email seller shipping address.\r\n> Will be shipped within 7-10 business days from date shipping address is received.\r\n> Read over added ingredients prior to purchase.",
  "value": "29.99",
  "price": "14.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56bcf17aa10a1a283f0017ef"
  ],
  "expires_at": "",
  "user_id": "561e9b30f02cbe7a1300656c",
  "updated_at": 1480349803394,
  "created_at": 1470678142182,
  "locations": [
    {
      "_id": "57a8c47e4811f5620c000913",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "57a8c47e4811f5620c000915",
        "country": "US",
        "street": "296 W Sunset Ave",
        "suite": "11",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57a8c47e4811f5620c000912",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57a8c47e4811f50007000001",
    "attached_width": 600,
    "attached_height": 338,
    "attached_size": 94113,
    "attached_name": "1baglejoyva.jpg",
    "main": false,
    "updated_at": 1470678142181,
    "created_at": 1470678142181
  },
  "activated_at": 1470678145409
}
deals << {
  "_id": "57b210824811f5bb06001814",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Permanent Makeup (Eyebrows",
  "category_id": "4f7d215974dbb02de3000429",
  "offer": "ONLY $100 for Permanent Make Up for Eyebrows | Normally $300\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "300.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1485936000000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1485228295504,
  "created_at": 1471287426456,
  "locations": [
    {
      "_id": "57b210824811f5bb06001813",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "57b210824811f5bb06001815",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1471287487575,
  "image": {
    "_id": "57b210e659a0dfd1fc0017e3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b210e659a0df000600000d",
    "attached_width": 477,
    "attached_height": 287,
    "attached_size": 26348,
    "attached_name": "cdaesthetics.JPG",
    "main": false,
    "updated_at": 1471287526666,
    "created_at": 1471287526666
  },
  "referral_code": ""
}
deals << {
  "_id": "57b2126059a0dfac750017eb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Permanent Makeup (Eyeliner: Both top & bottom",
  "category_id": "4f7d215974dbb02de3000429",
  "offer": "ONLY $100 for Permanent Makeup (Eyeliner: Both top & bottom\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "250.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1485936000000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1485228335086,
  "created_at": 1471287904125,
  "locations": [
    {
      "_id": "57b2126059a0dfac750017ea",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "57b2126059a0dfac750017ec",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "57b2126059a0dfac750017e9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b2126059a0df000600000f",
    "attached_width": 482,
    "attached_height": 319,
    "attached_size": 31225,
    "attached_name": "cdaesthetics2.JPG",
    "main": false,
    "updated_at": 1471287904125,
    "created_at": 1471287904125
  },
  "activated_at": 1471287907240,
  "referral_code": ""
}
deals << {
  "_id": "57b2134d1aa720781c001826",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Permanent Makeup (Lip Liner",
  "category_id": "4f7d215974dbb02de3000429",
  "offer": "ONLY $100 for Permanent Makeup (Lip Liner | Normally $200\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "200.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1485936000000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1485228357989,
  "created_at": 1471288141822,
  "locations": [
    {
      "_id": "57b2134d1aa720781c001825",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "57b2134d1aa720781c001827",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1471288148109,
  "image": {
    "_id": "57b213db1aa7207c15001828",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b213db1aa720000600001d",
    "attached_width": 423,
    "attached_height": 278,
    "attached_size": 18556,
    "attached_name": "cdaesthetics4.JPG",
    "main": false,
    "updated_at": 1471288283736,
    "created_at": 1471288283736
  },
  "referral_code": ""
}
deals << {
  "_id": "57b215b6a4152f7b9100183d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "1 Chemical Peel",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "ONLY $39 for 1 Chemical Peel | Normally $100\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "100.0",
  "price": "39.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1485936000000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1485228370519,
  "created_at": 1471288758370,
  "locations": [
    {
      "_id": "57b215b6a4152f7b9100183c",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "57b215b6a4152f7b9100183e",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "57b215b6a4152f7b9100183b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b215b6a4152f000700000d",
    "attached_width": 488,
    "attached_height": 356,
    "attached_size": 30276,
    "attached_name": "chempeel.JPG",
    "main": false,
    "updated_at": 1471288758369,
    "created_at": 1471288758369
  },
  "activated_at": 1471288765222,
  "referral_code": ""
}
deals << {
  "_id": "57b216071aa7209be000182e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "2 Chemical Peels",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "ONLY $75 for 2 Chemical Peels | Normally $200\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.\r\n",
  "value": "200.0",
  "price": "75.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1485936000000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1485228391996,
  "created_at": 1471288839199,
  "locations": [
    {
      "_id": "57b216071aa7209be000182d",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "57b216071aa7209be000182f",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "activated_at": 1471288842745,
  "image": {
    "_id": "57b21626ce4560ab7e00182f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b21626ce45600006000009",
    "attached_width": 590,
    "attached_height": 334,
    "attached_size": 31201,
    "attached_name": "chempeel2.JPG",
    "main": false,
    "updated_at": 1471288870454,
    "created_at": 1471288870454
  },
  "referral_code": ""
}
deals << {
  "_id": "57b2168d59ffa8d53b0017fb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "1 Chemical Peel & 1 Microdermabrasion Peel",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "ONLY $99 for 1 Chemical Peel & 1 Microdermabrasion Peel | Normally $275\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID\r\n(208 661-7295\r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "275.0",
  "price": "99.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1485936000000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1485228409143,
  "created_at": 1471288973054,
  "locations": [
    {
      "_id": "57b2168d59ffa8d53b0017fa",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "57b2168d59ffa8d53b0017fc",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "57b2168d59ffa8d53b0017f9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b2168d59ffa80006000025",
    "attached_width": 461,
    "attached_height": 408,
    "attached_size": 28735,
    "attached_name": "chempeel4.JPG",
    "main": false,
    "updated_at": 1471288973053,
    "created_at": 1471288973053
  },
  "activated_at": 1471288980889,
  "referral_code": ""
}
deals << {
  "_id": "57b23ccbce4560ff8d0018a3",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56845c7994cacf755a000f04",
  "title": "1 Hot Dog (FREE Kettle Corn",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "ONLY $6.50 for 1 Hot Dog & FREE Kettle Corn | Normally $10.50\r\n\r\nFranko's Dog House located in Post Falls ID\r\n(208 964-5489\r\nfrankosdh@usa.com\r\n\r\n\r\nHot dog choices are: \r\n> German Shepard or Italian Hound\r\nKettle Corn choices are: \r\n> Huckleberry or Root Beer Float ",
  "terms": "> Not good with any other specials.\r\n> Amount paid never expires.\r\n> Promotional value expires 120 days from purchase.\r\n> Limit of 1 per person per visit.",
  "value": "10.5",
  "price": "6.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568f06e90ce944778b006257"
  ],
  "expires_at": 1485936000000,
  "user_id": "568dbbfc0ce944a99b003f41",
  "updated_at": 1485228158817,
  "created_at": 1471298763054,
  "locations": [
    {
      "_id": "57b23ccbce4560ff8d0018a2",
      "point": [
        -116.916788,
        47.7104917
      ],
      "address": {
        "_id": "57b23ccbce4560ff8d0018a4",
        "country": "US",
        "street": "1900 E. Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "activated_at": 1471298768667,
  "image": {
    "_id": "57b23e20ce45600c470018ab",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b23e20ce45600006000013",
    "attached_width": 1053,
    "attached_height": 724,
    "attached_size": 92571,
    "attached_name": "dog3.jpg",
    "main": false,
    "updated_at": 1471299104343,
    "created_at": 1471299104343
  },
  "referral_code": ""
}
deals << {
  "_id": "57b23fa0a4152f9e720018e9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56845c7994cacf755a000f04",
  "title": "1 Hot Dog (FREE Cup of Chili",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "ONLY $4.50 for 1 Hot Dog (FREE Cup of Chili | Normally 6.50\r\n\r\nFranko's Dog House located in Post Falls ID\r\n(208 964-5489\r\nfrankosdh@usa.com\r\n\r\nHot dog choices are: \r\n> Huckleberry Hound, Yorkie, Spanish Chihuahua or Dotson\t\r\n\r\nIncludes: Frito Lu Bowl (chili, cheese and onions in Frito bag",
  "terms": "> Not good with any other specials.\r\n> Amount paid never expires.\r\n> Promotional value expires 120 days from purchase.\r\n> Limit of 1 per person per visit.",
  "value": "6.5",
  "price": "4.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568f06e90ce944778b006257"
  ],
  "expires_at": 1485936000000,
  "user_id": "568dbbfc0ce944a99b003f41",
  "updated_at": 1485228174232,
  "created_at": 1471299488471,
  "locations": [
    {
      "_id": "57b23fa0a4152f9e720018e8",
      "point": [
        -116.916788,
        47.7104917
      ],
      "address": {
        "_id": "57b23fa0a4152f9e720018ea",
        "country": "US",
        "street": "1900 E. Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "57b23fa0a4152f9e720018e7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b23fa0a4152f0007000013",
    "attached_width": 522,
    "attached_height": 430,
    "attached_size": 34204,
    "attached_name": "dog2.JPG",
    "main": false,
    "updated_at": 1471299488471,
    "created_at": 1471299488471
  },
  "activated_at": 1471299491616,
  "referral_code": ""
}
deals << {
  "_id": "57b5ea6359ffa8632c00267c",
  "active": true,
  "vouchers_count": 2,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$13 for $20 Worth of Burgers and Food",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "ONLY $13 for $20 Worth of Food & Drinks!\r\n\r\nSchmidty's Burgers located in Coeur d'Alene ID\r\n(208 292-4545",
  "terms": "> Must be used during regular business hours.\r\n> Must be used in 1 visit. \r\n> $13 Cash value never expires",
  "value": "20.0",
  "price": "13.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "expires_at": "",
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1477341285267,
  "created_at": 1471539811573,
  "locations": [
    {
      "_id": "57b5ea6359ffa8632c00267b",
      "point": [
        -116.7809347,
        47.674579
      ],
      "address": {
        "_id": "57b5ea6359ffa8632c00267d",
        "country": "US",
        "street": "206 N. 4th St.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "57b5ea8d59a0df047b002621",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57b5ea8d59a0df000600001d",
    "attached_width": 600,
    "attached_height": 448,
    "attached_size": 47878,
    "attached_name": "schmidtys3 (1.jpg",
    "main": false,
    "updated_at": 1471539853476,
    "created_at": 1471539853476
  },
  "activated_at": 1471539861285,
  "referral_code": ""
}
deals << {
  "_id": "57bddfb859ffa88a860002cd",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "49ers Special (Pitcher of Domestic Beer & Wings",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "49ers Special Pitcher of Domestic  Beer & Wings ONLY $10 | Normally $18\r\n\r\nRivelle's River Grill located in Coeur d'Alene, ID\r\n(208 930-0381\r\nrivellescda@gmail.com\r\n",
  "terms": "> Must wear 49ers swag to redeem deal (hat, shirt, flag, etc.\r\n> Cannot be combined with any other deal or coupon\r\n> Can only be redeemed on day of 49ers game\r\n> Must bring in printed voucher to redeem\r\n> 1 per visit, 2 per table.",
  "value": "18.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "57336af61aa7203be5000ace"
  ],
  "expires_at": "",
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1477340534220,
  "created_at": 1472061368428,
  "locations": [
    {
      "_id": "57bddfb859ffa88a860002cc",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "57bddfb859ffa88a860002ce",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "57bddfb859ffa88a860002cb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57bddfb859ffa80007000009",
    "attached_width": 979,
    "attached_height": 610,
    "attached_size": 103909,
    "attached_name": "deal2.JPG",
    "main": false,
    "updated_at": 1472061368428,
    "created_at": 1472061368428
  },
  "activated_at": 1473199174680,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "57f44d99e262db354e01e168",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57f43623e262dbaf8a01e015",
  "title": "$20 Pre-Paid card for ONLY $15",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "SAVE $5 - $20 Pre-Paid card for ONLY $15\r\n\r\nAnchors Aweigh Coffee House located in Rathdrum, ID \r\n(208 262-9042 \r\nsilver12b@hotmail.com",
  "terms": "> Usage during hours of business. M-F 7A.M. to 4 P.M. or Sat. 7 to 3 P.M.\r\n> Can choose 2 $10 cards in place of 1 $20 card",
  "value": "20.0",
  "price": "15.0",
  "limit": 40,
  "limit_per_customer": 2,
  "listing_ids": [
    "57f43e4fe262dba5f601e06f"
  ],
  "expires_at": 1483257600000,
  "user_id": "57f4355be262dbb52c01dff8",
  "updated_at": 1477340176469,
  "created_at": 1475628441743,
  "locations": [
    {
      "_id": "57f44d99e262db354e01e167",
      "point": [
        -116.8874663,
        47.81844479999999
      ],
      "address": {
        "_id": "57f44d99e262db354e01e169",
        "country": "US",
        "street": "13785 Hwy 53",
        "suite": "4",
        "city": "Rathdrum",
        "region": "Id",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "57f44f97e262dbd39701e18f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57f44f97e262db000100025a",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 49650,
    "attached_name": "13407145_979517108814252_6141857557936661250_n.jpg",
    "main": false,
    "updated_at": 1475628951088,
    "created_at": 1475628951088
  },
  "activated_at": 1475629038074,
  "referral_code": "currentattractions",
  "deleted_at": 1485227899101
}
deals << {
  "_id": "57f44f59e262db2b0201e189",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57f43623e262dbaf8a01e015",
  "title": "SAVE $1.25 | Latte and a Treat",
  "category_id": "4f7d215e74dbb02de3000634",
  "offer": "SAVE $1.25 - Get a Flavored Latte and a Bagel or Cinnamon Roll \r\n\r\nAnchors Aweigh Coffee House located in Rathdrum, ID\r\n(208 262-9042\r\nsilver12b@hotmail.com",
  "terms": "",
  "value": "6.25",
  "price": "5.0",
  "limit": 40,
  "limit_per_customer": 5,
  "listing_ids": [
    "57f43e4fe262dba5f601e06f"
  ],
  "expires_at": 1483257600000,
  "user_id": "57f4355be262dbb52c01dff8",
  "updated_at": 1477340145765,
  "created_at": 1475628889355,
  "locations": [
    {
      "_id": "57f44f58e262db2b0201e188",
      "point": [
        -116.8874663,
        47.81844479999999
      ],
      "address": {
        "_id": "57f44f59e262db2b0201e18a",
        "country": "US",
        "street": "13785 Hwy 53",
        "suite": "4",
        "city": "Rathdrum",
        "region": "Id",
        "postal_code": "83858"
      }
    }
  ],
  "image": {
    "_id": "57f44f58e262db2b0201e187",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57f44f58e262db0001000257",
    "attached_width": 1152,
    "attached_height": 2048,
    "attached_size": 315801,
    "attached_name": "14567387_1058584714240824_6552425042104179314_o.jpg",
    "main": false,
    "updated_at": 1475628889355,
    "created_at": 1475628889355
  },
  "activated_at": 1475629114522,
  "referral_code": "currentattractions",
  "deleted_at": 1485227904559
}
deals << {
  "_id": "57fd8fec88682e529d008781",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "578443921aa7209c6700bdbc",
  "title": "ONLY $30 for $35 | Painting Experience",
  "category_id": "570d6ac3e9330c1184002192",
  "offer": "SAVE $5 -  $35 worth of classes or products for ONLY $30 | \r\n\r\n\"Paint. Drink. Have Fun.\" \r\n\r\nPinot's Palette CDA\r\n (208 930-4763\r\n\r\n> Value of voucher can be redeemed for services and/or products.\r\n\r\n",
  "terms": "> CDA location only\r\n> Must contact seller via phone to schedule class\r\n> Must be redeemed in 1 visit\r\n> Cannot be combined with any other deals or offers\r\n> 1 per customer\r\n> $30 value never expires\r\n> This offer is exclusive ONLY to the Coeur d'Alene location.\r\n> 208 930-4763",
  "value": "35.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5790037659a0df77270007ed"
  ],
  "expires_at": 1485417600000,
  "user_id": "579000024811f553df0007e3",
  "updated_at": 1485227810464,
  "created_at": 1476235244287,
  "locations": [
    {
      "_id": "57fd8fec88682e529d008780",
      "point": [
        -116.7805453,
        47.6795782
      ],
      "address": {
        "_id": "57fd8fec88682e529d008782",
        "country": "US",
        "street": "728 North 4th Street",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "57fd8fec88682e529d00877f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "57fd8fec88682e0001000023",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 81016,
    "attached_name": "Pinots&Palette11.jpg",
    "main": false,
    "updated_at": 1476235244286,
    "created_at": 1476235244286
  },
  "activated_at": 1476235407415,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5806b6f088682e4f29010436",
  "active": true,
  "vouchers_count": 0,
  "referral_code": "",
  "profile_id": "50b510829d8c211d6200005a",
  "title": "Free Windshield Chip Repair with every PDR Repair",
  "category_id": "4f7d215774dbb02de300031b",
  "offer": "Dent Viking with offer free rock chip repair on small chips and cracks (under an inch long and up to 3 repairs with every purchase of Paintless Dent Repair on the same vehicle*.  ",
  "terms": "Repairs must be quoted and scheduled within normal operating hours. ",
  "value": "35",
  "price": "0",
  "limit": "",
  "limit_per_customer": 1,
  "listing_ids": [
    "5806b4c888682e2d0c0102b6"
  ],
  "expires_at": 1508310000000,
  "user_id": "504651719d8c2124b2002cf6",
  "updated_at": 1476835075765,
  "created_at": 1476835056340,
  "locations": [
    {
      "_id": "5806b6f088682e4f29010435",
      "point": [
        -116.7864356,
        47.7750937
      ],
      "address": {
        "_id": "5806b6f088682e4f29010437",
        "country": "US",
        "street": "11358 N. Government Way",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "5806b6f088682e4f29010434",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5806b6f088682e0001000180",
    "attached_width": 850,
    "attached_height": 400,
    "attached_size": 216945,
    "attached_name": "mp6.jpg",
    "main": false,
    "updated_at": 1476835056340,
    "created_at": 1476835056340
  },
  "activated_at": 1476835075753,
  "deleted_at": 1476839483410
}
deals << {
  "_id": "58070bb188682e7bb1010a54",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5806bf9388682e9af60104e1",
  "title": "Full Color Weave w/haircut ",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "30% off full weave color service \r\n\r\nCreative Touch by Heidi\r\n(360 975-2836\r\n",
  "terms": "Includes lowlights and or highlights, additional color charges may apply",
  "value": "140.0",
  "price": "98.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "580707e788682e7b880109f6"
  ],
  "expires_at": 1546243200000,
  "user_id": "5806bd7e88682e8d780104c3",
  "updated_at": 1486067683827,
  "created_at": 1476856753981,
  "locations": [
    {
      "_id": "58070bb188682e7bb1010a53",
      "point": [
        -117.2827011,
        47.6864283
      ],
      "address": {
        "_id": "58070bb188682e7bb1010a55",
        "country": "US",
        "street": "3207 N Argonne Road",
        "suite": "",
        "city": "Spokane ",
        "region": "Wa",
        "postal_code": "99206"
      }
    }
  ],
  "image": {
    "_id": "58070bb188682e7bb1010a52",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58070bb188682e00010001ae",
    "attached_width": 750,
    "attached_height": 1334,
    "attached_size": 1652981,
    "attached_name": "IMG_0241.PNG",
    "main": false,
    "updated_at": 1476856753981,
    "created_at": 1476856753981
  },
  "activated_at": 1476856759961,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5807134388682ef9a8010b38",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5806bf9388682e9af60104e1",
  "title": "Women's Haircut and style",
  "category_id": "4f7d216574dbb02de3000936",
  "offer": "30% hair cut (includes style\r\n\r\nCreative Touch by Heidi\r\n(360 975-2836",
  "terms": "",
  "value": "45.0",
  "price": "31.5",
  "limit": "",
  "limit_per_customer": 10,
  "listing_ids": [
    "580707e788682e7b880109f6"
  ],
  "expires_at": 1546243200000,
  "user_id": "5806bd7e88682e8d780104c3",
  "updated_at": 1486067709116,
  "created_at": 1476858691462,
  "locations": [
    {
      "_id": "5807134388682ef9a8010b37",
      "point": [
        -117.2827011,
        47.6864283
      ],
      "address": {
        "_id": "5807134388682ef9a8010b39",
        "country": "US",
        "street": "3207 N Argonne Road",
        "suite": "",
        "city": "Spokane ",
        "region": "Wa",
        "postal_code": "99206"
      }
    }
  ],
  "image": {
    "_id": "5807134388682ef9a8010b36",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5807134388682e00010001d2",
    "attached_width": 100,
    "attached_height": 124,
    "attached_size": 3961,
    "attached_name": "14390877_1784067368538276_455896592489824768_n.jpg",
    "main": false,
    "updated_at": 1476858691462,
    "created_at": 1476858691462
  },
  "activated_at": 1476858698074,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5810ead288682ecfe00226bb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Long Sleeve Crew Neck Shirt - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$22.99 per shirt, normally $32.99 - Must buy pack of 12, all same color, sizes can vary.\r\n\r\ninfo@marketpad.net\r\n\r\n-Made from Lycra, the best material for flexibility and muscle enhancement \r\n-Enchances muscle power and performance\r\n-Great fit, long lasting\r\n-Lightweight and comfortable\r\n-Durable elastic waistband with drawstring\r\n-Flatlock stitch prevents rashes\r\n-Wicks moisture away from the skin while resisting odor\r\n-Blocks over 97% of UV rays from the sun\r\n-UPF rating of 50+\r\n",
  "terms": "Minimum order of 12.  \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net\r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "395.88",
  "price": "275.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477591740174,
  "created_at": 1477503698418,
  "locations": [
    {
      "_id": "5810ead288682ecfe00226ba",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810ead288682ecfe00226bc",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "581232c588682efe19025e32",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "581232c588682e00010003c7",
    "attached_width": 300,
    "attached_height": 305,
    "attached_size": 19505,
    "attached_name": "longslv-crew-comp-solid.jpg",
    "main": false,
    "updated_at": 1477587653806,
    "created_at": 1477587653806
  }
}
deals << {
  "_id": "5810ed2a88682eca5b02270c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Short Sleeve Crew Neck Shirt - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$19.99 per shirt, normally $28.99 each.  \r\n\r\ninfo@marketpad.net\r\n\r\n-Made from Lycra, the best material for flexibility and muscle enhancement \r\n-Enchances muscle power and performance\r\n-Great fit, long lasting\r\n-Lightweight and comfortable\r\n-Durable elastic waistband with drawstring\r\n-Flatlock stitch prevents rashes\r\n-Wicks moisture away from the skin while resisting odor\r\n-Blocks over 97% of UV rays from the sun\r\n-UPF rating of 50+",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "347.88",
  "price": "239.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477591884526,
  "created_at": 1477504298311,
  "locations": [
    {
      "_id": "5810ed2a88682eca5b02270b",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810ed2a88682eca5b02270d",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5812334b88682ef760025e94",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5812334b88682e00010003cd",
    "attached_width": 811,
    "attached_height": 1219,
    "attached_size": 603346,
    "attached_name": "QSpictures 027.jpg",
    "main": false,
    "updated_at": 1477587787872,
    "created_at": 1477587787872
  }
}
deals << {
  "_id": "5810f06088682e28ca0227d7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Long Sliding Shorts - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$25.99 per pair of shorts, normally $32.99\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon blend fabric creates stronger compression on your powerhouse, legs, and buttocks \r\n-Extra padding to help protect athlete\r\n-Lycra is the best material for flexibility and muscle enhancement \r\n-Light and comfortable \r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor \r\n-Maximum comfort in hot or cold conditions.",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "395.88",
  "price": "311.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477595947370,
  "created_at": 1477505120083,
  "locations": [
    {
      "_id": "5810f06088682e28ca0227d6",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810f06088682e28ca0227d8",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5812366a88682ea16b025f39",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5812366a88682e00010003d1",
    "attached_width": 310,
    "attached_height": 333,
    "attached_size": 15716,
    "attached_name": "sliding-shorts-long-2.jpg",
    "main": false,
    "updated_at": 1477588586679,
    "created_at": 1477588586679
  }
}
deals << {
  "_id": "5810f0d188682e2cd3022832",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Short Sliding Shorts - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$25.99 per pair of shorts, normally $32.99 each\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon blend fabric creates stronger compression on your powerhouse, legs, and buttocks \r\n-Extra padding to help protect athlete\r\n-Lycra is the best material for flexibility and muscle enhancement \r\n-Light and comfortable \r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor \r\n-Maximum comfort in hot or cold conditions",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "359.88",
  "price": "287.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477595976199,
  "created_at": 1477505233172,
  "locations": [
    {
      "_id": "5810f0d188682e2cd3022831",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810f0d188682e2cd3022833",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5812368488682ee566025f49",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5812368488682e00010003d3",
    "attached_width": 300,
    "attached_height": 162,
    "attached_size": 10506,
    "attached_name": "sliding-shorts-short.jpg",
    "main": false,
    "updated_at": 1477588612336,
    "created_at": 1477588612336
  }
}
deals << {
  "_id": "5810f1e888682e3dbe0228eb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Sleeveless Shirt - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$19.99 per shirt, normally $24.99\r\n\r\ninfo@marketpad.net\r\n\r\n-Made from Lycra, the best material for flexibility and muscle enhancement \r\n-Enchances muscle power and performance \r\n-Great fit, long lasting \r\n-Lightweight and comfortable \r\n-Durable elastic waistband with drawstring \r\n-Flatlock stitch prevents rashes \r\n-Wicks moisture away from the skin while resisting odor \r\n-Blocks over 97% of UV rays from the sun \r\n-UPF rating of 50+",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "299.88",
  "price": "239.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477592075800,
  "created_at": 1477505512185,
  "locations": [
    {
      "_id": "5810f1e888682e3dbe0228ea",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810f1e888682e3dbe0228ec",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58122f1e88682eb605025d5b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58122f1e88682e00010003bf",
    "attached_width": 811,
    "attached_height": 1219,
    "attached_size": 597789,
    "attached_name": "QSpictures 005.jpg",
    "main": false,
    "updated_at": 1477586718933,
    "created_at": 1477586718933
  }
}
deals << {
  "_id": "5810f27388682ebb830228fb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Loose Fit Shirt - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$21.99 per shirt, normally $29.99",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "359.88",
  "price": "263.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477591972811,
  "created_at": 1477505651578,
  "locations": [
    {
      "_id": "5810f27388682ebb830228fa",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810f27388682ebb830228fc",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58122f8f88682efe14025d8c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58122f8e88682e00010003c3",
    "attached_width": 811,
    "attached_height": 1219,
    "attached_size": 619611,
    "attached_name": "QSpictures 007.jpg",
    "main": false,
    "updated_at": 1477586831273,
    "created_at": 1477586831273
  }
}
deals << {
  "_id": "5810f2d588682ea079022907",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Compression Pants - Team Order",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$24.99 per pair of pants, normally $32.99\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon blend fabric creates stronger compression on your powerhouse, legs, and buttocks\r\n-Lycra is the best material for flexibility and muscle enhancement\r\n-Light and comfortable\r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor\r\n-Maximum comfort in hot or cold conditions.\r\n \r\n",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "395.88",
  "price": "299.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477592435051,
  "created_at": 1477505749706,
  "locations": [
    {
      "_id": "5810f2d588682ea079022906",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810f2d588682ea079022908",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58123c0088682e0f6e026031",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58123c0088682e00010003d7",
    "attached_width": 260,
    "attached_height": 220,
    "attached_size": 11368,
    "attached_name": "lycro-comp-big.jpg",
    "main": false,
    "updated_at": 1477590016226,
    "created_at": 1477590016226
  }
}
deals << {
  "_id": "5810f34d88682eef94022914",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "580fbebd88682ea9cd0200d9",
  "title": "Compression Shorts",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$19.99 per pair of shorts, normally $26.99 each\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon blend fabric creates stronger compression on your powerhouse, legs, and buttocks\r\n-Lycra is the best material for flexibility and muscle enhancement\r\n-Light and comfortable\r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor\r\n-Maximum comfort in hot or cold conditions.",
  "terms": "Minimum order of 12. \r\nAfter purchase, email specifications for sizes (XXS-4XL and color (all 12 must be the same color to info@marketpad.net \r\n(See sizing and color charts on MP Compression Clothing home page",
  "value": "323.88",
  "price": "239.88",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "expires_at": "",
  "user_id": "580fbebd88682ea9cd0200d6",
  "updated_at": 1477592458460,
  "created_at": 1477505869809,
  "locations": [
    {
      "_id": "5810f34d88682eef94022913",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5810f34d88682eef94022915",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "581236af88682e7a4a025f53",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "581236af88682e00010003d5",
    "attached_width": 251,
    "attached_height": 296,
    "attached_size": 10304,
    "attached_name": "compressionshorts.jpg",
    "main": false,
    "updated_at": 1477588655540,
    "created_at": 1477588655540
  }
}
deals << {
  "_id": "581390f388682e549202b904",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Long Sleeve Crew Neck Shirt - 12 shirt minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$22.99 per shirt, normally $32 each.  Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/Nylon blend, the best material for flexibility and actual muscle enhancement.\r\n-Tiny to pack and easy to wash\r\n-Light and comfortable\r\n-Wicks moisture away from the skin while resisting odor \r\n-Blocks over 97% of the UV rays from the sun \r\n-UPF rating of 50+\r\n-Enhances muscle power and performance\r\n-Great fitting and long lasting\r\n-Durable elastic waistband with drawstring\r\n-Flatlock stitch prevents rashes\r\n \r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge\r\n\r\n",
  "value": "32.0",
  "price": "22.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705351729,
  "created_at": 1477677299372,
  "locations": [
    {
      "_id": "581390f388682e549202b903",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "581390f388682e549202b905",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477678984769,
  "image": {
    "_id": "5813abff88682eb80802bc57",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813abff88682e00010004c3",
    "attached_width": 300,
    "attached_height": 305,
    "attached_size": 30001,
    "attached_name": "MP 8.jpg",
    "main": false,
    "updated_at": 1477684223130,
    "created_at": 1477684223130
  }
}
deals << {
  "_id": "5813918a88682eba2b02b92a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Short Sleeve Crew Neck Shirt - 12 shirt minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$19.99 per shirt, normally $28 each.  Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/Nylon blend, the best material for flexibility and actual muscle enhancement.\r\n-Tiny to pack and easy to wash\r\n-Light and comfortable\r\n-Wicks moisture away from the skin while resisting odor \r\n-Blocks over 97% of the UV rays from the sun \r\n-UPF rating of 50+\r\n-Enhances muscle power and performance\r\n-Great fitting and long lasting\r\n-Durable elastic waistband with drawstring\r\n-Flatlock stitch prevents rashes\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge\r\n",
  "value": "28.0",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705529549,
  "created_at": 1477677450432,
  "locations": [
    {
      "_id": "5813918a88682eba2b02b929",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5813918a88682eba2b02b92b",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477678998298,
  "image": {
    "_id": "5813abc788682e349702bc09",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813abc788682e00010004c1",
    "attached_width": 811,
    "attached_height": 1219,
    "attached_size": 210344,
    "attached_name": "whie.jpg",
    "main": false,
    "updated_at": 1477684167475,
    "created_at": 1477684167475
  },
  "referral_code": ""
}
deals << {
  "_id": "5813926588682e9eae02b954",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Sleeveless Compression Shirt - 12 shirt minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$19.99 per shirt, normally $24.99 each.  Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/Nylon blend, the best material for flexibility and actual muscle enhancement.\r\n-Tiny to pack and easy to wash\r\n-Light and comfortable\r\n-Wicks moisture away from the skin while resisting odor \r\n-Blocks over 97% of the UV rays from the sun \r\n-UPF rating of 50+\r\n-Enhances muscle power and performance\r\n-Great fitting and long lasting\r\n-Flatlock stitch prevents rashes\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge\r\n",
  "value": "24.99",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705548521,
  "created_at": 1477677669067,
  "locations": [
    {
      "_id": "5813926588682e9eae02b953",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5813926588682e9eae02b955",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477679009895,
  "image": {
    "_id": "5813ac6188682e659202bcb6",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813ac6188682e00010004c7",
    "attached_width": 717,
    "attached_height": 1003,
    "attached_size": 148446,
    "attached_name": "MP 5.jpg",
    "main": false,
    "updated_at": 1477684321600,
    "created_at": 1477684321600
  }
}
deals << {
  "_id": "581392d888682e700902b96d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Loose Fit Shirt - 12 shirt minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$21.99 per shirt, normally $29.99 each. Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/Nylon blend, the best material for flexibility and actual muscle enhancement.\r\n-Tiny to pack and easy to wash\r\n-Light and comfortable\r\n-Wicks moisture away from the skin while resisting odor \r\n-Blocks over 97% of the UV rays from the sun \r\n-UPF rating of 50+\r\n-Enhances muscle power and performance\r\n-Great fitting and long lasting\r\n-Flatlock stitch prevents rashes\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge\r\n",
  "value": "29.99",
  "price": "21.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705566725,
  "created_at": 1477677784354,
  "locations": [
    {
      "_id": "581392d888682e700902b96c",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "581392d888682e700902b96e",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477679017661,
  "image": {
    "_id": "5813ab3988682ec25902bbf3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813ab3988682e00010004bb",
    "attached_width": 269,
    "attached_height": 390,
    "attached_size": 25062,
    "attached_name": "MP 2.jpg",
    "main": false,
    "updated_at": 1477684025886,
    "created_at": 1477684025886
  }
}
deals << {
  "_id": "5813953e88682efe6b02b9be",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Long Sliding Shorts - 12 shorts minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$25.99 per pair, normally $32.99 each. Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon fabric blend is the best material for flexibility and muscle enhancement\r\n-Extra padding to help protect athletes from abrasions\r\n-Creates stronger compression on your powerhouse, legs, and buttocks\r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor\r\n-Maximum comfort in hot or cold conditions\r\n-Light and comfortable\r\n\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge",
  "value": "32.99",
  "price": "25.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705595102,
  "created_at": 1477678398923,
  "locations": [
    {
      "_id": "5813953e88682efe6b02b9bd",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5813953e88682efe6b02b9bf",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477679026271,
  "image": {
    "_id": "5813ac3488682efcc102bc9c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813ac3488682e00010004c5",
    "attached_width": 310,
    "attached_height": 333,
    "attached_size": 23090,
    "attached_name": "MP 7.jpg",
    "main": false,
    "updated_at": 1477684276809,
    "created_at": 1477684276809
  },
  "referral_code": ""
}
deals << {
  "_id": "581395c388682e273002b9d1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Short Sliding Shorts - 12 shorts minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$23.99 per pair, normally $29.99 each.  Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon fabric blend is the best material for flexibility and muscle enhancement\r\n-Extra padding to help protect athletes from abrasions\r\n-Creates stronger compression on your powerhouse, legs, and buttocks\r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor\r\n-Maximum comfort in hot or cold conditions\r\n-Light and comfortable\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge\r\n",
  "value": "29.99",
  "price": "23.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705616335,
  "created_at": 1477678531949,
  "locations": [
    {
      "_id": "581395c388682e273002b9d0",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "581395c388682e273002b9d2",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477679044320,
  "image": {
    "_id": "5813ab9188682e0b7a02bbff",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813ab9188682e00010004bf",
    "attached_width": 300,
    "attached_height": 162,
    "attached_size": 15463,
    "attached_name": "MP 9.jpg",
    "main": false,
    "updated_at": 1477684113604,
    "created_at": 1477684113604
  }
}
deals << {
  "_id": "5813963b88682edd6c02b9e0",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Compression Shorts - 12 shorts minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$19.99 per pair, normally $26.99 each. Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon fabric blend is the best material for flexibility and muscle enhancement\r\n-Creates stronger compression on your powerhouse, legs, and buttocks\r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor\r\n-Maximum comfort in hot or cold conditions\r\n-Light and comfortable\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge\r\n",
  "value": "26.99",
  "price": "19.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705635359,
  "created_at": 1477678651309,
  "locations": [
    {
      "_id": "5813963b88682edd6c02b9df",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5813963b88682edd6c02b9e1",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477679051320,
  "image": {
    "_id": "5813ab6988682e7f3102bbf8",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813ab6988682e00010004bd",
    "attached_width": 251,
    "attached_height": 296,
    "attached_size": 15797,
    "attached_name": "MP 11.jpg",
    "main": false,
    "updated_at": 1477684073859,
    "created_at": 1477684073859
  }
}
deals << {
  "_id": "581396a788682e320102b9eb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "Compression Pants - 12 pants minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$24.99 per pair, normally $32.99 each. Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6 oz Lycra/nylon fabric blend is the best material for flexibility and muscle enhancement\r\n-Creates stronger compression on your powerhouse, legs, and buttocks\r\n-Get off the mark faster, put more power in your stride and keep your legs fresher longer\r\n-Breathable fabric wicks moisture away from skin while resisting odor\r\n-Maximum comfort in hot or cold conditions\r\n-Light and comfortable\r\n",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge",
  "value": "32.99",
  "price": "24.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705671124,
  "created_at": 1477678759570,
  "locations": [
    {
      "_id": "581396a788682e320102b9ea",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "581396a788682e320102b9ec",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "581396a788682e320102b9e9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "581396a788682e0001000499",
    "attached_width": 260,
    "attached_height": 220,
    "attached_size": 11368,
    "attached_name": "lycro-comp-big.jpg",
    "main": false,
    "updated_at": 1477678759570,
    "created_at": 1477678759570
  },
  "activated_at": 1477679060047
}
deals << {
  "_id": "58139efa88682e72c702bade",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5813888088682e5c8d02b834",
  "title": "One Piece Suit - 12 suits minimum order required",
  "category_id": "4f7d216974dbb02de3000af2",
  "offer": "$59.99 per suit, normally $79.99 each.  Sold as team order of minimum of 12.\r\n\r\ninfo@marketpad.net\r\n\r\n-6oz Lycra/Nylon blend fabric is the best material for flexibility and muscle enhancement\r\n-Easy to pack and wash\r\n-Wicks moisture away while resisting odor\r\n-Blocks 97% of UV rays from the sun\r\n-UPF rating of 50+\r\n-Protects from rash, jellyfish and cold when used in or around water\r\n-Proven to increase muscle power\r\n-Flat-stictched seams prevents rashes\r\n-Light and comfortable\r\n-Zips up the back\r\n-Stylish and form fitting",
  "terms": "-Must buy minimum of 12.  \r\n-After purchase, email order specifications to info@marketpad.net \r\n     *Sizes (XXS-4XL, sizes can vary within the order of 12 See sizing chart on home page\r\n     *Color choice (all 12 must be the same color See color samples on home page\r\n              - White - Red - Royal Blue - Black - Navy - Forest Green - Maroon -\r\n-Logos may be added for an extra charge",
  "value": "79.99",
  "price": "59.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "58138a1e88682e90e802b85b"
  ],
  "expires_at": "",
  "user_id": "5813855c88682ea4ba02b7da",
  "updated_at": 1480705700327,
  "created_at": 1477680890160,
  "locations": [
    {
      "_id": "58139efa88682e72c702badd",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "58139efa88682e72c702badf",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "activated_at": 1477680894210,
  "image": {
    "_id": "5813aac688682ee7e502bbde",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5813aac688682e00010004b8",
    "attached_width": 811,
    "attached_height": 1219,
    "attached_size": 263084,
    "attached_name": "MP 10.jpg",
    "main": false,
    "updated_at": 1477683910212,
    "created_at": 1477683910212
  }
}
deals << {
  "_id": "5833a57388682ec4e40691f9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ed003cf02cbee3b500309d",
  "title": "$1 OFF ANY Flatbread Pizza",
  "category_id": "4f7d216574dbb02de3000979",
  "offer": "$1 OFF ANY Flatbread Pizza",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> Only available during regular Kitchen hours. Thu-Sat 11am-11pm   Sun-Wed 11am-10pm\r\n> 1 per customer per visit.  \r\n> Entire amount must be redeemed in 1 visit.\r\n> Made with ingredients from local companies\r\n",
  "value": "9.0",
  "price": "7.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54f74a09f02cbe12b600101a"
  ],
  "expires_at": "",
  "user_id": "5785181ca4152f9042008de6",
  "updated_at": 1479779712560,
  "created_at": 1479779699770,
  "locations": [
    {
      "_id": "5833a57388682ec4e40691f8",
      "point": [
        -117.0128559479492,
        47.74871308718497
      ],
      "address": {
        "_id": "5833a57388682ec4e40691fa",
        "country": "US",
        "street": "26433 Hwy 53,",
        "suite": "",
        "city": "Hauser Junction,",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5833a57388682ec4e40691f7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5833a57388682e00010007ee",
    "attached_width": 1280,
    "attached_height": 853,
    "attached_size": 330920,
    "attached_name": "CurleysPizza.jpg",
    "main": false,
    "updated_at": 1479779699770,
    "created_at": 1479779699770
  },
  "activated_at": 1479779712548,
  "deleted_at": 1485227981292
}
deals << {
  "_id": "583e32db88682efbf007791b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5772f040a4152f91c0003dc9",
  "title": "Only $20 for $25 worth of food",
  "category_id": "4f7d215e74dbb02de3000651",
  "offer": "Only $20 for $25 worth of Food",
  "terms": "> Cannot be combined with any other deal or coupon.  \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n\r\n\r\n\r\n",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5772f390ce4560feb7003cff"
  ],
  "expires_at": "",
  "user_id": "5772f040a4152f91c0003dc6",
  "updated_at": 1486668462709,
  "created_at": 1480471259569,
  "locations": [
    {
      "_id": "583e32db88682efbf007791a",
      "point": [
        -116.7927753,
        47.7019986
      ],
      "address": {
        "_id": "583e32db88682efbf007791c",
        "country": "US",
        "street": "503 W Appleway Ave",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "583e32db88682efbf0077919",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "583e32db88682e000100087f",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 149551,
    "attached_name": "PizzaFactoryDeal.jpg",
    "main": false,
    "updated_at": 1480471259569,
    "created_at": 1480471259569
  },
  "activated_at": 1480471307329,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "583e3f6e88682e6162077a68",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5772f040a4152f91c0003dc9",
  "title": "Large 1 topping Pizza",
  "category_id": "4f7d215e74dbb02de3000651",
  "offer": "Large 1 topping Pizza $9.99 Normally $16",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. ",
  "value": "16.0",
  "price": "9.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5772f390ce4560feb7003cff"
  ],
  "expires_at": "",
  "user_id": "5772f040a4152f91c0003dc6",
  "updated_at": 1481662639883,
  "created_at": 1480474478768,
  "locations": [
    {
      "_id": "583e3f6e88682e6162077a67",
      "point": [
        -116.7927753,
        47.7019986
      ],
      "address": {
        "_id": "583e3f6e88682e6162077a69",
        "country": "US",
        "street": "503 W Appleway Ave",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "583e3f6e88682e6162077a66",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "583e3f6e88682e0001000883",
    "attached_width": 960,
    "attached_height": 364,
    "attached_size": 121890,
    "attached_name": "PizzaFactoryDeal2 (2.jpg",
    "main": false,
    "updated_at": 1480474478767,
    "created_at": 1480474478767
  },
  "activated_at": 1480474497757,
  "referral_code": "currentattractions"
}
deals << {
  "_id": "5886ac9288682e7b0a033685",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "Buy a 1/2 Sandwich, get a free Soda",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "Purchase Classic 1/2 Sandwich and get a 20oz Soda free",
  "terms": "Cannot be combined with any other deals, discounts or coupons.  Valid during normal business hours",
  "value": "8.35",
  "price": "6.3",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1546243200000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1486058293068,
  "created_at": 1485221010039,
  "locations": [
    {
      "_id": "5886ac9188682e7b0a033684",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "5886ac9288682e7b0a033686",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "5886ac9188682e7b0a033683",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5886ac9188682e0001000235",
    "attached_width": 593,
    "attached_height": 237,
    "attached_size": 43500,
    "attached_name": "localdelidealpromo2.jpg",
    "main": false,
    "updated_at": 1485221010038,
    "created_at": 1485221010038
  },
  "activated_at": 1485221014197
}
deals << {
  "_id": "58936fb788682e0cd8045c01",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": " HIPPO  GOLD WASH FOR $13",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": "$15 GOLD WASH FOR ONLY $13  ",
  "terms": "BLUE WASH + LAVA SHINE, EXTREME SHINE, RAIN X, TIRE SHINE. MUST BE USED DURING REGULAR BUSINESS HOURS.  CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS.  DEAL EXPIRES DECEMBER 31ST 2016.  $13 CASH VALUE VALID AFTER DECEMBER 31ST 2016.  MUST REDEEM IN 1 VISIT",
  "value": "15.0",
  "price": "13.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1546243200000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1486057403671,
  "created_at": 1486057399320,
  "locations": [
    {
      "_id": "58936fb788682e0cd8045c00",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "58936fb788682e0cd8045c02",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58936fb788682e0cd8045bff",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58936fb788682e000100024b",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 72806,
    "attached_name": "hippocarwashdeal.jpg",
    "main": false,
    "updated_at": 1486057399320,
    "created_at": 1486057399320
  },
  "activated_at": 1486057403663
}
deals << {
  "_id": "5893702b88682e7864045c66",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "HIPPO $10 WHITE WASH FOR $9",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": " $10 WHITE WASH FOR $9",
  "terms": "RED WASH + WHEEL APPEAL, SEAL COAT PROTECTANT, UNDER BODY FLUSH. MUST BE USED DURING REGULAR BUSINESS HOURS. CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS. DEAL EXPIRES DECEMBER 31ST 2016. $9 CASH VALUE VALID AFTER DECEMBER 31ST 2016. MUST REDEEM IN 1 VISIT",
  "value": "10.0",
  "price": "9.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1640937600000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1486057519936,
  "created_at": 1486057515734,
  "locations": [
    {
      "_id": "5893702b88682e7864045c65",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5893702b88682e7864045c67",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893702b88682e7864045c64",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893702b88682e000100024f",
    "attached_width": 223,
    "attached_height": 178,
    "attached_size": 35544,
    "attached_name": "Hippocarwashlogo3.png",
    "main": false,
    "updated_at": 1486057515734,
    "created_at": 1486057515734
  },
  "activated_at": 1486057519927
}
deals << {
  "_id": "5893704988682e5441045c84",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "SAVE $1 HIPPO CAR WASH $8 RED WASH FOR $7",
  "category_id": "4f7d216774dbb02de3000a67",
  "offer": "$8 RED WASH FOR $7",
  "terms": "MUST BE USED DURING REGULAR BUSINESS HOURS. CANNOT BE COMBINED WITH ANY OTHER COUPONS OR DEALS. DEAL EXPIRES DECEMBER 31ST 2016. $7 CASH VALUE VALID AFTER DECEMBER 31ST 2016. MUST REDEEM IN 1 VISIT",
  "value": "8.0",
  "price": "7.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "expires_at": 1546243200000,
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1486057548822,
  "created_at": 1486057545549,
  "locations": [
    {
      "_id": "5893704988682e5441045c83",
      "point": [
        -116.7933792,
        47.7112016
      ],
      "address": {
        "_id": "5893704988682e5441045c85",
        "country": "US",
        "street": "510 W. Bosanko Ave.,",
        "suite": "Hippo Car Wash,",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893704988682e5441045c82",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893704988682e0001000253",
    "attached_width": 397,
    "attached_height": 397,
    "attached_size": 23050,
    "attached_name": "hippocarwashlogo2.jpg",
    "main": false,
    "updated_at": 1486057545549,
    "created_at": 1486057545549
  },
  "activated_at": 1486057548813
}
deals << {
  "_id": "589371d588682e46f3045cf9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568af05c94cacff84e009aa7",
  "title": "$25 Deal for $20",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": " $25 Deal for $20 at GW Hunters Post Falls  ",
  "terms": "> Cannot be combined with any other deals or coupons\r\n> Entire amount must be used in 1 visit\r\n> 1 voucher per table\r\n> $20 amount never expires\r\n> Must print voucher and present to server",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568b034a94cacfc64c00a094"
  ],
  "expires_at": 1546243200000,
  "user_id": "568af05c94cacff84e009aa4",
  "updated_at": 1486057944868,
  "created_at": 1486057941735,
  "locations": [
    {
      "_id": "589371d588682e46f3045cf8",
      "point": [
        -116.9484784,
        47.7136606
      ],
      "address": {
        "_id": "589371d588682e46f3045cfa",
        "country": "US",
        "street": "615 N. Spokane Street",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "589371d588682e46f3045cf7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589371d588682e0001000257",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 86342,
    "attached_name": "gwhuntersfood.jpg",
    "main": false,
    "updated_at": 1486057941735,
    "created_at": 1486057941735
  },
  "activated_at": 1486057944859
}
deals << {
  "_id": "5893729d88682ef4cc045d23",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": " ONLY $29.99 for $35 Worth of Food",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": " ONLY $29.99 for $35 Worth of Food",
  "terms": "> Limit 1 per visit. \r\n> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value valid after expiration date",
  "value": "35.0",
  "price": "29.99",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1546243200000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1486058146547,
  "created_at": 1486058141961,
  "locations": [
    {
      "_id": "5893729d88682ef4cc045d22",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "5893729d88682ef4cc045d24",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "5893729d88682ef4cc045d21",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893729d88682e000100025d",
    "attached_width": 660,
    "attached_height": 660,
    "attached_size": 79715,
    "attached_name": "thelocaldelipromo4.jpg",
    "main": false,
    "updated_at": 1486058141961,
    "created_at": 1486058141961
  },
  "activated_at": 1486058146538
}
deals << {
  "_id": "589372c688682ea447045d31",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "$115 for $99.99 Deal",
  "category_id": "4f7d215e74dbb02de300063c",
  "offer": "$115 deal for $99.99   ",
  "terms": "> Limit 1 per visit. \r\n> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Cannot be combined with any other deals or coupons \r\n> Cash value valid after expiration date",
  "value": "115.0",
  "price": "99.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1546243200000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1486058186914,
  "created_at": 1486058182546,
  "locations": [
    {
      "_id": "589372c688682ea447045d30",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "589372c688682ea447045d32",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "589372c688682ea447045d2f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589372c688682e0001000261",
    "attached_width": 960,
    "attached_height": 503,
    "attached_size": 69333,
    "attached_name": "localdeliphoto.jpg",
    "main": false,
    "updated_at": 1486058182545,
    "created_at": 1486058182545
  },
  "activated_at": 1486058186905
}
deals << {
  "_id": "589372ea88682ea3b2045d41",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "Buy a 1/2 Sandwich, get a free Soda",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "Purchase Classic 1/2 Sandwich and get a 20oz Soda free",
  "terms": "Cannot be combined with any other deals, discounts or coupons.  Valid during normal business hours",
  "value": "8.35",
  "price": "6.3",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "expires_at": 1546243200000,
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1486058222279,
  "created_at": 1486058218118,
  "locations": [
    {
      "_id": "589372ea88682ea3b2045d40",
      "point": [
        -116.7887219,
        47.7457895
      ],
      "address": {
        "_id": "589372ea88682ea3b2045d42",
        "country": "US",
        "street": "113 W. Prairie Shopping Ctr.,",
        "suite": "",
        "city": "Hayden",
        "region": "ID",
        "postal_code": "83835"
      }
    }
  ],
  "image": {
    "_id": "589372ea88682ea3b2045d3f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589372ea88682e0001000265",
    "attached_width": 593,
    "attached_height": 237,
    "attached_size": 43500,
    "attached_name": "localdelidealpromo2.jpg",
    "main": false,
    "updated_at": 1486058218118,
    "created_at": 1486058218118
  },
  "activated_at": 1486058222271
}
deals << {
  "_id": "58937f7588682e90ec045ee5",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56ce28aebd40b1e322019b94",
  "title": "ONLY $19 for $25 Worth of Food",
  "category_id": "4f7d215e74dbb02de3000633",
  "offer": "ONLY $19 for $25 Worth of Food",
  "terms": "> Must redeem full amount of deal at visit. \r\n> Limit 1 per person. \r\n> Limit 1 per visit. \r\n> Cannot be combined with any other deals or coupons\r\n> Cash value valid after expiration date",
  "value": "25.0",
  "price": "19.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56e0578b1aa72095c900d5e5"
  ],
  "expires_at": 1546243200000,
  "user_id": "56e052861aa72021dd00d579",
  "updated_at": 1486061433736,
  "created_at": 1486061429852,
  "locations": [
    {
      "_id": "58937f7588682e90ec045ee4",
      "point": [
        -116.7816524,
        47.7002452
      ],
      "address": {
        "_id": "58937f7588682e90ec045ee6",
        "country": "US",
        "street": "356 E. Appleway Ave.",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "58937f7588682e90ec045ee3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58937f7588682e000100026b",
    "attached_width": 528,
    "attached_height": 386,
    "attached_size": 69956,
    "attached_name": "surfshack7.jpg",
    "main": false,
    "updated_at": 1486061429852,
    "created_at": 1486061429852
  },
  "activated_at": 1486061433727
}
deals << {
  "_id": "5893805988682ee63b045f13",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56845c7994cacf755a000f04",
  "title": "1 Hot Dog (FREE Kettle Corn",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "ONLY $6.50 for 1 Hot Dog & FREE Kettle Corn | Normally $10.50\r\n\r\nFranko's Dog House located in Post Falls ID\r\n(208 964-5489\r\nfrankosdh@usa.com\r\n\r\n\r\nHot dog choices are: \r\n> German Shepard or Italian Hound\r\nKettle Corn choices are: \r\n> Huckleberry or Root Beer Float ",
  "terms": "> Not good with any other specials.\r\n> Amount paid never expires.\r\n> Promotional value expires 120 days from purchase.\r\n> Limit of 1 per person per visit.",
  "value": "10.5",
  "price": "6.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568f06e90ce944778b006257"
  ],
  "expires_at": 1577779200000,
  "user_id": "568dbbfc0ce944a99b003f41",
  "updated_at": 1486061665752,
  "created_at": 1486061657471,
  "locations": [
    {
      "_id": "5893805988682ee63b045f12",
      "point": [
        -116.916788,
        47.7104917
      ],
      "address": {
        "_id": "5893805988682ee63b045f14",
        "country": "US",
        "street": "1900 E. Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893805988682ee63b045f11",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893805988682e000100026f",
    "attached_width": 1053,
    "attached_height": 724,
    "attached_size": 92571,
    "attached_name": "dog3.jpg",
    "main": false,
    "updated_at": 1486061657471,
    "created_at": 1486061657471
  },
  "activated_at": 1486061665744
}
deals << {
  "_id": "5893808088682e39d7045f24",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56845c7994cacf755a000f04",
  "title": "1 Hot Dog (FREE Cup of Chili",
  "category_id": "4f7d215974dbb02de30003ef",
  "offer": "ONLY $4.50 for 1 Hot Dog (FREE Cup of Chili | Normally 6.50\r\n\r\nFranko's Dog House located in Post Falls ID\r\n(208 964-5489\r\nfrankosdh@usa.com\r\n\r\nHot dog choices are: \r\n> Huckleberry Hound, Yorkie, Spanish Chihuahua or Dotson\t\r\n\r\nIncludes: Frito Lu Bowl (chili, cheese and onions in Frito bag",
  "terms": "> Not good with any other specials.\r\n> Amount paid never expires.\r\n> Promotional value expires 120 days from purchase.\r\n> Limit of 1 per person per visit.",
  "value": "6.5",
  "price": "4.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "568f06e90ce944778b006257"
  ],
  "expires_at": 1546243200000,
  "user_id": "568dbbfc0ce944a99b003f41",
  "updated_at": 1486061699647,
  "created_at": 1486061696664,
  "locations": [
    {
      "_id": "5893808088682e39d7045f23",
      "point": [
        -116.916788,
        47.7104917
      ],
      "address": {
        "_id": "5893808088682e39d7045f25",
        "country": "US",
        "street": "1900 E. Seltice Way",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893808088682e39d7045f22",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893808088682e0001000273",
    "attached_width": 522,
    "attached_height": 430,
    "attached_size": 34204,
    "attached_name": "dog2.JPG",
    "main": false,
    "updated_at": 1486061696664,
    "created_at": 1486061696664
  },
  "activated_at": 1486061699639
}
deals << {
  "_id": "589380f088682ec1ca045f3d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Permanent Makeup (Eyebrows",
  "category_id": "4f7d215974dbb02de3000429",
  "offer": "ONLY $100 for Permanent Make Up for Eyebrows | Normally $300\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "300.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1546243200000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1486061812737,
  "created_at": 1486061808878,
  "locations": [
    {
      "_id": "589380f088682ec1ca045f3c",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "589380f088682ec1ca045f3e",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "589380f088682ec1ca045f3b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589380f088682e0001000277",
    "attached_width": 477,
    "attached_height": 287,
    "attached_size": 26348,
    "attached_name": "cdaesthetics.JPG",
    "main": false,
    "updated_at": 1486061808878,
    "created_at": 1486061808878
  },
  "activated_at": 1486061812728
}
deals << {
  "_id": "5893811988682e083a045f49",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Permanent Makeup (Eyeliner: Both top & bottom",
  "category_id": "4f7d215974dbb02de3000429",
  "offer": "ONLY $100 for Permanent Makeup (Eyeliner: Both top & bottom\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "250.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1546243200000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1486061852549,
  "created_at": 1486061849059,
  "locations": [
    {
      "_id": "5893811988682e083a045f48",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5893811988682e083a045f4a",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893811988682e083a045f47",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893811988682e000100027b",
    "attached_width": 482,
    "attached_height": 319,
    "attached_size": 31225,
    "attached_name": "cdaesthetics2.JPG",
    "main": false,
    "updated_at": 1486061849059,
    "created_at": 1486061849059
  },
  "activated_at": 1486061852541
}
deals << {
  "_id": "5893813688682ebe9a045f53",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "Permanent Makeup (Lip Liner",
  "category_id": "4f7d215974dbb02de3000429",
  "offer": "ONLY $100 for Permanent Makeup (Lip Liner | Normally $200\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "200.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1546243200000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1486061881995,
  "created_at": 1486061878856,
  "locations": [
    {
      "_id": "5893813688682ebe9a045f52",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5893813688682ebe9a045f54",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893813688682ebe9a045f51",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893813688682e000100027f",
    "attached_width": 423,
    "attached_height": 278,
    "attached_size": 18556,
    "attached_name": "cdaesthetics4.JPG",
    "main": false,
    "updated_at": 1486061878856,
    "created_at": 1486061878856
  },
  "activated_at": 1486061881986
}
deals << {
  "_id": "5893815788682ed4bd045f5d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "1 Chemical Peel",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "ONLY $39 for 1 Chemical Peel | Normally $100\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "100.0",
  "price": "39.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1546243200000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1486061915191,
  "created_at": 1486061911586,
  "locations": [
    {
      "_id": "5893815788682ed4bd045f5c",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5893815788682ed4bd045f5e",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893815788682ed4bd045f5b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893815788682e0001000283",
    "attached_width": 488,
    "attached_height": 356,
    "attached_size": 30276,
    "attached_name": "chempeel.JPG",
    "main": false,
    "updated_at": 1486061911585,
    "created_at": 1486061911585
  },
  "activated_at": 1486061915182
}
deals << {
  "_id": "5893817888682ec269045f69",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "2 Chemical Peels",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "ONLY $75 for 2 Chemical Peels | Normally $200\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID \r\n(208 661-7295 \r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.\r\n",
  "value": "200.0",
  "price": "75.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1546243200000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1486061947624,
  "created_at": 1486061944487,
  "locations": [
    {
      "_id": "5893817888682ec269045f68",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5893817888682ec269045f6a",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893817888682ec269045f67",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893817888682e0001000287",
    "attached_width": 590,
    "attached_height": 334,
    "attached_size": 31201,
    "attached_name": "chempeel2.JPG",
    "main": false,
    "updated_at": 1486061944487,
    "created_at": 1486061944487
  },
  "activated_at": 1486061947615
}
deals << {
  "_id": "5893819e88682e4624045f75",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56969aa7845f09fce100bc98",
  "title": "1 Chemical Peel & 1 Microdermabrasion Peel",
  "category_id": "4f7d215774dbb02de3000322",
  "offer": "ONLY $99 for 1 Chemical Peel & 1 Microdermabrasion Peel | Normally $275\r\n\r\nCoeur d'Alene Esthetics located in Coeur d'Alene ID\r\n(208 661-7295\r\ncdaesthetics@gmail.com",
  "terms": "> The value expires 90 days after purchase. \r\n> The amount paid never expires.\r\n> Limit ONE per person, but ca\r\n> All treatments must be used within 6 months of your first visit. \r\n> Appointment is required and subject to availability. \r\n> 24 hour cancellation notice required. \r\n> Services must be used by the same person.",
  "value": "275.0",
  "price": "99.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56970583845f096afe00d050"
  ],
  "expires_at": 1546243200000,
  "user_id": "5696ffa8845f09a25800cf02",
  "updated_at": 1486061985458,
  "created_at": 1486061982540,
  "locations": [
    {
      "_id": "5893819e88682e4624045f74",
      "point": [
        -116.7804664,
        47.6776832
      ],
      "address": {
        "_id": "5893819e88682e4624045f76",
        "country": "US",
        "street": "1034 North 3rd Street ",
        "suite": "3rd Street Healing Arts Center",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893819e88682e4624045f73",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893819e88682e000100028b",
    "attached_width": 461,
    "attached_height": 408,
    "attached_size": 28735,
    "attached_name": "chempeel4.JPG",
    "main": false,
    "updated_at": 1486061982540,
    "created_at": 1486061982540
  },
  "activated_at": 1486061985450
}
deals << {
  "_id": "5893820388682ea146045f8f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "title": "$24 for $30 Worth of Beverages",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "ONLY $24 for $30 worth of Beverages\r\n\r\nEl Patio Bar & Grill located in Post Falls ID \r\n(208 773-2611 \r\nelpatio6902@gmail.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Must be over 21 to purchase alcoholic beverages.",
  "value": "30.0",
  "price": "24.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "578ed303a4152f53910004bd"
  ],
  "expires_at": 1546243200000,
  "user_id": "578ed12a59ffa8af060004d7",
  "updated_at": 1486062087698,
  "created_at": 1486062083581,
  "locations": [
    {
      "_id": "5893820288682ea146045f8e",
      "point": [
        -117.0378159,
        47.70392380000001
      ],
      "address": {
        "_id": "5893820388682ea146045f90",
        "country": "US",
        "street": "6902 W. Seltice Wy.",
        "suite": "",
        "city": "Post Falls",
        "region": "Id",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893820288682ea146045f8d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893820288682e0001000296",
    "attached_width": 4368,
    "attached_height": 2912,
    "attached_size": 1911318,
    "attached_name": "beverages2.jpg",
    "main": false,
    "updated_at": 1486062083580,
    "created_at": 1486062083580
  },
  "activated_at": 1486062087678
}
deals << {
  "_id": "5893822888682e8b37045f9b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "title": "$20 for $25 Worth of Beverages",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "ONLY $20 for $25 worth of Beverages\r\n\r\nEl Patio Bar & Grill located in Post Falls ID \r\n(208 773-2611 \r\nelpatio6902@gmail.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Must be over 21 to purchase alcoholic beverages.\r\n",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "578ed303a4152f53910004bd"
  ],
  "expires_at": 1546243200000,
  "user_id": "578ed12a59ffa8af060004d7",
  "updated_at": 1486062123122,
  "created_at": 1486062120226,
  "locations": [
    {
      "_id": "5893822888682e8b37045f9a",
      "point": [
        -117.0378159,
        47.70392380000001
      ],
      "address": {
        "_id": "5893822888682e8b37045f9c",
        "country": "US",
        "street": "6902 W. Seltice Wy.",
        "suite": "",
        "city": "Post Falls",
        "region": "Id",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893822888682e8b37045f99",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893822888682e00010002a1",
    "attached_width": 626,
    "attached_height": 442,
    "attached_size": 47622,
    "attached_name": "beverages1.jpg",
    "main": false,
    "updated_at": 1486062120226,
    "created_at": 1486062120226
  },
  "activated_at": 1486062123112
}
deals << {
  "_id": "5893824a88682ef436045fa9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "title": "$12 for $15 Worth of Beverages",
  "category_id": "4f7d215e74dbb02de300064e",
  "offer": "ONLY $12 for $15 worth of Beverages\r\n\r\nEl Patio Bar & Grill located in Post Falls ID\r\n(208 773-2611\r\nelpatio6902@gmail.com",
  "terms": "> Cannot be combined with any other deal or coupon. \r\n> 1 per customer per visit. \r\n> Entire amount must be redeemed in 1 visit. \r\n> Must be over 21 to purchase alcoholic beverages.",
  "value": "15.0",
  "price": "12.0",
  "limit": "",
  "limit_per_customer": 5,
  "listing_ids": [
    "578ed303a4152f53910004bd"
  ],
  "expires_at": 1546243200000,
  "user_id": "578ed12a59ffa8af060004d7",
  "updated_at": 1486062157680,
  "created_at": 1486062154691,
  "locations": [
    {
      "_id": "5893824a88682ef436045fa8",
      "point": [
        -117.0378159,
        47.70392380000001
      ],
      "address": {
        "_id": "5893824a88682ef436045faa",
        "country": "US",
        "street": "6902 W. Seltice Wy.",
        "suite": "",
        "city": "Post Falls",
        "region": "Id",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893824a88682ef436045fa7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893824a88682e00010002a7",
    "attached_width": 2643,
    "attached_height": 2135,
    "attached_size": 542499,
    "attached_name": "beverages3.jpg",
    "main": false,
    "updated_at": 1486062154691,
    "created_at": 1486062154691
  },
  "activated_at": 1486062157667
}
deals << {
  "_id": "589382a288682e8b3b045fc1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1hr of Laser Tag or Nerf Battle for 2 ",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $15 for 1 hour of Laser Tag or Nerf Battles for 2 | Normally $27.98\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased",
  "value": "27.98",
  "price": "15.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1546243200000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1486062245188,
  "created_at": 1486062242273,
  "locations": [
    {
      "_id": "589382a288682e8b3b045fc0",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "589382a288682e8b3b045fc2",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589382a288682e8b3b045fbf",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589382a288682e00010002ad",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 57640,
    "attached_name": "deal1.jpg",
    "main": false,
    "updated_at": 1486062242272,
    "created_at": 1486062242272
  },
  "activated_at": 1486062245179
}
deals << {
  "_id": "589382c188682e255a045fcf",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1 hour Laser Tag or Nerf Battles for 4",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $27 for 1 hour of Laser Tag or Nerf Battles for 4 | Normally $55.96\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "55.96",
  "price": "27.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1546243200000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1486062276994,
  "created_at": 1486062273927,
  "locations": [
    {
      "_id": "589382c188682e255a045fce",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "589382c188682e255a045fd0",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589382c188682e255a045fcd",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589382c188682e00010002b1",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 101129,
    "attached_name": "deal3.jpg",
    "main": false,
    "updated_at": 1486062273927,
    "created_at": 1486062273927
  },
  "activated_at": 1486062276985
}
deals << {
  "_id": "589382e588682e3cf4045fdb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1 Month Unlimited Nerf Battle Pass",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $15 for a 1 Month Unlimited Nerf Battle Membership Pass for 1 Person | Normally $29\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "29.0",
  "price": "15.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1546243200000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1486062312694,
  "created_at": 1486062309342,
  "locations": [
    {
      "_id": "589382e588682e3cf4045fda",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "589382e588682e3cf4045fdc",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589382e588682e3cf4045fd9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589382e588682e00010002b5",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 113313,
    "attached_name": "deal4.jpg",
    "main": false,
    "updated_at": 1486062309342,
    "created_at": 1486062309342
  },
  "activated_at": 1486062312684
}
deals << {
  "_id": "5893830388682eb2ab045fe9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1  Month Unlimited Laser Tag Pass",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $29 for a 1 Month Unlimited Laser Tag Membership Pass for 1 Person | Normally $59\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "59.0",
  "price": "29.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1546243200000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1486062342831,
  "created_at": 1486062339896,
  "locations": [
    {
      "_id": "5893830388682eb2ab045fe8",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "5893830388682eb2ab045fea",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893830388682eb2ab045fe7",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893830388682e00010002b9",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 96190,
    "attached_name": "deal5.jpg",
    "main": false,
    "updated_at": 1486062339896,
    "created_at": 1486062339896
  },
  "activated_at": 1486062342822
}
deals << {
  "_id": "5893832388682e77b3045ff7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "57897cdf4811f5ac29009e76",
  "title": "1 Month ALL Access Membership Pass",
  "category_id": "4f7d215874dbb02de30003c4",
  "offer": "ONLY $34 for a 1 Month ALL Access Membership Pass for 1 Person | Normally $69\r\n\r\nStrike Zone Arena located in Coeur d'Alene ID\r\n(208 277-9011\r\ninfo@strikezonearena.com\r\n\r\nStrike Zone is home of the largest indoor arena in the Northwest at 6,000 square feet! Membership plans are now available so you can enjoy unlimited play for you and a guest whenever you want to come in to play!\r\n",
  "terms": "> Promotional value valid for 6 months from date of purchase\r\n> Dollar amount paid does not ever expire\r\n> Membership options are for 1 person only.\r\n> Must sign a waiver.\r\n> Valid only for option purchased\r\n> Limit 1 per person at time of visit ",
  "value": "69.0",
  "price": "34.0",
  "limit": 50,
  "limit_per_customer": "",
  "listing_ids": [
    "578a7abb59a0df661600a223"
  ],
  "expires_at": 1546243200000,
  "user_id": "578a68721aa720525a00db88",
  "updated_at": 1486062374227,
  "created_at": 1486062371637,
  "locations": [
    {
      "_id": "5893832388682e77b3045ff6",
      "point": [
        -116.7874078,
        47.7401533
      ],
      "address": {
        "_id": "5893832388682e77b3045ff8",
        "country": "US",
        "street": "7419 N. Governemt Way",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893832388682e77b3045ff5",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893832388682e00010002bd",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 79845,
    "attached_name": "deal6.jpg",
    "main": false,
    "updated_at": 1486062371637,
    "created_at": 1486062371637
  },
  "activated_at": 1486062374217
}
deals << {
  "_id": "5893839388682ed4e404601a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "1 Brazilian Wax",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "ONLY $40 for 1 Brazilian Wax | Normally $50\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "50.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1546243200000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1486062490102,
  "created_at": 1486062483266,
  "locations": [
    {
      "_id": "5893839388682ed4e4046019",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "5893839388682ed4e404601b",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893839388682ed4e4046018",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893839388682e00010002c1",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 88899,
    "attached_name": "brazilianwax1.jpg",
    "main": false,
    "updated_at": 1486062483266,
    "created_at": 1486062483266
  },
  "activated_at": 1486062490095
}
deals << {
  "_id": "589384d388682e002a04603e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "3 Brazilian Waxes",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "ONLY $110 for 3 Brazilian Waxes | Normally $135\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "135.0",
  "price": "110.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1546243200000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1486062806174,
  "created_at": 1486062803311,
  "locations": [
    {
      "_id": "589384d388682e002a04603d",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "589384d388682e002a04603f",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589384d388682e002a04603c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589384d388682e00010002c5",
    "attached_width": 876,
    "attached_height": 493,
    "attached_size": 32397,
    "attached_name": "brazilianwax2.jpg",
    "main": false,
    "updated_at": 1486062803311,
    "created_at": 1486062803311
  },
  "activated_at": 1486062806167
}
deals << {
  "_id": "589384f388682e0b01046058",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "1 Microdermabrasion Session",
  "category_id": "4f7d216574dbb02de300093e",
  "offer": "ONLY $60 for 1 Microdermabrasion Session | Normally $100\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "100.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1546243200000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1486062838115,
  "created_at": 1486062835050,
  "locations": [
    {
      "_id": "589384f388682e0b01046057",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "589384f388682e0b01046059",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589384f388682e0b01046056",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589384f388682e00010002c9",
    "attached_width": 730,
    "attached_height": 581,
    "attached_size": 41597,
    "attached_name": "micro3.JPG",
    "main": false,
    "updated_at": 1486062835050,
    "created_at": 1486062835050
  },
  "activated_at": 1486062838106
}
deals << {
  "_id": "5893851488682edc4c046065",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "3 Microdermabrasion Sessions",
  "category_id": "4f7d216574dbb02de300093e",
  "offer": "ONLY $170 for 3 Microdermabrasion Sessions | Normally $250\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "250.0",
  "price": "170.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1546243200000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1486062871386,
  "created_at": 1486062868438,
  "locations": [
    {
      "_id": "5893851488682edc4c046064",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "5893851488682edc4c046066",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893851488682edc4c046063",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893851488682e00010002cd",
    "attached_width": 800,
    "attached_height": 533,
    "attached_size": 70267,
    "attached_name": "micro2.jpg",
    "main": false,
    "updated_at": 1486062868437,
    "created_at": 1486062868437
  },
  "activated_at": 1486062871376
}
deals << {
  "_id": "589385a088682ee73c04607d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5768882ba4152f5e02001438",
  "title": "6 Microdermabrasion Sessions",
  "category_id": "4f7d216574dbb02de300093e",
  "offer": "ONLY $320 for 6 Microdermabrasion Sessions | Normally $500\r\n\r\nHawaiian Sun Tanning Salon & Spa located in Dalton Gardens ID\r\n(208 762-8267\r\nhawaiiansunrb@gmail.com",
  "terms": "> Must redeem voucher in person. \r\n> Voucher must be redeemed before expiration date.",
  "value": "500.0",
  "price": "320.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "576889e459a0df43e900146e"
  ],
  "expires_at": 1546243200000,
  "user_id": "5761d2601aa720fe0e00008d",
  "updated_at": 1486063011539,
  "created_at": 1486063008035,
  "locations": [
    {
      "_id": "589385a088682ee73c04607c",
      "point": [
        -116.7858515,
        47.7342802
      ],
      "address": {
        "_id": "589385a088682ee73c04607e",
        "country": "US",
        "street": "6848 N Government Way",
        "suite": "",
        "city": "Dalton Gardens",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589385a088682ee73c04607b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589385a088682e00010002d1",
    "attached_width": 657,
    "attached_height": 511,
    "attached_size": 82322,
    "attached_name": "micro1.jpg",
    "main": false,
    "updated_at": 1486063008035,
    "created_at": 1486063008035
  },
  "activated_at": 1486063011529
}
deals << {
  "_id": "5893860a88682e164204609e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "578443921aa7209c6700bdbc",
  "title": "ONLY $30 for $35 | Painting Experience",
  "category_id": "570d6ac3e9330c1184002192",
  "offer": "SAVE $5 -  $35 worth of classes or products for ONLY $30 | \r\n\r\n\"Paint. Drink. Have Fun.\" \r\n\r\nPinot's Palette CDA\r\n (208 930-4763\r\n\r\n> Value of voucher can be redeemed for services and/or products.\r\n\r\n",
  "terms": "> CDA location only\r\n> Must contact seller via phone to schedule class\r\n> Must be redeemed in 1 visit\r\n> Cannot be combined with any other deals or offers\r\n> 1 per customer\r\n> $30 value never expires\r\n> This offer is exclusive ONLY to the Coeur d'Alene location.\r\n> 208 930-4763",
  "value": "35.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5790037659a0df77270007ed"
  ],
  "expires_at": 1546243200000,
  "user_id": "579000024811f553df0007e3",
  "updated_at": 1486063117894,
  "created_at": 1486063114736,
  "locations": [
    {
      "_id": "5893860a88682e164204609d",
      "point": [
        -116.7805453,
        47.6795782
      ],
      "address": {
        "_id": "5893860a88682e164204609f",
        "country": "US",
        "street": "728 North 4th Street",
        "suite": "",
        "city": "Coeur d' Alene",
        "region": "Id",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893860a88682e164204609c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893860a88682e00010002d5",
    "attached_width": 720,
    "attached_height": 960,
    "attached_size": 81016,
    "attached_name": "Pinots&Palette11.jpg",
    "main": false,
    "updated_at": 1486063114736,
    "created_at": 1486063114736
  },
  "activated_at": 1486063117885
}
deals << {
  "_id": "5893881c88682e07bd04612d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Chicken Strips, Fries and Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": " ONLY $9 for Chicken Strips, Fries and Small Soda (Normally $10\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com",
  "terms": "Cannot be combined with any other deals or offers.  Must be used in 1 visit..",
  "value": "10.0",
  "price": "9.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1546243200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1486063648088,
  "created_at": 1486063644954,
  "locations": [
    {
      "_id": "5893881c88682e07bd04612c",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "5893881c88682e07bd04612e",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893881c88682e07bd04612b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893881c88682e00010002d9",
    "attached_width": 607,
    "attached_height": 464,
    "attached_size": 54410,
    "attached_name": "fedorachickenstripsdeal.jpg",
    "main": false,
    "updated_at": 1486063644953,
    "created_at": 1486063644953
  },
  "activated_at": 1486063648080
}
deals << {
  "_id": "5893883f88682e0ca804613c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Cheese Burger, Fries and a Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": "ONLY $8.50 for a Fedora Cheese Burger, Fries and a Small Soda (Normally $9.75\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com",
  "terms": "Cannot be combined with any other offers or coupons.  Must be used in 1 visit.  Offer valid through 12/31/16.  $8.50 cash valid always.",
  "value": "9.75",
  "price": "8.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1546243200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1486063682202,
  "created_at": 1486063679125,
  "locations": [
    {
      "_id": "5893883f88682e0ca804613b",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "5893883f88682e0ca804613d",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893883f88682e0ca804613a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893883f88682e00010002dd",
    "attached_width": 640,
    "attached_height": 460,
    "attached_size": 153670,
    "attached_name": "fedoraburgerdeal.png",
    "main": false,
    "updated_at": 1486063679125,
    "created_at": 1486063679125
  },
  "activated_at": 1486063682193
}
deals << {
  "_id": "5893885e88682e6eeb04614e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "1/2 Cuban Melt, Fries or Ceasar Salad and Small Soda",
  "category_id": "4f7d215e74dbb02de300063f",
  "offer": "ONLY $11 for a 1/2 Cuban Melt, Fries or Ceasar Salad, and Small Soda (Normally $13.75\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com",
  "terms": "Cannot be combined with any other offers or coupons. Must be used in 1 visit. Offer valid through 12/31/16. $8.50 cash valid always.",
  "value": "13.75",
  "price": "11.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1546243200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1486063713639,
  "created_at": 1486063710399,
  "locations": [
    {
      "_id": "5893885e88682e6eeb04614d",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "5893885e88682e6eeb04614f",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893885e88682e6eeb04614c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893885e88682e00010002e1",
    "attached_width": 314,
    "attached_height": 323,
    "attached_size": 73058,
    "attached_name": "fedoracubanmelt.jpg",
    "main": false,
    "updated_at": 1486063710399,
    "created_at": 1486063710399
  },
  "activated_at": 1486063713629
}
deals << {
  "_id": "5893888488682e2d8c046160",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Feed The Team (15 People = $10/person",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "ONLY $150 to Feed The Team | Normally $200 (That's only $10 per person!\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com\r\n\r\nOffer Includes\r\n> Main Choices are: Cheese Burger, Chicken Strips or Grilled Cheese\r\n> 5 Appetizers - Choose from: Sriracha Popcorn Chicken, Fried pickles, Homemade Fedora Chips\r\n> Includes Fedora Fries and a Drink (Soda, Tea or Coffee",
  "terms": "> Must be minimum of 15 people\r\n> Must be pre-ordered, voucher printed and redeemed at time of visit\r\n> Minimum 15 people\r\n> Cannot be combined with any other Deals or Coupons",
  "value": "200.0",
  "price": "150.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1546243200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1486063751836,
  "created_at": 1486063748758,
  "locations": [
    {
      "_id": "5893888488682e2d8c04615f",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "5893888488682e2d8c046161",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893888488682e2d8c04615e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893888488682e00010002e5",
    "attached_width": 263,
    "attached_height": 255,
    "attached_size": 54440,
    "attached_name": "fedorasoftballdeal.jpg",
    "main": false,
    "updated_at": 1486063748757,
    "created_at": 1486063748757
  },
  "activated_at": 1486063751827
}
deals << {
  "_id": "589388a588682ef0c004616d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Dinner/Lunch for 2 (FREE App",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "ONLY $31 for Dinner or Lunch for 2 | Normally $37\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com\r\n\r\nPick any 2:\r\n> Burger (Fedora Burger, Pattywagon, Bugsy, Cheddar jack The Ripper or Sandwich (Hardtimes, Yardbird, Big House, Monte Cristo, Cubano Melt, Ruebon\r\n\r\nComes with: \r\n>Fries and drink (soft drink, tea or coffee\r\n\r\nGet a FREE Appetizer:\r\n> Sriracha popcorn chicken, fried pickles or homemade Fedora chips",
  "terms": "> Must bring in printed voucher to redeem at location\r\n> Cannot be combined with any other deals or offers.\r\n> Must be used in 1 visit",
  "value": "37.0",
  "price": "31.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1546243200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1486063784534,
  "created_at": 1486063781367,
  "locations": [
    {
      "_id": "589388a588682ef0c004616c",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "589388a588682ef0c004616e",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589388a588682ef0c004616b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589388a588682e00010002e9",
    "attached_width": 480,
    "attached_height": 388,
    "attached_size": 35395,
    "attached_name": "saladsandwich.jpg",
    "main": false,
    "updated_at": 1486063781367,
    "created_at": 1486063781367
  },
  "activated_at": 1486063784527
}
deals << {
  "_id": "589388c888682e5953046177",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "Dinner/Lunch for 2 (FREE App",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": "ONLY $31 for Dinner or Lunch for 2 | Normally $37\r\n\r\nThe Fedora Bar & Grill located in Coeur d'Alene ID\r\n(208 765-8888\r\nmallory.fedora@hotmail.com\r\n\r\nPick any 2:\r\n> Burger (Fedora Burger, Pattywagon, Bugsy, Cheddar jack The Ripper or Sandwich (Hardtimes, Yardbird, Big House, Monte Cristo, Cubano Melt, Ruebon\r\n\r\nComes with: \r\n>Fries and drink (soft drink, tea or coffee\r\n\r\nGet a FREE Appetizer:\r\n> Sriracha popcorn chicken, fried pickles or homemade Fedora chips",
  "terms": "> Must bring in printed voucher to redeem at location\r\n> Cannot be combined with any other deals or offers.\r\n> Must be used in 1 visit",
  "value": "37.0",
  "price": "31.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "expires_at": 1546243200000,
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1486063819120,
  "created_at": 1486063816029,
  "locations": [
    {
      "_id": "589388c888682e5953046176",
      "point": [
        -116.8094706,
        47.71516219999999
      ],
      "address": {
        "_id": "589388c888682e5953046178",
        "country": "US",
        "street": "1726 West Kathleen Avenue,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "589388c888682e5953046175",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589388c888682e00010002ed",
    "attached_width": 480,
    "attached_height": 388,
    "attached_size": 35395,
    "attached_name": "saladsandwich.jpg",
    "main": false,
    "updated_at": 1486063816029,
    "created_at": 1486063816029
  },
  "activated_at": 1486063819111
}
deals << {
  "_id": "58938bd088682e511f0463a1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ad9f9af02cbe950100199b",
  "title": "$20 for $25 Worth of Food",
  "category_id": "4f7d216674dbb02de300098f",
  "offer": "ONLY $20 for $25 Worth of Food\r\n\r\n315 Martinis & Tapas located in Coeur d'Alene ID\r\n(208 667-9660",
  "terms": "> Offer only valid for food, NOT DRINKS.\r\n> Expires 120 days after purchase.\r\n> May be repurchased every 120 days. \r\n> Limit 1 per person, may buy 1 additional as gift. \r\n> Limit 1 per visit.\r\n> Must redeem full amount in one visit.",
  "value": "25.0",
  "price": "20.0",
  "limit": 1000,
  "limit_per_customer": 20,
  "listing_ids": [
    "54ada5ecf02cbed2a5001ac0"
  ],
  "expires_at": 1546243200000,
  "user_id": "54aed4cbf02cbe47d000008b",
  "updated_at": 1486064597209,
  "created_at": 1486064592944,
  "locations": [
    {
      "_id": "58938bd088682e511f0463a0",
      "point": [
        -116.781648,
        47.676967
      ],
      "address": {
        "_id": "58938bd088682e511f0463a2",
        "country": "US",
        "street": "315 Wallace Ave.,",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "58938bd088682e511f04639f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938bd088682e00010002f1",
    "attached_width": 1000,
    "attached_height": 750,
    "attached_size": 103825,
    "attached_name": "o (4.jpg",
    "main": false,
    "updated_at": 1486064592944,
    "created_at": 1486064592944
  },
  "activated_at": 1486064597199
}
deals << {
  "_id": "58938d0a88682ebb9e0464f5",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "3 Private Golf Lessons",
  "category_id": "57437c99e9330c36f3000a7e",
  "offer": "3 golf lessons for only $99.99 | normally $135\r\n\r\nSandbaggerz Golf located in Coeur d'Alene ID \r\n(208 665-7250 \r\nsandbaggerzgolf.hb@gmail.com",
  "terms": "Please bring in a hard copy of this deal.",
  "value": "135.0",
  "price": "99.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1522479600000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1486064909117,
  "created_at": 1486064906213,
  "locations": [
    {
      "_id": "58938d0988682ebb9e0464f4",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "58938d0a88682ebb9e0464f6",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58938d0988682ebb9e0464f3",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938d0988682e00010002f7",
    "attached_width": 2048,
    "attached_height": 1529,
    "attached_size": 561783,
    "attached_name": "sandbag 2.jpg",
    "main": false,
    "updated_at": 1486064906213,
    "created_at": 1486064906213
  },
  "activated_at": 1486064909104
}
deals << {
  "_id": "58938d2888682ebed304650d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Men's & Women's Golf Shirts - Discounted",
  "category_id": "57437fcc1aa7209386000b03",
  "offer": "Men's & Women's Golf Shirts - Discounted\r\n\r\nSandbaggerz Golf located in Coeur d'Alene ID \r\n(208 665-7250 \r\nsandbaggerzgolf.hb@gmail.com",
  "terms": "Please bring in a hard copy of this Deal",
  "value": "34.95",
  "price": "27.96",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1546243200000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1486064939415,
  "created_at": 1486064936602,
  "locations": [
    {
      "_id": "58938d2888682ebed304650c",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "58938d2888682ebed304650e",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58938d2888682ebed304650b",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938d2888682e00010002fd",
    "attached_width": 972,
    "attached_height": 726,
    "attached_size": 201443,
    "attached_name": "sandbag.jpg",
    "main": false,
    "updated_at": 1486064936602,
    "created_at": 1486064936602
  },
  "activated_at": 1486064939405
}
deals << {
  "_id": "58938d4b88682ed364046520",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": " Custom Fit Golf Clubs",
  "category_id": "57437fcc1aa7209386000b03",
  "offer": "ONLY $224 for a set of Custom Fit Golf Clubs | Normally $249\r\n\r\nSandbaggerz Golf located in Coeur d'Alene ID\r\n(208 665-7250\r\nsandbaggerzgolf.hb@gmail.com",
  "terms": "Please bring in a hard copy of this deal",
  "value": "249.0",
  "price": "224.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": 1546243200000,
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1486064974508,
  "created_at": 1486064971571,
  "locations": [
    {
      "_id": "58938d4b88682ed36404651f",
      "point": [
        -116.7904773,
        47.7122129
      ],
      "address": {
        "_id": "58938d4b88682ed364046521",
        "country": "US",
        "street": "296 West Sunset Ave, ",
        "suite": "Suite 25",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58938d4b88682ed36404651e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938d4b88682e0001000301",
    "attached_width": 498,
    "attached_height": 378,
    "attached_size": 100651,
    "attached_name": "golfclubs.jpg",
    "main": false,
    "updated_at": 1486064971571,
    "created_at": 1486064971571
  },
  "activated_at": 1486064974501
}
deals << {
  "_id": "58938e7888682e68c7046629",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "555e1c59f02cbe17ed002574",
  "title": "Your First Music Video",
  "category_id": "57437de5e9330c54aa000ad1",
  "offer": "> 2-3 Minute HD music video\r\n> Assistance uploading to website/social media\r\n> Creative consultations to design a video that compliments your music and get your name out there\r\n> Ask about becoming a Pulse Productions Featured Band\r\n\r\nPulse Productions located in Coeur d'Alene ID\r\n(208 771-4118\r\nTravis@pulsecda.com",
  "terms": "> Film your bands FIRST Pulse Productions music video for only $50 -- a $200 value. \r\n> Each video after the FIRST will be $200.  ",
  "value": "200.0",
  "price": "50.0",
  "limit": 100,
  "limit_per_customer": 1,
  "listing_ids": [
    "555e232bf02cbe935b00288f"
  ],
  "expires_at": 1546243200000,
  "user_id": "555fa5fbf02cbe21730033dc",
  "updated_at": 1486065275219,
  "created_at": 1486065272129,
  "locations": [
    {
      "_id": "58938e7888682e68c7046628",
      "point": [
        -116.78498,
        47.69284
      ],
      "address": {
        "_id": "58938e7888682e68c704662a",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58938e7888682e68c7046627",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938e7888682e0001000305",
    "attached_width": 925,
    "attached_height": 558,
    "attached_size": 108012,
    "attached_name": "150offmusicvideo.JPG",
    "main": false,
    "updated_at": 1486065272129,
    "created_at": 1486065272129
  },
  "activated_at": 1486065275122
}
deals << {
  "_id": "58938e9e88682e183c046656",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "555e1c59f02cbe17ed002574",
  "title": "Short Online Commercial Production",
  "category_id": "57437de5e9330c54aa000ad1",
  "offer": "> 1-2 minute HD video\r\n> Assistance uploading to website/social media \r\n> Optional packages with multiple videos of varying lengths\r\n> Creative consultations to create effective averstisement\r\n\r\nPulse Productions located in Coeur d'Alene ID\r\n(208 771-4118\r\nTravis@pulsecda.com",
  "terms": "$50 off short online commercial production totaling $450.00 -- Normally $500.00",
  "value": "500.0",
  "price": "450.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "555e232bf02cbe935b00288f"
  ],
  "expires_at": 1546243200000,
  "user_id": "555fa5fbf02cbe21730033dc",
  "updated_at": 1486065313871,
  "created_at": 1486065310341,
  "locations": [
    {
      "_id": "58938e9e88682e183c046653",
      "point": [
        -116.78498,
        47.69284
      ],
      "address": {
        "_id": "58938e9e88682e183c046657",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'Alene",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "58938e9e88682e183c046650",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938e9e88682e0001000309",
    "attached_width": 886,
    "attached_height": 599,
    "attached_size": 96211,
    "attached_name": "50offcommercial.JPG",
    "main": false,
    "updated_at": 1486065310341,
    "created_at": 1486065310341
  },
  "activated_at": 1486065313862
}
deals << {
  "_id": "58938f0788682e2ed50466c0",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Compact Car Full Detail (Interior & Exterior",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $99 for Full Interior and Exterior Detailing for a Compact Car | Normally $250",
  "terms": "> Can be bought every 180 days.\r\n> Limit 1 per person (may purchase 1 additional as a gift\r\n> Valid only for the option purchased.",
  "value": "250.0",
  "price": "99.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1546243200000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1486065419163,
  "created_at": 1486065415844,
  "locations": [
    {
      "_id": "58938f0788682e2ed50466bf",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "58938f0788682e2ed50466c1",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "58938f0788682e2ed50466be",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938f0788682e000100030d",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 125844,
    "attached_name": "car.jpg",
    "main": false,
    "updated_at": 1486065415843,
    "created_at": 1486065415843
  },
  "activated_at": 1486065419154
}
deals << {
  "_id": "58938f2b88682e56040466de",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Medium Size Car Full Detail (Interior & Exterior",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $119 for Full Medium Size Car Full Detail (Interior & Exterior | Normally $300",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "300.0",
  "price": "119.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1546243200000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1486065454630,
  "created_at": 1486065451753,
  "locations": [
    {
      "_id": "58938f2b88682e56040466dd",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "58938f2b88682e56040466df",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "58938f2b88682e56040466dc",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938f2b88682e0001000311",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 143883,
    "attached_name": "medium.jpg",
    "main": false,
    "updated_at": 1486065451753,
    "created_at": 1486065451753
  },
  "activated_at": 1486065454621
}
deals << {
  "_id": "58938f4888682e03230466fb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Large Car Full Detail (Interior & Exterior",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $139 Large Car Full Detail (Interior & Exterior | Normally $350",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "350.0",
  "price": "139.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1546243200000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1486065483219,
  "created_at": 1486065480336,
  "locations": [
    {
      "_id": "58938f4888682e03230466fa",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "58938f4888682e03230466fc",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "58938f4888682e03230466f9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938f4888682e0001000315",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 104382,
    "attached_name": "truck.jpg",
    "main": false,
    "updated_at": 1486065480336,
    "created_at": 1486065480336
  },
  "activated_at": 1486065483210
}
deals << {
  "_id": "58938f6f88682e185c04672a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Compact Car Full Interior Detail",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $75 for Compact Car Full Interior Detail | Normally $200",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "200.0",
  "price": "75.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1546243200000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1486065522587,
  "created_at": 1486065519695,
  "locations": [
    {
      "_id": "58938f6f88682e185c046729",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "58938f6f88682e185c04672b",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "58938f6f88682e185c046728",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938f6f88682e0001000319",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 83921,
    "attached_name": "honda.jpg",
    "main": false,
    "updated_at": 1486065519695,
    "created_at": 1486065519695
  },
  "activated_at": 1486065522578
}
deals << {
  "_id": "58938f8e88682e1947046756",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Medium Size Car Full Interior Detail",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $119 for Medium Size Car Full Interior Detail | Normally $250 value",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "250.0",
  "price": "119.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1546243200000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1486065553697,
  "created_at": 1486065550964,
  "locations": [
    {
      "_id": "58938f8e88682e1947046755",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "58938f8e88682e1947046757",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "58938f8e88682e1947046754",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938f8e88682e000100031d",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 137881,
    "attached_name": "compact.jpg",
    "main": false,
    "updated_at": 1486065550964,
    "created_at": 1486065550964
  },
  "activated_at": 1486065553688
}
deals << {
  "_id": "58938faa88682ec208046777",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "571122661aa720eeaf005657",
  "title": "Large Car Full Interior Detail",
  "category_id": "4f7d216774dbb02de3000a4d",
  "offer": "ONLY $139 for Large Car Full Interior Detail | $300 value",
  "terms": "> Can be bought every 180 days. \r\n> Limit 1 per person (may purchase 1 additional as a gift \r\n> Valid only for the option purchased.",
  "value": "300.0",
  "price": "139.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "571129e91aa7207a170056b5"
  ],
  "expires_at": 1546243200000,
  "user_id": "571122661aa720eeaf005654",
  "updated_at": 1486065583372,
  "created_at": 1486065578942,
  "locations": [
    {
      "_id": "58938faa88682ec208046776",
      "point": [
        -117.3296819,
        47.6682147
      ],
      "address": {
        "_id": "58938faa88682ec208046778",
        "country": "US",
        "street": "5519 e boone",
        "suite": "",
        "city": "Spokane valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "58938faa88682ec208046775",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58938faa88682e0001000321",
    "attached_width": 960,
    "attached_height": 720,
    "attached_size": 62697,
    "attached_name": "large.jpg",
    "main": false,
    "updated_at": 1486065578942,
    "created_at": 1486065578942
  },
  "activated_at": 1486065583364
}
deals << {
  "_id": "5893900888682e9a590467d3",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": "  21 Day Fitness Jumpstart",
  "category_id": "4f7d216574dbb02de3000973",
  "offer": "ONLY $98 for a 21 Day Fitness Jumpstart | Normally $175  \r\nComplete Athlete Located in Spokane Valley\r\n(509 808-2716\r\ninfo@completeathletespokane.com\r\n\r\n> 1 personal assessment training session.\r\n> 8 small group training sessions\r\n> 1 group nutrition consultation.\r\n> Daily nutrition tips.\r\n",
  "terms": "> Limit 1 per person per visit. \r\n> Please call ahead to schedule appointment.\r\n> Waiver must be signed at time of visit.",
  "value": "175.0",
  "price": "98.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": 1546243200000,
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1486065676128,
  "created_at": 1486065672770,
  "locations": [
    {
      "_id": "5893900888682e9a590467d2",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "5893900888682e9a590467d4",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "5893900888682e9a590467d1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893900888682e0001000327",
    "attached_width": 2048,
    "attached_height": 1365,
    "attached_size": 564971,
    "attached_name": "3.jpg",
    "main": false,
    "updated_at": 1486065672770,
    "created_at": 1486065672770
  },
  "activated_at": 1486065676120
}
deals << {
  "_id": "5893903088682e97730467f2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "574f0edae9330cb85200bb07",
  "title": "  Sports Performance Programs",
  "category_id": "4f7d216574dbb02de3000973",
  "offer": "ONLY $50 for $100 Worth of Services towards Sports Performance Programs (youth to professional \r\nComplete Athlete Located in Spokane Valley.\r\n(509 808-2716\r\ninfo@completeathletespokane.com\r\n",
  "terms": "> Not valid with any other offer\r\n> Limit 1 per person per visit. \r\n> Please call ahead to schedule batting cage time.\r\n> Waiver must be signed at time of visit.",
  "value": "100.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "574f15b1e9330c7e0a00bb4f"
  ],
  "expires_at": 1546243200000,
  "user_id": "574f0edae9330cb85200bb04",
  "updated_at": 1486065715589,
  "created_at": 1486065712524,
  "locations": [
    {
      "_id": "5893903088682e97730467f1",
      "point": [
        -117.1986606,
        47.65577649999999
      ],
      "address": {
        "_id": "5893903088682e97730467f3",
        "country": "US",
        "street": " 15312 E Sprague Ave",
        "suite": "C",
        "city": " Spokane Valley",
        "region": "WA ",
        "postal_code": "99037"
      }
    }
  ],
  "image": {
    "_id": "5893903088682e97730467f0",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893903088682e000100032d",
    "attached_width": 960,
    "attached_height": 539,
    "attached_size": 45292,
    "attached_name": "1.jpg",
    "main": false,
    "updated_at": 1486065712524,
    "created_at": 1486065712524
  },
  "activated_at": 1486065715525
}
deals << {
  "_id": "5893921588682e9c2204694c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6752e08dea15b5a00a7f6",
  "title": "Resilient Cleaner Floor Cleaner",
  "category_id": "4f7d215874dbb02de30003b4",
  "offer": "ONLY $9.99 for a bottle of Resilient Cleaner Floor Cleaner | Normally $11.99\r\n\r\nPanhandle Carpet One located in Coeur d'Alene ID\r\n(208 765-5456\r\n\r\n> The formula is a special blend that provides superior cleaning and a beautiful shine.\r\n\r\n",
  "terms": "> Must bring in printed voucher to store location to redeem and get bottle. \r\n> Not shipped to your location\r\n> Only valid for the Resilient Carpet Cleaner.  \r\n> Not valid for any other Resilient product.",
  "value": "11.99",
  "price": "9.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6752e08dea15b5a00a7f5"
  ],
  "expires_at": 1546243200000,
  "user_id": "4fb553a508dea138060006a9",
  "updated_at": 1486066204433,
  "created_at": 1486066197750,
  "locations": [
    {
      "_id": "5893921588682e9c2204694b",
      "geocode_source": "google",
      "geocoded_city_level": false,
      "point": [
        -116.797707,
        47.7011619
      ],
      "address": {
        "_id": "5893921588682e9c2204694d",
        "country": "US",
        "city": "COEUR D'ALENE",
        "postal_code": "83814",
        "region": "ID",
        "street": "739 WEST APPLEWAY",
        "suite": ""
      }
    }
  ],
  "image": {
    "_id": "5893921588682e9c2204694a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893921588682e0001000331",
    "attached_width": 293,
    "attached_height": 469,
    "attached_size": 30959,
    "attached_name": "resilient 2.JPG",
    "main": false,
    "updated_at": 1486066197750,
    "created_at": 1486066197750
  },
  "activated_at": 1486066204424
}
deals << {
  "_id": "5893928788682e63180469ce",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "537fc71af02cbef1c2001059",
  "title": "Bedliner - Over-the-Rail! SAVE $100",
  "category_id": "4f7d215774dbb02de3000319",
  "offer": "ONLY $350 for a Bedliner - Over-the-Rail | Normally $450\r\n\r\nCustom Truck located in Coeur d'Alene ID\r\n(208 765-4444\r\nrob@custom-truck.net\r\n\r\n> Vehicle-customization company since 1971! \r\n> Transforms Trucks & SUVs into full expressions of their owners styles!\r\n> ANY TRUCK\r\n> Get your favorite truck ready for summer work! \r\n> Protect your truck bed with our Liner!!",
  "terms": "> Promotional Value of $100 Expires 30 Days After Purchase Date! \r\n> Limit 1 per person, may buy 1 additional as gift. \r\n> Limit 1 per visit. \r\n> Valid only for option purchased. \r\n> In-Store ONLY. Not valid for sale items. \r\n> Not valid with other specials or offers. \r\n> Must use promotional value in 1 visit. \r\n> Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services.  \r\n> MUST make an appointment with store manager so call ahead. \r\n> Spokane: Call (509 924-9999   CDA: Call (208 765-4444",
  "value": "450.0",
  "price": "350.0",
  "limit": 5,
  "limit_per_customer": 1,
  "listing_ids": [
    "537fd226f02cbee6d80011d1",
    "537fcc92f02cbe71ec001189"
  ],
  "expires_at": 1546243200000,
  "user_id": "4fb5539308dea138060001a5",
  "updated_at": 1486066314852,
  "created_at": 1486066311738,
  "locations": [
    {
      "_id": "5893928788682e63180469cc",
      "point": [
        -116.7895391,
        47.7150691
      ],
      "address": {
        "_id": "5893928788682e63180469cf",
        "country": "US",
        "street": "254 W. Kathleen Ave.",
        "suite": "",
        "city": "Coeur d 'Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    },
    {
      "_id": "5893928788682e63180469cd",
      "point": [
        -117.3069313,
        47.6574996
      ],
      "address": {
        "_id": "5893928788682e63180469d0",
        "country": "US",
        "street": "7225 E. Sprague Avenue",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893928788682e63180469cb",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893928788682e0001000335",
    "attached_width": 1024,
    "attached_height": 768,
    "attached_size": 123825,
    "attached_name": "Spray Liner 2.jpg",
    "main": false,
    "updated_at": 1486066311738,
    "created_at": 1486066311738
  },
  "activated_at": 1486066314738
}
deals << {
  "_id": "589392a888682ebd510469f7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "537fc71af02cbef1c2001059",
  "title": "$20 for $40 at Custom Truck",
  "category_id": "4f7d215774dbb02de3000319",
  "offer": "ONLY $20 for $40 at Custom Truck\r\n\r\nCustom Truck located in Coeur d'Alene ID \r\n(208 765-4444 \r\nrob@custom-truck.net\r\n\r\n> Double your money at Custom Truck!\r\n> Must Present Gift Card before Purchasing. \r\n> Limited time offer.",
  "terms": "> One coupon per household per year.\r\n> Valid during normal working hours.",
  "value": "40.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "537fd226f02cbee6d80011d1",
    "537fcc92f02cbe71ec001189"
  ],
  "expires_at": 1546243200000,
  "user_id": "4fb5539308dea138060001a5",
  "updated_at": 1486066348077,
  "created_at": 1486066344901,
  "locations": [
    {
      "_id": "589392a888682ebd510469f5",
      "point": [
        -116.7895391,
        47.7150691
      ],
      "address": {
        "_id": "589392a888682ebd510469f8",
        "country": "US",
        "street": "254 W. Kathleen Ave.",
        "suite": "",
        "city": "Coeur d 'Alene",
        "region": "Id",
        "postal_code": "83815"
      }
    },
    {
      "_id": "589392a888682ebd510469f6",
      "point": [
        -117.3069313,
        47.6574996
      ],
      "address": {
        "_id": "589392a888682ebd510469f9",
        "country": "US",
        "street": "7225 E. Sprague Avenue",
        "suite": "",
        "city": "Spokane",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "589392a888682ebd510469f4",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589392a888682e0001000339",
    "attached_width": 1000,
    "attached_height": 670,
    "attached_size": 121951,
    "attached_name": "truck.jpg",
    "main": false,
    "updated_at": 1486066344900,
    "created_at": 1486066344900
  },
  "activated_at": 1486066348046
}
deals << {
  "_id": "5893931588682e3ba0046a6f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56141846f02cbe4b23001bbc",
  "title": "$85 for $100 Worth of Auto Services",
  "category_id": "4f7d215774dbb02de300031a",
  "offer": "ONLY $85 for $100 Worth of Auto Services\r\n> Must Present Gift Card before Purchasing.",
  "terms": "> Valid during normal working hours.\r\n> If work exceeds deal amount, difference must be paid in cash at time of service",
  "value": "100.0",
  "price": "85.0",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "56141c7af02cbeac4b001b4e"
  ],
  "expires_at": 1522479600000,
  "user_id": "5627f042f02cbe046500214c",
  "updated_at": 1486066456077,
  "created_at": 1486066453109,
  "locations": [
    {
      "_id": "5893931588682e3ba0046a6e",
      "point": [
        -116.940599,
        47.707892
      ],
      "address": {
        "_id": "5893931588682e3ba0046a70",
        "country": "US",
        "street": "516 E. 3rd Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893931588682e3ba0046a6d",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893931588682e000100033d",
    "attached_width": 960,
    "attached_height": 540,
    "attached_size": 66634,
    "attached_name": "car.jpg",
    "main": false,
    "updated_at": 1486066453108,
    "created_at": 1486066453108
  },
  "activated_at": 1486066456069
}
deals << {
  "_id": "5893933288682ef2f0046a90",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56141846f02cbe4b23001bbc",
  "title": "$40 for $50 Worth of Auto Services",
  "category_id": "4f7d215774dbb02de300031a",
  "offer": "ONLY $40 for $50 Worth of Auto Services\r\n> Must Present Gift Card before Purchasing.",
  "terms": "> Valid during normal working hours. \r\n> If work exceeds deal amount, difference must be paid in cash at time of service",
  "value": "50.0",
  "price": "40.0",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "56141c7af02cbeac4b001b4e"
  ],
  "expires_at": 1546243200000,
  "user_id": "5627f042f02cbe046500214c",
  "updated_at": 1486066485219,
  "created_at": 1486066482425,
  "locations": [
    {
      "_id": "5893933288682ef2f0046a8f",
      "point": [
        -116.940599,
        47.707892
      ],
      "address": {
        "_id": "5893933288682ef2f0046a91",
        "country": "US",
        "street": "516 E. 3rd Ave.,",
        "suite": "",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "5893933288682ef2f0046a8e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893933288682e0001000341",
    "attached_width": 590,
    "attached_height": 443,
    "attached_size": 44298,
    "attached_name": "car1.jpg",
    "main": false,
    "updated_at": 1486066482425,
    "created_at": 1486066482425
  },
  "activated_at": 1486066485212
}
deals << {
  "_id": "589393ac88682e43c1046aff",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "One Colonic Session",
  "category_id": "570562e81aa720df6f00ed6e",
  "offer": "ONLY $39 for One Colonic Session | Normally $80\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\n> During each 40-minute colonic, veteran colon hydrotherapists flush out debris with warm triple-purified water.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "80.0",
  "price": "39.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1543651200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066607093,
  "created_at": 1486066604007,
  "locations": [
    {
      "_id": "589393ab88682e43c1046afa",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "589393ac88682e43c1046b00",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "589393ab88682e43c1046af9",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589393ab88682e0001000354",
    "attached_width": 2716,
    "attached_height": 1810,
    "attached_size": 4141035,
    "attached_name": "bodydetox.jpg",
    "main": false,
    "updated_at": 1486066604006,
    "created_at": 1486066604006
  },
  "activated_at": 1486066607004
}
deals << {
  "_id": "589393cc88682e4ac9046b21",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "Two Colonic Sessions",
  "category_id": "570562e81aa720df6f00ed6e",
  "offer": "ONLY $71.99 for Two Colonic Sessions | Normally $160.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\n> During each 40-minute colonic, veteran colon hydrotherapists flush out debris with warm triple-purified water.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. \r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided.",
  "value": "160.0",
  "price": "71.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066639712,
  "created_at": 1486066636811,
  "locations": [
    {
      "_id": "589393cc88682e4ac9046b20",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "589393cc88682e4ac9046b22",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "589393cc88682e4ac9046b1f",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589393cc88682e0001000376",
    "attached_width": 2716,
    "attached_height": 1810,
    "attached_size": 4141035,
    "attached_name": "bodydetox.jpg",
    "main": false,
    "updated_at": 1486066636811,
    "created_at": 1486066636811
  },
  "activated_at": 1486066639674
}
deals << {
  "_id": "589393ec88682e0859046b47",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "1 - 60 Minute Ultrasonic Cavitation Session",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $129 for 1 - 60 Minute Ultrasonic-Cavitation Treatment | Normally $250.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nUltrasonic Cavitation or Ultrasound Liposuction is a NEW, non-surgical fat removal procedure. This revolutionary procedure was designed in Europe. Because there is no surgery and no anesthesia, there is no hospital stay, no time off from work and no recovery time. Clients see immediate results and will continue to see results in the reduction of fat up to a week following the treatment.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "250.0",
  "price": "129.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066672155,
  "created_at": 1486066668952,
  "locations": [
    {
      "_id": "589393ec88682e0859046b46",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "589393ec88682e0859046b48",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "589393ec88682e0859046b45",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589393ec88682e0001000389",
    "attached_width": 324,
    "attached_height": 233,
    "attached_size": 16911,
    "attached_name": "ultrasoniccavitationtreatments.jpeg",
    "main": false,
    "updated_at": 1486066668952,
    "created_at": 1486066668952
  },
  "activated_at": 1486066672146
}
deals << {
  "_id": "5893941188682ebb7e046b70",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "2 - 60 Minute Ultrasonic Cavitation Sessions",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $249 for 2 - 60 Minute Ultrasonic-Cavitation Treatments | Normally $500.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nUltrasonic Cavitation or Ultrasound Liposuction is a NEW, non-surgical fat removal procedure. This revolutionary procedure was designed in Europe. Because there is no surgery and no anesthesia, there is no hospital stay, no time off from work and no recovery time. Clients see immediate results and will continue to see results in the reduction of fat up to a week following the treatment.\r\n\r\n2-5 sessions may be needed for some patients to experience optimal overall results.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "500.0",
  "price": "249.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066708704,
  "created_at": 1486066705866,
  "locations": [
    {
      "_id": "5893941188682ebb7e046b6f",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "5893941188682ebb7e046b71",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893941188682ebb7e046b6e",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893941188682e000100038d",
    "attached_width": 324,
    "attached_height": 233,
    "attached_size": 16911,
    "attached_name": "ultrasoniccavitationtreatments.jpeg",
    "main": false,
    "updated_at": 1486066705866,
    "created_at": 1486066705866
  },
  "activated_at": 1486066708695
}
deals << {
  "_id": "5893943388682e070b046b96",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "1 Lipotropic Vitamin Injection (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $19 for 1 Lipotropic Vitamin Shots (B6 & B12 | Normally $49.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "49.0",
  "price": "19.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066742440,
  "created_at": 1486066739412,
  "locations": [
    {
      "_id": "5893943388682e070b046b95",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "5893943388682e070b046b97",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893943388682e070b046b94",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893943388682e0001000391",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1486066739412,
    "created_at": 1486066739412
  },
  "activated_at": 1486066742432
}
deals << {
  "_id": "5893945788682e8136046bbf",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "3 Lipotropic Vitamin Injections (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $65 for 3 Lipotropic Vitamin Injections (B6 & B12 | Normally $147.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA\r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. \r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided.",
  "value": "147.0",
  "price": "65.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066778875,
  "created_at": 1486066775828,
  "locations": [
    {
      "_id": "5893945788682e8136046bbe",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "5893945788682e8136046bc0",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893945788682e8136046bbd",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893945788682e0001000395",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1486066775828,
    "created_at": 1486066775828
  },
  "activated_at": 1486066778867
}
deals << {
  "_id": "5893947988682ecab4046be6",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "6 Lipotropic Vitamin Injection or 10 Self-Administered Kit (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $109 for 6 Lipotropic Vitamin Injection or 10 Self-Administered Kit | Normally $240.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.\r\n\r\n> With all Self-Administered Vitamin Shots you will receive a FREE Do-It-Yourself detailed instruction guide\r\n> NEW! All take home shots are pre-loaded straight from the pharmacy – smaller needles, pre-loaded, no hassle!",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "240.0",
  "price": "109.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066812634,
  "created_at": 1486066809464,
  "locations": [
    {
      "_id": "5893947988682ecab4046be5",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "5893947988682ecab4046be7",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893947988682ecab4046be4",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893947988682e0001000399",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1486066809464,
    "created_at": 1486066809464
  },
  "activated_at": 1486066812625
}
deals << {
  "_id": "5893949c88682e9685046c0b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "12 Lipotropic Vitamin Injection or 20 Self-Administered Kit (B6 & B12",
  "category_id": "4f7d215e74dbb02de300067c",
  "offer": "ONLY $179 for 12 Lipotropic Vitamin Injection or 20 Self-Administered Kit (B6 & B12 | Normally $480.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nVitamin B12 and Vitamin B6 Injections, also known as lipotropic nutrients, are a class of agents that play important roles in the body’s use of fat. These compounds enhance your body’s liver and gall bladder roles by decreasing fat deposits and speeding up metabolism of fat and its removal. Lipotropic injections effectively reduce appetite and increase your body’s natural fat-burning processes. Using Lipotropic shots, along with proper diet and exercise, can help you reach your goal weight faster and healthier. Lipotropic injections aid your weight loss by giving you the extra boost you need to lose weight fast and healthy.\r\n\r\n> With all Self-Administered Vitamin Shots you will receive a FREE Do-It-Yourself detailed instruction guide \r\n> NEW! All take home shots are pre-loaded straight from the pharmacy – smaller needles, pre-loaded, no hassle!",
  "terms": "> Offer valid only for the exact option purchased.\r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.\r\n> A Consultation is required - non-candidates and other refund requests, will be honored before any service is provided. ",
  "value": "480.0",
  "price": "179.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066848024,
  "created_at": 1486066844879,
  "locations": [
    {
      "_id": "5893949c88682e9685046c0a",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "5893949c88682e9685046c0c",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893949c88682e9685046c09",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893949c88682e000100039d",
    "attached_width": 1000,
    "attached_height": 613,
    "attached_size": 60101,
    "attached_name": "lipotropicinjections.jpg",
    "main": false,
    "updated_at": 1486066844878,
    "created_at": 1486066844878
  },
  "activated_at": 1486066848015
}
deals << {
  "_id": "589394bf88682ebb89046c38",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "6 - 20 Minute HydroMassage Sessions",
  "category_id": "5705626de9330c5f5a00a834",
  "offer": "ONLY $30 for 6 - 20 Minute HydroMassage Sessions | Normally $60.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909\r\n\r\nHydroMassage tables were designed to provide the rejuvenating benefits of a warm water massage without getting wet or taking off your clothes. You can receive these therapeutic benefits in just one 20 minute water massage session: Temporary relief from minor aches and pains, Reduced levels of stress and anxiety, Deep relaxation and a sense of well-being, Increased circulation locally in massaged areas, Relief from muscle soreness, stiffness and tension.",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual. ",
  "value": "60.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066882079,
  "created_at": 1486066879195,
  "locations": [
    {
      "_id": "589394bf88682ebb89046c37",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "589394bf88682ebb89046c39",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "589394bf88682ebb89046c36",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589394bf88682e00010003a1",
    "attached_width": 787,
    "attached_height": 504,
    "attached_size": 39532,
    "attached_name": "hydrobedbig.JPG",
    "main": false,
    "updated_at": 1486066879195,
    "created_at": 1486066879195
  },
  "activated_at": 1486066882071
}
deals << {
  "_id": "589394e488682e3387046c64",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56fafebae9330cdbd80031cc",
  "title": "12 - 20 Minute HydroMassage Sessions",
  "category_id": "5705626de9330c5f5a00a834",
  "offer": "ONLY $48 for 12 - 20 Minute HydroMassage Sessions | Normally $96.00\r\n\r\nBody Detox & Weight Loss Center located in Spokane Valley WA \r\n(509 922-9909",
  "terms": "> Offer valid only for the exact option purchased. \r\n> An appointment is required and a 24-hr cancellation notice required. \r\n> Services must be used by the same individual.",
  "value": "96.0",
  "price": "48.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56fb07221aa72060a4006720"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fb00af1aa72051120066be",
  "updated_at": 1486066919642,
  "created_at": 1486066916694,
  "locations": [
    {
      "_id": "589394e488682e3387046c61",
      "point": [
        -117.2820526,
        47.6707875
      ],
      "address": {
        "_id": "589394e488682e3387046c65",
        "country": "US",
        "street": "1510 N Argonne",
        "suite": "Suite G",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "589394e488682e3387046c60",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589394e488682e00010003a5",
    "attached_width": 787,
    "attached_height": 504,
    "attached_size": 39532,
    "attached_name": "hydrobedbig.JPG",
    "main": false,
    "updated_at": 1486066916694,
    "created_at": 1486066916694
  },
  "activated_at": 1486066919633
}
deals << {
  "_id": "589396ee88682ed2df046e22",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "1 Custom Photo Mug",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $15 for One Custom Photo Mug | Normally $20.65 ",
  "terms": "> 1 picture per mug.\r\n> Words are included\r\n> One full color or black and white photo onto each white,\r\n> 11-ounce mug.",
  "value": "20.65",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1486067441754,
  "created_at": 1486067438832,
  "locations": [
    {
      "_id": "589396ee88682ed2df046e21",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "589396ee88682ed2df046e23",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "589396ee88682ed2df046e20",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "589396ee88682e00010003a9",
    "attached_width": 477,
    "attached_height": 376,
    "attached_size": 24918,
    "attached_name": "andersoninkmug.JPG",
    "main": false,
    "updated_at": 1486067438831,
    "created_at": 1486067438831
  },
  "activated_at": 1486067441745
}
deals << {
  "_id": "5893970d88682e649f046e45",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "2 Custom Photo Mugs",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $25 for 2 Custom Photo Mugs | Normally $41.30",
  "terms": "> 1 picture per mug. \r\n> Words are included \r\n> One full color or black and white photo onto each white, \r\n> 11-ounce mug.",
  "value": "41.3",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1486067473395,
  "created_at": 1486067469424,
  "locations": [
    {
      "_id": "5893970d88682e649f046e44",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "5893970d88682e649f046e46",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "5893970d88682e649f046e43",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893970d88682e00010003ad",
    "attached_width": 477,
    "attached_height": 376,
    "attached_size": 24918,
    "attached_name": "andersoninkmug.JPG",
    "main": false,
    "updated_at": 1486067469424,
    "created_at": 1486067469424
  },
  "activated_at": 1486067473387
}
deals << {
  "_id": "5893972f88682ed096046e65",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "1 Custom Photo Stainless Steel Travel Mug ",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $18 for 1 Custom Photo Stainless Steel Travel Mug | Normally $25\r\n> Either White or Stainless Color\r\n",
  "terms": "> 1 picture per mug. \r\n> Words are included \r\n> One full color or black and white photo onto each white, ",
  "value": "25.0",
  "price": "18.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1486067506840,
  "created_at": 1486067503995,
  "locations": [
    {
      "_id": "5893972f88682ed096046e64",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "5893972f88682ed096046e66",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "5893972f88682ed096046e63",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893972f88682e00010003b1",
    "attached_width": 753,
    "attached_height": 555,
    "attached_size": 55839,
    "attached_name": "andersonmug.JPG",
    "main": false,
    "updated_at": 1486067503995,
    "created_at": 1486067503995
  },
  "activated_at": 1486067506832
}
deals << {
  "_id": "5893974f88682eb65e046e8b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56facb031aa720e17a0063a1",
  "title": "2 Custom Photo Stainless Steel Travel Mug",
  "category_id": "4f7d216374dbb02de300087b",
  "offer": "ONLY $30 for 2 Custom Photo Stainless Steel Travel Mug | Normally $50 \r\n> Either White or Stainless Color",
  "terms": "> 1 picture per mug. \r\n> Words are included \r\n> One full color or black and white photo onto each white,",
  "value": "50.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faefbbe9330c0c0e0030f9"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1486067538138,
  "created_at": 1486067535020,
  "locations": [
    {
      "_id": "5893974f88682eb65e046e8a",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "5893974f88682eb65e046e8c",
        "country": "US",
        "street": "14700 E Indiana Ave ",
        "suite": "Space 2088",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "5893974e88682eb65e046e89",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893974f88682e00010003b5",
    "attached_width": 753,
    "attached_height": 555,
    "attached_size": 55839,
    "attached_name": "andersonmug.JPG",
    "main": false,
    "updated_at": 1486067535019,
    "created_at": 1486067535019
  },
  "activated_at": 1486067538129
}
deals << {
  "_id": "5893976d88682eda21046ea4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56faf140e9330cce4400310b",
  "title": "$7.50 for $10 Worth of Goodies",
  "category_id": "4f7d215e74dbb02de3000662",
  "offer": "ONLY $7.50 for $10 Worth of Goodies",
  "terms": "> Food Items Only\r\n> No Gift Items\r\n> Tins NOT Included",
  "value": "10.0",
  "price": "7.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faf3d9e9330cf48d003125"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1486067568562,
  "created_at": 1486067565328,
  "locations": [
    {
      "_id": "5893976d88682eda21046ea3",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "5893976d88682eda21046ea5",
        "country": "US",
        "street": "14700 E Indiana Ave, ",
        "suite": "Space 2001",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "5893976d88682eda21046ea2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893976d88682e00010003b9",
    "attached_width": 508,
    "attached_height": 337,
    "attached_size": 36971,
    "attached_name": "double.JPG",
    "main": false,
    "updated_at": 1486067565328,
    "created_at": 1486067565328
  },
  "activated_at": 1486067568554
}
deals << {
  "_id": "5893978a88682e4b84046ec4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56faf140e9330cce4400310b",
  "title": "$13.50 for $20 Worth of Goodies",
  "category_id": "4f7d215e74dbb02de3000662",
  "offer": "ONLY $13.50 for $20 Worth of Goodies",
  "terms": "> Food Items Only \r\n> No Gift Items \r\n> Tins NOT Included",
  "value": "20.0",
  "price": "13.5",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56faf3d9e9330cf48d003125"
  ],
  "expires_at": 1546243200000,
  "user_id": "56fae796e9330ca038003027",
  "updated_at": 1486067597927,
  "created_at": 1486067594966,
  "locations": [
    {
      "_id": "5893978a88682e4b84046ec3",
      "point": [
        -117.2068987,
        47.6735329
      ],
      "address": {
        "_id": "5893978a88682e4b84046ec5",
        "country": "US",
        "street": "14700 E Indiana Ave, ",
        "suite": "Space 2001",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99216"
      }
    }
  ],
  "image": {
    "_id": "5893978a88682e4b84046ec2",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893978a88682e00010003bd",
    "attached_width": 515,
    "attached_height": 328,
    "attached_size": 32719,
    "attached_name": "double2.JPG",
    "main": false,
    "updated_at": 1486067594966,
    "created_at": 1486067594966
  },
  "activated_at": 1486067597918
}
deals << {
  "_id": "5893984d88682e3271046f88",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "568aecb194cacf4e3e009a0f",
  "title": "$20 For $25 Worth of Food",
  "category_id": "4f7d215e74dbb02de300062e",
  "offer": " $20 for $25 worth of food\r\n\r\nRivelle's River Grill located in Coeur d'Alene, ID\r\n(208 930-0381\r\nrivellescda@gmail.com",
  "terms": "> Offer expires September 1st 2016. \r\n> Must be used during regular business hours. \r\n> Must be used in 1 visit. \r\n> $20 Cash does not expire\r\n> Cannot be combined with any other deals or coupons\r\n> Limit 1 per table\r\n> Limit 1 per person",
  "value": "25.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": 1,
  "listing_ids": [
    "57336af61aa7203be5000ace"
  ],
  "expires_at": 1546243200000,
  "user_id": "56956ded845f097d3000990b",
  "updated_at": 1486067792057,
  "created_at": 1486067789279,
  "locations": [
    {
      "_id": "5893984d88682e3271046f87",
      "point": [
        -116.781196,
        47.7276369
      ],
      "address": {
        "_id": "5893984d88682e3271046f89",
        "country": "US",
        "street": "",
        "suite": "",
        "city": "Coeur d'alene ",
        "region": "ID",
        "postal_code": "83815"
      }
    }
  ],
  "image": {
    "_id": "5893984d88682e3271046f86",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893984d88682e00010003c4",
    "attached_width": 796,
    "attached_height": 531,
    "attached_size": 804118,
    "attached_name": "riverpix2.jpg.png",
    "main": false,
    "updated_at": 1486067789279,
    "created_at": 1486067789279
  },
  "activated_at": 1486067792043
}
deals << {
  "_id": "5893991b88682e6edc04705e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b3cd21802e881648002210",
  "title": "Full Brazilian Wax",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "SAVE $10 | Full Brazilian Wax (First time clients only ONLY $65 normally $75",
  "terms": "> For Women Only\r\n> Deal valid ONLY for first time clients",
  "value": "75.0",
  "price": "65.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56c7a5f9bd40b133d800bdb0"
  ],
  "expires_at": 1546243200000,
  "user_id": "56c7a17dbd40b151c900b7b0",
  "updated_at": 1486067998677,
  "created_at": 1486067995716,
  "locations": [
    {
      "_id": "5893991b88682e6edc04705d",
      "point": [
        -116.8073089,
        47.6948666
      ],
      "address": {
        "_id": "5893991b88682e6edc04705f",
        "country": "US",
        "street": "2062 Main Street",
        "suite": "4",
        "city": " Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893991b88682e6edc04705c",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893991b88682e00010003cb",
    "attached_width": 700,
    "attached_height": 420,
    "attached_size": 44643,
    "attached_name": "c700x420.jpg",
    "main": false,
    "updated_at": 1486067995716,
    "created_at": 1486067995716
  },
  "activated_at": 1486067998669
}
deals << {
  "_id": "5893993b88682ee62904707a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b3cd21802e881648002210",
  "title": "Full Brazilian Wax Maintenance ",
  "category_id": "4f7d215a74dbb02de3000432",
  "offer": "Full Brazilian Wax Maintenance  ",
  "terms": "> For women only",
  "value": "55.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56c7a5f9bd40b133d800bdb0"
  ],
  "expires_at": 1546243200000,
  "user_id": "56c7a17dbd40b151c900b7b0",
  "updated_at": 1486068030263,
  "created_at": 1486068027540,
  "locations": [
    {
      "_id": "5893993b88682ee629047079",
      "point": [
        -116.8073089,
        47.6948666
      ],
      "address": {
        "_id": "5893993b88682ee62904707b",
        "country": "US",
        "street": "2062 Main Street",
        "suite": "4",
        "city": " Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893993b88682ee629047078",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893993b88682e00010003cf",
    "attached_width": 756,
    "attached_height": 373,
    "attached_size": 59823,
    "attached_name": "brazilian-wax-citrus.jpg",
    "main": false,
    "updated_at": 1486068027540,
    "created_at": 1486068027540
  },
  "activated_at": 1486068030254
}
deals << {
  "_id": "5893995888682e95ae0470a3",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56b3cd21802e881648002210",
  "title": "Earlobe Piercing",
  "category_id": "4f7d215a74dbb02de300043a",
  "offer": "Earlobe Piercing \r\n",
  "terms": "> Must be over 18 or have parental consent ",
  "value": "25.0",
  "price": "22.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56c7a5f9bd40b133d800bdb0"
  ],
  "expires_at": 1546243200000,
  "user_id": "56c7a17dbd40b151c900b7b0",
  "updated_at": 1486068059368,
  "created_at": 1486068056591,
  "locations": [
    {
      "_id": "5893995888682e95ae0470a2",
      "point": [
        -116.8073089,
        47.6948666
      ],
      "address": {
        "_id": "5893995888682e95ae0470a4",
        "country": "US",
        "street": "2062 Main Street",
        "suite": "4",
        "city": " Coeur D Alene",
        "region": "ID",
        "postal_code": "83814"
      }
    }
  ],
  "image": {
    "_id": "5893995888682e95ae0470a1",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893995888682e00010003d3",
    "attached_width": 350,
    "attached_height": 500,
    "attached_size": 48206,
    "attached_name": "ear-piercing.jpg",
    "main": false,
    "updated_at": 1486068056591,
    "created_at": 1486068056591
  },
  "activated_at": 1486068059358
}
deals << {
  "_id": "58939e4788682e2391047473",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "title": "Spa Pedicure $35 | Normally $45",
  "category_id": "4f7d215a74dbb02de3000436",
  "offer": "ONLY $35.00 (Normally $45.00 for a Spa Pedicure\r\n\r\nStunning Creations Salon located in Post Falls ID\r\n(208 262-8019",
  "terms": "> Must setup an appointment\r\n> Cannot be combined with any other promotional offer\r\n> Limit 1 per person per visit\r\n",
  "value": "45.0",
  "price": "35.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "569ff05e47ffb01d870080af",
    "55c14fbcf02cbecd98001044"
  ],
  "expires_at": 1546243200000,
  "user_id": "55c14d2af02cbeecaf000f78",
  "updated_at": 1486069321927,
  "created_at": 1486069319232,
  "locations": [
    {
      "_id": "58939e4788682e2391047471",
      "point": [
        -116.9145487,
        47.7110821
      ],
      "address": {
        "_id": "58939e4788682e2391047474",
        "country": "US",
        "street": "2525 E Seltice Way,",
        "suite": "A,",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    },
    {
      "_id": "58939e4788682e2391047472",
      "point": [
        -116.9148121,
        47.7111491
      ],
      "address": {
        "_id": "58939e4788682e2391047475",
        "country": "US",
        "street": "2525 e seltice way",
        "suite": "A",
        "city": "Post Falls",
        "region": "ID",
        "postal_code": "83854"
      }
    }
  ],
  "image": {
    "_id": "58939e4788682e2391047470",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "58939e4788682e00010003d7",
    "attached_width": 600,
    "attached_height": 600,
    "attached_size": 43509,
    "attached_name": "spa.jpg",
    "main": false,
    "updated_at": 1486069319232,
    "created_at": 1486069319232
  },
  "activated_at": 1486069321917
}
deals << {
  "_id": "5893a7f488682e50ff047d9c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "536295a6f02cbeae11006d6b",
  "title": "Swedish Massage | 60 Minute",
  "category_id": "4f7d215974dbb02de30003d1",
  "offer": "ONLY $40 for one 60-minute Swedish Massage | Normally $80\r\n\r\nLone Wolf Therapies located in Spokane Valley WA\r\n(509 228-3772\r\n",
  "terms": "> Must Present Deal at your appointment.\r\n> Limit 1 per person. Appointment required.  \r\n> 24-hr cancellation notice or fee may apply. \r\n> Services must be used by the same person. ",
  "value": "80.0",
  "price": "40.0",
  "limit": 100,
  "limit_per_customer": 1,
  "listing_ids": [
    "536299fff02cbe758b005f03"
  ],
  "expires_at": 1546243200000,
  "user_id": "536295a6f02cbeae11006d6c",
  "updated_at": 1486071799511,
  "created_at": 1486071796145,
  "locations": [
    {
      "_id": "5893a7f488682e50ff047d9b",
      "geocode_source": "nominatim",
      "geocoded_city_level": false,
      "point": [
        -117.2822574,
        47.6681222
      ],
      "address": {
        "_id": "5893a7f488682e50ff047d9d",
        "country": "US",
        "street": "1210 N Argonne",
        "suite": "C",
        "city": "Spokane Valley",
        "region": "WA",
        "postal_code": "99212"
      }
    }
  ],
  "image": {
    "_id": "5893a7f488682e50ff047d9a",
    "_type": "Attachments::Image",
    "position": 100,
    "attached_uid": "5893a7f488682e00010003db",
    "attached_width": 385,
    "attached_height": 380,
    "attached_size": 45778,
    "attached_name": "massage1.jpg",
    "main": false,
    "updated_at": 1486071796145,
    "created_at": 1486071796145
  },
  "activated_at": 1486071799503
}


deals.each do |d|
  desc = d[:offer].present? ? d[:offer] : d[:terms]
  latitude = nil
  longitude = nil
  if d[:locations].present?
    location = d[:locations].first
    if location.present? && location[:point].present?
      latitude = location[:point][0]
      longitude = location[:point][1]
    end
  end
  discount = Discount.new
  discount.active = false
  discount.name = d[:title]
  discount.price = d[:value]
  discount.discounted_price = d[:price]
  discount.discount_type = 'deal'
  discount.description = desc
  discount.latitude = latitude
  discount.longitude = longitude

  discount.save(validate: false)

end

end

end