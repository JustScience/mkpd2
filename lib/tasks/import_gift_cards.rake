namespace :import_gift_cards do

  desc "Create Gift Cards"
  task :update => :environment do

deals = []

deals <<
{
  "_id": "546cd91cf02cbef3f4000d86",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53dab96ff02cbe466d001cde",
  "title": "Two Knights Home Brew Gift Card",
  "notes": "",
  "terms": "Valid During Regular Business Hour Only. Not to Be Combined With Any Other Offer.",
  "value": "25",
  "price": "25",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "53d41b57f02cbebade0034fe"
  ],
  "user_id": "53dab96ff02cbe466d001cdf",
  "updated_at": 1416419627997,
  "created_at": 1416419612422,
  "activated_at": 1416419627992
}
deals <<{
  "_id": "546cd946f02cbe6512000b73",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53dab96ff02cbe466d001cde",
  "title": "Two Knights Home Brew Gift Card",
  "notes": "",
  "terms": "Valid During Regular Business Hour Only. Not to Be Combined With Any Other Offer.",
  "value": "50",
  "price": "50",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "53d41b57f02cbebade0034fe"
  ],
  "user_id": "53dab96ff02cbe466d001cdf",
  "updated_at": 1416419656581,
  "created_at": 1416419654145,
  "activated_at": 1416419656578
}
deals <<{
  "_id": "546e3356f02cbebb0e000034",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "5391fdedf02cbecf46000aa7",
  "title": "BubbleLand Laundry",
  "notes": "",
  "terms": "Valid during regular business hours only. Not to be combined with any other offers.",
  "value": "30.0",
  "price": "20.0",
  "limit": 50,
  "limit_per_customer": 50,
  "listing_ids": [
    "5391ffb4f02cbe3ce1000b7d"
  ],
  "user_id": "5391fdedf02cbecf46000aa8",
  "updated_at": 1460396213949,
  "created_at": 1416508246400,
  "activated_at": 1416508251120
}
deals <<{
  "_id": "546e6581f02cbea29100014c",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "541f71b1f02cbe8020000e8b",
  "title": "Spokane Popcorn Gift Card",
  "notes": "",
  "terms": "",
  "value": "10.0",
  "price": "10.0",
  "limit": 100,
  "limit_per_customer": "",
  "listing_ids": [
    "54335cc1f02cbee52500034f"
  ],
  "user_id": "541f71b1f02cbe8020000e88",
  "updated_at": 1424196312041,
  "created_at": 1416521089839,
  "activated_at": 1416521105030
}
deals <<{
  "_id": "54740778f02cbea336000263",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "541f71b1f02cbe8020000e8b",
  "title": "Spokane Popcorn gift card",
  "notes": "",
  "terms": "Good towards any Spokane Popcorn product in stock. ",
  "value": "25",
  "price": "25",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54335cc1f02cbee52500034f"
  ],
  "user_id": "541f71b1f02cbe8020000e88",
  "updated_at": 1424196326957,
  "created_at": 1416890232602,
  "activated_at": 1416890239931
}
deals <<{
  "_id": "547f5ea7f02cbe5fc1004b44",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "546e6a7af02cbe10de0001ae",
  "title": "Temptations! Bakery, \"It's All Good\".",
  "notes": "$10.00 Gift Card to be redeemed at Temptations! Bakery.",
  "terms": "For best selection, AFTER purchasing Gift Card, please call (208 762-5700 and order ahead. \r\nFor Pick Up Only. Mon 12:00 pm - 6:00 pm, Tues - Sat 10:30 am - 6:00 pm, Closed Sundays. \r\nTemptations! \"It's All Good\".",
  "value": "10.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "546e7930f02cbeb9f40001c4"
  ],
  "user_id": "54738ba9f02cbeb8f8000dd6",
  "updated_at": 1447185576847,
  "created_at": 1417633447523,
  "activated_at": 1417634525199
}
deals <<{
  "_id": "54861590f02cbea7af008f23",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "543ea063f02cbec4d7000420",
  "title": "Cloud Vapor",
  "notes": "$10.00 Gift Card at Cloud Vapor.",
  "terms": "Must be 18 years old or older to redeem Gift Card.\r\nPlease Call (208446-8381 for more information.",
  "value": "10.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "543ea2a8f02cbeedce00031e"
  ],
  "user_id": "543ea063f02cbec4d700041d",
  "updated_at": 1418667103815,
  "created_at": 1418073488633,
  "activated_at": 1418073686601
}
deals <<{
  "_id": "548616b8f02cbe8bc500914e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "543ea063f02cbec4d7000420",
  "title": "Cloud Vapor",
  "notes": "$20.00 Gift Card at Cloud Vapor.",
  "terms": "Must be 18 years old or older to redeem Gift Card. \r\nPlease Call (208446-8381 for more information.",
  "value": "20.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "543ea2a8f02cbeedce00031e"
  ],
  "user_id": "543ea063f02cbec4d700041d",
  "updated_at": 1418667134786,
  "created_at": 1418073784287,
  "activated_at": 1418073804567
}
deals <<{
  "_id": "548791b8f02cbe186e0096d7",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53c32e56f02cbef5890021fc",
  "title": "1 Hour Massage",
  "notes": "Call Tracy at (509 216-3663",
  "terms": "Limited Number Available! By appointment only. ",
  "value": "50.0",
  "price": "35.0",
  "limit": 20,
  "limit_per_customer": 2,
  "listing_ids": [
    "53c336e3f02cbe3efc001d48"
  ],
  "user_id": "53c32cc5f02cbe2b1e002092",
  "updated_at": 1444407103984,
  "created_at": 1418170808955,
  "activated_at": 1418170816839
}
deals <<{
  "_id": "5489d47af02cbef6ec00f5fd",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5489cea2f02cbe0ef300f904",
  "title": "Flock's Rabbitry",
  "notes": "",
  "terms": "Only valid on animals marked for sell.  If the value of the animal is more than the value of the gift card, the gift card amount will be applied toward the purchase of said animal.  For sale animals may not always be available, gift card is only valid when for sale animals are available.  Flocks rabbitry does reserve the right to refuse business to certain individuals in which case any moneys will be returned.",
  "value": "30.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5489d435f02cbe826900f5f4"
  ],
  "user_id": "5489cea2f02cbe0ef300f901",
  "updated_at": 1418667007590,
  "created_at": 1418318970018,
  "activated_at": 1418318974914
}
deals <<{
  "_id": "548b42fdf02cbec16e01077a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "546e6a7af02cbe10de0001ae",
  "title": "Temptations! Bakery, \"It's All Good\".",
  "notes": "$20.00 Gift Card to be redeemed at Temptations! Bakery.",
  "terms": "For best selection, AFTER purchasing Gift Card, please call (208 762-5700 and order ahead. \r\nFor Pick Up Only. Mon 12:00 pm - 6:00 pm, Tues - Sat 10:30 am - 6:00 pm, Closed Sundays. \r\nTemptations! \"It's all Good\".",
  "value": "20.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "546e7930f02cbeb9f40001c4"
  ],
  "user_id": "54738ba9f02cbeb8f8000dd6",
  "updated_at": 1447185592962,
  "created_at": 1418412797497,
  "activated_at": 1418412800733
}
deals <<{
  "_id": "548b4315f02cbe44d701079c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "546e6a7af02cbe10de0001ae",
  "title": "Temptations! Bakery, \"It's All Good\".",
  "notes": "$30.00 Gift Card to be redeemed at Temptations! Bakery.",
  "terms": "For best selection, AFTER purchasing Gift Card, please call (208 762-5700 and order ahead. \r\nFor Pick Up Only. Mon 12:00 pm - 6:00 pm, Tues - Sat 10:30 am - 6:00 pm, Closed Sundays. \r\nTemptations! \"It's all Good\".",
  "value": "30.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "546e7930f02cbeb9f40001c4"
  ],
  "user_id": "54738ba9f02cbeb8f8000dd6",
  "updated_at": 1447185601304,
  "created_at": 1418412821959,
  "activated_at": 1418412824066
}
deals <<{
  "_id": "549088cbf02cbeb38100082a",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "4fbd64f09d8c215b1e0001a6",
  "title": "Rathdrum, ID Fireworks Donation Gift Card!",
  "notes": "100% of every donation will go towards the Fireworks Products Purchased. All labor and work is full volunteer!  We use EVERY PENNY to have a great show to celebrate our Rathdrum Vets!",
  "terms": "Everyone associated with the Rathdrum Fireworks Group THANKS YOU for any and all donations you offer.  PLEASE note that YOU can forward this gift card to your own social network via Facebook and 300 other networks. Thanks AGAIN! Dave and Royce!",
  "value": "10",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54908768f02cbe015000088c"
  ],
  "user_id": "4fb5538e08dea1380600000d",
  "updated_at": 1435678138821,
  "created_at": 1418758347035,
  "activated_at": 1418758375165
}
deals <<{
  "_id": "549472f2f02cbee659001aae",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6dc5708dea166270005a3",
  "title": "Must SMELL as good as it looks!",
  "notes": "Every Home MUST SMELL as good as it looks to sell in todays markets.  Attention all Real Estate Sales people:  Buy a Gift Card from my Scentsy Line and show your Listing Customer you better understand the value of a nice smelling home as YOU and other agents show their home.  Make sure your 1st impression to any home buyer is a GREAT SMELLING ONE!  This is also a business expense for your Real Estate Business.  Have your customers call me: Sandy Oliver at \r\n(208 818-5934",
  "terms": "For just $33 you and/or your customer will receive ONE easy plug in with TWO special scent bars to keep their home smelling great for up to 6 MONTHS!  Call Sandy for delivery or refills. ",
  "value": "33.0",
  "price": "33.0",
  "limit": 39,
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6dc5708dea166270005a2"
  ],
  "user_id": "4fb5539208dea13806000135",
  "updated_at": 1419015471551,
  "created_at": 1419014898507,
  "activated_at": 1419015305354
}
deals <<{
  "_id": "54947684f02cbed0d2001a81",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6dc5708dea166270005a3",
  "title": "HOMES must SMELL GREAT!",
  "notes": "Every Home MUST SMELL as good as it looks to sell in todays markets.  Attention all Real Estate Sales people:  Buy a Gift Card from my Scentsy Line and show your Listing Customer you better understand the value of a nice smelling home as YOU and other agents show their home.  Make sure your 1st impression to any home buyer is a GREAT SMELLING ONE!  This is also a business expense for your Real Estate Business.  Have your customers call me: Sandy Oliver at \r\n(208 818-5934",
  "terms": "AGENTS or Home Owners: For just $83 you or your listing customer will get:\r\n1- Easy Plug In Heater with 2 Scent Bars (up to 6 months of great smells\r\n1- Full Sized Table/Counter Top with 2 Scent Bars. (up to 6 months of great smells\r\n\r\nCall Sandy Oliver for any questions and or product delivery in Kootenai and Spokane Counties: (208 818-5934",
  "value": "83.0",
  "price": "83.0",
  "limit": 49,
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6dc5708dea166270005a2"
  ],
  "user_id": "4fb5539208dea13806000135",
  "updated_at": 1419016527908,
  "created_at": 1419015812340,
  "activated_at": 1419015863038
}
deals <<{
  "_id": "54b818bbf02cbe9de300004e",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "54ad9f9af02cbe950100199b",
  "title": "Greenbriar Gift Card",
  "notes": "",
  "terms": "",
  "value": "25.0",
  "price": "20.0",
  "limit": 1000,
  "limit_per_customer": 20,
  "listing_ids": [
    "54ada5ecf02cbed2a5001ac0"
  ],
  "user_id": "54aed4cbf02cbe47d000008b",
  "updated_at": 1460485062390,
  "created_at": 1421351099952,
  "activated_at": 1421351128652
}
deals <<{
  "_id": "54c93460f02cbe276e00705b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53ceb12cf02cbed42a0026fe",
  "title": "$50 Gift Card!",
  "notes": "Gift Card only good towards Services.\r\nMon - Thu: 9:00 am - 8:00 pm, Fri: 9:00 am - 7:00 pm, Sat: 9:00 am - 4:00 pm, Sun: 9:00 am - 2:00 pm.",
  "terms": "",
  "value": "50.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53ceb286f02cbe399a002a38"
  ],
  "user_id": "53ceb12cf02cbed42a0026ff",
  "updated_at": 1422472486575,
  "created_at": 1422472288859,
  "activated_at": 1422472338147
}
deals <<{
  "_id": "54d3e362f02cbe5f3200baba",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "538e04b59c058e34c4000bb9",
  "title": "Big Ben's Gift Card",
  "notes": "This is a gift card for Big Ben's Property Maintenance in Spokane, Washington. I serve Spokane and some surrounding areas. ",
  "terms": "This Gift Card is only applicable for bid lawn care service. Because my business is a bid-oriented service, I must survey the area to be worked on and provide a proper bid for my services. This amount varies depending on size, weight, distance, etc. ",
  "value": "50",
  "price": "40",
  "limit": 10,
  "limit_per_customer": 1,
  "listing_ids": [
    "538e075f9c058e7a03000b24"
  ],
  "user_id": "538e04b59c058e34c4000bba",
  "updated_at": 1460399271208,
  "created_at": 1423172450120,
  "activated_at": 1423172454394,
  "deleted_at": 1460399296082
}
deals <<{
  "_id": "54d3e3cbf02cbeca0900babd",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "538e04b59c058e34c4000bb9",
  "title": "Big Ben's Gift Card",
  "notes": "This Gift Card is used for Big Ben's Property Maintenance in Spokane, WA and certain surrounding areas.",
  "terms": "This Gift Card is only applicable for bid lawn care service. Because my business is a bid-oriented service, I must survey the area to be worked on and provide a proper bid for my services. This amount varies depending on size, weight, distance, etc. ",
  "value": "100",
  "price": "90",
  "limit": 10,
  "limit_per_customer": 1,
  "listing_ids": [
    "538e075f9c058e7a03000b24"
  ],
  "user_id": "538e04b59c058e34c4000bba",
  "updated_at": 1460399281788,
  "created_at": 1423172555516,
  "activated_at": 1423172558682,
  "deleted_at": 1460399289015
}
deals <<{
  "_id": "54ea4425f02cbe7103001c03",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "$15 COBB'S CREATIONS GIFT CARDS",
  "notes": "GOOD ON ANY ITEMS ",
  "terms": "",
  "value": "15.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1429109967781,
  "created_at": 1424639013852,
  "activated_at": 1424639056581,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "54ea463bf02cbeab21001c12",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "$25 COBB\"S CREATIONS GIFT CARD",
  "notes": "$25 COBB''S CREATIONS GIFT CARDS",
  "terms": "",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1429109975313,
  "created_at": 1424639547901,
  "activated_at": 1424639560093,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "54ea7c19f02cbee4a7001d1d",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "title": "$40 COBB'S CREATIONS GIFT CARDS",
  "notes": "$40 GIFT CARD",
  "terms": "",
  "value": "40.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54eb5345f02cbe538a002266",
    "54ea428bf02cbe0f39001acc"
  ],
  "user_id": "54ea3da9f02cbee1bb001ab1",
  "updated_at": 1429109982010,
  "created_at": 1424653337619,
  "activated_at": 1424653344031,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "54f9e8ecf02cbefea5001dc9",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54f0d7aff02cbe2b66005858",
  "title": "$20 gift card for a music lesson",
  "notes": "Purchase this gift card for a guitar/bass/keys/ or vocal lesson.",
  "terms": "Limited Time Offer!",
  "value": "20.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "user_id": "4fb5539508dea13806000219",
  "updated_at": 1444841141334,
  "created_at": 1425664236693,
  "activated_at": 1425664255140,
  "referral_code": ""
}
deals <<{
  "_id": "54f9ee3bf02cbe16b2001ddb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54f0d7aff02cbe2b66005858",
  "title": "$40 Gift Card for 2 music lessons",
  "notes": "$40 Gift Card for 2 Half Hour Guitar/Bass/Keys or Vocal lessons new or returning students",
  "terms": "Limited Time Offer!",
  "value": "40.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "user_id": "4fb5539508dea13806000219",
  "updated_at": 1444841120530,
  "created_at": 1425665595892,
  "activated_at": 1425665642709,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "54f9eedbf02cbe68200020b2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "54f0d7aff02cbe2b66005858",
  "title": "$60 Gift Card for 3 Music lessons",
  "notes": "$60 Gift Card for 3 Guitar/Bass/Keys/ or Vocal Lessons",
  "terms": "Limited Time Offer!",
  "value": "60.0",
  "price": "60.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    
  ],
  "user_id": "4fb5539508dea13806000219",
  "updated_at": 1444841082562,
  "created_at": 1425665755732,
  "activated_at": 1425665761918,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "55148482f02cbe83bc002dd3",
  "active": false,
  "vouchers_count": 4,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$25 SCHMIDTY'S BURGERS DISCOUNTED GIFT CARD",
  "notes": "Valid anytime during business hours",
  "terms": "PURCHASE A $25 SCHMIDTY'S DISCOUNTED GIFTCARD FOR $19.99.   GOOD DURING REGULAR BUSINESS HOURS.",
  "value": "25.0",
  "price": "19.99",
  "limit": 15,
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1452708781873,
  "created_at": 1427408002795,
  "activated_at": 1427408045587,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "552d897ef02cbe58310007d8",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$15 SCHMIDTY'S BURGERS DISCOUNTED GIFT CARD",
  "notes": "PURCHASE A $15 SCHMIDTY'S GIFT CARD FOR $11.99",
  "terms": "GOOD DURING REGULAR BUSINESS HOURS.",
  "value": "15.0",
  "price": "11.99",
  "limit": 15,
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1452708808370,
  "created_at": 1429047678456,
  "activated_at": 1429047682660,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "552d89bef02cbef0c000077e",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$10 SCHMIDTY'S BURGERS GIFT CARD!",
  "notes": "GOOD DURING REGULAR BUSINESS HOURS",
  "terms": "",
  "value": "10.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1469124885622,
  "created_at": 1429047742080,
  "activated_at": 1429047747105,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5536a199f02cbefb36003986",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "536295a6f02cbeae11006d6b",
  "title": "$80 Gift Card",
  "notes": "Must Present MarketPad.com Gift Card at your appointment.\r\nCannot be combined with any other offer.\r\n",
  "terms": "Must call to make an appointment, 509-228-3772",
  "value": "80.0",
  "price": "80.0",
  "limit": 40,
  "limit_per_customer": 1,
  "listing_ids": [
    "536299fff02cbe758b005f03"
  ],
  "user_id": "536295a6f02cbeae11006d6c",
  "updated_at": 1429727613831,
  "created_at": 1429643673006,
  "activated_at": 1429644159618,
  "referral_code": "mlv9240"
}
deals <<{
  "_id": "5536a954f02cbeedbf003555",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "536295a6f02cbeae11006d6b",
  "title": "$80 FOR AN 1 HOUR DEEP TISSUE MASSAGE",
  "notes": "Must Present MarketPad.com Gift Card at your appointment. \r\nCannot be combined with any other offer.",
  "terms": "Must call to make an appointment, 509-228-3772",
  "value": "80.00",
  "price": "80.00",
  "limit": 40,
  "limit_per_customer": 1,
  "listing_ids": [
    "536299fff02cbe758b005f03"
  ],
  "user_id": "536295a6f02cbeae11006d6c",
  "updated_at": 1429645663239,
  "created_at": 1429645652504,
  "activated_at": 1429645663237,
  "deleted_at": 1429717526204
}
deals <<{
  "_id": "554281acf02cbe57820020eb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "554144a8f02cbe3d3d00151d",
  "title": "$20.00 Gift Card at Rustic Petal Florist",
  "notes": "$20.00 Gift Card",
  "terms": "During Business Hours.  Must present Gift Card at time of purchase.",
  "value": "20.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55414943f02cbe920300143f"
  ],
  "user_id": "554144a8f02cbe3d3d00151a",
  "updated_at": 1461348747541,
  "created_at": 1430421932068,
  "activated_at": 1430421950532,
  "referral_code": "rachelr83"
}
deals <<{
  "_id": "554281ebf02cbe8250002017",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "554144a8f02cbe3d3d00151d",
  "title": "$30.00 Gift Card at Rustic Petal Florist",
  "notes": "",
  "terms": "During Business Hours. Must present Gift Card at time of purchase.\r\n",
  "value": "30.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55414943f02cbe920300143f"
  ],
  "user_id": "554144a8f02cbe3d3d00151a",
  "updated_at": 1461348743415,
  "created_at": 1430421995046,
  "activated_at": 1430422004657,
  "referral_code": "rachelr83"
}
deals <<{
  "_id": "5542821df02cbe193a0020f2",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "554144a8f02cbe3d3d00151d",
  "title": "$40.00 Gift Card at Rustic Petal Florist",
  "notes": "",
  "terms": "During Business Hours. Must present Gift Card at time of purchase.\r\n",
  "value": "40.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55414943f02cbe920300143f"
  ],
  "user_id": "554144a8f02cbe3d3d00151a",
  "updated_at": 1461348738829,
  "created_at": 1430422045580,
  "activated_at": 1430422059155,
  "referral_code": "rachelr83"
}
deals <<{
  "_id": "5550ffa9f02cbea23d002fbb",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "553fd0b0f02cbe6a6300cb88",
  "title": "$10 Gift Card at Twisp Cafe in Liberty Lake",
  "notes": "$10 Gift Card at Twisp Cafe in Liberty Lake",
  "terms": "Cannot combine with any other offer.  Limited number of gift cards available.",
  "value": "10.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "553fd58bf02cbe4b7300c22c"
  ],
  "user_id": "5540ffc0f02cbe721100123f",
  "updated_at": 1431371745187,
  "created_at": 1431371689559,
  "activated_at": 1431371745184
}
deals <<{
  "_id": "5550fffcf02cbe760e002c2b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "553fd0b0f02cbe6a6300cb88",
  "title": "$20 Gift Card at Twisp Cafe in Liberty Lake",
  "notes": "$20 Gift Card at Twisp Cafe in Liberty Lake",
  "terms": "Cannot combine with any other offer.  Limited number of gift cards available.",
  "value": "20",
  "price": "20",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "553fd58bf02cbe4b7300c22c"
  ],
  "user_id": "5540ffc0f02cbe721100123f",
  "updated_at": 1431371789202,
  "created_at": 1431371772841,
  "activated_at": 1431371789199
}
deals <<{
  "_id": "55510031f02cbe0fe3002c2f",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "553fd0b0f02cbe6a6300cb88",
  "title": "$30 Gift Card at Twisp Cafe in Liberty Lake",
  "notes": "$30 Gift Card at Twisp Cafe in Liberty Lake",
  "terms": "Cannot combine with any other offer.  Limited number of gift cards available.",
  "value": "30",
  "price": "30",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "553fd58bf02cbe4b7300c22c"
  ],
  "user_id": "5540ffc0f02cbe721100123f",
  "updated_at": 1431371828915,
  "created_at": 1431371825106,
  "activated_at": 1431371828913
}
deals <<{
  "_id": "55510060f02cbe4442002c36",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "553fd0b0f02cbe6a6300cb88",
  "title": "$50 Gift Card at Twisp Cafe in Liberty Lake",
  "notes": "$50 Gift Card at Twisp Cafe in Liberty Lake",
  "terms": "Cannot combine with any other offer.  Limited number of gift cards available.",
  "value": "50",
  "price": "50",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "553fd58bf02cbe4b7300c22c"
  ],
  "user_id": "5540ffc0f02cbe721100123f",
  "updated_at": 1431371876312,
  "created_at": 1431371872723,
  "activated_at": 1431371876310
}
deals <<{
  "_id": "55511408f02cbe313c002ebd",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$50 SCHMIDTY'S BURGERS DISCOUNTED GIFTCARD",
  "notes": "PURCHASE A $50 DISCOUNTED SCHMIDTY'S GIFTCARD FOR $39.99",
  "terms": "VALID DURING REGULAR BUSINESS HOURS.",
  "value": "50.0",
  "price": "39.99",
  "limit": 15,
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1469124883901,
  "created_at": 1431376904355,
  "activated_at": 1445011068314
}
deals <<{
  "_id": "55511606f02cbe99af00306c",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$50 SCHMIDTY'S BURGERS DISCOUNTED GIFTCARD",
  "notes": "PURCHASE A $50 SCHMIDTY'S BURGERS DISCOUNTED GIFTCARD FOR $39.99",
  "terms": "VALID DURING REGULAR BUSINESS HOURS. \r\nCANNOT BE USED WITH ANY OTHER DISCOUNTS\r\nLimited Time Offer!",
  "value": "50.0",
  "price": "39.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1445011101222,
  "created_at": 1431377414345,
  "activated_at": 1431377441656,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "55539174f02cbe759d003f52",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "545d2b6af02cbe4c8500ff07",
  "title": "RANCHO VIEJO HAYDEN $25 GIFTCARD",
  "notes": "RANCHO VIEJO HAYDEN $25 GIFT CARD.",
  "terms": "GOOD DURING REGULAR BUSINESS HOURS.  HAYDEN LOCATION ONLY",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54612638f02cbeb4a00115ec"
  ],
  "user_id": "555245edf02cbe522800339e",
  "updated_at": 1431616094557,
  "created_at": 1431540084050,
  "activated_at": 1431540115300,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5553922bf02cbebd84003c49",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "545d2b6af02cbe4c8500ff07",
  "title": "RANCHO VIEJO HAYDEN $40 GIFTCARD",
  "notes": "RANCHO VIEJO HAYDEN $40 GIFT CARD.  ",
  "terms": "RANCHO VIEJO HAYDEN $40 GIFT CARD.  GOOD DURING REGULAR BUSINESS HOURS.  HAYDEN LOCATION ONLY.",
  "value": "40.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "54612638f02cbeb4a00115ec"
  ],
  "user_id": "555245edf02cbe522800339e",
  "updated_at": 1431616104462,
  "created_at": 1431540267352,
  "activated_at": 1431540278660,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "558328a6f02cbebafe00110e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5583030ff02cbe27d80010da",
  "title": "18.00 gift card",
  "notes": "Not to combine with other offers.",
  "terms": "18.00 includes haircut, shampoo, hot towel, BEER and straight edge neck shave",
  "value": "18.0",
  "price": "18.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55830522f02cbe2b9a001183"
  ],
  "user_id": "558318daf02cbeb6c8001246",
  "updated_at": 1434660090171,
  "created_at": 1434658982965,
  "activated_at": 1434659022517
}
deals <<{
  "_id": "55b681fcf02cbe34e3001cde",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6752e08dea15b5a00a7f6",
  "title": "$100.00 Gift Card at Panhandle Carpet One",
  "notes": "$100.00 Gift Card at Panhandle Carpet One.\r\nGood only at 739 W. Appleway, Coeur d'Alene, ID 83814.\r\nCannot be combined with any other offer.\r\nMust Present Offer before Purchasing.\r\n",
  "terms": "Valid during normal working hours.",
  "value": "100.0",
  "price": "100.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6752e08dea15b5a00a7f5"
  ],
  "user_id": "4fb553a508dea138060006a9",
  "updated_at": 1438204238927,
  "created_at": 1438024188093,
  "activated_at": 1438204238922
}
deals <<{
  "_id": "55f8813af02cbea21400319a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb6b74008dea15b5a0661e3",
  "title": "$50.00 Gift Card at The Car Lot!",
  "notes": "$50.00 Gift Card at The Car Lot.\r\nCannot be combined with any other offer. \r\nMust Present Offer before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "50.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb6b74008dea15b5a0661e2"
  ],
  "user_id": "4fb553b408dea13806000abd",
  "updated_at": 1442349453563,
  "created_at": 1442349370317,
  "activated_at": 1442349453561
}
deals <<{
  "_id": "55fb453ef02cbeec8a0044ac",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55d22627f02cbe45d700b51a",
  "title": "$15.00 Gift Card for only $13.50!",
  "notes": "Save 10% on our $15.00 Gift Card!\r\nCannot be combined with any other offer. \r\nMust Present Offer before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "15.00",
  "price": "13.50",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55d226eef02cbeda9c00b52e"
  ],
  "user_id": "55d22627f02cbe45d700b517",
  "updated_at": 1442530789443,
  "created_at": 1442530622551,
  "activated_at": 1442530789441
}
deals <<{
  "_id": "55fb48f0f02cbed0c30046bf",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55d22627f02cbe45d700b51a",
  "title": "$25.00 Gift Card for only $22.50!",
  "notes": "Save 10% on our $25.00 Gift Card! \r\nCannot be combined with any other offer. \r\nMust Present Offer before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "25.00",
  "price": "22.50",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55d226eef02cbeda9c00b52e"
  ],
  "user_id": "55d22627f02cbe45d700b517",
  "updated_at": 1442531583963,
  "created_at": 1442531568157,
  "activated_at": 1442531583962
}
deals <<{
  "_id": "560db36bf02cbea2b700bdb6",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "$25.00 Gift Card at The Fedora Pub and Grille.",
  "notes": "Cannot be combined with any other offer. \r\nMust Present Gift Card before Purchasing.\r\nOffer Expires on October 31, 2016.",
  "terms": "Valid during normal working hours.",
  "value": "25.00",
  "price": "25.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1443738475875,
  "created_at": 1443738475875,
  "deleted_at": 1443738515056
}
deals <<{
  "_id": "560db893f02cbe6d9a00c188",
  "active": true,
  "vouchers_count": 1,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "$25.00 Gift Card at The Fedora Pub and Grille.",
  "notes": "Cannot be combined with any other offer. \r\nMust Present Gift Card before Purchasing.\r\nLimited Time Offer!",
  "terms": "Valid during normal working hours.",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1444841310410,
  "created_at": 1443739795275,
  "activated_at": 1443739822936,
  "referral_code": "boyd"
}
deals <<{
  "_id": "560db914f02cbead8b00bdc4",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "$50.00 Gift Card at The Fedora Pub and Grille",
  "notes": "Cannot be combined with any other offer. \r\nMust Present Gift Card before Purchasing. \r\nLimited Time Offer!",
  "terms": "Valid during normal working hours.",
  "value": "50.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1444841284490,
  "created_at": 1443739924212,
  "activated_at": 1443739941973,
  "referral_code": "boyd"
}
deals <<{
  "_id": "560db990f02cbe5ee600bdc6",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53e53e6af02cbed474000131",
  "title": "$75.00 Gift Card at The Fedora Pub and Grille",
  "notes": "Cannot be combined with any other offer. \r\nMust Present Gift Card before Purchasing. \r\nLimited Time Offer!",
  "terms": "Valid during normal working hours.",
  "value": "75.0",
  "price": "75.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53e547fff02cbe3c87000140"
  ],
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "updated_at": 1444841249442,
  "created_at": 1443740048406,
  "activated_at": 1443740059652,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5612f028f02cbe7c84002e3e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5612e677f02cbed3e1002bcf",
  "title": "Buy three get one free guitar lessons",
  "notes": "Buy three guitar lessons get one free. $75 value for only $56.25",
  "terms": "Available for a limited time!",
  "value": "75.0",
  "price": "56.25",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5612ef22f02cbed0e3002d6a"
  ],
  "user_id": "5612e5c7f02cbe734d002bc4",
  "updated_at": 1444841389749,
  "created_at": 1444081704891,
  "activated_at": 1444084572718,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "561441adf02cbe6782001cce",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5612e677f02cbed3e1002bcf",
  "title": "Buy 3 months of guitar lessons, get one month free.",
  "notes": "Buy 3 months of guitar lessons, get one month free. 16 lessons for the price of 12.",
  "terms": "Can't be used with other deals. First time students only.\r\nAvailable for a limited time!",
  "value": "300.0",
  "price": "225.0",
  "limit": "",
  "limit_per_customer": 1,
  "listing_ids": [
    "5612ef22f02cbed0e3002d6a"
  ],
  "user_id": "5612e5c7f02cbe734d002bc4",
  "updated_at": 1444841361838,
  "created_at": 1444168109389,
  "activated_at": 1444168935817,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "56156769f02cbedb960024a4",
  "active": false,
  "vouchers_count": 2,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "$50 Gift Card Value for $40",
  "notes": "Come get your car washed at the best wash in town with the best service.\r\n\r\nWhile Supplies Last!!! Only 20 Available!!!!",
  "terms": "$50 gift card must be redeemed at the wash: 510 W Bosanko Ave\r\n",
  "value": "50.0",
  "price": "40.0",
  "limit": 20,
  "limit_per_customer": 5,
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1452722151773,
  "created_at": 1444243305831,
  "activated_at": 1444243314403,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "56157fcbf02cbeab3200272a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "$10 Gift Card",
  "notes": "$10 gift card to the best car wash in town with the best service ",
  "terms": "Bring receipt in to the car wash to redeem. ",
  "value": "10",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1470076150984,
  "created_at": 1444249547384
}
deals <<{
  "_id": "56157fcbf02cbed32300272b",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "$10 Gift Card",
  "notes": "$10 gift card to the best car wash in town with the best service ",
  "terms": "Bring receipt in to the car wash to redeem. ",
  "value": "10",
  "price": "10",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1470076098930,
  "created_at": 1444249547457,
  "activated_at": 1444249550273,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "56158016f02cbe5f51002735",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "$25 Gift Card",
  "notes": "$25 gift card to the best car wash in town with the best service ",
  "terms": "Must redeem at the car wash: 510 W Bosanko Ave",
  "value": "25",
  "price": "25",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1470076100623,
  "created_at": 1444249622087,
  "activated_at": 1444249624921,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5615807df02cbed65d0025f1",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55c0dffff02cbec9980048b6",
  "title": "$100 gift card for $79.99",
  "notes": "This gift card is valued at $100 and will be sold for $79.99\r\n\r\nLIMITED SUPPLIES!!!! Only 15 available",
  "terms": "Must be redeemed at the car wash. ",
  "value": "100.0",
  "price": "79.99",
  "limit": "",
  "limit_per_customer": 15,
  "listing_ids": [
    "5615662bf02cbef6d10024df"
  ],
  "user_id": "55c0dffff02cbec9980048b3",
  "updated_at": 1470076102176,
  "created_at": 1444249725338,
  "activated_at": 1444249728946,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5616c38cf02cbe82c3002fc2",
  "active": false,
  "vouchers_count": 1,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "$35.00 Gift Card for only $29.99!",
  "notes": "Must Present Gift Card before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "35.0",
  "price": "29.99",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1471908427657,
  "created_at": 1444332428052,
  "activated_at": 1444332457751,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5616c410f02cbee623002f0c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "$115.00 Gift Card for only $99.99!",
  "notes": "Cannot be combined with any other offer. \r\nMust Present Gift Card before Purchasing. \r\nGift Card Expires on January 31, 2016",
  "terms": "Valid during normal working hours.",
  "value": "115.0",
  "price": "99.99",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1444332641222,
  "created_at": 1444332560941,
  "activated_at": 1444332584923,
  "deleted_at": 1444332737188
}
deals <<{
  "_id": "5616c515f02cbecfec002f19",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "$115.00 Gift Card for only $99.99!",
  "notes": "Must Present Gift Card before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "115.0",
  "price": "99.99",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1471908358502,
  "created_at": 1444332821747,
  "activated_at": 1444332825698,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "561d845df02cbe10e2005a14",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "537fc71af02cbef1c2001059",
  "title": "$40.00 Gift Card for Only $20.00!",
  "notes": "Double your money at Custom Truck!\r\nMust Present Gift Card before Purchasing. \r\nLimited time offer.",
  "terms": "One coupon per household per year.\r\nValid during normal working hours.",
  "value": "40.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "537fd226f02cbee6d80011d1",
    "537fcc92f02cbe71ec001189"
  ],
  "user_id": "4fb5539308dea138060001a5",
  "updated_at": 1460404401276,
  "created_at": 1444775005287,
  "activated_at": 1444775031659,
  "referral_code": "boyd"
}
deals <<{
  "_id": "561fe522f02cbed725006c31",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "Gift Card for $25.00",
  "notes": "Must Present Gift Card before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "25.00",
  "price": "25.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1471908399824,
  "created_at": 1444930850477,
  "activated_at": 1444930858764,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "561fe546f02cbeb9a7006c37",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "title": "Gift Card for $15.00",
  "notes": "Must Present Gift Card before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "15.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "561681d5f02cbe8460002d97"
  ],
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "updated_at": 1471908392206,
  "created_at": 1444930886190,
  "activated_at": 1444930888890,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "56212003f02cbe72a900718a",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$15.00 Gift Card at Schmidty's Burgers!",
  "notes": "GOOD DURING REGULAR BUSINESS HOURS.",
  "terms": "",
  "value": "15.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1469124882221,
  "created_at": 1445011459775,
  "activated_at": 1445011466625
}
deals <<{
  "_id": "5621207cf02cbed8f4007193",
  "active": false,
  "vouchers_count": 4,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$25.00 Gift Card at Schmidty's Burgers!",
  "notes": "GOOD DURING REGULAR BUSINESS HOURS.",
  "terms": "",
  "value": "25.00",
  "price": "25.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1469124880464,
  "created_at": 1445011580273,
  "activated_at": 1445011584898
}
deals <<{
  "_id": "562120d9f02cbe8f07007194",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "title": "$50.00 Gift Card at Schmidty's Burgers!",
  "notes": "GOOD DURING REGULAR BUSINESS HOURS.",
  "terms": "",
  "value": "50.00",
  "price": "50.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55107421f02cbe6369001592"
  ],
  "user_id": "55148038f02cbeeac2002c8e",
  "updated_at": 1469124878720,
  "created_at": 1445011673465,
  "activated_at": 1445011676631
}
deals <<{
  "_id": "5628086df02cbe47da00232c",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "56141846f02cbe4b23001bbc",
  "title": "$100.00 Gift Card for only $85.00!",
  "notes": "Must Present Gift Card before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "100.0",
  "price": "85.0",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "56141c7af02cbeac4b001b4e"
  ],
  "user_id": "5627f042f02cbe046500214c",
  "updated_at": 1460401635283,
  "created_at": 1445464173444,
  "activated_at": 1445464183871,
  "referral_code": "boyd"
}
deals <<{
  "_id": "5628091af02cbe79390023c4",
  "active": false,
  "vouchers_count": 0,
  "profile_id": "56141846f02cbe4b23001bbc",
  "title": "$50.00 Gift Card for only $45.00!",
  "notes": "Must Present Gift Card before Purchasing.",
  "terms": "Valid during normal working hours.",
  "value": "50.0",
  "price": "45.0",
  "limit": 20,
  "limit_per_customer": "",
  "listing_ids": [
    "56141c7af02cbeac4b001b4e"
  ],
  "user_id": "5627f042f02cbe046500214c",
  "updated_at": 1460401633086,
  "created_at": 1445464346133,
  "activated_at": 1445464367162,
  "referral_code": "boyd"
}
deals <<{
  "_id": "562fddd3f02cbef53c0005b1",
  "active": true,
  "vouchers_count": 9,
  "profile_id": "53a471a1f02cbeda2a002b1c",
  "title": "Grille From Ipanema $25.00 Gift Card!",
  "notes": "Must Present Gift Card before Purchasing. Must use entire ammount in one visit",
  "terms": "Valid during normal working hours.",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "53a472a4f02cbee325002e16"
  ],
  "user_id": "53a471a1f02cbeda2a002b1d",
  "updated_at": 1452884529408,
  "created_at": 1445977555812,
  "activated_at": 1445977565701,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "565e0fe9f02cbe48d4003229",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Special Gift Card",
  "notes": "Owners would like all gift cards to be printed and brought into shop",
  "terms": "",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": -62135395622000,
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1449080172902,
  "created_at": 1449005033779,
  "activated_at": 1449005088619,
  "referral_code": "bvo9552"
}
deals <<{
  "_id": "565e10bcf02cbe9be90033ab",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Why wait for a special ocassion",
  "notes": "Owners would like to have the customer print and bring into the store.",
  "terms": "",
  "value": "50.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": "",
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1450375471765,
  "created_at": 1449005244146,
  "activated_at": 1449005252728,
  "referral_code": "boyd"
}
deals <<{
  "_id": "565f3304f02cbefff1000220",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Helping the Lake City High School Deca Club",
  "notes": "Every time you buy this gift card a portion of the proceeds will go to the Lake City High School Deca Club",
  "terms": "Please bring in a printed copy of gift into the shop",
  "value": "10.0",
  "price": "10.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": "",
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1450375545475,
  "created_at": 1449079556649,
  "activated_at": 1449079560737,
  "referral_code": "boyd"
}
deals <<{
  "_id": "565f33eff02cbe6d7d000258",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Lake City High School Deca Club",
  "notes": "Every time you buy this gift card a portion of the proceeds will go to the Lake City High School Deca Club",
  "terms": "Please bring a copy of this gift card to the shop",
  "value": "20.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": "",
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1450370074209,
  "created_at": 1449079791098,
  "activated_at": 1449079806621,
  "referral_code": "boyd"
}
deals <<{
  "_id": "565f3481f02cbe38f200020c",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Lake City High School Deca Club",
  "notes": "Every time you buy this gift card a portion of the proceeds will go to the Lake City High School Deca Club",
  "terms": "Please bring in a hard copy of this gift card to the shop",
  "value": "30.0",
  "price": "30.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": "",
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1450375452474,
  "created_at": 1449079937061,
  "activated_at": 1449079941428,
  "referral_code": "boyd"
}
deals <<{
  "_id": "565f34c2f02cbe300700020e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Lake City High School Deca Club",
  "notes": "Every time you buy this gift card a portion of the proceeds will go to the Lake City High School Deca Club",
  "terms": "Please bring in a hard copy of tis gift card to the shop",
  "value": "40.0",
  "price": "40.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": "",
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1450375490927,
  "created_at": 1449080002974,
  "activated_at": 1449080006235,
  "referral_code": "boyd"
}
deals <<{
  "_id": "565f3501f02cbec4fe00021b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "4fb7452208dea126b90374f5",
  "title": "Lake City High School Deca Club",
  "notes": "Every time you buy this gift card a portion of the proceeds will go to the Lake City High School Deca Club",
  "terms": "Please bring in a hard copy of this gift card to the shop",
  "value": "50.0",
  "price": "50.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "4fb7452208dea126b90374f4"
  ],
  "expires_at": "",
  "user_id": "4fb553b208dea13806000a31",
  "updated_at": 1450375571280,
  "created_at": 1449080065290,
  "activated_at": 1449080068970,
  "referral_code": "boyd"
}
deals <<{
  "_id": "5668896ff02cbee4a6000d21",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "SAndbaggerz Golf Gift Card",
  "notes": "",
  "terms": "Would like you to ring in a hard copy of this gift card, but not required.",
  "value": "10.00",
  "price": "10.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": "",
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1450375636623,
  "created_at": 1449691503576,
  "activated_at": 1449691516728,
  "referral_code": "boyd"
}
deals <<{
  "_id": "566889c8f02cbeda00000d17",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Sandbaggerz Gift Card",
  "notes": "",
  "terms": "Would like to have a hard copy of this gift card brought in, but not required.",
  "value": "20.00",
  "price": "20.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": "",
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1450375647608,
  "created_at": 1449691592369,
  "activated_at": 1449691596473,
  "referral_code": "boyd"
}
deals <<{
  "_id": "56688a20f02cbe2d68000c88",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Sandbaggerz Gift Card",
  "notes": "",
  "terms": "If possible please bring in a hard copy of this gift card, but not required.",
  "value": "30.00",
  "price": "30.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": "",
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1450375661577,
  "created_at": 1449691680186,
  "activated_at": 1449691684094,
  "referral_code": "boyd"
}
deals <<{
  "_id": "56688a73f02cbe74b0000d2a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Sandbaggerz Gift Card",
  "notes": "",
  "terms": "Please bring in a hard copy of this gift card if possible, but not required.",
  "value": "40.00",
  "price": "40.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": "",
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1450375678275,
  "created_at": 1449691763043,
  "activated_at": 1449691767410,
  "referral_code": "boyd"
}
deals <<{
  "_id": "56688a9bf02cbee948000d2a",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56674631f02cbe7a8e0007c7",
  "title": "Sandbaggerz Gift Card",
  "notes": "",
  "terms": "Please bring in a hard copy of this gift card if possible, but not required.",
  "value": "50.00",
  "price": "50.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56674707f02cbe7dcc0007ca"
  ],
  "expires_at": "",
  "user_id": "564cf543f02cbee2a4002ab0",
  "updated_at": 1450375690560,
  "created_at": 1449691803162,
  "activated_at": 1449691806857,
  "referral_code": "boyd"
}
deals <<{
  "_id": "566b108cf02cbe7c5c001b08",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Flair Sun Salon Gift Card",
  "notes": "Best Winter Sun Tanning In Town",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "10.00",
  "price": "10.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1450155507571,
  "created_at": 1449857164245,
  "activated_at": 1449857178426,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "566b1152f02cbe2b32001d62",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Falir Sun Salon Gift Card",
  "notes": "Best Winter Tan In Tan",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "20.00",
  "price": "20.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1450155484415,
  "created_at": 1449857362617,
  "activated_at": 1449857367977,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "566b119cf02cbe78e2001d65",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Flair Sun Salon Gift Card",
  "notes": "Best Winter Tan in Tan",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "30.00",
  "price": "30.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1450155466127,
  "created_at": 1449857436386,
  "activated_at": 1449857439580,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "566b11cff02cbe367f001da1",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Flair Sun Salon Gift Card",
  "notes": "Best Winter Tan in Tan",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "40.00",
  "price": "40.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1450155453190,
  "created_at": 1449857487958,
  "activated_at": 1449857491779,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "566b120af02cbe3957001b48",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Flair Sun Salon Gift Card",
  "notes": "Best Winter Tan in Tan",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "50.00",
  "price": "50.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1450155435513,
  "created_at": 1449857546449,
  "activated_at": 1449857549947,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "566b123cf02cbe2d1a001d6b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5653a95ef02cbed2db0001cf",
  "title": "Solar Flair Sun Salon Gift Card",
  "notes": "Best Winter Tan in Tan",
  "terms": "Print hard copy to show at store or show on your smartphone.",
  "value": "100.00",
  "price": "100.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5661d57df02cbefb940017c7"
  ],
  "expires_at": "",
  "user_id": "565f309ff02cbe6b2b0001ef",
  "updated_at": 1450155417178,
  "created_at": 1449857596780,
  "activated_at": 1449857600420,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "5682dde1f02cbe6e7e003761",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56442c6df02cbebdb30009e9",
  "title": "Gift card",
  "notes": "No cash refund  ",
  "terms": "Only valid for hair done by Tari Moore at stunning creations salon in post falls Idaho ",
  "value": "85",
  "price": "75",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56442ff4f02cbe44b6000a75"
  ],
  "expires_at": "",
  "user_id": "56442c6df02cbebdb30009e6",
  "updated_at": 1451417086532,
  "created_at": 1451417057543,
  "activated_at": 1451417086525
}
deals <<{
  "_id": "56a171ed47ffb0dfef00c46e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "title": "Pamper Package",
  "notes": "Pamper package\r\nTreat yourself or your\r\nspecial someone to a customized pamper package today. Choose three of\r\nthe following enticing services to enjoy on the special day of your choice:\r\nPedicure\r\nManicure\r\nDeep Condition\r\nUpdo\r\nHaircut & Style\r\nFacial Wax\r\nTheracure\r\n*Enjoy with one glass of wine or an ice cold beer as a thank you from all of us here at Stunning Creations Salon",
  "terms": "* age restrictions apply. ",
  "value": "120.0",
  "price": "99.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "569ff05e47ffb01d870080af"
  ],
  "expires_at": "",
  "user_id": "55c14d2af02cbeecaf000f78",
  "updated_at": 1453421254134,
  "created_at": 1453421037559
}
deals <<{
  "_id": "56a1762047ffb0162c00c519",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "title": "Pamper Package",
  "notes": "Pamper Package \r\nTreat yourself or your special someone to a customized pamper package today! Choose from three of the following enticing services to enjoy on the special day of your choice:\r\n\r\nPedicure \r\nManicure \r\nDeep Condition \r\nUpdo \r\nHaircut & Style \r\nFacial Wax \r\nTheracure \r\n* Enjoy one glass of wine or an ice cold beer as a thank you from all of us here at Stunning Creations Salon (age restrictions apply.",
  "terms": "",
  "value": "120.00",
  "price": "99.00",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "569ff05e47ffb01d870080af"
  ],
  "expires_at": "",
  "user_id": "55c14d2af02cbeecaf000f78",
  "updated_at": 1453422129452,
  "created_at": 1453422112584,
  "activated_at": 1453422129449
}
deals <<{
  "_id": "56c53361bd40b187a20082ee",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5487976df02cbe84660098ef",
  "title": "$20 Gift Card Stateline Subs and Pizza",
  "notes": "$20 Gift Card Stateline Subs and Pizza",
  "terms": "Must be redeemed in 1 visit.  Good during regular business hours.",
  "value": "20",
  "price": "20",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5490bb45f02cbe24e4000987"
  ],
  "expires_at": "",
  "user_id": "5487976df02cbe84660098ec",
  "updated_at": 1455764390230,
  "created_at": 1455764321059,
  "activated_at": 1455764390227
}
deals <<{
  "_id": "56c53422bd40b1b5b80082fa",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "5487976df02cbe84660098ef",
  "title": "$15 Gift Card Stateline Subs and Pizza",
  "notes": "$15 Gift Card Stateline Subs and Pizza",
  "terms": "Must be redeemed in 1 visit.  Valid during regular business hours",
  "value": "15",
  "price": "15",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "5490bb45f02cbe24e4000987"
  ],
  "expires_at": "",
  "user_id": "5487976df02cbe84660098ec",
  "updated_at": 1455764519479,
  "created_at": 1455764514214,
  "activated_at": 1455764519476
}
deals <<{
  "_id": "56c6739dbd40b1eaa7009b44",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55aeda05f02cbe0e7600aceb",
  "title": "$50 Gift Card for Hair by Jenny",
  "notes": "",
  "terms": "Must redeem in 1 visit.  Good during regular business hours.  Must contact seller via phone or email to schedule appointment.",
  "value": "50",
  "price": "50",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55aee0c7f02cbe3d6c00a2f4"
  ],
  "expires_at": "",
  "user_id": "55aed9a1f02cbe91fd00a0db",
  "updated_at": 1455846310070,
  "created_at": 1455846301607,
  "activated_at": 1455846310067
}
deals <<{
  "_id": "56c6768bbd40b1d4d1009b6e",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "55aeda05f02cbe0e7600aceb",
  "title": "$75 Gift Card for Hair by Jenny",
  "notes": "",
  "terms": "Must redeem in 1 visit.  Must contact seller to via phone or email to schedule appointment. Good during regular business hours.  Valid for Hair by Jenny at Traci & Charli's full service salon",
  "value": "75",
  "price": "75",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "55aee0c7f02cbe3d6c00a2f4"
  ],
  "expires_at": "",
  "user_id": "55aed9a1f02cbe91fd00a0db",
  "updated_at": 1455847058995,
  "created_at": 1455847051857,
  "activated_at": 1455847058992
}
deals <<{
  "_id": "56e0883a1aa7206aab00da3b",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d5f18ba5a015753e007e9f",
  "title": "$15 Mud Puddle Gift Card",
  "notes": "",
  "terms": "Must redeem in 1 visit. Good during regular business hours",
  "value": "15.0",
  "price": "15.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d5f361a5a015fc1c007f0c"
  ],
  "expires_at": "",
  "user_id": "56d5eff4a5a015afda007c99",
  "updated_at": 1457832762502,
  "created_at": 1457555514765,
  "activated_at": 1457555526481,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "56e088a61aa720a15000da43",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d5f18ba5a015753e007e9f",
  "title": "$20 Mud Puddle Gift Card ",
  "notes": "Paint your own ceramics.",
  "terms": "Must redeem in 1 visit. Good during regular business hours",
  "value": "20.0",
  "price": "20.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d5f361a5a015fc1c007f0c"
  ],
  "expires_at": "",
  "user_id": "56d5eff4a5a015afda007c99",
  "updated_at": 1457832790676,
  "created_at": 1457555622876,
  "activated_at": 1457555627537,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "56e08bb31aa72030aa00dade",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "56d5f18ba5a015753e007e9f",
  "title": "$25 Mud Puddle Gift Card",
  "notes": "Paint your own ceramics",
  "terms": "Must redeem in 1 visit. Good during regular business hours",
  "value": "25.0",
  "price": "25.0",
  "limit": "",
  "limit_per_customer": "",
  "listing_ids": [
    "56d5f361a5a015fc1c007f0c"
  ],
  "expires_at": "",
  "user_id": "56d5eff4a5a015afda007c99",
  "updated_at": 1457832957726,
  "created_at": 1457556403608,
  "activated_at": 1457556414682,
  "referral_code": "currentattractions"
}
deals <<{
  "_id": "57166e73e9330c29b6006a05",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53a6561df02cbeeeb800408f",
  "title": "$10 off $50",
  "notes": "Can use towards any service.",
  "terms": "Can't use more than one per visit.",
  "value": "50.00",
  "price": "40.00",
  "limit": 10,
  "limit_per_customer": "",
  "listing_ids": [
    "53a6572df02cbef3460041ac"
  ],
  "expires_at": 1467356400000,
  "user_id": "53a6561df02cbeeeb8004090",
  "updated_at": 1461087859111,
  "created_at": 1461087859111
}
deals <<{
  "_id": "57166eea1aa72009ad00b447",
  "active": true,
  "vouchers_count": 0,
  "profile_id": "53a6561df02cbeeeb800408f",
  "title": "$20 off $100",
  "notes": "Can use towards any service ",
  "terms": "Can't use more than one per visit ",
  "value": "100.00",
  "price": "80.00",
  "limit": 10,
  "limit_per_customer": "",
  "listing_ids": [
    "53a6572df02cbef3460041ac"
  ],
  "expires_at": 1467356400000,
  "user_id": "53a6561df02cbeeeb8004090",
  "updated_at": 1461087978859,
  "created_at": 1461087978859
}

deals.each do |d|
  desc = d[:notes].present? ? d[:notes] : d[:terms]
  latitude = nil
  longitude = nil
  if d[:locations].present?
    location = d[:locations].first
    if location.present? && location[:point].present?
      latitude = location[:point][0]
      longitude = location[:point][1]
    end
  end
  discount = Discount.new
  discount.active = false
  discount.name = d[:title]
  discount.price = d[:value]
  discount.discounted_price = d[:price]
  discount.discount_type = 'card'
  discount.description = desc
  discount.latitude = latitude
  discount.longitude = longitude

  discount.save(validate: false)

end

end
end