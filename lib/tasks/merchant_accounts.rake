namespace :merchant_accounts do

  desc "Create Merchant Accounts"
  task :create => :environment do
merchant_accounts = []

merchant_accounts << 
{
  "_id": "5159d749f02cbec8100018fa",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "profile_id": "5159cef9f02cbef99e0019fb",
  "access_token": "sk_live_tJiDblQk35MTLTQIoiQCZkp2",
  "livemode": true,
  "refresh_token": "rt_1ZLk5VexiG1DkCbCQOTNgWU6mNJZ6gl2ImvaIahtu08ixvcU",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_07eiNpr8p0RjRWZQXVVcr5eb",
  "stripe_user_id": "acct_1ZLUjeryO8GL29ylENzA",
  "scope": "read_write",
  "_type": "MerchantAccounts::Stripe",
  "user_id": "5159cc8bf02cbec7ed001360"
}
merchant_accounts << {
  "_id": "5178251df02cbef35c000c28",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "profile_id": "4fb6bab908dea15b5a06f40c",
  "access_token": "sk_live_b3tRHyuO85Sx77j7uVTjYc0n",
  "livemode": true,
  "refresh_token": "rt_1hxb5gXzrKYyiT2QsnrQRYMCTSXsMTGqG0GV9a2SH3GlefVp",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_AaUbQGGnZ4z3ZZGS4fXwniqy",
  "stripe_user_id": "acct_1hwsuuUEe3gGIps8qekU",
  "scope": "read_write",
  "_type": "MerchantAccounts::Stripe",
  "user_id": "4fb553ad08dea138060008f5"
}
merchant_accounts << {
  "_id": "519190e1f02cbee006000055",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "profile_id": "5152f542f02cbecc3f0000cd",
  "access_token": "sk_live_QflT1CL8T88fEDax9OGXa5qh",
  "livemode": true,
  "refresh_token": "rt_1pBSGFqOU5sFC5FKUNb0pMUb493jPbd26PZi0En8UlnP40Sq",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_CXOlfzv0QkcBVoYoCjXedSie",
  "stripe_user_id": "acct_1pBExgu9yuS1O2iUtfly",
  "scope": "read_write",
  "_type": "MerchantAccounts::Stripe",
  "user_id": "514db52bf02cbeb3fd0007e5"
}
merchant_accounts << {
  "_id": "51d13296f02cbe9bbb00130d",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "51d13177f02cbe0271001098",
  "email": "valerie@coursesforlife.com",
  "user_id": "51d130f2f02cbe9ef20012ae"
}
merchant_accounts << {
  "_id": "521a3a05f02cbec0960020fd",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "521a3822f02cbe5901001f11",
  "email": "mdull@therightfitsports.com",
  "user_id": "521a37fcf02cbe767b001f0d"
}
merchant_accounts << {
  "_id": "5255bf31f02cbef6120087b3",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5255be86f02cbed6980087b1",
  "email": "jcoverby@comcast.net",
  "user_id": "5255bde6f02cbed2710087a0"
}
merchant_accounts << {
  "_id": "5271798cf02cbe26e0017570",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "51f93b4ef02cbe45760007ca",
  "email": "abigailgibson3712@gmail.com",
  "user_id": "51f8116ef02cbe99e7000247"
}
merchant_accounts << {
  "_id": "52734edbf02cbe3eab00016d",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "50b510829d8c211d6200005a",
  "email": "jjcutt@gmail.com",
  "user_id": "504651719d8c2124b2002cf6"
}
merchant_accounts << {
  "_id": "527fb35af02cbea1dd0036ee",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "527fb1fdf02cbe0d470039b6",
  "email": "lmeyer323@gmail.com",
  "user_id": "527fb18af02cbe17310036e4"
}
merchant_accounts << {
  "_id": "528af271f02cbeb08800c13a",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "profile_id": "4fb73e8b08dea126b9026615",
  "access_token": "sk_live_ufUuugz9fE28IHLo4ya3Vezt",
  "livemode": true,
  "refresh_token": "rt_2y2rfVTJEGatVW2bZ5kppBpuCYFA5zxOxOGj00EHkCSHgUZI",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_HTZHor0QBYJmh4wgKhSdlT7J",
  "stripe_user_id": "acct_102y2i2m0Egxb29L",
  "scope": "read_write",
  "_type": "MerchantAccounts::Stripe",
  "user_id": "4fb553b408dea13806000aad"
}
merchant_accounts << {
  "_id": "52a0d0e3f02cbe4f9801d532",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "52a0d0b7f02cbe1dca01c3b6",
  "email": "jessica.gaffney@eagles.ewu.edu",
  "user_id": "528c2fbef02cbe1d1400d042"
}
merchant_accounts << {
  "_id": "52fd8879f02cbe1d5603af7e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "51756d51f02cbeece0000023",
  "email": "business@techwarrior.biz",
  "user_id": "51604685f02cbe9b75000537"
}
merchant_accounts << {
  "_id": "52fe7407f02cbeb3d903bd79",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "52fd5a05f02cbe20e603aee0",
  "email": "phillip@needsmet.us",
  "user_id": "52fa9f6ff02cbe4795037105"
}
merchant_accounts << {
  "_id": "531e142cf02cbeecd9006915",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "531e1330f02cbe314e00700d",
  "access_token": "sk_live_ISQaGcE4UmiroTKM1qQPG9kr",
  "livemode": true,
  "refresh_token": "rt_3dqusDO0Jt82rbXV5SPmJg3cg2CJKSMXAUyY2idHlncCmhtz",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_yiXEllgEttONpy4tgh06Dvn7",
  "stripe_user_id": "nY9eZXlEdLRWRRpgiGNVbtrSJrwafNvM",
  "scope": "read_write",
  "user_id": "531e08eef02cbe781b006e76"
}
merchant_accounts << {
  "_id": "5330f1cbf02cbe2ab2000381",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "50ab04ffcfc07c153d0000bc",
  "email": "truenorthscholz@gmail.com",
  "user_id": "4fb5538e08dea13806000025"
}
merchant_accounts << {
  "_id": "533cdedaf02cbece39000ec7",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "533cd74bf02cbe1f6000091a",
  "email": "thecdae@gmail.com",
  "user_id": "533cd74bf02cbe1f6000091b"
}
merchant_accounts << {
  "_id": "534d6d9cf02cbebd950042b2",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "534d5a17f02cbe0440005142",
  "email": "carolyn@paycheckconnection.com",
  "user_id": "534d5a17f02cbe0440005143"
}
merchant_accounts << {
  "_id": "535abf0af02cbe7ca5001fdd",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "535aba6cf02cbe3b50001b93",
  "email": "tinamonteleon@hotmail.com",
  "user_id": "535aba6cf02cbe3b50001b94"
}
merchant_accounts << {
  "_id": "535ee271f02cbebaea0003ec",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "535edc26f02cbea8190003e9",
  "email": "crazyearl3432@gmail.com",
  "user_id": "535edc26f02cbea8190003ea"
}
merchant_accounts << {
  "_id": "53625fc4f02cbe5db5006036",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "4fb7259d08dea126b9004824",
  "email": "tracie.cawley@dfmgroup.com",
  "user_id": "4fb5539b08dea138060003f1"
}
merchant_accounts << {
  "_id": "53629892f02cbedea6006d8e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "536295a6f02cbeae11006d6b",
  "email": "kevin@lonewolftherapies.com",
  "user_id": "536295a6f02cbeae11006d6c"
}
merchant_accounts << {
  "_id": "53629bcaf02cbec98e005f4b",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5362969bf02cbec5dd005f98",
  "email": "affordableprocessserver@gmail.com",
  "user_id": "5362950bf02cbea065005f5a"
}
merchant_accounts << {
  "_id": "5367f71ef02cbed61300c9ce",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5367e7bdf02cbeb00000d743",
  "email": "pat@peakre.com",
  "user_id": "5367e7bdf02cbeb00000d744"
}
merchant_accounts << {
  "_id": "53683de0f02cbe3c3b00ce58",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53683749f02cbeca4900e05f",
  "email": "captaincookoriginals@gmail.com",
  "user_id": "53683749f02cbeca4900e060"
}
merchant_accounts << {
  "_id": "5372c9b0f02cbe9f4b008efd",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5372ac10f02cbe732d008c9a",
  "email": "premiereautodetail@gmail.com",
  "user_id": "5372ac10f02cbe732d008c9b"
}
merchant_accounts << {
  "_id": "53763feef02cbe100d00204d",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "533f072bf02cbec521000030",
  "email": "mike@specializedsafety.net",
  "user_id": "533f072bf02cbec521000031"
}
merchant_accounts << {
  "_id": "53768d9ef02cbe8a750023d1",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "537681d6f02cbe232b002454",
  "user_id": "537681d6f02cbe232b002455",
  "access_token": "sk_live_suRVH6MJxnKWgeHq2WU3WG5p",
  "livemode": true,
  "refresh_token": "rt_42zZwfhVOhGcFeRhxjG1bNZUXumogvigTlwtkHUWXHhHfJRx",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_j8Nyzvjv7St7kqBaWtlZxJ2q",
  "stripe_user_id": "acct_1042zR4I8uyJc2Uw",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "537cf59af02cbe8609006cfc",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 2499,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "537cec15f02cbe5912006c87",
  "email": "wildwallsgym@gmail.com",
  "user_id": "537cec15f02cbe5912006c88"
}
merchant_accounts << {
  "_id": "537d2ecff02cbebc89000091",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "537d2cb0f02cbe54ab000067",
  "email": "service@haydenautoservice.com",
  "user_id": "537d2cb0f02cbe54ab000068"
}
merchant_accounts << {
  "_id": "5385115bf02cbea959004f0d",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "537fc71af02cbef1c2001059",
  "user_id": "4fb5539308dea138060001a5",
  "access_token": "sk_live_NJNNtKVi7PPdKMZwswg3Bii9",
  "livemode": true,
  "refresh_token": "rt_477HcuAiBlvJHtqI5RCuJoLOoBZr0je4MYvVZGAPnK2IwLnF",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_F37FKKDgQcdD7w3d077gTim3",
  "stripe_user_id": "acct_10477C4DVhLOMerF",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5388d48a9c058ec222000813",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "535e8c27f02cbe3bc9003b63",
  "user_id": "4fb553b108dea138060009fd",
  "access_token": "sk_live_xXK3ehuzOCnTt0WVYtAVjG09",
  "livemode": true,
  "refresh_token": "rt_48BYM1CenacpHXzmzDM0VYgTuf99oHPzpZN6bfeTYql2mr15",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_xmaeQDJ0n2lgQ04nkcfPJBBN",
  "stripe_user_id": "acct_1048B342JHywm2HG",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5388eb9f9c058e1f0f0008ca",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5388e6049c058ed8e30009d3",
  "email": "svaorders@gmail.com",
  "user_id": "5388e6049c058ed8e30009d4"
}
merchant_accounts << {
  "_id": "538fb281f02cbe6e9f000291",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "538faf13f02cbebb7f0000f9",
  "user_id": "538fade2f02cbe3053000266",
  "access_token": "sk_live_mftiA9gfnn74YqdASOC7KGeN",
  "livemode": true,
  "refresh_token": "rt_4A8XtyNtgMKYfssUUXRI2VDfEr9wQD9EhI6wBVgz9pumbS3I",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_KJOxvN03u9yZzJmd2VlKYEg1",
  "stripe_user_id": "acct_104A8M4ORMSzkICi",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "539208cef02cbe07d400093a",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5391fdedf02cbecf46000aa7",
  "user_id": "5391fdedf02cbecf46000aa8",
  "access_token": "sk_live_exR7xaPEjMBfdSmhsDS37Q1x",
  "livemode": true,
  "refresh_token": "rt_4AniWjKKiNh5maP5bs2f0kmsSj0qkGIeByd055lb2ZkpWqki",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Pnww0Q37GJveOJqAq0jraans",
  "stripe_user_id": "acct_104AnW4znJyTN2z5",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5392301cf02cbe0506000e8e",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 3999,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53922124f02cbec7ac000d93",
  "user_id": "53922124f02cbec7ac000d94",
  "access_token": "sk_live_7XvuVtyWMEVaj6wMkgjsTT85",
  "livemode": true,
  "refresh_token": "rt_4AqQghXFDP7i1BnZ7TRui7pwa6DmfcoPulcvJzTcvFEXpE2T",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_lXizDUfgiOkGnRxt94wAYpmh",
  "stripe_user_id": "acct_104AqH42Jj9q96El",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5392456bf02cbec30c0010b5",
  "active": true,
  "purchases": 4,
  "total_revenue_cents": 5122,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "538e4d2f9c058e2308000ebd",
  "user_id": "538e4d2f9c058e2308000ebe",
  "access_token": "sk_live_wobliWBxGGfUmapJg1zxWvUE",
  "livemode": true,
  "refresh_token": "rt_4ArsmdfECsE4OPswHLKmKOmplpdhCjCtLEhst9jHlaSZYg7Q",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_WHcoPlMLsWTsDFVa6thRqMEP",
  "stripe_user_id": "acct_104A7q4OggmOlWjJ",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5395370ef02cbef4de003420",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 5000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "538e6b579c058eccfc001428",
  "user_id": "538e6b579c058eccfc001429",
  "access_token": "sk_live_1CuvsRoBkO7DI5fnlkcf1FUv",
  "livemode": true,
  "refresh_token": "rt_4BhkN5SvsncZWrfziQQHnKv19Ri3O8agRbAqhawC2FYuYgNK",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_61AaeZiYjozM0DXVh1XA8evi",
  "stripe_user_id": "acct_104Bhc4FpRxv5ZTS",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5395f6a0f02cbe72590038d3",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 6000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "538e04b59c058e34c4000bb9",
  "user_id": "538e04b59c058e34c4000bba",
  "access_token": "sk_live_tmJKHBVGT9AiX3mTPTXb77bD",
  "livemode": true,
  "refresh_token": "rt_4Buvbj8l5AhD3SrIOk2E5PCuNcoSkbgJFJCQfdePaDHqbBO8",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_nTAnsqz2rxw7sYmOC2SakNj6",
  "stripe_user_id": "acct_104Bum4EehVsg9PV",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53973894f02cbe6bb200511a",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "4fb7452208dea126b90374f5",
  "email": "creativetouchfloral@netzero.com",
  "user_id": "4fb553b208dea13806000a31"
}
merchant_accounts << {
  "_id": "53979221f02cbe9180005abb",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "537e8090f02cbe2016000003",
  "user_id": "537e6c7bf02cbe89990005c1",
  "access_token": "sk_live_At9QxqjO28Dr5sUqP4wqJA6I",
  "livemode": true,
  "refresh_token": "rt_4CNF3Nu5OlOdzhMvr4t3IEWLCQpjkp4aK3zP9Ggz1zbXsxMG",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_qqD5HTQ2fR99yim55ti0GIa5",
  "stripe_user_id": "acct_104CMl4tRPjRZqN3",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "539a2002f02cbedbad001443",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5399bf83f02cbe179a000e1e",
  "email": "sarahshearer7@hotmail.com",
  "user_id": "5399bf83f02cbe179a000e1f"
}
merchant_accounts << {
  "_id": "539b2a99f02cbe7e54000008",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "539a2808f02cbe889b0014d5",
  "user_id": "539a2808f02cbe889b0014d6",
  "access_token": "sk_live_wyWQ2ziPepiZL1qIXBYOoGvq",
  "livemode": true,
  "refresh_token": "rt_4DObSjsFi8OaggNCQikxMvykX4dZ4vTgPb7tn9NSdkeh3zuQ",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_FU4yiLR1v8IEEeeBCCDfJiTH",
  "stripe_user_id": "acct_104DOW4s2wAwPxOf",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53a1de57f02cbeebd5000628",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53979932f02cbe33a70058bc",
  "user_id": "53979932f02cbe33a70058bd",
  "access_token": "sk_live_nI0pZ18wd3MJnLGuO6Dl71Zu",
  "livemode": true,
  "refresh_token": "rt_4FIf2gKbAju1EYPdHCcqO5vvsurrk01GEa0qBsAFQEhnaAwu",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_CyIsSgD2AGpk7MEdrVZt4huz",
  "stripe_user_id": "acct_104FIW4A4acVBiFX",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53a33e6cf02cbe8bae001af6",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "539b294ef02cbe0ae1000014",
  "user_id": "539b294ef02cbe0ae1000015",
  "access_token": "sk_live_s4OnXx7GkEyzb3uSXDQDYyCf",
  "livemode": true,
  "refresh_token": "rt_4FgtCwd0YAs9cQBwu5iQgEirZfr3wHtPShN9ZETcARqwKMq9",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Nn67kHrtS3p3JEshRVev4Fww",
  "stripe_user_id": "acct_104C134cSldOzPRe",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53a4a422f02cbe8d1b003218",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "536295a6f02cbeae11006d6b",
  "user_id": "536295a6f02cbeae11006d6c",
  "access_token": "sk_live_1NVDQoA3ajnG9GRp4RoGi5X7",
  "livemode": true,
  "refresh_token": "rt_4G5WU0UV8Zf5iZ9m7vhuuIZJPiTaKh8GOjk0T6Hj8atvrIKl",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_iGUnUJGOaBicolgP5dxA860k",
  "stripe_user_id": "acct_104G5P4xOyW8aDvf",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53a65f6bf02cbe054e0042a4",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 2800,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53a6561df02cbeeeb800408f",
  "user_id": "53a6561df02cbeeeb8004090",
  "access_token": "sk_live_vlpzHpWi2WSJNt09IYna9bSE",
  "livemode": true,
  "refresh_token": "rt_4Ga1Z5cWhtApzyL2ZizvOSuRDP4X3iCpnRw42XBlUSWdwtMe",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_y9MUSuJUTkZjuJ4k3vSaCgqU",
  "stripe_user_id": "acct_104GZx4gP5VSLg1b",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53bc5697f02cbea255000dc9",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "501c418bcfc07c5166000141",
  "user_id": "501c4079cfc07c51600001d3",
  "access_token": "sk_live_4Moz7Kx7N35oFEnq1xCYmdHK",
  "livemode": true,
  "refresh_token": "rt_4MozPCvj7Is8CUcCCL6enymgHr92dPG1FPKpqM4BYoAPzUEg",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_4MozJsRwKnCYDZ5F5EB70gOM",
  "stripe_user_id": "acct_14E5Fh4REbexOpZc",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53c33555f02cbe602e001d22",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 3500,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53c32e56f02cbef5890021fc",
  "user_id": "53c32cc5f02cbe2b1e002092",
  "access_token": "sk_live_4Om14WobmeGo7ewAlSPv0BQv",
  "livemode": true,
  "refresh_token": "rt_4Om1DB2YJKoUcEjyuMfWEyDLMrRYloPIx0I0H2K7ZV8QeKLi",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_4Om1UPE0XoT1kl3fsbASsTR1",
  "stripe_user_id": "acct_14FyEJ49oXLJtGCf",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53cd2f50f02cbea007005bb2",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53bdae9af02cbea133001e19",
  "email": "nlacy1312@gmail.com",
  "user_id": "53bdae9af02cbea133001e1a"
}
merchant_accounts << {
  "_id": "53cd74e1f02cbe83fb000292",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "539f5692f02cbe13560019cb",
  "user_id": "539f5692f02cbe13560019cc",
  "access_token": "sk_live_4RgZxPtDCk31EJrVsqpfVDzK",
  "livemode": true,
  "refresh_token": "rt_4RgZiLKgWHYKRl3ziNHp2CpCBCbHqZduoWuHo36V2hbgYaqp",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_4RgZnPfnizMTlJub2bBoriBj",
  "stripe_user_id": "acct_14ImIX4cIcU03TPo",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "53d03d46f02cbecd2c00000c",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "51237801f02cbe8d2200023e",
  "email": "matthew.c.cummings@gmail.com",
  "user_id": "512375acf02cbe88ea00021e"
}
merchant_accounts << {
  "_id": "53d931b4f02cbe236a00020f",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53cebe84f02cbea8f2002758",
  "email": "marchsabatke@fastmail.fm",
  "user_id": "53d93032f02cbeb2e80000dc"
}
merchant_accounts << {
  "_id": "53fd54a8f02cbe2e3f0000b8",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53fce68df02cbede93000143",
  "email": "egoaasd@hotmail.com",
  "user_id": "53fab66cf02cbebe46005692"
}
merchant_accounts << {
  "_id": "53ffa463f02cbe2b43000da5",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53f64834f02cbecf0e003cab",
  "email": "sandy.tarbox@gmail.com",
  "user_id": "53ff89fff02cbed873000d0c"
}
merchant_accounts << {
  "_id": "5415316ef02cbe99870009d6",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 14500,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5408b6e0f02cbe4d40003e11",
  "email": "anastasiashealthandwellness@gmail.com",
  "user_id": "540df46ef02cbefd15000075"
}
merchant_accounts << {
  "_id": "5436f832f02cbe360f002543",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5436e1fef02cbe4273002485",
  "email": "awardstetcidaho@gmail.com",
  "user_id": "5436f508f02cbed7cb002263"
}
merchant_accounts << {
  "_id": "54398b2af02cbe1f3e00146d",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54206248f02cbee8bc001342",
  "email": "jedbadders@gmail.com",
  "user_id": "542469b3f02cbeab87000008"
}
merchant_accounts << {
  "_id": "5447eed6f02cbe673400002b",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 5200,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "4fb7259d08dea126b9004824",
  "user_id": "4fb5539b08dea138060003f1",
  "access_token": "sk_live_4bv3mkOkx3lrxH1uufnMDtWd",
  "livemode": true,
  "refresh_token": "rt_50UGAPR41GnFuQ9ZhCoyQ10wVBpDBopiTerYQWdtqtPSqAw4",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_1GJy0VQ1OfO8DmkjZ8Wv2jGO",
  "stripe_user_id": "acct_14q5WxCTXcxWavHV",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "544fc848f02cbecfd800c72f",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5432e269f02cbe1c7f0002e8",
  "user_id": "5432aeaef02cbed1a3000589",
  "access_token": "sk_live_hM0F8HCf9vbXq5WkL0SwRy76",
  "livemode": true,
  "refresh_token": "rt_52iXfX69eoy8w3SUbE5hrY63SFBXLCHMYTnlceYDK1grWQuT",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_4kDh4kYMY6WSr0W1KEUFvyOD",
  "stripe_user_id": "acct_14sctlGtHgUAWdWm",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "544fec23f02cbe8593000765",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "544fe97ef02cbeae36000680",
  "email": "ernestmitchell946@gmail.com",
  "user_id": "544fdaa6f02cbeb192000190"
}
merchant_accounts << {
  "_id": "5452cd2ff02cbe7f2a0043a0",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5452ccecf02cbe6a17004387",
  "email": "choldread@gmail.com",
  "user_id": "514aea6ef02cbe68ac000546"
}
merchant_accounts << {
  "_id": "54566967f02cbe644900d24e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "545015cdf02cbeb87b0012a4",
  "email": "coolcoyote@frontier.com",
  "user_id": "54512088f02cbec14d00454c"
}
merchant_accounts << {
  "_id": "54593dfdf02cbe34ba00e9d1",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54592ba5f02cbe251b00f119",
  "email": "jennifervonbehren@gmail.com",
  "user_id": "54592ba5f02cbe251b00f116"
}
merchant_accounts << {
  "_id": "5463ca1af02cbe4f89000e87",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 12000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "545930acf02cbe590000e7db",
  "user_id": "545930acf02cbe590000e7d8",
  "access_token": "sk_live_VSBtWyEppuXde01aAUibqilM",
  "livemode": true,
  "refresh_token": "rt_58P1Ht4YOquiL06w9xawmwWTJ9zuIdYihhTRnoPa1Nih1nSE",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_D6pTB0miA60S7nWsKc4Jdfzl",
  "stripe_user_id": "acct_14y87tFmZHL9IiUd",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5463e81af02cbeface000fe9",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "5349bb4df02cbe5dab0019c4",
  "email": "gifts@iatemygift.com",
  "user_id": "5349badaf02cbe4dbc001b60"
}
merchant_accounts << {
  "_id": "546945e7f02cbe51d80034ee",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 1000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "541f71b1f02cbe8020000e8b",
  "user_id": "541f71b1f02cbe8020000e88",
  "access_token": "sk_live_KbytE0BYdU6A41UATKLoO264",
  "livemode": true,
  "refresh_token": "rt_59xcJIPX8IHzhUbK5eKe1T038PlZzgYH98Bm5rBAsbtxVxwq",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_9Owaf1b1B0AqMSVCO1GyIBhG",
  "stripe_user_id": "acct_14zdaHA2xOzEuWyk",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "546a4a6ef02cbea8b100361e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "546a41c7f02cbeb1c0003867",
  "user_id": "546a41c7f02cbeb1c0003864",
  "access_token": "sk_live_2xeCqBvvMOYooofNa8gidFKl",
  "livemode": true,
  "refresh_token": "rt_5AFY1kE0220XOL6cD2bachHcX0Ot1m4pVrIcX9e1N3rTYqz4",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_UaKVPiOq2ub4BhgBSjyNdiuo",
  "stripe_user_id": "acct_14zuqJLHyWPo8g9x",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "546a4a91f02cbe907300388b",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "546a41c7f02cbeb1c0003867",
  "email": "sheilagracepage@gmail.com",
  "user_id": "546a41c7f02cbeb1c0003864"
}
merchant_accounts << {
  "_id": "546d1cb1f02cbeecc3000e7e",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 6796,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53dab96ff02cbe466d001cde",
  "user_id": "53dab96ff02cbe466d001cdf",
  "access_token": "sk_live_FwJIpvMcIqCCJKeTGm7XCfkC",
  "livemode": true,
  "refresh_token": "rt_5B3FiSCMm4UPo1IIQ6ZJmAsSPifzuDCzI29keYgYsNBHtrV9",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_SosP0BR6YeJ5iRMuNHJuBHJD",
  "stripe_user_id": "acct_150dXWE8046y1jzp",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "54726878f02cbef617000907",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "547260adf02cbebd1c000893",
  "email": "pamsbags2013@gmail.com",
  "user_id": "547260adf02cbebd1c000890"
}
merchant_accounts << {
  "_id": "547e824bf02cbe67fb004251",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "546e6a7af02cbe10de0001ae",
  "email": "joesam64@msn.com",
  "user_id": "54738ba9f02cbeb8f8000dd6"
}
merchant_accounts << {
  "_id": "5480974df02cbe63880053d8",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53519450f02cbe1b2d005909",
  "email": "atvbarn@hotmail.com",
  "user_id": "53519450f02cbe1b2d00590a"
}
merchant_accounts << {
  "_id": "5489d109f02cbe90dc00f5cd",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5489cea2f02cbe0ef300f904",
  "user_id": "5489cea2f02cbe0ef300f901",
  "access_token": "sk_live_hKaAGV7B8x8mU8MPMHi4n2en",
  "livemode": true,
  "refresh_token": "rt_5JCxfADmEajoQuHxd15k1beW2doTJyDf8YLYF5g1OJDt4zMv",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ffxkp8XaKMVDLyDo5XdOk1Ia",
  "stripe_user_id": "acct_158aU6CrLjRNNIjo",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "54908438f02cbe11a700083c",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 2000,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "4fbd64f09d8c215b1e0001a6",
  "email": "dspiker@marketpad.net",
  "user_id": "4fb5538e08dea1380600000d"
}
merchant_accounts << {
  "_id": "54987ea4f02cbeafe2002c50",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 1000,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "543ea063f02cbec4d7000420",
  "email": "annette.desjardins@yahoo.com",
  "user_id": "543ea063f02cbec4d700041d"
}
merchant_accounts << {
  "_id": "54af06bcf02cbe37aa0001dd",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "50b65c5c9d8c213c9a00019d",
  "user_id": "4fb5539208dea13806000135",
  "access_token": "sk_live_FDnRAh7HDxZQ5BIJ1W5RqQB5",
  "livemode": true,
  "refresh_token": "rt_5TmVb4GemS7e1Z17OTc31ZFdax3AGs7BNezBlPP8AaG0ZSN9",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ZTkwJO4BPtUULCm7iZzwUmzm",
  "stripe_user_id": "acct_15Ioo8Bx0oYS4AGD",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "54b71e9bf02cbeea43000c4a",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "54ad9f9af02cbe950100199b",
  "user_id": "54aed4cbf02cbe47d000008b",
  "access_token": "sk_live_lCqLBveFDhNUWmANsntsb1yX",
  "livemode": true,
  "refresh_token": "rt_5W55qpBKsMP58OsFyuzDu2x65oG3d0Xx3tRfg8DjxNVKuNOc",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_urh14kyMWjmnUforER7KPyUg",
  "stripe_user_id": "acct_15JAKwFeTvkMv2tJ",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "54b984b8f02cbed91000074e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "53ceb12cf02cbed42a0026fe",
  "email": "charlotten@spokanefederal.com",
  "user_id": "53ceb12cf02cbed42a0026ff"
}
merchant_accounts << {
  "_id": "54ea3f72f02cbe5d27001bce",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 2500,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54ea3da9f02cbee1bb001ab4",
  "email": "kenzieway18@gmail.com",
  "user_id": "54ea3da9f02cbee1bb001ab1"
}
merchant_accounts << {
  "_id": "54ecd6d7f02cbe1836003492",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "542edf56f02cbe23bc0015ae",
  "email": "jillzee53@hotmail.com",
  "user_id": "5432ddd2f02cbe2aa10002be"
}
merchant_accounts << {
  "_id": "54f8dcadf02cbe91da001a5b",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54ee2196f02cbe319d004823",
  "email": "dave@clearheadlightgel.com",
  "user_id": "54ef6a31f02cbebcbd004c65"
}
merchant_accounts << {
  "_id": "54f9e0f2f02cbee913001eb1",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54f0d7aff02cbe2b66005858",
  "email": "kichoforrest@mac.com",
  "user_id": "4fb5539508dea13806000219"
}
merchant_accounts << {
  "_id": "54fa2e9df02cbefdc8002116",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54f749adf02cbe5464001017",
  "email": "eldonna@foodflavorfit.com",
  "user_id": "54fa2d53f02cbe55a3002231"
}
merchant_accounts << {
  "_id": "54ff2d4cf02cbea754000716",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54ee2198f02cbe23330043a6",
  "email": "sassysistersbling@yahoo.com",
  "user_id": "54ee2198f02cbe23330043a3"
}
merchant_accounts << {
  "_id": "5514469bf02cbebfe0002b69",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "54eb7dd2f02cbe475300222a",
  "email": "vacsthatsuck@live.com",
  "user_id": "55144567f02cbe5d95002b5f"
}
merchant_accounts << {
  "_id": "553e9e53f02cbe2e8500a4b2",
  "active": true,
  "purchases": 35,
  "total_revenue_cents": 138000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "550b3260f02cbe892e000e24",
  "user_id": "551451bbf02cbeaf22002ba4",
  "access_token": "sk_live_eqVEuvyHzrKPqMyv9guzprGQ",
  "livemode": true,
  "refresh_token": "rt_68aDRVA60CE8ZsryrExguYocFN6hOpi74h9kUUjsXbzWaFxA",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_u1UAMpTCb2ayETjMI8QcKsBS",
  "stripe_user_id": "acct_15wHurCZP1WSYSi7",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55414590f02cbed2e7001567",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Paypal",
  "profile_id": "554144a8f02cbe3d3d00151d",
  "email": "rusticpetal@gmail.com",
  "user_id": "554144a8f02cbe3d3d00151a"
}
merchant_accounts << {
  "_id": "5550ef35f02cbe8574002ad7",
  "active": true,
  "purchases": 32,
  "total_revenue_cents": 88871,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "55106dc5f02cbe6b5b0015d4",
  "user_id": "55148038f02cbeeac2002c8e",
  "access_token": "sk_live_jZHp50VWz6Y3sbZa5tR6TRkh",
  "livemode": true,
  "refresh_token": "rt_6DmtjRpmCBxkJ3inn8TcrK5GKxgyzE9GrgSPNRh61V6orsCF",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Pbd50v3Z321WIBbP0GpDGdE2",
  "stripe_user_id": "acct_15zY0gHGVUlovcFF",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5550f91ff02cbe8c5e002bf4",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "553fd0b0f02cbe6a6300cb88",
  "user_id": "5540ffc0f02cbe721100123f",
  "access_token": "sk_live_DrLp54sqJAIfOWVVTHnVom3W",
  "livemode": true,
  "refresh_token": "rt_6Dnav3KHQMkDT69Nxi3lzYB0AIwSGHkrHZChJE18odjAmfAa",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_J8Z8E8GyCJSp90p9cNSHQKZM",
  "stripe_user_id": "acct_161LtaLJrQzsRYxl",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55539038f02cbeec57003f25",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "545d2b6af02cbe4c8500ff07",
  "user_id": "555245edf02cbe522800339e",
  "access_token": "sk_live_wJzOZoOQ68g3ESfIUPlqNkix",
  "livemode": true,
  "refresh_token": "rt_6EXDqBFwbiV6pXrAbtX6t7p8MxA5rdb7rr2XbDXy1PBNXYCG",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Y8dcYC4ZJQ42IdTseVW4fM3e",
  "stripe_user_id": "acct_16240kB5tDoW60RP",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "555b7e4bf02cbef598000a85",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "551c2c4cf02cbefd33000d03",
  "user_id": "555b7418f02cbe2a3a000b0b",
  "access_token": "sk_live_GIUyfdjaMDBFB5DwuCGNmcfx",
  "livemode": true,
  "refresh_token": "rt_6Gmv699BZ14BwAjTcLSLmBG4x4aLGsy4KvdQb3ElVSZFKKDG",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_uLKJlHsFMX2Bk7EsQK20n5QT",
  "stripe_user_id": "acct_164EtZBYdvu9tD7P",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55778e9cf02cbe61e5010262",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5576ea21f02cbefb5700ffaf",
  "user_id": "5576ea21f02cbefb5700ffac",
  "access_token": "sk_live_VgJKDKVMfuVzOtn105GsmOQr",
  "livemode": true,
  "refresh_token": "rt_6OlKptKcbG01smqnzGWErKtclEADYx6itaf1LSxV5EUvyvSO",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Pl42eO2FVQul7Sx4Q0Xyrtis",
  "stripe_user_id": "acct_16BmxREdKDMHATpc",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5583225cf02cbe714a00128e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5583030ff02cbe27d80010da",
  "user_id": "558318daf02cbeb6c8001246",
  "access_token": "sk_live_KQftoJZH0341KNmjuH3RMH9I",
  "livemode": true,
  "refresh_token": "rt_6S3HS5vZVLKVvuZKuwxSMrxha2jkmB5JoHXybYwf9dwnORKJ",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_AuAB2nhnjdoL6gdszuH4JMI5",
  "stripe_user_id": "acct_16F8xXHBzUGt5Geh",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "558c7312f02cbe210500085f",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "558c6136f02cbeb9860007de",
  "user_id": "558c6ecbf02cbec019000909",
  "access_token": "sk_live_311rTFSELqU1cZ7sKVzA4Kgh",
  "livemode": true,
  "refresh_token": "rt_6UhOWRBjW58RZtgKLYZuKfCiyc6XOCwXuyAd7h6m2VZDgxhh",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_4FfBZKlixMeodvbL6pD1WaID",
  "stripe_user_id": "acct_16HhmiBTq3whfe33",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5591729ef02cbebab1002749",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "555e1c59f02cbe17ed002574",
  "user_id": "555fa5fbf02cbe21730033dc",
  "access_token": "sk_live_NfdojqjCOHfcpJz0RSSmAViq",
  "livemode": true,
  "refresh_token": "rt_6W7SlZBTcitxH65rSTJ6w4hPBtOhcJ0XN3vYTbx8rVw8WkNy",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_XgJmDNVvmWY8MKc2Gwx9FvsE",
  "stripe_user_id": "acct_16J4N7DO9NhaKWvR",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "559c429cf02cbe5d5a000b0b",
  "active": false,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5570d8fcf02cbe462c00e318",
  "user_id": "5570d8fcf02cbe462c00e315",
  "access_token": "sk_live_8odV4DeFTWgTQwOHcgW0as3C",
  "livemode": true,
  "refresh_token": "rt_6ZBHlD57EpWrtJuocYmR9Bqcc1SJWjsg3IDrBmvC4hYlUc0b",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_mRj0Q3ni22VqjWz1S28fm95U",
  "stripe_user_id": "acct_16M2ePIYrBks4Luf",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55a55165f02cbe0372003e42",
  "active": true,
  "purchases": 10,
  "total_revenue_cents": 25000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5592f256f02cbec07b002f8f",
  "user_id": "5282ba8df02cbed3de004efc",
  "access_token": "sk_live_Nb8qSkhxzEtCgHgUXfVkwxGv",
  "livemode": true,
  "refresh_token": "rt_6blWRfsKW5Z8cGcWv1Oz1hIXLt0uQm7SIi4O3r7sqDqMWl1R",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_RJ7r5d8qYs5zHCHGbxm2eYcC",
  "stripe_user_id": "acct_16OXoAFulOtCaWcM",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55aedd99f02cbedb1f00a0f7",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "55aeda05f02cbe0e7600aceb",
  "user_id": "55aed9a1f02cbe91fd00a0db",
  "access_token": "sk_live_uXrwhWm3rXA5K3S8uqseds1A",
  "livemode": true,
  "refresh_token": "rt_6eTiDkv4lcNUdSY0DBLXMxLXRWDhzTkZVRGd5bZ2hNGAMsmN",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_hYaEhxmiG6MiHs7ke44uVWIn",
  "stripe_user_id": "acct_16RAfhHItege38xM",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55b67ddbf02cbe5c75001f4e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "4fb6752e08dea15b5a00a7f6",
  "user_id": "4fb553a508dea138060006a9",
  "access_token": "sk_live_hAwcuQLYbST4b4Bg3g5LLwG6",
  "livemode": true,
  "refresh_token": "rt_6ge4MwJzMZFUkbj5UYrPQHYCCl1YHd8waYE1WlDxLy465VXD",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_63BAnkONXGCwyWu4EBR76jMA",
  "stripe_user_id": "acct_16TGhnKkBnTtRn7w",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55d22896f02cbe85e60033cf",
  "active": true,
  "purchases": 3,
  "total_revenue_cents": 4600,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "55d22627f02cbe45d700b51a",
  "user_id": "55d22627f02cbe45d700b517",
  "access_token": "sk_live_rUuCO9KnYQ6DPID0k6PauOKw",
  "livemode": true,
  "refresh_token": "rt_6oVUeitqIPf9HTG9w2Nvl8B0HHlicid2TzdAkmMgmDUxRi67",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_VX5mGNVuxEAZeRtGyeVNCdUd",
  "stripe_user_id": "acct_16asPjIGcAB7NDvN",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55f1dcdcf02cbebee4000671",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "55b68778f02cbe6fbf001d13",
  "user_id": "50f49248cfc07cad1b000030",
  "access_token": "sk_live_doGRj1slAOvMzk8gbs1hEWyx",
  "livemode": true,
  "refresh_token": "rt_6xW21As6jVfUN3oKnHU2TG4fbtpIOVpGOWTOQ5OGwp436Jl8",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_10wE28ohZqzVdPaOtJH6VSr6",
  "stripe_user_id": "acct_16jav2IMHfarOveX",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "55f87e0df02cbe0eca003179",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "4fb6b74008dea15b5a0661e3",
  "user_id": "4fb553b408dea13806000abd",
  "access_token": "sk_live_eW9H1urXYFp0h0v3IIkzdmM8",
  "livemode": true,
  "refresh_token": "rt_6zOpI02TsLDl0kYa7oogZbKFjflCBjg84C2HwusA6nclFB3b",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ylcpsyMWEi8lfbDbxR9n6ONH",
  "stripe_user_id": "acct_16lPttHyVJG0JaOe",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "560dae09f02cbe34e300bcce",
  "active": true,
  "purchases": 5,
  "total_revenue_cents": 18600,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53e53e6af02cbed474000131",
  "user_id": "53ea8b0af02cbe6eb9002b3f",
  "access_token": "sk_live_Cek9IUgdMALdDItu1lgtfOey",
  "livemode": true,
  "refresh_token": "rt_75Q6kWEP9uwCaKDYOFcqpR4zW8J3ozFRAUD60yNDpkOT87hD",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ZDeLUnwpXTq878Z2sym5YVgk",
  "stripe_user_id": "acct_16rF4VI82JIBVgkT",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5612ed24f02cbe0019002ac1",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5612e677f02cbed3e1002bcf",
  "user_id": "5612e5c7f02cbe734d002bc4",
  "access_token": "sk_live_vVyOEi4wZKsX9lkzZZqWDgi2",
  "livemode": true,
  "refresh_token": "rt_76uX3RqrsVz0r6ChvqjYbnYhuw2Dc5b2JtXSzwky1FuXwGlO",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Qa1JfLIT6DQcSuonCpp3H5Uc",
  "stripe_user_id": "acct_16sgT3KaqPHmOVux",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "561561dbf02cbed5e90024ba",
  "active": true,
  "purchases": 9,
  "total_revenue_cents": 18300,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "55c0dffff02cbec9980048b6",
  "user_id": "55c0dffff02cbec9980048b3",
  "access_token": "sk_live_188w2OwrTqniB8SyPT7vrjXZ",
  "livemode": true,
  "refresh_token": "rt_77bnLdvj6Id618B9TXuAtYSdxRDg5FjoFCC3cKPkGl3GMKwO",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_54tO54Dli6pab6hwDaZ2amtx",
  "stripe_user_id": "acct_16tMWIKkiAsZZ51N",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56168086f02cbe3b32002dfa",
  "active": true,
  "purchases": 17,
  "total_revenue_cents": 49034,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "560077ccf02cbe2d3c0066a7",
  "user_id": "560c1cb9f02cbeb93800ae3c",
  "access_token": "sk_live_zmOp8pZJGtWUxMjcz0mYOWOU",
  "livemode": true,
  "refresh_token": "rt_77vWq1v1IOynkD4zRTny1wXhRTk7mn78YWHSmtBV7vjwVC6T",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_M31evfDivcwDsxQu5mwcdiVs",
  "stripe_user_id": "acct_16tfUkHd3NDm2pDW",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5627f282f02cbe2a3a002174",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56141846f02cbe4b23001bbc",
  "user_id": "5627f042f02cbe046500214c",
  "access_token": "sk_live_sZoM4dy1FU9D80Nz5JTW0KHq",
  "livemode": true,
  "refresh_token": "rt_7CsrDFO3ymkxg5zbD7tw1nv1sMDobBuF9i7GCufIPlXJkZeG",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_fuLuFli8b3zhBCwIs7vri4cC",
  "stripe_user_id": "acct_16yT1jK6K452XT7c",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "562fd052f02cbeb7920004e6",
  "active": true,
  "purchases": 7,
  "total_revenue_cents": 26498,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "53a471a1f02cbeda2a002b1c",
  "user_id": "53a471a1f02cbeda2a002b1d",
  "access_token": "sk_live_lBgJnJzoiUhQ5fbDZ6VlR7u8",
  "livemode": true,
  "refresh_token": "rt_7F7SvU3T4DYKrhXe2DoGOVUkZcHc6SYTZn46OtwzxMyxD8Ig",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_BjHWfU0lJKPQ6OZUMEDszhw0",
  "stripe_user_id": "acct_170d2gGJGQFXmN0k",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "563143aaf02cbea47c000d14",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "562eb44ff02cbe9cc2000288",
  "user_id": "560dd5c1f02cbefc5300bec9",
  "access_token": "sk_live_Vpgkdhrh6acdQt4g4y8yz12P",
  "livemode": true,
  "refresh_token": "rt_7FX0dVbhJyIYJP9V8DuvxHYpjevt56s1strPAP5FlVijNdyg",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ealuPModI5wZLq8QF1iLzMOo",
  "stripe_user_id": "acct_170JGBJ58fRJT6j8",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56314f1bf02cbe0e54000e41",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "562ea24bf02cbef43e00020c",
  "user_id": "560dd5c1f02cbefc5300bec9",
  "access_token": "sk_live_VRU4gUtQGnVR2t3wzh2L7skY",
  "livemode": true,
  "refresh_token": "rt_7FXnoVJN0WYRLunObSQVGCNLRkSpzOtqkIS6tnL4GdYNoRJ9",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_8LyWsVhGc0NiKYB7Mt7ZhIs5",
  "stripe_user_id": "acct_1712T5CeNLwjTdZA",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56675f19f02cbe3ce9000871",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56674631f02cbe7a8e0007c7",
  "user_id": "564cf543f02cbee2a4002ab0",
  "access_token": "sk_live_kMokFI3UgKh8Ppza6jySKWMD",
  "livemode": true,
  "refresh_token": "rt_7Uu8PBfDBsyn4tzc6hCOjkFrzMFTfhGLgOl2JejJ8KNBlDoK",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_HSDJ1UDcrPKgIkl9nplAUysY",
  "stripe_user_id": "acct_17Fu4hECeXePzehy",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5670b75df02cbe2a12004495",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "566b6033f02cbe8002001d7f",
  "user_id": "56718eabf02cbe949100514b",
  "access_token": "sk_live_QTCipya3y7bzz7GCrfFr2NJG",
  "livemode": true,
  "refresh_token": "rt_7XYl6h8NGyzZOvSCkK9KuUpp2ALauLSYWjWgL6ZRXuldNQvN",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ahWmGorD1zOTsGscle0qgk0z",
  "stripe_user_id": "acct_17GW03EN6DCuyngV",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5682d5aaf02cbe578f003748",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56442c6df02cbebdb30009e9",
  "user_id": "56442c6df02cbebdb30009e6",
  "access_token": "sk_live_MnYUHT3hamIokl3brLhBska8",
  "livemode": true,
  "refresh_token": "rt_7chx8GgQUxojXvXBeCb2GQO2BoZV8PzIKDe3iyVtuRx2XsRK",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_XY5a6n4jltqYY8bzzuTqJkSd",
  "stripe_user_id": "acct_17NSNqIf2IYsNW2p",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568aa35394cacf79df008d46",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568582d294cacf15990023e4",
  "user_id": "5689750a94cacfe7540079c3",
  "access_token": "sk_live_30vuhsJVpl8RXhDaKLDQmO7O",
  "livemode": true,
  "refresh_token": "rt_7evQZOIYMxpc17i6ivr0qPOPfhThM0UXS3n4vz4HbnW7NDoS",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_J4VGtS8Hb4yDZQnNcZ8S7DdT",
  "stripe_user_id": "acct_17PbVsF7FnqAqeq5",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568af29194cacf3863009b18",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 2000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568af05c94cacff84e009aa7",
  "user_id": "568af05c94cacff84e009aa4",
  "access_token": "sk_live_x4dJynOGWvitS1V722MuybKD",
  "livemode": true,
  "refresh_token": "rt_7f0sYMkeZfOH879VpX7YyPY0gwHGortSB7RBuSoLiRuPeTla",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_XgBleupdN7KCXC2q37W0Flom",
  "stripe_user_id": "acct_17PgijKebSetOEQX",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568b4f6b0ce944d372000639",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 5000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568adc1694cacfff800096a8",
  "user_id": "568ae7ba94cacf10970098eb",
  "access_token": "sk_live_zZKv9w6yJhIIiuThpTdt8naR",
  "livemode": true,
  "refresh_token": "rt_7f7GQmvTjhpK1pTrcPA21SbOvvj4CSeIHhlo2IdL3aGFw5Hc",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_bfpge0v01WELRYff9L9dSbk1",
  "stripe_user_id": "acct_17PmwkCMPkRnFuch",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568c04c50ce94458b900146c",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 8000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5685696694cacf1cf900226f",
  "user_id": "568b45570ce94475dd000582",
  "access_token": "sk_live_1PDsxdtTUaW3jQKcktlNQn9a",
  "livemode": true,
  "refresh_token": "rt_7fJk2umIQXheNnvtixlZDSLpNzXgVDDicRchvUzCNBaRd5nS",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_k7f8aQYDnwYDdtlIA29dk6dy",
  "stripe_user_id": "acct_17Pz0aKYnbgNTpyE",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568c27770ce9446243001998",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568acee794cacfde9d009407",
  "user_id": "568c22e00ce944ca2b0018d6",
  "access_token": "sk_live_GqMQntWJLDWlb12KDBa5E16s",
  "livemode": true,
  "refresh_token": "rt_7fM8xNSBEp20HDS9YfrIRy6zZzuiABDrmZDtYrojVsLYWZEm",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_IgqMGO9ADFggoLuKIEb28YHn",
  "stripe_user_id": "acct_17Q1AdHDxLnyc4E9",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568d83120ce94410d8003a48",
  "active": true,
  "purchases": 6,
  "total_revenue_cents": 6000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568acc9294cacf81e7009396",
  "user_id": "568d64ea0ce9441590003609",
  "access_token": "sk_live_F083fhDqElb7RZS1v2euWQOv",
  "livemode": true,
  "refresh_token": "rt_7fk3IhbzhKp2CnZ3fYG1F0YFMPEx86Zj11T7FUOTE2dTxrZM",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_INrPudo47eOyCRohew7XDaCX",
  "stripe_user_id": "acct_17QNuWJg4rfeIpNF",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568d89ae0ce944bf79003abf",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568b020494cacffa2a00a05d",
  "user_id": "568d81c40ce944d7320039f6",
  "access_token": "sk_live_XZEZtItvUxRaKEuXUE9TXpp2",
  "livemode": true,
  "refresh_token": "rt_7fkVqHRbmovN1G4gDrvRTasYRfKWdBp2mGA24NZIE9hPXXk8",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_o2AMMCpmAuqHCOBLPSTQWrbZ",
  "stripe_user_id": "acct_17QOaOBh0xUlf1vt",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568d954a0ce9449d9e003c31",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568d92730ce94431fb003be9",
  "user_id": "568d92730ce94431fb003be6",
  "access_token": "sk_live_OcwVHBIJuvkjpD6NSxAbg2r6",
  "livemode": true,
  "refresh_token": "rt_7flJZQREIXCD6YKV2Qxt4LIDloGaoeT8iWpstaYq6PqKdenj",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_TvEZnYItpjUeCOmBf6n9C13I",
  "stripe_user_id": "acct_17QPiWGCYpeYj579",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568dc08d0ce9445a5a003fe3",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56845c7994cacf755a000f04",
  "user_id": "568dbbfc0ce944a99b003f41",
  "access_token": "sk_live_6agFKAkvNMaSLxrxG5a4m6vX",
  "livemode": true,
  "refresh_token": "rt_7foHRB0tSbXWsOCaKOSvoyew411xLAnrtnax586X4OVYGhGe",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_KPLhlJPkxPMpnNamD1NHwRGw",
  "stripe_user_id": "acct_17QSPXGqJ8TOL1qt",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "568eac7f0ce9443c070054c5",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568af16b94cacf6b8a009ad1",
  "user_id": "568ea9f70ce9441805005448",
  "access_token": "sk_live_xEO4bNQyrAwY4GX6mWn3FBmC",
  "livemode": true,
  "refresh_token": "rt_7g4WB0WfRO99efTGRthZyRUXZtEnWvADAgTZwQZccdeFzfce",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_VHnobMakFN2b7ORvQLKsp70X",
  "stripe_user_id": "acct_17QiEpIJbKQHUCTQ",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "569405e3845f094cb700365d",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 1200,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568461c994cacf7bf8000fa9",
  "user_id": "568ed0de0ce9447d34005e00",
  "access_token": "sk_live_jaUFp5iE2xjVuVzDDPO5I2Qc",
  "livemode": true,
  "refresh_token": "rt_7halGn65ZLzPd8Yjdi2Sk27VdfxjgHq0HTltJB9uIgmMbXci",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_c66avcD5eBKUvnjUfFoBrRk8",
  "stripe_user_id": "acct_17SAjZKKYGyCtRGx",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56985753845f09c68200f653",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 7000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56969aa7845f09fce100bc98",
  "user_id": "5696ffa8845f09a25800cf02",
  "access_token": "sk_live_k7iPcpsR1UCJHpO4vnhy6WTY",
  "livemode": true,
  "refresh_token": "rt_7iopq4JolyqoTiQPcWpuWA7jWffNarnTzbtERyzDYNgq3Lxu",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_8XPFRsacsNeEy91y8RYS5ChR",
  "stripe_user_id": "acct_17TMVkC07YLOi8ks",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56a11aa347ffb0434100b1cc",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "55c14d2af02cbeecaf000f7b",
  "user_id": "55c14d2af02cbeecaf000f78",
  "access_token": "sk_live_6t8q1WhzGL4ITyhxS2SIVwmL",
  "livemode": true,
  "refresh_token": "rt_7lJDUTyUKlZehYN7OUezCjKHSkQnjSBfMuQSUKZxcvBhqeSi",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_GlyO5U6zw1ljrNRor7CpGlbM",
  "stripe_user_id": "acct_17VmSpGauHW3yy6o",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56a1287d47ffb0bbdc00b66c",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568d59ab0ce94441b3003501",
  "user_id": "569d22d247ffb0a9cd001ec8",
  "access_token": "sk_live_8OtAu09ve5JNEvSAUUAedQ2T",
  "livemode": true,
  "refresh_token": "rt_7lKAXOwG2YeSPenjedqrlP2dZo90eAyc2q77GyL9E49DjQJQ",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_X8Bd9GAkw12weunF8oVKU8lv",
  "stripe_user_id": "acct_17VnIaBDEMNM1OOU",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56a12d4c47ffb0507e00b76b",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568aecb194cacf4e3e009a0f",
  "user_id": "56956ded845f097d3000990b",
  "access_token": "sk_live_xtrJx2rbypcGa9T1iSBQgVtg",
  "livemode": true,
  "refresh_token": "rt_7lKUF6jPR82BPMHQ3frUwoLPx4cz7QemF8gwGuc98o3ZHnmh",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_zuLpNPiAjZ4nU60URlhDJt9F",
  "stripe_user_id": "acct_17VnhxHjzqLmfE9x",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56b1338f47ffb02e7102f19b",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56b11e1747ffb0e0d002efa2",
  "user_id": "56b12ce447ffb020f802f0c5",
  "access_token": "sk_live_UtetaEhqciahRJHi3Y9esTpg",
  "livemode": true,
  "refresh_token": "rt_7psnBcFrXsVLjLqVkURPR6pZLgovK4fX7R6xWAtz1BKJUyB5",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_sYAa8v6Hn4X267j8mREAHXzB",
  "stripe_user_id": "acct_17aCxfAB6wEljTE2",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56b238a247ffb0aa0e0315a7",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56b1588f47ffb0474b02f50c",
  "user_id": "56b1588f47ffb0474b02f509",
  "access_token": "sk_live_TSJBzEoVc4zKIu4Nxq2Ds65y",
  "livemode": true,
  "refresh_token": "rt_7qAl8ZCKVNVLlpB2yBQTJKn5LPNbJYc7a59nMsqmV1j5ZsAK",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_gzW4t6O40h15rymQHXg0HJyN",
  "stripe_user_id": "acct_17aFWXJJUErFj4JC",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56ba5465802e881e1000afcb",
  "active": true,
  "purchases": 2,
  "total_revenue_cents": 1800,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5487976df02cbe84660098ef",
  "user_id": "5487976df02cbe84660098ec",
  "access_token": "sk_live_rtRIHwpN8iMk0KAruRcO9xkH",
  "livemode": true,
  "refresh_token": "rt_7sTcnVsaGkxmqBXDnbMalLqgqiZBX7Q2zxU3Lo1sy5w8mWIN",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_13PKv7TiJ2ftoq087aQi7IoV",
  "stripe_user_id": "acct_17ciSiJqOEoezGHB",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56bceab3a10a1ad73c00177b",
  "active": true,
  "purchases": 4,
  "total_revenue_cents": 199499,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56bbbb12802e887f3d00dd8e",
  "user_id": "561e9b30f02cbe7a1300656c",
  "access_token": "sk_live_8hPNJ72S9LhUKU2lEiYTvpW5",
  "livemode": true,
  "refresh_token": "rt_7tDBAMH086Y3ajLz0mRLKgTouKypo2OqrQnXvFGf9ogPBvyj",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_IKPfLFEPgMFzY19HbZI1K822",
  "stripe_user_id": "acct_17dQeNE90RgwfOUh",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56c77dd6bd40b1c87700ae47",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 2900,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5570d8fcf02cbe462c00e318",
  "user_id": "5570d8fcf02cbe462c00e315",
  "access_token": "sk_live_XKXKW4BEWs3U1r613T8eB3n6",
  "livemode": true,
  "refresh_token": "rt_7wDUG36SWt3ozjjJtmIRtfiVjXDj3DeOUrbdpvo1ItfdzX9M",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_OVTszr7Dqil1H7oUVx8tiARH",
  "stripe_user_id": "acct_17gHr0BArTvZvdxT",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56c7a5e1bd40b19d1500bdaa",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56b3cd21802e881648002210",
  "user_id": "56c7a17dbd40b151c900b7b0",
  "access_token": "sk_live_tSxT9dQKQbOwQWMCpxLelKj6",
  "livemode": true,
  "refresh_token": "rt_7wGFM9x0XXFnKTZK8QXdYyeUkBAulOauK2XSs4kJkvw4jTrG",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_hTHRwuxvYA3QPLbbKPREasT6",
  "stripe_user_id": "acct_17gNcxLG6wrZ1O1V",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56d327a1a5a0159f240038a0",
  "active": true,
  "purchases": 4,
  "total_revenue_cents": 8600,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56c4990cbd40b1a8a30071c9",
  "user_id": "56c73f07bd40b1ad7200a8a1",
  "access_token": "sk_live_jSJ4UgKWiHJtzRe0OS2YiOSv",
  "livemode": true,
  "refresh_token": "rt_7zWxb8bdDTD1ZqemRumMu0cCLWDc6ZN1MBAXrq5K139NV4An",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_8u7XKfqrfKqotxd4uLhNasKr",
  "stripe_user_id": "acct_17ipLWLv49a4mzSC",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56d5f294a5a0158003007ee1",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56d5f18ba5a015753e007e9f",
  "user_id": "56d5eff4a5a015afda007c99",
  "access_token": "sk_live_w8Wkovo0rRizZ8XHK3DfQ0BH",
  "livemode": true,
  "refresh_token": "rt_80KAQQXPDwAvprCtpV3WxWAryef4JqbGLiVr6ar8lEcUIb4A",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_83cb3lcTdqX8Lr5UhVrXEqmc",
  "stripe_user_id": "acct_17kJS7AWjk62u4Jd",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56e8c8321aa7203c32001bbf",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56d8be50a5a015ed7f00bdf2",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "access_token": "sk_live_MdwAeg2mtOYxWo8OIfoqNatH",
  "livemode": true,
  "refresh_token": "rt_85fyBnM2ci0R2xKWCFoMK4uTGDOr5Tl0HwAVtXS15FEFST71",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_tGLZmYIyjtwX7AuYpF8SJeB5",
  "stripe_user_id": "acct_103X9v2JBAo3lODP",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56e8c86a1aa720c120001bc6",
  "active": false,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56d8c053a5a015873b00be29",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "access_token": "sk_live_MdwAeg2mtOYxWo8OIfoqNatH",
  "livemode": true,
  "refresh_token": "rt_85fyBnM2ci0R2xKWCFoMK4uTGDOr5Tl0HwAVtXS15FEFST71",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_tGLZmYIyjtwX7AuYpF8SJeB5",
  "stripe_user_id": "acct_103X9v2JBAo3lODP",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56e8c8fa1aa720ab51001bce",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56d8c053a5a015873b00be29",
  "user_id": "56d8be50a5a015ed7f00bdef",
  "access_token": "sk_live_MdwAeg2mtOYxWo8OIfoqNatH",
  "livemode": true,
  "refresh_token": "rt_85fyBnM2ci0R2xKWCFoMK4uTGDOr5Tl0HwAVtXS15FEFST71",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_tGLZmYIyjtwX7AuYpF8SJeB5",
  "stripe_user_id": "acct_103X9v2JBAo3lODP",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56f369cae9330c8fc90000b0",
  "active": true,
  "purchases": 5,
  "total_revenue_cents": 7500,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56ce28aebd40b1e322019b94",
  "user_id": "56e052861aa72021dd00d579",
  "access_token": "sk_live_dIa2sWOtbQRHe7TP9lL4Wy3W",
  "livemode": true,
  "refresh_token": "rt_88hGpxewW8fU4T7wQ0Kqm4zskhuAga9nAf5FMWOwPj4lUabp",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_P3pXCxaTpisNIAqVNfEKdCmy",
  "stripe_user_id": "acct_17sPUeAqqC0PD3e5",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56fb04811aa7207856006709",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56fafebae9330cdbd80031cc",
  "user_id": "56fb00af1aa72051120066be",
  "access_token": "sk_live_vuZRRgnEPaApLMWORtORUzlB",
  "livemode": true,
  "refresh_token": "rt_8ArEDJvD7EsiPTFp4izlO5gbmUYH6X637K801N7kEt9HmQo2",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_BdyQLnamdbXAsEz5VDYZiRu9",
  "stripe_user_id": "acct_17uVOsEiPTYwM1TY",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56fd60d8e9330c01650047b6",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56facb031aa720e17a0063a1",
  "user_id": "56fae796e9330ca038003027",
  "access_token": "sk_live_Vqs1Ue3JkDmcpTUBaML1lIMv",
  "livemode": true,
  "refresh_token": "rt_8BWpQSVar5KguaYTNoHSjSDsNsmQw87bv4JMRQ0qB9AFka8F",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_8bJ3lzuxzOkj1VA99Qq3NTDz",
  "stripe_user_id": "acct_17v9OSHewYWjvjtI",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56fd62bae9330cdf1a0047c5",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56faf140e9330cce4400310b",
  "user_id": "56fae796e9330ca038003027",
  "access_token": "sk_live_q9a0x7BcsYB7bjFt72FJr0kE",
  "livemode": true,
  "refresh_token": "rt_8BWxIkmmeUh9Q0pSNUYoxl1hsp7rynJl2R1wqjfX5EwrFyGq",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_AHMGKFhD0UGmS6Y21sHhtaHy",
  "stripe_user_id": "acct_17v9nvEN5L5bYBB6",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "56fd7bbd1aa7201cc1008064",
  "active": true,
  "purchases": 12,
  "total_revenue_cents": 17600,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "568af45294cacf5fb6009ba8",
  "user_id": "56fd730ee9330c677c0047fd",
  "access_token": "sk_live_oY60mXZbI0GfJW3GW3tuyrla",
  "livemode": true,
  "refresh_token": "rt_8BYgr58NO8pKIChjdw06zn3qimvvDHfrytt0tYG1tB6w9gyF",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_joAkza9IivGpozt7qPmZh0cR",
  "stripe_user_id": "acct_17vBAiDK6lTYhgJO",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5702bf791aa720a1f200bc69",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5702b6e6e9330cc9c4007acb",
  "user_id": "561e9b30f02cbe7a1300656c",
  "access_token": "sk_live_CVrNf4y5uEhI0IxBgoqfHw3o",
  "livemode": true,
  "refresh_token": "rt_8D3QlyHuwJTMhP4bcwMDO3qsMHEPasW3yCX0LlhlrLoTpPk9",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_yLERk78Ky35Sj51uebTiDeqt",
  "stripe_user_id": "acct_17wd6fDOYs42VsQk",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "570301741aa7201df100c280",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 8900,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "56f1ac441aa720e8320152ee",
  "user_id": "5702f6451aa720287e00c182",
  "access_token": "sk_live_nrKRsBH5AkZiAd9YKgvvv81w",
  "livemode": true,
  "refresh_token": "rt_8D7xOAdAH1DVtYcmWukZKRy1GxdKjN7iyQtwY4DZ7efv9CMv",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_uRrZRD3AQPWNyuyUFFvz8wJT",
  "stripe_user_id": "acct_17wgzvHVI4G8JIox",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "57044ac11aa7207d1b00d9b3",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 12900,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5703eef4e9330c46b2008fad",
  "user_id": "5703eebde9330c4d6a008f9f",
  "access_token": "sk_live_DcRJvI1oMdhK9oakjKQfd4N8",
  "livemode": true,
  "refresh_token": "rt_8DUcPndr3kJwjZRt6XNj3cGu5U5BQvAT7MjgJ4isCTDtI2dv",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_z6WWVGqDyrOFx4p9ycBoYFUw",
  "stripe_user_id": "acct_17wxYgBEHlqJfB6n",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "57112486e9330c91db004b75",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "571122661aa720eeaf005657",
  "user_id": "571122661aa720eeaf005654",
  "access_token": "sk_live_8fyQ2ZcfsKeix1XKrllh8tnR",
  "livemode": true,
  "refresh_token": "rt_8H911lZW8B9032YufREvNifg4JgGsGYdgIId3i6sklfsgTVA",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_YEn819LtD40IHWne7mEoggrC",
  "stripe_user_id": "acct_180adkBwFBkHVLmM",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5736159d1aa720481a003125",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5735fcc01aa7205cc0003002",
  "user_id": "5735fcc01aa7205cc0002fff",
  "access_token": "sk_live_22D8naWncK0LGx8Cm2vZjCIX",
  "livemode": true,
  "refresh_token": "rt_8RdppFRm4pjrsDv5ql28N7k3I9apIX3rmMcxLIRhE5YNcUyb",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_APJbd3sMdzvu83Wjrjk3Ayod",
  "stripe_user_id": "acct_18Aj4GGZBKAH1POv",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5744d9d71aa7202e5e0042ff",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5683237ff02cbe7f16003908",
  "user_id": "56ac03b647ffb0099b0273c0",
  "access_token": "sk_live_igoWYqaoSpTg3hpimfz7iTzn",
  "livemode": true,
  "refresh_token": "rt_8VpyjlML4CyLP3e8I3INIH1YW7mJGWR2PQ2RsdSqRiDW7YsK",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_ksWGKMvjOfBpLb1CmoeObWmT",
  "stripe_user_id": "acct_18ElxTEwTPTvW6it",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "575095bc1aa7207b01010b82",
  "active": true,
  "purchases": 3,
  "total_revenue_cents": 3200,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "574f0edae9330cb85200bb07",
  "user_id": "574f0edae9330cb85200bb04",
  "access_token": "sk_live_EJR6b1ZSpDZ3e8QZQu6DaTNm",
  "livemode": true,
  "refresh_token": "rt_8ZAhEt9dcx16pB63EXrLBJvAx9gkciPgtiCSM4C3KyzH4L20",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_lY00PwqVODvYteaBmwxl7Q9b",
  "stripe_user_id": "acct_18I2IqKOXKb8Hw8v",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5755fab31aa7207a29014813",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 30000,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5744a3e2e9330c3ef0003a3a",
  "user_id": "5750c6e6e9330ce7aa00cf0f",
  "access_token": "sk_live_yMs5IeBv5fXdVPGHSmogx9xL",
  "livemode": true,
  "refresh_token": "rt_8ahjiXNzYlNmifSuuZHEHyEUNKgsXyPCUStv3PZTbG52ZMdf",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_9mRGUgqLXQLYvbTLYKI8lYpp",
  "stripe_user_id": "acct_18JW75JYUajwH8Xj",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "576d843bce45605baa002a32",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "576d8197a4152f878e002a5c",
  "user_id": "576d8197a4152f878e002a59",
  "access_token": "sk_live_nVORRWsmIX7sSHSspw4PJRs5",
  "livemode": true,
  "refresh_token": "rt_8hOORuACjfBLgaFosY6PqKrJovKVQ0b6CZMJPwLWtF7WnbA9",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_uoeXKsQg6ntaYJuoDoElOnEi",
  "stripe_user_id": "acct_18PzU7H26L2x9Y9a",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "57756d7ba4152f4fe100458e",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5762e1fe59ffa8d88f000579",
  "user_id": "5762e1fe59ffa8d88f000576",
  "access_token": "sk_live_WP18xdnpYfB46dWDIcAQdpSI",
  "livemode": true,
  "refresh_token": "rt_8jdlIc9eTDC57v7OPwasOV0i9KuGF5PGZ74qaGTII9OWGNBU",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_GbKNIhAiUTer35z0sHGQyWI3",
  "stripe_user_id": "acct_18SA7tKIPcW0wChI",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5775a1c6a4152faecf00462a",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5768882ba4152f5e02001438",
  "user_id": "5761d2601aa720fe0e00008d",
  "access_token": "sk_live_0S4h0WoSkBDxCq8jopiz9g1h",
  "livemode": true,
  "refresh_token": "rt_8fxqzzkCC3PcDAD4zVCl3NtCA2bUKBejRvtBFk3n2f8NAycA",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_KoWybeBcEgwMW8ax6G9yHIXs",
  "stripe_user_id": "acct_18ObiLCRO7pHwz0F",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "57857dcca4152fb0d3008fbf",
  "active": true,
  "purchases": 1,
  "total_revenue_cents": 700,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "54ed003cf02cbee3b500309d",
  "user_id": "5785181ca4152f9042008de6",
  "access_token": "sk_live_DMO0I9fu7uaHmOHgmZbNJT7B",
  "livemode": true,
  "refresh_token": "rt_8oClXyYZONWbr5xMtIinSaSnpomwXwCU2c0uXKHdVUByk8mY",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_oKFRNswLBxfxiIcia92WUpIb",
  "stripe_user_id": "acct_18WaG4AdkH23Eoky",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "579266f059a0df5297000e12",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "57897cdf4811f5ac29009e76",
  "user_id": "578a68721aa720525a00db88",
  "access_token": "sk_live_AqEBKLyvyOk37pz9zqTrJ0PP",
  "livemode": true,
  "refresh_token": "rt_8rsDHOeXHsIEZAymMKwYFx7LwOGMRuwfd2cQbg24529ljNjx",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_r4m4Tx7nsqUzZ2cCkOU5zXYO",
  "stripe_user_id": "acct_160CWMDzKQme0nOo",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5797d47d59ffa880d3002479",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5787f1bc59ffa893fb00cf0e",
  "user_id": "578ed12a59ffa8af060004d7",
  "access_token": "sk_live_pwvkhna9l3n9pPhWqCMXdmnL",
  "livemode": true,
  "refresh_token": "rt_8tPpqtOfoMBHMl5NeRbxOBkbrsZr5FVBkOlqYyQOCcAHBI7f",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_HqMh7DmugFYTYfM9HYVq0Ng7",
  "stripe_user_id": "acct_18bcksLWG2JvX7Os",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "57f4145be262db344401de6d",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "578443921aa7209c6700bdbc",
  "user_id": "579000024811f553df0007e3",
  "access_token": "sk_live_YCd0O4gdFH4iICddgg5LgoOl",
  "livemode": true,
  "refresh_token": "rt_9Jd0OXKqMMOTK9LCaxPvIAx6DkaHuC0uika9v78aZSA72iOK",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_3binKqY7gVjvrTc0OlhpUFve",
  "stripe_user_id": "acct_190zLWCGGp8LItfg",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "57f44c74e262dbd69001e150",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "57f43623e262dbaf8a01e015",
  "user_id": "57f4355be262dbb52c01dff8",
  "access_token": "sk_live_qw71ceWVuAJExh3ovSFFai7v",
  "livemode": true,
  "refresh_token": "rt_9JgsepnfnKtgwpPxQsOvDlYfttYnW363eLBviF73VVde0Ho5",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_xVIGAEznuD0DCaqv1SS8Wll9",
  "stripe_user_id": "acct_1913LYCVOhfvsJ3U",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5807044588682ea78e010996",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5806bf9388682e9af60104e1",
  "user_id": "5806bd7e88682e8d780104c3",
  "access_token": "sk_live_8YTUr0GUcXExaZZMskbJsXzo",
  "livemode": true,
  "refresh_token": "rt_9P0dlSoozA85fRU1kIWmRiHwwpOvRCfQliMhZM3LoohCoYES",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_IIpPubwPqk2zLYsCtvGJvVAG",
  "stripe_user_id": "acct_196CQvDzGhxpMynp",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5810e59b88682e49ec022625",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "580fbebd88682ea9cd0200d9",
  "user_id": "580fbebd88682ea9cd0200d6",
  "access_token": "sk_live_GxidhhNprk6ep9EtcxGQONpT",
  "livemode": true,
  "refresh_token": "rt_9RohYsasaNDUxn4KKBb06x4q4lh7jJscUKsUcImDy6CSLlRn",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_k2fG9UJRI0ucZx2KRChFF8b6",
  "stripe_user_id": "acct_198uwhH38PbTZmHF",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "58138db788682e88e002b8bd",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5813888088682e5c8d02b834",
  "user_id": "5813855c88682ea4ba02b7da",
  "access_token": "sk_live_GxidhhNprk6ep9EtcxGQONpT",
  "livemode": true,
  "refresh_token": "rt_9RohYsasaNDUxn4KKBb06x4q4lh7jJscUKsUcImDy6CSLlRn",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_k2fG9UJRI0ucZx2KRChFF8b6",
  "stripe_user_id": "acct_198uwhH38PbTZmHF",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5818e33588682eafbf03452f",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "58095e8088682eae82014bbf",
  "user_id": "5809597788682e282a014b76",
  "access_token": "sk_live_hEfE7xp9TYIgAM5DZC33Mcnm",
  "livemode": true,
  "refresh_token": "rt_9U5SB11CO5rdVahLNUjKv31xpTaYNczLBaxWyO7konfjc7cG",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_gbK2nH5ae66tZUBQBvXBnkHp",
  "stripe_user_id": "acct_19B7D7DuWLfWgpWG",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "583e071d88682ec800077196",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "5772f040a4152f91c0003dc9",
  "user_id": "5772f040a4152f91c0003dc6",
  "access_token": "sk_live_j1h75ubVlHYJNbO9rR3eaWgk",
  "livemode": true,
  "refresh_token": "rt_9edmkEOtcB0iDW00nyYgidcEEjzZyXWta83RXdzxEC5QVyjB",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_Cv2kCtt0ikQZ6BfzmTqXqDlF",
  "stripe_user_id": "acct_19LKOuKLFDjiTLgo",
  "scope": "read_write"
}
merchant_accounts << {
  "_id": "5850541988682e50db092645",
  "active": true,
  "purchases": 0,
  "total_revenue_cents": 0,
  "_type": "MerchantAccounts::Stripe",
  "profile_id": "58503b6888682e236709245a",
  "user_id": "584736bf88682e9854085140",
  "access_token": "sk_live_ZaDinzRiumVfJVRe89Lbu7HH",
  "livemode": true,
  "refresh_token": "rt_9jqBFr2958DLC2Oje4wRfHhou5NeRTC6Gf8xyQvntFrBaBxE",
  "token_type": "bearer",
  "stripe_publishable_key": "pk_live_dZ9wDJWKFm7qwF9wdZLsFjOw",
  "stripe_user_id": "acct_19QM4pEqhLxnBMXF",
  "scope": "read_write"
}


merchant_accounts.each do |mc|
  if mc[:_type] == "MerchantAccounts::Stripe"
    user = User.find_by_old_mongo_id(mc[:user_id])
    profile = Profile.find_by_old_mongo_id(mc[:profile_id])
    profile = user.profile if user.present? && profile.blank?
    
    if user.present?
     
      if profile.blank?
        name = [user.first_name, user.last_name].join(" ")
        profile = Profile.new(user_id: user.id, name: name, profile_type: 'business')
        profile.save(validate: false)
        user.update_column(:user_type, 'business')
      end

      if profile.present?
        merchant_account = MerchantAccount.where(user_id: user.id, livemode: true).first
        merchant_account =  MerchantAccount.new if merchant_account.blank?
        attributes = {user_id: user.id, profile_id: profile.id,
          active: mc[:active], access_token: mc[:access_token], 
          livemode: mc[:livemode], refresh_token: mc[:refresh_token],
          token_type: mc[:token_type], stripe_publishable_key: mc[:stripe_publishable_key],
          stripe_user_id: mc[:stripe_user_id], scope: mc[:scope], account_type: 'Stripe'}
        merchant_account.attributes = attributes
        merchant_account.save
        puts "Saved MerchantAccount ID:: #{merchant_account.try(:id)}"
       

      end
    end
  end
end

end

end

