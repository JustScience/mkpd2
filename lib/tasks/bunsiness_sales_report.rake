namespace :business_sales_report do

	desc "Send weekly sales report to businesses"
  task :weekly => :environment do
  	start_date = (Date.today - 7.days).beginning_of_day
		end_date = Date.today.end_of_day
		##run report on every monday only
		if end_date.monday?
			Rails.logger.info "Sales report Run day:: #{end_date}"
			carts = Cart.where(paid: true, completed_at: start_date..end_date)
	    data_set = Hash.new { |hash, key| hash[key] = [] }
			if carts.present?
			  carts.each do |cart|
			  	cart.cart_items.includes(:discount, :business).each do |item|
			    	discount = item.discount
			    	business = item.business
			      customer = cart.customer
			    	data_set[business.user.email] << [cart.purchase_number, cart.completed_at, customer.email, business.name, discount.name, discount.discount_type, item.price, item.taxes, item.shipping, item.total_price]
			  	end

			  end

			  if data_set.present?
		  		data_set.each do |email, data|
		  			sub_total = 0.0
		  			taxes = 0.0
		  			shipping = 0.0
		  			total_sale = 0.0
		  			data.each do |d|
		  				sub_total += d[6]
		  				taxes += d[7]
		  				shipping += d[8]
		  				total_sale += d[9]
		  			end

			  		NotificationMailer.merchant_weekly_sales_report(email, data, sub_total, taxes, shipping, total_sale).deliver_now
			  		Rails.logger.info "Sending email to #{email}"
			  		puts "Sending email to #{email}"
			  	end
			  end
			
	    else
	    	puts "No Weekly purchase"
	    	Rails.logger.info "No Weekly purchase"
			end
		end

  end

end