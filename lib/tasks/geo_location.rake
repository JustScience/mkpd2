namespace :geo_location do

  desc "Update Geo location for profiles"
  task :update => :environment do

geo_locations = []
geo_locations <<
{
  "_id"=>"56719ee1f02cbe21660054b1",
  "email"=>"kgsaidahosoftball@gmail.com",
  "location"=>{
    "_id"=>"56719ee1f02cbe21660054b2",
    "address"=>{
      "_id"=>"56719ee1f02cbe21660054b3",
      "country"=>"US",
      "city"=>"Hayden",
      "region"=>"ID",
      "postal_code"=>"83835",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<
{
  "_id"=>"5282ba8df02cbed3de004efc",
  "location"=>{
    "_id"=>"5282ba8df02cbed3de004efd",
    "address"=>{
      "_id"=>"5282ba8df02cbed3de004efe",
      "city"=>"Rathdrum",
      "country"=>"US",
      "postal_code"=>"83858",
      "region"=>"ID",
      "street"=>"16114 N. Meyer Road,",
      "suite"=>""
    }
  }
}

geo_locations <<
{
  "_id"=>"56684ef5f02cbe9fc1000b8f",
  "email"=>"rblank@cdaschools.org",
  "location"=>{
    "_id"=>"56684ef5f02cbe9fc1000b90",
    "address"=>{
      "_id"=>"56684ef5f02cbe9fc1000b91",
      "country"=>"US",
      "city"=>"Coeur d Alene",
      "region"=>"ID",
      "postal_code"=>"83815",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<
{
  "_id"=>"56718eabf02cbe949100514b",
  "email"=>"nwwf12515@gmail.com",
  "location"=>{
    "_id"=>"56718eabf02cbe949100514c",
    "address"=>{
      "_id"=>"56718eabf02cbe949100514d",
      "country"=>"US",
      "city"=>"Hayden",
      "region"=>"ID",
      "postal_code"=>"83835",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<
{
  "_id"=>"56719ee1f02cbe21660054b1",
  "email"=>"kgsaidahosoftball@gmail.com",
  "location"=>{
    "_id"=>"56719ee1f02cbe21660054b2",
    "address"=>{
      "_id"=>"56719ee1f02cbe21660054b3",
      "country"=>"US",
      "city"=>"Hayden",
      "region"=>"ID",
      "postal_code"=>"83835",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"56c6a52cbd40b1510d009d44",
  "email"=>"comparepolicyseo@gmail.com",
  "location"=>{
    "_id"=>"56c6a52cbd40b1510d009d45",
    "address"=>{
      "_id"=>"56c6a52cbd40b1510d009d46",
      "country"=>"US",
      "city"=>"Noida",
      "region"=>"UP",
      "postal_code"=>""
    }
  }
}

geo_locations <<
{
  "_id"=>"56de107b1aa720f75400a4bc",
  "email"=>"acarrico@cdaschools.org",
  "location"=>{
    "_id"=>"56de107b1aa720f75400a4bd",
    "address"=>{
      "_id"=>"56de107b1aa720f75400a4be",
      "country"=>"US",
      "city"=>"Coeur d'Alene",
      "region"=>"ID",
      "postal_code"=>"83814",
      "street"=>"",
      "suite"=>""
    }
  }
}


geo_locations <<

{
  "_id"=>"56e98ce71aa7208b9f003464",
  "email"=>"secretary@altarcda.com",
  "location"=>{
    "_id"=>"56e98ce71aa7208b9f003465",
    "address"=>{
      "_id"=>"56e98ce71aa7208b9f003466",
      "country"=>"US",
      "city"=>"Coeur D Alene",
      "region"=>"ID",
      "postal_code"=>"83814",
      "street"=>"",
      "suite"=>""
    }
  }
}


geo_locations <<

{
  "_id"=>"56fd7857e9330cf9e900483f",
  "email"=>"baymontcda@yahoo.com",
  "location"=>{
    "_id"=>"56fd7857e9330cf9e9004840",
    "address"=>{
      "_id"=>"56fd7857e9330cf9e9004841",
      "country"=>"US",
      "city"=>"Coeur d'Alene",
      "region"=>"ID",
      "postal_code"=>"83814"
    }
  }
}

geo_locations <<

{
  "_id"=>"5716b1d9e9330cb092006ba4",
  "email"=>"sdavis@qualfon.com",
 
  "location"=>{
    "_id"=>"5716b1d9e9330cb092006ba5",
    "address"=>{
      "_id"=>"5716b1d9e9330cb092006ba6",
      "country"=>"US",
      "city"=>"",
      "region"=>"",
      "postal_code"=>"83854",
      "street"=>"",
      "suite"=>""
    }
  }
}


geo_locations <<

{
  "_id"=>"5759202e1aa720f68f016995",
  "email"=>"dppalmercontractor@gmail.com",
  "location"=>{
    "_id"=>"5759202e1aa720f68f016996",
    "address"=>{
      "_id"=>"5759202e1aa720f68f016997",
      "country"=>"US",
      "city"=>"Seattle",
      "region"=>"WA",
      "postal_code"=>"98118"
    }
  }
}


geo_locations <<

{
  "_id"=>"5772d9ec59a0dfe13c003dca",
  "email"=>"abudhabidesertsafari@yandex.com",
  "location"=>{
    "_id"=>"5772d9ec59a0dfe13c003dcb",
    "address"=>{
      "_id"=>"5772d9ec59a0dfe13c003dcc",
      "country"=>"US",
      "city"=>"San Antonio",
      "region"=>"TX",
      "postal_code"=>"78232"
    }
  }
}

geo_locations <<

{
  "_id"=>"579a66874811f59373000277",
  "email"=>"seattleseniorliving@yahoo.com",
  "location"=>{
    "_id"=>"579a66874811f59373000278",
    "address"=>{
      "_id"=>"579a66874811f59373000279",
      "country"=>"US",
      "city"=>"Seattle",
      "region"=>"WA",
      "postal_code"=>"98115"
    }
  }
}


geo_locations <<

{
  "_id"=>"57e23682e262db3eeb0173b7",
  "email"=>"mikewilsonn1@gmail.com",
  "location"=>{
    "_id"=>"57e23682e262db3eeb0173b8",
    "address"=>{
      "_id"=>"57e23682e262db3eeb0173b9",
      "country"=>"US",
      "city"=>"Laguna Beach",
      "region"=>"CA",
      "postal_code"=>"92651"
    }
  }
}


geo_locations <<

{
  "_id"=>"583fba3188682e1f94079999",
  "email"=>"nomersbusinessservice@gmail.com",
  "location"=>{
    "_id"=>"583fba3188682e1f9407999a",
    "address"=>{
      "_id"=>"583fba3188682e1f9407999b",
      "country"=>"US",
      "city"=>"San Francisco",
      "region"=>"CA",
      "postal_code"=>"94105"
    }
  }
}



geo_locations <<
{
  "_id"=>"5728fa621aa720c208018d3b",
  "email"=>"colleen@syringafp.com",
  "location"=>{
    "_id"=>"5728fa621aa720c208018d3c",
    "address"=>{
      "_id"=>"5728fa621aa720c208018d3d",
      "country"=>"US",
      "city"=>"Hayden",
      "region"=>"ID ",
      "postal_code"=>"83835"
    }
  }
}


geo_locations <<

{
  "_id"=>"576155241aa720110e004271",
  "email"=>"bluegrafixdesignca@gmail.com",
  "location"=>{
    "_id"=>"576155241aa720110e004272",
    "address"=>{
      "_id"=>"576155241aa720110e004273",
      "country"=>"US",
      "city"=>"Long Beach",
      "region"=>"CA",
      "postal_code"=>"90814",
      "street"=>"2436 E 4th St #800",
      "suite"=>""
    }
  }
}


geo_locations <<

{
  "_id"=>"577a177dce4560b175005249",
  "email"=>"furnituregalore@yandex.com",
  "location"=>{
    "_id"=>"577a177dce4560b17500524a",
    "address"=>{
      "_id"=>"577a177dce4560b17500524b",
      "country"=>"US",
      "city"=>"Epping",
      "region"=>"VIC",
      "postal_code"=>""
    }
  }
}


geo_locations <<

{
  "_id"=>"57b5e2d8a4152f6e340025c2",
  "email"=>"development@kootenaihumanesociety.com",
  "location"=>{
    "_id"=>"57b5e2d8a4152f6e340025c3",
    "address"=>{
      "_id"=>"57b5e2d8a4152f6e340025c4",
      "country"=>"US",
      "city"=>"Hayden",
      "region"=>"ID",
      "postal_code"=>"83835"
    }
  }
}



geo_locations <<

{
  "_id"=>"57fa916588682e9540002f48",
  "email"=>"businesssumbission@yahoo.com",
  "location"=>{
    "_id"=>"57fa916588682e9540002f49",
    "address"=>{
      "_id"=>"57fa916588682e9540002f4a",
      "country"=>"US",
      "city"=>"San Antonio",
      "region"=>"TX",
      "postal_code"=>"78232"
    }
  }
}


geo_locations <<

{
  "_id"=>"58672e0d88682efd110078c6",
  "email"=>"illinoisdentalcareers0@gmail.com",
  "location"=>{
    "_id"=>"58672e0d88682efd110078c7",
    "address"=>{
      "_id"=>"58672e0d88682efd110078c8",
      "country"=>"US",
      "city"=>"Mount Prospect",
      "region"=>"IL",
      "postal_code"=>"60056"
    }
  }
}

geo_locations <<

{
  "_id"=>"57436c691aa7206d370005fa",
  "email"=>"strangers@crushsoftball.org",
  "location"=>{
    "_id"=>"57436c691aa7206d370005fb",
    "address"=>{
      "_id"=>"57436c691aa7206d370005fc",
      "country"=>"US",
      "city"=>"Coeur d' Alene",
      "region"=>"Id",
      "postal_code"=>"83815",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"5761854d1aa720eaab0046da",
  "email"=>"evek@knudtsen.com",
  "location"=>{
    "_id"=>"5761854d1aa720eaab0046db",
    "address"=>{
      "_id"=>"5761854d1aa720eaab0046dc",
      "country"=>"US",
      "city"=>"Post Falls",
      "region"=>"ID",
      "postal_code"=>"83854",
      "street"=>"1900 E Polston Ave",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"577c450c59a0df44d9005c43",
  "email"=>"kgalietti@cdacasino.com",
  "location"=>{
    "_id"=>"577c450c59a0df44d9005c44",
    "address"=>{
      "_id"=>"577c450c59a0df44d9005c45",
      "country"=>"US",
      "city"=>"Worley",
      "region"=>"ID",
      "postal_code"=>"83854"
    }
  }
}

geo_locations <<

{
  "_id"=>"57bd7cafa4152f5b62000125",
  "email"=>"slashdot-au@yandex.com",
  "location"=>{
    "_id"=>"57bd7cafa4152f5b62000126",
    "address"=>{
      "_id"=>"57bd7cafa4152f5b62000127",
      "country"=>"US",
      "city"=>"Malvern East",
      "region"=>"Vic",
      "postal_code"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"5805190f88682ec0a800d67e",
  "email"=>"colby@ktectraining.org",
  "location"=>{
    "_id"=>"5805190f88682ec0a800d67f",
    "address"=>{
      "_id"=>"5805190f88682ec0a800d680",
      "country"=>"US",
      "city"=>"Rathdrum",
      "region"=>"ID",
      "postal_code"=>"83858"
    }
  }
}

geo_locations <<

{
  "_id"=>"58a49b5088682e34cf06813a",
  "email"=>"orlipb@aol.com",
  "location"=>{
    "_id"=>"58a49b5088682e34cf06813b",
    "address"=>{
      "_id"=>"58a49b5088682e34cf06813c",
      "country"=>"US",
      "city"=>"Coeur d Alene",
      "region"=>"ID",
      "postal_code"=>"83814",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"5756650a1aa720377b014db7",
  "email"=>"valuebuildhomes1@gmail.com",
  "location"=>{
    "_id"=>"5756650a1aa720377b014db8",
    "address"=>{
      "_id"=>"5756650a1aa720377b014db9",
      "country"=>"US",
      "city"=>"Sanford",
      "region"=>"NC",
      "postal_code"=>"27332"
    }
  }
}


geo_locations <<

{
  "_id"=>"576c069c59ffa83f5c0027da",
  "email"=>"fishingcharter481@gmail.com",
  "location"=>{
    "_id"=>"576c069c59ffa83f5c0027db",
    "address"=>{
      "_id"=>"576c069c59ffa83f5c0027dc",
      "country"=>"US",
      "city"=>"Westport,",
      "region"=>"WA",
      "postal_code"=>"98595",
      "street"=>"2549 Westhaven Dr,",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"57965197ce45607718001ec6",
  "email"=>"movinggoodguys@gmail.com",
  "location"=>{
    "_id"=>"57965197ce45607718001ec7",
    "address"=>{
      "_id"=>"57965197ce45607718001ec8",
      "country"=>"US",
      "city"=>"Schertz",
      "region"=>"TX",
      "postal_code"=>"78154"
    }
  }
}


geo_locations <<

{
  "_id"=>"57bdd48e4811f5874500024c",
  "email"=>"carmenm@pmtsspokane.com",
  "location"=>{
    "_id"=>"57bdd48e4811f5874500024d",
    "address"=>{
      "_id"=>"57bdd48e4811f5874500024e",
      "country"=>"US",
      "city"=>"Spokane Valley",
      "region"=>"WA",
      "postal_code"=>"99037"
    }
  }
}

geo_locations <<

{
  "_id"=>"582ddde488682e076b05fe96",
  "email"=>"ytpak87@gmail.com",
  "location"=>{
    "_id"=>"582ddde488682e076b05fe97",
    "address"=>{
      "_id"=>"582ddde488682e076b05fe98",
      "country"=>"US",
      "city"=>"Lahore",
      "region"=>"PB",
      "postal_code"=>"54000"
    }
  }
}


geo_locations <<

{
  "_id"=>"5655f765f02cbe6c4d000033",
  "email"=>"mama_jlo@hotmail.com",
  "location"=>{
    "_id"=>"5655f765f02cbe6c4d000034",
    "address"=>{
      "_id"=>"5655f765f02cbe6c4d000035",
      "country"=>"US",
      "city"=>"CoeurdAlene",
      "region"=>"ID",
      "postal_code"=>"83815",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<


{
  "_id"=>"566f0aa3f02cbe6ae200369c",
  "email"=>"rachelreimche@yahoo.com",
  "location"=>{
    "_id"=>"566f0aa3f02cbe6ae200369d",
    "address"=>{
      "_id"=>"566f0aa3f02cbe6ae200369e",
      "country"=>"US",
      "city"=>"Post Falls",
      "region"=>"ID",
      "postal_code"=>"83854",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"56a676e247ffb0fef901f1a9",
  "email"=>"anprcglobal@gmail.com",
  "location"=>{
    "_id"=>"56a676e247ffb0fef901f1aa",
    "address"=>{
      "_id"=>"56a676e247ffb0fef901f1ab",
      "country"=>"US",
      "city"=>"Coeur d'Alene",
      "region"=>"ID",
      "postal_code"=>"83815",
      "street"=>"",
      "suite"=>""
    }
  }
}

geo_locations <<

{
  "_id"=>"56a7d75c47ffb071bd0213d5",
  "email"=>"happyedward777@gmail.com",
  "location"=>{
    "_id"=>"56a7d75c47ffb071bd0213d6",
    "address"=>{
      "_id"=>"56a7d75c47ffb071bd0213d7",
      "country"=>"US",
      "city"=>"Madurai",
      "region"=>"TN",
      "postal_code"=>"",
      "street"=>"",
      "suite"=>""
    }
  }
}


geo_locations <<

{
  "_id"=>"56c372febd40b1fa71005acf",
  "email"=>"jessicaanderson89@msn.com",
  "location"=>{
    "_id"=>"56c372febd40b1fa71005ad0",
    "address"=>{
      "_id"=>"56c372febd40b1fa71005ad1",
      "country"=>"US",
      "city"=>"Post Falls",
      "region"=>"ID",
      "postal_code"=>"83854",
      "street"=>"",
      "suite"=>""
    }
  }
}


geo_locations.each do |loc|
  user = User.find_by_old_mongo_id(loc["_id"])
  profile = user.profile
  if profile.present?
    address = loc["location"]["address"]
    region = address["region"].present? ? address["region"] : "ID"
    state = User.us_states(region)
    zip_code = address["postal_code"]
    city = address["city"]
    street = address["street"].present? ? address["street"] : city
    if address.present?
      profile.street = street
      profile.city = city
      profile.state = state
      profile.zipcode = zip_code
      profile.country = "USA"
      profile.save(validate: false)
      puts "Saved profile id #{profile.id}"
    end
  end
end



end
end