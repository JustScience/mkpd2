aff = []

aff <<
{
  "_id" => "564b8406f02cbe7e7d0020df",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Uber - Coeur d'Alene ",
  "alias" => "coeur-d-alene-uber",
  "email" => "",
  "phone" => "Use your free Uber App",
  "website" => "",
  "main" => true,
  "affiliate" => true,
  "user_id" => "5655f765f02cbe6c4d000033",
  "description" => "",
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!",
  "organization" => false,
  "mission_title" => " Uber",
  "mission_statement" => "Located in Coeur d'Alene ID\r\n\r\nNo more waiting for taxis.  Get a ride home today in a safe, clean, and insured vehicle.  Get $20 off of your first ride.\r\n\r\n1. Download the free Uber App from your App store\r\n2, Enter promo code ymt6vc25ue\r\n3. Follow the info and request your first ride....It's that easy\r\n\r\nIT'S UBER COOL"
}

aff <<

{
  "_id" => "5696bac6845f09e68b00c0d3",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Rachel Reimche",
  "alias" => "rachel-reimche",
  "email" => "",
  "phone" => "(208) 243-7181",
  "website" => "",
  "main" => false,
  "affiliate" => true,
  "user_id" => "566f0aa3f02cbe6ae200369c",
  "description" => ""
}

aff <<

{
  "_id" => "56a676e247ffb0fef901f1ac",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "affiliate" => true,
  "name" => "ANPRC",
  "phone" => "208.821.7020",
  "alias" => "anprc",
  "user_id" => "56a676e247ffb0fef901f1a9",
  "description" => "<p><span style=\"font-family =>arial,helvetica,sans-serif; font-size =>16px\">A New Perspective Restoration Corporation is an innovative company bringing diversified, environmentally responsible training and products to the Sustainable Living and Development Centers (SLDC) demonstrating the principles of Permaculture, Ecolonomics, Sustainable Agriculture, Alternative Energy production, and a Wellness Center.</span></p>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">A New Perspective Restoration Corporation (ANPRC) is dedicated to providing a variety of high-quality, low-cost products to sell to consumers who want healthful, affordable foods. ANPRC also provides training in the fields of hydroponics, aquaculture, and composting. ANPRC is committed to meeting the demand for healthy locally-grown agricultural products, which are grown in computer-controlled, environmentally-friendly greenhouses.</span></span></p>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">ANPRC has a vision for environmental, human, and economic health that motivates every aspect of this plan.</span></span></p>\n\n<p> </p>\n",
  "email" => "",
  "website" => "",
  "sponsor_id" => "MP",
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!",
  "organization" => true,
  "mission_title" => "A New Perspective Restoration Corporation",
  "mission_statement" => "ANPRC is a non-profit corporation, for sustainable living education and wellness organization.  We are setting up intelligent living communities based upon sustainable practices such as, solar and wind energy, alternative building construction, aquaponics, greenhouse agriculture, natural compost, and clean biofuels.  \r\n\r\nWe also have a vision to set up educational centers where individuals and communities can learn safe and sustainable practices; such as, greenhouse gardening, recycling garbage and compost and economical building practices, and wellness including diet and nutrition."
}

aff <<

{
  "_id" => "56a7d75c47ffb071bd0213d8",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "main" => true,
  "affiliate" => true,
  "name" => "Edward Daniel (Marketing Director - India)",
  "phone" => "+919047886433",
  "alias" => "edde",
  "user_id" => "56a7d75c47ffb071bd0213d5",
  "email" => "",
  "website" => "",
  "description" => "<p style=\"text-align =>center\"><img alt=\"\" src=\"https =>//m2.marketpad.com/secured-images/cde6b9b0fba9792d3ce38cbc9763cac5fa6ba470/687474703a2f2f696d616765736861636b2e636f6d2f612f696d673930352f313532372f63354c5352732e6a7067\" style=\"float =>left; height =>245px; width =>375px\"><img alt=\"\" src=\"https =>//m0.marketpad.com/media/W1siZiIsIjU2YTkxMjQyNDdmZmIwMDAwNzAwMDM3YyJdXQ/maduri.jpg\" style=\"float =>right; height =>243px; width =>375px\"></p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"> </p>\n\n<p style=\"text-align =>center\"><span style=\"font-family =>arial,helvetica,sans-serif\"><strong><span style=\"font-size =>24px\"><a href=\"http =>//anprc.webs.com/\">HOME</a> | <a href=\"http =>//anprc.webs.com/apps/videos/\">VIDEOS</a> | <a href=\"http =>//anprc.webs.com/apps/photos/\">PHOTOS</a> | <a href=\"http =>//anprc.webs.com/mission.htm\">MISSION</a> | <a href=\"https =>//marketpad.com/anprc#The%20Vision\">VISION</a></span></strong></span></p>\n\n<p style=\"text-align =>center\"><span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"color =>rgb(0, 0, 205)\"><strong><span style=\"font-size =>24px\">ANPRC is all about communities</span></strong></span><span style=\"color =>rgb(0, 0, 205)\"><strong>.</strong></span></span></p>\n\n<p style=\"text-align =>center\"><span style=\"font-size =>14px\"><span style=\"font-family =>arial,helvetica,sans-serif\"><a href=\"https =>//marketpad.com/anprc#Click%20On%20Logo%20To%20See%20Local%20Businesses.\"><strong><span style=\"color =>rgb(0, 128, 0)\">| </span></strong><span style=\"color =>rgb(255, 140, 0)\">Click Here To See Some Local </span><span style=\"color =>rgb(255, 140, 0)\">Buisnesses And Charities ANPRC Supports</span></a><strong><a href=\"https =>//marketpad.com/anprc#Click%20On%20Logo%20To%20See%20Local%20Businesses.\"><span style=\"color =>rgb(255, 0, 0)\"> </span></a></strong><a href=\"https =>//marketpad.com/anprc#Click%20On%20Logo%20To%20See%20Local%20Businesses.\"><strong><span style=\"color =>rgb(0, 128, 0)\">|</span></strong></a></span></span></p>\n\n<h1 style=\"text-align =>center\"><u><span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"font-size =>26px\"><strong><span style=\"color =>black\">A New Perspective Restoration Corporation</span></strong></span></span></u></h1>\n\n<p><span style=\"font-family =>arial,helvetica,sans-serif; font-size =>16px\">ANPRC is a non-profit corporation, for sustainable living education and wellness organization.  We are setting </span></p>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">ANPRC is a non-profit corporation, for sustainable living education and wellness organization.  We are setting up intelligent living communities based upon sustainable practices such as, solar and wind energy, alternative building construction, aquaponics, greenhouse agriculture, natural compost, and clean biofuels.  </span></span></p>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">We also have a vision to set up educational centers where individuals and communities can learn safe and </span></span><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">sustainable practices; such as, greenhouse gardening, recycling garbage and compost and economical building practices, and wellness including diet and nutrition.</span></span></p>\n\n<h1 style=\"text-align =>center\"><u><span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"font-size =>26px\"><a id=\"The Vision\" name=\"The%20Vision\"><span style=\"color =>rgb(0, 0, 0)\"><strong>The Vision</strong></span></a></span></span></u></h1>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">A New Perspective Restoration Corporation is an innovative company bringing diversified, environmentally responsible training and products to the Sustainable Living and Development Centers (SLDC) demonstrating the principles of Permaculture, Ecolonomics, Sustainable Agriculture, Alternative Energy production, and a Wellness Center.</span></span></p>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">A New Perspective Restoration Corporation (ANPRC) is dedicated to providing a variety of high-quality, low-cost products to sell to consumers who want healthful, affordable foods. ANPRC also provides training in the fields of hydroponics, aquaculture, and composting. ANPRC is committed to meeting the demand for healthy locally-grown agricultural products, which are grown in computer-controlled, environmentally-friendly greenhouses.</span></span></p>\n\n<p><span style=\"font-size =>16px\"><span style=\"font-family =>arial,helvetica,sans-serif\">ANPRC has a vision for environmental, human, and economic health that motivates every aspect of this plan.</span></span></p>\n"
}

aff <<

{
  "_id" => "55b7b9bdf02cbe9d06002763",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "GarysMarketing",
  "alias" => "garysmarketing",
  "email" => "gary@marketpad.com",
  "phone" => "208 818 5878",
  "website" => "",
  "main" => false,
  "user_id" => "4fb5539508dea13806000219",
  "description" => "",
  "affiliate" => true,
  "emailer_description" => "Like to Save Money? Sign up to recieve our deals via email!",
  "organization" => false
}

aff <<
{
  "_id" => "57a105054811f5f97900183f",
  "emailer_description" => "Like to Save Money? ",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Century 21 Beutler & Associates",
  "alias" => "century21johnbeutler",
  "email" => "",
  "phone" => "(208) 765-5554",
  "website" => "",
  "main" => false,
  "affiliate" => true,
  "organization" => false,
  "user_id" => "4fb5539508dea13806000219",
  "mission_title" => "Century 21 John Beutler & Associates ",
  "mission_statement" => " Century 21 located at => 1836 Northwest Blvd Coeur d'Alene, ID\r\n\r\nThis special site is for our employees, family, friends and extended networks. This storefront is filled with local deals to restaurants, chiropractors, tanning salons, massage therapists, dropship merchants and more! You work hard for the money you have and we want you to enjoy it, but save money too.\r\n\r\nA portion of EVERY purchase made here will go to the Charities we support, XYZ.  Our goal is to help you help them fundraise, while we help you save $$$ doing things you already do.\r\n\r\nGroupon takes up to 50-60% of small businesses sales profits, whereas these deals leave business owners with over 90% of their profits. Our goal is to help build up our local economy, small businesses and enjoy great deals along the way. Browse! Purchase! Save! Share! Repeat!\r\n",
  "description" => ""
}

aff <<

{
  "_id" => "56bd060aa10a1a45b100196f",
  "show_real_name" => true,
  "show_email" => false,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "Honeycomb Hair Studio",
  "alias" => "honeycombhairstudio",
  "email" => "test@test.com",
  "phone" => "(208) 818-0727",
  "website" => "https =>//www.facebook.com/Honeycombhairstudiopf",
  "main" => true,
  "affiliate" => true,
  "user_id" => "56c372febd40b1fa71005acf",
  "description" => "<p><img alt=\"\" src=\"https =>//m1.marketpad.com/media/W1siZiIsIjU2YmQwNjIyYTEwYTFhMDAwNzAwMDAyNSJdXQ/honeycomb5.jpg\" style=\"float =>left; height =>243px; width =>375px\"><img alt=\"\" src=\"https =>//m3.marketpad.com/media/W1siZiIsIjU2YmQwNjJjYTEwYTFhMDAwNzAwMDAyOSJdXQ/honeycomb2.jpg\" style=\"float =>right; height =>243px; width =>375px\"></p>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>\n\n<h1 style=\"text-align =>center\"><br>\n<strong><u><span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"font-size =>28px\">Honeycomb Hair Studio - Post Falls ID</span></span></u></strong></h1>\n\n<p><span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"font-size =>16px\">At Honeycomb Hair Studio you can get it all! From all kinds of hair services, to massage, mani and pedi, facials, waxing and many more services give us a call at </span></span>(208) 283-0116<span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"font-size =>16px\"> we would love to talk with you.</span></span></p>\n\n<p style=\"text-align =>center\"><strong><u><span style=\"font-family =>arial,helvetica,sans-serif\"><span style=\"font-size =>28px\">Our Deals</span></span></u></strong></p>\n\n<p> </p>\n",
}


aff <<

{
  "_id" => "56bbbb12802e887f3d00dd8e",
  "show_real_name" => true,
  "show_email" => true,
  "show_phone" => true,
  "show_website" => true,
  "show_location" => true,
  "name" => "MarketPad's Virtual Store",
  "alias" => "mpwholesale",
  "email" => "marketing@marketpad.net",
  "phone" => "(208) 953-1590",
  "website" => "",
  "main" => false,
  "affiliate" => true,
  "user_id" => "561e9b30f02cbe7a1300656c",
  "description" => "",
  "emailer_description" => "Sign up to receive our deals via email!",
  "organization" => false,
  "mission_title" => "MarketPad's Virtual Store",
  "mission_statement" => "We are proud to offer items to the public that are at a great DISCOUNT.  Products such as custom safety/sunglasses (logo included), men's and women's accessories, gourmet instant healthy coffee, sweet compression clothing for personal or team use, and MORE!  \r\n\r\nWe carry individual products and products in bulk.  Bulk items are at an even greater discounted price, so that you can resell as a great source to raise money, or start your own company by reselling our discounted products.  \r\n\r\nSavings are instant and so is the ability to make money.  \r\n\r\nBROWSE! SHOP! SHARE! REPEAT!"
}



aff.each do |o|

  user = User.find_by_old_mongo_id(o["user_id"])
  if user.blank?
    user = User.find_by_email(o["email"])
  end

  if user.blank?
    user = User.new
    user.user_type = "affiliate"
    user.email = o["email"]
    user.first_name = o["name"]
    user.last_name = o["name"]
    user.password = 'password'
    user.save

  end

  if user.present?
    user.user_type = "affiliate"
    user.save

    profile = Profile.new
    profile.user_id = user.id
    profile.name = o["name"]
    profile.profile_type = user.user_type
    profile.description = o["description"].present? ? o["description"] : o["mission_statement"]
    profile.website = o["website"]
    profile.phone_number = o["phone"]
    profile.save(validate: false)
  end

end