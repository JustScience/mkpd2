namespace :mark_discounts_inactive do

	desc "Mark All Discount inactive whose expiry date is today"
  task :update => :environment do
  	start_date = Date.today.beginning_of_day
  	end_date = Date.today.end_of_day
  	discounts = Discount.where(active: true, sold_out: true).where("expiry_date >= '#{start_date}' AND expiry_date <= '#{end_date}'")
  	if discounts.present?
  		discounts.each do |d|
  			d.update(active: false)
  			puts "Date #{start_date}"
  			puts "Marked discount inactive ID:: #{d.id}"
  			Rails.logger.info "Date #{start_date}"
  			Rails.logger.info "Marked discount inactive ID:: #{d.id}"
  		end
  	else
  		puts "No Discount expiring today date:: #{start_date}"
  		Rails.logger.info "No Discount expiring today date:: #{start_date}"
  	end
  end
	
end